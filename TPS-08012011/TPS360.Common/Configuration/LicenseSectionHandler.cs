﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: LicenseSectionHandler.cs
    Description: This is the page used read and write license details to web.config.
    Created By: Anand Dixit
    Created On: 15th June 2009
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1               15-Jun-2009         Anand                 Created.
    0.2               24-Jun-2009         Anand                 Renamed all classes starting with License to TPS360 to make it a generic implemetation
------------------------------------------------------------------------------------------------------------------------------------------- 
*/

namespace TPS360.Common
{
    #region Namespaces
    using System;
    using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
    #endregion
    /*
    <configSections>
          <section name="tps360license" type="TPS360.Common.LicenseSectionHandler,TPS360.Common"    
        allowDefinition="Everywhere"
        allowExeDefinition="MachineToApplication"
        restartOnExternalChanges="false"  requirePermission="false" />
     </configSections>
    <tps360license>
        <elements>
            <clear />
            <add name="TPS360LV"   value="password=Tps360#^);user id=sa;server=BRIGHU;database=LICENSE;connection timeout=600" />
            <add name="LicenseKey" value="3020302EREDDEE" />
            <add name="StartDate"  value="12-02-2009" />
            <add name="ExpiryDate" value="12-02-2009" />
        </license>
    </tps360license>
     */
    public class TPS360SectionHandler : ConfigurationSection
    {
        [ConfigurationProperty("elements", IsDefaultCollection = false)]
        [ConfigurationCollection(typeof(TPS360ConfigCollection),
            AddItemName = "add",
            ClearItemsName = "clear",
            RemoveItemName = "remove")]

        public TPS360ConfigCollection Details
        {
            get
            {
                TPS360ConfigCollection license =
                    (TPS360ConfigCollection)base["elements"];
                return license;
            }
        }

    }


    public class TPS360ConfigCollection : ConfigurationElementCollection
    {
        public TPS360ConfigCollection()
        {

            TPS360ConfigElement licenseElement = (TPS360ConfigElement)CreateNewElement();
            Add(licenseElement);
        }

        public override ConfigurationElementCollectionType CollectionType
        {
            get
            {
                return ConfigurationElementCollectionType.AddRemoveClearMap;
            }
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new TPS360ConfigElement();
        }

        protected override object GetElementKey(ConfigurationElement element)  
        {
            return ((TPS360ConfigElement)element).Name;
        }

        public TPS360ConfigElement this[int index]
        {
            get
            {
                return (TPS360ConfigElement)BaseGet(index);
            }
            set
            {
                if (BaseGet(index) != null)
                {
                    BaseRemoveAt(index);
                }
                BaseAdd(index, value);
            }
        }

        new public TPS360ConfigElement this[string Name]
        {
            get
            {
                return (TPS360ConfigElement)BaseGet(Name);
            }
        }

        public int IndexOf(TPS360ConfigElement element)
        {
            return BaseIndexOf(element);
        }

        public void Add(TPS360ConfigElement element)
        {
            BaseAdd(element);
        }
        protected override void BaseAdd(ConfigurationElement element)
        {
            BaseAdd(element, false);
        }

        public void Remove(TPS360ConfigElement element)
        {
            if (BaseIndexOf(element) >= 0)
                BaseRemove(element.Name);
        }

        public void RemoveAt(int index)
        {
            BaseRemoveAt(index);
        }

        public void Remove(string name)
        {
            BaseRemove(name);
        }

        public void Clear()
        {
            BaseClear();
        }

    }
    public class TPS360ConfigSection : ConfigurationSection
    {

        [ConfigurationProperty("elements", IsDefaultCollection = false)]
        [ConfigurationCollection(typeof(TPS360ConfigCollection),
            AddItemName = "add",
            ClearItemsName = "clear",
            RemoveItemName = "remove")]

        public TPS360ConfigCollection Details
        {
            get
            {
                TPS360ConfigCollection licenseCollection =
                    (TPS360ConfigCollection)base["elements"];
                return licenseCollection;
            }
        }


    }


    public class TPS360ConfigElement : ConfigurationElement
    {
        public TPS360ConfigElement(string name,string strvalue) 
        {
            this.Name = name;
            this.Value = strvalue;
        }
        public TPS360ConfigElement()
        {
        }

        [ConfigurationProperty("name", DefaultValue = "TPS360RS",
            IsRequired = true, IsKey = true)]
        public string Name
        {
            get
            {
                return (string)this["name"];
            }
            set
            {
                this["name"] = value;
            }
        }


        [ConfigurationProperty("value", DefaultValue = @"password=Tps360)^#;user id=sa;server=license.talentrackr.com;database=License;connection timeout=600",
            IsRequired = true)]
        //[ConfigurationProperty("value", DefaultValue = @"Data Source=.\SQLEXPRESS;Initial Catalog=License; integrated security=true; Connection Timeout=600",
        //  IsRequired = true)]
        //[ConfigurationProperty("value", DefaultValue = @"password=talent;user id=sa;server=localhost\xpress;database=Version151;connection timeout=600",
        // IsRequired = true)]
        //[ConfigurationProperty("value", DefaultValue = @"password=Tps543$#;user id=sa;server=si-sv2421.com,2433;database=License;connection timeout=600",
        //   IsRequired = true)]

        public string Value
        {
            get
            {
                return (string)this["value"];
            }
            set
            {
                this["value"] = value;
            }
        }
    }
    }

