using System;
using System.Collections;
using System.Collections.Specialized;
using System.Configuration;


namespace TPS360.Common
{
	internal sealed class Settings
	{
		private ListDictionary _mailMaps = new ListDictionary();

		private static Settings _instance;
		
		public Settings()
		{
		}


		public static Settings Instance
		{
			get
			{
				const string CONFIG_SECTION = "commonSetting";

				if (_instance == null)
				{
					lock(typeof(Settings))
					{
						if (_instance == null)
						{
                            _instance = (Settings)ConfigurationManager.GetSection(CONFIG_SECTION);
						}
					}
				}

				return _instance;
			}
		}


		public MailMap GetMailMap(string key)
		{
			if (_mailMaps.Contains(key))
			{
				return _mailMaps[key] as MailMap;
			}

			return null;
		}


		public void SetMailMap(string key,
			MailMap map)
		{
			_mailMaps[key] = map;
		}
	}
}