﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "CompanyReference", Namespace = "http://www.tps360.com/types")]
    public class CompanyReference : BaseEntity
    {
        #region Properties

        [DataMember]
        public string CompanyName
        {
            get;
            set;
        }

        [DataMember]
        public string ContactName
        {
            get;
            set;
        }

        [DataMember]
        public string ContactPosition
        {
            get;
            set;
        }

        [DataMember]
        public string ContactEmail
        {
            get;
            set;
        }

        [DataMember]
        public string PhoneNumber
        {
            get;
            set;
        }

        [DataMember]
        public string Description
        {
            get;
            set;
        }

        [DataMember]
        public int CompanyId
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public CompanyReference()
            : base()
        {
        }

        #endregion
    }
}