﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "ActivityResource", Namespace = "http://www.tps360.com/types")]
    public class ActivityResource : BaseEntity
    {
        #region Properties

        [DataMember]
        public int ActivityID
        {
            get;
            set;
        }

        [DataMember]
        public int ResourceID
        {
            get;
            set;
        }

        #endregion

        #region Constructor

        public ActivityResource()
            : base()
        {
        }

        #endregion
    }
}