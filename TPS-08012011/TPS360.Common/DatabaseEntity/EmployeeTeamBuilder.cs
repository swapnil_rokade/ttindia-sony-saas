﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "EmployeeTeamBuilder", Namespace = "http://www.tps360.com/types")]
    public class EmployeeTeamBuilder : BaseEntity
    {
        #region Properties

        [DataMember]
        public string Title
        {
            get;
            set;
        }

        [DataMember]
        public int TeamLeader
        {
            get;
            set;
        }
       
		[DataMember]
        public string EmployeeId
        {
            get;
            set;
        }

        [DataMember]
        public string TeamLeaderName
        {
            get;
            set;
        }

        [DataMember]
        public string TeamMemberList 
        {
            get;
            set;
        }
		 

        #endregion

        #region Constructor

        public EmployeeTeamBuilder()
            : base()
        {
        }

        #endregion
    }
}