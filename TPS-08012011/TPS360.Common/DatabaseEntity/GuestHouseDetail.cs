﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "GuestHouseDetail", Namespace = "http://www.tps360.com/types")]
    public class GuestHouseDetail : BaseEntity
    {
        #region Properties

        [DataMember]
        public int SeatNumber
        {
            get;
            set;
        }

        [DataMember]
        public string SeatCode
        {
            get;
            set;
        }

        [DataMember]
        public decimal SeatRent
        {
            get;
            set;
        }

        [DataMember]
        public int SeatRentCurrencyLookupId
        {
            get;
            set;
        }

        [DataMember]
        public string PhotoOfSeat
        {
            get;
            set;
        }

        [DataMember]
        public int GuestHouseId
        {
            get;
            set;
        }

        [DataMember]
        public bool IsAvailable
        {
            get;
            set;
        }

        [DataMember]
        public int Status
        {
            get;
            set;
        }

        [DataMember]
        public bool IsVIPSeat
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }

        #endregion

        #region Constructor

        public GuestHouseDetail()
            : base()
        {
        }

        #endregion
    }
}