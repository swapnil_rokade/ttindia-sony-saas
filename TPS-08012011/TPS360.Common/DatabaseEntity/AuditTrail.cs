﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "AditTrail", Namespace = "http://www.tps360.com/types")]
    public class AuditTrail : BaseEntity
    {
        #region Properties

        [DataMember]
        public Int64 ActivityControlMapID
        {
            get;
            set;
        }

        [DataMember]
        public string Page
        {
            get;
            set;
        }

        [DataMember]
        public string Module
        {
            get;
            set;
        }

        [DataMember]
        public string Fields
        {
            get;
            set;
        }

        [DataMember]
        public string Values
        {
            get;
            set;
        }

        [DataMember]
        public Int64 CreatorId
        {
            get;
            set;
        }

        [DataMember]
        public DateTime CreatedOn
        {
            get;
            set;
        }

        [DataMember]
        public Int64 EntityID
        {
            get;
            set;
        }

        [DataMember]
        public string EntityName
        {
            get;
            set;
        }     
        #endregion

        #region Constructor

        public AuditTrail(): base()
        {

        }
        #endregion
    }
}
