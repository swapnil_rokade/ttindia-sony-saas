﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "MemberManager", Namespace = "http://www.tps360.com/types")]
    public class MemberManager : BaseEntity
    {
        #region Properties

        [DataMember]
        public int MemberId
        {
            get;
            set;
        }

        [DataMember]
        public int ManagerId
        {
            get;
            set;
        }

        [DataMember]
        public bool IsPrimaryManager
        {
            get;
            set;
        }

        [DataMember]
        public int UnAssignedBy
        {
            get;
            set;
        }

        [DataMember]
        public DateTime UnAssignDate
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public MemberManager()
            : base()
        {
        }

        #endregion
    }
}