﻿using System;
using System.Diagnostics;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "MemberAttribute", Namespace = "http://www.tps360.com/types")]
    public class MemberAttribute : BaseEntity
    {
        #region Properties

        [DataMember]
        public string Comment
        {
            get;
            set;
        }

        [DataMember]
        public decimal Score
        {
            get;
            set;
        }

        [DataMember]
        public int AppraisalAttributeId
        {
            get;
            set;
        }

        [DataMember]
        public int AppraisalRatingId
        {
            get;
            set;
        }

        [DataMember]
        public int MemberId
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public MemberAttribute()
            : base()
        {
        }

        #endregion
    }
}