﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "CampaignDocument", Namespace = "http://www.tps360.com/types")]
    public class CampaignDocument : BaseEntity
    {
        #region Properties

        [DataMember]
        public int CampaignId
        {
            get;
            set;
        }

        [DataMember]
        public string Title
        {
            get;
            set;
        }

        [DataMember]
        public string FileName
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public CampaignDocument()
            : base()
        {
        }

        #endregion
    }
}
