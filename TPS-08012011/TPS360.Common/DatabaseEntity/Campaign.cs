﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "Campaign", Namespace = "http://www.tps360.com/types")]
    public class Campaign : BaseEntity
    {
        #region Properties

        [DataMember]
        public string Name
        {
            get;
            set;
        }

        [DataMember]
        public string Description
        {
            get;
            set;
        }

        [DataMember]
        public DateTime StartDate
        {
            get;
            set;
        }

        [DataMember]
        public DateTime EndDate
        {
            get;
            set;
        }

        [DataMember]
        public string Location
        {
            get;
            set;
        }

        [DataMember]
        public int Status
        {
            get;
            set;
        }

        [DataMember]
        public decimal Price
        {
            get;
            set;
        }

        [DataMember]
        public int PriceCurrencyLookupId
        {
            get;
            set;
        }        

        [DataMember]
        public decimal ExpectedRevenue
        {
            get;
            set;
        }

        [DataMember]
        public int ExpectedRevenueCurrencyLookupId
        {
            get;
            set;
        }

        [DataMember]
        public string DiscountPolicy
        {
            get;
            set;
        }

        [DataMember]
        public int ProductId
        {
            get;
            set;
        }

        [DataMember]
        public int NumberOfCompanies
        {
            get;
            set;
        }

        [DataMember]
        public int NumberOfTeam
        {
            get;
            set;
        }

        [DataMember]
        public int CampaignColdCallScriptId
        {
            get;
            set;
        }

        [DataMember]
        public int EmailContentId
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public Campaign()
            : base()
        {
        }

        #endregion
    }
}