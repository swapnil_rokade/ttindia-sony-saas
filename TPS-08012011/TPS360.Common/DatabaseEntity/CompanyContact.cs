﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "CompanyContact", Namespace = "http://www.tps360.com/types")]
    public class CompanyContact : BaseEntity
    {
        #region Properties

        [DataMember]
        public int CompanyId
        {
            get;
            set;
        }

        [DataMember]
        public bool IsPrimaryContact
        {
            get;
            set;
        }

        [DataMember]
        public string Title
        {
            get;
            set;
        }

        [DataMember]
        public bool NoBulkEmail
        {
            get;
            set;
        }

        [DataMember]
        public string Email
        {
            get;
            set;
        }

        [DataMember]
        public string FirstName
        {
            get;
            set;
        }

        [DataMember]
        public string LastName
        {
            get;
            set;
        }

        [DataMember]
        public string Address1
        {
            get;
            set;
        }

        [DataMember]
        public string Address2
        {
            get;
            set;
        }

        [DataMember]
        public string City
        {
            get;
            set;
        }

        [DataMember]
        public int StateId
        {
            get;
            set;
        }

        [DataMember]
        public int CountryId
        {
            get;
            set;
        }

        [DataMember]
        public string ZipCode
        {
            get;
            set;
        }

        [DataMember]
        public string OfficePhone
        {
            get;
            set;
        }

        [DataMember]
        public string OfficePhoneExtension
        {
            get;
            set;
        }

        [DataMember]
        public string MobilePhone
        {
            get;
            set;
        }

        [DataMember]
        public string Fax
        {
            get;
            set;
        }

        [DataMember]
        public string DirectNumber
        {
            get;
            set;
        }

        [DataMember]
        public string DirectNumberExtension
        {
            get;
            set;
        }

        [DataMember]
        public string ContactRemarks
        {
            get;
            set;
        }

        [DataMember]
        public bool IsOwner
        {
            get;
            set;
        }

        [DataMember]
        public bool PortalAccess
        {
            get;
            set;
        }

        [DataMember]
        public decimal OwnershipPercentage
        {
            get;
            set;
        }

        [DataMember]
        public string ResumeFile
        {
            get;
            set;
        }

        [DataMember]
        public int EthnicGroupLookupId
        {
            get;
            set;
        }

        [DataMember]
        public int MemberId
        {
            get;
            set;
        }

        [DataMember]
        public int DivisionLookupId
        {
            get;
            set;
        }

        [DataMember]
        public string FolderName
        {
            get;
            set;
        }
        #endregion

        #region Constructor

        public CompanyContact()
            : base()
        {
        }

        #endregion
    }
}
