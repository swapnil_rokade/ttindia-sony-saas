﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "CommonTask", Namespace = "http://www.tps360.com/types")]
    public class CommonTask : BaseEntity
    {
        #region Properties

        [DataMember]
        public string Subject
        {
            get;
            set;
        }

        [DataMember]
        public string Description
        {
            get;
            set;
        }

        [DataMember]
        public int Status
        {
            get;
            set;
        }

        [DataMember]
        public DateTime StartDate
        {
            get;
            set;
        }

        [DataMember]
        public DateTime EndDate
        {
            get;
            set;
        }

        [DataMember]
        public DateTime ReminderDate
        {
            get;
            set;
        }

        [DataMember]
        public int Priority
        {
            get;
            set;
        }

        [DataMember]
        public int TaskCategoryId
        {
            get;
            set;
        }

        [DataMember]
        public int ActivityId
        {
            get;
            set;
        }

        [DataMember]
        public int AppliedTo
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public CommonTask()
            : base()
        {
        }

        #endregion
    }
}