﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "JobFamily", Namespace = "http://www.tps360.com/types")]
    public class JobProfile : BaseEntity
    {
        #region Properties

        [DataMember]
        public string JobFamily
        {
            get;
            set;
        }
        [DataMember]
        public Int32 Id
        {
            get;
            set;
        }
        [DataMember]
        public string JobProfileCode
        {
            get;
            set;
        }
        #endregion

        #region Constructor

        public JobProfile()
            : base()
        {
        }

        #endregion
    }
}
