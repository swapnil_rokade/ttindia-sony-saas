﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "MemberEducation", Namespace = "http://www.tps360.com/types")]
    public class MemberEducation : BaseEntity
    {
        #region Properties

        [DataMember]
        public int LevelOfEducationLookupId
        {
            get;
            set;
        }

        [DataMember]
        public string DegreeTitle
        {
            get;
            set;
        }

        [DataMember]
        public int FieldOfStudyLookupId
        {
            get;
            set;
        }

        [DataMember]
        public string MajorSubjects
        {
            get;
            set;
        }

        [DataMember]
        public string GPA
        {
            get;
            set;
        }

        [DataMember]
        public string GpaOutOf
        {
            get;
            set;
        }

        [DataMember]
        public DateTime AttendedFrom
        {
            get;
            set;
        }

        [DataMember]
        public DateTime AttendedTo
        {
            get;
            set;
        }

        [DataMember]
        public string DegreeObtainedYear
        {
            get;
            set;
        }

        [DataMember]
        public string InstituteName
        {
            get;
            set;
        }

        [DataMember]
        public string InstituteCity
        {
            get;
            set;
        }

        [DataMember]
        public int InstituteCountryId
        {
            get;
            set;
        }

        [DataMember]
        public string BriefDescription
        {
            get;
            set;
        }

        [DataMember]
        public bool IsHighestEducation
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }

        [DataMember]
        public int MemberId
        {
            get;
            set;
        }
        // 11065
        public int StateId
        {
            get;
            set;
        }
        #endregion

        #region Constructor

        public MemberEducation()
            : base()
        {
        }

        #endregion
    }
}