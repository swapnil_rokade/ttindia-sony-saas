﻿using System;
using System.Diagnostics;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "TrainingProgramDetail", Namespace = "http://www.tps360.com/types")]
    public class TrainingProgramDetail : BaseEntity
    {
        #region Properties

        [DataMember]
        public int Status
        {
            get;
            set;
        }

        [DataMember]
        public string Note
        {
            get;
            set;
        }

        [DataMember]
        public int TrainingProgramId
        {
            get;
            set;
        }

        [DataMember]
        public int TrainingCourseModuleId
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public TrainingProgramDetail()
            : base()
        {
        }

        #endregion
    }
}