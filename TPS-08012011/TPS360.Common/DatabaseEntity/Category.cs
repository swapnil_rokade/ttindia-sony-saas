﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "Category", Namespace = "http://www.tps360.com/types")]
    public class Category : BaseEntity
    {
        #region Properties

        [DataMember]
        public string Name
        {
            get;
            set;
        }

       

        #endregion

        #region Constructor

        public Category()
            : base()
        {
        }

        #endregion
    }
}