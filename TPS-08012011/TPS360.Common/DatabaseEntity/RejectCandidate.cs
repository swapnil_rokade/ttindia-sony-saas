﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: RejectCandidate.cs
    Description: 
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.        Date               Author           Modification
    ------------------------------------------------------------------------------------------------------------------------------    
    0.1           14/March/2016       pravin khot      New Fields Added.
                                                                       
-------------------------------------------------------------------------------------------------------------------------------------------        
*/


using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "RejectCandidate", Namespace = "http://www.tps360.com/types")]
    public class RejectCandidate : BaseEntity 
    {
        #region Properties

        [DataMember]
        public int MemberId
        {
            get;
            set;
        }

        public int JobPostingID
        {
            get;
            set;
        }

        [DataMember]
        public string  RejectDetails
        {
            get;
            set;
        }

        [DataMember]
        public int HiringMatrixLevel
        {
            get;
            set;
        }

        [DataMember]
        public string FirstName
        {
            get;
            set;
        }

        [DataMember]
        public string MiddleName
        {
            get;
            set;
        }

        [DataMember]
        public string LastName
        {
            get;
            set;
        }

        [DataMember]
        public string RejectorName
        {
            get;
            set;
        }

        [DataMember]
        public string CellPhone
        {
            get;
            set;
        }

        [DataMember]
        public string PrimaryEmail
        {
            get;
            set;
        }

        [DataMember]
        public string DateTime
        {
            get;
            set;
        }

        [DataMember]
        public string JobTitle
        {
            get;
            set;
        }

        [DataMember]
        public string InterviewTitle
        {
            get;
            set;
        }
     
        //*******fields added by pravin khot on 14/March/2016*******START CODE********

        [DataMember]
        public string PrimaryPhone
        {
            get;
            set;
        }
        [DataMember]
        public string PrimaryPhoneExtension
        {
            get;
            set;
        }
        [DataMember]
        public int Status
        {
            get;
            set;
        }
        [DataMember]
        public string HomePhone
        {
            get;
            set;
        }

        [DataMember]
        public string CurrentCity
        {
            get;
            set;
        }

        [DataMember]
        public string StateName
        {
            get;
            set;
        }

        [DataMember]
        public string StateCode
        {
            get;
            set;
        }

        [DataMember]
        public string OfficeCity
        {
            get;
            set;
        }

        [DataMember]
        public string CurrentPosition
        {
            get;
            set;
        }

        [DataMember]
        public string Remarks
        {
            get;
            set;
        }

        [DataMember]
        public string Objective
        {
            get;
            set;
        }
        [DataMember]
        public string Summary
        {
            get;
            set;
        }

        [DataMember]
        public string Skills
        {
            get;
            set;
        }
        [DataMember]
        public Guid UserId
        {
            get;
            set;
        }
       
        [DataMember]
        public string NickName
        {
            get;
            set;
        }

        [DataMember]
        public DateTime DateOfBirth
        {
            get;
            set;
        }

        [DataMember]
        public string PermanentAddressLine1
        {
            get;
            set;
        }

        [DataMember]
        public string PermanentAddressLine2
        {
            get;
            set;
        }

        [DataMember]
        public string PermanentCity
        {
            get;
            set;
        }

        [DataMember]
        public int PermanentStateId
        {
            get;
            set;
        }

        [DataMember]
        public string PermanentZip
        {
            get;
            set;
        }

        [DataMember]
        public int PermanentCountryId
        {
            get;
            set;
        }

        [DataMember]
        public string PermanentPhone
        {
            get;
            set;
        }

        [DataMember]
        public string PermanentPhoneExt
        {
            get;
            set;
        }

        [DataMember]
        public string PermanentMobile
        {
            get;
            set;
        }    

        [DataMember]
        public string AlternateEmail
        {
            get;
            set;
        }    
        [DataMember]
        public string PermanentStateName
        {
            get;
            set;
        }

        [DataMember]
        public string PermanentStateCode
        {
            get;
            set;
        }
        [DataMember]
        public string PermanentCountryName
        {
            get;
            set;
        }

        [DataMember]
        public string PermanentCountryCode
        {
            get;
            set;
        }

        [DataMember]
        public string CurrentAddressLine1
        {
            get;
            set;
        }

        [DataMember]
        public string CurrentAddressLine2
        {
            get;
            set;
        }

        [DataMember]
        public int CurrentStateId
        {
            get;
            set;
        }

        [DataMember]
        public string CurrentStateName
        {
            get;
            set;
        }

        [DataMember]
        public string CurrentStateCode
        {
            get;
            set;
        }

        [DataMember]
        public string CurrentZip
        {
            get;
            set;
        }

        [DataMember]
        public int CurrentCountryId
        {
            get;
            set;
        }

        [DataMember]
        public string CurrentCountryName
        {
            get;
            set;
        }

        [DataMember]
        public string CurrentCountryCode
        {
            get;
            set;
        }

        [DataMember]
        public string OfficePhone
        {
            get;
            set;
        }

        [DataMember]
        public string OfficePhoneExtension
        {
            get;
            set;
        }

        [DataMember]
        public string CityOfBirth
        {
            get;
            set;
        }

        [DataMember]
        public int ResumeSource
        {
            get;
            set;
        }
        [DataMember]
        public string CountryIdOfBirth
        {
            get;
            set;
        }

        [DataMember]
        public string BirthCountryName
        {
            get;
            set;
        }

        [DataMember]
        public string BirthCountryCode
        {
            get;
            set;
        }

        [DataMember]
        public string CountryIdOfCitizenship
        {
            get;
            set;
        }

        [DataMember]
        public string CitizenshipCountryName
        {
            get;
            set;
        }

        [DataMember]
        public string CitizenshipCountryCode
        {
            get;
            set;
        }

        [DataMember]
        public int GenderLookupId
        {
            get;
            set;
        }

        [DataMember]
        public string Gender
        {
            get;
            set;
        }

        [DataMember]
        public int EthnicGroupLookupId
        {
            get;
            set;
        }

        [DataMember]
        public string EthnicGroup
        {
            get;
            set;
        }

        [DataMember]
        public int BloodGroupLookupId
        {
            get;
            set;
        }

        [DataMember]
        public string BloodGroup
        {
            get;
            set;
        }

        [DataMember]
        public int MaritalStatusLookupId
        {
            get;
            set;
        }

        [DataMember]
        public string MaritalStatus
        {
            get;
            set;
        }

        [DataMember]
        public bool Relocation
        {
            get;
            set;
        }    

        [DataMember]
        public string TotalExperienceYears
        {
            get;
            set;
        }

        [DataMember]
        public int Availability
        {
            get;
            set;
        }

        [DataMember]
        public string AvailabilityText
        {
            get;
            set;
        }

        [DataMember]
        public DateTime AvailableDate
        {
            get;
            set;
        }

        [DataMember]
        public decimal CurrentYearlyRate
        {
            get;
            set;
        }

        [DataMember]
        public int CurrentYearlyCurrencyLookupId
        {
            get;
            set;
        }

        [DataMember]
        public string CurrentYearlyCurrency
        {
            get;
            set;
        }

        [DataMember]
        public decimal CurrentMonthlyRate
        {
            get;
            set;
        }

        [DataMember]
        public int CurrentMonthlyCurrencyLookupId
        {
            get;
            set;
        }

        [DataMember]
        public string CurrentMonthlyCurrency
        {
            get;
            set;
        }
        [DataMember]
        public string LastEmployer
        {
            get;
            set;
        }
        [DataMember]
        public decimal CurrentHourlyRate
        {
            get;
            set;
        }

        [DataMember]
        public int CurrentHourlyCurrencyLookupId
        {
            get;
            set;
        }

        [DataMember]
        public string CurrentHourlyCurrency
        {
            get;
            set;
        }

        [DataMember]
        public decimal ExpectedYearlyRate
        {
            get;
            set;
        }

        [DataMember]
        public int ExpectedYearlyCurrencyLookupId
        {
            get;
            set;
        }

        [DataMember]
        public string ExpectedYearlyCurrency
        {
            get;
            set;
        }

        [DataMember]
        public decimal ExpectedMonthlyRate
        {
            get;
            set;
        }

        [DataMember]
        public int ExpectedMonthlyCurrencyLookupId
        {
            get;
            set;
        }

        [DataMember]
        public string ExpectedMonthlyCurrency
        {
            get;
            set;
        }

        [DataMember]
        public decimal ExpectedHourlyRate
        {
            get;
            set;
        }

        [DataMember]
        public int ExpectedHourlyCurrencyLookupId
        {
            get;
            set;
        }

        [DataMember]
        public string ExpectedHourlyCurrency
        {
            get;
            set;
        }

        [DataMember]
        public bool WillingToTravel
        {
            get;
            set;
        }

        [DataMember]
        public int SalaryTypeLookupId
        {
            get;
            set;
        }

        [DataMember]
        public string SalaryType
        {
            get;
            set;
        }
        [DataMember]
        public int JobTypeLookupId
        {
            get;
            set;
        }

        [DataMember]
        public string JobType
        {
            get;
            set;
        }

        [DataMember]
        public bool SecurityClearance
        {
            get;
            set;
        }

        [DataMember]
        public int WorkAuthorizationLookupId
        {
            get;
            set;
        }

        [DataMember]
        public int WorkScheduleLookupId
        {
            get;
            set;
        }

        [DataMember]
        public string WorkSchedule
        {
            get;
            set;
        }

        [DataMember]
        public string WorkAuthorization
        {
            get;
            set;
        }
        [DataMember]
        public string CreatorName
        {
            get;
            set;
        }

        [DataMember]
        public string LastUpdatorName
        {
            get;
            set;
        }
        [DataMember]
        public string Website
        {
            get;
            set;
        }
        [DataMember]
        public string LinkedInProfile
        {
            get;
            set;
        }
        [DataMember]
        public bool PassportStatus
        {
            get;
            set;
        }
        [DataMember]
        public string IDCard
        {
            get;
            set;
        }
        [DataMember]
        public string Source
        {
            get;
            set;
        }
        [DataMember]
        public string SourceDescription
        {
            get;
            set;
        }


        [DataMember]
        public int CandidateIndustryLookupID
        {
            get;
            set;
        }

        [DataMember]
        public string CandidateIndustry
        {
            get;
            set;
        }

        [DataMember]
        public int JobFunctionLookupID
        {
            get;
            set;
        }

        [DataMember]
        public string JobFunction
        {
            get;
            set;
        }
      
        [DataMember]
        public string NoticePeriod
        {
            get;
            set;
        }


        [DataMember]
        public string Department
        {
            get;
            set;
        }

        [DataMember]
        public string HighestDegree
        {
            get;
            set;
        }
        [DataMember]
        public string JobCode
        {
            get;
            set;
        }

        [DataMember]
        public string FeedbackStatus
        {
            get;
            set;
        }

        [DataMember]
        public string HiringManager
        {
            get;
            set;
        }
        [DataMember]
        public DateTime RejectedDate
        {
            get;
            set;
        }
        //************************END********************************************
       #endregion

        #region Constructor

        public RejectCandidate()
            : base()
        {
        }

        #endregion
    }
}