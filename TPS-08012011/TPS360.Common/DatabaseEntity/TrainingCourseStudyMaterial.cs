﻿using System;
using System.Diagnostics;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "TrainingCourseStudyMaterial", Namespace = "http://www.tps360.com/types")]
    public class TrainingCourseStudyMaterial : BaseEntity
    {
        #region Properties

        [DataMember]
        public int StudyMaterialType
        {
            get;
            set;
        }

        [DataMember]
        public string FileName
        {
            get;
            set;
        }

        [DataMember]
        public string FileType
        {
            get;
            set;
        }

        [DataMember]
        public int TrainingCourseModuleId
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public TrainingCourseStudyMaterial()
            : base()
        {
        }

        #endregion
    }
}