﻿using System;
using System.Diagnostics;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "MemberCapabilityRating", Namespace = "http://www.tps360.com/types")]
    public class MemberCapabilityRating : BaseEntity
    {
        #region Properties

        [DataMember]
        public int MemberId
        {
            get;
            set;
        }

        [DataMember]
        public int PositionId
        {
            get;
            set;
        }

        [DataMember]
        public int CapabilityType
        {
            get;
            set;
        }

        [DataMember]
        public int PositionCapabilityMapId
        {
            get;
            set;
        }

        [DataMember]
        public int Rating
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public MemberCapabilityRating()
            : base()
        {
        }

        #endregion
    }
}