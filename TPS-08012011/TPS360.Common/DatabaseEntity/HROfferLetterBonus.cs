﻿using System;
using System.Diagnostics;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "HROfferLetterBonu", Namespace = "http://www.tps360.com/types")]
    public class HROfferLetterBonus : BaseEntity
    {
        #region Properties

        [DataMember]
        public int BonusLookupId
        {
            get;
            set;
        }

        [DataMember]
        public decimal BonusValue
        {
            get;
            set;
        }

        [DataMember]
        public string BonusValueInWords
        {
            get;
            set;
        }

        [DataMember]
        public int BonusCurrencyLookupId
        {
            get;
            set;
        }

        [DataMember]
        public string BonusNote
        {
            get;
            set;
        }

        [DataMember]
        public int HROfferLetterId
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public HROfferLetterBonus()
            : base()
        {
        }

        #endregion
    }
}