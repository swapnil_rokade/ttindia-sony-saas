﻿using System;
using System.Diagnostics;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "OnBoardingOffBoardingChecklist", Namespace = "http://www.tps360.com/types")]
    public class OnBoardingOffBoardingChecklist : BaseEntity
    {
        #region Properties

        [DataMember]
        public string Name
        {
            get;
            set;
        }

        [DataMember]
        public string Description
        {
            get;
            set;
        }

        [DataMember]
        public int ParentId
        {
            get;
            set;
        }

        [DataMember]
        public int CheckListType
        {
            get;
            set;
        }

        [DataMember]
        public int HRFormId
        {
            get;
            set;
        }

        [DataMember]
        public string HRForm
        {
            get;
            set;
        }

        [DataMember]
        public int WhoWillDo
        {
            get;
            set;
        }

        [DataMember]
        public bool IsForFirstMonth
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public OnBoardingOffBoardingChecklist()
            : base()
        {
        }

        #endregion
    }
}