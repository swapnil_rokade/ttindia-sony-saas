﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "RequsitionApprovalStatus", Namespace = "http://www.tps360.com/types")]
    public class RequsitionApprovalStatus: BaseEntity
    {
        #region Properties
        [DataMember]
        public string Status
        {
            get;
            set;
        }
        [DataMember]
        public int Id
        {
            get;
            set;
        }
        #endregion

        #region Constructor

        public RequsitionApprovalStatus()
            : base()
        {
        }

        #endregion
    }
}
