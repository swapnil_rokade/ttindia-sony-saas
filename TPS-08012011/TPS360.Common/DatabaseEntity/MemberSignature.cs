﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "MemberSignature", Namespace = "http://www.tps360.com/types")]
    public class MemberSignature : BaseEntity
    {
        #region Properties

        [DataMember]
        public string Signature
        {
            get;
            set;
        }

        [DataMember]
        public string Logo
        {
            get;
            set;
        }

        [DataMember]
        public bool Active
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }

        [DataMember]
        public int MemberId
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public MemberSignature()
            : base()
        {
        }

        #endregion
    }
}