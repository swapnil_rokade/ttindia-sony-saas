﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "IPAccessRules", Namespace = "http://www.tps360.com/types")]
    public class IPAccessRules : BaseEntity
    {
        #region Properties

        [DataMember]
        public string MemberId
        {
            get;
            set;
        }

        [DataMember]
        public string UserName
        {
            get;
            set;
        }

        [DataMember]
        public string AllowedIP
        {
            get;
            set;
        }
        [DataMember]
        public string AllowedIPTo
        {
            get;
            set;
        }

        #endregion

        #region Constructor

        public IPAccessRules()
            : base()
        {
        }

        #endregion
    }
}