﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "MemberWorkDetail", Namespace = "http://www.tps360.com/types")]
    public class MemberWorkDetail : BaseEntity
    {
        #region Properties

        [DataMember]
        public string Position
        {
            get;
            set;
        }

        [DataMember]
        public string OfficeAddress1
        {
            get;
            set;
        }

        [DataMember]
        public string OfficeAddress2
        {
            get;
            set;
        }

        [DataMember]
        public string City
        {
            get;
            set;
        }

        [DataMember]
        public string StateId
        {
            get;
            set;
        }

        [DataMember]
        public string Zip
        {
            get;
            set;
        }

        [DataMember]
        public string CountryId
        {
            get;
            set;
        }

        [DataMember]
        public string Email
        {
            get;
            set;
        }

        [DataMember]
        public string Phone
        {
            get;
            set;
        }

        [DataMember]
        public string PhoenExtension
        {
            get;
            set;
        }

        [DataMember]
        public string Fax
        {
            get;
            set;
        }

        [DataMember]
        public string CellPhone
        {
            get;
            set;
        }

        [DataMember]
        public DateTime HireDate
        {
            get;
            set;
        }

        [DataMember]
        public decimal Salary
        {
            get;
            set;
        }

        [DataMember]
        public int SalaryCurrencyLookupId
        {
            get;
            set;
        }

        [DataMember]
        public string Benefits
        {
            get;
            set;
        }

        [DataMember]
        public string AdditionalSalaryNote
        {
            get;
            set;
        }

        [DataMember]
        public string SalesCommissionNote
        {
            get;
            set;
        }

        [DataMember]
        public string WorkingHours
        {
            get;
            set;
        }

        [DataMember]
        public DateTime AvailabilityDate
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }

        [DataMember]
        public int MemberId
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public MemberWorkDetail()
            : base()
        {
        }

        #endregion
    }
}