﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "CommonNote", Namespace = "http://www.tps360.com/types")]
    public class CommonNote : BaseEntity
    {
        #region Properties

        [DataMember]
        public string NoteDetail
        {
            get;
            set;
        }

        [DataMember]
        public bool IsInternal
        {
            get;
            set;
        }

        [DataMember]
        public int Status
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }

        [DataMember]
        public int NoteCategoryLookupId
        {
            get;
            set;
        }

        [DataMember]
        public int JobPostingId
        {
            get;
            set;
        }
        [DataMember]
        public string  JobPostingCode
        {
            get;
            set;
        }
        [DataMember]
        public string  JobTitle
        {
            get;
            set;
        }
        #endregion

        #region Constructor

        public CommonNote()
            : base()
        {
        }

        #endregion
    }
}