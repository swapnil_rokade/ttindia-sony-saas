﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "JobPostingHiringTeam", Namespace = "http://www.tps360.com/types")]
    public class JobPostingHiringTeam : BaseEntity
    {
        #region Properties

        [DataMember]
        public string EmployeeType
        {
            get;
            set;
        }

        [DataMember]
        public int JobPostingId
        {
            get;
            set;
        }

        [DataMember]
        public int MemberId
        {
            get;
            set;
        }

        [DataMember]
        public string Name
        {
            get;
            set;
        }

        [DataMember]
        public string PrimaryEmail
        {
            get;
            set;
        }

        [DataMember]
        public string NamePrimaryEmail
        {
            get;
            set;
        }
        #endregion

        #region Constructor

        public JobPostingHiringTeam()
            : base()
        {
        }

        #endregion
    }
}