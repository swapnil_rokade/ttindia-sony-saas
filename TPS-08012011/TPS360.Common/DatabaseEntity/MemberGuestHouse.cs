﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "MemberGuestHouse", Namespace = "http://www.tps360.com/types")]
    public class MemberGuestHouse : BaseEntity
    {
        #region Properties

        [DataMember]
        public DateTime CheckInDate
        {
            get;
            set;
        }

        [DataMember]
        public DateTime CheckOutDate
        {
            get;
            set;
        }

        [DataMember]
        public decimal PayableAmount
        {
            get;
            set;
        }

        [DataMember]
        public int PayableAmountCurrencyLookupId
        {
            get;
            set;
        }

        [DataMember]
        public string Note
        {
            get;
            set;
        }

        [DataMember]
        public int MemberId
        {
            get;
            set;
        }

        [DataMember]
        public int GuestHouseDetailId
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }

        #endregion

        #region Constructor

        public MemberGuestHouse()
            : base()
        {
        }

        #endregion
    }
}