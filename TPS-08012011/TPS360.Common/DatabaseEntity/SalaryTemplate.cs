﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "SalaryTemplate", Namespace = "http://www.tps360.com/types")]
    public class SalaryTemplate : BaseEntity
    {
        #region Properties

        [DataMember]
        public string NewGrade
        {
            get;
            set;
        }
        [DataMember]
        public string Heads
        {
            get;
            set;
        }
        [DataMember]
        public double Value
        {
            get;
            set;
        }
        [DataMember]
        public string Calculation
        {
            get;
            set;
        }

        #endregion

        #region Constructor

        public SalaryTemplate()
            : base()
        {
        }

        #endregion
    }
}
