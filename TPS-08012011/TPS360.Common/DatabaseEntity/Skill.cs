﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "Skill", Namespace = "http://www.tps360.com/types")]
    public class Skill : BaseEntity
    {
        #region Properties

        [DataMember]
        public string Name
        {
            get;
            set;
        }

        [DataMember]
        public string Description
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }

        [DataMember]
        public int ParentId
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public Skill()
            : base()
        {
        }

        #endregion
    }
}