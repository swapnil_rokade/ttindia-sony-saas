﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "ExternalViewStages", Namespace = "http://www.tps360.com/types")]
    public class ExternalViewStages : BaseEntity
    {
        #region Properties

        [DataMember]
        public string Name
        {
            get;
            set;
        }
         #endregion

        #region Constructor

        public ExternalViewStages()
            : base()
        {
        }

        #endregion
    }
}
