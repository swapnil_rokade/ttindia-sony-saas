﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "CompanyDocument", Namespace = "http://www.tps360.com/types")]
    public class CompanyDocument : BaseEntity
    {
        #region Properties

        [DataMember]
        public string Title
        {
            get;
            set;
        }

        [DataMember]
        public string FileName
        {
            get;
            set;
        }

        [DataMember]
        public string Description
        {
            get;
            set;
        }

        [DataMember]
        public int DocumentType
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }

        [DataMember]
        public int CompanyId
        {
            get;
            set;
        }

        [DataMember]
        public string DocumentTypeName
        {
            get;
            set;
        }

        #endregion

        #region Constructor

        public CompanyDocument()
            : base()
        {
        }

        #endregion
    }
}