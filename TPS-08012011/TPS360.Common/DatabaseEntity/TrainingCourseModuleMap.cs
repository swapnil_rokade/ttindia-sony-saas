﻿using System;
using System.Diagnostics;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "TrainingCourseModuleMap", Namespace = "http://www.tps360.com/types")]
    public class TrainingCourseModuleMap : BaseEntity
    {
        #region Properties

        [DataMember]
        public int TrainingCourseId
        {
            get;
            set;
        }

        [DataMember]
        public int TrainingCourseModuleId
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public TrainingCourseModuleMap()
            : base()
        {
        }

        #endregion
    }
}