﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "MemberSkillMap", Namespace = "http://www.tps360.com/types")]
    public class MemberSkillMap : BaseEntity
    {
        #region Properties

        [DataMember]
        public int ProficiencyLookupId
        {
            get;
            set;
        }

        [DataMember]
        public int YearsOfExperience
        {
            get;
            set;
        }

        [DataMember]
        public DateTime LastUsed
        {
            get;
            set;
        }

        [DataMember]
        public string Comment
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }

        [DataMember]
        public int MemberId
        {
            get;
            set;
        }

        [DataMember]
        public int SkillId
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public MemberSkillMap()
            : base()
        {
        }

        #endregion
    }
}