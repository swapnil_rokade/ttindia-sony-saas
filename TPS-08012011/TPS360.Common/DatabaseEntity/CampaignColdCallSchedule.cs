﻿using System;
using System.Diagnostics;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "CampaignColdCallSchedule", Namespace = "http://www.tps360.com/types")]
    public class CampaignColdCallSchedule : BaseEntity
    {
        #region Properties

        [DataMember]
        public int CampaignCompanyId
        {
            get;
            set;
        }

        [DataMember]
        public int CallBackStatus
        {
            get;
            set;
        }

        [DataMember]
        public DateTime CallBackDate
        {
            get;
            set;
        }

        [DataMember]
        public string CallNotes
        {
            get;
            set;
        }

        [DataMember]
        public DateTime ScheduleCallBack
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemove
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public CampaignColdCallSchedule()
            : base()
        {
        }

        #endregion
    }
}