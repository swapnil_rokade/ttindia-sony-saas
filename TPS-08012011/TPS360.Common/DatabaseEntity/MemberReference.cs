﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "MemberReference", Namespace = "http://www.tps360.com/types")]
    public class MemberReference : BaseEntity
    {
        #region Properties

        [DataMember]
        public string ReferenceName
        {
            get;
            set;
        }

        [DataMember]
        public string ReferencePosition
        {
            get;
            set;
        }

        [DataMember]
        public string CompanyName
        {
            get;
            set;
        }

        [DataMember]
        public string Email
        {
            get;
            set;
        }

        [DataMember]
        public string Phone
        {
            get;
            set;
        }

        [DataMember]
        public string PhoneExtension
        {
            get;
            set;
        }

        [DataMember]
        public string CellPhone
        {
            get;
            set;
        }

        [DataMember]
        public string Remark
        {
            get;
            set;
        }

        [DataMember]
        public int ReferenceCategoryLookupId
        {
            get;
            set;
        }

        [DataMember]
        public int Status
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }

        [DataMember]
        public int MemberId
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public MemberReference()
            : base()
        {
        }

        #endregion
    }
}