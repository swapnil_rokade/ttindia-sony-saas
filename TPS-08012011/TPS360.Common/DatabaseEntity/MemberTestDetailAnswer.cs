﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "MemberTestDetailAnswer", Namespace = "http://www.tps360.com/types")]
    public class MemberTestDetailAnswer : BaseEntity
    {
        #region Properties

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }

        [DataMember]
        public int MemberTestDetailId
        {
            get;
            set;
        }

        [DataMember]
        public int AnswerId
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public MemberTestDetailAnswer()
            : base()
        {
        }

        #endregion
    }
}