﻿using System;
using System.Diagnostics;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "InvoiceDetail", Namespace = "http://www.tps360.com/types")]
    public class InvoiceDetail : BaseEntity
    {
        #region Properties

        [DataMember]
        public string ByProductName
        {
            get;
            set;
        }

        [DataMember]
        public string Description
        {
            get;
            set;
        }

        [DataMember]
        public int Quantity
        {
            get;
            set;
        }

        [DataMember]
        public decimal Rate
        {
            get;
            set;
        }

        [DataMember]
        public int RateCurrencyLookupId
        {
            get;
            set;
        }

        [DataMember]
        public decimal Amount
        {
            get;
            set;
        }

        [DataMember]
        public int ByProductId
        {
            get;
            set;
        }

        [DataMember]
        public int InvoiceId
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public InvoiceDetail()
            : base()
        {
        }

        #endregion
    }
}