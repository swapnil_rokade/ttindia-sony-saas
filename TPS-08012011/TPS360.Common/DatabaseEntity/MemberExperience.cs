﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: MemberExperience.cs
    Description: This page is used to hold properties for MemberExperience.
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.         Date                Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1             Dec-22-2008         Shivanand           Defect #9511; new propertis added [ContactName,CompanyEmail,CompanyPhone,CompanyWebsite].
    0.2             July-7-2009         Gopala Swamy        Defect #10847; Added new peoperty called "StateId"
    ------------------------------------------------------------------------------------------------------------------------------
*/

using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "MemberExperience", Namespace = "http://www.tps360.com/types")]
    public class MemberExperience : BaseEntity
    {
        #region Properties

        [DataMember]
        public string CompanyName
        {
            get;
            set;
        }

        [DataMember]
        public string PositionName
        {
            get;
            set;
        }

        [DataMember]
        public string City
        {
            get;
            set;
        }

        [DataMember]
        public DateTime DateFrom
        {
            get;
            set;
        }

        [DataMember]
        public DateTime DateTo
        {
            get;
            set;
        }

        [DataMember]
        public bool IsTillDate
        {
            get;
            set;
        }

        [DataMember]
        public string Responsibilities
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }

        [DataMember]
        public int CountryLookupId
        {
            get;
            set;
        }

        [DataMember]
        public int IndustryCategoryLookupId
        {
            get;
            set;
        }

        [DataMember]
        public int FunctionalCategoryLookupId
        {
            get;
            set;
        }

        [DataMember]
        public int MemberId
        {
            get;
            set;
        }

 // 0.1 starts
        [DataMember]
        public string ContactName
        {
            get;
            set;
        }

        [DataMember]
        public string CompanyEmail
        {
            get;
            set;
        }

        public string CompanyPhone
        {
            get;
            set;
        }

        public string CompanyWebsite
        {
            get;
            set;
        }
// 0.1 ends

        //0.2 starts here
        public int  StateId
        {
            get;
            set;
        }
        //0.2 end here

        #endregion

        #region Constructor

        public MemberExperience()
            : base()
        {
        }

        #endregion
    }
}