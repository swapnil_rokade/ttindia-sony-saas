﻿using System;
using System.Diagnostics;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "SavedQuery", Namespace = "http://www.tps360.com/types")]
    public class SavedQuery : BaseEntity
    {
        #region Properties

        [DataMember]
        public string QueryCode
        {
            get;
            set;
        }

        [DataMember]
        public string Name
        {
            get;
            set;
        }

        [DataMember]
        public int QueryType
        {
            get;
            set;
        }

        [DataMember]
        public string Query
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public SavedQuery()
            : base()
        {
        }

        #endregion
    }
}