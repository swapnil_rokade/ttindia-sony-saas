using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
	[Serializable]
    [DataContract(Name = "ReferenceCheck", Namespace = "http://www.tps360.com/types")]
	public class ReferenceCheck : BaseEntity
	{
		#region Properties
		
		            [DataMember]
		            public string LinkToRespond
		            {	
			            get;
			            set;
		            }

		            [DataMember]
		            public string ReferenceDocument
		            {	
			            get;
			            set;
		            }

		            [DataMember]
		            public DateTime QueryCreateDate
		            {	
			            get;
			            set;
		            }

		            [DataMember]
		            public DateTime ResponseDate
		            {	
			            get;
			            set;
		            }

		            [DataMember]
		            public int ReferenceCheckStatusLookupId
		            {	
			            get;
			            set;
		            }

		            [DataMember]
		            public int MemberReferenceId
		            {	
			            get;
			            set;
		            }

		
		#endregion

		#region Constructor
		
		public ReferenceCheck():base()
		{
		}
		
		#endregion
	}
}