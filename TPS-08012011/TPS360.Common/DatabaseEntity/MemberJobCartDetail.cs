﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "MemberJobCartDetail", Namespace = "http://www.tps360.com/types")]
    public class MemberJobCartDetail : BaseEntity
    {
        #region Properties

        [DataMember]
        public DateTime AddedDate
        {
            get;
            set;
        }

        [DataMember]
        public int SelectionStepLookupId
        {
            get;
            set;
        }

        [DataMember]
        public int AddedBy
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }

        [DataMember]
        public int MemberJobCartId
        {
            get;
            set;
        }

        #endregion

        #region Constructor

        public MemberJobCartDetail() : base()
        {
        }

        #endregion
    }
}