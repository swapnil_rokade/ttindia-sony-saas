﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "WebParserDomain", Namespace = "http://www.tps360.com/types")]
    public class WebParserDomain : BaseEntity
    {
        #region Properties

        [DataMember]
        public string DomainName
        {
            get;
            set;
        }

        [DataMember]
        public string DomainUrl
        {
            get;
            set;
        }

        [DataMember]
        public string FullName
        {
            get;
            set;
        }

        [DataMember]
        public string CurrentPosition
        {
            get;
            set;
        }

        [DataMember]
        public string Address1
        {
            get;
            set;
        }

        [DataMember]
        public string Email
        {
            get;
            set;
        }

        [DataMember]
        public string OfficePhone
        {
            get;
            set;
        }

        [DataMember]
        public string HomePhone
        {
            get;
            set;
        }

        [DataMember]
        public string CellPhone
        {
            get;
            set;
        }
        [DataMember]
        public string TotalExperienceYears
        {
            get;
            set;
        }

        [DataMember]
        public string CurrentCompany
        {
            get;
            set;
        }

        [DataMember]
        public string Objective
        {
            get;
            set;
        }

        [DataMember]
        public string Summary
        {
            get;
            set;
        }
        [DataMember]
        public string Skill
        {
            get;
            set;
        }

        [DataMember]
        public string Education
        {
            get;
            set;
        }

        [DataMember]
        public string WorkAuthorization
        {
            get;
            set;
        }

        [DataMember]
        public string JobType
        {
            get;
            set;
        }
        [DataMember]
        public string Relocation
        {
            get;
            set;
        }

        [DataMember]
        public string WillingToTravel
        {
            get;
            set;
        }

        [DataMember]
        public string YearlySalaryRate
        {
            get;
            set;
        }

        [DataMember]
        public string HourlySalarRate
        {
            get;
            set;
        }

        [DataMember]
        public string SecurityClearance
        {
            get;
            set;
        }

        [DataMember]
        public string CopyPasteResume
        {
            get;
            set;
        }

        [DataMember]
        public string WholeResume
        {
            get;
            set;
        }

        [DataMember]
        public string WordResume
        {
            get;
            set;
        }

        [DataMember]
        public string DocumentUrl
        {
            get;
            set;
        }

        [DataMember]
        public string ReplaceKeyword
        {
            get;
            set;
        }

        [DataMember]
        public string Address2
        {
            get;
            set;
        }
        [DataMember]
        public string City
        {
            get;
            set;
        }
        [DataMember]
        public string State
        {
            get;
            set;
        }
        [DataMember]
        public string Country
        {
            get;
            set;
        }
        [DataMember]
        public string ZipCode
        {
            get;
            set;
        }
        [DataMember]
        public string DateOfBirth
        {
            get;
            set;
        }
        [DataMember]
        public string Gender
        {
            get;
            set;
        }
        [DataMember]
        public string MaritalStatus
        {
            get;
            set;
        }
        [DataMember]
        public bool IsOmitEmptyLine
        {
            get;
            set;
        }
        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public WebParserDomain()
            : base()
        {
        }

        #endregion
    }
}