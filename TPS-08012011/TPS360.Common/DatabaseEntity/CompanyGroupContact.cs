﻿using System;
using System.Diagnostics;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "CompanyGroupContact", Namespace = "http://www.tps360.com/types")]
    public class CompanyGroupContact : BaseEntity
    {
        #region Properties

        [DataMember]
        public int CompanyGroupId
        {
            get;
            set;
        }

        [DataMember]
        public int CompanyContactId
        {
            get;
            set;
        }

        private CompanyContact _companyContact;

        [DataMember]
        public CompanyContact CompanyContact
        {
            get
            {
                if (_companyContact == null)
                {
                    _companyContact = new CompanyContact();
                }

                return _companyContact;
            }
            set
            {
                _companyContact = value;
            }
        }


        #endregion

        #region Constructor

        public CompanyGroupContact()
            : base()
        {
        }

        #endregion
    }
}