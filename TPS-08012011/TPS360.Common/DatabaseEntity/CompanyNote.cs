﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "CompanyNote", Namespace = "http://www.tps360.com/types")]
    public class CompanyNote : BaseEntity
    {
        #region Properties

        [DataMember]
        public int CompanyId
        {
            get;
            set;
        }

        [DataMember]
        public int CommonNoteId
        {
            get;
            set;
        }

        private CommonNote _commonNote;

        [DataMember]
        public CommonNote CommonNote
        {
            get
            {
                if (_commonNote == null)
                {
                    _commonNote = new CommonNote();
                }

                return _commonNote;
            }
            set
            {
                _commonNote = value;
            }
        }


        #endregion

        #region Constructor

        public CompanyNote()
            : base()
        {
        }

        #endregion
    }
}