﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "Content", Namespace = "http://www.tps360.com/types")]
    public class Content : BaseEntity
    {
        #region Properties

        [DataMember]
        public string PageName
        {
            get;
            set;
        }

        [DataMember]
        public string PageTitle
        {
            get;
            set;
        }

        [DataMember]
        public string Subject
        {
            get;
            set;
        }

        [DataMember]
        public int ContentCategoryId
        {
            get;
            set;
        }

        [DataMember]
        public string ShortDescription
        {
            get;
            set;
        }

        [DataMember]
        public string PageContent
        {
            get;
            set;
        }
        
        [DataMember]
        public int PublishedFolderId
        {
            get;
            set;
        }

        [DataMember]
        public int Status
        {
            get;
            set;
        }

        [DataMember]
        public bool IsHilightItem
        {
            get;
            set;
        }

        [DataMember]
        public DateTime PublishDate
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public Content()
            : base()
        {
        }

        #endregion
    }
}