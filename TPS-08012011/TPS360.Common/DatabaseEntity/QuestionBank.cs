﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName            :   QuestionBank.cs
    Description         :   This page is used to Create Properties for QuestionBank.
    Created By          :   Prasanth
    Created On          :   15/Oct/2015
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
    
 ---------------------------------------------------------------------------------------------------------------------------------------------------------------------        
*/
using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "QuestionBank", Namespace = "http://www.tps360.com/types")]
    public class QuestionBank : BaseEntity
    {
        #region Properties

        public int QuestionBank_Id
        {
            get;
            set;
        }

        public int QuestionBankType_Id
        {
            get;
            set;
        }

        public int AnswerType_Id
        {
            get;
            set;
        }
        //public int InterviewId
        //{
        //    get;
        //    set;
        //}

        public string Question
        {
            get;
            set;
        }


        public string QuestionBankType
        {
            get;
            set;
        }

        public string AnswerType
        {
            get;
            set;
        }

        #endregion

        #region Constructor

        public QuestionBank()
            : base()
        {
        }

        #endregion
    }
}