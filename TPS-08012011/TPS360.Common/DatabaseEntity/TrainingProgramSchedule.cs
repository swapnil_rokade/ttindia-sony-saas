﻿using System;
using System.Diagnostics;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "TrainingProgramSchedule", Namespace = "http://www.tps360.com/types")]
    public class TrainingProgramSchedule : BaseEntity
    {
        #region Properties

        [DataMember]
        public int TypeId
        {
            get;
            set;
        }

        [DataMember]
        public DateTime StartTime
        {
            get;
            set;
        }

        [DataMember]
        public DateTime EndTime
        {
            get;
            set;
        }

        [DataMember]
        public int TrainingProgramId
        {
            get;
            set;
        }

        [DataMember]
        public int TrainingCourseModuleId
        {
            get;
            set;
        }

        [DataMember]
        public int TrainerId
        {
            get;
            set;
        }

        [DataMember]
        public int AlternateTrainerId
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public TrainingProgramSchedule()
            : base()
        {
        }

        #endregion
    }
}