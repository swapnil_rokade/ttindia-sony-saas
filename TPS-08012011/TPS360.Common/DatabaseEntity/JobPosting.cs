﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: JobPosting.cs
    Description: This page is used for Job posting functionality
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1            Nov-13-2008           Yogeesh Bhat           Defect ID:9180; Added new data member IsExpensesPaid
    0.2            Mar-06-2009           Yogeesh Bhat           Defect Id: 10049; Added new data member RequisitionSource and EmailSubject
    0.3            22/May/2015           Prasanth Kumar G       Introduced TotalTimeTaken For Requisition Aging Report
 *  0.4            2/Feb/2015            Pravin khot            Introduced by IList<GenericLookup_Levels>GenericLookupLevels,GenericLookup_Levels

-------------------------------------------------------------------------------------------------------------------------------------------        
*/

using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "JobPosting", Namespace = "http://www.tps360.com/types")]
    public class JobPosting : BaseEntity
    {
        #region Properties

        [DataMember]
        public string JobTitle
        {
            get;
            set;
        }

        [DataMember]
        public string JobPostingCode
        {
            get;
            set;
        }

        [DataMember]
        public string ClientJobId
        {
            get;
            set;
        }

        [DataMember]
        public string Grade
        {
            get;
            set;
        }
        [DataMember]
        public string Location
        {
            get;
            set;
        }
        [DataMember]
        public int NoOfOpenings
        {
            get;
            set;
        }

        [DataMember]
        public string PayRate
        {
            get;
            set;
        }

        [DataMember]
        public int PayRateCurrencyLookupId
        {
            get;
            set;
        }

        [DataMember]
        public string PayCycle
        {
            get;
            set;
        }

        [DataMember]
        public bool TravelRequired
        {
            get;
            set;
        }

        [DataMember]
        public string TravelRequiredPercent
        {
            get;
            set;
        }

        [DataMember]
        public string OtherBenefits
        {
            get;
            set;
        }

        [DataMember]
        public int JobStatus
        {
            get;
            set;
        }

        [DataMember]
        public string  JobDurationLookupId
        {
            get;
            set;
        }

        [DataMember]
        public string JobDurationMonth
        {
            get;
            set;
        }

        [DataMember]
        public string JobAddress1
        {
            get;
            set;
        }

        [DataMember]
        public string JobAddress2
        {
            get;
            set;
        }

        [DataMember]
        public string City
        {
            get;
            set;
        }

        [DataMember]
        public string ZipCode
        {
            get;
            set;
        }

        [DataMember]
        public int CountryId
        {
            get;
            set;
        }

        [DataMember]
        public int StateId
        {
            get;
            set;
        }

        [DataMember]
        public string StartDate
        {
            get;
            set;
        }

        [DataMember]
        public DateTime FinalHiredDate
        {
            get;
            set;
        }

        [DataMember]
        public string JobDescription
        {
            get;
            set;
        }

        [DataMember]
        public string AuthorizationTypeLookupId
        {
            get;
            set;
        }

        [DataMember]
        public DateTime PostedDate
        {
            get;
            set;
        }

        [DataMember]
        public DateTime ActivationDate
        {
            get;
            set;
        }

        [DataMember]
        public string  RequiredDegreeLookupId
        {
            get;
            set;
        }

        [DataMember]
        public string MinExpRequired
        {
            get;
            set;
        }

        [DataMember]
        public string MaxExpRequired
        {
            get;
            set;
        }

       
        [DataMember]
        public bool IsJobActive
        {
            get;
            set;
        }
     
        [DataMember]
        public int JobType
        {
            get;
            set;
        }

        [DataMember]
        public bool IsApprovalRequired
        {
            get;
            set;
        }

        [DataMember]
        public string InternalNote
        {
            get;
            set;
        }

        [DataMember]
        public int ClientId
        {
            get;
            set;
        }
        // Vishal Tripathy Starts
        [DataMember]
        public string ReportingManagerName
        {
            get;
            set;


        }
        // Vishal Tripathy Ends

      

      

        [DataMember]
        public string ClientHourlyRate
        {
            get;
            set;
        }

        [DataMember]
        public int ClientHourlyRateCurrencyLookupId
        {
            get;
            set;
        }

        [DataMember]
        public string ClientRatePayCycle
        {
            get;
            set;
        }


      

        [DataMember]
        public string TaxTermLookupIds
        {
            get;
            set;
        }

     

   

        [DataMember]
        public decimal ExpectedRevenue
        {
            get;
            set;
        }

        [DataMember]
        public int ExpectedRevenueCurrencyLookupId
        {
            get;
            set;
        }

    

        [DataMember]
        public bool WorkflowApproved
        {
            get;
            set;
        }

      

        [DataMember]
        public bool TeleCommunication
        {
            get;
            set;
        }

        [DataMember]
        public bool IsTemplate
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }
        [DataMember]
        public string RawDescription
        {
            get;
            set;
        }
        [DataMember]
        public int JobDepartmentLookUpId
        {
            get;
            set;
        }
        [DataMember]
        public string JobSkillLookUpId
        {
            get;
            set;
        }
        //0.1 starts here
        [DataMember]
        public bool IsExpensesPaid
        {
            get;
            set;
        }

        [DataMember]
        public int OccuptionalSeriesLookupId
        {
            get;
            set;
        }

        [DataMember]
        public int PayGradeLookupId
        {
            get;
            set;
        }
      
        [DataMember]
        public int WorkScheduleLookupId
        {
            get;
            set;
        }


        [DataMember]
        public bool SecurityClearance
        {
            get;
            set;
        }

        [DataMember]
        public int  ClientContactId
        {
            get;
            set;
        }

        [DataMember]
        public int JobCategoryLookupId
        {
            get;
            set;
        }





        [DataMember]
        public System .Collections .Generic .IList <Hiring_Levels> HiringMatrixLevels
        {
            get;
            set;
        }

        //*******Code introduced by Pravin khot on 2/Feb/2015 Start*************
        [DataMember]
        public System.Collections.Generic.IList<GenericLookup_Levels> GenericLookupLevels
        {
            get;
            set;
        }
        //*************************END********************************************


        [DataMember]
        public string ReportingTo
        {
            get;
            set;
        }
        [DataMember]
        public int NoOfReportees
        {
            get;
            set;
        }
        [DataMember]
        public string  MinimumQualifyingParameters
        {
            get;
            set;
        }

        [DataMember]
        public decimal  MaxPayRate
        {
            get;
            set;
        }

        [DataMember]
        public bool AllowRecruitersToChangeStatus
        {
            get;
            set;
        }
        //0.1 ends here

        [DataMember]
        public string ClientBrief
        {
            get;
            set;
        }

        [DataMember]
        public int NoOfAssignedManagers
        {
            get;
            set;
        }

        [DataMember]
        public int TurnArroundTime
        {
            get;
            set;
        }
        [DataMember]
        public int RecruiterTurnArroundTime
        {
            get;
            set;
        }
        [DataMember]
        public int InterViewCount
        {
            get;
            set;
        }
        [DataMember]
        public int JoiningDetailsCount
        {
            get;
            set;
        }
        [DataMember]
        public int OfferDetailsCount
        {
            get;
            set;
        }
        [DataMember]
        public int SubmissionCount
        {
            get;
            set;
        }
        [DataMember]
        public string InterviewCandidateName
        {
            get;
            set;
        }
        [DataMember]
        public string SubmittedCandidateName
        {
            get;
            set;
        }
        [DataMember]
        public string OfferedCandidateName
        {
            get;
            set;
        }
        [DataMember]
        public string JoinedCandidateName
        {
            get;
            set;
        }
        
        [DataMember]
        public string JobPostNotes
        {
            get;
            set;
        }

        [DataMember]
        public int NoOfUnfilledOpenings
        {
            get;
            set;
        }


        [DataMember]
        public string RequisitionSource
        {
            get;
            set;
        }

        [DataMember]
        public string EmailSubject
        {
            get;
            set;
        }
        [DataMember]
        public bool ShowInCandidatePortal
        {
            get;
            set;

        }

        [DataMember]
        public bool ShowInEmployeeReferralPortal
        {
            get;
            set;

        }


        public bool  IsApplied
        {
            get;
            set;
        }
        public bool DisplayRequisitionInVendorPortal
        {
            get;
            set;
        }

        public string CreatorName
        {
            get;
            set;
        }
        [DataMember]
        public DateTime OpenDate
        {
            get;
            set;
        }
        [DataMember]
        public DateTime ResourceStartDate
        {
            get;
            set;
        }
        [DataMember]
        public DateTime ResourceEndDate
        {
            get;
            set;
        }
        [DataMember]
        public DateTime CreatedOn
        {
            get;
            set;
        }
        [DataMember]
        public DateTime SubmittedOn
        {
            get;
            set;
        }
        [DataMember]
        public int SalesRegionLookUpId
        {
            get;
            set;
        }
        [DataMember]
        public int SalesGroupLookUpId
        {
            get;
            set;
        }
        [DataMember]
        public string CustomerName
        {
            get;
            set;
        }
        [DataMember]
        public int POAvailability
        {
            get;
            set;
        }
        public int JobLocationLookUpID
        {
            get;
            set;
        }
        public int EmployementTypeLookUpID
        {
            get;
            set;

        }

        public string JobLocationText
        {
            get;
            set;
        }

        public string SalesRegionText
        {
            get;
            set;
        }

        public string SalesGroupText
        {
            get;
            set;
        }

        public string BUContactText
        {
            get;
            set;
        }

        public string JobCategoryText
        {
            get;
            set;
        }
        public string VendorList
        {
            get;
            set;
        }
        public string RequisitionType
        {
            get;
            set;
        }
        //Code introduced by Prasanth on 22/May/2015 Start
        public int TotalTimeTaken
        {
            get;
            set;
        }
        //****************END*********************


        public string RequestionBranch
        {
            get;
            set;
        }
        public string RequestionGrade
        {
            get;
            set;
        }

        public string onnoticeperiod
        {
            get;
            set;
        }

        public string Filled
        {
            get;
            set;
        }

        public int Available
        {
            get;
            set;
        }
        //Code introduced by Pravin khot on 18/Jan/2016 Start
        public int MemberId
        {
            get;
            set;
        }
        [DataMember]
        public int CareerJobId
        {
            get;
            set;
        }
        //****************END*********************

        ///////////////////////////// Rishi Code Start///////////////////////////////////////
        [DataMember]
        public string CandidateName
        {
            get;
            set;
        }
        [DataMember]
        public string ProjectName
        {
            get;
            set;
        }
        [DataMember]
        public string CustomerInfo
        {
            get;
            set;
        }
        [DataMember]
        public string ReasonForHire
        {
            get;
            set;
        }
        [DataMember]
        public string RelMinExpRequired
        {
            get;
            set;
        }
        [DataMember]
        public string RelMaxExpRequired
        {
            get;
            set;
        }
        [DataMember]
        public string SecondarySkill
        {
            get;
            set;
        }
        [DataMember]
        public int GDCGSC
        {
            get;
            set;
        }
        [DataMember]
        public int IndivContribMgnt
        {
            get;
            set;
        }
        [DataMember]
        public int DemandType
        {
            get;
            set;
        }
        [DataMember]
        public int Priority
        {
            get;
            set;
        }
        [DataMember]
        public int Billability
        {
            get;
            set;
        }
        //[DataMember]
        //public Int64 CostCenter
        //{
        //    get;
        //    set;
        //}
        [DataMember]
        public string CostCenter
        {
            get;
            set;
        }
        [DataMember]
        public int Region
        {
            get;
            set;
        }
        [DataMember]
        public int JobFamily
        {
            get;
            set;
        }
        [DataMember]
        public string DivisionText
        {
            get;
            set;
        }
        [DataMember]
        public string Division
        {
            get;
            set;
        }
        [DataMember]
        public string SuperOrgCode
        {
            get;
            set;
        }
        [DataMember]
        public string ReportingManagerGID
        {
            get;
            set;
        }
        ///////////////////////////// Rishi Code End///////////////////////////////////////
        //************************** Added by Kanchan Yeware **************************
        //public Int64 SupervisoryOrganizationCode
        //{
        //    get;
        //    set;
        //}
        public string SupervisoryOrgShortText
        {
            get;
            set;
        }
        public string SupervisoryOrgDesc
        {
            get;
            set;
        }
        public string JobProfileCode
        {
            get;
            set;
        }
        public string ManagerName
        {
            get;
            set;
        }
        public string OrgUnitHead
        {
            get;
            set;
        }
        [DataMember]
        public string GDCGSCText
        {
            get;
            set;
        }
        [DataMember]
        public string IndContrMangement
        {
            get;
            set;
        }
        [DataMember]
        public string BillabilityText
        {
            get;
            set;
        }
        [DataMember]
        public string RegionText
        {
            get;
            set;
        }
        [DataMember]
        public string CostCenterName
        {
            get;
            set;
        }
        //************************** Added by Kanchan Yeware **************************


        //Changes start by vishal Tripathy

        [DataMember]
        public string membername
        {
            get;
            set;
        }
        //Changes end by Vishal Tripathy


        //*****************************************************
        //Rupesh Kadam Start
        [DataMember]
        public string ApprovalStatus
        {
            get;
            set;
        }
        //[DataMember]
        public string DemandTypeText
        {
            get;
            set;
        }
        [DataMember]
        public string PriorityText
        {
            get;
            set;
        }
        [DataMember]
        public string OrgnizationName
        {
            get;
            set;
        }
        [DataMember]
        public string PostedBy
        {
            get;
            set;
        }
        [DataMember]
        public string SupervisoryOrganizationCode
        {
            get;
            set;
        }
        [DataMember]
        public string JobFamilyName
        {
            get;
            set;
        }
        [DataMember]
        public string CandidateManagerGID
        {
            get;
            set;
        }
        //Rupesh Kadam End
        //*****************************************************
        //pravin khot Start
        [DataMember]
        public int AdminMemberid
        {
            get;
            set;
        }
        //*******************Suraj Adsule******Start********For********20160830*********//

        [DataMember]
        public bool DisplayInIJP
        {
            get;
            set;
        }
        [DataMember]
        public DateTime? IJPExpiryDate
        {
            get;
            set;
        }
        [DataMember]
        public bool DisplayInExternal
        {
            get;
            set;
        }

        [DataMember]
        public bool ApprovedForIJP
        {
            get;
            set;
        }
        //*******************Suraj Adsule******End********For********20160830*********//
        [DataMember]
        public int NoofSubmissions
        {
            get;
            set;
        }
        [DataMember]
        public int WIP
        {
            get;
            set;
        }
        [DataMember]
        public int Rejected
        {
            get;
            set;
        }
        [DataMember]
        public int Joined
        {
            get;
            set;
        }
        [DataMember]
        public string Comment
        {
            get;
            set;
        }
        [DataMember]
        public string Status
        {
            get;
            set;
        }
        [DataMember]
        public string GID
        {
            get;
            set;
        }
        #endregion

        #region Constructor

        public JobPosting()
            : base()
        {
        }

     

        #endregion
    }
    public struct Hiring_Levels
    {
        public int HiringMatrixLevelID;
        public int CandidateCount;

    }
    //*******Code introduced by Pravin khot on 2/Feb/2015 Start*************
    public struct GenericLookup_Levels
    {
        public int GenericLookupID;
        public int GenericLookupCount;

    }
    //*************END***************************
}