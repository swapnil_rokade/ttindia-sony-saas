﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "IndustryCapability", Namespace = "http://www.tps360.com/types")]
    public class IndustryCapability : BaseEntity
    {
        #region Properties

        [DataMember]
        public string CapabilityQuestion
        {
            get;
            set;
        }

        [DataMember]
        public string Description
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }

        [DataMember]
        public int IndustryCategoryId
        {
            get;
            set;
        }

        #endregion

        #region Constructor

        public IndustryCapability()
            : base()
        {
        }

        #endregion
    }
}