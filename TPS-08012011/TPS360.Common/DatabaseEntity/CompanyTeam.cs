﻿using System;
using System.Diagnostics;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "CompanyTeam", Namespace = "http://www.tps360.com/types")]
    public class CompanyTeam : BaseEntity
    {
        #region Properties

        [DataMember]
        public int CompanyId
        {
            get;
            set;
        }

        [DataMember]
        public int MemberId
        {
            get;
            set;
        }

        [DataMember]
        public DateTime AssignDate
        {
            get;
            set;
        }

        public bool IsManager
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public CompanyTeam()
            : base()
        {
        }

        #endregion
    }
}