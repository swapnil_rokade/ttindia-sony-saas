﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "MemberAttendence", Namespace = "http://www.tps360.com/types")]
    public class MemberAttendence : BaseEntity
    {
        #region Properties

        [DataMember]
        public int AttendenceType
        {
            get;
            set;
        }

        [DataMember]
        public DateTime AttendenceDate
        {
            get;
            set;
        }

        [DataMember]
        public string Comment
        {
            get;
            set;
        }

        [DataMember]
        public int MemberId
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public MemberAttendence()
            : base()
        {
        }

        #endregion
    }
}