﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "InterviewInterviewerMap", Namespace = "http://www.tps360.com/types")]
    public class InterviewInterviewerMap : BaseEntity
    {
        #region Properties

        [DataMember]
        public int InterviewId
        {
            get;
            set;
        }

        [DataMember]
        public int JobPostingHiringTeamId
        {
            get;
            set;
        }

        [DataMember]
        public int InterviewerId
        {
            get;
            set;
        }

        [DataMember]
        public bool IsClient
        {
            get;
            set;
        }
        #endregion

        #region Constructor

        public InterviewInterviewerMap()
            : base()
        {
        }

        #endregion
    }
}