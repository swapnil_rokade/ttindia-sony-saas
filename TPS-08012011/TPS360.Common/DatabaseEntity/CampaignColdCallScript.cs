﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "CampaignColdCallScript", Namespace = "http://www.tps360.com/types")]
    public class CampaignColdCallScript : BaseEntity
    {
        #region Properties

        [DataMember]
        public string Subject
        {
            get;
            set;
        }

        [DataMember]
        public string Body
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }

        #endregion

        #region Constructor

        public CampaignColdCallScript()
            : base()
        {
        }

        #endregion
    }
}