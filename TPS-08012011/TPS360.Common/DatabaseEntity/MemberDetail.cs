﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "MemberDetail", Namespace = "http://www.tps360.com/types")]
    public class MemberDetail : BaseEntity
    {
        #region Properties

        [DataMember]
        public bool IsCurrentSameAsPrimaryAddress
        {
            get;
            set;
        }
        
        [DataMember]
        public string CurrentAddressLine1
        {
            get;
            set;
        }

        [DataMember]
        public string CurrentAddressLine2
        {
            get;
            set;
        }

        [DataMember]
        public string CurrentCity
        {
            get;
            set;
        }

        [DataMember]
        public int CurrentStateId
        {
            get;
            set;
        }

        [DataMember]
        public string CurrentZip
        {
            get;
            set;
        }

        [DataMember]
        public int CurrentCountryId
        {
            get;
            set;
        }

        [DataMember]
        public string HomePhone
        {
            get;
            set;
        }

        [DataMember]
        public string OfficePhone
        {
            get;
            set;
        }

        [DataMember]
        public string OfficePhoneExtension
        {
            get;
            set;
        }

        [DataMember]
        public string Fax
        {
            get;
            set;
        }

        [DataMember]
        public string CityOfBirth
        {
            get;
            set;
        }

        [DataMember]
        public string ProvinceOfBirth
        {
            get;
            set;
        }

        [DataMember]
        public string StateIdOfBirth
        {
            get;
            set;
        }

        [DataMember]
        public string CountryIdOfBirth
        {
            get;
            set;
        }

        [DataMember]
        public string CountryIdOfCitizenship
        {
            get;
            set;
        }

        [DataMember]
        public string SSNPAN
        {
            get;
            set;
        }

        [DataMember]
        public string BirthMark
        {
            get;
            set;
        }

        [DataMember]
        public int GenderLookupId
        {
            get;
            set;
        }

        [DataMember]
        public int EthnicGroupLookupId
        {
            get;
            set;
        }

        [DataMember]
        public string Photo
        {
            get;
            set;
        }

        [DataMember]
        public int BloodGroupLookupId
        {
            get;
            set;
        }

        [DataMember]
        public string Height
        {
            get;
            set;
        }

        [DataMember]
        public string Weight
        {
            get;
            set;
        }

        [DataMember]
        public string EmergencyContactPerson
        {
            get;
            set;
        }

        [DataMember]
        public string EmergencyContactNumber
        {
            get;
            set;
        }

        [DataMember]
        public string EmergencyContactRelation
        {
            get;
            set;
        }

        [DataMember]
        public string FatherName
        {
            get;
            set;
        }

        [DataMember]
        public string MotherName
        {
            get;
            set;
        }

        [DataMember]
        public int MaritalStatusLookupId
        {
            get;
            set;
        }

        [DataMember]
        public string SpouseName
        {
            get;
            set;
        }

        [DataMember]
        public DateTime AnniversaryDate
        {
            get;
            set;
        }

        [DataMember]
        public int NumberOfChildren
        {
            get;
            set;
        }

        [DataMember]
        public string DisabilityInformation
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }

        [DataMember]
        public int MemberId
        {
            get;
            set;
        }

        [DataMember]
        public int CandidateIndustryLookupID
        {
            get;
            set;
        }

        [DataMember]
        public int JobFunctionLookupID
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public MemberDetail()
            : base()
        {
        }

        #endregion
    }
}