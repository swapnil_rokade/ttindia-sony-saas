﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "TestMaster", Namespace = "http://www.tps360.com/types")]
    public class TestMaster : BaseEntity
    {
        #region Properties

        [DataMember]
        public string Name
        {
            get;
            set;
        }

        [DataMember]
        public string Description
        {
            get;
            set;
        }

        [DataMember]
        public int Type
        {
            get;
            set;
        }

        [DataMember]
        public int NumberOfDisplayQuestions
        {
            get;
            set;
        }

        [DataMember]
        public string Duration
        {
            get;
            set;
        }

        [DataMember]
        public int TotalScore
        {
            get;
            set;
        }

        [DataMember]
        public int PassingScore
        {
            get;
            set;
        }

        [DataMember]
        public bool NegativeMarking
        {
            get;
            set;
        }

        [DataMember]
        public int NegativeMarks
        {
            get;
            set;
        }

        [DataMember]
        public int PublishStatus
        {
            get;
            set;
        }

        [DataMember]
        public bool IsFromExistingTest
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }

        [DataMember]
        public int PublisherId
        {
            get;
            set;
        }

        [DataMember]
        public DateTime PublishDate
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public TestMaster()
            : base()
        {
        }

        #endregion
    }
}