﻿using System;
using System.Diagnostics;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "PerformanceStandard", Namespace = "http://www.tps360.com/types")]
    public class PerformanceStandard : BaseEntity
    {
        #region Properties

        [DataMember]
        public string Responsibility
        {
            get;
            set;
        }

        [DataMember]
        public int MonitoredType
        {
            get;
            set;
        }

        [DataMember]
        public double JobPercentage
        {
            get;
            set;
        }

        [DataMember]
        public string Standard
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public PerformanceStandard()
            : base()
        {
        }

        #endregion
    }
}