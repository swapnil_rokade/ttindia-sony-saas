﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "MemberAttendenceYearlyReport", Namespace = "http://www.tps360.com/types")]
    public class MemberAttendenceYearlyReport : BaseEntity
    {
        #region Properties
        [DataMember]
        public string Letter
        {
            get;
            set;
        }
        [DataMember]
        public string Meaning
        {
            get;
            set;
        }
        [DataMember]
        public int January
        {
            get;
            set;
        }
        [DataMember]
        public int February
        {
            get;
            set;
        }
        [DataMember]
        public int March
        {
            get;
            set;
        }
        [DataMember]
        public int April
        {
            get;
            set;
        }
        [DataMember]
        public int May
        {
            get;
            set;
        }
        [DataMember]
        public int June
        {
            get;
            set;
        }
        [DataMember]
        public int July
        {
            get;
            set;
        }
        [DataMember]
        public int August
        {
            get;
            set;
        }
        [DataMember]
        public int September
        {
            get;
            set;
        }
        [DataMember]
        public int October
        {
            get;
            set;
        }
        [DataMember]
        public int November
        {
            get;
            set;
        }
        [DataMember]
        public int December
        {
            get;
            set;
        }
        #endregion
         #region Constructor

        public MemberAttendenceYearlyReport()
            : base()
        {
        }

        #endregion
    }
}
