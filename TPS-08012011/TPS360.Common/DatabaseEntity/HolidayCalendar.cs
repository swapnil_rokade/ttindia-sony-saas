﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "HolidayCalendar", Namespace = "http://www.tps360.com/types")]
    public class HolidayCalendar : BaseEntity
    {
        #region Properties

        [DataMember]
        public string Name
        {
            get;
            set;
        }

        [DataMember]
        public string Description
        {
            get;
            set;
        }

        [DataMember]
        public int StartDay
        {
            get;
            set;
        }

        [DataMember]
        public int StartMonth
        {
            get;
            set;
        }

        [DataMember]
        public int StartYear
        {
            get;
            set;
        }

        [DataMember]
        public int EndDay
        {
            get;
            set;
        }

        [DataMember]
        public int EndMonth
        {
            get;
            set;
        }

        [DataMember]
        public int EndYear
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRecursiveHoliday
        {
            get;
            set;
        }

        [DataMember]
        public int HolidayType
        {
            get;
            set;
        }

        [DataMember]
        public int HolidayCategoryLookupId
        {
            get;
            set;
        }

        [DataMember]
        public int Status
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public HolidayCalendar()
            : base()
        {
        }

        #endregion
    }
}
