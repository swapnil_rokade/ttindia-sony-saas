﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "MemberH1B1Document", Namespace = "http://www.tps360.com/types")]
    public class MemberH1B1Document : BaseEntity
    {
        #region Properties

        [DataMember]
        public string Title
        {
            get;
            set;
        }

        [DataMember]
        public string FileName
        {
            get;
            set;
        }

        [DataMember]
        public int FileTypeLookupId
        {
            get;
            set;
        }

        [DataMember]
        public int MemberH1B1Id
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public MemberH1B1Document()
            : base()
        {
        }

        #endregion
    }
}