﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "Product", Namespace = "http://www.tps360.com/types")]
    public class Product : BaseEntity
    {
        #region Properties

        [DataMember]
        public string Name
        {
            get;
            set;
        }

        [DataMember]
        public string Description
        {
            get;
            set;
        }

        [DataMember]
        public int ProductType
        {
            get;
            set;
        }

        [DataMember]
        public decimal UnitPrice
        {
            get;
            set;
        }

        [DataMember]
        public int UnitPriceCurrencyLookupId
        {
            get;
            set;
        }

        #endregion

        #region Constructor

        public Product()
            : base()
        {
        }

        #endregion
    }
}
