﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "JobPostingSearchAgent", Namespace = "http://www.tps360.com/types")]
    public class JobPostingSearchAgent : BaseEntity
    {
        #region Properties
        [DataMember]
        public string CountryName
        {
            get;
            set;
        }
        [DataMember]
        public string StateName
        {
            get;
            set;
        }
        [DataMember]
        public string CurrencyName
        {
            get;
            set;
        }
        [DataMember]
        public string HotlistName
        {
            get;
            set;
        }
        [DataMember]
        public string EducationTypeName
        {
            get;
            set;
        }
        [DataMember]
        public string EmployementTypeName
        {
            get;
            set;
        }
        [DataMember]
        public string CandidateTypeName
        {
            get;
            set;
        }
        [DataMember]
        public string WorkScheduleTypeName
        {
            get;
            set;
        }
        [DataMember]
        public string WorkStatusTypeName
        {
            get;
            set;
        }

        [DataMember]
        public string HiringStatusTypeName
        {
            get;
            set;
        }

        [DataMember]
        public string JobTitle
        {
            get;
            set;
        }

        [DataMember]
        public string KeyWord
        {
            get;
            set;
        }

        [DataMember]
        public string SalaryFrom
        {
            get;
            set;
        }

        [DataMember]
        public string SalaryTo
        {
            get;
            set;
        }

        [DataMember]
        public int CurrencyLookUpId
        {
            get;
            set;
        }

        [DataMember]
        public int JobPostingId
        {
            get;
            set;
        }

        [DataMember]
        public int HotListId
        {
            get;
            set;
        }

        [DataMember]
        public string City
        {
            get;
            set;
        }

        [DataMember]
        public int CountryId
        {
            get;
            set;
        }

        [DataMember]
        public int StateId
        {
            get;
            set;
        }

        [DataMember]
        public string EducationLookupId
        {
            get;
            set;
        }

        [DataMember]
        public DateTime AvailableAfter
        {
            get;
            set;
        }

        [DataMember]
        public string MinExperience
        {
            get;
            set;
        }

        [DataMember]
        public string MaxExperience
        {
            get;
            set;
        }

        [DataMember]
        public string SalaryType
        {
            get;
            set;
        }

        [DataMember]
        public int EmployementTypeLookUpId
        {
            get;
            set;
        }

        [DataMember]
        public string ResumeLastUpdated
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }
      
        [DataMember]
        public int CandidateTypeLookUpId
        {
            get;
            set;
        }
               
        [DataMember]
        public int WorkScheduleLookupId
        {
            get;
            set;
        }

        [DataMember]
        public bool SecurityClearance
        {
            get;
            set;
        }

        [DataMember]
        public string WorkStatusLookUpId
        {
            get;
            set;
        }

        [DataMember]
        public string HiringStatusLookUpId
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public JobPostingSearchAgent()
            : base()
        {
        }

        #endregion
    }
}
