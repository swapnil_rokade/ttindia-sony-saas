﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "JobPostingGeneralQuestion", Namespace = "http://www.tps360.com/types")]
    public class JobPostingGeneralQuestion : BaseEntity
    {
        #region Properties

        [DataMember]
        public string Question
        {
            get;
            set;
        }

        [DataMember]
        public int ScreeningScore
        {
            get;
            set;
        }

        [DataMember]
        public int QuestionOrder
        {
            get;
            set;
        }

        [DataMember]
        public string QuestionType
        {
            get;
            set;
        }

        [DataMember]
        public int JobPostingId
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public JobPostingGeneralQuestion()
            : base()
        {
        }

        #endregion
    }
}