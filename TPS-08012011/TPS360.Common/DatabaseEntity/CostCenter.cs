﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "CostCenter", Namespace = "http://www.tps360.com/types")]
    public class CostCenter : BaseEntity
    {
        #region Properties

        [DataMember]
        public string COST_CENTER_NAME
        {
            get;
            set;
        }
        //[DataMember]
        //public Int64 COST_CENTER_ID
        //{
        //    get;
        //    set;
        //}
        [DataMember]
        public string COST_CENTER_ID
        {
            get;
            set;
        }
        #endregion

        #region Constructor

        public CostCenter()
            : base()
        {
        }

        #endregion
    }
}
