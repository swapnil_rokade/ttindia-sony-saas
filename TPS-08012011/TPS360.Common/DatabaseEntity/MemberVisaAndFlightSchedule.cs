﻿using System;
using System.Diagnostics;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "MemberVisaAndFlightSchedule", Namespace = "http://www.tps360.com/types")]
    public class MemberVisaAndFlightSchedule : BaseEntity
    {
        #region Properties

        [DataMember]
        public string VisaNo
        {
            get;
            set;
        }

        [DataMember]
        public int VisaStatus
        {
            get;
            set;
        }

        [DataMember]
        public string VisaPaidBy
        {
            get;
            set;
        }

        [DataMember]
        public int VisaCountryId
        {
            get;
            set;
        }

        [DataMember]
        public DateTime VisaAppliedDate
        {
            get;
            set;
        }

        [DataMember]
        public DateTime VisaReceivedDate
        {
            get;
            set;
        }

        [DataMember]
        public DateTime VisaEntryPermitExpiryDate
        {
            get;
            set;
        }

        [DataMember]
        public DateTime VisaExpiryDate
        {
            get;
            set;
        }

        [DataMember]
        public string AirTicketNo
        {
            get;
            set;
        }

        [DataMember]
        public int TicketStatus
        {
            get;
            set;
        }

        [DataMember]
        public string TicketPNRNo
        {
            get;
            set;
        }

        [DataMember]
        public int TicketSource
        {
            get;
            set;
        }

        [DataMember]
        public int TicketCountryId
        {
            get;
            set;
        }

        [DataMember]
        public string TicketSector
        {
            get;
            set;
        }

        [DataMember]
        public string TicketAirlines
        {
            get;
            set;
        }

        [DataMember]
        public string TicketPaidBy
        {
            get;
            set;
        }

        [DataMember]
        public bool IsTicketMessageReceived
        {
            get;
            set;
        }

        [DataMember]
        public int MemberId
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public MemberVisaAndFlightSchedule()
            : base()
        {
        }

        #endregion
    }
}