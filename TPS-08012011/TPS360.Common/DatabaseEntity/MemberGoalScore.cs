﻿using System;
using System.Diagnostics;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "MemberGoalScore", Namespace = "http://www.tps360.com/types")]
    public class MemberGoalScore : BaseEntity
    {
        #region Properties

        [DataMember]
        public decimal EmployeeScore
        {
            get;
            set;
        }

        [DataMember]
        public decimal ManagerScore
        {
            get;
            set;
        }

        [DataMember]
        public DateTime CompletedDate
        {
            get;
            set;
        }

        [DataMember]
        public string GoalAccomplishment
        {
            get;
            set;
        }

        [DataMember]
        public string ActionStepsAccomplishment
        {
            get;
            set;
        }

        [DataMember]
        public string MeasureOfSuccessAccomplishment
        {
            get;
            set;
        }

        [DataMember]
        public int MemberId
        {
            get;
            set;
        }

        [DataMember]
        public int MemberGoalId
        {
            get;
            set;
        }

        [DataMember]
        public int Status
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public MemberGoalScore()
            : base()
        {
        }

        #endregion
    }
}