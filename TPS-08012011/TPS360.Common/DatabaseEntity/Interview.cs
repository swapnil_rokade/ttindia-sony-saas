﻿/*-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: Interview.cs
    Description:  
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date           Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1             16/Oct/2015       Prasanth Kumar G     Introduced InterviewQuestionBankType

-------------------------------------------------------------------------------------------------------------------------------------------       
*/
using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "Interview", Namespace = "http://www.tps360.com/types")]
    public class Interview : BaseEntity
    {
        #region Properties

        [DataMember]
        public string Location
        {
            get;
            set;
        }

        [DataMember]
        public string Title
        {
            get;
            set;
        }

        [DataMember]
        public bool AllDayEvent
        {
            get;
            set;
        }
        [DataMember]
        public bool EnableReminder
        {
            get;
            set;
        }
        [DataMember]
        public int TimezoneId //added by pravin khot on 25/May/2016
        {
            get;
            set;
        }
        [DataMember]
        public int ReminderInterval
        {
            get;
            set;
        }
        [DataMember]
        public DateTime StartDateTime
        {
            get;
            set;
        }

        [DataMember]
        public int Duration
        {
            get;
            set;
        }

        [DataMember]
        public int JobPostingId
        {
            get;
            set;
        }

        [DataMember]
        public string Remark
        {
            get;
            set;
        }

        [DataMember]
        public string Feedback
        {
            get;
            set;
        }
        [DataMember]
        public string Division  //added by pravin 24/Nov/2016
        {
            get;
            set;
        }

        [DataMember]
        public int Status
        {
            get;
            set;
        }

        [DataMember]
        public int MemberId
        {
            get;
            set;
        }

        [DataMember]
        public int MemberJobCartId
        {
            get;
            set;
        }

        [DataMember]
        public int  ClientId
        {
            get;
            set;
        }

        [DataMember]
        public int TypeLookupId
        {
            get;
            set;
        }

        //Code introduced by Prasanth on 16/Oct/2015 Start
        [DataMember]
        public int QuestionBankTypeLookupId
        {
            get;
            set;
        }
        //***************END********************


        [DataMember]
        public int InterviewLevel
        {
            get;
            set;
        }

        [DataMember]
        public int ActivityId
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }

        [DataMember]
        public string OtherInterviewers
        {
            get;
            set;
        }
        [DataMember]
        public string RequiredDegreeLookupId
        {
            get;
            set;
        }

        [DataMember]
        public int InterviewDocumentId
        {
            get;
            set;
        }

        //******Code added by pravin khot on 4/May/2016************
        [DataMember]
        public int IsCancel
        {
            get;
            set;
        }
        [DataMember]
        public string IcsFileUIDCode
        {
            get;
            set;
        }
        //********************END***************************


        //******Code added by Sumit Sonawane on 1/Mar/2017 ************
        [DataMember]
        public int CandidateHiringMatrixlevelid
        {
            get;
            set;
        }
        //********************END***************************       
        #endregion

        #region Constructor

        public Interview()
            : base()
        {
        }

        #endregion
    }
}