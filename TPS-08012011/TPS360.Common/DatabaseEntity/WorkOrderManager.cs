﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "WorkOrderManager", Namespace = "http://www.tps360.com/types")]
    public class WorkOrderManager : BaseEntity
    {
        #region Properties

        [DataMember]
        public int MemberId
        {
            get;
            set;
        }

        [DataMember]
        public int SupplierWorkOrderId
        {
            get;
            set;
        }

        [DataMember]
        public decimal CommissionRate
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public WorkOrderManager()
            : base()
        {
        }

        #endregion
    }
}