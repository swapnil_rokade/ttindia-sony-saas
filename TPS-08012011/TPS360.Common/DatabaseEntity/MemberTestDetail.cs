using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "MemberTestDetail", Namespace = "http://www.tps360.com/types")]
    public class MemberTestDetail : BaseEntity
    {
        #region Properties

        [DataMember]
        public DateTime TestTakenDate
        {
            get;
            set;
        }

        [DataMember]
        public int ExamTakeNumber
        {
            get;
            set;
        }

        [DataMember]
        public int MemberId
        {
            get;
            set;
        }

        [DataMember]
        public int TestMasterId
        {
            get;
            set;
        }

        [DataMember]
        public int QuestionId
        {
            get;
            set;
        }        

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public MemberTestDetail()
            : base()
        {
        }

        #endregion
    }
}