﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName            :   InterviewPanelMaster.cs
    Description         :   This page is used to Create Properties for InterviewPanelMaster.
    Created By          :   Pravin
    Created On          :   27/nov/2015
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
    
 ---------------------------------------------------------------------------------------------------------------------------------------------------------------------        
*/
using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "InterviewPanelMaster", Namespace = "http://www.tps360.com/types")]
    public class InterviewPanelMaster : BaseEntity
    {
        #region Properties

        public int InterviewPanel_Id
        {
            get;
            set;
        }

        public string InterviewPanel_Name
        {
            get;
            set;
        }
        public string InterviewSkill
        {
            get;
            set;
        }
          
        #endregion

        #region Constructor

        public InterviewPanelMaster()
            : base()
        {
        }

        #endregion
    }
}