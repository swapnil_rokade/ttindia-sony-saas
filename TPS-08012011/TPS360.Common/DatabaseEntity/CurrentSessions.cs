﻿using System;
using System.Diagnostics;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "CurrentSessions", Namespace = "http://www.tps360.com/types")]
    public class CurrentSessions : BaseEntity
    {
        #region Properties

        [DataMember]
        public string EmployeeName
        {
            get;
            set;
        }

        [DataMember]
        public string ApplicationName
        {
            get;
            set;
        }

        [DataMember]
        public DateTime  LoggedIn
        {
            get;
            set;
        }

       

        #endregion

        #region Constructor

        public CurrentSessions()
            : base()
        {
        }

        #endregion
    }
}