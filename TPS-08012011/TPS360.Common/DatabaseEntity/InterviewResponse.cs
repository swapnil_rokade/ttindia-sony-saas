﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName            :   QuestionBank.cs
    Description         :   This page is used to Create Properties for InterviewResponse.
    Created By          :   
    Created On          :   
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1                 27/Dec/2015         Prasanth Kumar G    Introduced Property RowNo
 ---------------------------------------------------------------------------------------------------------------------------------------------------------------------        
*/
using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "InterviewResponse", Namespace = "http://www.tps360.com/types")]
    public class InterviewResponse : BaseEntity
    {
        #region Properties

        //Property introduced by Prasanth on 27/Dec/2015
        public Int64 RowNo 
        {
            get;
            set;
        }

        public int InterviewId
        {
            get;
            set;
        }
        public int InterviewResponse_Id
        {
            get;
            set;
        }

        public int InterviewQuestionBank_Id
        {
            get;
            set;
        }
        public int AnswerType_ID
        {
            get;
            set;
        }
        public string AnswerType
        {
            get;
            set;
        }
        public string Question
        {
            get;
            set;
        }
        public int QuestionBank_id
        {
            get;
            set;
        }


        public string QuestionBankType
        {
            get;
            set;
        }

        public string Response
        {
            get;
            set;
        }


        public string Rationale
        {
            get;
            set;
        }

        public string InterviewerEmail
        {
            get;
            set;
        }

        #endregion

        #region Constructor

        public InterviewResponse()
            : base()
        {
        }

        #endregion
    }
}