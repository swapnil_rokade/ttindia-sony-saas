﻿using System;
using System.Diagnostics;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "ClientWorkOrderDetail", Namespace = "http://www.tps360.com/types")]
    public class ClientWorkOrderDetail : BaseEntity
    {
        #region Properties

        [DataMember]
        public int ConsultantId
        {
            get;
            set;
        }

        [DataMember]
        public decimal ClientHourlyRate
        {
            get;
            set;
        }

        [DataMember]
        public decimal ClientDailyRate
        {
            get;
            set;
        }

        [DataMember]
        public decimal ClientOverTimeRate
        {
            get;
            set;
        }

        [DataMember]
        public int HourlyRateCurrencyLookupId
        {
            get;
            set;
        }

        [DataMember]
        public int DailyRateCurrencyLookupId
        {
            get;
            set;
        }

        [DataMember]
        public int ClientWorkOrderId
        {
            get;
            set;
        }

        [DataMember]
        public bool IsSupplierWorkOrderCreated
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public ClientWorkOrderDetail()
            : base()
        {
        }

        #endregion
    }
}