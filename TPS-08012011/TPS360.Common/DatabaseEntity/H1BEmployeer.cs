﻿using System;
using System.Diagnostics;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "H1BEmployeer", Namespace = "http://www.tps360.com/types")]
    public class H1BEmployeer : BaseEntity
    {
        #region Properties

        [DataMember]
        public string CompanyName
        {
            get;
            set;
        }

        [DataMember]
        public string CompanyCO
        {
            get;
            set;
        }

        [DataMember]
        public string CompanyPhoneNumber
        {
            get;
            set;
        }

        [DataMember]
        public string CompanyPhoneExtension
        {
            get;
            set;
        }

        [DataMember]
        public string CompanyFaxNumber
        {
            get;
            set;
        }

        [DataMember]
        public string CompanyWebUrl
        {
            get;
            set;
        }

        [DataMember]
        public string CompanyAddress
        {
            get;
            set;
        }

        [DataMember]
        public string CompanySuite
        {
            get;
            set;
        }

        [DataMember]
        public string CompanyCity
        {
            get;
            set;
        }

        [DataMember]
        public int CompanyStateId
        {
            get;
            set;
        }

        [DataMember]
        public int CompanyCountryId
        {
            get;
            set;
        }

        [DataMember]
        public string CompanyZipCode
        {
            get;
            set;
        }

        [DataMember]
        public int CompanyIndustryTypeLookupId
        {
            get;
            set;
        }

        [DataMember]
        public int PetitionType
        {
            get;
            set;
        }

        [DataMember]
        public string CompanyEstablishmentYear
        {
            get;
            set;
        }

        [DataMember]
        public int NumberOfEmployee
        {
            get;
            set;
        }

        [DataMember]
        public string AboutCompany
        {
            get;
            set;
        }

        [DataMember]
        public string CompanyEIN
        {
            get;
            set;
        }

        [DataMember]
        public string CompanySSN
        {
            get;
            set;
        }

        [DataMember]
        public string CompanyTIN
        {
            get;
            set;
        }

        [DataMember]
        public string CompanyIRSTax
        {
            get;
            set;
        }

        [DataMember]
        public string FederalEmployerIdentificationNumber
        {
            get;
            set;
        }

        [DataMember]
        public decimal NetAnnualIncome
        {
            get;
            set;
        }

        [DataMember]
        public decimal GrossAnnualIncome
        {
            get;
            set;
        }

        [DataMember]
        public string AuthorizedSignatoryName
        {
            get;
            set;
        }

        [DataMember]
        public string AuthorizedSignatoryTitle
        {
            get;
            set;
        }

        [DataMember]
        public string AuthorizedSignatoryEmail
        {
            get;
            set;
        }

        [DataMember]
        public string AuthorizedSignatoryPhoneNumber
        {
            get;
            set;
        }

        [DataMember]
        public string AuthorizedSignatoryPhoneExtension
        {
            get;
            set;
        }

        [DataMember]
        public int AttorneyId
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public H1BEmployeer()
            : base()
        {
        }

        #endregion
    }
}