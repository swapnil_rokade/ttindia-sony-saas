﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "EmployeeReferral", Namespace = "http://www.tps360.com/types")]
    public class EmployeeReferral : BaseEntity
    {
        #region Properties

        [DataMember]
        public string RefererEmail
        {
            get;
            set;
        }
        [DataMember]
        public string RefererFirstName
        {
            get;
            set;
        }
        [DataMember]
        public string RefererLastName
        {
            get;
            set;
        }
        [DataMember]
        public string EmployeeId
        {
            get;
            set;
        }
        [DataMember]
        public int MemberId
        {
            get;
            set;
        }
        [DataMember]
        public string JobpostingId
        {
            get;
            set;
        }

        [DataMember]
        public string CandidateName
        {
            get;
            set;
        }

        [DataMember]
        public string JobTitle
        {
            get;
            set;
        }

        //*******************Suraj Adsule******Start********For********20160920*********//
        [DataMember]
        public string BUName
        {
            get;
            set;
        }

        [DataMember]
        public string Location
        {
            get;
            set;
        }
        [DataMember]
        public string CandiateStatus
        {
            get;
            set;
        }

        [DataMember]
        public string RequisitionID
        {
            get;
            set;
        }

        [DataMember]
        public int RequisitionStatus
        {
            get;
            set;
        }

        [DataMember]
        public string ReqPrimarySkills
        {
            get;
            set;
        }

        [DataMember]
        public string Gid
        {
            get;
            set;
        }

        [DataMember]
        public string Grade
        {
            get;
            set;
        }
        [DataMember]
        public DateTime DOJ
        {
            get;
            set;
        }
        [DataMember]
        public string Unit
        {
            get;
            set;
        }
        [DataMember]
        public string Designation
        {
            get;
            set;
        }
        [DataMember]
        public string ManagerName
        {
            get;
            set;
        }
        [DataMember]
        public string ManagerId
        {
            get;
            set;
        }

        [DataMember]
        public string OrgUnit
        {
            get;
            set;
        }
        [DataMember]
        public string OrgUnitLogText
        {
            get;
            set;
        }

        [DataMember]
        public string OrgUnitHeadGid
        {
            get;
            set;
        }

        [DataMember]
        public string Reason
        {
            get;
            set;
        }
        [DataMember]
        public DateTime DOL
        {
            get;
            set;
        }

        [DataMember]
        public string DivisionName
        {
            get;
            set;
        }

        [DataMember]
        public string DivisionHeadGid
        {
            get;
            set;
        }

        [DataMember]
        public string DivisionHeadEmailId
        {
            get;
            set;
        }

        [DataMember]
        public string EmployeeType
        {
            get;
            set;
        }
        //----- Added by Kanchan Yeware
        [DataMember]
        public string FirstName
        {
            get;
            set;
        }
        //*********************Suraj Adsule******End********For ********20160920********//     

        #endregion

        #region Constructor

        public EmployeeReferral()
            : base()
        {
        }

        #endregion
    }
}