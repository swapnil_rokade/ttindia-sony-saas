﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "MemberOnBoardingOffBoardingChecklist", Namespace = "http://www.tps360.com/types")]
    public class MemberOnBoardingOffBoardingChecklist : BaseEntity
    {
        #region Properties

        [DataMember]
        public int MemberId
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public MemberOnBoardingOffBoardingChecklist()
            : base()
        {
        }

        #endregion
    }
}
