﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "JobPostingSkillSet", Namespace = "http://www.tps360.com/types")]
    public class JobPostingSkillSet : BaseEntity
    {
        #region Properties

        [DataMember]
        public int JobPostingId
        {
            get;
            set;
        }

        [DataMember]
        public int SkillId
        {
            get;
            set;
        }

        [DataMember]
        public int ProficiencyLookupId
        {
            get;
            set;
        }

        [DataMember]
        public int NoOfYearsExp
        {
            get;
            set;
        }

        [DataMember]
        public bool AssessmentRequired
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public JobPostingSkillSet()
            : base()
        {
        }

        #endregion
    }
}