﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "OfferGeneration", Namespace = "http://www.tps360.com/types")]
    public class OfferGeneration : BaseEntity
    {
        #region Properties

        [DataMember]
        public string DemandType
        {
            get;
            set;
        }
        [DataMember]
        public int JobPostingId
        {
            get;
            set;
        }

        [DataMember]
        public int MemberHiringId
        {
            get;
            set;
        }
        [DataMember]
        public string Grade
        {
            get;
            set;
        }
        [DataMember]
        public string Heads
        {
            get;
            set;
        }
        [DataMember]
        public double Value
        {
            get;
            set;
        }

        
        //**************Suraj Adsule*************20160824****************For Permanent PDF*************//

        [DataMember]
        public string FirstName
        {
            get;
            set;
        }

        [DataMember]
        public string MiddleName
        {
            get;
            set;
        }

        [DataMember]
        public string LastName
        {
            get;
            set;
        }

        [DataMember]
        public string PermanentAddressLine1
        {
            get;
            set;
        }

        [DataMember]
        public string PermanentAddressLine2
        {
            get;
            set;
        }

        [DataMember]
        public string PermanentCity
        {
            get;
            set;
        }

        [DataMember]
        public string PermanentState
        {
            get;
            set;
        }

        [DataMember]
        public string PermanentZip
        {
            get;
            set;
        }
        [DataMember]
        public decimal Basic
        {
            get;
            set;
        }
        [DataMember]
        public decimal OfferedSalary
        {
            get;
            set;
        }
        [DataMember]
        public string FilePath
        {
            get;
            set;
        }

        [DataMember]
        public decimal Conveyance
        {
            get;
            set;
        }

        [DataMember]
        public decimal CompensationPlan
        {
            get;
            set;
        }

        [DataMember]
        public decimal HouseRent
        {
            get;
            set;
        }

        [DataMember]
        public decimal ProvidentFund
        {
            get;
            set;
        }

        [DataMember]
        public decimal SpecialAllowance
        {
            get;
            set;
        }
        [DataMember]
        public decimal VariablePay
        {
            get;
            set;
        }

        [DataMember]
        public string CurrentPosition
        {
            get;
            set;
        }

        [DataMember]
        public DateTime DateOfJoining
        {
            get;
            set;
        }
        [DataMember]
        public DateTime DateOfOffered
        {
            get;
            set;
        }

        //**************Suraj Adsule*************20160824****************For Permanent PDF*************//
        #endregion
   

        #region Constructor

        public OfferGeneration()
            : base()
        {
        }

        #endregion
    }
}
