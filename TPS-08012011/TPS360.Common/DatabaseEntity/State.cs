﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "State", Namespace = "http://www.tps360.com/types")]
    public class State : BaseEntity
    {
        #region Properties

        [DataMember]
        public string Name
        {
            get;
            set;
        }

        [DataMember]
        public string StateCode
        {
            get;
            set;
        }

        [DataMember]
        public int CountryId
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public State()
            : base()
        {
        }

        #endregion
    }
}