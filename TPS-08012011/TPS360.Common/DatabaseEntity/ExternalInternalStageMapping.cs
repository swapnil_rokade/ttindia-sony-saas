﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "ExternalInternalStageMapping", Namespace = "http://www.tps360.com/types")]
    public class ExternalInternalStageMapping : BaseEntity
    {
        #region Properties

        [DataMember]
        public int ExternalViewStageID
        {
            get;
            set;
        }
        [DataMember]
        public int HiringMatrixLevelsID
        {
            get;
            set;
        }
        [DataMember]
        public string HiringMatrixLevel
        {
            get;
            set;
        }
         #endregion

        #region Constructor

        public ExternalInternalStageMapping()
            : base()
        {
        }

        #endregion
    }
}
