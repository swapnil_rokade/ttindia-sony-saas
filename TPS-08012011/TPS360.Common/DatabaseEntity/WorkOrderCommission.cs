﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "WorkOrderCommission", Namespace = "http://www.tps360.com/types")]
    public class WorkOrderCommission : BaseEntity
    {
        #region Properties

        [DataMember]
        public decimal CommissionRate
        {
            get;
            set;
        }

        [DataMember]
        public decimal TotalCommission
        {
            get;
            set;
        }

        [DataMember]
        public int WorkOrderManagerId
        {
            get;
            set;
        }

        [DataMember]
        public int WorkOrderProfitAndLossDetailId
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public WorkOrderCommission()
            : base()
        {
        }

        #endregion
    }
}