﻿using System;
using System.Diagnostics;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "HROfferLetterBenefit", Namespace = "http://www.tps360.com/types")]
    public class HROfferLetterBenefit : BaseEntity
    {
        #region Properties

        [DataMember]
        public int BenefitLookupId
        {
            get;
            set;
        }

        [DataMember]
        public string BenefitValue
        {
            get;
            set;
        }

        [DataMember]
        public int HROfferLetterId
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public HROfferLetterBenefit()
            : base()
        {
        }

        #endregion
    }
}