﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "MemberGroupMap", Namespace = "http://www.tps360.com/types")]
    public class MemberGroupMap : BaseEntity
    {
        #region Properties

        [DataMember]
        public int MemberId
        {
            get;
            set;
        }

        [DataMember]
        public int MemberGroupId
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }

        [DataMember]
        public string GroupName
        {
            get;
            set;
        }

        #endregion

        #region Constructor

        public MemberGroupMap()
            : base()
        {
        }

        #endregion
    }
}