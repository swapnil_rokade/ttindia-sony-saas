﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "MemberOfferRejection", Namespace = "http://www.tps360.com/types")]
    public class MemberOfferRejection : BaseEntity
    {
        #region Properties

      
        [DataMember]
        public string ApplicantName
        {
            get;
            set;
        }
        [DataMember]
        public int MemberId
        {
            get;
            set;
        }

        [DataMember]
        public int JobPostingId
        {
            get;
            set;
        }
        [DataMember]
        public string  JobTitle
        {
            get;
            set;
        }
     
        [DataMember]
        public DateTime  RejectedDate
        {
            get;
            set;
        }
        [DataMember]
        public int ReasonForRejectionLookUpID
        {
            get;
            set;
        }
       
        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }
	
      #endregion

        #region Constructor

        public MemberOfferRejection()
            : base()
        {
        }

        #endregion
    }
}