﻿/*-------------------------------------------------------------------------------------------------------------   FileName: MemberJobCart.cs
   Description: 
   Created By: 
   Created On:
   Modification Log:
   ------------------------------------------------------------------------------------------------------------   Ver.No.             Date                  Author              Modification
   ------------------------------------------------------------------------------------------------------------    0.1             26-Feb-2009             Sandeesh            Defect Fix - ID: 9671; Added new properties to this class.
*/
using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "MemberJobCart", Namespace = "http://www.tps360.com/types")]
    public class MemberJobCart : BaseEntity
    {
        #region Properties

        [DataMember]
        public DateTime ApplyDate
        {
            get;
            set;
        }
        [DataMember]
        public int Status
        {
            get;
            set;
        }
        [DataMember]
        public bool IsPrivate
        {
            get;
            set;
        }

        [DataMember]
        public bool IsInternal
        {
            get;
            set;
        }

        [DataMember]
        public int MemberId
        {
            get;
            set;
        }

        

        [DataMember]
        public int JobPostingId
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }
        //0.1 start
        [DataMember]
        public string CompanyName
        {
            get;
            set;
        } 
        [DataMember]
        public string JobTitle
        {
            get;
            set;
        } 
        [DataMember]
        public string JobPostingCode
        {
            get;
            set;
        }
        [DataMember]
        public string ClientJobId
        {
            get;
            set;
        }
        [DataMember]
        public DateTime JobPostingUpdateDate
        {
            get;
            set;
        }
        [DataMember]
        public int CurrentLevel
        {
            get;
            set;
        }
        [DataMember]
        public string CurrentLevelName
        {
            get;
            set;
        }
        [DataMember]
        public string PublisherName
        {
            get;
            set;
        }
         [DataMember]
        public string FirstInterviewTotal
        {
            get;
            set;
        }
           [DataMember]
         public string SecondInterviewTotal
        {
            get;
            set;
        }

        
        [DataMember]
           public string ThirdInterviewTotal
        {
            get;
            set;
        }

        [DataMember]
        public int SourceId
        {
            get;
            set;
        }
        //Candidate Status in Employee Referral Portal.
		[DataMember]
        public string MemberName
        {
            get;
            set;
        }
        //0.1 End

        #endregion

        #region Constructor

        public MemberJobCart() : base()
        {
        }

        #endregion
    }
}