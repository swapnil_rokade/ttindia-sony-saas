﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "OrganizationUnit", Namespace = "http://www.tps360.com/types")]
    public class OrganizationUnit : BaseEntity
    {
        #region Properties

        [DataMember]
        public string OrgName
        {
            get;
            set;
        }
        [DataMember]
        public string OrgId
        {
            get;
            set;
        }
        [DataMember]
        public Int32 Id
        {
            get;
            set;
        }
        [DataMember]
        public Int32 MemberId
        {
            get;
            set;
        }
        [DataMember]
        public string OrgUnitCode
        {
            get;
            set;
        }
        [DataMember]
        public string ReportingOrgStTxt
        {
            get;
            set;
        }
        [DataMember]
        public string ReportingOrgLngTxt
        {
            get;
            set;
        }
        [DataMember]
        public string HeadName
        {
            get;
            set;
        }
        [DataMember]
        public string HeadGID
        {
            get;
            set;
        }
        [DataMember]
        public string ORG_CODE
        {
            get;
            set;
        }
        [DataMember]
        public string SHORT_TEXT
        {
            get;
            set;
        }
        [DataMember]
        public string LONG_TEXT
        {
            get;
            set;
        }
        [DataMember]
        public string Department_Head_Approval
        {
            get;
            set;
        }
        [DataMember]
        public string Department_Head_Approval_GID
        {
            get;
            set;
        }
        [DataMember]
        public string Department_Head_Approval_Email_ID
        {
            get;
            set;
        }
        [DataMember]
        public string Tower_Head_Approval
        {
            get;
            set;
        }
        [DataMember]
        public string Tower_Head_GID
        {
            get;
            set;
        }
        [DataMember]
        public string Tower_Head_Email_ID
        {
            get;
            set;
        }
        [DataMember]
        public string Division_Head_Approval
        {
            get;
            set;
        }
        [DataMember]
        public string Division_Head_GID
        {
            get;
            set;
        }
        [DataMember]
        public string Division_Head_Email_ID
        {
            get;
            set;
        }
        #endregion

        #region Constructor

        public OrganizationUnit()
            : base()
        {
        }

        #endregion
    }
}
