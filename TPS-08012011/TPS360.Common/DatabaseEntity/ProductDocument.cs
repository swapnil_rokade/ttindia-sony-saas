﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "ProductDocument", Namespace = "http://www.tps360.com/types")]
    public class ProductDocument : BaseEntity
    {
        #region Properties

        [DataMember]
        public int ProductId
        {
            get;
            set;
        }

        [DataMember]
        public string Title
        {
            get;
            set;
        }

        [DataMember]
        public string FileName
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public ProductDocument()
            : base()
        {
        }

        #endregion
    }
}
