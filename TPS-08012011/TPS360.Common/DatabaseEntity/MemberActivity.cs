﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "MemberActivity", Namespace = "http://www.tps360.com/types")]
    public class MemberActivity : BaseEntity
    {
        #region Properties

        [DataMember]
        public int ActivityOn
        {
            get;
            set;
        }

        [DataMember]
        public int ActivityOnObjectId
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }

        [DataMember]
        public int MemberActivityTypeId
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public MemberActivity()
            : base()
        {
        }

        #endregion
    }
}