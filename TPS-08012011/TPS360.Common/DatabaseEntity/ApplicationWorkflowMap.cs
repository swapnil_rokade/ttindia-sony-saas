﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "ApplicationWorkflowMap", Namespace = "http://www.tps360.com/types")]
    public class ApplicationWorkflowMap : BaseEntity
    {
        #region Properties

        [DataMember]
        public int ApplicationWorkflowId
        {
            get;
            set;
        }

        [DataMember]
        public int MemberId
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public ApplicationWorkflowMap()
            : base()
        {
        }

        #endregion
    }
}