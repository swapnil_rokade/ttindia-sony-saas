﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "GuestHouseDocument", Namespace = "http://www.tps360.com/types")]
    public class GuestHouseDocument : BaseEntity
    {
        #region Properties

        [DataMember]
        public string Title
        {
            get;
            set;
        }

        [DataMember]
        public string Description
        {
            get;
            set;
        }

        [DataMember]
        public string FileName
        {
            get;
            set;
        }

        [DataMember]
        public int DocumentType
        {
            get;
            set;
        }

        [DataMember]
        public int GuestHouseId
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public GuestHouseDocument()
            : base()
        {
        }

        #endregion
    }
}
