﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "InterviewAlert", Namespace = "http://www.tps360.com/types")]
    public class InterviewAlert : BaseEntity
    {
        #region Properties

        [DataMember]
        public int Type
        {
            get;
            set;
        }

        [DataMember]
        public int InterviewId
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public InterviewAlert()
            : base()
        {
        }

        #endregion
    }
}