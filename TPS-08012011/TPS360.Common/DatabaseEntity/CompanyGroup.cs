﻿using System;
using System.Diagnostics;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "CompanyGroup", Namespace = "http://www.tps360.com/types")]
    public class CompanyGroup : BaseEntity
    {
        #region Properties

        [DataMember]
        public string GroupName
        {
            get;
            set;
        }

        [DataMember]
        public string Description
        {
            get;
            set;
        }

        [DataMember]
        public int GroupType
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }

        [DataMember]
        public int NumberOfContact
        {
            get;
            set;
        }

        [DataMember]
        public bool IsVolumeHireEnabled
        {
            get;
            set;
        }

        private CompanyGroupTeam _manager;

        [DataMember]
        public CompanyGroupTeam Manager
        {
            get
            {
                if (_manager == null)
                {
                    _manager = new CompanyGroupTeam();
                }

                return _manager;
            }
            set
            {
                _manager = value;
            }
        }


        #endregion

        #region Constructor

        public CompanyGroup()
            : base()
        {
        }

        #endregion
    }
}