﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "MemberJoiningDetail", Namespace = "http://www.tps360.com/types")]
    public class MemberJoiningDetail:BaseEntity 
    {
          #region Properties

        [DataMember]
        public int MemberId
        {
            get;
            set;
        }

        [DataMember]
        public int JobPostingId
        {
            get;
            set;
        }
        [DataMember]
        public string  BillableSalary
        {
            get;
            set;
        }
        [DataMember]
        public string    OfferedSalary
        {
            get;
            set;
        }

        [DataMember]
        public int OfferedSalaryPayCycle
        {
            get;
            set;
        }

        [DataMember]
        public int OfferedSalaryCurrency
        {
            get;
            set;
        }

        [DataMember]
        public string  BillingRate
        {
            get;
            set;
        }
       
       
        [DataMember]
        public DateTime  JoiningDate
        {
            get;
            set;
        }



       
        [DataMember]
        public string Revenue
        {
            get;
            set;
        }

        [DataMember]
        public int BillableSalaryCurrency
        {
            get;
            set;
        }

        [DataMember]
        public int RevenueCurrency
        {
            get;
            set;
        }
        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }
        [DataMember]
        public string PSID
        {
            get;
            set;
        }

        [DataMember]
        public int Location
        {
            get;
            set;
        }
        [DataMember]
        public string  OtherLocation
        {
            get;
            set;
        }

      #endregion

        #region Constructor

        public MemberJoiningDetail()
            : base()
        {
        }

        #endregion
    }
}
