﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "EmployeeMemberMapping", Namespace = "http://www.tps360.com/types")]
    public class EmployeeMemberMapping : BaseEntity
    {
        #region Properties

        [DataMember]
        public string ManagerName
        {
            get;
            set;
        }
        [DataMember]
        public string Gid
        {
            get;
            set;
        }
        #endregion

        #region Constructor

        public EmployeeMemberMapping()
            : base()
        {
        }

        #endregion
    }
}
