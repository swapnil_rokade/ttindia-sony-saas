﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "MemberProject", Namespace = "http://www.tps360.com/types")]
    public class MemberProject : BaseEntity
    {
        #region Properties

        [DataMember]
        public string Title
        {
            get;
            set;
        }

        [DataMember]
        public string ClientName
        {
            get;
            set;
        }

        [DataMember]
        public string ProjectName
        {
            get;
            set;
        }

        [DataMember]
        public string AddressLine1
        {
            get;
            set;
        }

        [DataMember]
        public string AddressLine2
        {
            get;
            set;
        }

        [DataMember]
        public string City
        {
            get;
            set;
        }

        [DataMember]
        public int StateId
        {
            get;
            set;
        }

        [DataMember]
        public string Zip
        {
            get;
            set;
        }

        [DataMember]
        public int CountryId
        {
            get;
            set;
        }

        [DataMember]
        public DateTime StartDate
        {
            get;
            set;
        }

        [DataMember]
        public DateTime EndDate
        {
            get;
            set;
        }

        [DataMember]
        public string ManagerName
        {
            get;
            set;
        }

        [DataMember]
        public string ManagerPhone
        {
            get;
            set;
        }

        [DataMember]
        public string ManagerEmail
        {
            get;
            set;
        }

        [DataMember]
        public string RecruiterName
        {
            get;
            set;
        }

        [DataMember]
        public decimal BillingRate
        {
            get;
            set;
        }

        [DataMember]
        public int BillingRateCurrencyLookupId
        {
            get;
            set;
        }

        [DataMember]
        public decimal GrossPayRate
        {
            get;
            set;
        }

        [DataMember]
        public int GrossPayRateCurrencyLookupId
        {
            get;
            set;
        }

        [DataMember]
        public string InternalRecName
        {
            get;
            set;
        }

        [DataMember]
        public string Note
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }

        [DataMember]
        public int MemberId
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public MemberProject()
            : base()
        {
        }

        #endregion
    }
}