﻿using System;
using System.Diagnostics;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "MemberExpense", Namespace = "http://www.tps360.com/types")]
    public class MemberExpense : BaseEntity
    {
        #region Properties

        [DataMember]
        public int ExpenseCategoryLookupId
        {
            get;
            set;
        }

        [DataMember]
        public string Description
        {
            get;
            set;
        }

        [DataMember]
        public decimal ExpenseAmount
        {
            get;
            set;
        }

        [DataMember]
        public int ExpenseAmountCurrencyLookupId
        {
            get;
            set;
        }

        [DataMember]
        public DateTime ExpenseDate
        {
            get;
            set;
        }

        [DataMember]
        public decimal ApproveAmount
        {
            get;
            set;
        }

        [DataMember]
        public int ApproveAmountCurrencyLookupId
        {
            get;
            set;
        }

        [DataMember]
        public DateTime ApproveDate
        {
            get;
            set;
        }

        [DataMember]
        public decimal PaidAmount
        {
            get;
            set;
        }

        [DataMember]
        public int PaidAmountLookupId
        {
            get;
            set;
        }

        [DataMember]
        public DateTime PaidDate
        {
            get;
            set;
        }

        [DataMember]
        public string CheckNumber
        {
            get;
            set;
        }

        [DataMember]
        public string UploadReceipt
        {
            get;
            set;
        }

        [DataMember]
        public int Status
        {
            get;
            set;
        }

        [DataMember]
        public int MemberId
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public MemberExpense()
            : base()
        {
        }

        #endregion
    }
}