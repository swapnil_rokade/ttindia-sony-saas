﻿using System;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "MemberPendingApprovals", Namespace = "http://www.tps360.com/types")]
    public class MemberPendingApprovals : BaseEntity
    {
        #region Properties

        [DataMember]
        public int CurrentApprovalStageId
        {
            get;
            set;
        }

        [DataMember]
        public bool IsStateApprovedByUser
        {
            get;
            set;
        }

        [DataMember]
        public DateTime DateSubmitted
        {
            get;
            set;
        }

        [DataMember]
        public string SubmittedBy
        {
            get;
            set;
        }

        [DataMember]
        public string Requisition
        {
            get;
            set;
        }

        [DataMember]
        public string OldStageName
        {
            get;
            set;
        }

        [DataMember]
        public string NewStageName
        {
            get;
            set;
        }
        [DataMember]
        public int JobpostingCreatorid
        {
            get;
            set;
        }
        [DataMember]
        public int JobPostingId
        {
            get;
            set;
        }
        [DataMember]
        public int NextStageDetailId
        {
            get;
            set;
        }

        [DataMember]
        public string JobPostingCode
        {
            get;
            set;
        }

        [DataMember]
        public string CustomerName
        {
            get;
            set;
        }

        [DataMember]
        public int MemberId
        {
            get;
            set;
        }

        [DataMember]
        public string MemberName
        {
            get;
            set;
        }
        #endregion

        #region Constructor

        public MemberPendingApprovals()
            : base()
        {
        }

        #endregion
    }
}
