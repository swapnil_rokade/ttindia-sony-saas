﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "MenuListCount", Namespace = "http://www.tps360.com/types")]
    public class ListWithCount: BaseEntity
    {
        #region Properties

     

        [DataMember]
        public string Name
        {
            get;
            set;
        }

        [DataMember]
        public int Count
        {
            get;
            set;
        }


        [DataMember]
        public int AdditionalID
        {
            get;
            set;
        }
        [DataMember]
        public string AdditionalName
        {
            get;
            set;
        }

        #endregion

        #region Constructor

        public ListWithCount()
            : base()
        {
        }

        
        #endregion
    }
}