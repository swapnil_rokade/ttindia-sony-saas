﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: CandidateJoiningReport.cs
    Description: Create Class for CandidateJoiningReport - defined all properties in it.
    Created By: Kanchan Yeware
    Created On: 09-Aug-2016
    Modification Log:   
*/

using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "Employee", Namespace = "http://www.tps360.com/types")]
    public class CandidateJoiningReport : Member
    {
        #region Properties
        [DataMember]
        public int Id
        {
            get;
            set;
        }

        [DataMember]
        public string JobPostingCode
        {
            get;
            set;
        }

        [DataMember]
        public string FirstName
        {
            get;
            set;
        }

        [DataMember]
        public string MiddleName
        {
            get;
            set;
        }

        [DataMember]
        public string LastName
        {
            get;
            set;
        }

        [DataMember]
        public int SupervisoryOrgCode
        {
            get;
            set;
        }

        [DataMember]
        public string SupervisoryOrgShortText
        {
            get;
            set;
        }

        [DataMember]
        public string SupervisoryOrgDesc
        {
            get;
            set;
        }

        [DataMember]
        public string ReasonForHire
        {
            get;
            set;
        }

        [DataMember]
        public string JobTitle
        {
            get;
            set;
        }

        [DataMember]
        public int NoOfOpenings
        {
            get;
            set;
        }

        [DataMember]
        public DateTime FinalHiredDate
        {
            get;
            set;
        }

        [DataMember]
        public DateTime OpenDate
        {
            get;
            set;
        }

        [DataMember]
        public string AvailabilityDate
        {
            get;
            set;
        }

        [DataMember]
        public DateTime ActivationDate
        {
            get;
            set;
        }

        [DataMember]
        public DateTime RequisitionExpectedDate
        {
            get;
            set;
        }

        [DataMember]
        public string JobRestrictions
        {
            get;
            set;
        }

        [DataMember]
        public string JobFamily
        {
            get;
            set;
        }

        [DataMember]
        public string JobProfile
        {
            get;
            set;
        }

        [DataMember]
        public string jobProfileName
        {
            get;
            set;
        }

        [DataMember]
        public string City
        {
            get;
            set;
        }

        [DataMember]
        public string TimeType
        {
            get;
            set;
        }

        [DataMember]
        public string WorkerType
        {
            get;
            set;
        }

        [DataMember]
        public string WorkerSubType
        {
            get;
            set;
        }

        [DataMember]
        public string Company
        {
            get;
            set;
        }

        [DataMember]
        public string CostCenter
        {
            get;
            set;
        }

        [DataMember]
        public string Designation
        {
            get;
            set;
        }

        [DataMember]
        public string GlobalGrade
        {
            get;
            set;
        }

        [DataMember]
        public string Currency
        {
            get;
            set;
        }

        [DataMember]
        public string Frequency
        {
            get;
            set;
        }

        [DataMember]
        public string CompensationPlan
        {
            get;
            set;
        }

        [DataMember]
        public Decimal OfferHead1
        {
            get;
            set;
        }

        [DataMember]
        public Decimal OfferHead2
        {
            get;
            set;
        }

        [DataMember]
        public Decimal OfferHead3
        {
            get;
            set;
        }

        [DataMember]
        public Decimal OfferHead4
        {
            get;
            set;
        }

        [DataMember]
        public Decimal OfferHead5
        {
            get;
            set;
        }

        [DataMember]
        public Decimal OfferHead6
        {
            get;
            set;
        }

        [DataMember]
        public Decimal OfferHead7
        {
            get;
            set;
        }

        [DataMember]
        public string ProjectName
        {
            get;
            set;
        }

        [DataMember]
        public string Division
        {
            get;
            set;
        }

        [DataMember]
        public string ReportMangGID
        {
            get;
            set;
        }

        [DataMember]
        public string ManagerName
        {
            get;
            set;
        }

        [DataMember]
        public string OrgUnitHead
        {
            get;
            set;
        }

        [DataMember]
        public string Region
        {
            get;
            set;
        }

        [DataMember]
        public string InternationalEmployeeStatus
        {
            get;
            set;
        }

        [DataMember]
        public string PayGroup
        {
            get;
            set;
        }

        [DataMember]
        public string PayGroupGrade
        {
            get;
            set;
        }

        [DataMember]
        public int MaximumFTE
        {
            get;
            set;
        }

        [DataMember]
        public string Notes
        {
            get;
            set;
        }

        [DataMember]
        public DateTime DOJ
        {
            get;
            set;
        }

        [DataMember]
        public string DOC
        {
            get;
            set;
        }

        [DataMember]
        public string GID
        {
            get;
            set;
        }

        [DataMember]
        public DateTime DateOfBirth
        {
            get;
            set;
        }

        [DataMember]
        public string MaritalStatus
        {
            get;
            set;
        }

        [DataMember]
        public string Gender
        {
            get;
            set;
        }

        [DataMember]
        public string NationalIDType
        {
            get;
            set;
        }

        [DataMember]
        public string NationalID
        {
            get;
            set;
        }

        [DataMember]
        public string PhoneNumber
        {
            get;
            set;
        }


        [DataMember]
        public string PhoneDevice
        {
            get;
            set;
        }

        [DataMember]
        public string PhoneType
        {
            get;
            set;
        }

        [DataMember]
        public string Address
        {
            get;
            set;
        }

        [DataMember]
        public string VendorName
        {
            get;
            set;
        }

        [DataMember]
        public string EmployeeType
        {
            get;
            set;
        }

        [DataMember]
        public string SubtypeofCW
        {
            get;
            set;
        }

        [DataMember]
        public string IndividualContributor
        {
            get;
            set;
        }

        [DataMember]
        public string Position
        {
            get;
            set;
        }

        [DataMember]
        public string PrimaryEmail
        {
            get;
            set;
        }

        #endregion

        #region Constructor

        public CandidateJoiningReport()
            : base()
        {
        }

        #endregion
    }
}
