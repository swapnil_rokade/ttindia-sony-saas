﻿using System;
using System.Runtime.Serialization;
using System.Collections;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "Employee", Namespace = "http://www.tps360.com/types")]
    public class ReportCriteria
    {
        #region Properties

        [DataMember]
        public Hashtable SearchCondition
        {
            get;
            set;
        }

        [DataMember]
        public Hashtable ColumnFilter
        {
            get;
            set;
        }

        #endregion

        #region Constructor

        public ReportCriteria()
            : base()
        {
        }

        #endregion
    }
}
