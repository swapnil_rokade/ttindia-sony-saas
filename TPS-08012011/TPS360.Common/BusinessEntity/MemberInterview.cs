﻿using System;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "MemberInterview", Namespace = "http://www.tps360.com/types")]
    public class MemberInterview : Interview
    {
        #region Properties

        [DataMember]
        public DateTime StartDateTimeUtc
        {
            get;
            set;
        }

        [DataMember]
        public string JobTitle
        {
            get;
            set;
        }

        [DataMember]
        public string JobPostingCode
        {
            get;
            set;
        }

        [DataMember]
        public string InterviewTypeName
        {
            get;
            set;
        }

        [DataMember]
        public string InterviewerName
        {
            get;
            set;
        }

        [DataMember]
        public string ClientInterviewerName
        {
            get;
            set;
        }
       
        [DataMember]
        public string FirstName
        {
            get;
            set;
        }

        [DataMember]
        public string LastName
        {
            get;
            set;
        }

        [DataMember]
        public string CompanyName
        {
            get;
            set;
        }

        [DataMember]
        public int CompanyId
        {
            get;
            set;
        }
        #endregion

        #region Constructor

        public MemberInterview()
            : base()
        {
        }

        #endregion
    }
}
