﻿using System;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "MemberExperienceDetail", Namespace = "http://www.tps360.com/types")]
    public class MemberExperienceDetail : MemberExperience
    {
        #region Properties

        [DataMember]
        public string IndustryCategory
        {
            get;
            set;
        }

        [DataMember]
        public string FunctionalCategory
        {
            get;
            set;
        }

        [DataMember]
        public string Country
        {
            get;
            set;
        }

        #endregion

        #region Constructor

        public MemberExperienceDetail()
            : base()
        {
        }

        #endregion
    }
}
