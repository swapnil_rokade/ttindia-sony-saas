﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "JobFamily", Namespace = "http://www.tps360.com/types")]
    public class GlobalGrades : BaseEntity
    {

        #region Properties


        [DataMember]
        public Int32 Id
        {
            get;
            set;
        }
        [DataMember]
        public int OldGrade
        {
            get;
            set;
        }
        [DataMember]
        public string NewGrade
        {
            get;
            set;
        }

        [DataMember]
        public int DemandRole
        {
            get;
            set;
        }
        #endregion

        #region Constructor

        public GlobalGrades()
            : base()
        {
        }

        #endregion
    }
}
