﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "CandidateJoiningDetail", Namespace = "http://www.tps360.com/types")]
    public class CandidateJoiningDetails : BaseEntity
    {
        #region Properties

        [DataMember]
        public int DemandID
        {
            get;
            set;
        }

        [DataMember]
        public string RequisitionTitle
        {
            get;
            set;
        }

        [DataMember]
        public int JobRestrictions
        {
            get;
            set;
        }

        [DataMember]
        public DateTime DateOfJoining
        {
            get;
            set;
        }

        [DataMember]
        public int EmployeeType
        {
            get;
            set;
        }

        [DataMember]
        public int WorkerSubType
        {
            get;
            set;
        }

        [DataMember]
        public int PayGroup
        {
            get;
            set;
        }

        [DataMember]
        public int PayGroupGrade
        {
            get;
            set;
        }

        [DataMember]
        public int Currency
        {
            get;
            set;
        }

        [DataMember]
        public int Frequency
        {
            get;
            set;
        }

        [DataMember]
        public int CompensationPlan
        {
            get;
            set;
        }

        [DataMember]
        public int MaximumFTE
        {
            get;
            set;
        }

        [DataMember]
        public string Notes
        {
            get;
            set;
        }

        [DataMember]
        public string CandidateID
        {
            get;
            set;
        }

        #endregion
    }
}