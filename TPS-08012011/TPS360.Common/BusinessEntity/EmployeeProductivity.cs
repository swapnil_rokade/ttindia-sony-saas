﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "Employee", Namespace = "http://www.tps360.com/types")]
    public class EmployeeProductivity :BaseEntity 
    {
        #region Properties

        [DataMember]
        public int EmployeeId 
        {
            get;
            set;
        }

        [DataMember]
        public string EmployeeName
        {
            get;
            set;
        }

        [DataMember]
        public int  JobCount
        {
            get;
            set;
        }

        [DataMember]
        public System.Collections.Generic.IList<Hiring_Levels> HiringMatrixLevels
        {
            get;
            set;
        }



        [DataMember]
        public int SubmissionCount
        {
            get;
            set;
        }

        [DataMember]
        public int  NewClientCount
        {
            get;
            set;
        }


        [DataMember]
        public int InterviewsCount
        {
            get;
            set;
        }

        [DataMember]
        public int OfferCount
        {
            get;
            set;
        }

        [DataMember]
        public int OfferRejectedCount
        {
            get;
            set;
        }


        [DataMember]
        public int JoinedCount
        {
            get;
            set;
        }





        [DataMember]
        public int CandidateSourcedCount
        {
            get;
            set;
        }

        [DataMember]
        public int NewCandidateCount
        {
            get;
            set;
        }
        [DataMember]
        public int PendingJoiners
        {
            get;
            set;
        }

        #endregion

        #region Constructor

        public EmployeeProductivity()
            : base()
        {
        }

        #endregion
    }
}
