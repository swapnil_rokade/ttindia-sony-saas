﻿using System;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name="ATSLandingPage",Namespace="http://www.tps360.com/types")]

    public class ATSLandingPage : BaseEntity
    {
        #region Properties

        [DataMember]
        public int CandidateCount
        {
            get;
            set;
        }

        [DataMember]
        public int MyCandidateCount
        {
            get;
            set;
        }

        [DataMember]
        public int AvailableCandidateCount
        {
            get;
            set;
        }

        [DataMember]
        public int SubmittedCandidatesCount
        {
            get;
            set;
        }

        [DataMember]
        public int MySubmittedCandidatesCount
        {
            get;
            set;
        }

        [DataMember]
        public int PreSelectedCnadidateCount
        {
            get;
            set;
        }

        [DataMember]
        public int Level1CandidateCount
        {
            get;
            set;
        }

        [DataMember]
        public int Level2CandidateCount
        {
            get;
            set;
        }

        [DataMember]
        public int FinalHiredCandidateCount
        {
            get;
            set;
        }

        [DataMember]
        public int InterviewScheduledCount
        {
            get;
            set;
        }

        [DataMember]
        public int InterviewCompletedCount
        {
            get;
            set;
        }

        [DataMember]
        public int InterviewFeedbackedCount
        {
            get;
            set;
        }

        [DataMember]
        public int Level1InterviewCount
        {
            get;
            set;
        }

        [DataMember]
        public int Level2InterviewCount
        {
            get;
            set;
        }

        [DataMember]
        public int Level3InterviewCount
        {
            get;
            set;
        }

        [DataMember]
        public IList<MemberInterview> MemberInterviewList
        {
            get;
            set;
        }

        [DataMember]
        public IList<MemberGroup> MemberGroupList
        {
            get;
            set;
        }

        [DataMember]
        public IList<Candidate> SubmittedCandidateList
        {
            get;
            set;
        }

        [DataMember]
        public IList<Candidate> CandidateList
        {
            get;
            set;
        }

        #endregion

        #region Constructor

        public ATSLandingPage()
            :base()
        { 
        }

        #endregion
    }
}
