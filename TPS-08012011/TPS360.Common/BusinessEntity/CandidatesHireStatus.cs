﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TPS360.Common.BusinessEntities;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "CandidateHiringStatus", Namespace = "http://www.tps360.com/types")]

    public class CandidatesHireStatus : BaseEntity
    {
        #region Properties
        [DataMember]
        public int ID
        {
            get;
            set;
        }

        [DataMember]
        public int MemberID
        {
            get;
            set;
        }

        [DataMember]
        public string CandidateName
        {
            get;
            set;
        }

        [DataMember]
        public string RequisitionJobTitle
        {
            get;
            set;
        }

        [DataMember]
        public int JobStatus
        {
            get;
            set;
        }

        [DataMember]
        public int ManagerId
        {
            get;
            set;
        }

        [DataMember]
        public string RequisitionCode
        {
            get;
            set;
        }

        [DataMember]
        public string Name
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }

        [DataMember]
        public int JobPostingID
        {
            get;
            set;
        }
        #endregion

        #region Constructor
        public CandidatesHireStatus()
            : base()
        {
        }
        #endregion
    }
}
