﻿using System;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using TPS360.BusinessFacade;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Web.UI.Helper;
using System.Web.Security;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Configuration;
namespace TPS360.Web.UI
{
    public partial class Submissions: CandidateBasePage
    {
        #region Veriables
        private int _memberId;
        #endregion

        #region Properties


        #endregion

        #region Methods



        #endregion

        #region Events
        /*
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]))
            {
                _memberId = Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]);
            }
            else { _memberId = base.CurrentMember.Id; }
            odsJobCartList.SelectParameters["memberId"].DefaultValue = _memberId.ToString();
            if (!IsPostBack) lsvJobCart.DataBind();
            string pagesize = "";
            pagesize = (Request.Cookies["JocartInCandidateOverviewRowPerPage"] == null ? "" : Request.Cookies["JocartInCandidateOverviewRowPerPage"].Value); ;

            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvJobCart.FindControl("pagerControl");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)PagerControl.FindControl("pager");
                if (pager != null) pager.PageSize = Convert.ToInt32(pagesize == "" ? "20" : pagesize);
            }
            if (!IsPostBack)
            {
                txtSortColumn.Text = "lnkJobTitle";
                txtSortOrder.Text = "asc";
                PlaceUpDownArrow();
            }
        }

        protected void lsvJobCart_ItemCommand(object sender, ListViewCommandEventArgs e)
        {

            try
            {

                if (e.CommandName == "Sort")
                {
                    LinkButton lnkbutton = (LinkButton)e.CommandSource;
                    if (txtSortColumn.Text == lnkbutton.ID)
                    {
                        if (txtSortOrder.Text == "ASC") txtSortOrder.Text = "DESC";
                        else txtSortOrder.Text = "ASC";
                    }
                    else
                    {
                        txtSortColumn.Text = lnkbutton.ID;
                        txtSortOrder.Text = "ASC";
                    }
                }
            }
            catch
            {
            }

        }

        protected void lsvJobCart_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            bool isAssingned = false;
            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                Common.BusinessEntities.MemberJobCart memberJobCart = ((ListViewDataItem)e.Item).DataItem as Common.BusinessEntities.MemberJobCart;

                if (memberJobCart != null)
                {
                    JobPostingHiringTeam team = Facade.GetJobPostingHiringTeamByMemberId(base.CurrentMember.Id, memberJobCart.JobPostingId);
                    if (team != null) isAssingned = true;
                    HyperLink lblJobTitle = (HyperLink)e.Item.FindControl("lblJobTitle");
                    Label lblDateSubmitted = (Label)e.Item.FindControl("lblDateSubmitted");
                    HyperLink  hlnkSubmittedBy = (HyperLink )e.Item.FindControl("hlnkSubmittedBy");
                    lblJobTitle.Attributes.Add("onclick", "EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/JobDetail.aspx?JID=" + memberJobCart.JobPostingId + "&FromPage=JobDetail" + "','700px','570px'); return false;");
                    lblJobTitle.Text = memberJobCart.JobTitle;

                    lblDateSubmitted.Text = memberJobCart.CreateDate.ToShortDateString();
                    //hlnkSubmittedBy .Text = memberJobCart.creat ;//.FirstName + " " + memberJobCart.Creator.LastName;
                }
            }
        }
        protected void lsvJobCart_PreRender(object sender, EventArgs e)
        {
            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvJobCart.FindControl("pagerControl");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)PagerControl.FindControl("pager");
                if (pager != null)
                {
                    DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                    if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
                }
                HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
                if (hdnRowPerPageName != null) hdnRowPerPageName.Value = "JocartInCandidateOverviewRowPerPage";
            }



            PlaceUpDownArrow();
        }
        private void PlaceUpDownArrow()
        {
            try
            {
                LinkButton lnk = (LinkButton)lsvJobCart.FindControl(txtSortColumn.Text);
                System.Web.UI.HtmlControls.HtmlTableCell im = (System.Web.UI.HtmlControls.HtmlTableCell)lnk.Parent;
                im.EnableViewState = false;
                im.Attributes.Add("class", (txtSortOrder.Text == "ASC" ? "Ascending" : "Descending"));

            }
            catch
            {
            }

        }
        */
        #endregion
    }
}