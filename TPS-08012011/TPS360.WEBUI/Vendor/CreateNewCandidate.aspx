﻿<%@ Page Language="C#" MasterPageFile="~/Vendor/VendorPortalLeft.master" AutoEventWireup="true" CodeFile="CreateNewCandidate.aspx.cs"  EnableEventValidation ="false" 
    Inherits="TPS360.Web.UI.CreateNewCandidate" Title="New Candidate Registration" %>

<%@ Register Src="~/Controls/RegistrationInternal.ascx" TagName="regI" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHomeMasterTitle" Runat="Server">
Candidates
</asp:Content>
<asp:Content ID="cntRegistrationInformation" ContentPlaceHolderID="cphVendorMaster"
    runat="Server">
  <asp:HiddenField ID="hdnCreateNewCandidate" runat="server" />
    <asp:UpdatePanel ID="pnlRegistrationInformation" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div id="divRegInternal" runat="server">
                 <uc1:regI ID="rgInternal" runat="server" />                                      
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

