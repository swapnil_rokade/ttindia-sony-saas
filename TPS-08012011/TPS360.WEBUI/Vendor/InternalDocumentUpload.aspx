﻿<%@ Page Language="C#" MasterPageFile="~/Masters/CandidateProfile.master" AutoEventWireup="true"
    CodeFile="InternalDocumentUpload.aspx.cs" Inherits="TPS360.Web.UI.CandidateInternalDocumentUpload"
    Title="Candidate Documents" %>

<%@ Register Src="~/Controls/MemberDocumentUploader.ascx" TagName="Document" TagPrefix="uc1" %>
<asp:Content ID="cntCandidateDocumentsUpload" ContentPlaceHolderID="cphCandidateMaster"
    runat="Server">
    <asp:HiddenField ID="hdnPageTitle" runat="server" />
    <div style="width: 100%;">
        <div style="text-align: left; width: 100%;">
            <uc1:Document ID="ucntrlDocument" runat="server" />
        </div>
    </div>
</asp:Content>
