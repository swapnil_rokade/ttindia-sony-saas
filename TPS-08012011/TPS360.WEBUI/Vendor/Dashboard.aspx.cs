using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Web.UI.Helper;
using TPS360.Web.UI;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using TPS360.BusinessFacade;
using System.Collections.Generic;
using TPS360.Common;
using System.IO;
using ExpertPdf.HtmlToPdf;
using System.Drawing;
public partial class Dashboard : AdminBasePage
{
    private int VendorId
    {
        get
        {
            if (CurrentMember != null)
            {
                CompanyContact com = new CompanyContact();
                com = Facade.GetCompanyContactByMemberId(CurrentMember.Id);
                return com.CompanyId;
            }
            else
            {
                return 0;
            }

        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //uclVendorSubmissions.ContactId = base.CurrentMember.Id;
        //hdnVendorId.Value = VendorId.ToString();
        //if (SiteSetting[DefaultSiteSetting.CompanyName.ToString()].ToString() != null) 
        //{
        //    hdnEmployeeDashboard.Value = SiteSetting[DefaultSiteSetting.CompanyName.ToString()].ToString();
        //    this.Page.Title = hdnEmployeeDashboard.Value + " - Dashboard";
        
        //}
        string pagesize = "";
        pagesize = (Request.Cookies["DashboardVendorJobPostingRowPerPage"] == null ? "" : Request.Cookies["DashboardVendorJobPostingRowPerPage"].Value); ;
        //ASP.controls_smallpagercontrol_ascx PagerControl = (ASP.controls_smallpagercontrol_ascx)this.lsvJobPosting .FindControl("pagerControl");
        //if (PagerControl != null)
        //{
        //    DataPager pager = (DataPager)PagerControl.FindControl("pager");
        //    if (pager != null) pager.PageSize = Convert.ToInt32(pagesize == "" ? "10" : pagesize);
        //}

    }
    //protected void lsvJobPosting_ItemDataBound(object sender, ListViewItemEventArgs e)
    //{
    //    if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
    //    {
    //        JobPosting jobPosting = ((ListViewDataItem)e.Item).DataItem as JobPosting;
    //        if (jobPosting != null)
    //        {


    //            Label lblPostedDate = (Label)e.Item.FindControl("lblPostedDate");
    //            HyperLink lnkJobTitle = (HyperLink)e.Item.FindControl("lnkJobTitle");
    //            Label lblPublishedBy = (Label)e.Item.FindControl("lblPublishedBy");
    //            Label lblAppy = (Label)e.Item.FindControl("lblAppy");
    //            ImageButton btnApply = (ImageButton)e.Item.FindControl("btnApply");
    //            lblPostedDate.Text = jobPosting.PostedDate.ToShortDateString();
    //            lnkJobTitle.Text = jobPosting.JobTitle;
    //            lnkJobTitle.Attributes.Add("onclick", "javascript:EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/JobDetail.aspx?JID=" + jobPosting.Id + "&FromPage=Vendor','700px','570px')");
    //            lblPublishedBy.Text = jobPosting.CreatorName;
    //        }
    //    }
    //}

    //protected void lsvJobPosting_ItemCommand(object sender, ListViewCommandEventArgs e)
    //{

    //    try
    //    {
    //        if (e.CommandName == "Sort")
    //        {
    //            LinkButton lnkbutton = (LinkButton)e.CommandSource;
    //            if (hdnSortColumn.Value == lnkbutton.ID)
    //            {
    //                if (hdnSortOrder.Value == "ASC") hdnSortOrder.Value = "DESC";
    //                else hdnSortOrder.Value = "ASC";
    //            }
    //            else
    //            {
    //                hdnSortColumn.Value = lnkbutton.ID;
    //                hdnSortOrder.Value = "ASC";
    //            }
    //        }
    //    }
    //    catch
    //    {
    //    }

    //}

    //protected void lsvJobPosting_PreRender(object sender, EventArgs e)
    //{
    //    ASP.controls_smallpagercontrol_ascx PagerControl = (ASP.controls_smallpagercontrol_ascx)this.lsvJobPosting .FindControl("pagerControl");
    //    if (PagerControl != null)
    //    {
    //        DataPager pager = (DataPager)PagerControl.FindControl("pager");
    //        if (pager != null)
    //        {
    //            if (pager.Controls.Count >= 1)
    //            {
    //                DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
    //                if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
    //            }
    //        }
    //        HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
    //        if (hdnRowPerPageName != null) hdnRowPerPageName.Value = "DashboardVendorJobPostingRowPerPage";
    //    }
    //    else
    //    {
    //        ExportButtons.Visible = false;
    //    }
    //    PlaceUpDownArrow();
    //}
    //protected void odsRequisitionList_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
    //{

    //}
    //private void PlaceUpDownArrow()
    //{
    //    try
    //    {
    //        LinkButton lnk = (LinkButton)lsvJobPosting.FindControl(hdnSortColumn.Value);
    //        System.Web.UI.HtmlControls.HtmlTableCell im = (System.Web.UI.HtmlControls.HtmlTableCell)lnk.Parent;
    //        im.EnableViewState = false;
    //        if (lnk.CommandArgument.Contains("[1]") || lnk.CommandArgument.Contains("[JMC].[ManagersCount]"))
    //        {
    //            im.Attributes.Add("class", (hdnSortOrder.Value == "ASC" ? "Descending" : "Ascending"));
    //        }
    //        else
    //            im.Attributes.Add("class", (hdnSortOrder.Value == "ASC" ? "Ascending" : "Descending"));
    //    }
    //    catch
    //    {
    //    }

    //}
    //protected void btnExportToPDF_Click(object sender, EventArgs e)
    //{
    //    repor("pdf");
    //}
    //protected void btnExportToExcel_Click(object sender, EventArgs e)
    //{
    //    repor("excel");
    //}
    //protected void btnExportToWord_Click(object sender, EventArgs e)
    //{
    //    repor("word");
    //}
    //public void repor(string rep)
    //{
    //    GenerateVendorCandidatePerformanceReport(rep);
    //}
    //private void GenerateVendorCandidatePerformanceReport(string format)
    //{
    //    if (string.Equals(format, "word"))
    //    {
    //        Response.Buffer = true;
    //        Response.AddHeader("content-disposition", "attachment;filename=VendorRecentlyPostedRequisitionsReport-" + DateTime.Now.ToString("yyyy-MM-dd") + ".doc");
    //        Response.ContentEncoding = System.Text.Encoding.ASCII;
    //        Response.ContentType = "application/msword";
    //        Response.Output.Write(GetVendorCandidatePerformanceReportTable());
    //        Response.Flush();
    //        Response.End();
    //    }
    //    else if (string.Equals(format, "excel"))
    //    {
    //        Response.Buffer = true;
    //        Response.AddHeader("content-disposition", "attachment;filename=VendorRecentlyPostedRequisitionsReport-" + DateTime.Now.ToString("yyyy-MM-dd") + ".xls");
    //        Response.ContentEncoding = System.Text.Encoding.ASCII;
    //        Response.ContentType = "application/msexcel";
    //        Response.Output.Write(GetVendorCandidatePerformanceReportTable());
    //        Response.Flush();
    //        Response.End();
    //    }
    //    else if (string.Equals(format, "pdf"))
    //    {
    //        PdfConverter pdfConverter = new PdfConverter();
    //        UnicodeEncoding unicodeEncoding = new UnicodeEncoding();
    //        pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A3;
    //        pdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Normal;
    //        pdfConverter.PdfDocumentOptions.ShowHeader = false;
    //        pdfConverter.PdfDocumentOptions.ShowFooter = false;
    //        pdfConverter.PdfDocumentOptions.LeftMargin = 15;
    //        pdfConverter.PdfDocumentOptions.RightMargin = 5;
    //        pdfConverter.PdfDocumentOptions.TopMargin = 15;
    //        pdfConverter.PdfDocumentOptions.BottomMargin = 5;
    //        pdfConverter.PdfDocumentOptions.GenerateSelectablePdf = true;
    //        pdfConverter.PdfDocumentOptions.ShowHeader = false;
    //        pdfConverter.PdfHeaderOptions.DrawHeaderLine = false;
    //        pdfConverter.PdfFooterOptions.FooterTextColor = Color.Black;
    //        pdfConverter.PdfFooterOptions.DrawFooterLine = true;
    //        pdfConverter.PdfFooterOptions.PageNumberText = "Page";
    //        pdfConverter.PdfFooterOptions.ShowPageNumber = true;

    //        pdfConverter.LicenseKey = ContextConstants.EPHTMLTOPDFKEY;
    //        byte[] downloadBytes = pdfConverter.GetPdfBytesFromHtmlStringWithTempFile(GetVendorCandidatePerformanceReportTable(), "");
    //        System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
    //        response.Clear();
    //        response.AddHeader("Content-Type", "binary/octet-stream");
    //        response.AddHeader("Content-Disposition", "attachment; filename=VendorRecentlyPostedRequisitionsReport-" + DateTime.Now.ToString("yyyy-MM-dd") + ".pdf; size=" + downloadBytes.Length.ToString());
    //        response.Flush();
    //        response.BinaryWrite(downloadBytes);
    //        response.Flush();
    //        response.End();
    //    }
    //}
    //private string GetVendorCandidatePerformanceReportTable()
    //{
    //    StringBuilder sourcingReport = new StringBuilder();
    //    JobPostingDataSource JobPostingDataSource = new JobPostingDataSource();

    //    IList<JobPosting> JobPosting = JobPostingDataSource.GetPagedRequisitionListForVendorPortal(null ,null,null,"0","0",VendorId.ToString(),"", 0, 100);

    //    if (JobPosting != null)
    //    {
    //        sourcingReport.Append("<table style='font-family : calibriz;width:100%; border-collapse:collapse; border-spacing: 1px;' align='left' border = '1' bordercolor='#000000'  cellspacing='0' cellpadding='0'>");

    //        sourcingReport.Append(" <tr>");
    //        sourcingReport.Append("     <th>Date Created</th>");
    //        sourcingReport.Append("     <th>Created By</th>");
    //        sourcingReport.Append("     <th>Job Title</th>");
    //        sourcingReport.Append(" </tr>");

    //        foreach (JobPosting info in JobPosting)
    //        {
    //            if (info != null)
    //            {
    //                sourcingReport.Append(" <tr>");
    //                sourcingReport.Append("     <td>" + info.PostedDate.ToShortDateString() + "&nbsp;</td>");
    //                sourcingReport.Append("     <td>" + info.CreatorName.ToString() + "&nbsp;</td>");
    //                sourcingReport.Append("     <td>" + info.JobTitle + "&nbsp;</td>");                  
    //            }
    //        }
    //        sourcingReport.Append("    <tr>");
    //        sourcingReport.Append(" </table>");
    //    }
    //    return sourcingReport.ToString();
    //}   
}
 