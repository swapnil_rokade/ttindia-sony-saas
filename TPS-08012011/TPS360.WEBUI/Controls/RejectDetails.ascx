﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RejectDetails.ascx.cs" Inherits="TPS360.Web.UI.RejectDetails" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
 <%@ Register assembly="Infragistics35.Web.v12.2, Version=12.2.20122.1007, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" namespace="Infragistics.Web.UI.LayoutControls" tagprefix="ig" %>
<%@ Register assembly="Infragistics35.Web.v12.2, Version=12.2.20122.1007, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" namespace="Infragistics.Web.UI.EditorControls" tagprefix="ig" %>

 
    <style type="text/css">
.front
{

 position : relative ;
}

</style>
<asp:UpdatePanel ID="upReject" runat ="server" >
<ContentTemplate >

<asp:HiddenField ID ="hdnIsEdit" runat ="server" />
<asp:HiddenField ID="hfMemberId" runat ="server" />
<asp:HiddenField ID ="hfStatusId" runat ="server" />
<asp:HiddenField ID="hfJobPostingId" runat ="server" />
<asp:HiddenField ID ="hfHiringMatrixLevel" runat ="server" />
<asp:HiddenField ID ="hfMemberRejectCandidateId" runat ="server" Value ="0" />

<div class ="TableRow" >
    <div class ="TableFormLeble" style =" padding-left :10px;">Reason for Rejection:
    </div>
    <div class ="TableFormContent">
        <asp:TextBox ID="txtReject" runat ="server" CssClass ="CommonTextBox" TabIndex ="1" TextMode ="MultiLine"  Width ="220px"  ></asp:TextBox>
    </div>
</div>
<div class ="TableRow" style =" padding-top :30px; margin-left : 42%">
    <asp:Button ID="btnSave" runat ="server" Text ="Save" CssClass="CommonButton"  ValidationGroup ="ValRejectDetails" OnClick ="btnSave_Click"  TabIndex ="9"
     />
</div>
</ContentTemplate>
</asp:UpdatePanel>

