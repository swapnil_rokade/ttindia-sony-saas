﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName:RequisitionWorkflow.aspx.cs
    Description: 
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
 *  0.1                4/March/2016         pravin khot        added- IList<CustomRole> roleList1 = Facade.GetCustomRoleRequisition(2);

-------------------------------------------------------------------------------------------------------------------------------------------       

>*/
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Web.UI.Helper;
using System.Web.UI;
using System.Collections;
using System.Text.RegularExpressions;
using System.Text;
using System.Linq;
namespace TPS360.Web.UI
{
    public partial class cltRequisitionWorkflow : RequisitionBaseControl
    {
        #region Member Variables
        private bool Isdelete = false;
        #endregion

        #region Methods
        //*******************Suraj Adsule******Start********For(uneditable for not accessible user)********20160916*********//
        protected void ReadOnlyControl()
        {

            txtCustomerName.ReadOnly = true;
            rdoYes.Enabled = false;
            rdoNA.Enabled = false;
            rdoNO.Enabled = false;
            txtAdditionalNote.ReadOnly = true;
            lstRecruiters.Enabled = false;
            btnAddRecruiters.Enabled = false;
            lsvAssignedTeam.Enabled = false;
            txtClientName.ReadOnly = true;
            ddlClientContact.Enabled = false;
            txtClientHourlyRate.ReadOnly = true;
            ddlSalary.Enabled = false;
            ddlClientHourlyRateCurrency.Enabled = false;
        }
        //*******************Suraj Adsule******End********For(uneditable for not accessible user)********20160916*********//
        private int SelectedClientID()
        {
            if (txtClientName.Visible == false)
            {
                return Int32.Parse(ddlClientName.SelectedValue);
            }
            else
            {
                if (txtClientName.Text == hdnSelectedClientText.Value)
                    return Int32.Parse(hdnSelectedClient.Value != "" ? hdnSelectedClient.Value : "0");
                else return 0;
            }
        }

        public JobPosting BuildJobPosting(JobPosting jobPosting)
        {
            jobPosting.IsApprovalRequired = false;
            //jobPosting.ClientId = SelectedClientID();
            //jobPosting.ClientContactId = Convert.ToInt32(hdnSelectedContactID.Value == "" ? "0" : hdnSelectedContactID.Value); //Convert.ToInt32(ddlClientContact.SelectedValue);
            if (SiteSetting[DefaultSiteSetting.ApplicationEdition.ToString()].ToString() == UIConstants.STAFFING_FIRM)
            {
                jobPosting.ClientHourlyRate = MiscUtil.RemoveScript(txtClientHourlyRate.Text);
                jobPosting.ClientRatePayCycle = ddlSalary.SelectedValue.ToString();
                jobPosting.ClientHourlyRateCurrencyLookupId = Int32.Parse(ddlClientHourlyRateCurrency.SelectedValue);
            }
            jobPosting.InternalNote = MiscUtil.RemoveScript(txtAdditionalNote.Text);
            jobPosting.CustomerName = MiscUtil.RemoveScript(txtCustomerName.Text);
            jobPosting.CustomerName = MiscUtil.RemoveScript(txtCustomerName.Text);


            switch (ApplicationSource)
            {
                case ApplicationSource.LandTApplication:
                    if (rdoYes.Checked)
                    {
                        jobPosting.POAvailability = 1;

                    }
                    else if (rdoNO.Checked)
                    {
                        jobPosting.POAvailability = 2;
                    }
                    else if (rdoNA.Checked)
                    {
                        jobPosting.POAvailability = 3;
                    }
                    break;
                case ApplicationSource.MainApplication:
                    jobPosting.POAvailability = 0;
                    break;
                case ApplicationSource.SelectigenceApplication:
                    jobPosting.POAvailability = 0;
                    break;
                default:
                    jobPosting.POAvailability = 0;
                    break;
            }

            //if (!isMainApplication)
            //{
            //    if (rdoYes.Checked)
            //    {
            //        jobPosting.POAvailability = 1;

            //    }
            //    else if (rdoNO.Checked)
            //    {
            //        jobPosting.POAvailability = 2;
            //    }
            //    else if (rdoNA.Checked)
            //    {
            //        jobPosting.POAvailability = 3;
            //    }
            //}
            //else jobPosting.POAvailability = 0;
            return jobPosting;
        }

        public void SaveRecruiters(int JobPostingID, int ContactId)
        {
            IList<int> recr = getAssignedRecruitersFromList();
            string MemberIDs = "";
            foreach (int r in recr)
            {
                if (MemberIDs != "")
                    MemberIDs += ","; MemberIDs += r;
            }
            //if (ContactId != 0) 
            //{
            //    MemberIDs += ","; MemberIDs += ContactId;
            //}

            //*********Code added by pravin khot on 4/March/2016********************
            //*********[Kanchan Yeware] - [Requisition Approver IJP] – [1-Sept-2016] – Start *****************
            // Add this change for assigning Requistion to HR_Manager instead of Reruiter Manager
            // As we had latest change in the Process of Requisition Approver in IJP Module.
            //IList<CustomRole> roleList1 = Facade.GetCustomRoleRequisition(2);
            // Section ID = 2 = Used for Rec_Manager
            // Section ID = 3 = Used for HR_Manager Added in new Recruitment.

            IList<CustomRole> roleList1 = Facade.GetCustomRoleRequisition(3);
            IList<CustomRole> roleList2 = Facade.GetCustomRoleRequisition(2);

            if (roleList2 != null)
            {
                //code commented by pravin khot on 23/Nov/2016
                //if (roleList1 != null)
                //    foreach (CustomRole role in roleList1)
                //    {
                //        string roleName = Facade.GetCustomRoleNameByMemberId(role.Id);
                //        if (roleName.ToLower() == "hr manager")
                //        {
                //            MemberIDs += ","; MemberIDs += role.Id;
                //        }                      
                //    }
                //****************END***************
                foreach (CustomRole role in roleList2)
                {
                    MemberIDs += ","; MemberIDs += role.Id;
                }
            }
            //*********[Kanchan Yeware] - [Requisition Approver IJP] – [1-Sept-2016] – End *****************
            //**************************END*******************************************

            Facade.AddMultipleJobPostingHiringTeam(MemberIDs, JobPostingID, CurrentMember.Id, EmployeeType.Internal.ToString());
        }
        private void AddSelectedMemberToJobPostingHiringTeam(ListBox listBox, EmployeeType employeeType)
        {
            bool iserror = false;
            int countSelected = 0;
            int countAdded = 0;
            int memberId = 0;
            bool isCurrentmemberadded = false;
            IList<int> AddedRecruiters = getAssignedRecruitersFromList();
            IList<JobPostingHiringTeam> team = new List<JobPostingHiringTeam>();
            string message = string.Empty;
            {
                //edited by Suraj Adsule for assigned team box 20161006
                team = new List<JobPostingHiringTeam>(Facade.GetAllJobPostingHiringTeamByJobPostingId(CurrentJobPostingId));
                //end suraj
                for (int i = 0; i <= listBox.Items.Count - 1; i++)
                {
                    Int32.TryParse(listBox.Items[i].Value, out memberId);
                    if (memberId > 0)
                    {
                        if (listBox.Items[i].Selected || AddedRecruiters.Contains(memberId))
                        {
                            if (listBox.Items[i].Selected) countSelected++;
                            JobPostingHiringTeam jobPostingHiringTeam = new JobPostingHiringTeam();
                            jobPostingHiringTeam.MemberId = memberId;

                            jobPostingHiringTeam.Name = listBox.Items[i].Text;
                            team.Add(jobPostingHiringTeam);
                            if (!AddedRecruiters.Contains(memberId)) countAdded++;
                            if (memberId == CurrentMember.Id)
                            {
                                isCurrentmemberadded = true;
                            }
                        }
                    }

                }
                if (CurrentJobPostingId == 0 || CurrentJobPosting.CreatorId == CurrentMember.Id)
                {
                    if (isCurrentmemberadded == false)
                    {
                        JobPostingHiringTeam jobPostingHiringTeam = new JobPostingHiringTeam();
                        jobPostingHiringTeam.MemberId = CurrentMember.Id;
                        jobPostingHiringTeam.Name = CurrentMember.FirstName + " " + CurrentMember.LastName + "  [" + CurrentMember.PrimaryEmail + "]";
                        team.Add(jobPostingHiringTeam);
                    }
                }

                //edited by Suraj Adsule for assigned team box 20161006    
                team = team.Distinct().ToList();
                team = team.GroupBy(x => x.MemberId).Select(x => x.FirstOrDefault()).ToList();
                //end suraj

                lsvAssignedTeam.DataSource = team;
                lsvAssignedTeam.DataBind();
                if (countSelected > 0)
                {
                    if (countAdded > 0)
                    {
                        MiscUtil.EventLogForRequisition(EventLogForRequisition.RequisitionAssigned, CurrentJobPosting.Id, CurrentMember.Id, Facade);
                        int countNotAdded = countSelected - countAdded;
                        if (countNotAdded == 0)
                        {
                            message = "Successfully added all the selected members.";
                        }
                        else
                        {
                            message = "Successfully added " + countAdded + " of " + countSelected + " selected members. The remaining members are already in the list</b></font>";
                        }
                    }
                    else
                    {
                        message = "The selected member(s) are already in the list";
                        iserror = true;
                    }
                }
                else
                {
                    message = "Select at least one member to add.";
                    iserror = true;
                }
            }
            //else 
            //{
            //    message = "Please Select a member to add"; 
            //}

            MiscUtil.ShowMessage(lblMessage, message, iserror);
        }

        private void PopulateClientContactList()
        {
            ddlClientContact.SelectedIndex = 0;


            ArrayList contacts = Facade.GetAllCompanyContactsByCompanyId(ddlClientName.Visible ? Convert.ToInt32(ddlClientName.SelectedValue == "" ? "0" : ddlClientName.SelectedValue) : Convert.ToInt32(hdnSelectedClient.Value == "" ? "0" : hdnSelectedClient.Value));
            CompanyContact con = new CompanyContact();
            con.Id = 0;
            con.FirstName = "Please Select";
            contacts.Insert(0, con);
            ddlClientContact.DataSource = contacts;
            ddlClientContact.DataValueField = "Id";
            ddlClientContact.DataTextField = "FirstName";

            ddlClientContact.DataBind();
            ddlClientContact = (DropDownList)MiscUtil.RemoveScriptForDropDown(ddlClientContact);
            if (ddlClientContact.Items.Count == 1) ddlClientContact.Enabled = false;
            else ddlClientContact.Enabled = true;


        }

        private void PopulateClientDropdowns()
        {
            int CompanyCount = Facade.Company_GetCompanyCount();
            if (CompanyCount <= 500)
            {
                int companyStatus = (int)MiscUtil.GetCompanyStatusFromDefaultSiteSetting(SiteSetting);

                ddlClientName.Visible = true;
                txtClientName.Visible = false;
                ddlClientName.Items.Clear();
                ddlClientName.DataSource = Facade.GetAllClientsByStatus(companyStatus);

                ddlClientName.DataTextField = "CompanyName";
                ddlClientName.DataValueField = "Id";
                ddlClientName.DataBind();
                if (SiteSetting[DefaultSiteSetting.AssociateRequisitionsWithEntitiesFromModule.ToString()].ToString() == UIConstants.AccountType_Department)
                {
                    ddlClientName = (DropDownList)MiscUtil.RemoveScriptForDropDown(ddlClientName);
                    ddlClientName.Items.Insert(0, new ListItem("Select Department", "0"));
                    lblClientName.Text = "Department Name";
                    lblClientContactName.Text = "Department Contact";

                }
                else if (SiteSetting[DefaultSiteSetting.AssociateRequisitionsWithEntitiesFromModule.ToString()].ToString() == UIConstants.AccountType_Client)
                {
                    ddlClientName = (DropDownList)MiscUtil.RemoveScriptForDropDown(ddlClientName);
                    ddlClientName.Items.Insert(0, new ListItem("Select Account", "0"));
                    lblClientName.Text = "Account Name";
                    lblClientContactName.Text = "Account Contact";
                }
                else if (SiteSetting[DefaultSiteSetting.AssociateRequisitionsWithEntitiesFromModule.ToString()].ToString() == UIConstants.AccountType_Vendor)
                {
                    ddlClientName = (DropDownList)MiscUtil.RemoveScriptForDropDown(ddlClientName);
                    ddlClientName.Items.Insert(0, new ListItem("Select Vendor", "0"));
                    lblClientName.Text = "Vendor Name";
                    lblClientContactName.Text = "Vendor Contact";

                }
            }
            else
            {
                ddlClientName.Visible = false;
                txtClientName.Visible = true;
            }
            PopulateClientContactList();

        }

        private void PopulateHiringTeamMembers(JobPosting jobPosting)
        {
            lsvAssignedTeam.DataSource = Facade.GetAllJobPostingHiringTeamByJobPostingId(jobPosting.Id);
            lsvAssignedTeam.DataBind();
        }


        public void PostbackCall()
        {
            if (ddlClientHourlyRateCurrency.SelectedItem.Text == "INR")
                lblBillrateCurrency.Text = "(Lacs)";
            PopulateClientContactList();
            ControlHelper.SelectListByValue(ddlClientContact, hdnSelectedContactID.Value);
        }
        private void PopulateFormData(JobPosting jobPosting)
        {
            //if (!txtClientName.Visible) ddlClientName.SelectedValue = jobPosting.ClientId.ToString();
            //  else
            //  {
            //      if (jobPosting.ClientId > 0)
            //      {
            //          Company company = Facade.GetCompanyById(jobPosting.ClientId);
            //          if (company != null)
            //          {
            //              txtClientName.Text = company.CompanyName;
            //              hdnSelectedClient.Value = company.Id.ToString ();
            //              hdnSelectedClientText.Value = company.CompanyName;
            //          }
            //      }

            //  }
            txtClientHourlyRate.Text = jobPosting.ClientHourlyRate;
            txtAdditionalNote.Text = MiscUtil.RemoveScript(jobPosting.InternalNote, string.Empty);
            txtCustomerName.Text = MiscUtil.RemoveScript(jobPosting.CustomerName, string.Empty);
            if (jobPosting.ClientHourlyRateCurrencyLookupId != 0)
            {
                ddlClientHourlyRateCurrency.SelectedValue = jobPosting.ClientHourlyRateCurrencyLookupId.ToString();
                ddlSalary.SelectedValue = jobPosting.ClientRatePayCycle.ToString();
            }
            if (ddlClientHourlyRateCurrency.SelectedItem.Text == "INR")
                lblBillrateCurrency.Text = "(Lacs)";
            //PopulateClientContactList();
            //ControlHelper.SelectListByValue(ddlClientContact, jobPosting.ClientContactId.ToString());
            //hdnSelectedContactID.Value = ddlClientContact.SelectedValue;

            if (jobPosting.POAvailability == 1)
            {
                rdoYes.Checked = true;
                rdoNO.Checked = false;
                rdoNA.Checked = false;

            }
            else if (jobPosting.POAvailability == 2)
            {
                rdoNO.Checked = true;
                rdoYes.Checked = false;
                rdoNA.Checked = false;
            }
            else if (jobPosting.POAvailability == 3)
            {
                rdoNA.Checked = true;
                rdoYes.Checked = false;
                rdoNO.Checked = false;
            }
            txtCustomerName.Text = MiscUtil.RemoveScript(jobPosting.CustomerName, string.Empty);
        }

        private void Prepareview(JobPosting jobPosting)
        {
            if (CurrentUserRole == "Vendor")
            {
                txtClientName.Visible = false;
                ddlClientName.Visible = false;
                lblVendorClientName.Visible = true;
                lblVendorClientName.Text = Facade.GetCompanyNameByVendorMemberId(CurrentMember.Id);
            }
            else PopulateClientDropdowns();
            //MiscUtil.PopulateMemberListByRole(lstRecruiters, ContextConstants.ROLE_EMPLOYEE, Facade);

            //*********[Kanchan Yeware] - [Requisition Approval IJP] – [1-Sep-2016] – Start *****************
            MiscUtil.PopulateHRMemberListWithEmailByRole(lstRecruiters, Facade);
            MiscUtil.PopulateMemberListWithEmailByRole(lstRecruiters, ContextConstants.ROLE_EMPLOYEE, Facade);
            //*********[Kanchan Yeware] - [Requisition Approval IJP] – [1-Sep-2016] – Start *****************

            ListItem removeitem = null;
            foreach (ListItem item in lstRecruiters.Items)
            {
                if (Convert.ToInt32(item.Value) == CurrentMember.Id)
                {
                    removeitem = item;
                }
            }
            if (removeitem != null) lstRecruiters.Items.Remove(removeitem);

            if (lstRecruiters.Items.Count > 0) lstRecruiters.Items.RemoveAt(0);


            if (SiteSetting[DefaultSiteSetting.ApplicationEdition.ToString()].ToString() == UIConstants.INTERNAL_HIRING)
            {
                //lblClientName.Text = "Department";
                //lblClientContactName.Text = "Department Contact";
            }
            if (SiteSetting[DefaultSiteSetting.ApplicationEdition.ToString()].ToString() != UIConstants.STAFFING_FIRM)
            {
                lstRecruiters.Focus();
                divStaffingFirm.Visible = false;
            }

        }

        private void DisableValidationControls()
        {
            rfvCustomerName.Enabled = false;
            trcustomername.Visible = false;
            trpoavailability.Visible = false;
        }

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {

            //if (isMainApplication)
            //{
            //    DisableValidationControls();
            //}

            if (ApplicationSource == ApplicationSource.MainApplication || ApplicationSource == ApplicationSource.SelectigenceApplication || ApplicationSource == ApplicationSource.GenisysApplication)
            {
                DisableValidationControls();
            }
            ddlClientName.Attributes.Add("onChange", "return Company_OnChange('" + ddlClientName.ClientID + "','" + ddlClientContact.ClientID + "','" + hdnSelectedContactID.ClientID + "')");
            ddlClientContact.Attributes.Add("OnChange", "return Contact_OnChange('" + ddlClientContact.ClientID + "','" + hdnSelectedContactID.ClientID + "')");
            ddlClientHourlyRateCurrency.Attributes.Add("onchange", "javascript:ShowYearlyCurrency('" + ddlClientHourlyRateCurrency.ClientID + "','" + lblBillrateCurrency.ClientID + "')");
            ddlClientContact.Attributes.Add("onmouseover", "this.title=this.options[this.selectedIndex].title");
            ddlClientName.Attributes.Add("onmouseover", "this.title=this.options[this.selectedIndex].title");
            if (!Page.IsPostBack)
            {
                MiscUtil.PopulateCurrency(ddlClientHourlyRateCurrency, Facade);
                ddlClientHourlyRateCurrency.SelectedValue = SiteSetting[DefaultSiteSetting.Currency.ToString()].ToString();
                ddlSalary.SelectedValue = SiteSetting[DefaultSiteSetting.PaymentType.ToString()].ToString();

                JobPostingHiringTeam jobPostingHiringTeam = new JobPostingHiringTeam();
                jobPostingHiringTeam.MemberId = CurrentMember.Id;
                jobPostingHiringTeam.Name = CurrentMember.FirstName + " " + CurrentMember.LastName + "[" + CurrentMember.PrimaryEmail + "]";

                IList<JobPostingHiringTeam> list = new List<JobPostingHiringTeam>();
                list.Add(jobPostingHiringTeam);
                lsvAssignedTeam.DataSource = list;
                lsvAssignedTeam.DataBind();

                //IList<CustomRole> roleList2 = Facade.GetCustomRoleRequisition(2);
                //lsvAssignedTeam.DataSource = roleList2;
                //lsvAssignedTeam.DataBind();

                Prepareview(CurrentJobPosting);
                if (CurrentJobPostingId > 0)
                {
                    PopulateHiringTeamMembers(CurrentJobPosting);
                    PopulateFormData(CurrentJobPosting);
                }
                //*******************Suraj Adsule******Start********For(uneditable for not accessible user)********20160916*********//
                string roleName = Facade.GetCustomRoleNameByMemberId(CurrentMember.Id);
                if (roleName.ToLower() == "hr manager" && CurrentJobPostingId > 0) //|| roleName.ToLower() == "recruitment manager"
                {
                    ReadOnlyControl();
                }
                //*******************Suraj Adsule******End********For(uneditable for not accessible user)********20160916*********//
            }
            if (IsPostBack) PostbackCall();
        }

        protected void btnAddRecruiters_Click(object sender, EventArgs e)
        {
            AddSelectedMemberToJobPostingHiringTeam(lstRecruiters, EmployeeType.Internal);
            //PopulateHiringTeamMembers();
            PopulateClientContactList();

            ControlHelper.SelectListByValue(ddlClientContact, hdnSelectedContactID.Value);
            lstRecruiters.Focus();
        }
        protected void lsvAssignedTeam_PreRender(object sender, EventArgs e)
        {
            Isdelete = lsvAssignedTeam.Items.Count > 1;
            System.Web.UI.HtmlControls.HtmlTableCell thUnAssign = (System.Web.UI.HtmlControls.HtmlTableCell)lsvAssignedTeam.FindControl("thUnAssign");
            if (thUnAssign != null)
                thUnAssign.Visible = Isdelete;
            
            foreach (ListViewDataItem list in lsvAssignedTeam.Items)
            {
                
                    System.Web.UI.HtmlControls.HtmlTableCell tdUnAssign = (System.Web.UI.HtmlControls.HtmlTableCell)list.FindControl("tdUnAssign");
                    if (tdUnAssign != null)
                        tdUnAssign.Visible = Isdelete;
  
            }
            //lsvAssignedTeam.DataBind();
            IList<int> selectedemp = getAssignedRecruitersFromList();
            if (selectedemp.Count == 0)
            {
                lsvAssignedTeam.DataSource = null;
                lsvAssignedTeam.DataBind();
            }



            try
            {

                foreach (ListViewDataItem item in lsvAssignedTeam.Items)
                {
                    HiddenField hdnMemberID = (HiddenField)item.FindControl("hdnMemberID");

                    if (hdnMemberID.Value == "")
                    {
                        lsvAssignedTeam.Items.Remove(item);
                        lsvAssignedTeam.DataBind();
                    }
                }
            }
            catch(Exception ex)
            {
                PopulateHiringTeamMembers(CurrentJobPosting);
            }

        }
        protected void lsvAssignedTeam_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            bool _isHiringManager = base.IsHiringManager;
            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                JobPostingHiringTeam jobPostingHiringTeam = ((ListViewDataItem)e.Item).DataItem as JobPostingHiringTeam;

                if (jobPostingHiringTeam != null)
                {
                    HiddenField hdnMemberID = (HiddenField)e.Item.FindControl("hdnMemberID");
                    Label lblMemberName = (Label)e.Item.FindControl("lblMemberName");
                    Label lblMemberEmail = (Label)e.Item.FindControl("lblMemberEmail");
                    ImageButton btnDelete = (ImageButton)e.Item.FindControl("btnDelete");

                    if (jobPostingHiringTeam.MemberId > 0)
                    {
                        //  if (jobPostingHiringTeam.EmployeeType == EmployeeType.Internal.ToString())
                        {
                            hdnMemberID.Value = jobPostingHiringTeam.MemberId.ToString();
                            /*lblMemberName.Text = jobPostingHiringTeam .Name ;
                            //lblMemberEmail.Text = jobPostingHiringTeam .PrimaryEmail ;*/
                            char[] delimiter = { '[', ']' };
                            string[] name = jobPostingHiringTeam.Name.Split(delimiter, StringSplitOptions.RemoveEmptyEntries);

                            if (name.Length == 2)
                            {
                                lblMemberName.Text = name[0];
                                lblMemberEmail.Text = name[1];
                            }
                            else
                            {
                                lblMemberName.Text = jobPostingHiringTeam.Name;
                                lblMemberEmail.Text = jobPostingHiringTeam.PrimaryEmail;
                            }
                        }
                        if (jobPostingHiringTeam.MemberId == CurrentJobPosting.CreatorId)
                        {
                            btnDelete.Visible = false;

                        }
                        else
                        {
                            Isdelete = true;
                            btnDelete.Visible = true;
                        }
                        if (CurrentJobPostingId == 0)
                        {
                            if (jobPostingHiringTeam.MemberId == CurrentMember.Id) btnDelete.Visible = false;

                        }
                    }
                    // btnDelete.OnClientClick = "return ConfirmDelete('Hiring Team Member')";
                    // btnDelete.CommandArgument = StringHelper.Convert(jobPostingHiringTeam.Id);
                }
            }
        }

        public IList<int> getAssignedRecruitersFromList()
        {
            IList<int> ReqIds = new List<int>();
            //ReqIds.Add(CurrentMember.Id);

          
            foreach (ListViewDataItem item in lsvAssignedTeam.Items)
            {
                HiddenField hdnMemberID = (HiddenField)item.FindControl("hdnMemberID");
                if (hdnMemberID.Value != "")
                {
                    if (!ReqIds.Contains(Convert.ToInt32(hdnMemberID.Value)))
                        ReqIds.Add(Convert.ToInt32(hdnMemberID.Value));
                }

            }
           
            return ReqIds;

        }
        public IList<JobPostingHiringTeam> getHiringTeam()
        {
            IList<JobPostingHiringTeam> team = new List<JobPostingHiringTeam>();
            bool iscurrentmemberexists = false;
            foreach (ListViewDataItem item in lsvAssignedTeam.Items)
            {
                HiddenField hdnMemberID = (HiddenField)item.FindControl("hdnMemberID");
                Label lblMemberName = (Label)item.FindControl("lblMemberName");
                Label lblMemberEmail = (Label)item.FindControl("lblMemberEmail");

                JobPostingHiringTeam te = new JobPostingHiringTeam();
                if (hdnMemberID.Value != "")
                {
                    te.MemberId = Convert.ToInt32(hdnMemberID.Value);
                    if (te.MemberId == CurrentMember.Id) iscurrentmemberexists = true;
                    te.Name = lblMemberName.Text;
                    te.PrimaryEmail = lblMemberEmail.Text;
                    team.Add(te);
                }
                /*else {
                    lsvAssignedTeam.Items.Remove(item);
                    lsvAssignedTeam.DataBind();
                }*/

            }

            if (!iscurrentmemberexists)
            {
                JobPostingHiringTeam te = new JobPostingHiringTeam();
                te.MemberId = CurrentMember.Id;
                te.Name = CurrentMember.FirstName + " " + CurrentMember.LastName;
                te.PrimaryEmail = CurrentMember.PrimaryEmail;
                team.Add(te);
            }
            if (team != null) 
            {
                lsvAssignedTeam.DataSource = team;
                lsvAssignedTeam.DataBind();

            }
            
            return team;
        }

        #endregion
    }
}
