﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VendorPortalAccess.ascx.cs"
    Inherits="TPS360.Web.UI.Controls_VendorPortalAccess" %>
<%@ Register Src="~/Controls/ConfirmationWindow.ascx" TagName="Confirm" TagPrefix="ucl" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Controls/ChangePasswordforVendor.ascx" TagName="Password" TagPrefix="ucl" %>
<%@ Register Src="~/Controls/PagerControl.ascx" TagName="Pager" TagPrefix="ucl" %>
<ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender" runat="server" BackgroundCssClass="modalBackground"
    DropShadow="false" PopupControlID="lgnPanel" PopupDragHandleControlID="uclPassword"
    TargetControlID="lblNotes" BehaviorID="MPE">
</ajaxToolkit:ModalPopupExtender>
<style>
    .dropdown-menu:after
    {
        border-bottom: 7px solid #FFFFFF;
        border-left: 7px solid transparent;
        border-right: 7px solid transparent;
        content: "";
        display: inline-block;
        left: 80%;
        position: absolute;
        top: -6px;
    }
    .dropdown-menu::before
    {
        position: absolute;
        top: -7px;
        left: 80%;
        display: inline-block;
        border-right: 7px solid transparent;
        border-bottom: 7px solid #CCC;
        border-left: 7px solid transparent;
        border-bottom-color: rgba(0, 0, 0, 0.2);
        content: '';
    }
</style>

<script src="../Scripts/fixWebkit.js" type="text/javascript"></script>

<script language="javascript" src="../js/AjaxScript.js" type="text/javascript">
<script type ="text/javascript" >
 	      
</script>

<div>
    <div>
        <asp:Panel ID="lgnPanel" runat="server" CssClass="modalPopup" Style="display: none;"
            Width="400px" Height="250px">
            <ucl:Password ID="uclPassword" runat="server" />
            <div class="TableRow" style="text-align: center">
                <div id="dv" runat="server" visible="false">
                    <ucl:Confirm ID="uclConfirm" runat="server"></ucl:Confirm>
                </div>
                <asp:HiddenField ID="hdfFromMemberId" runat="server" Value="0" />
                <asp:HiddenField ID="hdfToMemberId" runat="server" Value="0" />
            </div>
        </asp:Panel>
    </div>
    <asp:TextBox ID="txtSortColumn" runat="server" Visible="false"></asp:TextBox>
    <asp:TextBox ID="txtSortOrder" runat="server" Visible="false"></asp:TextBox>
    <asp:TextBox ID="SortColumn" runat="server" EnableViewState="true" Visible="false" />
    <asp:TextBox ID="SortOrder" runat="server" EnableViewState="true" Visible="false" />
    <asp:ObjectDataSource ID="odsContactList" runat="server" SelectMethod="GetPaged"
        TypeName="TPS360.Web.UI.CompanyConatctDataSource" SelectCountMethod="GetListCount"
        EnablePaging="True" SortParameterName="sortExpression">
        <SelectParameters>
            <asp:Parameter Name="CompanyId" DefaultValue="0" />
            <asp:Parameter Name="SortOrder" DefaultValue="asc" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <div class="TabPanelHeader">
        <asp:Label ID="lblTitle" runat="server"></asp:Label>
    </div>
    <div class="TableRow well" style="text-align: center; margin-bottom: 0px;">
        <asp:Label ID="lblMessage" EnableViewState="False" runat="server"></asp:Label>
        <asp:Label ID="lblNotes" runat="server"></asp:Label>
        <asp:Label ID="lblNote" runat="server"></asp:Label>
    </div>
    <asp:Panel ID="pnlMaster" runat="server">
        <br />
        <asp:Panel ID="pnlGrid" runat="server">
            <div class="GridContainer">
                <div>
                    <asp:Label ID="lblGrant" runat="server"></asp:Label>
                </div>
                <asp:ListView ID="lsvVendorPortalAccess" runat="server" DataKeyNames="Id" OnItemDataBound="lsvVendorPortalAccess_ItemDataBound"
                    OnItemCommand="lsvVendorPortalAccess_ItemCommand" OnPreRender="lsvVendorPortalAccess_PreRender">
                    <LayoutTemplate>
                        <table id="tlbTemplate" runat="server" class="Grid" cellspacing="0" border="0">
                            <tr>
                                <th>
                                    <asp:LinkButton ID="btnName" runat="server" ToolTip="Sort By Contact Name" CommandName="Sort"
                                        CommandArgument="FirstName" Text="Contact Name" />
                                </th>
                                <th>
                                    <asp:LinkButton ID="btnEmail" runat="server" ToolTip="Sort By Email" CommandName="Sort"
                                        CommandArgument="Email" Text="Email/Login ID" />
                                </th>
                                <th style="width: 100px !important">
                                    Portal Access
                                </th>
                                <th style="width: 55px !important">
                                    Action
                                </th>
                            </tr>
                            <tr id="itemPlaceholder" runat="server">
                            </tr>
                            <tr class="Pager">
                                <td colspan="4">
                                    <ucl:Pager ID="pagerControl" runat="server" EnableViewState="true" />
                                </td>
                            </tr>
                        </table>
                    </LayoutTemplate>
                    <EmptyDataTemplate>
                        <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                            <tr>
                                <td>
                                    No eligible contacts found. To grant access, first create a contact with an email
                                    address supplied.
                                </td>
                            </tr>
                        </table>
                    </EmptyDataTemplate>
                    <ItemTemplate>
                        <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                            <td>
                                <asp:Label ID="lblContactName" runat="server" />
                            </td>
                            <td>
                                <asp:Label ID="lblEmail" runat="server" />
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlAccess" runat="server" CssClass="CommonDropDownList" AutoPostBack="false"
                                    Width="100px">
                                    <asp:ListItem Value="0">Blocked</asp:ListItem>
                                    <asp:ListItem Value="1">Enabled</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td style="overflow: inherit;">
                                <ul class="nav" style="margin-bottom: 0px; padding-left: 0px; display: none;" runat="server"
                                    id="ulOptions">
                                    <li class="dropdown"><a data-toggle="dropdown" class="dropdown-toggle" style="width: 30px;
                                        margin-top: -15px;" href="#">
                                        <div class="container" style="padding: 0px;">
                                            <button class="btn btn-mini" type="button">
                                                <i class="icon icon-cog"></i><span class="caret"></span>
                                            </button>
                                        </div>
                                    </a>
                                        <ul class="dropdown-menu" style="margin-left: -115px; margin-top: -23px;">
                                            <li>
                                                <asp:LinkButton ID="lnkSendAccessEmail" runat="server" CommandName="AccessEmail"
                                                    Text="Send Access Email"></asp:LinkButton>
                                            </li>
                                            <li>
                                                <asp:LinkButton ID="lnkChangePassword" runat="server" CommandName="Password" Text="Change Password"></asp:LinkButton>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:ListView>
            </div>
        </asp:Panel>
    </asp:Panel>
</div>
