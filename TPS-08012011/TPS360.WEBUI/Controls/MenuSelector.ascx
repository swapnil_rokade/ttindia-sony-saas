﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MenuSelector.ascx.cs" Inherits="TPS360.Web.UI.MenuSelector" %>

<script language="javascript" type="text/javascript">
// Handle tree view click
    function OnTreeNodeChecked(ent)
    {
    try{
        var element = ent.srcElement? ent.srcElement:ent.target;
        }
        catch (e)
        {
        
        }
    try {
    if(element !=null )
    {
        if (element.tagName == "INPUT" && element.type == "checkbox")
        {
        
            var checkedState = element.checked;
           
            while (element.tagName != "TABLE") // Get wrapping table
            {
             
                element = element.parentNode;
            }
            
            CheckParents(element); // Check all parents
            
            element = element.nextSibling; //Return first of the sibling.In this case <DIV>
            
            if (element == null) // If no childrens than exit
                return;
            
            var childTables = element.getElementsByTagName("TABLE");
            for (var tableIndex = 0; tableIndex < childTables.length; tableIndex++)
            {
                CheckTable(childTables[tableIndex], checkedState);
            }
        }
        }
        }
        catch (e)
        {
        }
    }

    // Uncheck the parents of the given table, Can remove the recurse (redundant)
    function UnCheckParents(table)
    {try{
        if (table == null || table.rows[0].cells.length == 2) // This is the root
        {
            return;
        }
        
        var parentTable = table.parentElement.previousSibling;
        CheckTable(parentTable, false);
        UnCheckParents(parentTable);
        }
        catch (e)
        {
        }
    }
    
     // Check the parents of the given table, Can remove the recurse (redundant)
    function CheckParents(table)
    {
    try {
        if (table == null || table.rows[0].cells.length == 2) // This is the root
        {
            return;
        }
        
        var parentTable = table.parentNode.previousSibling;
        CheckTable(parentTable, true);
        }
        catch (e)
        {
        }
        //UnCheckParents(parentTable);
    }

    // Handle the set of checkbox checked state
    function CheckTable(table, checked)
    {
    try{
        var checkboxIndex = table.rows[0].cells.length - 1;
        var cell = table.rows[0].cells[checkboxIndex];
        var checkboxes = cell.getElementsByTagName("INPUT");
        if (checkboxes.length == 1)
        {
            checkboxes[0].checked = checked;
        }
        }
        catch (e)
        {
        }
    }
</script>
<asp:SiteMapDataSource ID="SiteMapDataSource1" runat="server" SiteMapProvider="SqlSiteMaps" ShowStartingNode="false" />
<asp:TreeView SkinID="TreeViewSiteMap"
    ID="treeViewSiteMap"
    ExpandImageUrl="../Images/CollaspeDown.gif"
    CollapseImageUrl="../Images/ExpandUp.gif"
    ExpandDepth="0"
    runat="server"
    ShowCheckBoxes="All"
    OnTreeNodeDataBound="TreeView1_TreeNodeDataBound"
    onclick="OnTreeNodeChecked(event);"
    PopulateNodesFromClient="false">
</asp:TreeView>