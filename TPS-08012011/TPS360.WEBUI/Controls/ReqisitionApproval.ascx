﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ReqisitionApproval.ascx.cs" Inherits="Controls_ReqisitionApproval" %>
<%@ Register Src="~/Controls/DateRangePicker.ascx" TagName="DateRangePicker" TagPrefix="ucl" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Controls/PagerControl.ascx" TagName="Pager" TagPrefix="ucl" %>
<%@ Register Src="~/Controls/ModalTemplate.ascx" TagName="Template" TagPrefix="ucl" %>
<%@ Register Src="~/Controls/RequisitionAssignedManagers.ascx" TagName="Managers"
    TagPrefix="ucl" %>
<%@ Register Src="~/Controls/CountryStatePicker.ascx" TagName="CountryState" TagPrefix="ucl" %>
<%@ Register Src ="~/Controls/MultipleItemPicker.ascx" TagName ="MultipleSelection" TagPrefix="ucl" %>
<style>
    .dropdown-menu:after
    {
        border-bottom: 7px solid  #FFFFFF;
        border-left: 7px solid transparent;
        border-right: 7px solid transparent;
        content: "";
        display: inline-block;
        left: 80%;
        position: absolute;
        top: -6px;
    }
    .dropdown-menu::before
    {
        position: absolute;
        top: -7px;
        left: 80%;
        display: inline-block;
        border-right: 7px solid transparent;
        border-bottom: 7px solid #CCC;
        border-left: 7px solid transparent;
        border-bottom-color: rgba(0, 0, 0, 0.2);
        content: '';
    }
    .pnlHide
    {
    }
    .pnlHide:first-child
    {
        overflow: hidden;
    }
</style>

<script type="text/javascript" language="javascript" src="../js/AjaxVariables.js"></script>

<script type="text/javascript" language="javascript" src="../js/AjaxScript.js"></script>

<script type="text/javascript">
    document.body.onclick = function(e) {
        $('#ctl00_cphHomeMaster_uclRequisition_pnlSearchBoxContent').css({ 'overflow-y': 'inherit', 'overflow-x': 'inherit' });
        $('#ctl00_cphHomeMaster_uclRequisition_pnlSearchBoxContent div:first').css({ 'overflow-y': 'inherit', 'overflow-x': 'inherit' });
    }
    //To Keep the scroll bar in the Exact Place
    Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(PageLoadedReq);
    function PageLoadedReq(sender, args) {
        try {
            var hdnScroll = document.getElementById('ctl00_cphHomeMaster_uclRequisition_hdnScrollPos');
            var bigDiv = document.getElementById('bigDiv');
            bigDiv.scrollLeft = hdnScroll.value;
            var background = $find("mpeModal")._backgroundElement;
            background.onclick = function() {
                CloseModal();
            }
        }
        catch (e) {
        }
    }
    function onShownManagers() {
        var background = $find("MPEManagers")._backgroundElement;
        background.onclick = function() { $find("MPEManagers").hide(); }
    }
    function SetScrollPosition() {
        var hdnScroll = document.getElementById('ctl00_cphHomeMaster_uclRequisition_hdnScrollPos');
        var bigDiv = document.getElementById('bigDiv');
        hdnScroll.value = bigDiv.scrollLeft;
    }


    function RefreshList() {
        var hdnDoPostPack = document.getElementById('<%=hdnDoPostPack.ClientID %>');
        hdnDoPostPack.value = "1";
        __doPostBack('ctl00_cphHomeMaster_uclRequisition_upcandidateList', ''); return false;
    }
    $('#ctl00_cphHomeMaster_uclRequisition_lsvJobPosting_lnkNoOfOpening').mouseover(function() {
        //$('#ctl00_cphHomeMaster_uclRequisition_lsvJobPosting_lnkNoOfOpening').append('<div>Handler for .mouseover() called.</div>');
        console.log('asdsaweqw');
        alert('asdasd');
    });

</script>

<asp:HiddenField ID="hdnScrollPos" runat="server" />
<asp:ObjectDataSource ID="odsRequisitionList" runat="server" SelectMethod="GetPagedApprovalRequisitionList"
    OnSelecting="odsRequisitionList_Selecting" TypeName="TPS360.Web.UI.JobPostingDataSource"
    SelectCountMethod="GetApprovalRequisitionListCount" EnablePaging="True" SortParameterName="sortExpression">
    <SelectParameters>
        <asp:Parameter Name="IsTemplate" DefaultValue="False" Type="Boolean" Direction="Input" />
        <asp:Parameter Name="memberId" />
        <asp:Parameter Name="IsCompanyContact" Type="Boolean" DefaultValue="False" />
        <asp:Parameter Name="CompanyContactId" Type="Int32" DefaultValue="0" />
        <asp:Parameter Name="IsTowerHead" Type="Boolean" DefaultValue="True" />   
        <asp:Parameter Name="IsAdmin" Type="Boolean" DefaultValue="False" />       
    </SelectParameters>
</asp:ObjectDataSource>
<br />
<div>
    <asp:UpdatePanel ID="up" runat="server">
        <ContentTemplate>
            <ajaxToolkit:AlwaysVisibleControlExtender ID="avceMessage" runat="server" TargetControlID="lblMessage">
            </ajaxToolkit:AlwaysVisibleControlExtender>
            <asp:Label ID="lblMessage" runat="server" EnableViewState="false"></asp:Label>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>
<asp:UpdatePanel ID="upcandidateList" runat="server">
    <ContentTemplate>
        <asp:HiddenField ID="hdnDoPostPack" runat="server" />
        <asp:HiddenField ID="hdnSortColumn" runat="server" />
        <asp:HiddenField ID="hdnSortOrder" runat="server" />
        <div class="GridContainer">
            <div style="overflow: inherit" id="bigDiv" onscroll='SetScrollPosition()'>
                <asp:ListView ID="lsvJobPosting" runat="server" EnableViewState="true" DataKeyNames="Id"
                    OnItemDataBound="lsvJobPosting_ItemDataBound" OnItemCommand="lsvJobPosting_ItemCommand"
                    OnPreRender="lsvJobPosting_PreRender" DataSourceID="odsRequisitionList">
                    <LayoutTemplate>
                        <table id="tlbTemplate" runat="server" class="Grid" cellspacing="0" border="0">
                            <tr id="trHeadLevel" runat="server">                            
                                <th runat="server" id="thReqCode" style="white-space: nowrap; width: 70px !important">
                                    <asp:LinkButton ID="lnkReqCode" runat="server" CommandName="Sort" ToolTip="Sort By Req Code"
                                        CommandArgument="[J].[JobPostingCode]" Text="Req Code" TabIndex="3" />
                                </th>
                                <th style="white-space: nowrap; min-width: 100px; width:170px">
                                    <asp:LinkButton ID="btnJobTitle" runat="server" CommandName="Sort" ToolTip="Sort By Requisition Job Title"
                                        CommandArgument="[J].[JobTitle]" Text="Requisition Job Title" TabIndex="6" />
                                </th>
                             <%--   <th  id="thNoOfOpening" runat="server" style="width: 110px !important">
                                    <asp:LinkButton ID="lnkNoOfOpening" runat="server" CommandName="Sort" ToolTip="No of Openings"
                                       Text="No of Openings" CommandArgument="[J].NoOfOpenings" TabIndex="8" />
                                </th>--%>
                                <th style="white-space: nowrap;" runat="server" id="th2">
                                    Demand Type
                                </th>
                                <th style="white-space: nowrap;" runat="server" id="th3">
                                    Priority
                                </th>
                                <th style="white-space: nowrap;width:120px" runat="server" id="th4">
                                    Organization Unit
                                </th>
                                <th style="white-space: nowrap;" runat="server" id="th5">
                                    Submitted By
                                </th>
                                <th style="white-space: nowrap; width: 125px !important">
                                    <asp:LinkButton ID="btnPostedDate" runat="server" CommandName="Sort" ToolTip="Sort By Submitted On"
                                        CommandArgument="[J].[PostedDate]" Text="Submitted On" TabIndex="2" />
                                </th>
                                <th style="white-space: nowrap;" runat="server" id="th6">
                                    Updated On
                                </th>                                                                                           
                                <th style="white-space: nowrap;" runat="server" id="th1">
                                    Stage
                                </th>
                                 <th style="white-space: nowrap; min-width: 30px; width: 100px !important" >
                                    <asp:Label ID="lblRequisitionStatus"  runat="server" Text="Requisition Status"  ToolTip="Requisition Status"/> 
                                </th>
                                <th style="text-align: center; white-space: nowrap; width: 45px !important;" id="thAction"
                                    runat="server">
                                    Action
                                </th>
                            </tr>
                            <tr>
                                <td colspan="8" style="padding: 0px 0px 0px 0px; border-style: none none none none">
                                </td>
                            </tr>
                            <tr id="itemPlaceholder" runat="server">
                            </tr>
                            <tr class="Pager">
                                <td colspan="10" runat="server" id="tdPager">
                                    <ucl:Pager ID="pagerControl" runat="server" EnableViewState="true" />
                                </td>
                            </tr>
                        </table>
                    </LayoutTemplate>
                    <EmptyDataTemplate>
                        <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                            <tr>
                                <td>
                                    No data was returned.
                                </td>
                            </tr>
                        </table>
                    </EmptyDataTemplate>
                    <ItemTemplate>
                        <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                            <td id="tdJobCode" runat="server">
                                <asp:Label ID="lblJobCode" runat="server" />
                            </td>
                            <td>
                                <asp:HyperLink ID="lnkJobTitle" runat="server" Target="_blank" TabIndex="8" Style="cursor: pointer;"></asp:HyperLink>
                            </td>
                          <%--   <td >
                                <asp:Label ID="lblNoOfOpenings"  runat="server" />
                            </td>  --%>                       
                            <td >
                                <asp:Label ID="lblDemandType" runat="server" />
                            </td>
                            <td >
                                <asp:Label ID="lblPriority" runat="server" />
                            </td>
                            <td >
                                <asp:Label ID="lblOrganizationUnit" runat="server" />
                            </td>
                            <td >
                                <asp:Label ID="lblPostedBy" runat="server" />
                            </td>
                            <td>
                                <asp:Label ID="lblPostedDate" runat="server" Width="80px" />
                            </td>
                            <td>
                                <asp:Label ID="lblUpdatedOn" runat="server" Width="80px" />
                            </td>
                            <td runat="server" id="tdApprovalStatus">
                                <asp:Label ID="lblApprovalStatus" runat="server" />
                            </td>   
                            <td>
                                <asp:Label ID="lblJobStatus" runat="server" />                               
                            </td>                            
                                                                                   
                            <td style="overflow: inherit;" runat="server" id="tdAction" enableviewstate="true">
                                <ul class="nav" style="margin-bottom: 0px; padding-left: 0px;">
                                    <li class="dropdown"><a data-toggle="dropdown" class="dropdown-toggle" style="width: 30px;
                                        margin-top: -15px;" href="#">
                                        <div class="container">
                                            <button class="btn btn-mini" type="button">
                                                <i class="icon icon-cog"></i><span class="caret"></span>
                                            </button>
                                        </div>
                                    </a>
                                        <ul class="dropdown-menu" style="margin-left: -120px; margin-top: -25px;">
                                            <li>
                                                <asp:HyperLink ID="hlkReqApprove" runat="server"  Text="Approve"></asp:HyperLink>
                                            </li>
                                            <li>
                                                <asp:HyperLink ID="hlkReqReject" runat="server"  Text="Send Back"></asp:HyperLink>
                                            </li>                                           
                                        </ul>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:ListView>
            </div>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
