﻿/*******************
Start
Suraj Adsule
For Candidate Joining Form
20160804*********/
using System;
using System.Web.UI;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using TPS360.Web.UI;
using TPS360.BusinessFacade;
using TPS360.Common.BusinessEntities;
using TPS360.Web.UI;
using TPS360.Common.Shared;

public partial class Controls_CandidateJoiningJobDetails : CandidateBaseControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        cvDoJ.ValueToCompare = DateTime.Now.ToShortDateString();
        if (!IsPostBack)
        {
            PrepareView();
        }
    }
    private void PrepareView()
    {
        JobPosting JobPostingDetails = Facade.GetJobPostingById(Convert.ToInt32(Request.QueryString["JID"]));
        lblDemandIdValue.Text = JobPostingDetails.JobPostingCode;
        lblRequisitionTitleValue.Text = JobPostingDetails.JobTitle;

        MiscUtil.PopulateBillability(ddlJobRestriction, Facade);
        ViewState.Add("SelectedValue", ddlJobRestriction.SelectedValue);

        MiscUtil.PopulateCompensationPlan(ddlCompensationPlan, Facade);
        ddlCompensationPlan.SelectedValue = ddlCompensationPlan.Items.FindByText("Salary Plan").Value;

        MiscUtil.PopulatePayGroup(ddlPayGroup, Facade);
        MiscUtil.PopulatePayGroupGrade(ddlPayGroupGrade, Facade);

        MiscUtil.PopulateCurrency(ddlCurrency, Facade);
        ddlCurrency.SelectedValue = ddlCurrency.Items.FindByText("INR").Value;

        MiscUtil.PopulateFrequency(ddlFrequency, Facade);
        ddlFrequency.SelectedValue = ddlFrequency.Items.FindByText("Annual").Value;

        MiscUtil.PopulateEmployeeType(ddlEmployeeType, Facade);
        MiscUtil.PopulateWorkerSubType(ddlWorkerSubType, Facade);
        ddlWorkerSubType.SelectedValue = ddlWorkerSubType.Items.FindByText("Agency").Value;

        txtMaximumFTE.Text = "100";
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        string CandidateId = Request.QueryString["CanId"];
        CandidateId.TrimStart(',');
        string JobPostingId = Request.QueryString["JID"];
        string StatusId = Request.QueryString["StatusId"];
        JobPosting JobPostingDetails = Facade.GetJobPostingById(Convert.ToInt32(JobPostingId));

        CandidateJoiningDetails candidateJoiningDetail = new CandidateJoiningDetails()
        {
            DemandID = Convert.ToInt32(JobPostingDetails.Id),
            CandidateID = Convert.ToString(CandidateId),
            JobRestrictions = Convert.ToInt32(ddlJobRestriction.SelectedValue),
            EmployeeType = Convert.ToInt32(ddlEmployeeType.SelectedValue),
            RequisitionTitle = JobPostingDetails.JobTitle,
            DateOfJoining = Convert.ToDateTime(wdcOpenDate.Value),
            WorkerSubType = Convert.ToInt32(ddlWorkerSubType.SelectedValue),
            PayGroup = Convert.ToInt32(ddlPayGroup.SelectedValue),
            Currency = Convert.ToInt32(ddlCurrency.SelectedValue),
            CompensationPlan = Convert.ToInt32(ddlCompensationPlan.SelectedValue),
            PayGroupGrade = Convert.ToInt32(ddlPayGroupGrade.SelectedValue),
            Frequency = Convert.ToInt32(ddlFrequency.Text),
            MaximumFTE = Convert.ToInt32(txtMaximumFTE.Text),
            Notes = txtNotes.Text,
            CreatorId = CurrentMember.Id,
            UpdatorId = CurrentMember.Id,
        };
        Facade.AddCandidateJoiningForm(candidateJoiningDetail);

        //*******************Suraj Adsule******Start********For Candidate Joining Form********20160812*********//
        Facade.MemberJobCart_MoveToNextLevel(0, CurrentMember.Id, CandidateId, Convert.ToInt32(JobPostingId), StatusId);
        HiringMatrixLevels l = Facade.GetHiringMatrixLevelsById(Convert.ToInt32(StatusId));
        Facade.UpdateCandidateRequisitionStatus(Convert.ToInt32(JobPostingId), CandidateId, base.CurrentMember.Id, Convert.ToInt32(StatusId));//Convert.ToInt32(hfStatusId.Value)
        MiscUtil.EventLogForCandidateStatus(EventLogForCandidate.CandidateStatusChange, Convert.ToInt32(JobPostingId), CandidateId, CurrentMember.Id, "", Facade);
        ScriptManager.RegisterClientScriptBlock(lblMessage, typeof(MiscUtil), "Show", "parent.ModalOperationCompleted('Selected candidate(s) moved Successfully.');", true);
        //*******************Suraj Adsule******Start********For Candidate Joining Form********20160812*********//
    }
}
