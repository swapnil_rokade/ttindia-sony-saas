﻿
<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DateRangePicker.ascx.cs"
    Inherits="TPS360.Web.UI.DateRangePicker" %>

  <link href ="../assets/css/daterangepicker.css"  type ="text/css" rel ="Stylesheet" />
  
  <asp:HiddenField ID="hdnDateTimeFormat" runat ="server" />
  <div style =" display : inline-block ;width : 200px; white-space : normal " >
  <div id="reportrange" runat ="server"    style="background: #fff; cursor: pointer; padding: 5px 5px; border : 1px solid #CCCCCC; width : 175px; border-radius: 3px;">
    <i class="icon-calendar icon-large"></i>
   <asp:Label ID="lblDateRange" runat ="server" Text ="Select Date Range" EnableViewState ="true" > </asp:Label>
   <asp:HiddenField ID="hdnDateRange"  runat ="server" Value ="Select Date Range" />
    <b class="caret" style =" float :right; margin-top : 8px; opacity:1 "></b>
</div>
</div> 

    <script type="text/javascript">
Sys.Application.add_load(function() { 

$('#<%= reportrange.ClientID %>').daterangepicker(
    {
        format: 'MM/dd/yyyy',
        ranges: {
            'Today': ['today', 'today'],
            'Yesterday': ['yesterday', 'yesterday'],
            'Last 7 Days': [Date.today().add({ days: -6 }), 'today'],
            'Last 30 Days': [Date.today().add({ days: -29 }), 'today'],
            'This Month': [Date.today().moveToFirstDayOfMonth(), Date.today().moveToLastDayOfMonth()],
            'Last Month': [Date.today().moveToFirstDayOfMonth().add({ months: -1 }), Date.today().moveToFirstDayOfMonth().add({ days: -1 })],
            'This Year':[Date.today().set({day:1}).set({month:0}),'today']
        }
    }, 
    
    
    function(start, end) {
        $('#<%= lblDateRange.ClientID %>').html(start.toString($('#<%= hdnDateTimeFormat.ClientID %>').val()) + ' - ' + end.toString($('#<%= hdnDateTimeFormat.ClientID %>').val()));
        $('#<%= hdnDateRange.ClientID %>').val(start.toString($('#<%= hdnDateTimeFormat.ClientID %>').val()) + ' - ' + end.toString($('#<%= hdnDateTimeFormat.ClientID %>').val()));
    }
);


});
</script>