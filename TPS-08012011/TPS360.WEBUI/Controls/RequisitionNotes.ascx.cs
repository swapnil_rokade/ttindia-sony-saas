﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using TPS360.Common.BusinessEntities;
using TPS360.Web.UI;
using TPS360.Common.Shared;
using System.Collections.Generic;
using TPS360.Web.UI.Helper;
public partial class RequisitionNotes : ATSBaseControl
{

    private int _JobPostingID = 0;
    public int JobPostingId
    {
        get
        {
            if (CurrentJobPostingId == 0) return Convert .ToInt32 (hdnCurrentJobPostingId.Value ) ;
            else return CurrentJobPostingId;
        }
        set
        {
            _JobPostingID = value;
            hdnCurrentJobPostingId.Value = value.ToString ();
            Bind();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
       
        if (!IsPostBack)
        {
            txtAdditionalNote.Focus();
            hdnSortColumn.Value = "btnDate";
            hdnSortOrder.Value = "DESC";

           
            Label lbModalTitle = (Label)this.Page.Master.FindControl("lbModalTitle");
            if (lbModalTitle != null) lbModalTitle.Text = Facade.GetJobPostingById (JobPostingId ).JobTitle + " - Notes";
        }

       
       
    }

    protected void lsvAddedNote_PreRender(object sender, EventArgs e)
    {
             ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvAddedNote.FindControl("pager");
             if (PagerControl != null)
             {

                 DataPager pager = (DataPager)PagerControl.FindControl("pager");
                 if (pager != null)
                 {
                     DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                     if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
                 }
                 if (hdnRowPerPageName != null)
                 {
                     hdnRowPerPageName.Value = "ReqNotes";
                 }
             }
        PlaceUpDownArrow();
    }


    private void PlaceUpDownArrow()
    {
        try
        {
            LinkButton lnk = (LinkButton)lsvAddedNote.FindControl(hdnSortColumn.Value);
            HtmlTableCell im = (HtmlTableCell)lnk.Parent;
            im.EnableViewState = false;
            im.Attributes.Add("class", (hdnSortOrder.Value  == "ASC" ? "Ascending" : "Descending"));
        }
        catch
        {
        }

    }


    private void PopulateAddedNoteList()
    {
        //lsvAddedNote.DataSource = Facade.GetAllJobPostingNoteByJobPostingId(base.CurrentJobPostingId);
        //lsvAddedNote.SelectParameters["CandidateId"].DefaultValue
        odsReqNotes.SelectParameters["JobPostingId"].DefaultValue = JobPostingId.ToString();
        lsvAddedNote.DataBind();
        string Cookiename = "";
        if (hdnRowPerPageName != null)
        {
            if (Page.Request.Url.AbsoluteUri.ToString().Contains("HiringMatrix.aspx"))
            {
                Cookiename = "ReqNotes";
            }
        }
        try
        {
            string pagesize = "";
            pagesize = (Request.Cookies[Cookiename] == null ? "" : Request.Cookies[Cookiename].Value); ;
            {
                DataPager pager = (DataPager)lsvAddedNote.FindControl("pager");
                if (pager != null) pager.PageSize = Convert.ToInt32(pagesize == "" ? "10" : pagesize);
            }
        }
        catch
        {
        }
    }
    private void SaveNote()
    {
        CommonNote commonNote = new CommonNote();
        commonNote.NoteDetail = MiscUtil.RemoveScript(txtAdditionalNote.Text.Trim());
        commonNote.IsInternal = false;
        commonNote.NoteCategoryLookupId = 0;
        commonNote.CreatorId = base.CurrentMember.Id;
        commonNote.UpdatorId = base.CurrentMember.Id;
        commonNote = Facade.AddCommonNote(commonNote);

        if (commonNote != null)
        {
            JobPostingNote jobPostingNote = new JobPostingNote();
            jobPostingNote.JobPostingId = JobPostingId ;
            jobPostingNote.CommonNoteId = commonNote.Id;

            jobPostingNote = Facade.AddJobPostingNote(jobPostingNote);
        }
        txtAdditionalNote.Text = string.Empty;
    }

    public void Bind()
    {

        lsvAddedNote.DataSourceID = "odsReqNotes";
        PopulateAddedNoteList();
    }

    protected void btnSaveNote_Click(object sender, EventArgs e)
    {
        string message = string.Empty;
        if (!String.IsNullOrEmpty(MiscUtil.RemoveScript(Convert.ToString(txtAdditionalNote.Text))))
        {
            SaveNote();
            PopulateAddedNoteList();
        }
        else
        {
            message = "Please Add Notes.";
            PopulateAddedNoteList();
            MiscUtil.ShowMessage(lblMessage, message, false);
        }

        
    }
    protected void lsvAddedNote_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
        {
            RequisitionNotesEntry ReqNotesEntry = ((ListViewDataItem)e.Item).DataItem as RequisitionNotesEntry;

            if (ReqNotesEntry != null)
            {
                Label lblDateTime = (Label)e.Item.FindControl("lblDateTime");
                Label lblAddedBy = (Label)e.Item.FindControl("lblAddedBy");
                Label lblNote = (Label)e.Item.FindControl("lblNote");
                lblDateTime.Text = ReqNotesEntry.CreateDate.ToShortDateString() +" "+  ReqNotesEntry .CreateDate .ToShortTimeString ();
                lblAddedBy.Text = ReqNotesEntry.CreatorName;
                lblNote.Text = ReqNotesEntry.NoteDetail;
            }
        }
    }

    protected void lsvAddedNote_ItemCommand(object sender, ListViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Sort")
            {
                LinkButton lnkbutton = (LinkButton)e.CommandSource;
                if (hdnSortColumn.Value == lnkbutton.ID)
                {
                    if (hdnSortOrder.Value == "DESC") hdnSortOrder.Value = "ASC";
                    else if (hdnSortOrder.Value == "ASC") hdnSortOrder.Value = "DESC";
                }
                else
                {
                    hdnSortColumn.Value = lnkbutton.ID;
                    hdnSortOrder.Value = "ASC";
                }
                try
                {
                    if (Page.Request.Url.AbsoluteUri.ToString().Contains("HiringMatrix.aspx"))
                    {
                        AjaxControlToolkit.ModalPopupExtender ext = (AjaxControlToolkit.ModalPopupExtender)this.Page.Master.FindControl("cphHomeMaster").FindControl("ucRequisitionHiringMatrixNavigationTopMenu").FindControl("uclHiringMatrix").FindControl("mpeNotes");
                        ext.Enabled = true;
                        ext.Show();
                    }
                }
                catch
                {
                }
            }
        }
        catch { }
    }


    protected void ddlRowPerPage_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataPager pager = (DataPager)lsvAddedNote.FindControl("pager");
        TextBox txtCurrentPage = sender as TextBox;
        DropDownList ddlRowPerPage = sender as DropDownList;
        int startRowIndex = 0;
        pager.PageSize = Convert.ToInt32(hdnSelectedRowPerPage .Value !=""? hdnSelectedRowPerPage .Value : ddlRowPerPage.SelectedValue);
        Response.Cookies[hdnRowPerPageName.Value].Value = pager.PageSize.ToString();
        pager.SetPageProperties(startRowIndex, Convert.ToInt32(hdnSelectedRowPerPage.Value != "" ? hdnSelectedRowPerPage.Value : ddlRowPerPage.SelectedValue), true);
        if (Page.Request.Url.AbsoluteUri.ToString().Contains("HiringMatrix.aspx"))
        {
            AjaxControlToolkit.ModalPopupExtender ext = (AjaxControlToolkit.ModalPopupExtender)this.Page.Master.FindControl("cphHomeMaster").FindControl("ucRequisitionHiringMatrixNavigationTopMenu").FindControl("uclHiringMatrix").FindControl("mpeNotes");
            ext.Enabled = true;
            ext.Show();
        }
    }

    protected void CurrentPageChanged(object sender, EventArgs e)
    {
        DataPager pager = (DataPager)lsvAddedNote.FindControl("pager");
        if (Page.Request.Url.AbsoluteUri.ToString().Contains("HiringMatrix.aspx"))
        {
            AjaxControlToolkit.ModalPopupExtender ext = (AjaxControlToolkit.ModalPopupExtender)this.Page.Master.FindControl("cphHomeMaster").FindControl("ucRequisitionHiringMatrixNavigationTopMenu").FindControl("uclHiringMatrix").FindControl("mpeNotes");
            ext.Enabled = true;
            ext.Show();
        }

        TextBox txtCurrentPage = sender as TextBox;
        txtCurrentPage.Text = hdnPageNumber.Value;
        int selectedpage = 0;
        Int32.TryParse(txtCurrentPage.Text, out selectedpage);

        if (selectedpage > 0 && Convert.ToDouble((pager.TotalRowCount) / Convert.ToDouble(pager.PageSize)) > selectedpage - 1)
        {
            int startRowIndex = (int.Parse(txtCurrentPage.Text) - 1) * pager.MaximumRows;
            pager.SetPageProperties(startRowIndex, pager.MaximumRows, true);

        }
        else
            MiscUtil.ShowMessage(lblMessage, "Please enter valid page number", true);


        if (Page.Request.Url.AbsoluteUri.ToString().Contains("HiringMatrix.aspx"))
        {
            AjaxControlToolkit.ModalPopupExtender ext = (AjaxControlToolkit.ModalPopupExtender)this.Page.Master.FindControl("cphHomeMaster").FindControl("ucRequisitionHiringMatrixNavigationTopMenu").FindControl("uclHiringMatrix").FindControl("mpeNotes");
            ext.Enabled = true;
            ext.Show();
        }
    
    }
    protected void PagerCommand(object sender, DataPagerCommandEventArgs e)
    {

        DataPager pager = e.Item.Pager;
        int newSartRowIndex;

        switch (e.CommandName)
        {
            case "Next":
                {
                    newSartRowIndex = pager.StartRowIndex + pager.MaximumRows >= pager.TotalRowCount ? pager.StartRowIndex : pager.StartRowIndex + pager.MaximumRows;
                    break;
                }
            case "Previous":
                {
                    newSartRowIndex = pager.StartRowIndex - pager.MaximumRows < 0 ? pager.StartRowIndex : pager.StartRowIndex - pager.MaximumRows;
                    break;
                }
            case "Last":
                {
                    newSartRowIndex = ((pager.TotalRowCount / pager.MaximumRows) * pager.MaximumRows) >= pager.TotalRowCount ? (((pager.TotalRowCount / pager.MaximumRows) - 1) * (pager.MaximumRows)) : ((pager.TotalRowCount / pager.MaximumRows) * pager.MaximumRows);
                    break;
                }
            case "First":
                {
                    newSartRowIndex = 0;
                    break;
                }
            default:
                {
                    newSartRowIndex = 0;
                    break;
                }
        }

        e.NewMaximumRows = e.Item.Pager.MaximumRows;
        e.NewStartRowIndex = newSartRowIndex;

        if (Page.Request.Url.AbsoluteUri.ToString().Contains("HiringMatrix.aspx"))
        {
            AjaxControlToolkit.ModalPopupExtender ext = (AjaxControlToolkit.ModalPopupExtender)this.Page.Master.FindControl("cphHomeMaster").FindControl("ucRequisitionHiringMatrixNavigationTopMenu").FindControl("uclHiringMatrix").FindControl("mpeNotes");
            ext.Enabled = true;
            ext.Show();
        }
    }
}
