﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using TPS360.Common.BusinessEntities;
using TPS360.Web.UI.Helper;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using System.Collections;
using System.Collections.Generic;
namespace TPS360.Web.UI
{

    public partial class PrintJob : BaseControl
    {
        private string FromPage
        {
            get
            {
                if (!StringHelper.IsBlank(Helper.Url.SecureUrl["PageFrom"]))
                {
                    return Helper.Url.SecureUrl["PageFrom"].ToString();
                }
                else return string .Empty ;
            }
        }

        private int JobID
        {
            get
            {
                //From Member Profile
                if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_JOB_ID ]))
                {
                    return Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_JOB_ID ]);

                }
                else
                {
                    return 0;
                }
            }

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (JobID > 0)
            {
                string jobtitle="";
                string jobpostingcode="";


                divPrint.InnerHtml = MiscUtil.GetJobDetailTable(JobID, Facade, out jobtitle, out jobpostingcode, FromPage == string.Empty ? "Print" : FromPage, null, CurrentMember.Id);
            }
        }
    }
}