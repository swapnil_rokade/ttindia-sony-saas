﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: OverviewMenu.ascx.cs
    Description: This is the user control page used to display overview menu.
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date             Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1                 May-11-2009      Jagadish            Defect id: 10143; Changes made in method BuildMenu();
-------------------------------------------------------------------------------------------------------------------------------------------       
*/
using System;
using System.Collections;
using System.Web;
using System.Web.UI.WebControls;

using TPS360.Common.Helper;
using TPS360.Web.UI.Helper;
using System.Web.UI.HtmlControls;
using TPS360.Common.BusinessEntities;
using TPS360.Providers;

namespace TPS360.Web.UI
{
    public partial class OverviewMenu : BaseControl
    {
        #region Private variable

        ArrayList _permittedMenuIdList;
        string script = "\n\t<script language='javascript' type='text/javascript'>";
        
        #endregion

        #region Methods

        private bool IsInPermitedMenuList(int menuId)
        {
            if (_permittedMenuIdList == null)
            {
                _permittedMenuIdList = Facade.GetAllMemberPrivilegeIdsByMemberId(CurrentMember.Id);
            }

            if ((_permittedMenuIdList != null) && (_permittedMenuIdList.Count > 0))
            {
                if (_permittedMenuIdList.Contains(menuId))
                {
                    return true;
                }
            }

            return false;
        }

        public int CurrentSiteMapType
        {
            get
            {
                return (int)(ViewState[this.ClientID + "_SiteMapType"] ?? 0);
            }
            set
            {
                ViewState[this.ClientID + "_SiteMapType"] = value;
            }
        }

        public OverviewType OverviewType
        {
            get
            {
                return (OverviewType)(ViewState[this.ClientID + "_OverviewType"] ?? OverviewType.Unknown);
            }
            set
            {
                ViewState[this.ClientID + "_OverviewType"] = value;
            }
        }

        public bool EnablePermissionChecking
        {
            get
            {
                return (bool)(ViewState[this.ClientID + "_EnablePermissionChecking"] ?? true);
            }
            set
            {
                ViewState[this.ClientID + "_EnablePermissionChecking"] = value;
            }
        }

        private void BuildJavaScriptMethods()
        {
            HtmlGenericControl placeHolder = (HtmlGenericControl)divScriptHolder;
            
            string currentNode = Helper.Url.SecureUrl[UrlConstants.PARAM_SITEMAP_ID].ToString();
            
            SqlSiteMapProvider provider = SiteMap.Providers["SqlSiteMap"] as SqlSiteMapProvider;
            SiteMapNode node = provider.FindSiteMapNodeFromKey(currentNode);
            
            placeHolder.InnerHtml = script + "initalizeOverviewTab('" + tabList.ClientID + "', '{0}', '{1}');\n\t</script>\n".Fill("tab" + currentNode, "false");
        }
        private void BuildMenu()
        {
            if (CurrentSiteMapType > 0)
            {
                SiteMapNodeCollection permittedNodeList = new SiteMapNodeCollection();
                
                SiteMapNodeCollection nodeList = MenuSiteMap.Provider.FindSiteMapNodeFromKey(StringHelper.Convert(CurrentSiteMapType)).ChildNodes;

                if (nodeList != null && nodeList.Count > 0)
                {
                    foreach (SiteMapNode node in nodeList)
                    {
                        if (EnablePermissionChecking)
                        {
                            bool IsDefault = Convert.ToBoolean(node["IsDefault"]);

                            if (IsDefault || IsInPermitedMenuList(Convert.ToInt32(node["Id"])))
                            {
                                permittedNodeList.Add(node);
                            }
                        }
                        else
                        {
                            permittedNodeList.Add(node);
                        }
                    }
                }

                rptTopMenu.DataSource = permittedNodeList;
                rptTopMenu.DataBind();
                BuildJavaScriptMethods();
            }
        }

        protected string BuildUrl(string pageUrl)
        {
            pageUrl = "~/" + pageUrl;
            pageUrl = (new SecureUrl(pageUrl)).ToString();
            return pageUrl;
        }

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            BuildMenu();
        }

        protected void rptTopMenu_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (ControlHelper.IsItemDataRow(e.Item.ItemType))
            {
                SiteMapNode siteMapNode = (SiteMapNode)e.Item.DataItem;                

                Label labelMenuName = (Label)e.Item.FindControl("lblMenuName");
                string secondaryMenuName = "tab" + siteMapNode["Id"].ToString();

                HtmlGenericControl li = (HtmlGenericControl)e.Item.FindControl("liId");
                li.Attributes.Add("rel", secondaryMenuName);

                HtmlAnchor linkMenuName = (HtmlAnchor)e.Item.FindControl("lnkMenuName");

                if (siteMapNode.Url.IsNotNullOrEmpty())
                {
                    if (OverviewType == OverviewType.Unknown)
                    {
                        throw new ArgumentNullException("Please provide a view type.");
                    }
                    else if (OverviewType == OverviewType.CandidateOverview || OverviewType == OverviewType.ConsultantOverview || OverviewType == OverviewType.EmployeeOverview)
                    {
                        ControlHelper.SetHtmlAnchor(linkMenuName, BuildUrl(siteMapNode.Url), string.Empty, UrlConstants.PARAM_MEMBER_ID, Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]);
                    }
                    else if (OverviewType == OverviewType.CompanyOverview)
                    {
                        ControlHelper.SetHtmlAnchor(linkMenuName, BuildUrl(siteMapNode.Url), string.Empty, UrlConstants.PARAM_COMPANY_ID, Helper.Url.SecureUrl[UrlConstants.PARAM_COMPANY_ID]);
                    }
                    else if (OverviewType == OverviewType.CampaignOverview)
                    {
                        ControlHelper.SetHtmlAnchor(linkMenuName, BuildUrl(siteMapNode.Url), string.Empty, UrlConstants.PARAM_ID, Helper.Url.SecureUrl[UrlConstants.PARAM_ID]);
                    }                    
                }
                else
                {
                    linkMenuName.Attributes.Add("onClick", "javascript:return false;");
                }                
                labelMenuName.Text = siteMapNode.Title;
            }
        }

        #endregion
    }
}