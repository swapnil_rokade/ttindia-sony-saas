﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using TPS360.Common.BusinessEntities;
using TPS360.Web.UI.Helper;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Web.UI;
using System.Text.RegularExpressions;
using ExpertPdf.HtmlToPdf;
using System.Drawing;
using System.IO;
using System.Text;

namespace TPS360.Web.UI
{
    public partial class AgentSearchTermEditor : RequisitionBaseControl
    {
        #region Member Variables
        public bool isNew = false;
        bool mailsetting = false;
        #endregion

        #region Properties
        public delegate void JobPostingSearchAgentEventHandler(JobPostingSearchAgent searchagent, bool isAdded);
        public event JobPostingSearchAgentEventHandler SearchAgentAdded;

        private string DefaultCountryCode
        {
            get
            {
                return SiteSetting[DefaultSiteSetting.Country.ToString()].ToString();
            }

        }
        #endregion

        #region Methods
        private void PrepareView()
        {
            MiscUtil.PopulateCountry(ddlCountry, Facade);
            ddlCountry.Items[0].Text = "All";
            MiscUtil.PopulateWorkAuthorization(lstWorkStatus, Facade);
            MiscUtil.PopulateCurrency(ddlCurrency, Facade);
            MiscUtil.PopulateEmploymentDuration(ddlJobType, Facade);
            MiscUtil.PopulateEducationQualification(chkEducationList, Facade);
            chkEducationList.Items.RemoveAt(0);
            MiscUtil.PopulateWorkSchedule(ddlWorkSchedule, Facade);

            ControlHelper.SelectListByValue(ddlCurrency, (SiteSetting[DefaultSiteSetting.Currency.ToString()] == null ? "0" : SiteSetting[DefaultSiteSetting.Currency.ToString()].ToString()));
            if (ddlCurrency.SelectedItem.Text == "INR")
            {
                lblSalaryRate.Text = "(Lacs)";
                lblSalaryRate.Visible = true;
            }
            else
            {
                lblSalaryRate.Text = "";
            }
            BuildPaymentTypeDropDownList();
            BuildResumeLastUpdateDropDownList();
            ApplyDefaultsFromSiteSetting();
            LoadHotList();
            LoadApplicantType();
            LoadRequisionList();
        }

        private void LoadHotList()
        {
            int memberGroupCount = Facade.GetMemberGroupCountByGroupType((int)MemberGroupType.Candidate);
            if (memberGroupCount < 500)
            {
                txtSearch_HotList.Visible = false;
                ddlHotList.Visible = true;
                ddlHotList.Items.Clear();
                ddlHotList.DataSource = Facade.GetAllMemberGroup((int)MemberGroupType.Candidate); //0.7
                ddlHotList.DataTextField = "Name";
                ddlHotList.DataValueField = "Id";
                ddlHotList.DataBind();
                ddlHotList = (DropDownList)MiscUtil.RemoveScriptForDropDown(ddlHotList);
                MiscUtil.InsertPleaseSelect(ddlHotList);
            }
            else
            {
                ddlHotList.Visible = false;
                txtSearch_HotList.Visible = true;

            }
        }

        private void LoadApplicantType()
        {
            ddlApplicantType.Items.Clear();
            MiscUtil.PopulateCandidateType(ddlApplicantType, Facade);
        }

        private void LoadRequisionList()
        {
            IList<HiringMatrixLevels> level = Facade.GetAllHiringMatrixLevels();
            chkHiringStatus.DataSource = level;
            chkHiringStatus.DataTextField = "Name";
            chkHiringStatus.DataValueField = "Id";
            chkHiringStatus.DataBind();
        }

        private void BuildPaymentTypeDropDownList()
        {
            ddlPaymentType.Items.Clear();
            ddlPaymentType.Items.Add(new ListItem("Yearly", "4"));
            ddlPaymentType.Items.Add(new ListItem("Monthly", "3"));
            ddlPaymentType.Items.Add(new ListItem("Daily", "2"));
            ddlPaymentType.Items.Add(new ListItem("Hourly", "1"));

        }

        private void BuildResumeLastUpdateDropDownList()
        {
            ddlResumeLastUpdate.Items.Clear();

            ddlResumeLastUpdate.Items.Add(new ListItem("Does not matter", "0"));
            ddlResumeLastUpdate.Items.Add(new ListItem("1 Days", "1"));
            ddlResumeLastUpdate.Items.Add(new ListItem("2 Days", "2"));
            ddlResumeLastUpdate.Items.Add(new ListItem("7 Days", "7"));
            ddlResumeLastUpdate.Items.Add(new ListItem("15 Days", "15"));
            ddlResumeLastUpdate.Items.Add(new ListItem("30 Days", "30"));
            ddlResumeLastUpdate.Items.Add(new ListItem("60 Days", "60"));
            ddlResumeLastUpdate.Items.Add(new ListItem("90 Days", "90"));
            ddlResumeLastUpdate.Items.Add(new ListItem("6 Months", "182"));
            ddlResumeLastUpdate.Items.Add(new ListItem("1 Year", "365"));
        }

        private void ApplyDefaultsFromSiteSetting()
        {
            MiscUtil.PopulateState(ddlState, Convert.ToInt32(ddlCountry.SelectedValue), Facade);   //9630
        }

        public void LoadDefaultSearchTerms()
        {
            if (CurrentJobPostingId > 0)
            {
                string Skills = CurrentJobPosting.JobSkillLookUpId != null ? (MiscUtil.SplitSkillValues(CurrentJobPosting.JobSkillLookUpId, '!', Facade)) : "";
                txtKeyword.Text = "\"" + MiscUtil.RemoveScript(CurrentJobPosting.JobTitle, string.Empty) + "\"" + (Skills != string.Empty ? "," + MiscUtil.RemoveScript(Skills, string.Empty) : string.Empty);
                txtJobTitle.Text = MiscUtil.RemoveScript(CurrentJobPosting.JobTitle, string.Empty);
                txtWorkCity.Text = MiscUtil.RemoveScript(CurrentJobPosting.City, string.Empty);
                ControlHelper.SelectListByValue(ddlCountry, CurrentJobPosting.CountryId.ToString());
                MiscUtil.PopulateState(ddlState, CurrentJobPosting.CountryId, Facade);
                ControlHelper.SelectListByValue(ddlState, CurrentJobPosting.StateId.ToString());
                hdnSelectedState.Value = CurrentJobPosting.StateId.ToString();
                txtMinExp.Text = CurrentJobPosting.MinExpRequired != string.Empty ? CurrentJobPosting.MinExpRequired : string.Empty;
                txtMaxExp.Text = CurrentJobPosting.MaxExpRequired != string.Empty ? CurrentJobPosting.MaxExpRequired : string.Empty;
                optSecurityClearance.SelectedIndex = CurrentJobPosting.SecurityClearance ? 0 : 1;
                if (CurrentJobPosting.RequiredDegreeLookupId != string.Empty)
                {
                    IList<string> EducationId = new List<string>();
                    EducationId = CurrentJobPosting.RequiredDegreeLookupId.Split(',');
                    foreach (string id in EducationId)
                    {
                        if (id != string.Empty)
                        {
                            foreach (ListItem item in chkEducationList.Items)
                            {
                                if (item.Value == id)
                                    item.Selected = true;
                            }
                        }
                    }
                }
                else
                    chkEducationList.ClearSelection();
            }
        }

        public  void LoadSearchTerms(JobPostingSearchAgent searchagent)
        {
            try
            {
                txtKeyword.Text = searchagent.KeyWord != null ? MiscUtil.RemoveScript(searchagent.KeyWord, string.Empty) : string.Empty;
                txtJobTitle.Text = searchagent.JobTitle != null ? MiscUtil.RemoveScript(searchagent.JobTitle, string.Empty) : string.Empty;
                txtWorkCity.Text = searchagent.City != null ? MiscUtil.RemoveScript(searchagent.City, string.Empty) : string.Empty;
                ddlCountry.SelectedValue = searchagent.CountryId != 0 ? searchagent.CountryId.ToString() : "0";
                hdnSelectedState.Value = ddlState.SelectedValue = searchagent.StateId != 0 ? searchagent.StateId.ToString() : "0";
                txtMinExp.Text = searchagent.MinExperience != null ? searchagent.MinExperience : string.Empty;
                txtMaxExp.Text = searchagent.MaxExperience != null ? searchagent.MaxExperience : string.Empty;
                txtSalaryRangeFrom.Text = searchagent.SalaryFrom != null ? searchagent.SalaryFrom : string.Empty;
                txtSalaryRangeTo.Text = searchagent.SalaryTo != null ? searchagent.SalaryTo : string.Empty;
                ddlCurrency.SelectedValue = searchagent.CurrencyLookUpId != 0 ? searchagent.CurrencyLookUpId.ToString() : "0";
                ddlPaymentType.SelectedValue = searchagent.SalaryType != null && searchagent.SalaryType != "0" ? searchagent.SalaryType : "4";
                try
                {
                    //ddlHotList.SelectedValue = searchagent.HotListId != 0 ? searchagent.HotListId.ToString() : "0";
                    ControlHelper.SelectListByValue(ddlHotList, searchagent.HotListId.ToString());
                }
                catch { }
                if (searchagent.EducationLookupId != string.Empty)
                {
                    IList<string> list = new List<string>();
                    list = searchagent.EducationLookupId.Split(',');
                    foreach (string li in list)
                    {
                        if (li != string.Empty)
                        {
                            foreach (ListItem item in chkEducationList.Items)
                            {
                                if (item.Value == li)
                                    item.Selected = true;
                                //else
                                //    item.Selected = false;
                            }
                        }
                    }
                }
                else
                    chkEducationList.ClearSelection();
                ddlJobType.SelectedValue = searchagent.EmployementTypeLookUpId != 0 ? searchagent.EmployementTypeLookUpId.ToString() : "0";
                ddlResumeLastUpdate.SelectedValue = searchagent.ResumeLastUpdated != null && searchagent.ResumeLastUpdated != string.Empty ? searchagent.ResumeLastUpdated : "0";
                ddlApplicantType.SelectedValue = searchagent.CandidateTypeLookUpId != 0 ? searchagent.CandidateTypeLookUpId.ToString() : "0";
                ddlWorkSchedule.SelectedValue = searchagent.WorkScheduleLookupId != 0 ? searchagent.WorkScheduleLookupId.ToString() : "0";
                if (searchagent.WorkStatusLookUpId != string.Empty)
                {
                    IList<string> list = new List<string>();
                    list = searchagent.WorkStatusLookUpId.Split(',');
                    foreach (string li in list)
                    {
                        if (li != string.Empty)
                        {
                            foreach (ListItem item in lstWorkStatus.Items)
                            {
                                if (item.Value == li)
                                    item.Selected = true;
                                //else
                                //    item.Selected = false;
                            }
                        }
                    }
                }
                else
                    lstWorkStatus.ClearSelection();
                if (searchagent.HiringStatusLookUpId != string.Empty)
                {
                    IList<string> list = new List<string>();
                    list = searchagent.HiringStatusLookUpId.Split(',');
                    foreach (string li in list)
                    {
                        if (li != string.Empty)
                        {
                            foreach (ListItem item in chkHiringStatus.Items)
                            {
                                if (item.Value == li)
                                    item.Selected = true;
                                //else
                                //    item.Selected = false;
                            }
                        }
                    }
                }
                else
                    chkHiringStatus.ClearSelection();
                if (searchagent.AvailableAfter != DateTime.MinValue)
                    txtJoiningDate.Text = searchagent.AvailableAfter.ToShortDateString();
                else { txtJoiningDate.Text = ""; wdcJoiningDate.SelectedDate = null; }
                optSecurityClearance.SelectedIndex = searchagent.SecurityClearance == false ? 1 : 0;
            }
            catch
            {
            }
        }

        private JobPostingSearchAgent BuildJobPostingSearchAgent()
        {
            JobPostingSearchAgent searchagent = new JobPostingSearchAgent();
            try
            {
                searchagent.JobPostingId = base.CurrentJobPostingId;
                searchagent.KeyWord = txtKeyword.Text != string.Empty ? MiscUtil.RemoveScript(txtKeyword.Text) : string.Empty;
                searchagent.JobTitle = txtJobTitle.Text != string.Empty ? MiscUtil.RemoveScript(txtJobTitle.Text) : string.Empty;
                searchagent.City = txtWorkCity.Text != string.Empty ? MiscUtil.RemoveScript(txtWorkCity.Text) : string.Empty;
                searchagent.CountryId = ddlCountry.SelectedIndex != 0 ? Int32.Parse(ddlCountry.SelectedValue) : 0;
                searchagent.StateId = hdnSelectedState.Value != string.Empty ? Int32.Parse(hdnSelectedState.Value) : 0;
                searchagent.MinExperience = txtMinExp.Text != string.Empty ? MiscUtil.RemoveScript(txtMinExp.Text) : string.Empty;
                searchagent.MaxExperience = txtMaxExp.Text != string.Empty ? MiscUtil.RemoveScript(txtMaxExp.Text) : string.Empty;
                searchagent.SalaryFrom = txtSalaryRangeFrom.Text != string.Empty ? MiscUtil.RemoveScript(txtSalaryRangeFrom.Text) : string.Empty;
                searchagent.SalaryTo = txtSalaryRangeTo.Text != string.Empty ? MiscUtil.RemoveScript(txtSalaryRangeTo.Text) : string.Empty;
                searchagent.CurrencyLookUpId = ddlCurrency.SelectedIndex != 0 ? Int32.Parse(ddlCurrency.SelectedValue) : 0;
                searchagent.SalaryType = ddlPaymentType.SelectedValue;
                searchagent.HotListId = Int32.Parse(ddlHotList.Visible == true ? (ddlHotList.SelectedValue == "0" ? "0" : ddlHotList.SelectedValue) : (hdnSelectedHotListText.Value == txtSearch_HotList.Text && txtSearch_HotList.Text != string.Empty ? hdnSelectedHotList.Value : "0"));
                string Qualification = string.Empty;
                foreach (ListItem li in chkEducationList.Items)
                    if (li.Selected) Qualification += li.Value.ToString() + ",";
                if (!(string.IsNullOrEmpty(Qualification)))
                    Qualification = Qualification.Remove(Qualification.Length - 1);
                searchagent.EducationLookupId = Qualification;
                searchagent.EmployementTypeLookUpId = ddlJobType.SelectedIndex != 0 ? Int32.Parse(ddlJobType.SelectedValue) : 0;
                searchagent.ResumeLastUpdated = ddlResumeLastUpdate.SelectedIndex != 0 ? ddlResumeLastUpdate.SelectedValue : "0";
                searchagent.CandidateTypeLookUpId = ddlApplicantType.SelectedIndex != 0 ? Int32.Parse(ddlApplicantType.SelectedValue) : 0;
                searchagent.WorkScheduleLookupId = ddlWorkSchedule.SelectedIndex != 0 ? Int32.Parse(ddlWorkSchedule.SelectedValue) : 0;
                string workStatus = string.Empty;
                foreach (ListItem li in lstWorkStatus.Items)
                {
                    if (li.Selected)
                    {
                        workStatus += li.Value.ToString() + ",";
                    }
                }
                if (!(string.IsNullOrEmpty(workStatus)))
                    workStatus = workStatus.Remove(workStatus.Length - 1);
                searchagent.WorkStatusLookUpId = workStatus;
                string hiringstatus = string.Empty;
                foreach (ListItem item in chkHiringStatus.Items)
                {
                    if (item.Selected)
                    {
                        if (hiringstatus != string.Empty) hiringstatus += ",";
                        hiringstatus += item.Value;
                    }

                }
                searchagent.HiringStatusLookUpId = hiringstatus;
                DateTime value;
                DateTime.TryParse(txtJoiningDate.Text, out value);
                wdcJoiningDate.SelectedDate = value;
                if (txtJoiningDate.Text.Trim() != string.Empty && wdcJoiningDate.SelectedDate != null)
                    searchagent.AvailableAfter = Convert.ToDateTime(txtJoiningDate.Text);
                if (dvSecurityClearence.Visible)
                    searchagent.SecurityClearance = optSecurityClearance.SelectedIndex == 0 ? true : false;
                else
                    searchagent.SecurityClearance = false;
                searchagent.CreatorId = searchagent.UpdatorId = CurrentMember.Id;
                searchagent.IsRemoved = false;
            }
            catch
            {
                return null;
            }
            return searchagent;
        }

        private void Clear()
        {
            txtKeyword.Text = string.Empty;
            txtJobTitle.Text = string.Empty;
            txtWorkCity.Text = string.Empty;
            ddlCountry.SelectedIndex = 0;
            hdnSelectedState.Value = string.Empty;
            ddlState.SelectedIndex = 0;
            MiscUtil.PopulateState(ddlState, Int32.Parse(ddlCountry.SelectedValue), Facade);
            txtMinExp.Text = string.Empty;
            txtMaxExp.Text = string.Empty;
            txtSalaryRangeFrom.Text = string.Empty;
            txtSalaryRangeTo.Text = string.Empty;
            ControlHelper.SelectListByValue(ddlCurrency, (SiteSetting[DefaultSiteSetting.Currency.ToString()] == null ? "0" : SiteSetting[DefaultSiteSetting.Currency.ToString()].ToString()));
            if (ddlCurrency.SelectedItem.Text == "INR")
            {
                lblSalaryRate.Text = "(Lacs)";
                lblSalaryRate.Visible = true;
            }
            else
            {
                lblSalaryRate.Text = "";
            }
            ddlPaymentType.SelectedIndex = 0;
            ddlHotList.SelectedIndex = 0;
            foreach (ListItem item in chkEducationList.Items)
            {
                item.Selected = false;
            }
            ddlJobType.SelectedIndex = 0;
            ddlResumeLastUpdate.SelectedIndex = 0;
            ddlApplicantType.SelectedIndex = 0;
            ddlWorkSchedule.SelectedIndex = 0;
            foreach (ListItem item in lstWorkStatus.Items)
            {
                item.Selected = false;
            }
            foreach (ListItem item in chkHiringStatus.Items)
            {
                item.Selected = false;
            }
            wdcJoiningDate.SelectedDate = null;
            txtJoiningDate.Text = string.Empty;
            optSecurityClearance.SelectedIndex = 1;
        }

        public void MessageAnswered(object sender, ConfirmationWindow.MsgBoxEventArgs e)
        {
            if (e.Answer == ConfirmationWindow.enmAnswer.OK)
            {
                SecureUrl url = UrlHelper.BuildSecureUrl(UrlConstants.ApplicationBaseUrl + "Employee/MailSetup.aspx", string.Empty);
                ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "Open", "<script>window.open('" + url + "');</script>", false);
            }


        }

       
        #endregion

        #region PageEvents
        protected void Page_Load(object sender, EventArgs e)
        {
            ddlCountry.Attributes.Add("onChange", "Country_OnChange('" + ddlCountry.ClientID + "','" + ddlState.ClientID + "','" + hdnSelectedState.ClientID + "')");
            ddlState.Attributes.Add("onChange", "State_OnChange('" + ddlState.ClientID + "','" + hdnSelectedState.ClientID + "')");
            ddlHotList.Attributes.Add("onmouseover", "this.title=this.options[this.selectedIndex].title");
            wdcJoiningDate.Format = System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern;
            //gvDate.MinimumValue = DateTime.Now.ToShortDateString();
            //gvDate.MaximumValue = DateTime.MaxValue.ToShortDateString();
            autoSeachHotList.ContextKey = "0";
            if (CurrentJobPostingId > 0)
            {
                SearchAgentEmailTemplate template = Facade.GetSearchAgentEmailTemplateByJobPostingId(CurrentJobPostingId);
                if (template == null)
                {
                    mailsetting = MiscUtil.IsValidMailSetting(Facade, CurrentMember.Id);
                }
                else
                {
                    if (template.SenderId == CurrentMember.Id)
                        mailsetting = MiscUtil.IsValidMailSetting(Facade, template.SenderId);
                    else
                        mailsetting = true;
                }
            }
            if (!mailsetting)
            {
                div.Visible = true;
                uclConfirms.MsgBoxAnswered += MessageAnswered;
            }
            if (!IsPostBack)
            {
                PrepareView();
            }
            if (ddlCountry.SelectedIndex != 0)
                MiscUtil.PopulateState(ddlState, Int32.Parse(ddlCountry.SelectedValue), Facade);
            JobPostingSearchAgent SearchTerms = new JobPostingSearchAgent();
            if(CurrentJobPostingId >0)
            SearchTerms = Facade.GetJobPostingSearchAgentByJobPostingId(base.CurrentJobPostingId);
            if (SearchTerms == null)
            {
                if (!IsPostBack) LoadDefaultSearchTerms();
                isNew = true;
                hdnJobPostingSearchAgentId.Value = string.Empty;
            }
            else
            {
                if (!IsPostBack) LoadSearchTerms(SearchTerms);
                isNew = false;
                hdnJobPostingSearchAgentId.Value = SearchTerms.Id.ToString();
            }
            if (DefaultCountryCode != "")
            {
                Country country = Facade.GetCountryById(Convert.ToInt32(DefaultCountryCode));
                if (country != null)
                {
                    if (country.CountryCode == "US")
                        dvSecurityClearence.Visible = true;
                    else
                        dvSecurityClearence.Visible = false;
                }
            }

        }
        protected void Page_PreRender(object sender, EventArgs e)
        {
            
            if (hdnPostback.Value  == "1" && CurrentJobPostingId >0)
            {
                JobPostingSearchAgent SearchTerms = new JobPostingSearchAgent();
                SearchTerms = Facade.GetJobPostingSearchAgentByJobPostingId(base.CurrentJobPostingId);
                if (SearchTerms == null)
                {
                    LoadDefaultSearchTerms();
                    hdnJobPostingSearchAgentId.Value = string.Empty;
                }
                else
                {
                    LoadSearchTerms(SearchTerms);
                    hdnJobPostingSearchAgentId.Value = SearchTerms.Id.ToString();
                }
                hdnPostback.Value = string.Empty;
            }
             
        }
        #endregion

        #region Button Events
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (mailsetting)
            {
                JobPostingSearchAgent searchterms = new JobPostingSearchAgent();
                searchterms = BuildJobPostingSearchAgent();
                if (isNew)
                {
                    searchterms = Facade.AddJobPostingSearchAgent(searchterms);
                }
                else
                {
                    searchterms.Id = hdnJobPostingSearchAgentId.Value != string.Empty ? Int32.Parse(hdnJobPostingSearchAgentId.Value) : 0;
                    searchterms = Facade.UpdateJobPostingSearchAgent(searchterms);
                }
                if (SearchAgentAdded != null)
                    SearchAgentAdded(searchterms, true);
            }
            else
            {
              
                div.Visible = true;
                uclConfirms.AddMessage("Mail Server SMTP details have not been entered. Would you like to enter them now?", ConfirmationWindow.enmMessageType.Attention, true, true, "");
                AjaxControlToolkit.ModalPopupExtender ex = (AjaxControlToolkit.ModalPopupExtender)this.Page.Master.FindControl("cphHomeMasterTitle").FindControl("reqPublish").FindControl("mpeSearchTerm");
                ex.Show();
            }
        }

        protected void btnRemove_Click(object sender, EventArgs e)
        {
            Clear();
            AjaxControlToolkit.ModalPopupExtender ex = (AjaxControlToolkit.ModalPopupExtender)this.Page.Master.FindControl("cphHomeMasterTitle").FindControl("reqPublish").FindControl("mpeSearchTerm");
            ex.Show();
            LoadDefaultSearchTerms();
        }
        #endregion
    }
}