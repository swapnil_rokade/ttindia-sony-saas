﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SiteMapList.ascx.cs" Inherits="TPS360.Web.UI.ctlSiteMapList" %>
<%@ Register Src="../Controls/MenuSelector.ascx" TagName="MenuSelector" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
 <%@ Register Src ="~/Controls/PagerControl.ascx" TagName ="Pager" TagPrefix ="ucl" %>  
 <script src ="../Scripts/fixWebkit.js" type ="text/javascript" ></script>
<script type ="text/javascript" >
Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(beginReq); 
Sys.WebForms.PageRequestManager.getInstance().add_endRequest(endReq);    
function beginReq(sender,args)
{var ModalProgress ='<%= Modal.ClientID %>';
$find(ModalProgress).show();
}
function endReq(sender, args) { 
var ModalProgress ='<%= Modal.ClientID %>';
    $find(ModalProgress).hide(); 
} 
</script>
<asp:TextBox ID="txtSortColumn" runat ="server" Visible ="false" ></asp:TextBox>
<asp:TextBox ID="txtSortOrder" runat ="server" Visible ="false" ></asp:TextBox>
<div style="padding: 15px;">
    <div class="MainBox" style="width: 100%;">
        <div class="BoxHeader">
            <div class="BoxHeaderContainer">
                <div class="TitleBoxLeft">
                    <asp:Image ID="imgArrow" runat="server" SkinID="sknArrow" AlternateText="" /></div>
                <div class="TitleBoxMid">
                    Site Map List</div>
            </div>
        </div>
        <div class="HeaderDevider">
        </div>
        <div class="MasterBoxContainer">
            <div class="ChildBoxContainer">
                <div style="clear: both">
                </div>
                <div class="BoxContainer">
                    <div style="width: 100%;">
                        <asp:ObjectDataSource ID="odsCustomSiteMap" runat="server" SelectMethod="GetPaged"
                            TypeName="TPS360.Web.UI.CustomSiteMapDataSource" SelectCountMethod="GetListCount"
                            EnablePaging="True" SortParameterName="sortExpression"></asp:ObjectDataSource>
                        <asp:UpdatePanel ID="updPanelCustomSiteMap" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div>
                                    <asp:Label ID="lblMessage" runat="server" EnableViewState="false"></asp:Label>
                                </div>
                                <br />
                                <asp:Panel ID="pnlListView" runat="server">
                                    <div class="GridContainer" style="text-align: left;">
                                        <asp:ListView ID="lsvCustomSiteMap" runat="server" DataSourceID="odsCustomSiteMap"
                                            DataKeyNames="Id" OnItemDataBound="lsvCustomSiteMap_ItemDataBound" OnItemCommand="lsvCustomSiteMap_ItemCommand" OnPreRender ="lsvCustomSiteMap_PreRender">
                                            <LayoutTemplate>
                                                <table id="tlbTemplate" runat="server" class="Grid" cellspacing="0" border="0">
                                                    <tr>
                                                        <th>
                                                            <asp:LinkButton ID="btnTitle" CausesValidation="false" runat="server" CommandName="Sort"
                                                                CommandArgument="Title" Text="Title" />
                                                        </th>
                                                        <th>
                                                            Description
                                                        </th>
                                                        <th>
                                                            Parent Menu
                                                        </th>
                                                        <th>
                                                            <asp:LinkButton ID="btnUpdateDate" CausesValidation="false" runat="server" CommandName="Sort"
                                                                CommandArgument="UpdateDate" Text="Last Updated" />
                                                        </th>
                                                        <th  style =" width :40px !important">
                                                            Action
                                                        </th>
                                                    </tr>
                                                    <tr id="itemPlaceholder" runat="server">
                                                    </tr>
                                                    <tr class="Pager">
                                                        <td colspan="5">
                                                             <ucl:Pager Id="pagerControl" runat ="server" EnableViewState ="true"  />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </LayoutTemplate>
                                            <EmptyDataTemplate>
                                                <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                                                    <tr>
                                                        <td>
                                                            No data was returned.
                                                        </td>
                                                    </tr>
                                                </table>
                                            </EmptyDataTemplate>
                                            <ItemTemplate>
                                                <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                                                    <td>
                                                        <asp:Label ID="lblName" runat="server" />
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblDescription" runat="server" />
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblParentMenu" runat="server" />
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblLastUpdated" runat="server" />
                                                    </td>
                                                    <td>
                                                        <asp:ImageButton ID="btnEdit" CausesValidation="false" SkinID="sknEditButton" runat="server"
                                                            CommandName="EditItem"></asp:ImageButton>
                                                        <asp:ImageButton ID="btnDelete" CausesValidation="false" SkinID="sknDeleteButton"
                                                            runat="server" CommandName="DeleteItem"></asp:ImageButton>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:ListView>
                                    </div>
                                </asp:Panel>
                                <div class="GridContainer" style="text-align: left; padding-left: 90px;">
                                    <asp:Panel ID="pnlTreeView" runat="server">
                                        <uc1:MenuSelector ID="ctlMenuSelector" runat="server" />
                                    </asp:Panel>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
                <asp:Image ID="imgRightTopImage" runat="server" SkinID="sknRightTopImage" AlternateText="" />
                <asp:Image ID="imgBoxLeftBottom" runat="server" SkinID="sknBoxLeftBottom" AlternateText="" />
                <asp:Image ID="imgBoxRightBottom" runat="server" SkinID="sknBoxRightBottom" AlternateText="" />
            </div>
        </div>
    </div>
    <asp:Panel ID="pnlmodal" runat="server" style="display: none">
                    <asp:UpdatePanel runat="server" ID="UpdatePanel2" RenderMode="Inline" UpdateMode="Conditional">
                        <ContentTemplate>
                        <img src="../Images/AjaxLoading.gif" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
            </asp:Panel>
            <ajaxToolkit:ModalPopupExtender ID ="Modal" runat ="server" TargetControlID ="pnlmodal" PopupControlID ="pnlmodal"
             BackgroundCssClass ="divModalBackground" ></ajaxToolkit:ModalPopupExtender> 
</div>
