﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MemberSourceHistory.ascx.cs"
    Inherits="TPS360.Web.UI.MemberSourceHistory" %>
<%@ Register Src="~/Controls/PagerControl.ascx" TagName="Pager" TagPrefix="ucl" %>
<div>
    <asp:UpdatePanel ID="uppane" runat="server">
        <ContentTemplate>
            <div class="TabPanelHeader">
                Source History Details:</div>
            <div class="GridContainer" style="padding-left: 25px;">
                <asp:ObjectDataSource ID="odsSourceHistory" runat="server" SelectMethod="GetPaged"
                    TypeName="TPS360.Web.UI.MemberSourceHistoryDataSource" EnablePaging="true" SelectCountMethod="GetListCount"
                    SortParameterName="sortExpression">
                    <SelectParameters>
                        <asp:Parameter Name="MemberId" DefaultValue="0" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:HiddenField ID="hdnSortColumn" runat="server" />
                <asp:HiddenField ID="hdnSortOrder" runat="server" />
                <asp:ListView ID="lsvSourceHistoryView" EnableViewState="true" runat="server" DataKeyNames="Id"
                    DataSourceID="odsSourceHistory" OnItemDataBound="lsvSourceHistoryView_ItemDataBound" OnPreRender="lsvSourceHistoryView_PreRender">
                    <LayoutTemplate>
                        <table id="tlbTemplate" runat="server" class="Grid" cellspacing="0" border="0">
                            <tr>
                                <th style="white-space: nowrap;">
                                    Date
                                </th>
                                <%--<th style="white-space: nowrap;">
                                    User 
                                </th>--%>
                                <th style="white-space: nowrap;">
                                    Source
                                </th>
                                <th style="white-space: nowrap;">
                                    Source Description
                                </th>
                            </tr>
                            <tr id="itemPlaceholder" runat="server">
                            </tr>
                            <tr class="Pager">
                                <td colspan="3" id="tdPager" runat="server">
                                    <ucl:Pager ID="pagerControl" runat="server" EnableViewState="true" NoOfRowsCookie="MemberSourceHistorybyTeamRowPerPage"/>
                                </td>
                            </tr>
                        </table>
                    </LayoutTemplate>
                    <EmptyDataTemplate>
                        <table id="tblEmptyData" style="" class="EmptyDataTable alert alert-warning" runat="server">
                            <tr>
                                <td>
                                    No Source detail available.
                                </td>
                            </tr>
                        </table>
                    </EmptyDataTemplate>
                    <ItemTemplate>
                        <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                            <td style="text-align: left">
                                <asp:Label ID="lblDate" runat="server" />
                            </td>
                            <%--<td style="text-align: left">
                                <asp:Label ID="lblUser" runat="server" />
                            </td>--%>
                            <td style="text-align: left">
                                <asp:Label ID="lblSource" runat="server" />
                            </td>
                            <td style="text-align: left">
                                <asp:Label ID="lblsourcedescription" runat="server" />
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:ListView>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>
