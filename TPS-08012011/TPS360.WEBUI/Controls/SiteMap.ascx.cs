﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: SiteMap.ascx.cs
    Description:
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.         Date                Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1             23-Nov-2009         Sandeesh            Defect id: 11893 ; Changes made for adding and updating a menu  item.
 *  0.2             31-Dec-2009         BasavarajAngadi     Defect id:10995 ; Changes made to sort alphabetics in the 'Menu Type'and 'Parent Type' Menu
------------------------------------------------------------------------------------------------------------------------------------------- 
*/
using System;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Providers;
using TPS360.Web.UI.Helper;
using System.Collections;
using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace TPS360.Web.UI
{
    public partial class cltSiteMap : BaseControl
    {
        #region Member Variables

        CustomSiteMap _customSiteMap;

        #endregion

        #region Methods

        private string GetTabKey(int tabPosition)
        {
            string test = "editorTab,listTab";

            string[] tabs = test.Split(',');

            return tabs[tabPosition];
        }

        private void RedirectToList(string message, string tabKey)
        {
            string returnUrl = Helper.Url.SecureUrl.ReturnUrl;

            if (StringHelper.IsBlank(returnUrl))
            {
                returnUrl = UrlConstants.Admin.SITEMAP_EDITOR_PAGE;
            }

            if (!StringHelper.IsBlank(message))
            {
                Helper.Url.Redirect(returnUrl, null, UrlConstants.PARAM_MSG, message, UrlConstants.PARAM_TAB, StringHelper.Convert(tabKey));
            }
            else
            {
                Helper.Url.Redirect(returnUrl);
            }
        }        

        private CustomSiteMap CurrentCustomSiteMap
        {
            get
            {
                if (_customSiteMap == null)
                {
                    if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_ID]))
                    {
                        int id = Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_ID]);

                        if (id > 0)
                        {
                            _customSiteMap = Facade.GetCustomSiteMapById(id);
                        }
                    }

                    if (_customSiteMap == null)
                    {
                        _customSiteMap = new CustomSiteMap();
                    }
                }

                return _customSiteMap;
            }
        }

        private void PopulateUrl()
        {
            XDocument feedXML = XDocument.Load(Server.MapPath("../TPSSiteMap.xml"));

            var feeds = from menu in feedXML.Descendants("Menu")
                        select new
                        {
                            Name = menu.Element("Name").Value,
                            Url = menu.Element("Url").Value,
                            Menus = menu.Parent.Attribute("name").Value
                        };

            foreach (var Property in feeds)
            {
                if (String.Compare(Property.Menus.ToString(), "TPS360Root") == 0)
                {
                    ddlUrl.Items.Add(Property.Url);
                }
                else
                {
                    ddlUrl.Items.Add(Property.Menus + "/" + Property.Url);
                }
            }

            ddlUrl.DataSource = ddlUrl.Items.Cast<ListItem>().OrderBy(o => o.Text).ToList();
            ddlUrl.DataBind();

            ddlUrl.Items.Insert(0, UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT);

        }

        private void PrepareView()
        {
            CustomSiteMap customSiteMap = CurrentCustomSiteMap;

            txtTitle.Text =MiscUtil .RemoveScript ( customSiteMap.Title);
            txtDescription.Text =MiscUtil .RemoveScript ( customSiteMap.Description);
            ControlHelper.SelectListByValue(ddlMenuType, StringHelper.Convert((int)customSiteMap.SiteMapType));

            if (customSiteMap.IsVirtual)
            {
                ddlParentSiteMap.Enabled = false;
            }
            ControlHelper.SelectListByText(ddlUrl, customSiteMap.Url);
            SiteMapNode root = SqlSiteMap.Provider.FindSiteMapNodeFromKey(ddlMenuType.SelectedItem.Value);

            if (root != null)
            {
                ddlParentSiteMap.Items.Clear();
                SiteMapNodeCollection nodeList = root.ChildNodes;
                MiscUtil.PopulateParentSiteMapTree(ddlParentSiteMap, nodeList, string.Empty);
                ControlHelper.SelectListByValue(ddlParentSiteMap, StringHelper.Convert(customSiteMap.ParentId));
                ddlParentSiteMap.Items.Insert(0, UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT);
            }
        }

        private CustomSiteMap BuildCustomSiteMap()
        {
            CustomSiteMap customSiteMap = CurrentCustomSiteMap;

            customSiteMap.Title =MiscUtil .RemoveScript ( txtTitle.Text.Trim());
            customSiteMap.Description =MiscUtil .RemoveScript ( txtDescription.Text.Trim());

            string url = ddlUrl.SelectedValue;
            if (url == "Please Select")
            {
                url = string.Empty;
            }

            customSiteMap.Url = url;
            customSiteMap.SiteMapType = (SiteMapType)Convert.ToInt32((ddlMenuType.SelectedValue));//0.1
            if (!StringHelper.Equals(ddlParentSiteMap.SelectedItem.Text, UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT))
            {
                customSiteMap.ParentId = Convert.ToInt32(ddlParentSiteMap.SelectedValue);//0.1
            }
            else
            {
                customSiteMap.ParentId = (int)customSiteMap.SiteMapType;
            }

            customSiteMap.CreatorId = CurrentMember.Id;
            customSiteMap.UpdatorId = CurrentMember.Id;
            return customSiteMap;
        }

        private void SaveCustomSiteMap()
        {
            if (IsValid)
            {
                try
                {
                    CustomSiteMap customSiteMap = BuildCustomSiteMap();

                    if (customSiteMap.IsNew)
                    {
                        Facade.AddCustomSiteMap(customSiteMap);
                        ResetSiteMap();                       
                        RedirectToList("Site menu has been added successfully.", StringHelper.Convert(GetTabKey(1)));
                    }
                    else
                    {
                        Facade.UpdateCustomSiteMap(customSiteMap);
                        ResetSiteMap();                       
                        RedirectToList("Site menu has been updated successfully.", StringHelper.Convert(GetTabKey(1)));
                    }
                }
                catch (ArgumentException ex)
                {
                    MiscUtil.ShowMessage(lblMessage, ex.Message, true);
                }
            }
        }

        private void ResetSiteMap()
        {
            SqlSiteMapProvider customProvider = SiteMap.Providers["SqlSiteMap"] as SqlSiteMapProvider;

            if (customProvider != null)
            {
                customProvider.ResetSiteMap();
            }
        }

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                MiscUtil.PopulateMenuType(ddlMenuType);
                ddlMenuType.DataSource = ddlMenuType.Items.Cast<ListItem>().OrderBy(o => o.Text).ToList();
                ddlMenuType.DataTextField = "text";
                ddlMenuType.DataValueField = "value";
                ddlMenuType.DataBind();
                PopulateUrl();
                PrepareView();
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            SaveCustomSiteMap();
        }

        protected void ddlMenuType_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlParentSiteMap.Items.Clear();
            SiteMapNode root = SqlSiteMap.Provider.FindSiteMapNodeFromKey(ddlMenuType.SelectedValue);//0.1

            if (root != null)
            {
                SiteMapNodeCollection nodeList = root.ChildNodes;

                if  (nodeList != null && nodeList.Count > 0)
                {
                    ddlParentSiteMap.Enabled = true;
                    MiscUtil.PopulateParentSiteMapTree(ddlParentSiteMap, nodeList, string.Empty);
                }
                else
                {
                    ddlParentSiteMap.Enabled = false;
                }
            }
            else
            {
                ddlParentSiteMap.Enabled = false;
            }
            pnlDynamic.Update();
            ddlParentSiteMap.DataSource = ddlParentSiteMap.Items.Cast<ListItem>().OrderBy(o => o.Text).ToList();
            ddlParentSiteMap.DataTextField = "Text";
            ddlParentSiteMap.DataValueField = "Value";
            ddlParentSiteMap.DataBind();

        }

        #endregion
    }
}