﻿<%-- 
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: EmailEditor.ascx
    Description: This is the user control page used to create and send mail.
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1             Dec-31-2008          Jagadish N            Defect id: 9490; added label 'lblAsteric'.
    0.2             Mar-24-2009          Jagadish N            Defect id: 9523; Changes made to Div 'divMasterTemplate'.
-------------------------------------------------------------------------------------------------------------------------------------------       
--%>
<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EmailEditor.ascx.cs" Inherits="TPS360.Web.UI.cltEmail" %>
<%@ Register Src="~/Controls/ConfirmationWindow.ascx" TagName="Confirm" TagPrefix="ucl" %>
<%@ Register Assembly="Infragistics2.WebUI.WebHtmlEditor.v7.3, Version=7.3.20073.38, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.WebUI.WebHtmlEditor" TagPrefix="ighedit" %>
<%@ Register Src="~/Controls/EmailFileUpload.ascx" TagName="EmailFileUpload" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/HtmlEditor.ascx" TagName="HtmlEditor" TagPrefix="ucl" %>

<script language="JavaScript" src="../Scripts/Spell.js" type="text/javascript"></script>

<script language="JavaScript" src="../Scripts/SendMail.js" type="text/javascript"></script>

<script language="JavaScript" src="../Scripts/attachment.js" type="text/javascript"></script>

<script type="text/javascript" language="javascript">

 window.onload = function(){
 if( document.getElementById("<%= hdnclear.ClientID %>").value=="true")
 {
 document.getElementById("<%= txtCC.ClientID %>").value="";
 document.getElementById("<%= txtBCC.ClientID %>").value="";
  document.getElementById("<%= txtSubject.ClientID %>").value="";
  document.getElementById("<%= txtEmailEditor.ClientID %>").value="";
   var textarea=document.getElementById("<%= txtEmailEditor.FindControl("txtEditor").ClientID %>");
  
 
   textarea.TextXhtml="";
 }
 };
 
 function removeHTMLTags(htmlString){
        if(htmlString){
          var mydiv = document.createElement("div");
           mydiv.innerHTML = htmlString;
 
            if (document.all) // IE Stuff
            {
                return mydiv.innerText;
               
            }   
            else // Mozilla does not work with innerText
            {
                return mydiv.textContent;
            }                           
      }
   } 
 
    function ValidateEmailField(sender, eventArgs)
    {
     var To="";
        try
        {
          To = document .getElementById ('<%=txtToText.ClientID%>').value;
        }
        catch (e)
        {
          To ="null";
        }
        var bcc=document .getElementById ("<%=txtBCC.ClientID%>").value;
        var cc=document .getElementById ("<%=txtCC.ClientID%>").value;
        if(To =="" && bcc =="" && cc =="")
        {
            eventArgs .IsValid=false;
        }
        else 
        {
            eventArgs .IsValid=true ;
        }
    }
function mail()
{

var to=document .getElementById ("<%=lblToText.ClientID%>").innerText;
if(to=="")
{
if(to=="")
{
var ddlReport = document.getElementById("<%=drpToText.ClientID%>");
to = ddlReport.options[ddlReport.selectedIndex].text; 
var Value = ddlReport.options[ddlReport.selectedIndex].value;
 
}
}
var body="";
var sign="";
var s="";
var sub =document .getElementById ("<%=txtSubject.ClientID%>").value;
var identify=document .getElementById ("<%=check.ClientID%>").value;
if(identify =="Company")
{
body=document .getElementById("ctl00_ctl00_cphHomeMaster_cphCompanyMaster_uwtEmail__ctl0_ucntrlEmailEditor_txtEmailEditor_t_a").value;//document .getElementById ("<%=txtEmailEditor.ClientID%>").value;
sign =document .getElementById ("ctl00_ctl00_cphHomeMaster_cphCompanyMaster_uwtEmail__ctl0_ucntrlEmailEditor_Signature").value;
s=removeHTMLTags (sign );
}
if(identify =="Candidate")
{
body =document .getElementById ("ctl00_cphCandidateMaster_uwtEmail__ctl0_ucntrlEmailEditor_txtEmailEditor_t_a").value;
sign  =document .getElementById ("ctl00_cphCandidateMaster_uwtEmail__ctl0_ucntrlEmailEditor_Signature").value;

s=removeHTMLTags(sign );

}
if(s==null)
{
body =body;
}
else 
{
body=body+s;
}

var bcc=document .getElementById ("<%=txtBCC.ClientID%>").value;

var cc=document .getElementById ("<%=txtCC.ClientID%>").value;


SendMail (to,sub,body ,bcc ,cc,identify );
}

</script>

<div style="width: 100%; overflow: auto">
 <asp:HiddenField ID="hdnclear" runat="server" value="false"/>
    <asp:HiddenField ID="Signature" runat="server" />
    <asp:HiddenField ID="check" runat="server" Value="" />
    <asp:HiddenField ID="hfSignature" runat="server" Value="" />
    <asp:UpdatePanel ID="updatepanelbuttons" runat="server">
        <ContentTemplate>
            <asp:Label ID="lblMessage" runat="server" EnableViewState="false" style="z-index:999;"></asp:Label>
            <asp:Panel ID="pnldefault" runat="server">
                <div class="TableRow" style="text-align: center">
                    <div id="dv" runat="server" visible="false">
                        <ucl:Confirm ID="uclConfirm" runat="server"></ucl:Confirm>
                    </div>
                    <asp:HiddenField ID="hdfFromMemberId" runat="server" Value="0" />
                    <asp:HiddenField ID="hdfToMemberId" runat="server" Value="0" />
                </div>
                <div class="TableRow" style="width: 100%; white-space: nowrap;">
                    <div style="float: left; white-space: nowrap; width: 60%;">
                        <div class="TableFormLeble">
                            <asp:Label ID="lblFrom" runat="server" Text="From"></asp:Label>:
                        </div>
                        <div class="TableFormContent">
                            <%--  <asp:DropDownList ID="ddlFrom" CssClass="CommonDropDownList" runat="server">
                </asp:DropDownList>--%>
                            <asp:Label ID="lblFromAdd" runat="server" Style="font-size: 14px"></asp:Label>
                        </div>
                    </div>
                </div>
                <%-- <div class="TableRow" style =" width :100%">
        <div class="TableFormValidatorContent"  style = "margin-left: 20%;">
            <asp:RequiredFieldValidator ID="rfvForm" runat="server" ControlToValidate="ddlFrom"
                ErrorMessage="Please enter from address." EnableViewState="False" Display="Dynamic"></asp:RequiredFieldValidator>
        </div>
    </div>--%>
                <div class="TableRow" style="width: 100%; white-space: nowrap;">
                    <div style="float: left; white-space: nowrap; width: 60%;">
                        <div class="TableFormLeble">
                            <asp:Label ID="lblTo" runat="server" Text="To"></asp:Label>:
                        </div>
                        <div class="TableFormContent">
                            <asp:Label ID="lblToText" runat="server" Text="" Style="font-size: 14px"></asp:Label>
                            <asp:TextBox ID="txtToText" runat="server" Visible="false"></asp:TextBox>
                            <asp:DropDownList ID="drpToText" runat="server" Visible="false" CssClass="CommonDropDownList">
                            </asp:DropDownList>
                            <asp:Label ID="lblAsteric" runat="server" ForeColor="Red" Text="*" Visible="false"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="TableRow" style="float: right; width: 74.5%;">
                    <asp:RegularExpressionValidator ID="ReTOEmail" runat="server" ErrorMessage="Invalid Email Address."
                        ControlToValidate="txtToText" ValidationExpression="((\w+([-+.!#$%&*+-/=?^_`{|}~'\\\s]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)(\s)*(;)*(\s)*)*"
                        ValidationGroup="email" EnableViewState="False" Display="Dynamic" SetFocusOnError="true"></asp:RegularExpressionValidator>
                    <asp:RequiredFieldValidator ID="RFVToEmail" runat="server" ControlToValidate="txtToText"
                        Enabled="false" ValidationGroup="email" ErrorMessage="Please enter Email address."
                        EnableViewState="False" Display="Dynamic" SetFocusOnError="true"></asp:RequiredFieldValidator>
                    <%--\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*([,;]\s*\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)*--%>
                </div>
                <div class="TableRow">
                    <div class="TableFormValidatorContent" style="margin-left: 20%;">
                    </div>
                </div>
                <div class="TableRow" style="width: 100%; white-space: nowrap;" id="divCCBCC" runat="server">
                    <div style="float: left; white-space: nowrap; width: 60%;">
                        <div class="TableFormLeble">
                            <asp:Label ID="lblCC" runat="server" Text="CC"></asp:Label>:
                        </div>
                        <div class="TableFormContent">
                            <asp:TextBox ID="txtCC" runat="server" CssClass="CommonTextBox"></asp:TextBox>
                            <asp:Label ID="lblvalid" runat="server" Visible="false"></asp:Label>
                        </div>
                    </div>
                    <div style="float: left; white-space: nowrap; width: 40%; margin-left: -65px">
                        <div class="TableFormLeble">
                            <asp:Label ID="lblBCC" runat="server" Text="BCC"></asp:Label>:
                        </div>
                        <div class="TableFormContent">
                            <asp:TextBox ID="txtBCC" runat="server" CssClass="CommonTextBox"></asp:TextBox>
                            <asp:Label ID="lblval" runat="server" Visible="false"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="TableRow" style="float: right; width: 74.5%;">
                    <asp:RegularExpressionValidator ID="RevCC" runat="server" ErrorMessage="Invalid Email Address."
                        ControlToValidate="txtCC" ValidationExpression="((\w+([-+.!#$%&*+-/=?^_`{|}~'\\\s]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)(\s)*(;)*(\s)*)*"
                        ValidationGroup="email" EnableViewState="False" Display="Dynamic" SetFocusOnError="true"></asp:RegularExpressionValidator>
                </div>
                <%--\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*([,;]\s*\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)*--%>
                <div class="TableFormContent" style="float: right; width: 29.3%;">
                    <asp:RegularExpressionValidator ID="RevBCC" runat="server" ErrorMessage="Invalid Email Address."
                        ControlToValidate="txtBCC" ValidationExpression="((\w+([-+.!#$%&*+-/=?^_`{|}~'\\\s]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)(\s)*(;)*(\s)*)*"
                        ValidationGroup="email" EnableViewState="False" Display="Dynamic" SetFocusOnError="true"></asp:RegularExpressionValidator>
                </div>
                <%--(\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*([,;]\s*\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)*)--%>
                <div id="divMyTemplate" runat="server" visible="true">
                    <div class="TableRow" style="width: 100%; white-space: nowrap;">
                        <div style="float: left; white-space: nowrap; width: 60%;">
                            <div class="TableFormLeble">
                                <asp:Label ID="lblSubject" runat="server" Text="Subject"></asp:Label>:
                            </div>
                            <div class="TableFormContent">
                                <asp:TextBox ID="txtSubject" runat="server" CssClass="CommonTextBox" Height="17px"
                                    Width="240px" EnableViewState="false"></asp:TextBox>
                                <span class="RequiredField">*</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="TableRow" style="width: 100%;">
                    <div class="TableFormValidatorContent" style="margin-left: 25%">
                        <asp:RequiredFieldValidator ID="rfvSubject" runat="server" ControlToValidate="txtSubject"
                            ValidationGroup="email" ErrorMessage="Please enter subject." EnableViewState="False"
                            Display="Dynamic"></asp:RequiredFieldValidator>
                    </div>
                    <div class="TableFormValidatorContent" style="margin-left: 25%">
                        <asp:CustomValidator ID="CsValidateEmail" runat="server" ValidationGroup="email"
                            EnableViewState="false" Display="Dynamic" ClientValidationFunction="ValidateEmailField"
                            ErrorMessage="Please add at least one recipient."></asp:CustomValidator>
                    </div>
                </div>
                <div id="divMasterTemplate" runat="server" class="TableRow" style="float: left; white-space: nowrap;
                    width: 50%">
                    <div class="TableFormLeble">
                    </div>
                </div>
                <div class="TableRow">
                    <div class="TableFormValidatorContent" style="margin-left: 20%;">
                    </div>
                </div>
                <div class="TableRow" style="text-align: left;">
                    <div class="TableFormLeble" style="text-align: left;">
                        <asp:Label ID="lblMessageBox" runat="server" Text="Message"></asp:Label>:
                    </div>
                    <div class="TableFormContent">
                    </div>
                </div>
                <br />
                <div class="TableRow" style="text-align: left;">
                    <ucl:HtmlEditor ID="txtEmailEditor" runat="server" />
                </div>
                <div class="TableRow" style="float: left; white-space: nowrap; width: 50%;">
                    <div class="TableFormContent">
                        <asp:CheckBox ID="chkResetPassword" runat="server" Checked="false" Text="Reset Password"
                            Visible="false" />
                    </div>
                    <div class="TableFormContent">
                        <asp:CheckBox ID="chkIncludeSignature" runat="server" Checked="false" OnCheckedChanged="chkIncludeSignature_CheckedChanged"
                            AutoPostBack="true" Text=" Include <a href='../Employee/MySignature.aspx' Target=_blank >My Signature</a>" />
                    </div>
                </div>
                <div class="TableRow" style="float: left; white-space: nowrap; width: 50%;" id="divFileUpload"
                    runat="server">
                    <div>
                        <uc1:EmailFileUpload ID="EmailFileUpload1" SessionKey="EmailFileUpload" runat="server" />
                    </div>
                </div>
                <div class="TableRow" style="text-align: center;">
                    <asp:Button ID="btnPreview" CssClass="CommonButton" runat="server" Text="Preview"
                        OnClick="btnPreview_Click" ValidationGroup="email" UseSubmitBehavior="false" />
                    <asp:Button ID="btnSendEmail" CssClass="CommonButton" runat="server" Text="Send Email"
                        ValidationGroup="email" OnClick="btnSendEmail_Click" UseSubmitBehavior="true" />
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>
