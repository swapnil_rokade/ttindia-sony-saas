﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="HiringDetails.ascx.cs"
    Inherits="TPS360.Web.UI.HiringDetails" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Controls/ExperiencePicker.ascx" TagName="Experience" TagPrefix="ucl" %>

<script type="text/javascript" language="javascript" src="../js/AjaxVariables.js"></script>

<script type="text/javascript" language="javascript" src="../js/AjaxScript.js"></script>

<script src="../js/OfferedDetails.js" type="text/javascript"></script>

<link href="../assets/css/chosen.css" rel="Stylesheet" />
<style type="text/css">
    .CalendarCSS
    {
        background-color: Gray;
        color: Blue;
        border-color: Maroon;
    }
    .custom-calendar .ajax__calendar_container
    {
        background-color: #ffc; /* pale yellow */
        border: solid 1px #666;
    }
    .custom-calendar .ajax__calendar_title
    {
        background-color: #cf9; /* pale green */
        height: 20px;
        color: #333;
    }
    .custom-calendar .ajax__calendar_prev, .custom-calendar .ajax__calendar_next
    {
        background-color: #aaa; /* darker gray */
        height: 20px;
        width: 20px;
    }
    .custom-calendar .ajax__calendar_today
    {
        background-color: #cff; /* pale blue */
        height: 20px;
    }
    .custom-calendar .ajax__calendar_days table thead tr td
    {
        background-color: #ff9; /* dark yellow */
        color: #333;
    }
    .custom-calendar .ajax__calendar_day
    {
        color: #333; /* normal day - darker gray color */
    }
    .custom-calendar .ajax__calendar_other .ajax__calendar_day
    {
        display: none;
        color: #666; /* day not actually in this month - lighter gray color */
    }
    .calendarDropdown
    {
        margin-left: -18px;
        margin-bottom: -3px;
    }
</style>

<script language="javascript" type="text/javascript">
  function ValidateSourceDescription(sender,args)
    {

        args .IsValid=true;
        
        alert('hgfhgf');
       
        if($('#<%=ddlSource.ClientID %>').prop("selectedIndex")==0)
        {
            
            //if( $('#<%=ddlSourceDescription.ClientID %>').prop("selectedIndex")<=0) args .IsValid=false ;
        }
        else if($('#<%=ddlSource.ClientID %>').prop("selectedIndex")==1 || $('#<%=ddlSource.ClientID %>').prop("selectedIndex")==2)
        {
                if( $('#<%= txtsrcdesc.ClientID %>').val().trim()=="") args .IsValid=false ;
        }
        else if($('#<%=ddlSource.ClientID %>').prop("selectedIndex")==3)
        {
           if( $('#<%=ddlEmployeeReferrer.ClientID %>').prop("selectedIndex")<=0) 
           args .IsValid=false ;
          


        }
        else if ($('#<%=ddlSource.ClientID %>').prop("selectedIndex")==4)
        {
            if( $('#<%=ddlVendorContact.ClientID %>').prop("selectedIndex")<=0) args .IsValid=false ;
        }
//         if($('#<%=ddlSource.ClientID %>').prop("selectedIndex")>0 && $('#<%=ddlEmployeeReferrer.ClientID %>').prop("selectedIndex")>0 && $('#<%=ddlSourceDescription.ClientID %>').prop("selectedIndex")>0 && $('#<%= txtsrcdesc.ClientID %>').val().trim()=="")
//        {
//         
//           args.IsValid=false;
//        }
//        
        
        
    }
    
  
    
    
function showHideSource()
{


        var ddlSource= $("#<%=ddlSource.ClientID %>");
        $("#<%=divEmpReferral.ClientID %>").css("display", "none");     
        $("#<%=divVendor.ClientID %>").css("display", "none");     
        $("#<%=txtsrcdesc.ClientID %>").css("display", "none");                   
        $("#<%=ddlSourceDescription.ClientID %>").css("display", "none"); 
         


        if ($(ddlSource).find('option:selected').text().trim() == "Job Portals") {
        $("#<%=ddlSourceDescription.ClientID %>").css("display", "");                     
        }

        else  if ($(ddlSource).find('option:selected').text().trim() == "Employee Referral")
        {
        $("#<%=divEmpReferral.ClientID %>").css("display", "");  
          
       
        }
        else  if ($(ddlSource).find('option:selected').text().trim() == "Placement Consultants")
        {
        $("#<%=divVendor.ClientID %>").css("display", "");     
        }
        else {
       
        $("#<%=txtsrcdesc.ClientID %>").css("display", ""); 
          
        }
}


function AddEmbSalaryFields()
{
    var total = 0;
    $('input[rel=Emb]').each(function (){

    total= total + parseFloat ($(this).val()==''?0:$(this).val());

    });
    $('input[rel=EmbTotal]').val(total.toFixed(2) );
}


function AddMechSalaryFields()
{
        var total=0;
        $('input[rel=Mech]').each(function (){
        total= total + parseFloat ($(this).val()==''?0:$(this).val());

        });
        $('input[rel=MechTotal]').val(total.toFixed(2) );
        }

        Sys.Application.add_load(function() { 
        $('input').each(function (){
        //  if($(this).val().trim()=='')$(this).val('0');
        });
        $('input').click(function (){
        $(this).select();
        });


        AddEmbSalaryFields();
        AddMechSalaryFields();
        $('input[rel=EmbTotal],input[rel=MechTotal]').attr("disabled", "disabled"); 
        $('input[rel=Emb]').change(function (){
        if($(this).val().trim()=='')$(this).val('0');
        if(parseFloat ($(this).val().trim()!=''))  $(this).val(parseFloat ($(this).val()));
            AddEmbSalaryFields();
        });
        $('input[rel=Mech]').change(function (){
        if($(this).val().trim()=='')$(this).val('0');
        if(parseFloat ($(this).val().trim()!=''))  $(this).val(parseFloat ($(this).val()));
            AddMechSalaryFields();
        });


        // $('input[rel=EmbTotal]').val( parseFloat ($('input[rel=EmbTotal]').val()==''?0 : $('input[rel=EmbTotal]').val()) + parseFloat ($(this ).val()));});
        //$('input[rel=Mech]').change(function (){ $('input[rel=MechTotal]').val( parseFloat ($('input[rel=MechTotal]').val()==''?0 : $('input[rel=MechTotal]').val()) + parseFloat ($(this ).val()));});
        $('input[rel=Emb], input[rel=Mech]').keypress(function (event){return allownumeric(event,$(this));});
        $("#<%=ddlSource.ClientID %>").change(function() {
      
        showHideSource();
        });
            showHideSource();
        $("#<%=ddlBVstatus.ClientID %>").change(function() {

        if($(this).find('option:selected').text().trim()=="Amber")
                 $(this).css("background-color","#FFC200");
        else 
            $(this).css("background-color",$(this).find('option:selected').text().trim());
        });
        
        var ddlBVStatus= $("#<%=ddlBVstatus.ClientID %>");
         if($(ddlBVStatus).find('option:selected').text().trim()=="Amber")
                 $(ddlBVStatus).css("background-color","#FFC200");
        else 
            $(ddlBVStatus).css("background-color",$(ddlBVStatus).find('option:selected').text().trim());
        
    });
        
</script>

<script language="javascript" type="text/javascript">
//  function ValidateSourceDescription(sender,args)
//    {

//        if($('#<%=ddlSource.ClientID %>').prop("selectedIndex")!=0 && $('#<%=ddlSource.ClientID %>').prop("selectedIndex")!=3 && $('#<%=ddlSource.ClientID %>').prop("selectedIndex")!=4 && $('#<%= txtsrcdesc.ClientID %>').val().trim()=="")
//           args.IsValid=false;
//    }
function WebDateChooser1_InitializeDateChooser(oDateChooser)
{
    // used to set "drop-down block" variable if calendar was already opened
    oDateChooser._myMouseDown = function()
    {
    var me = igdrp_getComboById('<%=wdcDateOffered.ClientID%>');
    if(me && me.isDropDownVisible())
    me._calOpenedTime = new Date().getTime();
    }
    // used to open 
    oDateChooser._myClick = function()
    {
    var me = igdrp_getComboById('<%=wdcDateOffered.ClientID%>');
    if(me && !me.isDropDownVisible() && (!me._calOpenedTime || me._calOpenedTime + 500 < new Date().getTime()))
    me.setDropDownVisible(true);
    }
    oDateChooser.inputBox.onmousedown = oDateChooser._myMouseDown;
    oDateChooser.inputBox.onclick = oDateChooser._myClick;
    //alert('ok');
}
    
</script>

<style type="text/css">
    .front
    {
        position: relative;
    }
</style>
<div>
    <asp:UpdatePanel ID="uppane" runat="server">
        <ContentTemplate>
            <asp:HiddenField ID="hdnBulkAction" runat="server" Value="" />
            <asp:HiddenField ID="hfMemberId" runat="server" />
            <asp:HiddenField ID="hfJobPostingId" runat="server" />
            <asp:HiddenField ID="hfMemberHiringDetailsId" runat="server" Value="0" />
            <asp:HiddenField ID="hfStatusId" runat="server" />
            <asp:HiddenField ID="hfCurrentMemberId" runat="server" />
            <div class="TableRow">
                <div class="FormLeftColumn">
                    <div class="TableRow">
                        <div class="TableFormLeble">
                            Date of Offer:</div>
                        <div class="TableFormContent">
                            <asp:TextBox ID="txtDateOffered" runat="server" Width="100px" TabIndex="1">  
                            </asp:TextBox>
                            <ajaxtoolkit:calendarextender id="wdcDateOffered" runat="server" targetcontrolid="txtDateOffered"
                                cssclass="custom-calendar" popupbuttonid="imgShow" enableviewstate="true">
                            </ajaxtoolkit:calendarextender>
                            <asp:ImageButton ID="imgShow" runat="server" CssClass="calendarDropdown" ImageUrl="~/Images/downarrow.gif" />
                        </div>
                    </div>
                    <div class="TableRow">
                        <div class="TableFormValidatorConent" style="margin-left: 44%">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtDateOffered"
                                ErrorMessage="Please select date of offer" Display="Dynamic" ValidationGroup="ValOfferDetails"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="TableRow">
                        <div class="TableFormLeble">
                            E-DOJ:
                        </div>
                        <div class="TableFormContent">
                            <asp:TextBox ID="txtJoiningDate" runat="server" Width="100px" TabIndex="2">  
                            </asp:TextBox>
                            <ajaxtoolkit:calendarextender id="wdcJoiningDate" runat="server" targetcontrolid="txtJoiningDate"
                                cssclass="custom-calendar" popupbuttonid="imgShows" enableviewstate="true">
                            </ajaxtoolkit:calendarextender>
                            <asp:ImageButton ID="imgShows" runat="server" CssClass="calendarDropdown" ImageUrl="~/Images/downarrow.gif" />
                        </div>
                    </div>
                    <div class="TableRow">
                        <div class="TableFormValidatorConent" style="margin-left: 44%">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtJoiningDate"
                                ErrorMessage="Please select E-DOJ" Display="Dynamic" ValidationGroup="ValOfferDetails"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="TableRow">
                        <div class="TableFormLeble">
                            Current CTC:
                        </div>
                        <div class="TableFormContent">
                            <asp:TextBox ID="txtcurrentCTC" runat="server" CssClass="CommonTextBox" rel="Numeric"
                                Width="40px" TabIndex="3"></asp:TextBox>
                            <asp:DropDownList ID="ddlcurrentCTC" runat="server" Width="50px" CssClass="CommonDropDownList"
                                TabIndex="4">
                            </asp:DropDownList>
                            <%--<asp:Label ID="lbllak" runat="server" Text="(Lacs)"></asp:Label>--%>
                            <asp:Label ID="lbllak" runat="server"></asp:Label>
                            <span class="RequiredField">*</span>
                        </div>
                    </div>
                    <div class="TableRow">
                        <div class="TableFormValidatorConent" style="margin-left: 44%">
                            <asp:RequiredFieldValidator ID="rfvCurrentCTC" runat="server" ControlToValidate="txtcurrentCTC"
                                ErrorMessage="Please enter Current CTC" Display="Dynamic" ValidationGroup="ValOfferDetails"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <%--<div class="TableRow">
                        <div class="TableFormValidatorContent" style="margin-left: 42%">
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtcurrentCTC"
                                Display="Dynamic" ErrorMessage="Please enter Numeric values" ValidationExpression="(^\d*\.?\d*[0-9]+\d*$)|(^[0-9]+\d*\.\d*$)"
                                ValidationGroup="ValOfferDetails"></asp:RegularExpressionValidator>
                        </div>
                    </div>--%>
                    <div class="TableRow">
                        <div class="TableFormLeble">
                            Offered CTC:
                        </div>
                        <div class="TableFormContent">
                            <asp:TextBox ID="txtMinCTC" runat="server" CssClass="CommonTextBox" rel="Numeric"
                                Width="40px" TabIndex="5"></asp:TextBox>
                            <asp:TextBox ID="txtMaxCTC" runat="server" CssClass="CommonTextBox" Width="20px"
                                TabIndex="6" Visible="false"></asp:TextBox>
                            <asp:DropDownList ID="ddlMaxCTC" runat="server" Width="50px" CssClass="CommonDropDownList"
                                TabIndex="7">
                            </asp:DropDownList>
                            <%--<asp:Label ID="lbllacs" runat="server" Text="(Lacs)"></asp:Label>--%>
                            <asp:Label ID="lbllacs" runat="server"></asp:Label>
                            <span class="RequiredField">*</span>
                        </div>
                        <%-- <div class="TableFormValidatorContent" style="margin-left: 42%">
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtMinCTC"
                                Display="Dynamic" ErrorMessage="Please enter Numeric values of Min CTC" ValidationExpression="(^\d*\.?\d*[0-9]+\d*$)|(^[0-9]+\d*\.\d*$)"
                                ValidationGroup="ValOfferDetails"></asp:RegularExpressionValidator>
                        </div>--%>
                        <%--<div class="TableFormValidatorContent" style="margin-left: 42%">
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="txtMaxCTC"
                                Display="Dynamic" ErrorMessage="Please enter Numeric values of Max CTC" ValidationExpression="(^\d*\.?\d*[0-9]+\d*$)|(^[0-9]+\d*\.\d*$)"
                                ValidationGroup="ValOfferDetails"></asp:RegularExpressionValidator>
                        </div>--%>
                    </div>
                    <div class="TableRow">
                        <div class="TableFormValidatorConent" style="margin-left: 44%">
                            <asp:RequiredFieldValidator ID="rfvOfferedCTC" runat="server" ControlToValidate="txtMinCTC"
                                ErrorMessage="Please enter Offered CTC" Display="Dynamic" ValidationGroup="ValOfferDetails"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <%--<div class="TableRow">
                        <div class="TableFormValidatorContent" style="margin-left: 42%">
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtcurrentCTC"
                                Display="Dynamic" ErrorMessage="Please enter Numeric values of Current CTC" ValidationExpression="(^\d*\.?\d*[0-9]+\d*$)|(^[0-9]+\d*\.\d*$)"
                                ValidationGroup="ValOfferDetails"></asp:RegularExpressionValidator>
                        </div>
                    </div>--%>
                    <%--<div class="TableRow">
                        <div class="TableFormValidatorContent" style="margin-left: 42%">
                            <asp:CompareValidator ID="cvCTCMin" runat="server" ControlToValidate="txtMinCTC"
                                ValidationGroup="ValOfferDetails" Display="Dynamic" ErrorMessage="Invalid Number of Min CTC"
                                Operator="DataTypeCheck" Type="Double"></asp:CompareValidator>
                            <asp:CompareValidator ID="cvCTCMax" runat="server" ControlToValidate="txtMaxCTC"
                                ValidationGroup="ValOfferDetails" Display="Dynamic" ErrorMessage="Invalid Number of Max CTC"
                                Operator="DataTypeCheck" Type="Double"></asp:CompareValidator>
                            <asp:CompareValidator ID="cmpCTCValidator" runat="server" ControlToValidate="txtMaxCTC"
                                ValidationGroup="ValOfferDetails" ControlToCompare="txtMinCTC" Display="Dynamic"
                                ErrorMessage="Minimum CTC should be less than or equal to maximum CTC" Operator="GreaterThanEqual"
                                Type="Double"></asp:CompareValidator>
                        </div>
                    </div>--%>
                    <div class="TableRow">
                        <div class="TableFormLeble">
                            Experience:
                        </div>
                        <div class="TableFormContent">
                            <ucl:experience id="uclExperience" runat="server" yearstabindex="8" monthtabindex="9"></ucl:experience>
                            <asp:TextBox EnableViewState="true" ID="txtExperience" runat="server" CssClass="CommonTextBox"
                                ValidationGroup="AdditionalInfo" Width="60px" MaxLength="5" onmouseover="this.title=this.value;"
                                Visible="false" TabIndex="8"></asp:TextBox>
                            <span class="RequiredField">*</span>
                        </div>
                    </div>
                    <div class="TableRow">
                        <div class="TableFormLeble">
                            Source:
                        </div>
                        <div class="TableFormContent">
                            <asp:DropDownList ID="ddlSource" runat="server" Width="100px" CssClass="CommonDropDownList"
                                EnableViewState="true" TabIndex="9">
                            </asp:DropDownList>
                            <span class="RequiredField">*</span>
                        </div>
                    </div>
                    <div class="TableRow">
                        <div class="TableFormValidatorContent" style="margin-left: 42%">
                            <asp:RequiredFieldValidator ID="rfvBU" runat="server" ControlToValidate="ddlSource"
                                SetFocusOnError="true" ErrorMessage="Please Select Source." InitialValue="0"
                                Display="Dynamic" ValidationGroup="ValOfferDetails"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="TableRow">
                        <div class="TableFormLeble">
                            Source Description:
                        </div>
                        <div class="TableFormContent">
                            <div id="divVendor" runat="server" style="display: none; float: left;">
                                <asp:DropDownList ID="ddlVendor" runat="server" CssClass="chzn-select" Width="90px"
                                    TabIndex="10">
                                </asp:DropDownList>
                                <asp:DropDownList ID="ddlVendorContact" runat="server" CssClass="chzn-select" Width="90px"
                                    TabIndex="11">
                                </asp:DropDownList>
                                <asp:HiddenField ID="hdnSelectedVendorContact" runat="server" Value="0" />
                            </div>
                            <div id="divEmpReferral" runat="server" style="display: none; float: left;">
                                <asp:DropDownList ID="ddlEmployeeReferrer" runat="server" CssClass="chzn-select"
                                    Width="120px" TabIndex="12">
                                </asp:DropDownList>
                            
                            </div>
                            
                            
                            <asp:DropDownList ID="ddlSourceDescription" runat="server" Width="100px" CssClass="CommonDropDownList"
                                EnableViewState="true" TabIndex="13">
                            </asp:DropDownList>
                            <div id="divsrcdesc" runat="server">
                            <asp:TextBox ID="txtsrcdesc" runat="server" CssClass="CommonTextBox" TabIndex="14"
                                Style="display: none"></asp:TextBox>
                            <span class="RequiredField">*</span>
                           
                           
                            </div>
                        </div>
                    </div>
                    <div class="TableRow">
                        <div class="TableFormValidatorContent" style="margin-left: 42%">
                            <asp:CustomValidator runat="server" Display="Dynamic" ValidationGroup="ValOfferDetails"
                                ID="CVSourceDescription" ErrorMessage="Please Select Source Description" ClientValidationFunction="ValidateSourceDescription"></asp:CustomValidator>
                        </div>
                    </div>
                    <div class="TableRow">
                        <div class="TableFormLeble">
                            Active Recruiter:
                        </div>
                        <div class="TableFormContent">
                            <asp:DropDownList ID="ddlActiveRecruiter" runat="server" Width="165px" CssClass="CommonDropDownList"
                                EnableViewState="true" TabIndex="15">
                            </asp:DropDownList>
                            <span class="RequiredField">*</span>
                        </div>
                    </div>
                </div>
                <div class="FormRightColumn">
                    <div class="TableRow">
                        <div class="TableFormLeble">
                            Fitment Percentile:</div>
                        <div class="TableFormContent">
                            <asp:DropDownList ID="ddlfitmentpercentile" runat="server" Width="100px" CssClass="CommonDropDownList"
                                TabIndex="16">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="TableRow">
                        <div class="TableFormLeble">
                            Joining Bonus:</div>
                        <div class="TableFormContent">
                            <asp:TextBox ID="txtjoiningbonus" runat="server" CssClass="CommonTextBox" TabIndex="17"
                                rel="Numeric" Text="0"></asp:TextBox>
                            <span class="RequiredField">*</span>
                        </div>
                    </div>
                    <div class="TableRow">
                        <div class="TableFormValidatorConent" style="margin-left: 44%">
                            <asp:RequiredFieldValidator ID="rfvJoiningbonus" runat="server" ControlToValidate="txtjoiningbonus"
                                ErrorMessage="Please enter Joining Bonus" Display="Dynamic" ValidationGroup="ValOfferDetails"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="TableRow">
                        <div class="TableFormLeble">
                            Relocation Allowance:</div>
                        <div class="TableFormContent">
                            <asp:TextBox ID="txtRelocationallowance" runat="server" rel="Numeric" CssClass="CommonTextBox"
                                Text="0" TabIndex="18"></asp:TextBox>
                            <span class="RequiredField">*</span>
                        </div>
                    </div>
                    <div class="TableRow">
                        <div class="TableFormValidatorConent" style="margin-left: 44%">
                            <asp:RequiredFieldValidator ID="rfvRelocationAllowance" runat="server" ControlToValidate="txtRelocationallowance"
                                ErrorMessage="Please enter Relocation Allowance " Display="Dynamic" ValidationGroup="ValOfferDetails"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="TableRow">
                        <div class="TableFormLeble">
                            Special Lump Sum:</div>
                        <div class="TableFormContent">
                            <asp:TextBox ID="txtSpecialLumpSum" runat="server" rel="Numeric" CssClass="CommonTextBox"
                                TabIndex="19" Text="0"></asp:TextBox>
                            <span class="RequiredField">*</span>
                        </div>
                    </div>
                    <div class="TableRow">
                        <div class="TableFormValidatorConent" style="margin-left: 44%">
                            <asp:RequiredFieldValidator ID="rfvSpecialLumpSum" runat="server" ControlToValidate="txtSpecialLumpSum"
                                ErrorMessage="Please enter Special Lump Sum" Display="Dynamic" ValidationGroup="ValOfferDetails"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="TableRow">
                        <div class="TableFormLeble">
                            Notice Period Payout:</div>
                        <div class="TableFormContent">
                            <asp:TextBox ID="txtNoticePeriodPayout" runat="server" CssClass="CommonTextBox" TabIndex="20"
                                rel="Numeric" Text="0"></asp:TextBox>
                            <span class="RequiredField">*</span>
                        </div>
                    </div>
                    <div class="TableRow">
                        <div class="TableFormValidatorConent" style="margin-left: 44%">
                            <asp:RequiredFieldValidator ID="rfvNoticePeriodPayout" runat="server" ControlToValidate="txtNoticePeriodPayout"
                                ErrorMessage="Please enter  Notice Period Payout" Display="Dynamic" ValidationGroup="ValOfferDetails"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="TableRow">
                        <div class="TableFormLeble">
                            Placement Fees:</div>
                        <div class="TableFormContent">
                            <asp:TextBox ID="txtPlacementFees" runat="server" rel="Numeric" CssClass="CommonTextBox"
                                TabIndex="21" Text="0"></asp:TextBox>
                            <span class="RequiredField">*</span>
                        </div>
                    </div>
                    <div class="TableRow">
                        <div class="TableFormValidatorConent" style="margin-left: 44%">
                            <asp:RequiredFieldValidator ID="rfvPlacementfees" runat="server" ControlToValidate="txtPlacementFees"
                                ErrorMessage="Please enter  Placement Fees" Display="Dynamic" ValidationGroup="ValOfferDetails"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="TableRow">
                        <div class="TableFormLeble">
                            BV Status:</div>
                        <div class="TableFormContent">
                            <asp:DropDownList ID="ddlBVstatus" runat="server" Width="80px" CssClass="CommonDropDownList"
                                EnableViewState="true" TabIndex="22">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
            </div>
            <div class="TableRow">
                <div class="TabPanelHeader">
                    Salary Details:</div>
                <div id="divSalaryDetails" runat="server">
                    <div class="TableRow">
                        <div class="TableFormLeble">
                            Offer Category:</div>
                        <div class="TableFormContent">
                            <asp:DropDownList ID="ddlOfferCategory" runat="server" Width="100px" CssClass="CommonDropDownList"
                                TabIndex="23">
                            </asp:DropDownList>
                            <span class="RequiredField">*</span>
                        </div>
                    </div>
                    <div class="TableRow">
                        <div class="TableFormLeble">
                            Offered Grade:</div>
                        <div class="TableFormContent">
                            <asp:DropDownList ID="ddlOfferedGrade" runat="server" Width="100px" CssClass="CommonDropDownList"
                                TabIndex="24">
                            </asp:DropDownList>
                            <asp:HiddenField ID="hdnSelectedOfferGrade" runat="server" />
                            <span class="RequiredField">*</span>
                        </div>
                    </div>
                    <div class="TableRow">
                        <div class="TableFormValidatorConent" style="margin-left: 44%">
                            <asp:RequiredFieldValidator ID="rfvGrade" runat="server" ControlToValidate="ddlOfferedGrade"
                                ErrorMessage="Please select Offered Grade" Display="Dynamic" ValidationGroup="ValOfferDetails"
                                InitialValue="0"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                </div>
                </dib>
                <div class="TabPanelHeader">
                    Salary Components:</div>
                <%--<div class="TableRow">
                     <div class="TableFormLeble">
                        Salary Components:</div> 
                    <div class="TableFormContent">
                        <asp:DropDownList ID="ddlSalaryComponents" runat="server" Width="100px" CssClass="CommonDropDownList">
                        </asp:DropDownList>
                </div>
                </div>--%>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div id="divEmbedded" runat="server">
        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>
                <div class="TableRow">
                    <div class="FormLeftColumn">
                        <div class="TableRow">
                            <div class="TableFormLeble">
                                Basic:
                            </div>
                            <div class="TableFormContent">
                                <asp:TextBox ID="txtBasicEmbedded" runat="server" CssClass="CommonTextBox" TabIndex="25"
                                    rel="Emb" Text="0"></asp:TextBox>
                                <span class="RequiredField">*</span>
                            </div>
                        </div>
                        <div class="TableRow">
                            <div class="TableFormLeble">
                                HRA:
                            </div>
                            <div class="TableFormContent">
                                <asp:TextBox ID="txtHRAEmbedded" runat="server" CssClass="CommonTextBox" TabIndex="26"
                                    rel="Emb" Text="0"></asp:TextBox>
                                <span class="RequiredField">*</span>
                            </div>
                        </div>
                        <div class="TableRow">
                            <div class="TableFormLeble">
                                Conveyence:
                            </div>
                            <div class="TableFormContent">
                                <asp:TextBox ID="txtConveyence" runat="server" CssClass="CommonTextBox" TabIndex="27"
                                    rel="Emb" Text="0"></asp:TextBox>
                                <span class="RequiredField">*</span>
                            </div>
                        </div>
                        <div class="TableRow">
                            <div class="TableFormLeble">
                                Medical:
                            </div>
                            <div class="TableFormContent">
                                <asp:TextBox ID="txtMedical" runat="server" CssClass="CommonTextBox" TabIndex="28"
                                    rel="Emb" Text="0"></asp:TextBox>
                                <span class="RequiredField">*</span>
                            </div>
                        </div>
                        <div class="TableRow">
                            <div class="TableFormLeble">
                                Educational Allowance:
                            </div>
                            <div class="TableFormContent">
                                <asp:TextBox ID="txtEducationalAllowanceEmbedded" runat="server" CssClass="CommonTextBox"
                                    rel="Emb" Text="0" TabIndex="29"></asp:TextBox>
                                <span class="RequiredField">*</span>
                            </div>
                        </div>
                        <div class="TableRow">
                            <div class="TableFormLeble">
                                LUFS:
                            </div>
                            <div class="TableFormContent">
                                <asp:TextBox ID="txtLUFS" runat="server" CssClass="CommonTextBox" TabIndex="30" rel="Emb"
                                    Text="0"></asp:TextBox>
                                <span class="RequiredField">*</span>
                            </div>
                        </div>
                        <div class="TableRow">
                            <div class="TableFormLeble">
                                Ad-hoc:
                            </div>
                            <div class="TableFormContent">
                                <asp:TextBox ID="txtAdhocEmbedded" runat="server" CssClass="CommonTextBox" TabIndex="31"
                                    rel="Emb" Text="0"></asp:TextBox>
                                <span class="RequiredField">*</span>
                            </div>
                        </div>
                        <div class="TableRow">
                            <div class="TableFormLeble">
                                Embedded Allowance:
                            </div>
                            <div class="TableFormContent">
                                <asp:TextBox ID="txtEmbeddedAllowance" runat="server" CssClass="CommonTextBox" TabIndex="32"
                                    rel="Emb" Text="0"></asp:TextBox>
                                <span class="RequiredField">*</span>
                            </div>
                        </div>
                        <div class="TableRow">
                            <div class="TableFormLeble">
                                PLM Allowance:
                            </div>
                            <div class="TableFormContent">
                                <asp:TextBox ID="txtPLMAllowanceEmbedded" runat="server" CssClass="CommonTextBox"
                                    TabIndex="33" rel="Emb" Text="0"></asp:TextBox>
                                <span class="RequiredField">*</span>
                            </div>
                        </div>
                    </div>
                    <div class="FormRightColumn">
                        <div class="TableRow">
                            <div class="TableFormLeble">
                                MPP:
                            </div>
                            <div class="TableFormContent">
                                <asp:TextBox ID="txtMPP" runat="server" CssClass="CommonTextBox" TabIndex="34" rel="Emb"
                                    Text="0"></asp:TextBox>
                                <span class="RequiredField">*</span>
                            </div>
                        </div>
                        <div class="TableRow">
                            <div class="TableFormLeble">
                                AGVI:
                            </div>
                            <div class="TableFormContent">
                                <asp:TextBox ID="txtAGVI" runat="server" CssClass="CommonTextBox" TabIndex="35" rel="Emb"
                                    Text="0"></asp:TextBox>
                                <span class="RequiredField">*</span>
                            </div>
                        </div>
                        <div class="TableRow">
                            <div class="TableFormLeble">
                                LTA:
                            </div>
                            <div class="TableFormContent">
                                <asp:TextBox ID="txtLTAEmbedded" runat="server" CssClass="CommonTextBox" TabIndex="36"
                                    rel="Emb" Text="0"></asp:TextBox>
                                <span class="RequiredField">*</span>
                            </div>
                        </div>
                        <div class="TableRow">
                            <div class="TableFormLeble">
                                PF:
                            </div>
                            <div class="TableFormContent">
                                <asp:TextBox ID="txtPFEmbedded" runat="server" CssClass="CommonTextBox" TabIndex="37"
                                    rel="Emb" Text="0"></asp:TextBox>
                                <span class="RequiredField">*</span>
                            </div>
                        </div>
                        <div class="TableRow">
                            <div class="TableFormLeble">
                                Gratuity:
                            </div>
                            <div class="TableFormContent">
                                <asp:TextBox ID="txtGratuityEmbedded" runat="server" CssClass="CommonTextBox" rel="Emb"
                                    Text="0" TabIndex="38"></asp:TextBox>
                                <span class="RequiredField">*</span>
                            </div>
                        </div>
                        <div class="TableRow">
                            <div class="TableFormLeble">
                                Maximun Annual Incentive:
                            </div>
                            <div class="TableFormContent">
                                <asp:TextBox ID="txtMaximunAnnualIncentive" runat="server" CssClass="CommonTextBox"
                                    rel="Emb" Text="0" TabIndex="39"></asp:TextBox>
                                <span class="RequiredField">*</span>
                            </div>
                        </div>
                        <div class="TableRow">
                            <div class="TableFormLeble">
                                Sales Incentive:
                            </div>
                            <div class="TableFormContent">
                                <asp:TextBox ID="txtSalesIncentiveEmbedded" runat="server" CssClass="CommonTextBox"
                                    TabIndex="40" rel="Emb" Text="0"></asp:TextBox>
                                <span class="RequiredField">*</span>
                            </div>
                        </div>
                        <div class="TableRow">
                            <div class="TableFormLeble">
                                Car Allowance:
                            </div>
                            <div class="TableFormContent">
                                <asp:TextBox ID="txtCarAllowanceEmbedded" runat="server" CssClass="CommonTextBox"
                                    TabIndex="41" rel="Emb" Text="0"></asp:TextBox>
                                <span class="RequiredField">*</span>
                            </div>
                        </div>
                        <div class="TableRow">
                            <div class="TableFormLeble">
                                Grand Total:
                            </div>
                            <div class="TableFormContent">
                                <asp:TextBox ID="txtGrandTotal" runat="server" CssClass="CommonTextBox" TabIndex="42"
                                    rel="EmbTotal" Text="0"></asp:TextBox>
                                <span class="RequiredField">*</span>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div id="divMech" runat="server" style="display: none">
        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
            <ContentTemplate>
                <div class="TableRow">
                    <div class="FormLeftColumn">
                        <div class="TableRow">
                            <div class="TableFormLeble">
                                Basic:
                            </div>
                            <div class="TableFormContent">
                                <asp:TextBox ID="txtBasic" runat="server" CssClass="CommonTextBox" TabIndex="25"
                                    rel="Mech" Text="0"></asp:TextBox>
                                <span class="RequiredField">*</span>
                            </div>
                        </div>
                        <div class="TableRow">
                            <div class="TableFormLeble">
                                Flexi Pay 1:
                            </div>
                            <div class="TableFormContent">
                                <asp:TextBox ID="txtFlexiPay1" runat="server" CssClass="CommonTextBox" TabIndex="26"
                                    rel="Mech" Text="0"></asp:TextBox>
                                <span class="RequiredField">*</span>
                            </div>
                        </div>
                        <div class="TableRow">
                            <div class="TableFormLeble">
                                Flexi Pay 2:
                            </div>
                            <div class="TableFormContent">
                                <asp:TextBox ID="txtFlexiPay2" runat="server" CssClass="CommonTextBox" TabIndex="27"
                                    rel="Mech" Text="0"></asp:TextBox>
                                <span class="RequiredField">*</span>
                            </div>
                        </div>
                        <div class="TableRow">
                            <div class="TableFormLeble">
                                Additional Allowance:
                            </div>
                            <div class="TableFormContent">
                                <asp:TextBox ID="txtAdditionalAllowance" runat="server" CssClass="CommonTextBox"
                                    rel="Mech" Text="0" TabIndex="28"></asp:TextBox>
                                <span class="RequiredField">*</span>
                            </div>
                        </div>
                        <div class="TableRow">
                            <div class="TableFormLeble">
                                Ad Hoc Allowance:
                            </div>
                            <div class="TableFormContent">
                                <asp:TextBox ID="txtAdHocAllowance" runat="server" CssClass="CommonTextBox" TabIndex="29"
                                    rel="Mech" Text="0"></asp:TextBox>
                                <span class="RequiredField">*</span>
                            </div>
                        </div>
                        <div class="TableRow">
                            <div class="TableFormLeble">
                                Location Allowance:
                            </div>
                            <div class="TableFormContent">
                                <asp:TextBox ID="txtLocationAllowance" runat="server" CssClass="CommonTextBox" TabIndex="30"
                                    rel="Mech" Text="0"></asp:TextBox>
                                <span class="RequiredField">*</span>
                            </div>
                        </div>
                        <div class="TableRow">
                            <div class="TableFormLeble">
                                SAF Allowance:
                            </div>
                            <div class="TableFormContent">
                                <asp:TextBox ID="txtSAFAllowance" runat="server" CssClass="CommonTextBox" TabIndex="31"
                                    rel="Mech" Text="0"></asp:TextBox>
                                <span class="RequiredField">*</span>
                            </div>
                        </div>
                        <div class="TableRow">
                            <div class="TableFormLeble">
                                HRA:
                            </div>
                            <div class="TableFormContent">
                                <asp:TextBox ID="txtHRA" runat="server" CssClass="CommonTextBox" TabIndex="32" rel="Mech"
                                    Text="0"></asp:TextBox>
                                <span class="RequiredField">*</span>
                            </div>
                        </div>
                        <div class="TableRow">
                            <div class="TableFormLeble">
                                H-LISA :
                            </div>
                            <div class="TableFormContent">
                                <asp:TextBox ID="txtHLISA" runat="server" CssClass="CommonTextBox" TabIndex="33"
                                    rel="Mech" Text="0"></asp:TextBox>
                                <span class="RequiredField">*</span>
                            </div>
                        </div>
                        <div class="TableRow">
                            <div class="TableFormLeble">
                                Education Allowance:
                            </div>
                            <div class="TableFormContent">
                                <asp:TextBox ID="txtEducationAllowance" runat="server" CssClass="CommonTextBox" TabIndex="34"
                                    rel="Mech" Text="0"></asp:TextBox>
                                <span class="RequiredField">*</span>
                            </div>
                        </div>
                        <div class="TableRow">
                            <div class="TableFormLeble">
                                ACLRA:
                            </div>
                            <div class="TableFormContent">
                                <asp:TextBox ID="txtACLRA" runat="server" CssClass="CommonTextBox" TabIndex="35"
                                    rel="Mech" Text="0"></asp:TextBox>
                                <span class="RequiredField">*</span>
                            </div>
                        </div>
                        <div class="TableRow">
                            <div class="TableFormLeble">
                                CLRA:
                            </div>
                            <div class="TableFormContent">
                                <asp:TextBox ID="txtCLRA" runat="server" CssClass="CommonTextBox" TabIndex="36" rel="Mech"
                                    Text="0"></asp:TextBox>
                                <span class="RequiredField">*</span>
                            </div>
                        </div>
                        <div class="TableRow">
                            <div class="TableFormLeble">
                                PLR:
                            </div>
                            <div class="TableFormContent">
                                <asp:TextBox ID="txtPLRMech" runat="server" CssClass="CommonTextBox" TabIndex="37"
                                    rel="Mech" Text="0"></asp:TextBox>
                                <span class="RequiredField">*</span>
                            </div>
                        </div>
                    </div>
                    <div class="FormRightColumn">
                        <div class="TableRow">
                            <div class="TableFormLeble">
                                Special Allowance:
                            </div>
                            <div class="TableFormContent">
                                <asp:TextBox ID="txtSpecialAllowance" runat="server" CssClass="CommonTextBox" TabIndex="38"
                                    rel="Mech" Text="0"></asp:TextBox>
                                <span class="RequiredField">*</span>
                            </div>
                        </div>
                        <div class="TableRow">
                            <div class="TableFormLeble">
                                Special Performance Pay:
                            </div>
                            <div class="TableFormContent">
                                <asp:TextBox ID="txtSpecialPerformancePay" runat="server" CssClass="CommonTextBox"
                                    rel="Mech" Text="0" TabIndex="39"></asp:TextBox>
                                <span class="RequiredField">*</span>
                            </div>
                        </div>
                        <div class="TableRow">
                            <div class="TableFormLeble">
                                ECAL:
                            </div>
                            <div class="TableFormContent">
                                <asp:TextBox ID="txtECAL" runat="server" CssClass="CommonTextBox" TabIndex="40" rel="Mech"
                                    Text="0"></asp:TextBox>
                                <span class="RequiredField">*</span>
                            </div>
                        </div>
                        <div class="TableRow">
                            <div class="TableFormLeble">
                                Car Mileage Reimbursement:
                            </div>
                            <div class="TableFormContent">
                                <asp:TextBox ID="txtCarMileageReimbursement" runat="server" CssClass="CommonTextBox"
                                    rel="Mech" Text="0" TabIndex="41"></asp:TextBox>
                                <span class="RequiredField">*</span>
                            </div>
                        </div>
                        <div class="TableRow">
                            <div class="TableFormLeble">
                                Telephone Reimbursement:
                            </div>
                            <div class="TableFormContent">
                                <asp:TextBox ID="txtTelephoneReimbursement" runat="server" CssClass="CommonTextBox"
                                    rel="Mech" Text="0" TabIndex="42"></asp:TextBox>
                                <span class="RequiredField">*</span>
                            </div>
                        </div>
                        <div class="TableRow">
                            <div class="TableFormLeble">
                                LTA:
                            </div>
                            <div class="TableFormContent">
                                <asp:TextBox ID="txtLTA" runat="server" CssClass="CommonTextBox" TabIndex="43" rel="Mech"
                                    Text="0"></asp:TextBox>
                                <span class="RequiredField">*</span>
                            </div>
                        </div>
                        <div class="TableRow">
                            <div class="TableFormLeble">
                                PF:
                            </div>
                            <div class="TableFormContent">
                                <asp:TextBox ID="txtPF" runat="server" CssClass="CommonTextBox" TabIndex="44" rel="Mech"
                                    Text="0"></asp:TextBox>
                                <span class="RequiredField">*</span>
                            </div>
                        </div>
                        <div class="TableRow">
                            <div class="TableFormLeble">
                                Gratuity:
                            </div>
                            <div class="TableFormContent">
                                <asp:TextBox ID="txtGratuity" runat="server" CssClass="CommonTextBox" TabIndex="45"
                                    rel="Mech" Text="0"></asp:TextBox>
                                <span class="RequiredField">*</span>
                            </div>
                        </div>
                        <div class="TableRow">
                            <div class="TableFormLeble">
                                Medical reimbursements - Domiciliary:
                            </div>
                            <div class="TableFormContent">
                                <asp:TextBox ID="txtMedicalreimbursementsDomiciliary" runat="server" CssClass="CommonTextBox"
                                    rel="Mech" Text="0" TabIndex="46"></asp:TextBox>
                                <span class="RequiredField">*</span>
                            </div>
                        </div>
                        <div class="TableRow">
                            <div class="TableFormLeble">
                                PC Scheme:
                            </div>
                            <div class="TableFormContent">
                                <asp:TextBox ID="txtPCScheme" runat="server" CssClass="CommonTextBox" TabIndex="47"
                                    rel="Mech" Text="0"></asp:TextBox>
                                <span class="RequiredField">*</span>
                            </div>
                        </div>
                        <div class="TableRow">
                            <div class="TableFormLeble">
                                Retention Pay:
                            </div>
                            <div class="TableFormContent">
                                <asp:TextBox ID="txtRetentionPay" runat="server" CssClass="CommonTextBox" TabIndex="48"
                                    rel="Mech" Text="0"></asp:TextBox>
                                <span class="RequiredField">*</span>
                            </div>
                        </div>
                        <div class="TableRow">
                            <div class="TableFormLeble">
                                SalesIncentive:
                            </div>
                            <div class="TableFormContent">
                                <asp:TextBox ID="txtSalesIncentiveMech" runat="server" CssClass="CommonTextBox" TabIndex="49"
                                    rel="Mech" Text="0"></asp:TextBox>
                                <span class="RequiredField">*</span>
                            </div>
                        </div>
                        <div class="TableRow">
                            <div class="TableFormLeble">
                                Total CTC:
                            </div>
                            <div class="TableFormContent">
                                <asp:TextBox ID="txtTotalCTC" runat="server" CssClass="CommonTextBox" TabIndex="50"
                                    rel="MechTotal" Text="0"></asp:TextBox>
                                <span class="RequiredField">*</span>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div class="TableRow" align="center">
        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="CommonButton"
            ValidationGroup="ValOfferDetails" />
    </div>
</div>

<script src="../assets/js/chosen.jquery.js" type="text/javascript"></script>

<script type="text/javascript"> 
   Sys.Application.add_load(function() { 
   $(".chzn-select").chosen();
  $(".chzn-select-deselect").chosen({allow_single_deselect:true});
  });
</script>

