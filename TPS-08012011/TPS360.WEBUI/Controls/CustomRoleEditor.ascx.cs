﻿using System;
using System.Web.UI.WebControls;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;

namespace TPS360.Web.UI
{
    public partial class ControlCustomRoleEditor : BaseControl
    {
        #region Member Variables

        CustomRole _customRole;

        #endregion

        #region Properties

        private CustomRole CurrentCustomRole
        {
            get
            {
                if (_customRole == null)
                {
                    if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_ID]))
                    {
                        int id = Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_ID]);

                        if (id > 0)
                        {
                            _customRole = Facade.GetCustomRoleById(id);
                        }
                    }

                    if (_customRole == null)
                    {
                        _customRole = new CustomRole();
                    }
                }

                return _customRole;
            }
        }

        #endregion

        #region Methods

        private string GetTabKey(int tabPosition)
        {
            string test = "editorTab,listTab";

            string[] tabs = test.Split(',');

            return tabs[tabPosition];
        }

        private void RedirectToList(string message, string tabKey)
        {
            string returnUrl = Helper.Url.SecureUrl.ReturnUrl;

            if (StringHelper.IsBlank(returnUrl))
            {
                returnUrl = UrlConstants.Admin.CUSTOM_ROLE_EDITOR_PAGE;
            }

            if (!StringHelper.IsBlank(message))
            {
                Helper.Url.Redirect(returnUrl, null, UrlConstants.PARAM_MSG, message, UrlConstants.PARAM_TAB, StringHelper.Convert(tabKey));
            }
            else
            {
                Helper.Url.Redirect(returnUrl);
            }
        }

        private void PrepareView()
        {
            CustomRole customRole = CurrentCustomRole;

            if (customRole != null)
            {
                txtName.Text =MiscUtil .RemoveScript ( customRole.Name);
                txtDescription.Text =MiscUtil .RemoveScript ( customRole.Description);
                ctlMenuSelector.SelectList = Facade.GetAllCustomRolePrivilegeIdsByRoleId(customRole.Id);

                SiteMapType[] siteMapTypes = new SiteMapType[] { SiteMapType.ApplicationTopMenu, 
                                                                SiteMapType.AdminPortalMenu, 
                                                                SiteMapType.EmployeePortalMenu, 
                                                                SiteMapType.CandidateCareerPortalMenu, 
                                                                };

                ctlMenuSelector.CurrentSiteMapTypes = siteMapTypes;
                ctlMenuSelector.BindList();
            }
        }

        private CustomRole BuildCustomRole()
        {
            CustomRole roleEditor = CurrentCustomRole;

            roleEditor.Name =MiscUtil .RemoveScript ( txtName.Text.Trim());
            roleEditor.Description =MiscUtil .RemoveScript ( txtDescription.Text.Trim());
            roleEditor.CreatorId = base.CurrentMember.Id;
            roleEditor.UpdatorId = base.CurrentMember.Id;

            return roleEditor;
        }

        private void SaveCustomRole()
        {
            if (IsValid)
            {
                try
                {
                    CustomRole customRole = BuildCustomRole();

                    if (customRole.IsNew)
                    {
                        customRole = Facade.AddCustomRole(customRole);
                        SaveMenuAccess(customRole);
                        RedirectToList("Role has been added successfully.", StringHelper.Convert(GetTabKey(1)));
                    }
                    else
                    {
                        customRole = Facade.UpdateCustomRole(customRole);
                        SaveMenuAccess(customRole);
                        RedirectToList("Role has been updated successfully.", StringHelper.Convert(GetTabKey(1)));
                    }
                }
                catch (ArgumentException ex)
                {
                    MiscUtil.ShowMessage(lblMessage, ex.Message, true);
                }
            }
        }

        private void SaveMenuAccess(CustomRole customRole)
        {
            TreeNodeCollection checkedNodes = ctlMenuSelector.SiteMapTree.CheckedNodes;

            Facade.DeleteCustomRolePrivilegeByRoleId(customRole.Id);

            foreach (TreeNode node in checkedNodes)
            {
                CustomRolePrivilege customRolePrivilege = new CustomRolePrivilege();

                customRolePrivilege.CustomSiteMapId = Convert.ToInt32(node.Value);
                customRolePrivilege.CustomRoleId = customRole.Id;
                Facade.AddCustomRolePrivilege(customRolePrivilege);
            }
        }

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                PrepareView();
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            SaveCustomRole();
        }

        #endregion
    }
}