﻿using System;
using System.Web.UI.WebControls;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using System.Collections.Generic;
using System.Collections;
using TPS360.BusinessFacade;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using TPS360.Web.UI.Helper;
using System.Linq;
namespace TPS360.Web.UI
{
    public partial class cltRequisitionEditor : RequisitionBaseControl
    {
        #region Member Variables

        private string _requisitionType;
        private string _ApplicationEdition
        {
            get
            {
                if (SiteSetting[DefaultSiteSetting.ApplicationEdition.ToString()] != null)
                    return SiteSetting[DefaultSiteSetting.ApplicationEdition.ToString()].ToString();
                else
                    return string.Empty;
            }
        }
        public string RequisitionType
        {
            set
            {
                this._requisitionType = value;
            }
        }

        private string DefaultCountryCode
        {
            get
            {
                return SiteSetting[DefaultSiteSetting.Country.ToString()].ToString();
            }

        }
        #endregion

        #region Methods
        private void PostbackCalls()
        {

            PopulateBUContactList();

            //  ControlHelper.SelectListByValue(ddlBUContact, hdnSelectedContactID.Value);
        }
        private void PopulateFormData(JobPosting jobPosting)
        {
            string date;
            if (!jobPosting.IsNew)
            {
                cmpStartDate.Enabled = false;
                txtJobTitle.Text = MiscUtil.RemoveScript(jobPosting.JobTitle, string.Empty);
                //-------------------------Rishi Code Start---------------------------------------//
                txtCandidateName.Text = MiscUtil.RemoveScript(jobPosting.CandidateName, string.Empty);
                txtProjectName.Text = MiscUtil.RemoveScript(jobPosting.ProjectName, string.Empty);
                txtCustInfo.Text = MiscUtil.RemoveScript(jobPosting.CustomerInfo, string.Empty);
                txtAreaReasonForHire.Text = MiscUtil.RemoveScript(jobPosting.ReasonForHire, string.Empty);
                txtRelExpMin.Text = MiscUtil.RemoveScript(jobPosting.RelMinExpRequired, string.Empty);
                txtRelExpMax.Text = MiscUtil.RemoveScript(jobPosting.RelMaxExpRequired, string.Empty);

                PopulateSecondarySkill(jobPosting.SecondarySkill);

                ddlGdcGsc.SelectedValue = jobPosting.GDCGSC.ToString();
                ddlIndivContribMgnt.SelectedValue = jobPosting.IndivContribMgnt.ToString();
                ddlDemandType.SelectedValue = jobPosting.DemandType.ToString();
                ddlPriority.SelectedValue = jobPosting.Priority.ToString();
                ddlBillability.SelectedValue = jobPosting.Billability.ToString();
                ddlCostCenter.SelectedValue = jobPosting.CostCenter.ToString();
                ddlRegion.SelectedValue = jobPosting.Region.ToString();
                ddlJobFamily.SelectedValue = jobPosting.JobFamily.ToString();
                //ddlDivision.SelectedValue = MiscUtil.RemoveScript(jobPosting.Division, string.Empty);
                ddlDivision.SelectedValue = jobPosting.Division.ToString();
                //ddlSuperOrgCode.SelectedValue = Convert.ToString(jobPosting.SuperOrgCode).ToString(); //commented by pravin on 8/Nov/2016

                 MiscUtil.PopulateSuperOrgCode(ddlSuperOrgCode, Facade,1);
                 ddlSuperOrgCode.SelectedValue = jobPosting.SuperOrgCode.ToString();
               
                //ddlSuperOrgCode.SelectedItem.Text = Convert.ToString(jobPosting.SuperOrgCode).ToString();

                 DivisionChangeEvent();
                 ddlReportingMgrGID.SelectedValue = jobPosting.ReportingManagerGID.ToString();

                if (jobPosting.Location != null)
                {
                    ddllocation.Visible = true;
                    ddllocation.Items.Clear();
                    ddllocation.DataSource = Facade.GetAllGenericLookupByLookupType(LookupType.VenueMaster);
                    ddllocation.DataTextField = "Name";
                    ddllocation.DataValueField = "Name";
                    ddllocation.DataBind();
                    ddllocation = (DropDownList)MiscUtil.RemoveScriptForDropDown(ddllocation);
                    ddllocation.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));

                    ddllocation.SelectedValue  = jobPosting.Location.ToString(); //added by pravin on 8/Nov/2016
                }
                //--------------------------Rishi Code End----------------------------------------// 
                if (jobPosting.NoOfOpenings == 0)
                {
                    txtNoOfOpenings.Text = "1";
                }
                else
                {
                    txtNoOfOpenings.Text = MiscUtil.RemoveScript(jobPosting.NoOfOpenings.ToString(), string.Empty);
                }
                if (string.IsNullOrEmpty(jobPosting.StartDate) || jobPosting.StartDate == "ASAP") //1.3
                {
                    chkStartDate.Checked = true;
                    //wdcStartDate.Enabled = false;
                }
                else
                {
                    chkStartDate.Checked = false;
                    Country coun = null;
                    int countryid = Convert.ToInt32(SiteSetting[DefaultSiteSetting.Country.ToString()].ToString());
                    coun = Facade.GetCountryById(countryid);
                    if (coun != null)
                    {
                        if (coun.Name == "India")
                        {
                            if (jobPosting.StartDate.ToString() == "" || jobPosting.StartDate.ToString().Trim() == "ASAP")
                                wdcStartDate.Value = jobPosting.StartDate.ToString();
                            else
                            {
                                date = jobPosting.StartDate.ToString().Split('/')[1] + "/" + jobPosting.StartDate.ToString().Split('/')[0] + "/" + jobPosting.StartDate.ToString().Split('/')[2];
                                wdcStartDate.Value = Convert.ToDateTime(jobPosting.StartDate);
                            }

                        }
                        else wdcStartDate.Value = jobPosting.StartDate;
                    }
                    else wdcStartDate.Value = jobPosting.StartDate;
                }

                if (jobPosting.IsNew)
                {
                    SiteSetting siteSetting = Facade.GetSiteSettingBySettingType((int)SettingType.SiteSetting);
                    if (siteSetting != null)
                    {
                        Hashtable siteSettingTable = ObjectEncrypter.Decrypt<Hashtable>(siteSetting.SettingConfiguration);
                        jobPosting.FinalHiredDate = DateTime.Now.AddDays(Convert.ToInt32(siteSettingTable[DefaultSiteSetting.FinalHireCompletionTimeframe.ToString()]));
                    }
                }
                else
                {
                    wdcClosingDate.Value = jobPosting.FinalHiredDate;
                }

                txtExpRequiredMin.Text = MiscUtil.RemoveScript(jobPosting.MinExpRequired, string.Empty);
                txtExpRequiredMax.Text = MiscUtil.RemoveScript(jobPosting.MaxExpRequired, string.Empty);
                uclEducationList.SelectedItems = jobPosting.RequiredDegreeLookupId;
                //if (!string.IsNullOrEmpty(jobPosting.RequiredDegreeLookupId ))
                //{
                //    string RequiredDegree = jobPosting.RequiredDegreeLookupId .ToString();
                //    string[] DegreeRequired = new string[20];
                //    char[] splitter = { ',' };
                //    DegreeRequired = RequiredDegree.Split(splitter);

                //    for (int i = 0; i < ddlQualification .Items .Count ; i++)
                //    {
                //        for (int x = 0; x < DegreeRequired.Length; x++)
                //        {
                //            if (ddlQualification.Items[i].Value == DegreeRequired[x].ToString())
                //            {
                //                ddlQualification.Items[i].Selected = true;
                //            }
                //        }
                //    }
                //}

                uclCountryState.SelectedCountryId = jobPosting.CountryId;
                uclCountryState.SelectedStateId = jobPosting.StateId;

                //txtCity.Text = MiscUtil .RemoveScript (jobPosting.City,string .Empty );
                txtZipCode.Text = MiscUtil.RemoveScript(jobPosting.ZipCode, string.Empty);
                txtSalary.Text = MiscUtil.RemoveScript(jobPosting.PayRate.ToString(), string.Empty);
                ddlSalaryCurrency.SelectedValue = jobPosting.PayRateCurrencyLookupId.ToString();
                ddlRequisitionType.SelectedValue = jobPosting.RequisitionType;

                //ddlbranch.SelectedValue = jobPosting.RequestionBranch;
                //ddlgrade.SelectedValue = jobPosting.RequestionGrade;
            }
            else
            {
                txtNoOfOpenings.Text = "1";
            }

            txtClientJobId.Text = jobPosting.ClientJobId == null ? "" : MiscUtil.RemoveScript(jobPosting.ClientJobId, string.Empty);

            PopulateSkill(jobPosting.JobSkillLookUpId);
            uclCountryState.SelectedStateId = jobPosting.StateId;
            if (!string.IsNullOrEmpty(Convert.ToString(jobPosting.OpenDate)))
            {
                wdcOpenDate.Value = jobPosting.OpenDate;
            }
            //************Added by pravin khot on 24/Jan/2017***********
            if (ddlDemandType.SelectedIndex > 0)
            {
                if (ddlDemandType.SelectedItem.Text != "Permanent")
                {
                    rfvClosingDate.Enabled = true;
                    DivResourceStartDate.Visible = true;
                    DivResourceEndtDate.Visible = true;
                }
                if (!string.IsNullOrEmpty(Convert.ToString(jobPosting.ResourceStartDate)))
                {
                    wdcResourceStartDate.Value = jobPosting.ResourceStartDate;
                }
                if (!string.IsNullOrEmpty(Convert.ToString(jobPosting.ResourceEndDate)))
                {
                    wdcResourceEndDate.Value = jobPosting.ResourceEndDate;
                }
            }
            //******************END***********************
            if (jobPosting.SalesGroupLookUpId != 0)
                ControlHelper.SelectListByValue(ddlSalesGroup, jobPosting.SalesGroupLookUpId.ToString());
            if (jobPosting.SalesRegionLookUpId != 0)
                ControlHelper.SelectListByValue(ddlSalesRegion, jobPosting.SalesRegionLookUpId.ToString());
            if (jobPosting.JobLocationLookUpID != 0)
                ControlHelper.SelectListByValue(ddlJobLocation, jobPosting.JobLocationLookUpID.ToString());
            if (jobPosting.ClientId != 0)
                ControlHelper.SelectListByValue(ddlBU, jobPosting.ClientId.ToString());
            if (jobPosting.JobCategoryLookupId != 0)
                ControlHelper.SelectListByValue(ddlJbCategory, jobPosting.JobCategoryLookupId.ToString());
            if (jobPosting.City != "")
            {
                //if (isMainApplication)
                //{
                //    txtCity.Text = MiscUtil.RemoveScript(jobPosting.City, string.Empty);
                //}
                //else {
                //    ControlHelper.SelectListByText(ddlCity, jobPosting.City);
                //}

                switch (ApplicationSource)
                {
                    case ApplicationSource.LandTApplication:
                        ControlHelper.SelectListByText(ddlCity, jobPosting.City);
                        break;
                    case ApplicationSource.MainApplication:
                        txtCity.Text = MiscUtil.RemoveScript(jobPosting.City, string.Empty);
                        break;
                    case ApplicationSource.SelectigenceApplication:
                        txtCity.Text = MiscUtil.RemoveScript(jobPosting.City, string.Empty);
                        break;
                    default:
                        txtCity.Text = MiscUtil.RemoveScript(jobPosting.City, string.Empty);
                        break;
                }
            }

            PopulateBUContactList();
            //ControlHelper.SelectListByValue(ddlBUContact, jobPosting.ClientContactId.ToString());
            // hdnSelectedContactID.Value = ddlBUContact.SelectedValue;

            if (jobPosting.ClientContactId > 0)
            {
                CompanyContact contact = Facade.GetCompanyContactById(jobPosting.ClientContactId);
                if (contact != null)
                {
                    hdnSelectedContactText.Value = txtBUcontact.Text = contact.FirstName + " " + contact.LastName;
                    hdnSelectedContactValue.Value = jobPosting.ClientContactId.ToString();

                }
            }

            ddlBUContacts.Visible = true;
            ddlBUContacts.Items.Clear();
            ddlBUContacts.DataSource = Facade.GetAllBUContactIdByBUId(Convert.ToInt32(ddlBU.SelectedValue));
            ddlBUContacts.DataTextField = "Name";
            ddlBUContacts.DataValueField = "Id";
            ddlBUContacts.DataBind();
            ddlBUContacts = (DropDownList)MiscUtil.RemoveScriptForDropDown(ddlBUContacts);
            ddlBUContacts.Items.Insert(0, new ListItem("Select Competency Manager", "0"));

            if (jobPosting.ClientContactId != 0)
                ControlHelper.SelectListByValue(ddlBUContacts, jobPosting.ClientContactId.ToString());

            //----------------------Rishi Code Start--------------------------------------//
            //*********[Rishi Jagati] - [Demand Form] – [14-07-2016] – Start *****************
            //DivisionChangeEvent();
            //*********[Rishi Jagati] - [Demand Form] – [14-07-2016] – End *****************
            ReportingMgrGIDChangeEvent();
            JobFamilyChangeEvent();
            SuperOrgCodeChangeEvent();           
                  
            //----------------------Rishi Code End--------------------------------------//

        }

        public void PopulateSkill(string skillids)
        {
            hfSkillSetNames.Value = "";
            IList<Skill> sk = SplitSkillValues(skillids, '!');
            foreach (Skill s in sk)
            {
                System.Web.UI.HtmlControls.HtmlGenericControl div = new System.Web.UI.HtmlControls.HtmlGenericControl("div");
                //div.InnerHtml = "<span>" + s.Name + "</span><button type='button' class='close' data-dismiss='alert'  onclick='javascript:Clicked(\"" + s.Name + "\")'>\u00D7</button>";
                div.InnerHtml = "<span>" + s.Name + "</span><button type='button' class='close' data-dismiss='alert'  onclick='javascript:Clicked(\"" + s.Name.Replace("&", "&amp;").Replace("\"", "~") + "\")'>\u00D7</button>";
                div.Attributes.Add("class", "EmailDiv");
                divContent.Controls.AddAt(0, div);
                //hfSkillSetNames.Value += "<item><skill>" + s.Name.Replace("&", "&amp;") + "</skill></item>";
                hfSkillSetNames.Value += "<item><skill>" + s.Name.Replace("&", "&amp;").Replace("\"", "~") + "</skill></item>";
            }
        }
        //--------------------------------------Rishi Code Start--------------------------------//
        public void PopulateSecondarySkill(string skillids)
        {
            hfSecondarySkillSetNames.Value = "";
            IList<Skill> sk = SplitSkillValues(skillids, '!');
            foreach (Skill s in sk)
            {
                System.Web.UI.HtmlControls.HtmlGenericControl div = new System.Web.UI.HtmlControls.HtmlGenericControl("div");
                div.InnerHtml = "<span>" + s.Name + "</span><button type='button' class='close' data-dismiss='alert'  onclick='javascript:SecondarySkillClicked(\"" + s.Name.Replace("&", "&amp;").Replace("\"", "~") + "\")'>\u00D7</button>";
                div.Attributes.Add("class", "secondarySkillDiv");
                divSecondarySkillContent.Controls.AddAt(0, div);
                hfSecondarySkillSetNames.Value += "<item><skill>" + s.Name.Replace("&", "&amp;").Replace("\"", "~") + "</skill></item>";
            }
        }
        //--------------------------------------Rishi Code End--------------------------------//
        public void postbackCall()
        {

            //string skilllistdy = hfSkillSetNames.Value;
            string skilllistdy = hfSkillSetNames.Value.Replace("amp;", "").Replace("~", "\"");
            string mn = Facade.Skill_GetandAddSkillIds("<SkillList>" + skilllistdy.Replace("&", "&amp;") + "</SkillList>", base.CurrentMember.Id);
            if (mn.Trim().EndsWith("!")) mn = mn.Substring(0, mn.Length - 1);
            PopulateSkill(mn);

            //-----------------------------Rishi Code Start-------------------------------------//
            string secondarySkilllistdy = hfSecondarySkillSetNames.Value.Replace("amp;", "").Replace("~", "\"");
            string ss = Facade.Skill_GetandAddSkillIds("<SkillList>" + secondarySkilllistdy.Replace("&", "&amp;") + "</SkillList>", base.CurrentMember.Id);
            if (ss.Trim().EndsWith("!")) ss = ss.Substring(0, ss.Length - 1);
            PopulateSecondarySkill(ss);
            //-----------------------------Rishi Code End---------------------------------------//


            // rfvcontactemail.Enabled = false;
            // rfvBUcontact.Visible  = false;
            //if (chkSalary .Checked )
            //{
            //    this.chkSalary.Checked = true;
            //    txtSalary.Enabled = false;
            //    txtMaxPayRate.Enabled = false;
            //    ddlSalary.Enabled = false;
            //    ddlSalaryCurrency.Enabled = false;
            //}
            //else
            //{
            //    txtSalary.Enabled = true;
            //    txtMaxPayRate.Enabled = true;
            //    ddlSalary.Enabled = true;
            //    ddlSalaryCurrency.Enabled = true;
            //    this.chkSalary.Checked = false;
            //    if (ddlSalaryCurrency.SelectedItem.Text == "INR")
            //    {
            //        lblPayrateCurrency.Visible = true;
            //        lblPayrateCurrency.Text = " Lacs";
            //    }
            //    else
            //        lblPayrateCurrency.Text = "";
            //}
            PopulateBUContactList();
            //ControlHelper.SelectListByValue(ddlBUContact, hdnSelectedContactID.Value);

            uclCountryState.SelectedCountryId = uclCountryState.SelectedCountryId;
            uclCountryState.SelectedStateId = uclCountryState.SelectedStateId;

        }
        private IList<Skill> SplitSkillValues(string value, char delim)
        {
            IList<Skill> result = new List<Skill>();
            char[] delimiters = new char[] { delim, '\n' };
            IList<string> splitarray = value.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            foreach (string s in splitarray)
            {
                if (Convert.ToInt32(s) > 0)
                {
                    Skill sk = new Skill();
                    sk.Id = Convert.ToInt32(s);
                    sk.Name = Facade.GetSkillById(Convert.ToInt32(s)).Name;
                    result.Add(sk);
                }
            }
            return result;
        }
        public JobPosting BuildJobPosting(JobPosting jobPosting)
        {
            jobPosting.JobTitle = MiscUtil.RemoveScript(txtJobTitle.Text.Trim());

            //-----------------------Rishi Code Start---------------------------------//
            jobPosting.CandidateName = MiscUtil.RemoveScript(txtCandidateName.Text.Trim());
            jobPosting.ProjectName = MiscUtil.RemoveScript(txtProjectName.Text.Trim());
            jobPosting.CustomerInfo = MiscUtil.RemoveScript(txtCustInfo.Text.Trim());
            jobPosting.ReasonForHire = MiscUtil.RemoveScript(txtAreaReasonForHire.Text.Trim());
            jobPosting.RelMinExpRequired = MiscUtil.RemoveScript(txtRelExpMin.Text.Trim());
            jobPosting.RelMaxExpRequired = MiscUtil.RemoveScript(txtRelExpMax.Text.Trim());
            //txtJobTitle.Text = jobPosting.JobTitle;

            string secndSkilllistdy = hfSecondarySkillSetNames.Value.Replace("~", "\"");
            string ss = Facade.Skill_GetandAddSkillIds("<SkillList>" + secndSkilllistdy.Replace("&", "&amp;") + "</SkillList>", base.CurrentMember.Id);

            if (ss.Trim().EndsWith("!")) ss = ss.Substring(0, ss.Length - 1);
            jobPosting.SecondarySkill = ss;

            jobPosting.GDCGSC = Convert.ToInt32(ddlGdcGsc.SelectedValue);
            jobPosting.IndivContribMgnt = Convert.ToInt32(ddlIndivContribMgnt.SelectedValue);
            jobPosting.DemandType = Convert.ToInt32(ddlDemandType.SelectedValue);
            jobPosting.Priority = Convert.ToInt32(ddlPriority.SelectedValue);
            jobPosting.Billability = Convert.ToInt32(ddlBillability.SelectedValue);
            jobPosting.CostCenter = Convert.ToString(ddlCostCenter.SelectedValue);
            //jobPosting.CostCenter = Convert.ToInt32(ddlCostCenter.SelectedValue);
            jobPosting.Region = Convert.ToInt32(ddlRegion.SelectedValue);
            jobPosting.JobFamily = Convert.ToInt32(ddlJobFamily.SelectedValue);
            //jobPosting.Division = Convert.ToString(ddlDivision.SelectedValue);
            jobPosting.Division = Convert.ToString(ddlDivision.SelectedValue);
            jobPosting.SuperOrgCode = Convert.ToString(ddlSuperOrgCode.SelectedValue);//commeneted by pravin khot on 8/Nov/2016
            //jobPosting.SuperOrgCode = Convert.ToString(ddlSuperOrgCode.SelectedItem.Text);
            //jobPosting.ReportingManagerGID = Convert.ToString(ddlReportingMgrGID.SelectedItem.Text);
            jobPosting.ReportingManagerGID = Convert.ToString(ddlReportingMgrGID.SelectedValue);

            // Vishal Tripathy starts
            jobPosting.Grade = Convert.ToString(ddlGrade.SelectedItem.Text);
            // Vishal Tripathy ends

            //********************Code modify by pravin khot on 11/Nov/2016********           

            jobPosting.Location = Convert.ToString(ddllocation.SelectedItem.Text);
            //jobPosting.Location= ddllocation.SelectedValue.ToString() ; ;
            //**************************End*******************
            //-----------------------Rishi Code End-----------------------------------//

            //jobPosting.JobCategorySubId = 3;
            //jobPosting.JobCategoryLookupId = 0; 
            // jobPosting.JobIndustryLookupId = 0;
            jobPosting.NoOfOpenings = Int32.Parse(string.IsNullOrEmpty(txtNoOfOpenings.Text.Trim()) ? "1" : txtNoOfOpenings.Text.Trim());                //1.1
            //wdcStartDate.Enabled = !chkStartDate.Checked;
            jobPosting.StartDate = (chkStartDate.Checked) ? "ASAP" : wdcStartDate.Text.Trim() == "" ? "" : Convert.ToDateTime(wdcStartDate.Value).ToString("MM/dd/yyyy");
            //jobPosting.OpenDate = Convert.ToDateTime(wdcOpenDate.Value).ToString("MM/dd/yyyy");

            if (!string.IsNullOrEmpty(wdcClosingDate.Text) && wdcClosingDate.Text != "Null")
            {
                jobPosting.FinalHiredDate = DateTime.Parse(wdcClosingDate.Text);
            }
            else
                jobPosting.FinalHiredDate = DateTime.MinValue;

            jobPosting.MinExpRequired = MiscUtil.RemoveScript(txtExpRequiredMin.Text.Trim());
            jobPosting.MaxExpRequired = MiscUtil.RemoveScript(txtExpRequiredMax.Text.Trim());
            string Qualification = uclEducationList.SelectedItems;
            //for (int i = 0; i < ddlQualification .Items .Count ; i++)
            //{
            //    if (ddlQualification.Items[i].Selected)
            //    {
            //        Qualification += "," + ddlQualification.Items[i].Value;
            //    }
            //}
            jobPosting.RequiredDegreeLookupId = Qualification == string.Empty ? string.Empty : Qualification + ",";
            string Duration = string.Empty;
            jobPosting.JobDurationLookupId = Duration == string.Empty ? string.Empty : Duration + ",";
            jobPosting.JobDurationMonth = "";

            jobPosting.JobAddress1 = "";
            jobPosting.JobAddress2 = "";
            jobPosting.CountryId = uclCountryState.SelectedCountryId;
            jobPosting.StateId = uclCountryState.SelectedStateId;
            jobPosting.ZipCode = MiscUtil.RemoveScript(txtZipCode.Text.Trim());
            jobPosting.PayRate = MiscUtil.RemoveScript(txtSalary.Text.Trim());
            jobPosting.PayRateCurrencyLookupId = Int32.Parse(ddlSalaryCurrency.SelectedValue);
            //Work Authorization
            jobPosting.AuthorizationTypeLookupId = "";
            jobPosting.TravelRequired = false;
            jobPosting.TravelRequiredPercent = "0";
            jobPosting.ClientJobId = MiscUtil.RemoveScript(txtClientJobId.Text);
            jobPosting.OtherBenefits = ""; ;
            jobPosting.TeleCommunication = false;
            jobPosting.BUContactText = MiscUtil.RemoveScript(txtContactEmail.Text);
            if (jobPosting.IsNew)
            {
                IList<GenericLookup> RequisitionStatusList = Facade.GetAllGenericLookupByLookupTypeAndName(LookupType.RequisitionStatus, JobStatus.Draft.ToString());
                if (RequisitionStatusList != null)
                    jobPosting.JobStatus = RequisitionStatusList[0].Id;
            }
            jobPosting.PostedDate = DateTime.Now;
            jobPosting.IsJobActive = false;
            jobPosting.IsTemplate = false;

            if (_requisitionType == UIConstants.TEMPLATE_REQUISITION)
            {
                jobPosting.IsTemplate = true;
            }
            if (jobPosting.CreatorId == 0)
                jobPosting.CreatorId = base.CurrentMember.Id;
            jobPosting.UpdatorId = base.CurrentMember.Id;
            jobPosting.IsExpensesPaid = false;
            jobPosting.RequisitionSource = string.Empty;
            jobPosting.EmailSubject = string.Empty;
            jobPosting.RawDescription = string.Empty;

            //**************added by pravin on 24/Jan/2016*************
            if (!string.IsNullOrEmpty(wdcResourceStartDate.Text) && wdcResourceStartDate.Text != "Null")
            {
                jobPosting.ResourceStartDate = DateTime.Parse(wdcResourceStartDate.Text);
            }
            else
            {
                jobPosting.ResourceStartDate = DateTime.MinValue;
            }
            if (!string.IsNullOrEmpty(wdcResourceEndDate.Text) && wdcResourceEndDate.Text != "Null")
            {
                jobPosting.ResourceEndDate = DateTime.Parse(wdcResourceEndDate.Text);
            }
            else
            {
                jobPosting.ResourceEndDate = DateTime.MinValue; 
            }
            //****************END*************************

            if (!string.IsNullOrEmpty(wdcOpenDate.Text) && wdcOpenDate.Text != "Null")
            {
                jobPosting.OpenDate = DateTime.Parse(wdcOpenDate.Text);
            }
            if (ddlSalesRegion.SelectedIndex != 0)
            {
                jobPosting.SalesRegionLookUpId = Convert.ToInt32(ddlSalesRegion.SelectedValue);
            }
            if (ddlSalesGroup.SelectedIndex != 0)
            {
                jobPosting.SalesGroupLookUpId = Convert.ToInt32(ddlSalesGroup.SelectedValue);
            }
            if (ddlJbCategory.SelectedIndex != 0)
            {
                jobPosting.JobCategoryLookupId = Convert.ToInt32(ddlJbCategory.SelectedValue);
            }
            if (ddlBU.SelectedIndex != 0)
            {
                jobPosting.ClientId = Convert.ToInt32(ddlBU.SelectedValue);
            }
            if (ddlJobLocation.SelectedIndex != 0)
            {
                jobPosting.JobLocationLookUpID = Convert.ToInt32(ddlJobLocation.SelectedValue);
            }
            jobPosting.ReportingManagerName = Convert.ToString(ddlReportingMgrGID.SelectedItem.Value);
            //if (isMainApplication)
            //{
            //    jobPosting.City = MiscUtil.RemoveScript(txtCity.Text.Trim());

            //}
            //else
            //{
            //    if (ddlCity.SelectedIndex != 0)
            //    {
            //        jobPosting.City = ddlCity.SelectedItem.ToString();
            //    }
            //}


            switch (ApplicationSource)
            {
                case ApplicationSource.LandTApplication:
                    if (ddlCity.SelectedIndex != 0)
                    {
                        jobPosting.City = ddlCity.SelectedItem.ToString();
                    }
                    break;
                case ApplicationSource.MainApplication:
                    jobPosting.City = MiscUtil.RemoveScript(txtCity.Text.Trim());
                    break;
                default:
                    jobPosting.City = MiscUtil.RemoveScript(txtCity.Text.Trim());
                    break;
            }
          
            //********Code modify by pravin khot on 8/Aug/2016**************
            //jobPosting.ClientContactId = getContactID();// Convert.ToInt32(hdnSelectedContactID.Value == "" ? "0" : hdnSelectedContactID.Value);
            jobPosting.ClientContactId = Convert.ToInt32(ddlBUContacts.SelectedValue);
            //***********************END**********************************

            //string skilllistdy = hfSkillSetNames.Value;
            string skilllistdy = hfSkillSetNames.Value.Replace("~", "\"");
            string mn = Facade.Skill_GetandAddSkillIds("<SkillList>" + skilllistdy.Replace("&", "&amp;") + "</SkillList>", base.CurrentMember.Id);

            if (mn.Trim().EndsWith("!")) mn = mn.Substring(0, mn.Length - 1);
            jobPosting.JobSkillLookUpId = mn;
            jobPosting.RequisitionType = ddlRequisitionType.SelectedValue;

            //jobPosting.RequestionBranch = ddlbranch.SelectedValue;
            //jobPosting.RequestionGrade = ddlgrade.SelectedValue;

            return jobPosting;
        }

        public int getContactID()
        {
            if (hdnSelectedContactText.Value.Trim() == txtBUcontact.Text.Trim()) return Convert.ToInt32(hdnSelectedContactValue.Value == string.Empty ? "0" : hdnSelectedContactValue.Value);
            else
            {
                Random randno = new Random();

                string placeholderemail = randno.Next() + "@placeholder.com";
                if (txtContactEmail.Text != string.Empty) placeholderemail = txtContactEmail.Text;
                System.Web.Security.MembershipUser user = System.Web.Security.Membership.GetUser(placeholderemail);
                while (user != null)
                {
                    placeholderemail = randno.Next() + "@placeholder.com";
                    user = System.Web.Security.Membership.GetUser(placeholderemail);
                }
                // user = System.Web.Security.Membership.CreateUser(placeholderemail, "changeme");
                char[] delim = { ' ' };
                string[] cname = txtBUcontact.Text.Split(delim, StringSplitOptions.RemoveEmptyEntries);
                string firstname = "";
                string lastname = "";
                if (cname.Length == 1)
                {
                    firstname = cname[0];
                }
                else
                {
                    firstname = cname[0];
                    lastname = txtBUcontact.Text.Trim().Substring(txtBUcontact.Text.Trim().IndexOf(' '));
                }

                int newmemberid = RegisterUser(placeholderemail, firstname, lastname);
                int companyContactId = BuildContact(firstname, lastname, Convert.ToInt32(ddlBU.SelectedValue), Convert.ToInt32(newmemberid));
                hdnSelectedContactText.Value = txtBUcontact.Text;
                hdnSelectedContactValue.Value = companyContactId.ToString();
                txtContactEmail.Text = "";
                return companyContactId;
            }
        }
        private int BuildContact(string firstname, string lastname, int companyid, int memberid)
        {
            CompanyContact companyContact = new CompanyContact();
            companyContact.FirstName = MiscUtil.RemoveScript(firstname);
            companyContact.LastName = MiscUtil.RemoveScript(lastname);
            companyContact.Title = "";
            companyContact.Email = txtContactEmail.Text == string.Empty ? "" : txtContactEmail.Text;
            companyContact.Address1 = "";
            companyContact.Address2 = "";
            companyContact.CountryId = 0;
            companyContact.StateId = 0;
            companyContact.City = "";
            companyContact.ZipCode = "";
            companyContact.OfficePhone = "";
            companyContact.OfficePhoneExtension = "";
            companyContact.MobilePhone = "";
            companyContact.Fax = "";
            companyContact.IsPrimaryContact = false;
            companyContact.NoBulkEmail = false;
            companyContact.ContactRemarks = "";
            companyContact.CreatorId = base.CurrentMember.Id;
            companyContact.UpdatorId = base.CurrentMember.Id;
            companyContact.IsOwner = true;
            companyContact.OwnershipPercentage = 0;
            companyContact.EthnicGroupLookupId = 0;
            companyContact.CompanyId = companyid;
            companyContact.MemberId = memberid;
            companyContact = Facade.AddCompanyContact(companyContact);
            return companyContact.Id;
        }

        private int RegisterUser(string email, string firstname, string lastname)
        {
            string strMessage = String.Empty;
            bool IsCompany = false;
            SecureUrl url;
            string SecureURL;
            string _role = "Department Contact";
            if (IsValid)
            {
                if (_role != "")
                {
                    try
                    {
                        System.Web.Security.MembershipUser newUser;
                        newUser = System.Web.Security.Membership.CreateUser(email, "vendoraccess", email.ToLower());
                        if (newUser.IsApproved == true)
                        {
                            if (!System.Web.Security.Roles.RoleExists(_role))
                            {
                                System.Web.Security.Roles.CreateRole(_role);
                                CustomRole ro = Facade.GetCustomRoleByName(_role);
                                if (ro == null)
                                {
                                    CustomRole rol = new CustomRole();
                                    rol.Name = _role;
                                    rol.CreatorId = base.CurrentMember.Id;
                                    Facade.AddCustomRole(rol);
                                }
                            }
                            try
                            {
                                System.Web.Security.Roles.AddUserToRole(newUser.UserName, _role);

                            }
                            catch
                            {
                            }
                            Member newMember = new Member();
                            newMember.UserId = (Guid)newUser.ProviderUserKey;
                            newMember.ResumeSource = Convert.ToInt32(ResumeSource.SelfRegistration);
                            newMember.PermanentCountryId = 0;
                            newMember.PermanentStateId = 0;
                            newMember.IsRemoved = false;
                            newMember.AutomatedEmailStatus = true;
                            newMember.FirstName = firstname;
                            newMember.LastName = lastname;
                            newMember.DateOfBirth = DateTime.MinValue;
                            newMember.PrimaryEmail = email;
                            newMember.Status = (int)MemberStatus.Active;

                            IsCompany = true;
                            newMember.ResumeSource = (int)ResumeSource.Client;

                            newMember.CreatorId = base.CurrentMember.Id;
                            newMember.CreateDate = DateTime.Now;
                            newMember.UpdatorId = base.CurrentMember.Id;
                            newMember.UpdateDate = DateTime.Now;
                            newMember = Facade.AddMember(newMember);



                            MemberDetail newMemberDetail = new MemberDetail();
                            newMemberDetail.MemberId = newMember.Id;
                            newMemberDetail.CurrentStateId = 0;
                            newMemberDetail.CurrentCountryId = 0;
                            newMemberDetail.GenderLookupId = 0;
                            newMemberDetail.EthnicGroupLookupId = 0;
                            newMemberDetail.MaritalStatusLookupId = 0;
                            newMemberDetail.NumberOfChildren = 0;
                            newMemberDetail.BloodGroupLookupId = 0;
                            newMemberDetail.AnniversaryDate = DateTime.MinValue;

                            Facade.AddMemberDetail(newMemberDetail);

                            //--Trace
                            MemberExtendedInformation memberExtended = new MemberExtendedInformation();
                            memberExtended.MemberId = newMember.Id;
                            memberExtended.Availability = ContextConstants.MEMBER_DEFAULT_AVAILABILITY;
                            Facade.AddMemberExtendedInformation(memberExtended);


                            //  Trace Tech
                            MemberObjectiveAndSummary m_memberObjectiveAndSummary = new MemberObjectiveAndSummary();
                            m_memberObjectiveAndSummary.CreatorId = base.CurrentMember.Id;
                            m_memberObjectiveAndSummary.UpdatorId = base.CurrentMember.Id;
                            m_memberObjectiveAndSummary.CopyPasteResume = "";
                            m_memberObjectiveAndSummary.CreateDate = DateTime.Today;
                            m_memberObjectiveAndSummary.UpdateDate = DateTime.Today;
                            m_memberObjectiveAndSummary.RawCopyPasteResume = "";// StripTagsCharArray(m_memberObjectiveAndSummary.CopyPasteResume);
                            m_memberObjectiveAndSummary.MemberId = newMember.Id;
                            Facade.AddMemberObjectiveAndSummary(m_memberObjectiveAndSummary);





                            MemberCustomRoleMap map = Facade.GetMemberCustomRoleMapByMemberId(newMember.Id);
                            CustomRole role = Facade.GetCustomRoleByName(_role.ToString());
                            int roleid = 0;
                            if (role != null)
                                roleid = role.Id;

                            if (map == null)
                            {
                                map = new MemberCustomRoleMap();
                                map.CustomRoleId = roleid;
                                map.MemberId = newMember.Id;
                                map.CreatorId = base.CurrentMember.Id;

                                Facade.AddMemberCustomRoleMap(map);
                                AddDefaultMenuAccess(newMember.Id, roleid);
                            }
                            else
                            {
                                if (map.CustomRoleId == roleid)
                                {
                                    map = new MemberCustomRoleMap();
                                    map.CustomRoleId = roleid;
                                    map.UpdatorId = base.CurrentMember.Id;
                                    Facade.UpdateMemberCustomRoleMap(map);
                                    AddDefaultMenuAccess(newMember.Id, roleid);
                                }
                            }
                            return newMember.Id;
                        }
                        newUser.IsApproved = false;
                        System.Web.Security.Membership.UpdateUser(newUser);

                    }
                    catch (System.Web.Security.MembershipCreateUserException ex)
                    {

                    }

                }
                else
                {
                    MiscUtil.ShowMessage(lblMessage, "No role specified.", true);
                }
            }
            return 0;
        }
        private void AddDefaultMenuAccess(int memId, int role)
        {

            ArrayList previlegeList = Facade.GetAllCustomRolePrivilegeIdsByRoleId(role);

            if (previlegeList != null && previlegeList.Count > 0)
            {
                Facade.DeleteMemberPrivilegeByMemberId(memId);

                for (int i = 0; i <= previlegeList.Count - 1; i++)
                {
                    MemberPrivilege previlege = new MemberPrivilege();

                    previlege.CustomSiteMapId = Convert.ToInt32(previlegeList[i]);
                    previlege.MemberId = memId;
                    Facade.AddMemberPrivilege(previlege);
                }
            }
        }
        public void PopulateBUList()
        {

            int CompanyCount = Facade.Company_GetCompanyCount();
            {
                //if (isMainApplication) 
                //{

                //    if (CompanyCount == 0) 
                //    {
                //        ddlBU.Enabled = false;
                //        txtBUcontact.Enabled = false;
                //    }
                //}
                int companyStatus = (int)MiscUtil.GetCompanyStatusFromDefaultSiteSetting(SiteSetting);
                if (CompanyCount <= 500)
                {
                    ddlBU.Visible = true;
                    //txtClientName.Visible = false;
                    ddlBU.Items.Clear();
                    ddlBU.DataSource = Facade.GetAllClientsByStatus(companyStatus);

                    ddlBU.DataTextField = "CompanyName";
                    ddlBU.DataValueField = "Id";
                    ddlBU.DataBind();
                    ddlBU = (DropDownList)MiscUtil.RemoveScriptForDropDown(ddlBU);
                    ddlBU.Items.Insert(0, new ListItem("Select " + lblBU.Text, "0"));

                    int buid = Convert.ToInt32(ddlBU.SelectedValue);

                    ddlBUContacts.Visible = true;
                    ddlBUContacts.Items.Clear();
                    ddlBUContacts.DataSource = Facade.GetAllBUContactIdByBUId(buid);
                    ddlBUContacts.DataTextField = "Name";
                    ddlBUContacts.DataValueField = "Id";
                    ddlBUContacts.DataBind();
                    ddlBUContacts = (DropDownList)MiscUtil.RemoveScriptForDropDown(ddlBUContacts);
                    ddlBUContacts.Items.Insert(0, new ListItem("Select " + lblBUContacts.Text, "0"));
 
                }
                else
                {
                    ddlBU.Visible = false;
                    //txtClientName.Visible = true;
                }
            }
            PopulateBUContactList();
        }

        private void PopulateBUContactList()
        {
            //ddlBUContact.SelectedIndex = 0;


            ArrayList contacts = Facade.GetAllCompanyContactsByCompanyId(ddlBU.Visible ? Convert.ToInt32(ddlBU.SelectedValue == "" ? "0" : ddlBU.SelectedValue) : Convert.ToInt32(hdnSelectedClient.Value == "" ? "0" : hdnSelectedClient.Value));
            CompanyContact con = new CompanyContact();
            con.Id = 0;
            con.FirstName = "Please Select";
            contacts.Insert(0, con);
            //ddlBUContact.DataSource = contacts;
            //ddlBUContact.DataValueField = "Id";
            //ddlBUContact.DataTextField = "FirstName";

            //ddlBUContact.DataBind();
            //ddlBUContact = (DropDownList)MiscUtil.RemoveScriptForDropDown(ddlBUContact);
            //if (ddlBUContact.Items.Count == 1) ddlBUContact.Enabled = false;
            // else ddlBUContact.Enabled = true;


        }
        public void PopulateCurrencyLookUp()
        {
            ddlCurrency.DataSource = Facade.GetAllGenericLookupByLookupType(LookupType.Currency);
            ddlCurrency.DataTextField = "Name";
            ddlCurrency.DataValueField = "Id";
            ddlCurrency.DataBind();
        }



        private void SaveJobPostingData(JobPosting jobPosting)
        {
            //IList<JobPostingDocument> documents = getAtachedDocumentList(CurrentJobPostingId );
            if (IsValid)
            {
                try
                {
                    BuildJobPosting(jobPosting);

                    if (jobPosting.IsNew)
                    {
                        jobPosting = Facade.AddJobPosting(jobPosting);

                        MiscUtil.EventLogForRequisition(EventLogForRequisition.RequisitionCreated, jobPosting.Id, CurrentMember.Id, Facade);
                        if (_requisitionType != UIConstants.TEMPLATE_REQUISITION)
                            Helper.Url.Redirect(UrlConstants.Requisition.JOB_POSTING_DESCRIPTION_PAGE, string.Empty, UrlConstants.PARAM_JOB_ID, StringHelper.Convert(jobPosting.Id), UrlConstants.PARAM_MSG, "Requisition has been added successfully.");     //0.1
                        else
                            Helper.Url.Redirect(UrlConstants.Requisition.JOB_POSTING_TEMPLATE_EDITOR_PAGE, string.Empty, UrlConstants.PARAM_ID, StringHelper.Convert(jobPosting.Id), UrlConstants.PARAM_MSG, "Requisition has been added successfully.");
                    }
                    else
                    {

                        Facade.UpdateJobPosting(jobPosting);
                        MiscUtil.EventLogForRequisition(EventLogForRequisition.RequisitionEdited, jobPosting.Id, CurrentMember.Id, Facade);
                        MiscUtil.ShowMessage(lblMessage, "Requisition has been updated successfully.", false);
                    }

                }
                catch (ArgumentException ax)
                {
                    MiscUtil.ShowMessage(lblMessage, ax.Message, true);
                }
            }
        }

        private void PrepareView()
        {
            MiscUtil.PopulateEducationQualification(uclEducationList.ListItem, Facade);
            uclEducationList.ListItem.Items.RemoveAt(0);
            //ddlQualification.Items.RemoveAt(0);
            MiscUtil.PopulateCurrency(ddlSalaryCurrency, Facade);
            MiscUtil.PopulateSalesRegion(ddlSalesRegion, Facade);
            MiscUtil.PopulateSalesGroup(ddlSalesGroup, Facade);
            MiscUtil.PopulateRequisitionType(ddlRequisitionType, Facade);

            //-------Vishal Tripathy Start
            MiscUtil.PopulateGrade(ddlGrade, Facade);
            //-------Vishal Tripathy End

            //-----------------------------Rishi Code Start-------------------------------//
            MiscUtil.PopulateGDCGSC(ddlGdcGsc, Facade);
            MiscUtil.PopulateIndivContribMgnt(ddlIndivContribMgnt, Facade);
            MiscUtil.PopulateDemandType(ddlDemandType, Facade);
            MiscUtil.PopulatePriority(ddlPriority, Facade);
            MiscUtil.PopulateBillability(ddlBillability, Facade);
            MiscUtil.PopulateCostCenter(ddlCostCenter, Facade);
            MiscUtil.PopulateRegion(ddlRegion, Facade);
            MiscUtil.PopulateJobFamily(ddlJobFamily, string.Empty, string.Empty, Facade);
            
           // MiscUtil.PopulateDivision(ddlDivision, Facade);
            //*********Autopopulate Division  sp-OrganizationUnit_GetDivisionsCodeByCurrentMemberId
            JobPosting jps = new JobPosting();
            if (CurrentJobPostingId > 0)
            {
                jps = Facade.GetJobPostingById(CurrentJobPostingId);
            }
            else
            {
                jps = null;
            }

            if (jps == null)
            {
                MiscUtil.PopulateDivisionByMemberId(ddlDivision, Facade, base.CurrentMember.Id);
                DivisionChangeEvent();
            }
            else
            {
                MiscUtil.PopulateDivisionByMemberId(ddlDivision, Facade, jps.CreatorId);
                DivisionChangeEvent();
            }
    
            //}
            //ddlDivision.Enabled = false;
            //if (Convert.ToString(ddlDivision.SelectedValue) != "")
            //{
            //    ReportingMgrGIDChangeEvent();
            //}
            //*********Autopopulate Division

                //MiscUtil.PopulateReportingMgrGID(ddlReportingMgrGID, Facade);
                //ReportingMgrGIDChangeEvent();
            //DivisionChangeEvent();
           

            MiscUtil.PopulateSuperOrgCode(ddlSuperOrgCode, Facade,0);

            //MiscUtil.PopulateReportingMgrGID(ddlReportingMgrGID, Facade);
            
          
            MiscUtil.PopulateVenueMaster(ddllocation, Facade);//Code introduced by pravin on 11/Nov/2016
            //-----------------------------Rishi Code End---------------------------------//

            switch (ApplicationSource)
            {
                case ApplicationSource.LandTApplication:
                    MiscUtil.PopulateJobCategory(ddlJbCategory, Facade);
                    break;
                case ApplicationSource.MainApplication:
                    MiscUtil.PopulateCategory(ddlJbCategory, Facade);
                    break;
                default:
                    MiscUtil.PopulateCategory(ddlJbCategory, Facade);
                    break;
            }

            MiscUtil.PopulateJobLocation(ddlJobLocation, Facade);
            MiscUtil.PopulateCity(ddlCity, Facade);
            GetDefaultsFromSiteSetting();
            PopulateBUList();
            if (DefaultCountryCode != "")
            {
                Country country = Facade.GetCountryById(Convert.ToInt32(DefaultCountryCode));
                if (country != null)
                {
                    if (country.Name == "India")
                    {
                        lblZipCode.Text = "PIN Code"; revZIPCode.ErrorMessage = "Please enter a valid PIN code";
                        divZip.Visible = false;
                        divPinCode.Visible = true;
                    }
                }
            }
            //*******************Suraj Adsule******Start********For(uneditable for not accessible user)********20160916*********//
            string roleName = Facade.GetCustomRoleNameByMemberId(CurrentMember.Id);
            if (CurrentJobPostingId > 0)
            {
                if (roleName.ToLower() == "hr manager" || roleName.ToLower() == "recruitment manager")
                {
                    //pnlRequisition.Enabled = false;
                    //wdcResourceEndDate.Enabled=true;
                    //wdcResourceStartDate.Enabled = true;
                    pnlRequisition1.Enabled = false;
                    pnlRequisition2.Enabled = true;
                    pnlRequisition3.Enabled = false;
                    pnlRequisition4.Enabled = false;
                   
                } 
            }
            pnlRequisition2.Enabled = true;
            //*******************Suraj Adsule******End********For(uneditable for not accessible user)********20160916*********//

        }

        private void GetDefaultsFromSiteSetting()
        {

            if (SiteSetting != null)
            {
                ddlSalaryCurrency.SelectedValue = SiteSetting[DefaultSiteSetting.Currency.ToString()].ToString();
                //if (ddlSalaryCurrency.SelectedItem.Text == "INR")
                //    lblPayrateCurrency.Text = "(Lacs)";
                ddlSalary.SelectedValue = SiteSetting[DefaultSiteSetting.PaymentType.ToString()].ToString();
            }
            else
            {
                ddlSalaryCurrency.SelectedValue = ddlSalaryCurrency.Items.FindByText("$ USD").Value;
            }
        }

        private void DisableValidationControls()
        {
            rfvBU.Enabled = false;
            //rfvcontactemail.Enabled = false;
            RFVBUContacts.Enabled = false;
            rfvLocation.Enabled = true;
            rfvBUcontact.Enabled = false;
            rfvnoofopenings.Enabled = false;
            rfvStartDate.Enabled = false;
            rfVOpendDate.Enabled = false;
            rfvClosingDate.Enabled = false ;
            if (ddlDemandType.SelectedIndex > 0)//addde by pravin khot on 27/12/2016
            {
                if (ddlDemandType.SelectedItem.Text == "Contract")
                {
                    rfvClosingDate.Enabled = true;
                    cmpDate.Enabled = true;
                    rfvResourceStartDate.Enabled = true;
                    cmpResourceDate.Enabled = true;
                    //cmpResourceDate1.Enabled = true;
                    rfvResourceClosingDate.Enabled = true;
                }
                else
                {
                    rfvResourceStartDate.Enabled = false;
                    rfvResourceClosingDate.Enabled = false;
                }
            }
          
            rfvCity.Enabled = false;
            uclCountryState.Enabled = true; //code condition TRUE by pravin khot on 11/Feb/2016
            cvSkill.Enabled = false;
            rfvJobCategory.Enabled = false;
            rfvJobLocation.Enabled = false;
            rfvBillRate.Enabled = false;
            rfvSalesRegion.Enabled = false;
            rfvSalesGroup.Enabled = false;
            cvExperience.Enabled = false;
            trsalesregion.Visible = false;
            trsalesgroup.Visible = false;
            trjoblocation.Visible = false;
            reqDivision.Enabled = true;
            reqReportingManager.Enabled = true;
        }
        private string GetTempFolder(bool App)
        {
            string dir = CurrentUserId.ToString();
            string path = "";
            if (!App)
            {
                path = System.IO.Path.Combine(UrlConstants.TempDirectory, dir) + "\\" + "Requisition";

                if (!System.IO.Directory.Exists(path))
                {
                    System.IO.Directory.CreateDirectory(path);
                }
            }
            else path = UrlConstants.ApplicationBaseUrl + "Temp/" + dir + "/Requisition";


            return path;
        }
        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
           
            //---------------------------------------Rishi Code Start---------------------------------------//
            //ddlSuperOrgCode.Attributes.Add("OnChange", "SuperOrgCode_OnChange('" + ddlSuperOrgCode.ClientID + "')");
            //---------------------------------------Rishi Code End-----------------------------------------//
            if (ApplicationSource == ApplicationSource.MainApplication || ApplicationSource == ApplicationSource.SelectigenceApplication || ApplicationSource == ApplicationSource.GenisysApplication)
            {

                if (!IsPostBack)
                {
                    lblCreatedOnTextValue.Text = Convert.ToString(System.DateTime.Now.ToShortDateString());
                    if (CurrentJobPostingId > 0)
                    {
                        JobPosting JS = Facade.GetJobPostingById(CurrentJobPostingId);
                        if (JS != null)
                        {
                            lblSubmittedOnTextValue.Text = Convert.ToString(JS.PostedDate.ToShortDateString());
                        }
                    }
                    if (ddlBU.SelectedIndex == 0 || ddlBU.SelectedIndex == -1)
                    {
                        //divBUContact.Disabled = true;
                        txtBUcontact.Enabled = false;
                    }

                }

                if (SiteSetting[DefaultSiteSetting.ApplicationEdition.ToString()].ToString() == UIConstants.STAFFING_FIRM)
                {
                    lblBU.Text = "Account";
                    lblBUContact.Text = "Account Contact";
                    //divClientBudget.Visible = true;
                    PopulateCurrencyLookUp();
                }

                if (SiteSetting[DefaultSiteSetting.AssociateRequisitionsWithEntitiesFromModule.ToString()].ToString() == UIConstants.AccountType_Client)
                {
                    lblBU.Text = "Account";
                    lblBUContact.Text = "Account Contact";

                }


                DisableValidationControls();
                hdnApplicationType.Value = "";
                dvBillrate.Visible = false;
                ddlCity.Visible = false;
                txtCity.Visible = true;

                cmpStartDate.Enabled = false;
                cmpOpenDate.Enabled = false;

            }
            else if (ApplicationSource == ApplicationSource.MainApplication || ApplicationSource == ApplicationSource.SelectigenceApplication || ApplicationSource == ApplicationSource.GenisysApplication)
            {
                ddlCity.Visible = true;
                txtCity.Visible = false;
                spnTxtCity.Visible = false;
                spnddlCity.Visible = true;
            }
            else if (ApplicationSource == ApplicationSource.SelectigenceApplication)
            {
                if (!IsPostBack)
                {
                    if (ddlBU.SelectedIndex == 0 || ddlBU.SelectedIndex == -1)
                    {
                        //divBUContact.Disabled = true;
                        txtBUcontact.Enabled = false;
                    }

                }

                if (SiteSetting[DefaultSiteSetting.ApplicationEdition.ToString()].ToString() == UIConstants.STAFFING_FIRM)
                {
                    lblBU.Text = "Account";
                    lblBUContact.Text = "Account Contact";

                }

                if (SiteSetting[DefaultSiteSetting.AssociateRequisitionsWithEntitiesFromModule.ToString()].ToString() == UIConstants.AccountType_Client)
                {
                    lblBU.Text = "Account";
                    lblBUContact.Text = "Account Contact";

                }


                DisableValidationControls();
                hdnApplicationType.Value = "";
                dvBillrate.Visible = false;
                ddlCity.Visible = false;
                txtCity.Visible = true;
            }
            txtZipCode.Attributes.Add("onblur", "javascript:FilterNumeric('" + txtZipCode.ClientID + "')");
            cmpStartDate.ValueToCompare = DateTime.Now.ToShortDateString();   // 1.2 start
            cmpOpenDate.ValueToCompare = DateTime.Now.ToShortDateString();

            //cmpResourceStartDate.ValueToCompare = DateTime.Now.ToShortDateString();   // 1.2 start

            //cmpResourceEndDate.ValueToCompare = DateTime.Now.ToShortDateString();

            ddlSalaryCurrency.Attributes.Add("onchange", "javascript:ShowYearlyCurrency('" + ddlSalaryCurrency.ClientID + "','" + lblPayrateCurrency.ClientID + "')");
            if (!Page.IsPostBack)
            {
                DivResourceStartDate.Visible = false;
                DivResourceEndtDate.Visible = false;

                wdcStartDate.Value = DateTime.Now;
                wdcOpenDate.Value = DateTime.Now;
                PrepareView();
                if (CurrentJobPostingId > 0) PopulateFormData(CurrentJobPosting);
                string message = Helper.Url.SecureUrl[UrlConstants.PARAM_MSG];
                if (!StringHelper.IsBlank(message))
                {
                    MiscUtil.ShowMessage(lblMessage, message, false);
                }
            }
            if (chkStartDate.Checked)
            {
                wdcStartDate.BorderColor = System.Drawing.Color.LightGray;
                //wdcStartDate.DropButton.ImageUrlDisabled = "../Images/ddlGrayOut.jpg";
            }
            else
                wdcStartDate.BorderColor = System.Drawing.Color.Gray;
            if (!string.IsNullOrEmpty(wdcClosingDate.Text) && wdcClosingDate.Text != "Null")
            {
                if (DateTime.Parse(wdcClosingDate.Text) == DateTime.MinValue)
                    wdcClosingDate.Value = null;
            }

            if (ApplicationSource == ApplicationSource.GenisysApplication || ApplicationSource == ApplicationSource.SelectigenceApplication)
            {
                cmpOpenDate.Enabled = false;
                cmbopenwithExpectedFullfillment.Enabled = false;
                //rfVOpendDate.Enabled = false;
                cmpStartDate.Enabled = false;
                cmpDate.Enabled = false;
            }



            // AjaxControlToolkit.Utility.SetFocusOnLoad(ddlBU);

            if (IsPostBack) postbackCall();

            

        }

        // Start changes by Vishal Tripathy
        protected void OnSelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlDemandType.SelectedIndex > 0)
            {
                if (ddlDemandType.SelectedItem.Text == "Permanent")
                {
                    rfvClosingDate.Enabled = false;
                    wdcClosingDate.Text = Convert.ToString(System.DateTime.Now.ToShortDateString());
                    lblGradeType.Visible = true;
                    ddlGrade.Visible = true;
                    DivResourceStartDate.Visible = false;
                    DivResourceEndtDate.Visible = false;
                }
                else
                {

                    cmpDate.Enabled = true;
                    rfvClosingDate.Enabled = true ;
                    MiscUtil.PopulateGrade(ddlGrade, Facade);
                    wdcClosingDate.Text = string.Empty;
                    lblGradeType.Visible = false;
                    ddlGrade.Visible = false;
                    DivResourceStartDate.Visible = true;
                    DivResourceEndtDate.Visible = true;
                }
            }
            else
            {
                MiscUtil.PopulateGrade(ddlGrade, Facade);
                wdcClosingDate.Text = string.Empty;
                lblGradeType.Visible = false;
                ddlGrade.Visible = false;
                DivResourceStartDate.Visible = false;
                DivResourceEndtDate.Visible = false;
            }
        }
        // End Changes By Vishal Tripathy

        //----------------------------Rishi Code Start-----------------------------------------//

        protected void SuperOrgCodeDropDown_Change(object sender, EventArgs e)
        {
            SuperOrgCodeChangeEvent();
            //DivisionChangeEvent();
           // CandidateManagerGIDBySuperOrgCodeChangeEvent();
        }
        protected void JobFamilyDropDown_Change(object sender, EventArgs e)
        {
            JobFamilyChangeEvent();
        }
        protected void ReportingMgrGID_Change(object sender, EventArgs e)
        {
            ReportingMgrGIDChangeEvent();
        }
        public void CandidateManagerGIDBySuperOrgCodeChangeEvent() //added by pravin on 25/Nov/2016
        {
            try
            {
                string superOrgCode = Convert.ToString(ddlSuperOrgCode.SelectedValue);
                if (superOrgCode !="")
                {                 
                    MiscUtil.PopulateDivisionInterviewers(ddlReportingMgrGID, superOrgCode, Facade);
                }
            }
            catch
            {

            }
        }//***********
        //*********Rishi Jagati – Resource Demand Approval – 13/7/2016 – Start *****************
        protected void Division_Change(object sender, EventArgs e)
        {
            //DivisionChangeEvent();
            JobFamilyByDivisionChangeEvent();           
        }

        protected void IndivContribMgnt_Change(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToString(ddlDivision.SelectedItem.Text) != "Please Select") //comeenetd by pravin khot on 27/12/2016               
                {
                    string divisionValue = Convert.ToString(ddlDivision.SelectedValue);
                    string IndivContribMgnt = "";
                    if (ddlIndivContribMgnt.SelectedItem.Text != "Please Select")
                    {
                        if (Convert.ToString(ddlIndivContribMgnt.SelectedItem.Text) == "Contributor(IC)")
                        {
                            IndivContribMgnt = "IC";
                        }
                        else
                        {
                            IndivContribMgnt = "M";
                        }
                    }
                    lblJobProfileCode.Text = "";
                    MiscUtil.PopulateJobFamily(ddlJobFamily, divisionValue, IndivContribMgnt, Facade);
                }
            }
            catch
            {
            }
        }
        public void JobFamilyByDivisionChangeEvent()
        {
            try
            {
                if (Convert.ToString(ddlDivision.SelectedItem.Text) != "Please Select") //comeenetd by pravin khot on 27/12/2016               
                {
                    string divisionValue = Convert.ToString(ddlDivision.SelectedValue);
                    string IndivContribMgnt = "";
                    if (ddlIndivContribMgnt.SelectedItem.Text != "Please Select")
                    {
                        //IndivContribMgnt = ddlIndivContribMgnt.SelectedItem.Value;
                        if (Convert.ToString(ddlIndivContribMgnt.SelectedItem.Text) == "Contributor(IC)")
                        {
                            IndivContribMgnt = "IC";
                        }
                        else
                        {
                            IndivContribMgnt = "M";
                        }
                    }
                    lblJobProfileCode.Text = "";
                    MiscUtil.PopulateJobFamily(ddlJobFamily,divisionValue,IndivContribMgnt, Facade);

                    //IList<TPS360.Common.BusinessEntities.OrganizationUnit> orgUnitData = Facade.GetDivisions(divisionValue, 4); //4 means all user active /unactive

                    //string firstitem = ddlReportingMgrGID.Items[0].ToString();

                    //ddlReportingMgrGID.Items.Clear();
                    //ddlReportingMgrGID.Items.Add(firstitem);
                    //if (orgUnitData != null)
                    //{

                    //    for (int i = 0; i < orgUnitData.Count; i++)
                    //    {
                    //        int j = 1;
                    //        ListItem li = new ListItem();
                    //        li.Value = orgUnitData[i].HeadGID;
                    //        li.Text = Convert.ToString(orgUnitData[i].HeadName);
                    //        ddlReportingMgrGID.Items.Insert(j, li);
                    //        j++;
                    //    }
                    //}
                    //else
                    //{
                    //    ddlReportingMgrGID.Items.Clear();
                    //}
                    //IList<TPS360.Common.BusinessEntities.OrganizationUnit> orgUnitHead = Facade.GetDivisions(divisionValue, 3);
                    //if (orgUnitHead != null)
                    //{
                    //    lblOrgUnitHead.Text = orgUnitHead[0].HeadGID.Trim();
                    //    lblMgrName.Text = "";
                    //}

                }
            }
            catch
            {
                int i = 0;
                ddlDivision.SelectedValue = i.ToString();
            }
        }

        public void DivisionChangeEvent()
        {
            try
            {
                //if (Convert.ToString(ddlSuperOrgCode.SelectedItem.Text) != "Please Select") //comeenetd by pravin khot on 27/12/2016
                //if (Convert.ToString(ddlDivision.SelectedItem.Text) != "Please Select")
                //{
                    MiscUtil.PopulateReportingMgrGID(ddlReportingMgrGID, Facade);

                    //ddlReportingMgrGID.Items.Add(firstitem);
                    // string divisionValue = Convert.ToString(ddlDivision.SelectedItem.Text);
                   // string divisionValue = Convert.ToString(ddlDivision.SelectedValue);
                    string divisionValue = Convert.ToString(ddlSuperOrgCode.SelectedValue);
                    //IList<TPS360.Common.BusinessEntities.OrganizationUnit> orgUnitData = Facade.GetDivisions(divisionValue, 1);
                    IList<TPS360.Common.BusinessEntities.OrganizationUnit> orgUnitData = Facade.GetDivisions(divisionValue, 4); //4 means all user active /unactive

                    string firstitem = ddlReportingMgrGID.Items[0].ToString();

                    ddlReportingMgrGID.Items.Clear();
                    ddlReportingMgrGID.Items.Add(firstitem);
                    if (orgUnitData != null)
                    {
                        MiscUtil.PopulateReportingMgrGID(ddlReportingMgrGID, Facade);

                        /*   for (int i = 0; i < orgUnitData.Count; i++)
                           {
                               int j = 1;
                               string item = orgUnitData[i].HeadGID.ToString();
                               ddlReportingMgrGID.Items.Insert(j, item);
                               j++;
                           }
                           */
                        //Code updated by Suraj Adsule on 20160924

                        //ddlReportingMgrGID.DataSource = orgUnitData;
                        //ddlReportingMgrGID.DataTextField = "HeadName";
                        //ddlReportingMgrGID.DataValueField = "HeadGID";
                        //ddlReportingMgrGID.DataBind();
                        //ddlReportingMgrGID.Items.Insert(0, new ListItem("-1", "Please Select"));

                        for (int i = 0; i < orgUnitData.Count; i++)
                        {
                            int j = 1;
                            // string item = orgUnitData[i].HeadGID.ToString();
                            ListItem li = new ListItem();
                           // li.Value = orgUnitData[i].HeadName;
                            li.Value = orgUnitData[i].HeadGID;
                            li.Text = Convert.ToString(orgUnitData[i].HeadName) ;
                            ddlReportingMgrGID.Items.Insert(j, li);
                            j++;
                        }
                        //ddlReportingMgrGID.Items.Remove(firstitem);  
                        //end Suraj Adsule
                    }
                    else
                    {
                        ddlReportingMgrGID.Items.Clear();
                        //ddlReportingMgrGID.Items.Add(firstitem);

                        //ddlReportingMgrGID.DataSource = null;
                        //ddlReportingMgrGID.DataBind();
                    }
                    //IList<TPS360.Common.BusinessEntities.OrganizationUnit> orgUnitHead = Facade.GetDivisions(divisionValue, 3);
                    //if (orgUnitHead != null)
                    //{
                    //    lblOrgUnitHead.Text = orgUnitHead[0].HeadGID.Trim();
                    //    lblMgrName.Text = "";
                    //}
                //}
                //else
                //{
                //    ddlReportingMgrGID.Items.Clear();
                //    MiscUtil.PopulateReportingMgrGID(ddlReportingMgrGID, Facade);
                //}
            }
            catch
            {
                int i = 0;
                ddlDivision.SelectedValue  =i.ToString();// "Please Select"; // Convert.ToString(ddlDivision.SelectedValue);
                //ddlReportingMgrGID.Items.Clear();
                //MiscUtil.PopulateReportingMgrGID(ddlReportingMgrGID, Facade);
            } 
        }

        //*********Rishi Jagati – Resource Demand Approval – 13/7/2016 – End *****************

        public void SuperOrgCodeChangeEvent()
        {
            string superOrgCode = Convert.ToString(ddlSuperOrgCode.SelectedValue); // commented by pravin khot on 8/Nov/2016
            //string superOrgCode = Convert.ToString(ddlSuperOrgCode.SelectedItem.Text);            
            TPS360.Common.BusinessEntities.OrganizationUnit orgUnitData = Facade.GetOrganizationDetailsById(superOrgCode);

            if (orgUnitData != null)
            {
                if (orgUnitData.ReportingOrgStTxt != null)
                {
                    lblSuperOrgShortTxt.Text = orgUnitData.ReportingOrgStTxt.Trim();

                    lblSuperOrgDescrp.Text = orgUnitData.ReportingOrgLngTxt.Trim();

                }
            }
            else
            {
                lblSuperOrgShortTxt.Text = "";
                lblSuperOrgDescrp.Text = "";
            }
            lblOrgUnitHead.Text = "";
            IList<TPS360.Common.BusinessEntities.OrganizationUnit> orgUnitHead = Facade.GetDivisions(superOrgCode, 3);
            if (orgUnitHead != null)
            {
                lblOrgUnitHead.Text = orgUnitHead[0].HeadGID.Trim();
            }

            //lblOrgUnitHead.Text = orgUnitData.HeadName.Trim();
        }

        public void JobFamilyChangeEvent()
        {
            //*********Rishi Jagati – Resource Demand Approval – 8/7/2016 – Start *****************
            if (Convert.ToString(ddlJobFamily.SelectedItem.Text) != "Please Select")
            {
                //*********Rishi Jagati – Resource Demand Approval – 8/7/2016 – End *****************
                //string jobFamily = Convert.ToString(ddlJobFamily.SelectedItem.Text);
                string jobFamily = Convert.ToString(ddlJobFamily.SelectedItem.Value);
                TPS360.Common.BusinessEntities.JobProfile jobProfileData = Facade.GetJobProfileCodeByName(jobFamily);
                lblJobProfileCode.Text = jobProfileData.JobProfileCode.Trim();
            }
        }

        public void ReportingMgrGIDChangeEvent()
        {
            try
            {
                //*********Rishi Jagati – Resource Demand Approval – 8/7/2016 – Start *****************
                if (ddlReportingMgrGID.SelectedValue != "Please Select")
                // if (Convert.ToString(ddlReportingMgrGID.SelectedItem.Text) != "Please Select")
                {
                    //*********Rishi Jagati – Resource Demand Approval – 8/7/2016 – End *****************
                    //string reportingMgrGID = Convert.ToString(ddlReportingMgrGID.SelectedItem.Text);
                    string reportingMgrGID = Convert.ToString(ddlReportingMgrGID.SelectedValue);

                    //TPS360.Common.BusinessEntities.EmployeeMemberMapping empMemberMappingData = Facade.GetEmployeeMemberMappingDetailsByGID(reportingMgrGID);
                    IList<TPS360.Common.BusinessEntities.OrganizationUnit> orgUnitData = Facade.GetDivisions(reportingMgrGID, 2);
                    if (orgUnitData != null)
                    {
                        lblMgrName.Text = orgUnitData[0].HeadGID.Trim();
                    }
                 
                }
                else
                {
                    lblMgrName.Text = "";
                }
            }
            catch
            {
            }
        }

        //----------------------------Rishi Code End------------------------------------------//


        protected void btnSave_Click(object sender, EventArgs e)
        {
            //if (MiscUtil .RemoveScript ( txtSkillName.Text.Trim()) != string.Empty)
            //{
            //    IList<Skill> AddedSkillList = getSkillList();
            //    var list = from a in AddedSkillList where a.Name.ToString().ToUpper() ==MiscUtil .RemoveScript ( txtSkillName.Text.ToUpper()) select a;
            //    if (list.ToList().Count == 0) addSkill(AddedSkillList);
            //}
            //txtJobTitle.Text = MiscUtil.RemoveScript(txtJobTitle.Text,string .Empty );
            //if (txtJobTitle.Text.ToString() != string.Empty)
            //{
            //    SaveJobPostingData();

            //    PopulateFormData();
            //    if (IsValid)       
            //    {
            //        Helper.Url.Redirect(UrlConstants.Requisition.JOB_POSTING_DESCRIPTION_PAGE, string.Empty, UrlConstants.PARAM_JOB_ID, StringHelper.Convert(base.CurrentJobPostingId));
            //    }
            //}

        }
        protected void chkStartDate_CheckedChanged(object sender, EventArgs e)
        {
            //    wdcStartDate.Value = "";        
            //    if (!(wdcStartDate.Enabled = !chkStartDate.Checked))
            //    {
            //        wdcStartDate.BorderColor = System.Drawing.Color.LightGray;
            //        //wdcStartDate.DropButton.ImageUrlDisabled = "../Images/ddlGrayOut.jpg";
            //        chkStartDate.Focus();
            //    }
            //    else
            //    {
            //        wdcStartDate.Value = DateTime.Now;
            //        wdcStartDate.BorderColor = System.Drawing.Color.Gray; chkStartDate.Focus();
            //    }
        }
        protected void ddlBU_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlBUContacts.SelectedIndex = -1;
            MiscUtil.PopulateBUContactIdByBUId(ddlBUContacts, Facade, Convert.ToInt32(ddlBU.SelectedValue));

        }     

        #endregion


  










    }
}

