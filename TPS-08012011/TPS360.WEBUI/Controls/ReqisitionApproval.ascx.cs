﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using TPS360.Web.UI;
using TPS360.Common.Shared;
using TPS360.Common.BusinessEntities;
using TPS360.Web.UI.Helper;
using TPS360.Common.Helper;
using System.Collections.Generic;

public partial class Controls_ReqisitionApproval : BaseControl
{

    #region Member Variables
    IList<HiringMatrixLevels> hiringLevels = null;
    bool ShowAction = false;
    public int HeaderAdd = 0;
    ArrayList _permittedMenuIdList;
    public string RequisitionType = "Master";
    public static bool IsAccss = false;

    public DropDownList ddlJobStatusCommon
    {
        set;
        get;
    }
    #endregion
    #region Methods
    private void BindList()
    {
        this.lsvJobPosting.DataBind();

    }    
    private bool IsInPermitedMenuList(int menuId)
    {
        if (_permittedMenuIdList == null)
        {
            if (CurrentMember != null) _permittedMenuIdList = Facade.GetAllMemberPrivilegeIdsByMemberId(CurrentMember.Id);
        }

        if ((_permittedMenuIdList != null) && (_permittedMenuIdList.Count > 0))
        {
            if (_permittedMenuIdList.Contains(menuId))
            {
                return true;
            }
        }

        return false;
    }
    #endregion
    #region Events

    #region Page Events    
    protected void Page_Load(object sender, EventArgs e)
    {
        odsRequisitionList.SelectParameters["IsAdmin"].DefaultValue = base.IsUserAdmin.ToString();
        ddlJobStatusCommon = new DropDownList();
        MiscUtil.PopulateRequistionStatus(ddlJobStatusCommon, Facade);
        if (hdnDoPostPack.Value == "1")
        {
            lsvJobPosting.DataBind();
            hdnDoPostPack.Value = "0";
            return;
        }
        string testUrl = HttpContext.Current.Request.Url.Authority;
        this.Page.MaintainScrollPositionOnPostBack = true;
        if (Page.Request.Url.AbsoluteUri.ToString().Contains("MasterRequisitionList.aspx"))
        {
            RequisitionType = "Master";
        }
        else
        {
            RequisitionType = "My";
        }

        if (CurrentMember != null)
        {
            ArrayList Privilege = Facade.GetAllMemberPrivilegeIdsByMemberId(CurrentMember.Id);
            if (Privilege.Contains(56))
                IsAccss = true;
            else IsAccss = false;
        }

        if (!IsPostBack)
        {
            string message = Helper.Url.SecureUrl[UrlConstants.PARAM_MSG];
            if (!StringHelper.IsBlank(message))
            {
                MiscUtil.ShowMessage(lblMessage, message, false);
            }
            hdnSortColumn.Value = "btnPostedDate";
            hdnSortOrder.Value = "DESC";
            PlaceUpDownArrow();
        }
        string pagesize = "";
        if (RequisitionType == "My")
        {
            pagesize = (Request.Cookies["MyRequisitionListRowPerPage"] == null ? "" : Request.Cookies["MyRequisitionListRowPerPage"].Value); ;
        }
        else
        {
            pagesize = (Request.Cookies["MasterRequisitionListRowPerPage"] == null ? "" : Request.Cookies["MasterRequisitionListRowPerPage"].Value); ;
        }
        ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvJobPosting.FindControl("pagerControl");
        if (PagerControl != null)
        {
            DataPager pager = (DataPager)PagerControl.FindControl("pager");
            if (pager != null)
            {
                pager.PageSize = Convert.ToInt32(pagesize == "" ? "20" : pagesize);
            }
        }
    }
    private void PlaceUpDownArrow()
    {
        try
        {
            LinkButton lnk = (LinkButton)lsvJobPosting.FindControl(hdnSortColumn.Value);
            System.Web.UI.HtmlControls.HtmlTableCell im = (System.Web.UI.HtmlControls.HtmlTableCell)lnk.Parent;
            im.EnableViewState = false;
            if (lnk.CommandArgument.Contains("[1]") || lnk.CommandArgument.Contains("[JMC].[ManagersCount]"))
            {
                im.Attributes.Add("class", (hdnSortOrder.Value == "ASC" ? "Descending" : "Ascending"));
            }
            else
                im.Attributes.Add("class", (hdnSortOrder.Value == "ASC" ? "Ascending" : "Descending"));
        }
        catch
        {
        }

    }
    #endregion

    #region ListView Events
    public bool isMemberinHinringTeam(int memberid, int jobpostingid)
    {
        bool returnValue = false;

        JobPostingHiringTeam team = Facade.GetJobPostingHiringTeamByMemberId(memberid, jobpostingid);

        if (team != null)
        {
            returnValue = true;
        }
        else
        {
            returnValue = false;
        }

        return returnValue;
    }
    protected void lsvJobPosting_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        bool _isAdmin = base.IsUserAdmin;
        bool _isHiringManager = base.IsHiringManager;
        bool isAssingned = false;
        if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
        {
            JobPosting jobPosting = ((ListViewDataItem)e.Item).DataItem as JobPosting;
            int ColLocation = 0;
            if (jobPosting != null)
            {                
                Label lblPostedDate = (Label)e.Item.FindControl("lblPostedDate");
                Label lblJobCode = (Label)e.Item.FindControl("lblJobCode");
                HyperLink lnkJobTitle = (HyperLink)e.Item.FindControl("lnkJobTitle");
                //Label lblNoOfOpenings = (Label)e.Item.FindControl("lblNoOfOpenings");                             
                Label lblDemandType = (Label)e.Item.FindControl("lblDemandType");
                Label lblPriority = (Label)e.Item.FindControl("lblPriority");
                Label lblOrganizationUnit = (Label)e.Item.FindControl("lblOrganizationUnit");
                Label lblPostedBy = (Label)e.Item.FindControl("lblPostedBy");
                Label lblUpdatedOn = (Label)e.Item.FindControl("lblUpdatedOn");
                HyperLink lnkReqApprove = (HyperLink)e.Item.FindControl("hlkReqApprove");
                HyperLink lnkReqReject = (HyperLink)e.Item.FindControl("hlkReqReject");
                lblDemandType.Text = jobPosting.DemandTypeText.ToString();
                lblPriority.Text = jobPosting.PriorityText.ToString();
                lblOrganizationUnit.Text = jobPosting.OrgnizationName.ToString();
                lblPostedBy.Text = jobPosting.PostedBy.ToString();
                lblUpdatedOn.Text = jobPosting.UpdateDate.ToShortDateString();

                Label lblJobStatus = (Label)e.Item.FindControl("lblJobStatus");
                GenericLookup _RequisitionStatusLookup = Facade.GetGenericLookupById(jobPosting.JobStatus);
                if (_RequisitionStatusLookup != null)
                {
                    lblJobStatus.Text = _RequisitionStatusLookup.Name;
                    if (_RequisitionStatusLookup.Name == "Approval In Progress")
                    {
                        lnkReqApprove.Attributes.Add("onclick", "EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/RequsitionApproval.aspx?JID=" + jobPosting.Id + "&STATUS=Pending with Division Head&ID=" + CurrentMember.Id + "','700px','350px'); return false;");
                        lnkReqReject.Attributes.Add("onclick", "var input=confirm('Are you sure want to send back the requisition?'); if (input==true) { EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/RequsitionApproval.aspx?JID=" + jobPosting.Id + "&STATUS=Rejected By Tower Head&ID=" + CurrentMember.Id + "','700px','350px');} return false;");
                    }
                    else
                    {
                        lnkReqApprove.Visible = false;
                        lnkReqReject.Visible = false;
                    }
                }
                else
                {
                    lblJobStatus.Text = "";
                    lnkReqApprove.Visible = false;
                    lnkReqReject.Visible = false;
                }
                lblJobCode.Text = jobPosting.JobPostingCode.ToString();
                lblPostedDate.Text = jobPosting.PostedDate.ToShortDateString();                
                SecureUrl urlEditJob = UrlHelper.BuildSecureUrl("../" + (UrlConstants.Requisition.JOB_POSTING_EDITOR_PAGE.Replace("~/", "").Replace("//", "/")), string.Empty, UrlConstants.PARAM_JOB_ID, StringHelper.Convert(jobPosting.Id), UrlConstants.PARAM_SITEMAP_PARENT_ID, "13", UrlConstants.PARAM_JOB_DISABLE,"true");
                lnkJobTitle.Text = jobPosting.JobTitle;                                
                lnkJobTitle.Attributes.Add("onclick", "window.open('" + urlEditJob.ToString() + "','View');");                
                Label lblApprovalStatus = (Label)e.Item.FindControl("lblApprovalStatus");
                lblApprovalStatus.Text = jobPosting.ApprovalStatus;
                //Member MemberCreated = null;
                //if (jobPosting.CreatorId != 0) MemberCreated = Facade.GetMemberById(jobPosting.CreatorId);                  //0.5                
                //lblNoOfOpenings.Text = jobPosting.NoOfOpenings.ToString();
            }
        }
    }
    protected void lsvJobPosting_ItemCommand(object sender, ListViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Sort")
            {
                LinkButton lnkbutton = (LinkButton)e.CommandSource;
                if (hdnSortColumn.Value == lnkbutton.ID)
                {
                    if (hdnSortOrder.Value == "ASC") hdnSortOrder.Value = "DESC";
                    else hdnSortOrder.Value = "ASC";
                }
                else
                {
                    hdnSortColumn.Value = lnkbutton.ID;
                    hdnSortOrder.Value = "ASC";
                }
            }
        }
        catch
        {
        }       
    }

    protected void lsvJobPosting_PreRender(object sender, EventArgs e)
    {
        try
        {
            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvJobPosting.FindControl("pagerControl");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)PagerControl.FindControl("pager");
                if (pager != null)
                {
                    DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                    if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
                }

                HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
                if (hdnRowPerPageName != null)
                {
                    if (RequisitionType == "My") hdnRowPerPageName.Value = "MyRequisitionListRowPerPage";
                    else hdnRowPerPageName.Value = "MasterRequisitionListRowPerPage";
                }               
            }
            if (ApplicationSource == ApplicationSource.GenisysApplication)
            {
                System.Web.UI.HtmlControls.HtmlTableCell tdpager = (System.Web.UI.HtmlControls.HtmlTableCell)lsvJobPosting.FindControl("tdPager");               
                tdpager.ColSpan = 11;               
            }
        }
        catch
        {
        }
        PlaceUpDownArrow();
        if (SiteSetting[DefaultSiteSetting.ApplicationEdition.ToString()].ToString() == UIConstants.INTERNAL_HIRING)
        {
            LinkButton lnkClient = (LinkButton)lsvJobPosting.FindControl("lnkClient");
            if (lnkClient != null)
            {
                if (SiteSetting[DefaultSiteSetting.AssociateRequisitionsWithEntitiesFromModule.ToString()].ToString() == UIConstants.AccountType_Client)
                    lnkClient.Text = "Account";
                else if (SiteSetting[DefaultSiteSetting.AssociateRequisitionsWithEntitiesFromModule.ToString()].ToString() == UIConstants.AccountType_Department)
                    lnkClient.Text = "BU";
                else lnkClient.Text = "Vendor";
            }
        }
    }
    protected void odsRequisitionList_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
        if (RequisitionType == "Master")
        {
            ShowAction = false;
            System.Web.UI.HtmlControls.HtmlTableCell thAction = (System.Web.UI.HtmlControls.HtmlTableCell)lsvJobPosting.FindControl("thAction");
            thAction.Visible = false;
        }
    }

    #endregion

    #endregion
}
