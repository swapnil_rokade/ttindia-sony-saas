﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
 suraj adsule
 * Internal Job Posting Report
 * 20160919
-------------------------------------------------------------------------------------------------------------------------------------------       

>*/


using System;
using System.Web.Security;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Web.UI.Helper;
using TPS360.Common.Shared;
using TPS360.Common;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using ExpertPdf.HtmlToPdf;
using System.Drawing;
using System.Collections.Generic;
using System.Collections;
using System.Web.UI.HtmlControls;
using System.Linq;
using System.Configuration;
using System.Web;

namespace TPS360.Web.UI
{
    public partial class InternalJobPostingReport : BaseControl
    {

        bool _IsAccessToCandidate = true;
        private string UrlForCandidate = string.Empty;
        private int SitemapIdForCandidate = 0;
        private static bool _IsMyReferralReport = true;
        private static bool _isTeamReport = false;
        private string CurentMemberJobPostingId
        {
            get { return Facade.GetJobPostingIdByMemberIdForERPortal(CurrentMember.Id).TrimEnd(','); }
        }

        public bool IsMyReferralReport
        {
            set { _IsMyReferralReport = value; }
        }
        public bool IsTeamReport
        {
            set { _isTeamReport = value; }
        }
        #region Methods

        private void PlaceUpDownArrow()
        {
            try
            {

                LinkButton lnk = (LinkButton)lsvReferralDetail.FindControl(txtSortColumn.Text);
                System.Web.UI.HtmlControls.HtmlTableCell im = (System.Web.UI.HtmlControls.HtmlTableCell)lnk.Parent;
                im.EnableViewState = false;
                im.Attributes.Add("class", (txtSortOrder.Text == "ASC" ? "Ascending" : "Descending"));

            }
            catch
            {
            }

        }

        public void repor(string rep)
        {
            GenerateEmployeeReferralReport(rep);
        }

        private void GenerateEmployeeReferralReport(string format)
        {
            if (string.Equals(format, "word"))
            {
                Response.AddHeader("content-disposition", "attachment;filename=EmployeeReferralReport-" + DateTime.Now.ToString("yyyy-MM-dd") + ".doc");
                Response.ContentEncoding = System.Text.Encoding.ASCII;
                Response.ContentType = "application/msword";
                Response.Output.Write(GetRequisitionReportTable("word"));
                Response.Flush();
                Response.End();
            }
            else if (string.Equals(format, "excel"))
            {
                Response.AddHeader("content-disposition", "attachment;filename=EmployeeReferralReport-" + DateTime.Now.ToString("yyyy-MM-dd") + ".xls");
                Response.ContentEncoding = System.Text.Encoding.ASCII;
                Response.ContentType = "application/msexcel";
                Response.Output.Write(GetRequisitionReportTable("excel"));
                Response.Flush();
                Response.End();
            }
            else if (string.Equals(format, "pdf"))
            {
                PdfConverter pdfConverter = new PdfConverter();
                UnicodeEncoding unicodeEncoding = new UnicodeEncoding();
                pdfConverter.PageWidth = 0;
                pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
                pdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Normal;
                pdfConverter.PdfDocumentOptions.ShowHeader = false;
                pdfConverter.PdfDocumentOptions.ShowFooter = false;
                pdfConverter.PdfDocumentOptions.LeftMargin = 15;
                pdfConverter.PdfDocumentOptions.RightMargin = 5;
                pdfConverter.PdfDocumentOptions.TopMargin = 15;
                pdfConverter.PdfDocumentOptions.BottomMargin = 5;
                pdfConverter.PdfDocumentOptions.GenerateSelectablePdf = true;
                pdfConverter.PdfDocumentOptions.ShowHeader = false;
                pdfConverter.PdfHeaderOptions.DrawHeaderLine = false;
                pdfConverter.PdfFooterOptions.FooterTextColor = Color.Black;
                pdfConverter.PdfFooterOptions.DrawFooterLine = true;
                pdfConverter.PdfFooterOptions.PageNumberText = "Page";
                pdfConverter.PdfFooterOptions.ShowPageNumber = true;

                pdfConverter.LicenseKey = ContextConstants.EPHTMLTOPDFKEY;
                byte[] downloadBytes = pdfConverter.GetPdfBytesFromHtmlString(GetRequisitionReportTable("pdf"));

                System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
                response.Clear();
                response.AddHeader("Content-Type", "binary/octet-stream");
                response.AddHeader("Content-Disposition", "attachment; filename=EmployeeReferralReport-" + DateTime.Now.ToString("yyyy-MM-dd") + ".pdf; size=" + downloadBytes.Length.ToString());
                response.Flush();
                response.BinaryWrite(downloadBytes);
                response.Flush();
                response.End();
            }
        }
        private string GetRequisitionReportTable(string format)
        {
            StringBuilder reqReport = new StringBuilder();
            EmployeeReferralDataSource referralDateSource = new EmployeeReferralDataSource();
            bool IsMyReport = _IsMyReferralReport ? true : false;
            bool IsTeamRefferalReport = _isTeamReport ? true : false;
            string base64Data = "";
            string path = "";
            string currentMemberJobpostingId = _IsMyReferralReport ? CurentMemberJobPostingId : "";
            List<EmployeeReferral> referralList = referralDateSource.GetPaged_ForInternalDetail((ddlRequisition.SelectedIndex > 0 ? Convert.ToInt32(ddlRequisition.SelectedValue) : 0), (ddlRefferedby.SelectedIndex > 0 ? Convert.ToInt32(ddlRefferedby.SelectedValue) : 0), dtReferralDate.StartDate, dtReferralDate.EndDate, IsMyReport, currentMemberJobpostingId, IsTeamRefferalReport, CurrentMember.Id, (ddlTeam.SelectedIndex > 0 ? Convert.ToInt32(ddlTeam.SelectedValue) : 0), null, -1, -1);
            if (referralList != null)
            {
                path = AppDomain.CurrentDomain.BaseDirectory + "Images/logo-left-75px.png";

                if (System.IO.File.Exists(path))
                {
                    base64Data = Convert.ToBase64String(System.IO.File.ReadAllBytes(path));
                }
                reqReport.Append("<table style='font-family : calibriz;width:100%; border-collapse:collapse; border-spacing: 1px;' align='left' border = '1' bordercolor='#000000'  cellspacing='0' cellpadding='0'>");

                reqReport.Append("    <tr>");
                if (format == "pdf")
                {
                    reqReport.Append("        <td align=left  width='300' height='75'>&nbsp;<img src='data:image/png;base64," + base64Data + "' style= style='height:56px;width:56px'/></td>"); //0.7
                }
                else
                {
                    reqReport.Append("        <td align=left  width='300' height='75'>&nbsp;<img src='" + UrlConstants.ApplicationBaseUrl + "Images/logo-left-75px.png'; style= style='height:56px;width:56px'/></td>"); //0.7
                }

                reqReport.Append("    </tr>");
                reqReport.Append(" <tr>");
                reqReport.Append("     <th>Date</th>");
                reqReport.Append("     <th>Requisition</th>");
                reqReport.Append("     <th>PS #</th>");
                reqReport.Append("     <th>Candidate ID #</th>");
                reqReport.Append("     <th>Candidate</th>");
                reqReport.Append("     <th>BU</th>");
                reqReport.Append("     <th>Location</th>");
                reqReport.Append("     <th>Candidate Status</th>");
                reqReport.Append("     <th>Assigned Recruiters</th>");
                reqReport.Append(" </tr>");

                foreach (EmployeeReferral log in referralList)
                {
                    if (log != null)
                    {
                        reqReport.Append(" <tr>");
                        reqReport.Append("     <td>" + log.CreateDate.ToShortDateString() + "&nbsp;</td>");
                        reqReport.Append("     <td>" + log.JobTitle + "&nbsp;</td>");
                        reqReport.Append("     <td>" + log.EmployeeId + "&nbsp;</td>");
                        reqReport.Append("     <td>" + "A" + log.MemberId.ToString() + "&nbsp;</td>");
                        reqReport.Append("     <td>" + log.CandidateName + "&nbsp;</td>");
                        reqReport.Append("     <td>" + log.BUName + "&nbsp;</td>");
                        reqReport.Append("     <td>" + log.Location + "&nbsp;</td>");
                        reqReport.Append("     <td>" + log.CandiateStatus + "&nbsp;</td>");
                        IList<JobPostingHiringTeam> hiringteam = null;
                        string Reqassignedmanagers = "";
                        //if (log.JobpostingId != string.Empty && log.JobpostingId != "0")
                           
                        //if (hiringteam != null)
                        //{
                        //    foreach (JobPostingHiringTeam assingedmember in hiringteam)
                        //    {
                        //        Reqassignedmanagers = Reqassignedmanagers + "," + assingedmember.Name;
                        //    }
                        //}

                        if (log.JobpostingId != string.Empty && log.JobpostingId != "0")
                        {
                            IList<JobPostingHiringTeam> ls = Facade.GetAllJobPostingHiringTeamByJobPostingId(Convert.ToInt32(log.JobpostingId));
                            foreach (JobPostingHiringTeam JobHireTam in ls)
                            {
                                Reqassignedmanagers = Reqassignedmanagers + "," + JobHireTam.Name;
                            }
                        }        
                        reqReport.Append("     <td>" + Reqassignedmanagers.TrimStart(',') + "&nbsp;</td>");
                        reqReport.Append(" </tr>");
                    }
                }

                reqReport.Append("    <tr>");
                if (format == "pdf")
                {
                    reqReport.Append("        <td align=left  width='300' height='75'>&nbsp;<img src='data:image/png;base64," + base64Data + "' style= style='height:56px;width:56px'/></td>"); //0.7
                }
                else
                {
                    reqReport.Append("        <td align=left  width='300' height='75'>&nbsp;<img src='" + UrlConstants.ApplicationBaseUrl + "Images/logo-left-75px.png';  style= style='height:56px;width:56px'/></td>"); //0.7
                } reqReport.Append("    </tr>");

                reqReport.Append(" </table>");


            }
            return reqReport.ToString();
        }

        void PopulateRequisition(int StatusId)
        {
            ddlRequisition.Items.Clear();
            ddlRequisition.DataSource = Facade.JobPosting_GetAllInternalEmployeeReferralEnabled();
            ddlRequisition.DataValueField = "Id";
            ddlRequisition.DataTextField = "JobTitle";
            ddlRequisition.DataBind();
            ddlRequisition.Items.Insert(0, new ListItem("Any", "0"));
        }

        #endregion

        #region Events


        private void PrepareView()
        {

            FillEmployeeReferrerList();
            FillTeamList(CurrentMember.Id);

            PopulateRequisition(0);
        }
        protected void ddlTeam_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void ddlRefferedby_SelectedIndexChanged(object sender, EventArgs e)
        {
            int ReferrerID = Convert.ToInt32(ddlRefferedby.SelectedValue);
            if (ReferrerID == 0)
            {
                ddlRequisition.DataSource = Facade.JobPosting_GetAllEmployeeReferralEnabled();
            }
            else
            {
                ddlRequisition.DataSource = Facade.JobPosting_GetAllEmployeeReferralEnabledByReferrerId(ReferrerID);
            }
            ddlRequisition.DataValueField = "Id";
            ddlRequisition.DataTextField = "JobTitle";
            ddlRequisition.DataBind();
            ddlRequisition.Items.Insert(0, new ListItem("Any", "0"));
        }

        private void FillEmployeeReferrerList()
        {
            if (_IsMyReferralReport)
            {
                ddlRefferedby.DataSource = Facade.GetEmployeeReferrerListByJobpostingId(CurentMemberJobPostingId);

            }
            else if (_isTeamReport)
            {
                ddlRefferedby.DataSource = Facade.GetEmployeeReferrerListByTeamLeaderId(CurrentMember.Id);
            }
            else
            {
                ddlRefferedby.DataSource = Facade.GetAllEmployeeReferrerList();
            }


            ddlRefferedby.DataValueField = "Id";
            ddlRefferedby.DataTextField = "Name";
            ddlRefferedby.DataBind();
            ddlRefferedby.Items.Insert(0, new ListItem("Any", "0"));
        }
        private void FillTeamList(int teamLeaderId)
        {
            ddlTeam.DataSource = Facade.GetAllTeamByTeamLeaderId(teamLeaderId);
            ddlTeam.DataTextField = "Title";
            ddlTeam.DataValueField = "Id";
            ddlTeam.DataBind();
            ddlTeam.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_ANY, "0"));

        }
        private void FillJobPosting(int CurrentMemberId)
        {
            if (CurrentMemberId > 0)
            {
                if (_IsMyReferralReport)
                {
                    //ddlRequisition.DataSource = Facade.JobPosting_GetAllEmployeeReferralEnabledByMemberId(CurrentMember.Id);
                    ddlRequisition.DataSource = Facade.JobPosting_GetAllInternalEmployeeReferralEnabled();
                }
                else
                {
                    //ddlRequisition.DataSource = Facade.JobPosting_GetAllEmployeeReferralEnabledByTeamLeaderId(CurrentMember.Id);
                    ddlRequisition.DataSource = Facade.JobPosting_GetAllInternalEmployeeReferralEnabled();
                }
            }
            else
            {
                //ddlRequisition.DataSource = Facade.JobPosting_GetAllEmployeeReferralEnabled();
                ddlRequisition.DataSource = Facade.JobPosting_GetAllInternalEmployeeReferralEnabled();            }

            ddlRequisition.DataValueField = "Id";
            ddlRequisition.DataTextField = "JobTitle";
            ddlRequisition.DataBind();
            ddlRequisition.Items.Insert(0, new ListItem("Any", "0"));
        }
        protected void Page_Load(object sender, EventArgs e)
        {

            ddlTeam.Attributes.Add("onchange", "TeamOnChangeForRequsition('" + ddlTeam.ClientID + "','" + ddlRequisition.ClientID + "','" + hdnRequisition.ClientID + "','" + CurrentMember.Id + "')");
            ddlRequisition.Attributes.Add("onchange", "RequsitionOnChange('" + ddlRequisition.ClientID + "','" + hdnRequisition.ClientID + "')");

            if (_IsMyReferralReport && ddlRequisition.SelectedValue == "" || ddlRequisition.SelectedValue == "0")
            {
                hdnCurrentMemberJobpostId.Value = CurentMemberJobPostingId != string.Empty ? CurentMemberJobPostingId : string.Empty;
            }
            else
            {
                hdnCurrentMemberJobpostId.Value = ddlRequisition.SelectedValue;
            }

            divReferredBy.Visible = false;

            hdnMyReport.Value = _IsMyReferralReport ? "true" : "false";
            if (IsPostBack)
            {
                hdnIsPostback.Value = "1";
            }

            if (!IsPostBack)
            {
                divExportButtons.Visible = false;
                dtReferralDate.setDateRange(DateTime.Now.AddDays(-6), DateTime.Now);
                txtSortColumn.Text = "btnDate";
                txtSortOrder.Text = "Desc";

                PrepareView();
            }
            CustomSiteMap CustomMap = new CustomSiteMap();
            CustomMap = Facade.GetAllCustomSiteMapByParentIdAndMemberPrivilege(358, CurrentMember.Id);
            if (CustomMap == null) _IsAccessToCandidate = false;
            else
            {
                SitemapIdForCandidate = CustomMap.Id;
                UrlForCandidate = "~/" + CustomMap.Url.ToString();
            }
            if (dtReferralDate.StartDate == DateTime.MinValue) hdnStartDate.Value = "0";
            else hdnStartDate.Value = dtReferralDate.StartDate.ToString("yyyyMMdd");
            if (dtReferralDate.EndDate == DateTime.MinValue) hdnEndDate.Value = "0";
            else hdnEndDate.Value = dtReferralDate.EndDate.ToString("yyyyMMdd");
        }


        protected void lsvReferralDetail_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                EmployeeReferral referral = ((ListViewDataItem)e.Item).DataItem as EmployeeReferral;

                if (referral != null)
                {
                    divExportButtons.Visible = true;
                    Label lblDate = (Label)e.Item.FindControl("lblDate");
                    HyperLink hlkJobTitle = (HyperLink)e.Item.FindControl("hlkJobTitle");
                    Label lblReferrerPS = (Label)e.Item.FindControl("lblReferrerPS");
                    Label lblCandidateID = (Label)e.Item.FindControl("lblCandidateID");
                    lblCandidateID.Text = "A" + referral.MemberId.ToString();                   
                    HyperLink hlnkCandidate = (HyperLink)e.Item.FindControl("hlnkCandidate");
                    Label lblCandidateStatus = (Label)e.Item.FindControl("lblCandidateStatus");
                    Label lblBU = (Label)e.Item.FindControl("lblBU");
                    Label lblLocation = (Label)e.Item.FindControl("lblLocation");
                    Label lblAssignedRecruiters = (Label)e.Item.FindControl("lblAssignedRecruiters");

                    lblCandidateStatus.Text = referral.CandiateStatus;
                    lblBU.Text = referral.BUName;
                    lblLocation.Text = referral.Location;
                    lblDate.Text = referral.CreateDate.ToShortDateString();
                    hlnkCandidate.Text = referral.CandidateName;
                    lblReferrerPS.Text = referral.Gid;

                    hlkJobTitle.Attributes.Add("onclick", "EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/JobDetail.aspx?JID=" + referral.JobpostingId + "&FromPage=JobDetail" + "','700px','570px'); return false;");
                    hlkJobTitle.Text = referral.JobTitle;
                    IList<JobPostingHiringTeam> hiringteam = null;
                    string Reqassignedmanagers = "";
                    if (referral.JobpostingId != string.Empty && referral.JobpostingId != "0")
                    {                        
                        IList<JobPostingHiringTeam> ls = Facade.GetAllJobPostingHiringTeamByJobPostingId(Convert.ToInt32(referral.JobpostingId));
                        foreach (JobPostingHiringTeam JobHireTam in ls)
                        {
                            Reqassignedmanagers = Reqassignedmanagers + ","+ JobHireTam.Name;                           
                        }
                    }                   
                    lblAssignedRecruiters.Text = Reqassignedmanagers.TrimStart(',');
                    if (_IsAccessToCandidate)
                    {
                        if (SitemapIdForCandidate > 0 && UrlForCandidate != string.Empty)
                            ControlHelper.SetHyperLink(hlnkCandidate, UrlForCandidate, string.Empty, referral.CandidateName, UrlConstants.PARAM_MEMBER_ID, StringHelper.Convert(referral.MemberId), UrlConstants.PARAM_SITEMAP_ID, SitemapIdForCandidate.ToString(), UrlConstants.PARAM_SITEMAP_PARENT_ID, UrlConstants.Candidate.ATS_OVERVIEW_SITEMAP_PARENTID);
                    }
                }
            }
        }

        protected void lsvReferralDetail_PreRender(object sender, EventArgs e)
        {

            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvReferralDetail.FindControl("pagerControl");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)PagerControl.FindControl("pager");
                if (pager != null)
                {
                    DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                    if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
                }
                HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
                if (hdnRowPerPageName != null) hdnRowPerPageName.Value = "ReferralReportRowPerPage";
            }
            PlaceUpDownArrow();

            if (lsvReferralDetail != null)
            {
                if (lsvReferralDetail.Items.Count == 0)
                {
                    lsvReferralDetail.DataSource = null;
                    lsvReferralDetail.DataBind();
                }
            }


        }

        protected void lsvReferralDetail_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Sort")
                {
                    LinkButton lnkbutton = (LinkButton)e.CommandSource;
                    if (txtSortColumn.Text == lnkbutton.ID)
                    {
                        if (txtSortOrder.Text == "ASC") txtSortOrder.Text = "DESC";
                        else txtSortOrder.Text = "ASC";
                    }
                    else
                    {
                        txtSortColumn.Text = lnkbutton.ID;
                        txtSortOrder.Text = "ASC";
                    }
                }
            }
            catch
            { }
        }

        protected void btnApply_Click(object sender, EventArgs e)
        {
            divExportButtons.Visible = false;
            lsvReferralDetail.DataBind();
            hdnCurrentMemberJobpostId.Value = ddlRequisition.SelectedValue;
        }
        protected void btnReset_Click(object sender, EventArgs e)
        {
            ddlRequisition.SelectedIndex = -1;
            ddlRefferedby.SelectedIndex = 0;
            ddlTeam.SelectedIndex = 0;
            dtReferralDate.setDateRange(DateTime.Now.AddDays(-6), DateTime.Now);
            if (dtReferralDate.StartDate == DateTime.MinValue) hdnStartDate.Value = "0";
            else hdnStartDate.Value = dtReferralDate.StartDate.ToString("yyyyMMdd");
            if (dtReferralDate.EndDate == DateTime.MinValue) hdnEndDate.Value = "0";
            else hdnEndDate.Value = dtReferralDate.EndDate.ToString("yyyyMMdd");
            FillJobPosting(Convert.ToInt32(ddlRefferedby.SelectedIndex));
        }
        protected void btnExportToPDF_Click(object sender, EventArgs e)
        {
            repor("pdf");
        }
        protected void btnExportToExcel_Click(object sender, EventArgs e)
        {
            repor("excel");
        }
        protected void btnExportToWord_Click(object sender, EventArgs e)
        {
            repor("word");
        }

        #endregion

    }
}
