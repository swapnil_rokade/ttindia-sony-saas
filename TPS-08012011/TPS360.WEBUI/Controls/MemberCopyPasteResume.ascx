﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MemberCopyPasteResume.ascx.cs"
    Inherits="TPS360.Web.UI.ControlMemberCopyPasteResume  " %>
<%--<%@ Register Assembly="Infragistics2.WebUI.WebHtmlEditor.v7.3, Version=7.3.20073.38, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.WebUI.WebHtmlEditor" TagPrefix="ighedit" %>--%>
<%@ Register Src ="~/Controls/HtmlEditor.ascx" TagName ="HtmlEditor" TagPrefix ="ucl" %>
<asp:UpdatePanel ID="upCopyPaste" runat ="server" >
<ContentTemplate >

<script type="text/javascript">
function ShowUploadResume()
{
document .getElementById ('<%= divUploadResume.ClientID %>').style.display='';
}

function UpdateUploadFile(file,message)
{
     var hlkUploadedDoc =document .getElementById ('<%= hlkUploadedDoc.ClientID %>');
     if(file !='')    hlkUploadedDoc.innerHTML=file ;
      var divUploadResume =document .getElementById ('<%= divResume.ClientID %>');
      divUploadResume .style.display="";
      var divUploadResume =document .getElementById ('<%= divUploadResume.ClientID %>');
      divUploadResume .style.display="none";
        ShowMessage('<%= lblMessage.ClientID %>',message,false );
}



</script>

 <asp:Label ID="lblMessage" runat="server" EnableViewState ="false" ></asp:Label>
<div>


    <div class="TableRow" style="text-align: center">
        <asp:HiddenField ID="hfMemberId" runat="server" Value="0" />
        <asp:HiddenField ID="hfResumeBuilderOption" runat="server" Value="0" />
    </div>
      <div class ="TableRow" id="divResume" runat ="server" >
        <div class ="TableFormLeble" style =" text-align :left; width : auto ; ">Most Recent Resume:</div>
          <div class="TableFormContent">
             <asp:Label ID="hlkUploadedDoc" runat ="server" ></asp:Label><br />
             <a href ="#" onclick ="ShowUploadResume()" >Attach New Resume</a>
          </div> 
    </div>
    <asp:UpdatePanel ID="updoc" runat ="server" >
    <ContentTemplate >
    <div  id ="divUploadResume" runat ="server" style =" display : none ; " align="left" >
              <div class ="TableRow"  >
            <div class ="TableFormLeble" style =" width : auto ;">Upload Resume File:</div>
              <div class="TableFormContent" style =" padding-top : 0px;">
                <iframe src="../CandidatePortal/ResumeUpload.aspx"  id="docFrame" runat="server" width ="50%" frameborder ="0" height ="85px"></iframe>
              </div> 
        </div>
    
    
            
            </div> 
            
             <div class="TableRow">
           
        </div>
    </ContentTemplate>
  
    </asp:UpdatePanel>
 
    <div class="TableRow" style="text-align: left">
        <div class="TableFormLeble" style="width: 110px;">
            <asp:Label ID="lblCopyPasteResume" runat="server" Text="Copy/Paste Resume"></asp:Label>:
        </div>
    </div>
    <div class="TableRow" style="text-align: left">
      
        <div class="TableFormContent">
                <ucl:HtmlEditor ID="txtCopyPasteResume" runat ="server" />
        </div>        
    </div>
    
    <div class="TableRow" style="text-align: center;">
        <asp:Button ID="btnSave" CssClass="CommonButton" runat="server" Text="Save" OnClick="btnSaveCopyPaste_Click"
            ValidationGroup="CopyPasteResume" TabIndex ="2" />
    </div>
    <br />
  
</div>
</ContentTemplate>

</asp:UpdatePanel>