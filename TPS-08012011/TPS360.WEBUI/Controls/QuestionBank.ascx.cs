﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName            :   Questionbank.cs
    Description         :   This page is used to fill up Interviewer Feed back .
    Created By          :   Prasanth
    Created On          :   16/Oct/2015
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
  
 ---------------------------------------------------------------------------------------------------------------------------------------------------------------------        
*/
using System;
using System.Text;
using System.Web.UI.WebControls;
using TPS360.BusinessFacade;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Web.UI.Helper;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Generic;
namespace TPS360.Web.UI
{
    public partial class ControlsQuestionBank : BaseControl
    {

        #region Member Variables

        private int _memberId
        {


            get
            {
                int mId = 0;

                if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]))
                {
                    mId = Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]);

                }

                return mId;
            }
        }
        QuestionBank _QuestionBank = null;

 
        #endregion

        #region Properties

        public int QuestionBankId
        {
            get
            {
                return (int)(ViewState[this.ClientID + "_QuestionBank"] ?? 0);
            }
            set
            {
                ViewState[this.ClientID + "_QuestionBank"] = value;
            }
        }
        private QuestionBank CurrentQuestionBank
        {
            get
            {
                if (_QuestionBank == null)
                {
                    if (QuestionBankId > 0)
                    {
                        _QuestionBank = Facade.QuestionBank_GetById(QuestionBankId);
                    }

                    if (_QuestionBank == null)
                    {
                        _QuestionBank = new QuestionBank();
                    }
                }

                return _QuestionBank;
            }
        }
        #endregion

        #region Methods

        private void BindList()
        {
            //odsQuestionBankList.SelectParameters["memberId"].DefaultValue = _memberId.ToString();
            this.lsvQuestionBankList.DataBind();
        }
        private void SaveQuestionBank()
        {
            if (IsValid)
            {
                try
                {
                    QuestionBank QuestionBank = BuildQuestionBank();
                    //MemberNote memberNote = BuildMemberNot();
                    txtQuestion.Text = MiscUtil.RemoveScript(txtQuestion.Text);
                    if (QuestionBankId == 0)
                    {
                        if (txtQuestion.Text.ToString() != string.Empty)
                        {
                            QuestionBank = Facade.QuestionBank_Add(QuestionBank);
                            //memberNote.QuestionBankId = QuestionBank.Id;
                            
                            //Facade.AddMemberNote(memberNote);
                            MiscUtil.ShowMessage(lblMessage, "Assessment has been added succesfully.", false);
                        }
                    }
                    else
                    {
                        if (txtQuestion.Text.ToString() != string.Empty)
                        {
                            Facade.QuestionBank_Update(QuestionBank);
                            MiscUtil.ShowMessage(lblMessage, "Assessment has been updated succesfully.", false);
                        }
                    }
               
                    ClearText();
                }
                catch (ArgumentException ex)
                {
                    MiscUtil.ShowMessage(lblMessage, ex.Message, true);
                }
            }



        }

        private QuestionBank BuildQuestionBank()
        {
            QuestionBank QuestionBank = null;
            if (QuestionBankId!= 0)
            {
                QuestionBank = CurrentQuestionBank;
            }
            else
            {
                QuestionBank = new QuestionBank();
            }
            QuestionBank.QuestionBankType_Id = Convert.ToInt32(ddlAssessmentForm.SelectedValue);
            QuestionBank.AnswerType_Id = Convert.ToInt32(DdlAnswerType.SelectedValue);
           // QuestionBank.Question = MiscUtil.RemoveScript(txtQuestion.Text.Trim()); //Code commented by pravin khot old code 13/Jan/2016

            QuestionBank.Question = txtQuestion.Text.Trim();//Code added by pravin khot  13/Jan/2016
            QuestionBank.CreatorId = base.CurrentMember.Id;
            QuestionBank.UpdatorId = base.CurrentMember.Id;
            return QuestionBank;
        }

        //private MemberNote BuildMemberNot()
        //{
        //    MemberNote memberNote = new MemberNote();

        //    if (_memberId > 0)
        //    {
        //        memberNote.MemberId = _memberId;
        //    }
        //    else
        //    {
        //        memberNote.MemberId = base.CurrentMember.Id;
        //    }
        //    memberNote.CreatorId = base.CurrentMember.Id;
        //    return memberNote;
        //}

        private void PrepareView()
        {

            MiscUtil.PopulateQuestionBankType(ddlAssessmentForm, Facade);
            MiscUtil.PopulateAnswerType(DdlAnswerType, Facade);

            //Code Commented 0123456789
            //ListItem li = ddlAssessmentForm.Items.FindByText("Added from Parser");
            //if (li.Text == "Added from Parser")
            //    ddlAssessmentForm.Items.Remove(li);




        }

        private void PrepareEditView()
        {
            QuestionBank QuestionBankActivities = CurrentQuestionBank;
            ddlAssessmentForm.SelectedValue = Convert.ToString(QuestionBankActivities.QuestionBankType_Id);
            txtQuestion.Text = QuestionBankActivities.Question;
             MiscUtil.RemoveScript(QuestionBankActivities.Question, string.Empty);
            DdlAnswerType.SelectedValue = Convert.ToString(QuestionBankActivities.AnswerType_Id);

        }

        private void PlaceUpDownArrow()
        {

            try
            {
                LinkButton lnk = (LinkButton)lsvQuestionBankList.FindControl(txtSortColumn.Text);
                System.Web.UI.HtmlControls.HtmlTableCell im = (System.Web.UI.HtmlControls.HtmlTableCell)lnk.Parent;
                im.EnableViewState = false;
                im.Attributes.Add("class", (txtSortOrder.Text == "ASC" ? "Ascending" : "Descending"));
            }
            catch
            {
            }
        }


        private void ClearText()
        {
            ddlAssessmentForm.SelectedIndex = 0;
            DdlAnswerType.SelectedIndex = 0;
            txtQuestion.Text = string.Empty;
            QuestionBankId = 0;
        }

        #endregion

        #region Events

        #region Page event

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                txtQuestion.Focus();
                PrepareView();
                BindList();
                txtSortColumn.Text = "btnDateTime";
                txtSortOrder.Text = "Desc";
                PlaceUpDownArrow();


            }
            lblMessage.Text = "";
            lblMessage.CssClass = "";

            string pagesize = "";
            pagesize = (Request.Cookies["QuestionBankRowPerPage"] == null ? "" : Request.Cookies["QuestionBankRowPerPage"].Value);
            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvQuestionBankList.FindControl("pagerControl_Requisition");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)PagerControl.FindControl("pager");
                if (pager != null) pager.PageSize = Convert.ToInt32(pagesize == "" ? "10" : pagesize);
            }



        }


        protected void btnSave_Click(object sender, EventArgs e)
        {
            SaveQuestionBank();
            PlaceUpDownArrow();
        }

        #endregion

        #region ListView Events

        protected void lsvQuestionBankList_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            bool _isAdmin = base.IsUserAdmin;



            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                QuestionBank QuestionBank = ((ListViewDataItem)e.Item).DataItem as QuestionBank;

                if (QuestionBank != null)
                {
                    Label lblAssessmentForm = (Label)e.Item.FindControl("lblAssessmentForm");
                    Label lblAnswer = (Label)e.Item.FindControl("lblAnswer");
                    Label lblQuestion = (Label)e.Item.FindControl("lblQuestion");
                    //Label lblNote = (Label)e.Item.FindControl("lblNote");

                    ImageButton btnEdit = (ImageButton)e.Item.FindControl("btnEdit");
                    ImageButton btnDelete = (ImageButton)e.Item.FindControl("btnDelete");

                    //lblDateTime.Text = QuestionBank.CreateDate.ToShortDateString() + " " + QuestionBank.CreateDate.ToShortTimeString();
                    Member member = Facade.GetMemberById(QuestionBank.CreatorId);
                    //lblManager.Text = "";

                    //if (member != null)
                    //{
                    //    lblManager.Text = member.FirstName + " " + member.MiddleName + " " + member.LastName;
                    //}


                    if (QuestionBank.QuestionBankType_Id > 0)
                    {
                        GenericLookup g = Facade.GetGenericLookupById(QuestionBank.QuestionBankType_Id);
                      
                        if (g != null)
                            lblAssessmentForm.Text = g.Name;
                    
                    }

                    if (QuestionBank.AnswerType_Id > 0)
                    {
                        GenericLookup g = Facade.GetGenericLookupById(QuestionBank.AnswerType_Id);

                        if (g != null)
                            lblAnswer.Text = g.Name;

                    }

                    lblQuestion.Text = QuestionBank.Question;

                    //if (QuestionBank.JobPostingId != null && QuestionBank.JobPostingId > 0)
                    //    lblNote.Text = QuestionBank.JobPostingCode + " - " + QuestionBank.JobTitle + "<br/>" + QuestionBank.NoteDetail.Replace("\r\n", "</br>");
                    //else
                    //    lblNote.Text = QuestionBank.NoteDetail.Replace("\r\n", "</br>");

                    btnDelete.OnClientClick = "return ConfirmDelete('Interview QuestionBank')";   //Code added by pravin khot add DELETE confirmation message on 13/Jan/2016

                    btnEdit.CommandArgument = StringHelper.Convert(QuestionBank.QuestionBank_Id);
                    btnDelete.CommandArgument = StringHelper.Convert(QuestionBank.QuestionBank_Id);
                }


            }





        }
        protected void lsvQuestionBankList_PreRender(object sender, EventArgs e)
        {

            System.Web.UI.HtmlControls.HtmlTableRow row = (System.Web.UI.HtmlControls.HtmlTableRow)lsvQuestionBankList.FindControl("row");
            if (row == null)
            {
                lsvQuestionBankList.DataSource = null;
                lsvQuestionBankList.DataBind();
            }

            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvQuestionBankList.FindControl("pagerControl_Requisition");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)PagerControl.FindControl("pager");
                if (pager != null)
                {
                    DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                    if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
                }
                HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
                if (hdnRowPerPageName != null)
                {
                    hdnRowPerPageName.Value = "QuestionBankRowPerPage";   //code change by pravin khot using ddlrowPerPage.selected value on 13/Jan/2016
                }
            }
            PlaceUpDownArrow();


        }
        protected void lsvQuestionBankList_ItemCommand(object sender, ListViewCommandEventArgs e)
        {

            try
            {

                if (e.CommandName == "Sort")
                {
                    LinkButton lnkbutton = (LinkButton)e.CommandSource;
                    if (txtSortColumn.Text == lnkbutton.ID)
                    {
                        if (txtSortOrder.Text == "ASC") txtSortOrder.Text = "DESC";
                        else txtSortOrder.Text = "ASC";
                    }
                    else
                    {
                        txtSortColumn.Text = lnkbutton.ID;
                        txtSortOrder.Text = "ASC";
                    }
                }
            }
            catch
            {
            }

            int id;

            int.TryParse(e.CommandArgument.ToString(), out id);

            if (id > 0)
            {
                if (string.Equals(e.CommandName, "EditItem"))
                {
                    QuestionBankId = id;
                    PrepareEditView();
                    int rows = lsvQuestionBankList.Items.Count;
                    for (int i = 0; i < rows; i++)
                    {
                        ListViewDataItem row = lsvQuestionBankList.Items[i];
                        ImageButton delete = (ImageButton)row.FindControl("btnDelete");
                        delete.Enabled = false;
                    }
                }
                else if (string.Equals(e.CommandName, "DeleteItem"))
                {
                    try
                    {
                        if (Facade.QuestionBank_DeleteById(id))
                        {
                            BindList();
                            MiscUtil.ShowMessage(lblMessage, "Assessment Form has been successfully deleted.", false);
                        }
                        if (QuestionBankId == id)
                            ClearText();

                    }
                    catch (ArgumentException ex)
                    {
                        MiscUtil.ShowMessage(lblMessage, ex.Message, true);
                    }
                }
            }
            BindList();
        }

        #endregion



        #endregion



    }
}


