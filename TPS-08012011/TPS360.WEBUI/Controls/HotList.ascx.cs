﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: HotList.ascx.cs
    Description: This is the user control page used to create hotlist.
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1             Oct-01-2008          Jagadish            Defect id: 8720; 'Delete Information alert message' was repeating, modified the code to 
                                                             correct it.
    0.2             Nov-5-2008           Gopala Swamy        Defect id:9044 :reset to _groupid=0 after saving the hotlist  
    0.3             Jan-28-2009          Shivanand           Defect #8879; In the method "Page_Load()", text is changed on label "NoOfCandidates".
    0.4             March-02-2009        Gopala Swamy        Defect #10025 :Put If condition
    0.5             Sep-24-2009            Veda              Defect #11519 :Blank datagrid was displying
 *  0.6             Feb-15-2010          Nagarathna V.B      Defect #12123 :my hotlist deletion is commented in "lsvHotList_ItemCommand()"
 *  0.7             May-13-2010          Sudarshan.R.        Defect #12787 :Removed isMy condition while saving hotlist.
------------------------------------------------------------------------------------------------------------------------------------------- 
*/
using System;
using System.Web.UI.WebControls;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Web.UI.Helper;

namespace TPS360.Web.UI
{
    public partial class ctlHotList : BaseControl
    {
        #region Member Variables

        private static Int32 _memberId = 0;
        private static Int32 _groupId = 0;

        MemberGroup memberGroup = null;

        #endregion

        #region Properties

        public int MemberId
        {
            set { _memberId = value; }
        }

        private bool isMy = false;
        public bool IsMy
        {
            set { isMy = value; }
        }

        private string candidateOrConsultant;
        public string CandidateOrConsultant
        {
            set { candidateOrConsultant = value; }
        }

        private int groupType = 0;
        public int GroupType
        {
            set { groupType = value; }
        }
        #endregion

        #region Methods

        private void BindList()
        {
            this.lsvHotList.DataBind();
        }

        private void SaveMemberGroup()
        {
            if (IsValid)
            {
                try
                {
                    MemberGroup memberGroup = BuildMemberGroup();

                    if (memberGroup.IsNew)
                    {
                        memberGroup = Facade.AddMemberGroup(memberGroup);
                        MemberGroupManager newManager = new MemberGroupManager();
                        newManager.MemberGroupId = memberGroup.Id;
                        newManager.MemberId = _memberId;
                        newManager.CreateDate = DateTime.Now;
                        newManager.CreatorId = base.CurrentMember.Id;
                        Facade.AddMemberGroupManager(newManager);
                        ClearText();
                        MiscUtil.ShowMessage(lblMessage, "Successfully added Hot List.", false);
                    }
                    else
                    {
                        Facade.UpdateMemberGroup(memberGroup);
                        ClearText();
                        MiscUtil.ShowMessage(lblMessage, "Successfully updated Hot List.", false);
                    }
                }
                catch (ArgumentException ex)
                {
                    MiscUtil.ShowMessage(lblMessage, ex.Message, true);
                }
                BindList();
                _groupId = 0;
            }
        }

        private MemberGroup BuildMemberGroup()
        {

            if (_groupId > 0)
            {
                memberGroup = Facade.GetMemberGroupById(_groupId);
            }
            else
            {
                memberGroup = new MemberGroup();
            }

            memberGroup.Name =MiscUtil .RemoveScript ( txtGroupName.Text.Trim());
            memberGroup.GroupType = groupType;
            memberGroup.Description =MiscUtil .RemoveScript ( txtGroupDesc.Text.Trim());
            memberGroup.CreatorId = base.CurrentMember.Id;
            memberGroup.UpdatorId = base.CurrentMember.Id;
            
            return memberGroup;
        }
        private void ClearText()
        {
            txtGroupName.Text = "";
            txtGroupDesc.Text = "";
        }
        private void PlaceUpDownArrow()
        {
            try
            { 
                LinkButton lnk = (LinkButton)lsvHotList.FindControl(hdnSortColumn.Value);
                System.Web.UI.HtmlControls.HtmlTableCell im = (System.Web.UI.HtmlControls.HtmlTableCell)lnk.Parent;
                im.EnableViewState = false;
                im.Attributes.Add("class", ( hdnSortOrder .Value  == "ASC" ? "Ascending" : "Descending"));
            }
            catch
            {
            }

        }
        #endregion

        #region Events

        #region Page Event

        protected void Page_Load(object sender, EventArgs e)
        {
            _memberId = base.CurrentMember.Id;
            if (!IsPostBack)
            {
                BindList();
                _groupId = 0;
                hdnSortColumn.Value = "btnGroupName";
                hdnSortOrder.Value = "ASC";
                PlaceUpDownArrow();
                
            }

            string pagesize = "";
            if (Page.Request.Url.AbsoluteUri.ToString().Contains("MyHotList.aspx"))  pagesize = (Request.Cookies["MyHotListRowPerPage"] == null ? "" : Request.Cookies["MyHotListRowPerPage"].Value); 
            else  pagesize = (Request.Cookies["MasterHotListRowPerPage"] == null ? "" : Request.Cookies["MasterHotListRowPerPage"].Value); ;
             ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvHotList.FindControl("pagerControl");
             if (PagerControl != null)
             {
                 DataPager pager = (DataPager)PagerControl.FindControl("pager");
                 if (pager != null) pager.PageSize = Convert.ToInt32(pagesize == "" ? "20" : pagesize);
             }
            LinkButton  btnNoOfCandidates = (LinkButton )lsvHotList.FindControl("btnNoOfCandidates");  // 0.3
            if (btnNoOfCandidates != null)
                btnNoOfCandidates.Text = "# of " + candidateOrConsultant;  // 0.3
            lblMessage.Text = "";
            lblMessage.CssClass = "";
        }

        protected void btnSaveHotList_Click(object sender, EventArgs e)
        {
            SaveMemberGroup();
            _groupId = 0;
        }

        #endregion

        #region ListView Events
        protected void lsvHotList_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            string link = string.Empty;
            SecureUrl url = null;
            bool _IsAdmin = base.IsUserAdmin;
            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                MemberGroup memberGroup = ((ListViewDataItem)e.Item).DataItem as MemberGroup;
                if (memberGroup != null)
                {
                    if (groupType == Convert.ToInt32(MemberGroupType.Candidate))
                    {
                        url = UrlHelper.BuildSecureUrl(UrlConstants.Candidate.CANDIDATE_MANAGEHOTLIST, "", UrlConstants.PARAM_ID, memberGroup.Id.ToString());
                    }
                    CheckBox chkGroup = (CheckBox)e.Item.FindControl("chkGroup");
                    Label lblGroupName = (Label)e.Item.FindControl("lblGroupName");
                    Label lblDescription = (Label)e.Item.FindControl("lblDescription");
                    Label lblNoofCandidate = (Label)e.Item.FindControl("lblNoofCandidate");
                    Label lblOwner = (Label)e.Item.FindControl("lblOwner");                   
                    ImageButton btnEdit = (ImageButton)e.Item.FindControl("btnEdit");
                    ImageButton btnDelete = (ImageButton)e.Item.FindControl("btnDelete");
                    link = "<a href='" + url + "'>" + memberGroup.Name + "</a>";
                    lblGroupName.Text = link;
                    lblDescription.Text = memberGroup.Description;
                    lblNoofCandidate.Text = memberGroup.CandidateCount.ToString(); 
                    lblOwner.Text = memberGroup.ManagerName; 
                    btnDelete.Visible = base.IsUserAdmin;
                    btnDelete.OnClientClick = "return ConfirmDelete('Hot list?')";
                    btnEdit.CommandArgument =btnDelete.CommandArgument= StringHelper.Convert(memberGroup.Id);
                }
            }
        }
        protected void lsvHotList_PreRender(object sender, EventArgs e)
        {
            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvHotList .FindControl("pagerControl");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)PagerControl.FindControl("pager");
                if (pager != null)
                {
                    DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                    if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
                }
                HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
                if (hdnRowPerPageName != null)
                {
                    if (Page.Request.Url.AbsoluteUri.ToString().Contains("MyHotList.aspx")) hdnRowPerPageName.Value = "MyHotListRowPerPage";
                    else hdnRowPerPageName.Value = "MasterHotListRowPerPage";
                }
            }
            PlaceUpDownArrow();
        }
        protected void lsvHotList_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Sort")
                {
                    LinkButton lnkbutton = (LinkButton)e.CommandSource;
                    if (hdnSortColumn.Value == lnkbutton.ID)
                    {
                        if (hdnSortOrder.Value == "ASC")hdnSortOrder.Value = "DESC";
                        else hdnSortOrder.Value = "ASC";
                    }
                    else
                    {
                        hdnSortColumn.Value = lnkbutton.ID;
                        hdnSortOrder.Value = "ASC";
                    }
                    if (SortColumn.Text == string.Empty || SortColumn.Text != e.CommandArgument.ToString())
                        SortOrder.Text = "asc";
                    else
                        SortOrder.Text = SortOrder.Text == "asc" ? "desc" : "asc";
                    SortColumn.Text = e.CommandArgument.ToString();
                    odsHotList.SelectParameters["SortOrder"].DefaultValue = SortOrder.Text.ToString();
                }
            }
            catch
            {
            }
            int id;

            int.TryParse(e.CommandArgument.ToString(), out id);

            if (id > 0)
            {
                if (string.Equals(e.CommandName, "EditItem"))
                {
                    memberGroup = Facade.GetMemberGroupById(id);
                    txtGroupDesc.Text =MiscUtil .RemoveScript ( memberGroup.Description,string .Empty );
                    txtGroupName.Text =MiscUtil .RemoveScript ( memberGroup.Name,string .Empty );
                    _groupId = id;
                }
                else if (string.Equals(e.CommandName, "DeleteItem"))
                {
                    try
                    {
                        if (Facade.DeleteMemberGroupById(id))
                        {
                            BindList();
                            MiscUtil.ShowMessage(lblMessage, "Hot list has been deleted successfully.", false);
                            ClearText();
                        }
                    }
                    catch (ArgumentException ex)
                    {
                        MiscUtil.ShowMessage(lblMessage, ex.Message, true);
                    }
                    _groupId = 0;
                }
            }
        }

        #endregion

        #region Object Datasource Event

        protected void odsHotList_Load(object sender, EventArgs e)
        {
            if (isMy)
            {
                odsHotList.SelectParameters["memberId"].DefaultValue = _memberId.ToString();
            }
            else
            {
                odsHotList.SelectParameters["memberId"].DefaultValue = "0";
            }
            if (groupType > 0)
            {
                odsHotList.SelectParameters["groupType"].DefaultValue = groupType.ToString();
            }

            if (SortOrder.Text == string.Empty && SortColumn.Text == string.Empty)
            {
                SortOrder.Text = "asc";
                SortColumn.Text = "[M].[Name]";
            }

            odsHotList.SelectParameters["SortOrder"].DefaultValue = SortOrder.Text.ToString();
        }

        #endregion

        #endregion

    }
}