﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using System.Collections.Generic;
using ParserLibrary;
using System.IO;
using System.Data;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Web.UI.Helper;
using TPS360.Common.Shared;
using TPS360.BusinessFacade;
using System.Text.RegularExpressions;
namespace TPS360.Web.UI
{
   
    public partial class MemberSkillsEditor : BaseControl
    {
        private bool IsManager = false;
        #region Member Variables
        private static int _skillId = 0;
        MemberSkillMap _memberskillmap = new MemberSkillMap();
        private static int _memberId = 0;
        private static string _memberrole = string.Empty;
        string CurrentJobPostingSkillNames = "";
        private int JobId = 0;
        #endregion

        #region Properties
        private void GetJobId()
        {
            if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_JOB_ID ]))
            {
                JobId = Convert .ToInt32 ( Helper.Url.SecureUrl[UrlConstants.PARAM_JOB_ID ]);
            }
        }
        private void GetMemberId()
        {
            if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]))
            {
                hfMemberId.Value = Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID];
            }
            else   hfMemberId.Value = base.CurrentMember.Id.ToString();
            _memberId = Int32.Parse(hfMemberId.Value);
        }
        public string MemberRole
        {
            set { _memberrole = value; }
            get { return _memberrole; }
        }
        #endregion

        #region Page Event
        protected void Page_Load(object sender, EventArgs e)
        {  
            {
                LoadodsSkillsList();
                MemberManager Manager = new MemberManager();
                Manager = Facade.GetMemberManagerByMemberIdAndManagerId(_memberId, CurrentMember.Id);
                if (Manager != null || IsUserAdmin)
                    IsManager = true;
                else
                    IsManager = false;


                GetJobId();
                if (JobId > 0) CurrentJobPostingSkillNames = Facade.JobPosting_GetRequiredSkillNamesByJobPostingID(JobId);
                if (!IsPostBack)
                {
                    hdnSortColumnOrder.Value = "asc";
                    hdnSortColumnName.Value = "SkillId";
                    hdnSortColumn.Text = "btnSkill";
                    hdnSortOrder.Text = "ASC";
                    PrepareView();
                    lsvSkills.DataBind();
                }
                string pagesize = "";
                pagesize = (Request.Cookies["SkillsRowPerPage"] == null ? "" : Request.Cookies["SkillsRowPerPage"].Value);
                ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvSkills.FindControl("pagerControl");
                if (PagerControl != null)
                {
                    DataPager pager = (DataPager)PagerControl.FindControl("pager");
                    if (pager != null) pager.PageSize = Convert.ToInt32(pagesize == "" ? "20" : pagesize);
                }
            }
        }
        #endregion

        #region Methods
        private void PrepareEditView()
        {
            MemberSkillMap memberSkillMap = Facade.GetMemberSkillMapById(_skillId);
            if (memberSkillMap != null)
            {
                txtSkillName.Text =MiscUtil .RemoveScript ( Facade.GetSkillById(memberSkillMap.SkillId).Name ,string .Empty );
                ddlLastUsed.SelectedValue = memberSkillMap.LastUsed==DateTime.MinValue ? DateTime .Today .Year +"": memberSkillMap.LastUsed.Year + "";
                ddlYearsUsed.SelectedIndex = memberSkillMap.YearsOfExperience < 1 ? 0 : (memberSkillMap.YearsOfExperience > 10 ? 11 : memberSkillMap.YearsOfExperience);
            }
        }
        private void FillYearsUsed()
        {
            for (int i = 0; i <= 11; i++)
            {
                ListItem list = new ListItem();
                list.Value = i.ToString(); 
                list.Text = i == 0 ? "<1" : i == 11 ? ">10" : i + "";
                ddlYearsUsed.Items.Add(list);
            }
        }
        private void FillLastUsed()
        {
            for (int i = DateTime.Now.Year; i > DateTime.Now.Year - 20; i--)
            {
                ListItem list = new ListItem();
                list.Value = i.ToString ();
                list.Text = i + "";
                ddlLastUsed.Items.Add(list);
            }
        }
        private string HighLightSkillName(string text)
        {
            char[] delim = { ',' };
            string[] oriArr = CurrentJobPostingSkillNames.Trim().Split(delim, StringSplitOptions.RemoveEmptyEntries);
            string[] curArr = text.Split(',');
            string ret = string.Empty;
            if (oriArr.Length > 0 && curArr.Length > 0)
            {
                if (("," + CurrentJobPostingSkillNames + ",").Contains(text.Trim()))
                {
                    return "<span class=highlight>" + text + "</span>";
                }
                else return text;


            }
            else return text;

        }
        private void PlaceUpDownArrow()
        {
            try
            {
                bool alreadyAvailable = true ;
                LinkButton lnk = (LinkButton)lsvSkills.FindControl(hdnSortColumn.Text);
                System.Web.UI.HtmlControls.HtmlTableCell im = (System.Web.UI.HtmlControls.HtmlTableCell)lnk.Parent;
                im.EnableViewState = false;
                if (lnk.ID == "btnYearsUsed")
                {
                    im.Attributes.Add("class", (hdnSortOrder.Text  == "ASC" ? "Descending" : "Ascending"));
                }
                else
                    im.Attributes.Add("class", (hdnSortOrder.Text  == "ASC" ? "Ascending" : "Descending"));
            }
            catch
            {
            }

        }
        private void Clear()
        {
            txtSkillName.Text = "";
            ddlLastUsed.SelectedIndex = 0;
            ddlYearsUsed.SelectedIndex = 0;
        }
        private void PrepareView()
        {
            FillYearsUsed();
            FillLastUsed();
        }
        #endregion

        #region Button Events
        protected void btnSave_Click(object sender, EventArgs e)
        {

            int skillid = 0;
            rfvSkill.Enabled = true;
            if (MiscUtil .RemoveScript ( txtSkillName.Text) != string.Empty)
            {
                if (txtSkillName.Text.Trim() == hdnSelectedSkillText.Value && txtSkillName.Text != string.Empty)
                {
                    skillid = Convert.ToInt32(hdnSelectedSkill.Value);
                }
                else
                {
                    txtSkillName.Text = MiscUtil .RemoveScript (txtSkillName.Text.Trim(),string .Empty );
                    if (txtSkillName.Text.Trim().ToString() != string.Empty)
                    {
                        Skill newskill = new Skill();
                        newskill.Name =MiscUtil .RemoveScript ( txtSkillName.Text.Trim());
                        if (base.CurrentMember != null)
                        {
                            newskill.CreatorId = base.CurrentMember.Id;
                            newskill.UpdatorId = base.CurrentMember.Id;
                        }
                        Skill sk = Facade.AddSkill(newskill);
                       if(sk!=null)  skillid =sk.Id;
                    }

                }

                MemberSkillMap memberSkillMap = new MemberSkillMap();
                if (_skillId == 0)
                {
                    MemberSkillMap _memskill = Facade.GetMemberSkillMapByMemberIdAndSkillId(_memberId, skillid);
                    if (_memskill == null)
                    {
                        memberSkillMap.SkillId = skillid;
                        memberSkillMap.MemberId = _memberId;
                        memberSkillMap.YearsOfExperience = Convert.ToInt32(ddlYearsUsed.SelectedValue);
                        memberSkillMap.LastUsed = new DateTime(Convert.ToInt32(ddlLastUsed.SelectedValue), 01, 01);
                        if (base.CurrentMember != null)
                        {
                            memberSkillMap.CreatorId = base.CurrentMember.Id;
                            memberSkillMap.UpdatorId = base.CurrentMember.Id;
                        }
                        Facade.AddMemberSkillMap(memberSkillMap);
                        MiscUtil.ShowMessage(lblMessage, "Skill has been added successfully.", false);
                    }
                    else
                    {

                        memberSkillMap.Id = _memskill.Id;
                        memberSkillMap.SkillId = skillid;
                        memberSkillMap.MemberId = _memberId;
                        if (base.CurrentMember != null)
                        {
                            memberSkillMap.UpdatorId = base.CurrentMember.Id;
                        }
                        memberSkillMap.YearsOfExperience = Convert.ToInt32(ddlYearsUsed.SelectedValue);
                        memberSkillMap.LastUsed = new DateTime(Convert.ToInt32(ddlLastUsed.SelectedValue), 01, 01);
                        Facade.UpdateMemberSkillMap(memberSkillMap);
                        MiscUtil.ShowMessage(lblMessage, "Skill has been updated successfully.", false);
                    }
                }
                else
                {
                    memberSkillMap.Id = _skillId;
                    memberSkillMap.SkillId = skillid;
                    memberSkillMap.MemberId = _memberId;
                    if (base.CurrentMember != null)
                    {
                        memberSkillMap.UpdatorId = base.CurrentMember.Id;
                    }
                    memberSkillMap.YearsOfExperience = Convert.ToInt32(ddlYearsUsed.SelectedValue);
                    memberSkillMap.LastUsed = new DateTime(Convert.ToInt32(ddlLastUsed.SelectedValue), 01, 01);
                    Facade.UpdateMemberSkillMap(memberSkillMap);
                    MiscUtil.ShowMessage(lblMessage, "Skill has been updated successfully.", false);
                }
                Facade.UpdateUpdateDateByMemberId(_memberId);
                lsvSkills.DataBind();
                Clear();
                _skillId = 0;
            }
        }
        #endregion

        #region List View Events
        protected void lsvSkills_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                MemberSkillMap memberskill = ((ListViewDataItem)e.Item).DataItem as MemberSkillMap;
                Label txtSkillName = (Label)e.Item.FindControl("lblSkill");
                Label lblYearsUsed = (Label)e.Item.FindControl("lblYearsUsed");
                Label lblLastUsed = (Label)e.Item.FindControl("lblLastUsed");
                HiddenField hfSkillId = (HiddenField)e.Item.FindControl("hfSkillId");
                hfSkillId.Value = memberskill.Id.ToString(); 
                if(memberskill .SkillId >0) txtSkillName .Text =Facade .GetSkillById (memberskill .SkillId ).Name ;
                lblYearsUsed.Text = memberskill.YearsOfExperience==0? "<1" : (memberskill .YearsOfExperience >10 ? ">10" : memberskill .YearsOfExperience + "");
                lblLastUsed.Text = memberskill.LastUsed.Year == 1 ? "" : (memberskill.LastUsed.Year + "");
                ImageButton btnEdit = (ImageButton)e.Item.FindControl("btnEdit");
                ImageButton btnDelete = (ImageButton)e.Item.FindControl("btnDelete");
                btnDelete.OnClientClick = "return ConfirmDelete('skill')";
                btnDelete.Visible = IsManager;
                btnEdit.CommandArgument = btnDelete.CommandArgument = StringHelper.Convert(memberskill.Id);
                if (Page.Request.Url.AbsoluteUri.ToString().Contains("Overview"))
                {
                    System.Web.UI.HtmlControls.HtmlTableCell table = (System.Web.UI.HtmlControls.HtmlTableCell)lsvSkills.FindControl("thAction");
                    System.Web.UI.HtmlControls.HtmlTableCell tabledata = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item .FindControl("tdAction");
                    table.Visible = false;
                    tabledata.Visible = false;
                    System.Web.UI.HtmlControls.HtmlTableCell thCheck = (System.Web.UI.HtmlControls.HtmlTableCell)lsvSkills.FindControl("thCheck");
                    System.Web.UI.HtmlControls.HtmlTableCell tdCheck = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdCheck");
                    thCheck.Visible = false;
                    tdCheck.Visible = false;
                }
                txtSkillName.Text = HighLightSkillName(txtSkillName.Text);
            }

        }
        protected void btnRemove_Click(object sender, EventArgs e)
        {
            if (hdnSelectedIDS.Value.Trim() != "")
            {
                string ids = hdnSelectedIDS.Value;
                Facade.DeleteMemberSkillMapByIds(ids);
                lsvSkills.DataBind();
                MiscUtil.ShowMessage(lblMessage, "Skills successfully deleted.", false);
                hdnSelectedIDS.Value = "";
            }
            else MiscUtil.ShowMessage(lblMessage, "Please select at least one skill to delete.", false);

            
        }
        protected void lsvSkills_PreRender(object sender, EventArgs e)
        {
            btnRemove.Visible = true;
            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvSkills.FindControl("pagerControl");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)PagerControl.FindControl("pager");
                if (pager != null)
                {
                    DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                    if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
                }
                else btnRemove.Visible = false;
                HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
                if (hdnRowPerPageName != null) hdnRowPerPageName.Value = "SkillsRowPerPage";
            }
            else btnRemove.Visible = false;
            PlaceUpDownArrow();
            if (Page.Request.Url.AbsoluteUri.ToString().Contains("Overview"))
            {
                btnRemove.Visible = false;
                System.Web.UI.HtmlControls.HtmlTableCell   tdPager = (System.Web.UI.HtmlControls.HtmlTableCell  )lsvSkills.FindControl("tdPager");
                if (tdPager != null) tdPager.ColSpan = 3;
            }

            if (lsvSkills.Items.Count == 0)
            {
                lsvSkills.DataSource = null;
                lsvSkills.DataBind();
            }
        }

        protected void lsvSkills_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Sort")
                {
                    LinkButton lnkbutton = (LinkButton)e.CommandSource;
                    if (hdnSortColumn.Text == lnkbutton.ID)
                    {
                        if (hdnSortOrder.Text == "ASC") hdnSortOrder.Text = "DESC";
                        else hdnSortOrder.Text = "ASC";
                    }
                    else
                    {
                        hdnSortColumn.Text = lnkbutton.ID;
                        hdnSortOrder.Text = "ASC";
                    }
                    if (hdnSortColumnName.Value == string.Empty || hdnSortColumnName.Value != e.CommandArgument.ToString())
                    {
                        hdnSortColumnOrder.Value = "asc";

                    }
                    else
                    {
                        hdnSortColumnOrder.Value  = hdnSortColumnOrder.Value  == "asc" ? "desc" : "asc";

                    }
                    hdnSortColumnName.Value  = e.CommandArgument.ToString();
                    odsSkillsList.SelectParameters["SortOrder"].DefaultValue = hdnSortColumnOrder.Value .ToString();
                }
            }
            catch
            {
            }
            int id;

            int.TryParse(e.CommandArgument.ToString(), out id);

            if (id > 0)
            {
                if (string.Equals(e.CommandName, "EditItem"))
                {
                    _skillId  = id;
                    PrepareEditView();
                    txtSkillName.Focus();
                    Page.ClientScript.RegisterStartupScript(typeof(Page), "MoveTop", "<script>self.moveTo(0,-300); </script>");
                }
                else if (string.Equals(e.CommandName, "DeleteItem"))
                {
                    try
                    {
                        if (Facade.DeleteMemberSkillMapById (id))
                        {
                            lsvSkills.DataBind();
                            _skillId  = 0;
                            MiscUtil.ShowMessage(lblMessage, "Skill has been successfully deleted.", false);
                        }
                    }
                    catch (ArgumentException ex)
                    {
                        MiscUtil.ShowMessage(lblMessage, ex.Message, true);
                    }
                }
            }
        }
        #endregion

        #region ObjectDataSource Event

        private void LoadodsSkillsList()
        {
            GetMemberId();
            if (_memberId > 0)
            {
                if(odsSkillsList.SelectParameters["memberId"] ==null)
                odsSkillsList.SelectParameters.Add("memberId", _memberId.ToString());

                if (hdnSortColumnOrder.Value == string.Empty && hdnSortColumnName.Value == string.Empty)
                {
                    hdnSortColumnOrder.Value = "asc";
                    hdnSortColumnName.Value = "SkillId";

                }
                if (odsSkillsList.SelectParameters["memberId"] == null)
                    odsSkillsList.SelectParameters.Add("SortOrder", hdnSortColumnOrder.Value);
            }
           
        }

        protected void odsSkillsList_Init(object sender, EventArgs e)
        {
           
        }

        #endregion
}
}