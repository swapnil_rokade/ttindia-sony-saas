﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using TPS360.Web.UI;
using TPS360.Common.Shared;
using TPS360.Common.BusinessEntities;
using TPS360.Web.UI.Helper;
using TPS360.Common.Helper;
using System.Collections.Generic;
using System.IO;
using System.Net;
using iTextSharp.text.pdf.parser;
using System.Drawing;
using ExpertPdf.HtmlToPdf;
using iTextSharp.text.pdf;
using iTextSharp.text;
using iTextSharp.text.pdf.parser;
using System.Text;
//using RasterEdge.Imaging.Basic;
//using RasterEdge.XDoc.PDF;

public partial class Controls_ShowOffer : BaseControl
{

    #region Member Variables
    bool ShowAction = false;
    public int HeaderAdd = 0;
    ArrayList _permittedMenuIdList;
    public string RequisitionType = "Master";
    public static bool IsAccss = false;
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        string pagesize = "";
        if (hdnDoPostPack.Value == "1")
        {
            lsvOfferList.DataBind();
            hdnDoPostPack.Value = "0";
            return;
        }
        if (!IsPostBack)
        {



            //string message = Helper.Url.SecureUrl[UrlConstants.PARAM_MSG];
            //if (!StringHelper.IsBlank(message))
            //{
            //    MiscUtil.ShowMessage(lblMessage, message, false);
            //}
            //hdnSortColumn.Value = "btnPostedDate";
            //hdnSortOrder.Value = "DESC";
            //PlaceUpDownArrow();
        }

        //ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvJobPosting.FindControl("pagerControl");
        //if (PagerControl != null)
        //{
        //    DataPager pager = (DataPager)PagerControl.FindControl("pager");
        //    if (pager != null)
        //    {
        //        pager.PageSize = Convert.ToInt32(pagesize == "" ? "20" : pagesize);
        //    }
        //}
        SetDataSourceParameters();

        ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvOfferList.FindControl("pagerControl");
        if (PagerControl != null)
        {
            DataPager pager = (DataPager)PagerControl.FindControl("pager");
            if (pager != null)
            {
                pager.PageSize = Convert.ToInt32(pagesize == "" ? "150" : pagesize);
            }
        }

        txtSortColumn.Text = "ApplicantName";
        txtSortOrder.Text = "DESC";
        RegisterControls();
        PlaceUpDownArrow();

    }
    private void BindList()
    {
        this.lsvOfferList.DataBind();

    }
    void RegisterControls()
    {

        foreach (var item in lsvOfferList.Items)
        {

            LinkButton lnkFull = item.FindControl("hlkReqApprove") as LinkButton;
            ScriptManager scmanager = ScriptManager.GetCurrent(this.Page);
            scmanager.RegisterPostBackControl(lnkFull);


        }

    }
    protected void lsvOfferList_ItemCommand(object sender, ListViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Sort")
            {
                LinkButton lnkbutton = (LinkButton)e.CommandSource;
                if (txtSortColumn.Text == lnkbutton.ID)
                {
                    if (txtSortOrder.Text == "ASC") txtSortOrder.Text = "DESC";
                    else txtSortOrder.Text = "ASC";
                }
                else
                {
                    txtSortColumn.Text = lnkbutton.ID;
                    txtSortOrder.Text = "ASC";
                }
                if (SortColumn.Text == string.Empty || SortColumn.Text != e.CommandArgument.ToString())
                    SortOrder.Text = "asc";
                else SortOrder.Text = SortOrder.Text == "asc" ? "desc" : "asc";
                SortColumn.Text = e.CommandArgument.ToString();
                odsOfferList.SelectParameters["SortColumn"].DefaultValue = SortColumn.Text.ToString();
                odsOfferList.SelectParameters["SortOrder"].DefaultValue = SortOrder.Text.ToString();
            }
            if (string.Equals(e.CommandName, "GenerateOfferLetter"))
            {
                try
                {
                       string Filename = "";
                        LinkButton lnkbutton = (LinkButton)e.CommandSource;
                        string[] commandArgValues = lnkbutton.CommandArgument.Split(';');
                        string CandidateId = commandArgValues[0];
                        string MemberHiringId = commandArgValues[1];
                        GenerateOfferLetter(Filename, Convert.ToInt32(MemberHiringId), Convert.ToInt32(CandidateId));
                        BindList();
                }
                catch (System.Threading.ThreadAbortException lException)
                {
                    Response.Write(lException.Message);
                    throw lException;
                }
            }
            else if (e.CommandName.Equals("DownLoad"))
            {
                try
                {
                    string savedPdfPath = Server.MapPath("~/bin/Templates/Download File/");
                    LinkButton lnkbutton = (LinkButton)e.CommandSource;
                    string[] commandArgValues = lnkbutton.CommandArgument.Split(';');
                    string CandidateId = commandArgValues[0];
                    string MemberHiringId = commandArgValues[1];
                    string templateFilePath = "";
                    StringBuilder text = new StringBuilder();

                    /*******start*******Following Code is for PDF Generation*********Suraj adsule**********20160822*/

                    OfferGeneration CandDetail = Facade.GetCandidateDetail_UsingMemberHiringId(Convert.ToInt32(MemberHiringId), 1);
                    if (CandDetail.DemandType.ToLower() == "permanent")
                    {
                        OfferGeneration CandOfferDetail = Facade.GetCandidateDetail_UsingMemberHiringId(Convert.ToInt32(MemberHiringId), 2);
                        
                        #region For Position SM1 and above
                        templateFilePath = Server.MapPath(CandOfferDetail.FilePath);
                        string candidateFullName = CandOfferDetail.FirstName + " " + CandOfferDetail.MiddleName + " " + CandOfferDetail.LastName;
                        savedPdfPath = savedPdfPath + "Offer_LetterTemplate-" + candidateFullName + ".pdf";
                        FileStream fs = new FileStream(savedPdfPath, FileMode.Create, FileAccess.Write);
                        //FileStream fs = new FileStream("PDFGeneratorSample.pdf", FileMode.Create, FileAccess.Write, FileShare.None);
                        //Added by Kanchan Yeware
                        Document doc = new Document(PageSize.A4, 36, 36, 36, 120);
                        //Document doc = new Document(PageSize.A4, 20f, 20f, 20f, 20f);
                        PdfWriter writer = PdfWriter.GetInstance(doc, fs);

                        //read pdf
                        using (PdfReader reader = new PdfReader(templateFilePath))
                        {
                            for (int i = 1; i <= reader.NumberOfPages; i++)
                            {
                                text.Append(PdfTextExtractor.GetTextFromPage(reader, i));
                            }
                        }

                        //replace with actual data
                        if (CandOfferDetail != null)
                        {
                            text.Replace("[%Candidate_Name%]", candidateFullName);
                            text.Replace("[%Candidate_Address%]", CandOfferDetail.PermanentAddressLine1 + "\n " + CandOfferDetail.PermanentAddressLine2 + "\n" +
                                CandOfferDetail.PermanentCity + "\n " + CandOfferDetail.PermanentState + "\n" + CandOfferDetail.PermanentZip);
                            text.Replace("[%Candidate_fName%]", Convert.ToString(CandOfferDetail.FirstName));
                            text.Replace("[%Designation%]", Convert.ToString(CandOfferDetail.CurrentPosition));
                            text.Replace("[%Grade%]", Convert.ToString(CandOfferDetail.Grade));
                            text.Replace("[%Date_of_Joining%]", Convert.ToString(CandOfferDetail.DateOfJoining));

                        }
                        else
                        {
                            text.Replace("[%Candidate_Name%]", string.Empty);
                            text.Replace("[%Candidate_Address%]", string.Empty);
                            text.Replace("[%Candidate_fName%]", string.Empty);
                            text.Replace("[%Designation%]", string.Empty);
                            text.Replace("[%Grade%]", string.Empty);
                            text.Replace("[%Date_of_Joining%]", string.Empty);
                            text.Replace("[%CTC%]", string.Empty);
                        }
                        //new code 20160909
                        string[] TemplateSplitWithTitle = text.ToString().Split(new string[] { "[%Title%]" }, StringSplitOptions.None);

                        var boldFont = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 12);

                        //for Annexure A

                        Paragraph annexureA = new Paragraph();  
                       // annexureA.Alignment = Element.ALIGN_CENTER;
                        annexureA.Font = boldFont;
                        annexureA.Add("Annexure A");

                        string[] TemplateSplitForAnnexA = TemplateSplitWithTitle[1].Split(new string[] { "[%Bold%]" }, StringSplitOptions.None);
                        Paragraph AnnexAParagraph = new Paragraph();
                        AnnexAParagraph.Add(TemplateSplitForAnnexA[0]);
                        Paragraph Compensation = new Paragraph();  //Paragraph
                        Compensation.Alignment = Element.ALIGN_CENTER;
                        Compensation.Font = boldFont;
                        Compensation.Add("Compensation Structure");
                        AnnexAParagraph.Add(new Paragraph(Compensation));
                        string[] Template_CompensationTableSplit = TemplateSplitForAnnexA[1].ToString().Split(new string[] { "[%Table%]" }, StringSplitOptions.None);
                        decimal ctc = 0;
                        PdfPTable compensationTable = CompensationTable(CandOfferDetail.Basic, CandOfferDetail.HouseRent,
                            CandOfferDetail.Conveyance, CandOfferDetail.SpecialAllowance, CandOfferDetail.CompensationPlan,
                            CandOfferDetail.ProvidentFund, CandOfferDetail.VariablePay, out ctc);

                        text.Replace("[%CTC%]", Convert.ToString(ctc));
                        AnnexAParagraph.Add(compensationTable);
                        AnnexAParagraph.Add(Template_CompensationTableSplit[1]);
                        //end annexure A

                        //For Split 1
                        string[] TemplateSplitForPoints = TemplateSplitWithTitle[0].Split(new string[] { "[%Bold%]" }, StringSplitOptions.None);
                        Paragraph p;
                        p = new Paragraph();
                        string strPointsData = string.Empty;
                        
                        strPointsData = TemplateSplitForPoints[0].ToString().Replace("[%CTC%]", Convert.ToString(ctc));
                        p.Add(strPointsData);
                        p.Add(new Phrase("1. Probation", boldFont));
                        p.Add(TemplateSplitForPoints[1]);
                        p.Add(new Phrase("2. Notice Period", boldFont));
                        p.Add(TemplateSplitForPoints[2]);
                        p.Add(new Phrase("3. Duties", boldFont));
                        p.Add(TemplateSplitForPoints[3]);
                        p.Add(new Phrase("4. Training", boldFont));
                        p.Add(TemplateSplitForPoints[4]);
                        p.Add(new Phrase("5. Confidentiality and Fidelity", boldFont));
                        p.Add(TemplateSplitForPoints[5]);
                        //end split 1

                        //for Annexure B
                        Paragraph annexureB = new Paragraph();  //Paragraph
                        annexureB.Alignment = Element.ALIGN_CENTER;
                        annexureB.Font = boldFont;
                        annexureB.Add("Annexure B");

                        string[] TemplateSplitForAnnexB = TemplateSplitWithTitle[2].Split(new string[] { "[%Bold%]" }, StringSplitOptions.None);
                        Paragraph AnnexBParagraph = new Paragraph();
                        AnnexBParagraph.Add(new Phrase("Flexible Compensation Plan:", boldFont));
                        string[] Template_AnnexBSplit = TemplateSplitForAnnexB[1].ToString().Split(new string[] { "[%Table%]" }, StringSplitOptions.None);
                        AnnexBParagraph.Add(Template_AnnexBSplit[0]);

                        PdfPTable flexiTable= null;

                        AnnexBParagraph.Add(flexiTable);
                        AnnexBParagraph.Add(Template_AnnexBSplit[1]);
                        //end annexure B


                        //for Annexure C
                        Paragraph annexureC = new Paragraph();  //Paragraph
                        annexureC.Alignment = Element.ALIGN_CENTER;
                        annexureC.Font = boldFont;
                        annexureC.Add("Annexure C");

                        string[] TemplateSplitForAnnexC = TemplateSplitWithTitle[3].Split(new string[] { "[%Bold%]" }, StringSplitOptions.None);
                        Paragraph AnnexCParagraph = new Paragraph();
                        AnnexCParagraph.Add(new Phrase("Variable Pay:", boldFont));
                        string[] Template_AnnexCSplit = TemplateSplitForAnnexC[1].ToString().Split(new string[] { "[%Table%]" }, StringSplitOptions.None);
                        AnnexCParagraph.Add(Template_AnnexCSplit[0]);
                        PdfPTable varPayTable = VariablePayTable();
                        AnnexCParagraph.Add(varPayTable);
                        AnnexCParagraph.Add(Template_AnnexCSplit[1]);
                        //end annexure C

                        //for Annexure D
                        Paragraph annexureD = new Paragraph();  //Paragraph
                        annexureD.Alignment = Element.ALIGN_CENTER;
                        annexureD.Font = boldFont;
                        annexureD.Add("Annexure D");

                        string[] TemplateSplitForAnnexD = TemplateSplitWithTitle[4].Split(new string[] { "[%Bold%]" }, StringSplitOptions.None);
                        Paragraph AnnexDParagraph = new Paragraph();
                        AnnexDParagraph.Add(Template_AnnexCSplit[0]);
                        AnnexDParagraph.Add(new Phrase("List of Documents to be submitted to HR:", boldFont));
                        AnnexDParagraph.Add(Template_AnnexCSplit[1]);
                        //end annexure D

                        //for Annexure E
                        Paragraph annexureE = new Paragraph();  //Paragraph
                        annexureE.Alignment = Element.ALIGN_CENTER;
                        annexureE.Font = boldFont;
                        annexureE.Add("Annexure E");

                        string[] TemplateSplitForAnnexE = TemplateSplitWithTitle[5].Split(new string[] { "[%Bold%]" }, StringSplitOptions.None);
                        Paragraph AnnexEParagraph = new Paragraph();
                        AnnexEParagraph.Add(TemplateSplitForAnnexE[0]);
                        AnnexEParagraph.Add(new Phrase("Note:", boldFont));
                        AnnexEParagraph.Add(TemplateSplitForAnnexE[1]);
                        //end annexure E


                        PDFFooter pdfFooter = new PDFFooter();
                        var page = new PDFFooter();
                        writer.PageEvent = page;
                        doc.Open();
                        doc.Add(new Paragraph(p));
                        doc.Add(annexureA);
                        doc.Add(new Paragraph(AnnexAParagraph));
                        doc.Add(new Paragraph(annexureB));
                        doc.Add(new Paragraph(AnnexBParagraph));
                        doc.Add(new Paragraph(annexureC));
                        doc.Add(new Paragraph(AnnexCParagraph));
                        doc.Add(new Paragraph(annexureD));
                        doc.Add(new Paragraph(AnnexDParagraph));
                        doc.Add(new Paragraph(annexureE));
                        doc.Add(new Paragraph(AnnexEParagraph));
                        doc.Close();
                        #endregion
                    }
                    if (CandDetail.DemandType.ToLower() == "contract")
                    {

                        #region ContractHire
                        var boldFont = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 12);
                        var font = FontFactory.GetFont("Calibri", 11);
                        //consultant info
                        Company vendorDetails = Facade.GetVendorByCandidateId(Convert.ToInt32(CandidateId));
                        savedPdfPath = savedPdfPath + "Offer_LetterTemplate-" + vendorDetails.CompanyName + ".pdf";
                        FileStream fs = new FileStream(savedPdfPath, FileMode.Create, FileAccess.Write);
                        //FileStream fs = new FileStream("PDFGeneratorSample.pdf", FileMode.Create, FileAccess.Write, FileShare.None);
                        Document doc = new Document(PageSize.A4, 36, 36, 36, 120);
                        PdfWriter writer = PdfWriter.GetInstance(doc, fs);

                        //pdf path
                        GenericLookup contractDetail = Facade.GetGenericLookupByType(LookupType.ContractTemplatePath);
                        templateFilePath = Server.MapPath(contractDetail.Name); // @"D:\Suraj\Documents\documents\SISCPL-WO Template.pdf";

                        //read pdf
                        using (PdfReader reader = new PdfReader(templateFilePath))
                        {
                            for (int i = 1; i <= reader.NumberOfPages; i++)
                            {
                                text.Append(PdfTextExtractor.GetTextFromPage(reader, i));

                            }
                        }

                        //replace data with actual values
                        if (vendorDetails != null)
                        {
                            text.Replace("[%Consultant_Name%]", vendorDetails.CompanyName);
                            text.Replace("[%Vendor_Name%]", vendorDetails.CompanyName);
                            text.Replace("[%Start_Date%]", Convert.ToString(vendorDetails.ContractStartDate));
                            text.Replace("[%End_Date%]", Convert.ToString(vendorDetails.ContractEndDate));
                            text.Replace("[%Charges%]", Convert.ToString(vendorDetails.ServiceFee));
                            text.Replace("[%Address%]", vendorDetails.Address1 + "\n" + vendorDetails.Address2 + "\n" + vendorDetails.City + "\n" + vendorDetails.Zip);
                            text.Replace("[%Contact_Number%]", Convert.ToString(vendorDetails.OfficePhone));
                        }
                        else
                        {
                            text.Replace("[%Vendor_Name%]", string.Empty);
                            text.Replace("[%Consultant_Name%]", string.Empty);
                            text.Replace("[%Start_Date%]", string.Empty);
                            text.Replace("[%End_Date%]", string.Empty);
                            text.Replace("[%Charges%]", string.Empty);
                            text.Replace("[%Address%]", string.Empty);
                            text.Replace("[%Contact_Number%]", string.Empty);
                        }

                        #region table
                        //split data according 
                        string[] templateSplitData = text.ToString().Split(new string[] { "[%Bold%]" }, StringSplitOptions.None);
                        string[] tableSplitData = templateSplitData[2].ToString().Split(new string[] { "[%Table%]" }, StringSplitOptions.None);

                        //for creating and adding data into pdf
                        PdfPTable table = new PdfPTable(4);
                        table.TotalWidth = 400f;
                        //fix the absolute width of the table
                        table.LockedWidth = true;

                        //relative col widths in proportions - 1/3 and 2/3
                        float[] widths = new float[] { 6f, 2f, 2f, 2f };
                        table.SetWidths(widths);
                        table.HorizontalAlignment = 0;
                        //leave a gap before and after the table
                        table.SpacingBefore = 20f;
                        table.SpacingAfter = 25f;

                        table.AddCell("Name of Consultant");
                        table.AddCell("Start Date");
                        table.AddCell("End Date");
                        table.AddCell("Service Fee");


                        if (vendorDetails != null)
                        {
                            table.AddCell(vendorDetails.CompanyName);
                            table.AddCell(Convert.ToString(vendorDetails.ContractStartDate));
                            table.AddCell(Convert.ToString(vendorDetails.ContractEndDate));
                            table.AddCell(Convert.ToString(vendorDetails.ServiceFee));
                        }
                        else
                        {
                            PdfPCell cell = new PdfPCell(new Phrase("No Details found"));
                            cell.Colspan = 4;
                            cell.HorizontalAlignment = 1;
                            table.AddCell(cell);
                        }
                        #endregion
                        #region documentWrite
                        //var page = new PDFContractFooter();
                        //writer.PageEvent = page;
                        doc.Open();
                        doc.Add(new Paragraph(templateSplitData[0].ToString(), font));
                        doc.Add(new Paragraph("SUB: ENGAGEMENT OF PROFESSIONAL/ CONSULTANCY SERVICES", boldFont));
                        doc.Add(new Paragraph(templateSplitData[1].ToString(), font));
                        doc.Add(new Paragraph("KIND ATTENTION:", boldFont));
                        doc.Add(new Paragraph(tableSplitData[0].ToString(), font));
                        doc.Add(table);
                        doc.Add(new Paragraph(tableSplitData[1].ToString(), font));
                        doc.Add(new Paragraph("Terms & Conditions:", boldFont));
                        doc.Add(new Paragraph(templateSplitData[3].ToString()));
                        doc.Add(new Paragraph("1. Location:", boldFont));
                        doc.Add(new Paragraph(templateSplitData[4].ToString()));
                        doc.Add(new Paragraph("2. Termination:", boldFont));
                        doc.Add(new Paragraph(templateSplitData[5].ToString()));
                        doc.Add(new Paragraph("3. Travel, Accommodation & Living expenses:", boldFont));
                        doc.Add(new Paragraph(templateSplitData[6].ToString()));
                        doc.Add(new Paragraph("4. Calendar:", boldFont));
                        doc.Add(new Paragraph(templateSplitData[7].ToString()));
                        doc.Add(new Paragraph("5. Billing:", boldFont));
                        doc.Add(new Paragraph(templateSplitData[8].ToString()));
                        doc.Add(new Paragraph("6. Timesheet:", boldFont));
                        doc.Add(new Paragraph(templateSplitData[9].ToString()));
                        doc.Add(new Paragraph("7. Taxes:", boldFont));
                        doc.Add(new Paragraph(templateSplitData[10].ToString()));
                        doc.Add(new Paragraph("8. Non-Disclosure Agreement:", boldFont));
                        doc.Add(new Paragraph(templateSplitData[11].ToString()));
                        doc.Add(new Paragraph("9. Background Verification:", boldFont));
                        doc.Add(new Paragraph(templateSplitData[12].ToString()));
                        doc.Add(new Paragraph("10. Trial Period:", boldFont));
                        doc.Add(new Paragraph(templateSplitData[13].ToString()));
                        doc.Add(new Paragraph("11. Shift Allowance:", boldFont));
                        doc.Add(new Paragraph(templateSplitData[14].ToString()));
                        doc.Add(new Paragraph("Yours Sincerely,", boldFont));
                        doc.Add(new Paragraph(templateSplitData[15].ToString()));
                        doc.Add(new Paragraph("For Sony India Software Centre Pvt. Ltd.,", boldFont));
                        doc.Add(new Paragraph(templateSplitData[16].ToString()));
                        doc.Add(new Paragraph("Authorized Signatory", boldFont));
                        doc.Add(new Paragraph(templateSplitData[17].ToString()));
                        doc.Add(new Paragraph("Accepted", boldFont));
                        doc.Add(new Paragraph(templateSplitData[18].ToString()));
                        doc.Add(new Paragraph("For", boldFont));
                        doc.Add(new Paragraph(templateSplitData[19].ToString()));
                        doc.Add(new Paragraph("Authorized Signatory", boldFont));
                        doc.Add(new Paragraph(templateSplitData[20].ToString()));
                        doc.Close();
                        #endregion
                        #endregion
                    }
                    #region Download PDF
                    FileStream sourceFile = new FileStream(savedPdfPath, FileMode.Open);
                    long FileSize; FileSize = sourceFile.Length;
                    byte[] getContent = new byte[(int)FileSize];
                    sourceFile.Read(getContent, 0, (int)sourceFile.Length);
                    sourceFile.Close();

                    
                    System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
                    response.Clear();
                    response.Cache.SetExpires(DateTime.UtcNow.AddMinutes(-1));
                    response.Cache.SetCacheability(HttpCacheability.NoCache);
                    response.Cache.SetNoStore();
                    response.AddHeader("Content-Type", "binary/octet-stream");
                    response.AddHeader("Content-Disposition", "attachment; filename=OfferLetter-" + DateTime.Now.ToString("yyyy-MM-dd") + ".pdf; size=" + getContent.Length.ToString());
                    response.Flush();
                    response.BinaryWrite(getContent);
                    response.Flush();
                    response.End();
                    #endregion

                    /*******End*******Following Code is for PDF Generation*********Suraj adsule**********20160822*/
                }
                catch (System.Threading.ThreadAbortException lException)
                {
                    Response.Write(lException.Message);
                    throw lException;
                }
            }
        }

        catch (Exception ess)
        {
        }
    }
    public string GenerateOfferLetter(string Filename, int MemberHiringId, int CandidateId)
    {
        MemberHiringDetails details = null;
        string fullpath = "";

        OfferGeneration CandOfferDetail = Facade.GetCandidateDetail_UsingMemberHiringId(Convert.ToInt32(MemberHiringId), 2);

        StringBuilder reqReport = new StringBuilder();
        if (details == null)
        {
            #region Offerletter
            Member candidatedetails = new Member();
            candidatedetails = Facade.GetMemberById(CandidateId);
            if (CandOfferDetail.CurrentPosition != null)
            {
                Filename = "Offer_Letter-" + candidatedetails.FirstName.ToString().Trim() + "-" + CandOfferDetail.CurrentPosition.ToString().Trim() + ".pdf";
            }
            else
            {
                Filename = "Offer_Letter-" + candidatedetails.FirstName.ToString().Trim() + ".pdf";
            }
            if (Filename.Contains("amp;_"))
                Filename = Filename.Replace("amp;_", "_");

            int jobPostingid = CandOfferDetail.JobPostingId; //CurrentJobPostingId;
            MemberHiringDetails OfferDetailsletter = new MemberHiringDetails();
            string OfferLetterImagePath = "";// Facade.OfferLetterImagePath_ByJobPostingID(jobPostingid);
            //string OfferLetterImagePath2 = "";
            StringBuilder FirstPageHeader = new StringBuilder();
            StringBuilder FirstPageHeader1 = new StringBuilder();
            StringBuilder SecondPageHeader = new StringBuilder();
            StringBuilder SecondPageFooter = new StringBuilder();

            FirstPageHeader.Append("    <tr style='height:180px'>");
            FirstPageHeader.Append("        <td ></td>");
            FirstPageHeader.Append("    </tr>");


            SecondPageHeader.Append("    <tr style='height:300px'>");
            SecondPageHeader.Append("        <td ><br></td>");
            SecondPageHeader.Append("    </tr>");

            SecondPageFooter.Append("    <tr style='height:180px'>");
            SecondPageFooter.Append("        <td ><br></td>");
            SecondPageFooter.Append("    </tr>");

            double AnnualTotal;
            AnnualTotal = Convert.ToDouble(CandOfferDetail.OfferedSalary);
            string AnnualTotaltext = string.Empty;
            int no = Convert.ToInt32(AnnualTotal);
            string strWords = "";
            string strWordsonly = "";
            int MemberId = CandidateId;
            string[] Ones = { "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Ninteen" };

            string[] Tens = { "Ten", "Twenty", "Thirty", "Fourty", "Fift", "Sixty", "Seventy", "Eighty", "Ninty" };
            //no = 1200010;
            strWords = strWords + NumberToText(no);
            strWordsonly = " Only";
            strWords = strWords + strWordsonly;
            AnnualTotaltext = strWords;
            String Offerdate = CandOfferDetail.DateOfOffered.ToLongDateString();
            String Joindate = CandOfferDetail.DateOfJoining.ToLongDateString();
            string[] Offerdates = Offerdate.Split(',');
            string[] Joindates = Joindate.Split(',');

            string grade = CandOfferDetail.Grade;
            if (grade == null)
            {
                grade = "";
            }
            else
            {
                grade = grade + " Grade ";
            }

            Offerdate = Offerdates[1];
            Joindate = Joindates[1];
            string EmpType = string.Empty;

            string OfferPrintDetails = OfferDetailsletter.Id + " || " + CurrentMember.FirstName + " " + CurrentMember.LastName + " || " + System.DateTime.Now.ToString();
            OfferLetterImagePath = "Letterhead3";
            //OfferLetterImagePath2 = "Letterhead2";
            string OfferLetterImagePathpk = "Letterhead4";
            string SignImage = "SignImage.jpg";

            //OfferLetterImagePath = "Chakan";
            reqReport.Append("<table style='font-family :Verdana ;font-size:16.5pt; width:100%;  text-align:justify' >");
            //reqReport.Append("<table style='font-family :Verdana ;font-size:16.5pt; width:100%;  text-align:justify'  background=" + UrlConstants.ApplicationBaseUrl + "Images/SONY/" + OfferLetterImagePath + ".png>");
            // reqReport.Append("<table style='font-family :Verdana ;font-size:16.0pt; width:200%;'>");

            string gender;
            MemberDetail md = Facade.GetMemberDetailByMemberId(CandidateId);
            if (md.GenderLookupId == 33)
            {
                gender = "Ms.";
            }
            else
            {
                gender = "Mr.";
            }


            #region Page Number
            //ColumnText ct1 = new ColumnText(cb);
            //cb.BeginText();
            //cb.SetFontAndSize(baseFont, 8);
            //cb.SetTextMatrix(35, 15);

            //cb.ShowText(text);
            //cb.EndText();
            //cb.AddTemplate(template, document.LeftMargin, pageSize.GetBottom(document.BottomMargin));
            #endregion
            #region Confidential
            //cb.BeginText();
            //cb.SetFontAndSize(baseFont, 8);
            //cb.SetTextMatrix(500, 15);//470, pageSize.GetBottom(document.BottomMargin)
            //cb.ShowText("Confidential");
            #endregion
            #region Footer text
            var font1 = FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.ITALIC);
            Paragraph footerPara = new Paragraph();
            footerPara.Add("Please maintain strict confidentially of the terms and conditions " +
            " of your employment. You are advised that the Management takes a very serious view of such disclosure " +
            " and you will be liable for disciplinary action for breach of this condition of service.");
            footerPara.Alignment = Element.ALIGN_CENTER;
            footerPara.Font = font1;
            #endregion

            #region 1
            reqReport.Append("<table style='font-family :Verdana ;font-size:16.5pt; width:100%;  text-align:justify'  background=" + UrlConstants.ApplicationBaseUrl + "Images/LetterHeads/" + OfferLetterImagePath + ".png>");
            //reqReport.Append("<table style='font-family :Verdana ;font-size:16.5pt; width:100%;  text-align:justify'>");

            reqReport.Append("<div style='width:100%'>");
            //reqReport.Append("    <tr><td >");
            //reqReport.Append("<img src='" + UrlConstants.ApplicationBaseUrl + "Images/LetterHeads/" + OfferLetterImagePath + "' visible='true' style='width: 1080px;height: 1360px;  '/>");
            //reqReport.Append("    </td></tr>");
            reqReport.Append(FirstPageHeader);

            reqReport.Append("</div>");

            FirstPageHeader1.Append("    <tr style='height:66px'>"); //70
            FirstPageHeader1.Append("        <td ></td>");
            FirstPageHeader1.Append("    </tr>");

            reqReport.Append(FirstPageHeader1);

            reqReport.Append("    <tr>");
            reqReport.Append("        <td><b>Date : " + DateTime.Now.ToString("dd/MM/yyyy") + "</b></p></td>");
            reqReport.Append("    </tr>");


            if (CandOfferDetail.PermanentAddressLine2 != string.Empty)
            {
                reqReport.Append("    <tr>");
                reqReport.Append("        <td><b>" + gender + " " + CandOfferDetail.FirstName + " " + CandOfferDetail.MiddleName + " " + CandOfferDetail.LastName + " </b><br>" + CandOfferDetail.PermanentAddressLine1 + "," + CandOfferDetail.PermanentAddressLine2 + "<br>" + CandOfferDetail.PermanentCity + "<br> pin code: " + CandOfferDetail.PermanentZip + "<br> </p> </td>");
                reqReport.Append("    </tr>");
            }
            else
            {
                reqReport.Append("    <tr>");
                reqReport.Append("        <td><b>" + gender + " " + CandOfferDetail.FirstName + " " + CandOfferDetail.MiddleName + " " + CandOfferDetail.LastName + " </b><br>" + CandOfferDetail.PermanentAddressLine1 + "<br>" + CandOfferDetail.PermanentCity + "<br> pin code: " + CandOfferDetail.PermanentZip + "<br> </p> </td>");
                reqReport.Append("    </tr>");
            }

            reqReport.Append("    <tr style='height:16px'>");//20
            reqReport.Append("       <td ><br></td>");
            reqReport.Append("    </tr>");

            reqReport.Append("    <tr>");
            //reqReport.Append("        <td><b>Dear " + CandOfferDetail.FirstName + ",</b></p></td>");
            reqReport.Append("        <td><b>Dear " + CandOfferDetail.FirstName + " " + CandOfferDetail.MiddleName + " " + CandOfferDetail.LastName + ",</b></p></td>");
            reqReport.Append("    </tr>");

            reqReport.Append("    <tr>");
            reqReport.Append("        <td>We are pleased to offer you an employment with us as " + CandOfferDetail.CurrentPosition.ToString() + " in " + grade + " ");
            //reqReport.Append(" as per the terms given below. You are required to join us latest by " + CandOfferDetail.DateOfJoining.ToString("MMM/dd/yyyy") + " </p></td>");
            reqReport.Append(" as per the terms given below. You are required to join us latest by " + CandOfferDetail.DateOfJoining.ToString("dd/MM/yyyy") + ". </p></td>");
            reqReport.Append("    </tr>");

            reqReport.Append("    <tr>");
            reqReport.Append("        <td>Your Total Annual Cost to Company will be ");
            reqReport.Append("<b>Rs." + AnnualTotal.ToString("#,#.##", System.Globalization.CultureInfo.CreateSpecificCulture("hi-IN")) + "/- ");
            reqReport.Append("(Rupees " + AnnualTotaltext + ")</b> as detailed in Annexure A. Appointment letter with terms and conditions of employment will be issued at the time of joining the services of the company. ");
            reqReport.Append("    </p></tr>");

            reqReport.Append("    <tr>");
            reqReport.Append("        <td><b> 1.	Probation </b></p></td>");
            reqReport.Append("    </tr>");
            reqReport.Append("    <tr>");
            reqReport.Append("        <td>You will be on probationary period for six months from your date of joining.  Subject to your ");
            reqReport.Append(" satisfactory work performance, your services are deemed to be confirmed at the end of six months  ");
            reqReport.Append("from the date of joining unless communicated otherwise.</p></td>");
            reqReport.Append("    </tr>");

            reqReport.Append("    <tr>");
            reqReport.Append("        <td><b> 2.	Notice Period </b></p></td>");
            reqReport.Append("    </tr>");
            reqReport.Append("    <tr>");
            reqReport.Append("        <td>This contract of employment is terminable, without reason, by either party by giving one month's ");
            reqReport.Append(" notice during probationary period and two month's notice on confirmation. The management   ");
            reqReport.Append(" reserves the right to pay or recover your basic salary in lieu of notice period. Further, the company ");
            reqReport.Append(" may at its discretion relieve you from such date as it may deem fit even prior to the expiry of the    ");
            reqReport.Append(" notice period. However, if the management desires the employee to continue the employment   ");
            reqReport.Append(" during notice period, the employee shall do so.</p></td>");
            reqReport.Append("    </tr>");

            reqReport.Append("    <tr>");
            reqReport.Append("        <td  ><b> 3.	Duties </b></p></td>");
            reqReport.Append("    </tr>");
            reqReport.Append("    <tr>");
            reqReport.Append("        <td>During your employment with the Company,you shall not undertake any other employment, assignment,  ");
            reqReport.Append("business,etc.without the prior permission from the management and you shall devote  ");
            reqReport.Append(" your whole time to the faithful and diligent performance of your duties. </p></td>");
            reqReport.Append("    </tr>");
            reqReport.Append("    <tr>");
            reqReport.Append("<td>You will perform, observe and conform to such duties, directions and instructions assigned or  ");
            reqReport.Append(" communicated to you from time to time by the management and will not perform acts which are not    ");
            reqReport.Append(" befitting your position and which will in any manner adversely affect the reputation of the Company. ");
            reqReport.Append(" You may have to work in shifts as per the necessity of work.  </p></td>");
            reqReport.Append("    </tr>");

            StringBuilder firstPage = new StringBuilder();
            int pp = CandOfferDetail.CurrentPosition.ToString().Length;
            if (pp > 15)
            {
                firstPage.Append("    <tr style='height:50px'>");
                firstPage.Append("        <td ></td>");
                firstPage.Append("    </tr>");
            }
            else
            {
                firstPage.Append("    <tr style='height:50px'>");
                firstPage.Append("        <td ></td>");
                firstPage.Append("    </tr>");
            }
            reqReport.Append(firstPage);

            reqReport.Append("    <tr style='font-family :Verdana ;font-size:15pt;width:100%;'>");
            reqReport.Append("        <td style='padding-left:1px'><i> Page 1 of 7 </i>");
            reqReport.Append(" &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  ");
            reqReport.Append(" &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  ");
            reqReport.Append(" &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  ");
            reqReport.Append(" &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  ");
            reqReport.Append(" &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  ");
            reqReport.Append(" &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  ");
            reqReport.Append(" &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  ");
            reqReport.Append(" &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; ");
            reqReport.Append(" <i>Confidential </i> </p></td>  ");
            reqReport.Append("   </tr>");

            //reqReport.Append("<table style='font-family :Arial ;font-size:15pt;width:100%;'>");
            reqReport.Append("    <tr style='font-family :Arial ;font-size:15pt; width:100%;  text-align:center' ><td>");
            reqReport.Append("<i> " + footerPara[0].ToString() + "  </i> </p></td>");
            reqReport.Append("    </tr>");
            //reqReport.Append("</table>");

            StringBuilder firstPage3 = new StringBuilder();
            reqReport.Append("    <tr style='height:100px'>");
            reqReport.Append("        <td ></td>");
            reqReport.Append("    </tr>");
            reqReport.Append(firstPage3);


            //reqReport.Append("<table>");
            //reqReport.Append("    <tr style='height:110px'>");
            //reqReport.Append("        <td ></td>");
            //reqReport.Append("    </tr>");
            //reqReport.Append("</table>");

            reqReport.Append("</table>");


            #endregion 1

            #region 2
            reqReport.Append("<table style='font-family :Verdana ;font-size:16.5pt; width:100%;  text-align:justify'  background=" + UrlConstants.ApplicationBaseUrl + "Images/LetterHeads/" + OfferLetterImagePathpk + ".png>");
            StringBuilder firstPage1 = new StringBuilder();
            firstPage1.Append("    <tr style='height:120px'>");
            firstPage1.Append("        <td ></td>");
            firstPage1.Append("    </tr>");
            reqReport.Append(firstPage1);

            reqReport.Append("    <tr>");
            reqReport.Append("        <td><b> 4.	Training </b></p></td>");
            reqReport.Append("    </tr>");
            reqReport.Append("    <tr>");
            reqReport.Append("        <td>If you are required to travel overseas for purposes of receiving training or knowledge transfer, you  ");
            reqReport.Append(" shall not voluntarily separate from the company for a period of 6 months from the date of your    ");
            reqReport.Append(" return. In case you voluntarily resign then you shall be liable to pay to the Company to the extent of  ");
            reqReport.Append(" actual expenses incurred on your travel, accommodation and all other related expenses.</p></td>");
            reqReport.Append("    </tr>");

            reqReport.Append("    <tr>");
            reqReport.Append("        <td><b> 5.	Confidentiality and Fidelity </b></p></td>");
            reqReport.Append("    </tr>");
            reqReport.Append("    <tr>");
            reqReport.Append("        <td>You shall not, either during or after your employment with us, either by word of mouth or otherwise,   ");
            reqReport.Append(" divulge particulars or details of our business process, technical know-how, security arrangements,    ");
            reqReport.Append(" administrative and/or other organisational matters of confidential and secret nature which may be   ");
            reqReport.Append(" your personal privilege to know by virtue of your employment with us and you shall both during and    ");
            reqReport.Append(" after your employment with us take all reasonable precautions to keep all such information secret.   ");
            reqReport.Append(" In consideration of your gaining sensitive information about the business, affairs, operations,    ");
            reqReport.Append(" processes of the Company, you also hereby undertake, agree and covenant with the Company that  ");
            reqReport.Append(" you will not for a period of two (2) years from the date of cessation/termination of your employment    ");
            reqReport.Append(" with the Company take any employment with the competitor of the Company (engaged in the  ");
            reqReport.Append(" development of software). You will also be required to enter into Intellectual Property & Confidential     ");
            reqReport.Append(" Information Non-Disclosure Agreement with the company. </p></td>");
            reqReport.Append("    </tr>");

            reqReport.Append("    <tr>");
            reqReport.Append("        <td>We request you to submit the documents as per Annexure D on the day of your joining. Please   ");
            reqReport.Append(" complete medical test before your joining date (refer to Annexure E). This offer is subject to you    ");
            reqReport.Append(" being medically fit and satisfactory background verification report. Medical and Background   ");
            reqReport.Append(" verification reports will be retained in our records, which will be accessible to authorized persons     ");
            reqReport.Append(" only. </p></td>");
            reqReport.Append("    </tr>");

            reqReport.Append("    <tr>");
            reqReport.Append("        <td>Please sign the duplicate copy of this letter as token of your acceptance of this offer and return to HR within one week from the date of receipt.      ");
            reqReport.Append("   </p></td> </tr>");

            reqReport.Append("    <tr><td>");
            reqReport.Append(" We look forward for a long and rewarding association with us. </p></td>");
            reqReport.Append("    </tr>");

            reqReport.Append("    <tr style='height:10px'>");
            reqReport.Append("       <td ><br></td>");
            reqReport.Append("    </tr>");

            reqReport.Append("    <tr>");
            reqReport.Append("        <td>Yours sincerely,      ");
            reqReport.Append("   </p></td> </tr>");

            reqReport.Append("    <tr><td>");
            reqReport.Append(" For, Sony India Software Centre Private Limited </td>");
            reqReport.Append("    </tr>");

            reqReport.Append("    <tr><td >");
            reqReport.Append("<img src='" + UrlConstants.ApplicationBaseUrl + "Images/LetterHeads/" + SignImage + "' visible='true' style='width: 210px;height: 110px;  '/>");
            reqReport.Append("    </td></tr>");

            //reqReport.Append("    <tr style='height:70px'>");
            //reqReport.Append("       <td ><br></td>");
            //reqReport.Append("    </tr>");

            reqReport.Append("    <tr>");
            reqReport.Append("        <td>Siva Murugan Sermakani     ");
            reqReport.Append("   </td> </tr>");

            reqReport.Append("    <tr><td>");
            reqReport.Append(" Senior Manager - Talent Acquisition   </p></td>");
            reqReport.Append("    </tr>");


            StringBuilder secondPage = new StringBuilder();
            //secondPage.Append("    <tr style='height:210px'>");
            secondPage.Append("    <tr style='height:165px'>");
            secondPage.Append("        <td ></td>");
            secondPage.Append("    </tr>");
            reqReport.Append(secondPage);

            reqReport.Append("<table style='font-family :Verdana ;font-size:15pt;width:100%;'>");
            reqReport.Append("    <tr><td style='padding-left:1px'>");
            reqReport.Append("<i> Page 2 of 7 </i></td>");
            reqReport.Append(" <td style='padding-left:760px'> <i>Confidential </i></p></td>");
            reqReport.Append("    </tr>");
            reqReport.Append("</table>");

            reqReport.Append("<table style='font-family :Arial ;font-size:15pt;width:100%;'>");
            reqReport.Append("    <tr style='font-family :Arial ;font-size:15pt; width:100%;  text-align:center' ><td>");
            reqReport.Append("<i> " + footerPara[0].ToString() + "  </i> </p></td>");
            reqReport.Append("    </tr>");
            reqReport.Append("    <tr style='height:120px'>");
            reqReport.Append("        <td ></td>");
            reqReport.Append("    </tr>");
            reqReport.Append("</table>");

            reqReport.Append("</table>");

            #endregion 2

            #region 3

            reqReport.Append("<table style='font-family :Verdana ;font-size:16.5pt; width:100%;  text-align:justify'  background=" + UrlConstants.ApplicationBaseUrl + "Images/LetterHeads/" + OfferLetterImagePathpk + ".png>");

            StringBuilder firstPage2 = new StringBuilder();
            firstPage2.Append("    <tr style='height:150px'>");
            firstPage2.Append("        <td ></td>");
            firstPage2.Append("    </tr>");
            reqReport.Append(firstPage2);

            reqReport.Append("    <tr>");
            reqReport.Append("        <td style='padding-left:450px;' ><b> Annexure A </b></p></td>");
            reqReport.Append("    </tr>");
            reqReport.Append("    <tr>");
            reqReport.Append("        <td><b> Name: " + CandOfferDetail.FirstName + " " + CandOfferDetail.MiddleName + " " + CandOfferDetail.LastName + " </b></td>");
            reqReport.Append("    </tr>");
            reqReport.Append("    <tr>");
            reqReport.Append("        <td><b> Designation: " + CandOfferDetail.CurrentPosition + " </b></td>");
            reqReport.Append("    </tr>");
            reqReport.Append("    <tr>");
            reqReport.Append("        <td><b> Grade: " + CandOfferDetail.Grade + " </b></p></td>");
            reqReport.Append("    </tr>");
            reqReport.Append("    <tr>");
            reqReport.Append("        <td style='padding-left:370px;' ><b> Compensation Structure </b></p></td>");
            reqReport.Append("    </tr>");

            //************************************************
            reqReport.Append("    <tr style='padding-left:40px;'>");
            reqReport.Append("<table border='2' cellpadding='0' cellspacing='0' style='font-family :Verdana ;font-size:16.5pt;' width='95%'>");
            reqReport.Append("<tr style='background-color:#B6B6B4;'>");
            GenericLookup Component = Facade.GetGenericLookupByType(LookupType.Component);
            reqReport.Append("<td style='padding-left:180px;'><b>" + Component.Name + "</b></td>");
            GenericLookup AmountInRS = Facade.GetGenericLookupByType(LookupType.AmountInRS);
            reqReport.Append("<td style='padding-left:150px;'><b>" + AmountInRS.Name + "</b></td>");
            reqReport.Append("</tr>");
            reqReport.Append("<tr>");
            GenericLookup AnnualEntitlements = Facade.GetGenericLookupByType(LookupType.AnnualEntitlements);
            reqReport.Append("<td  colspan='2' style='padding-left:370px;'><b>" + AnnualEntitlements.Name + "</b></td>");
            //GenericLookup MonthlyEntitlements = Facade.GetGenericLookupByType(LookupType.MonthlyEntitlements);
            //reqReport.Append("<td style='padding-left:10px;'><b>" + MonthlyEntitlements.Name + "</b></td>");
            reqReport.Append("</tr>");
            reqReport.Append("<tr>");
            GenericLookup Basic = Facade.GetGenericLookupByType(LookupType.Basic);
            reqReport.Append("<td style='padding-left:10px; width:500px'>" + Basic.Name + "</td>");
            reqReport.Append("<td style='text-align:right;padding-right:20px;'>" + CandOfferDetail.Basic.ToString("#,#.##", System.Globalization.CultureInfo.CreateSpecificCulture("hi-IN")) + "</td>");
            //reqReport.Append("<td style='text-align:right;padding-right:20px;'>" + Convert.ToString(Math.Round(CandOfferDetail.Basic)) + "</td>");
            reqReport.Append("</tr>");
            reqReport.Append("<tr>");
            GenericLookup HouseRentAllowance = Facade.GetGenericLookupByType(LookupType.HouseRentAllowance);
            reqReport.Append("<td style='padding-left:10px;'>" + HouseRentAllowance.Name + "</td>");
            reqReport.Append("<td style='text-align:right;padding-right:20px;'>" + CandOfferDetail.HouseRent.ToString("#,#.##", System.Globalization.CultureInfo.CreateSpecificCulture("hi-IN")) + "</td>");
            reqReport.Append("</tr>");
            reqReport.Append("<tr>");
            //GenericLookup TransportAllowance = Facade.GetGenericLookupByType(LookupType.TransportAllowance);
            //reqReport.Append("<td style='padding-left:10px;'>" + TransportAllowance.Name + "</td>");
            //reqReport.Append("<td style='text-align:right;padding-right:20px;'>" + CandOfferDetail.Conveyance.ToString("#,#.##", System.Globalization.CultureInfo.CreateSpecificCulture("hi-IN")) + "</td>");
            //reqReport.Append("</tr>");
            //reqReport.Append("<tr>");
            GenericLookup SpecialAllowance = Facade.GetGenericLookupByType(LookupType.SpecialAllowance);
            reqReport.Append("<td style='padding-left:10px;'>" + SpecialAllowance.Name + "</td>");
            reqReport.Append("<td style='text-align:right;padding-right:20px;'>" + CandOfferDetail.SpecialAllowance.ToString("#,#.##", System.Globalization.CultureInfo.CreateSpecificCulture("hi-IN")) + "</td>");
            reqReport.Append("</tr>");

            decimal totalPerAnnumA = Math.Round(CandOfferDetail.Basic, 2) + Math.Round(CandOfferDetail.HouseRent, 2) + Math.Round(CandOfferDetail.Conveyance, 2) + Math.Round(CandOfferDetail.SpecialAllowance, 2);
            //decimal totalPerMonth = totalPerAnnumA / 12;

            //reqReport.Append("<tr style='background-color:#B6B6B4;'>");
            //GenericLookup TotalPerMonth = Facade.GetGenericLookupByType(LookupType.TotalPerMonth);
            //reqReport.Append("<td style='padding-left:10px;'><b>" + TotalPerMonth.Name + "</b> </td>");
            //reqReport.Append("<td style='text-align:right;padding-right:20px;'><b>" + Convert.ToString(Math.Round(totalPerMonth, 2)) + "</b></td>");
            //reqReport.Append("</tr>");

            reqReport.Append("<tr style='background-color:#B6B6B4;'>");
            GenericLookup TotalPerAnnumA = Facade.GetGenericLookupByType(LookupType.TotalPerAnnumA);
            reqReport.Append("<td style='padding-left:10px;'><b>" + TotalPerAnnumA.Name + "</b> </td>");
            reqReport.Append("<td style='text-align:right;padding-right:20px;'><b>" + totalPerAnnumA.ToString("#,#.##", System.Globalization.CultureInfo.CreateSpecificCulture("hi-IN")) + "</b></td>");
            reqReport.Append("</tr>");

            //reqReport.Append("<tr>");
            //GenericLookup AnnualEntitlements = Facade.GetGenericLookupByType(LookupType.AnnualEntitlements);
            //reqReport.Append("<td style='padding-left:10px;'><b>" + AnnualEntitlements.Name + "</b></td>");
            //reqReport.Append("</tr>");

            reqReport.Append("<tr>");
            GenericLookup FCP = Facade.GetGenericLookupByType(LookupType.FCP);
            reqReport.Append("<td style='padding-left:10px;'>" + FCP.Name + "</td>");
            reqReport.Append("<td style='text-align:right;padding-right:20px;'>" + CandOfferDetail.CompensationPlan.ToString("#,#.##", System.Globalization.CultureInfo.CreateSpecificCulture("hi-IN")) + "</td>");
            reqReport.Append("</tr>");
            reqReport.Append("<tr>");
            GenericLookup PF = Facade.GetGenericLookupByType(LookupType.PF);
            reqReport.Append("<td style='padding-left:10px;'>" + PF.Name + "</td>");
            reqReport.Append("<td style='text-align:right;padding-right:20px;'>" + CandOfferDetail.ProvidentFund.ToString("#,#.##", System.Globalization.CultureInfo.CreateSpecificCulture("hi-IN")) + "</td>");
            reqReport.Append("</tr>");
            decimal totalPerAnnumB = Math.Round(CandOfferDetail.CompensationPlan, 2) + Math.Round(CandOfferDetail.ProvidentFund);
            Math.Round(totalPerAnnumB, 2);
            reqReport.Append("<tr style='background-color:#B6B6B4;'>");
            GenericLookup TotalPerAnnumB = Facade.GetGenericLookupByType(LookupType.TotalPerAnnumB);
            reqReport.Append("<td style='padding-left:10px;'><b>" + TotalPerAnnumB.Name + "</b></td>");
            reqReport.Append("<td style='text-align:right;padding-right:20px;'><b>" + totalPerAnnumB.ToString("#,#.##", System.Globalization.CultureInfo.CreateSpecificCulture("hi-IN")) + "</b></td>");
            reqReport.Append("</tr>");
            reqReport.Append("<tr style='background-color:#B6B6B4;'>");
            GenericLookup TotalGross = Facade.GetGenericLookupByType(LookupType.TotalGross);
            decimal AB = totalPerAnnumA + totalPerAnnumB;
            Math.Round(AB, 2);
            reqReport.Append("<td style='padding-left:10px;'><b>" + TotalGross.Name + "</b> </td>");
            reqReport.Append("<td style='text-align:right;padding-right:20px;'><b>" + AB.ToString("#,#.##", System.Globalization.CultureInfo.CreateSpecificCulture("hi-IN")) + "</b></td>");
            reqReport.Append("</tr>");
            reqReport.Append("<tr>");
            GenericLookup AnnualVariablePay = Facade.GetGenericLookupByType(LookupType.AnnualVariablePay);
            reqReport.Append("<td style='padding-left:10px;'>" + AnnualVariablePay.Name + "</td>");
            reqReport.Append("<td style='text-align:right;padding-right:20px;'>" + CandOfferDetail.VariablePay.ToString("#,#.##", System.Globalization.CultureInfo.CreateSpecificCulture("hi-IN")) + "</td>");
            reqReport.Append("</tr>");
            reqReport.Append("<tr style='background-color:#B6B6B4;'>");
            GenericLookup TotalAnnualCTC = Facade.GetGenericLookupByType(LookupType.TotalAnnualCTC);
            decimal ctc = totalPerAnnumA + totalPerAnnumB + Math.Round(CandOfferDetail.VariablePay, 2);
            reqReport.Append("<td style='padding-left:10px;'><b>" + TotalAnnualCTC.Name + "</b></td>");
            reqReport.Append("<td style='text-align:right;padding-right:20px;'><b>" + ctc.ToString("#,#.##", System.Globalization.CultureInfo.CreateSpecificCulture("hi-IN")) + "</b></td>");
            reqReport.Append("</tr>");

            reqReport.Append("</table>");

            reqReport.Append("</tr>");
            // *********************************************

            reqReport.Append("    <tr style='height:20px'>");
            reqReport.Append("       <td ><br></td>");
            reqReport.Append("    </tr>");

            reqReport.Append("<table style='font-family :Verdana ;font-size:16.5pt;' width='100%'>");

            reqReport.Append("    <tr>");
            reqReport.Append("        <td>In addition to the above, you will be eligible for Gratuity, Medical and Personal Accident Insurance coverage.   ");
            reqReport.Append(" </p></td>");
            reqReport.Append("    </tr>");

            reqReport.Append("    <tr>");
            reqReport.Append("        <td>*Flexible Compensation Plan (FCP) comprises of several components as detailed in Annexure B.     ");
            reqReport.Append("   </p></td> </tr>");

            reqReport.Append("    <tr><td>");
            reqReport.Append(" ** Variable Pay will depend on the Company's and Individual's performance during the financial year and will be paid out as per the company policy (refer to Annexure C). </p></td>");
            reqReport.Append("    </tr>");

            reqReport.Append("    <tr style='height:50px'>");
            reqReport.Append("       <td ><br></td>");
            reqReport.Append("    </tr>");

            reqReport.Append("    <tr>");
            reqReport.Append("        <td>Yours sincerely,      ");
            reqReport.Append("   </p></td> </tr>");
            reqReport.Append("</table>");

            reqReport.Append("<table style='font-family :Verdana ;font-size:16.5pt;'>");
            reqReport.Append("    <tr><td style='padding-left:1px;'>");
            reqReport.Append(" For,Sony India Software Centre Private Limited </td>");
            reqReport.Append(" <td style='padding-left:162px;'> Confirmed and Accepted </td>");
            reqReport.Append("    </tr>");

            reqReport.Append("    <tr><td >");
            reqReport.Append("<img src='" + UrlConstants.ApplicationBaseUrl + "Images/LetterHeads/" + SignImage + "' visible='true' style='width: 210px;height: 110px;  '/>");
            reqReport.Append("    </td></tr>");

            //reqReport.Append("    <tr style='height:100px'>");
            //reqReport.Append("       <td ><br></td>");
            //reqReport.Append("    </tr>");

            reqReport.Append("    <tr>");
            reqReport.Append("        <td>Siva Murugan Sermakani    </td> ");
            reqReport.Append(" <td style='padding-left:170px;'>   " + candidatedetails.FirstName + " " + CandOfferDetail.MiddleName + " " + CandOfferDetail.LastName + " </p></td>");
            reqReport.Append("   </p></td> </tr>");

            reqReport.Append("    <tr><td>");
            reqReport.Append(" Senior Manager - Talent Acquisition   </p></td>");
            reqReport.Append("    </tr>");

            StringBuilder thirdPage = new StringBuilder();
            thirdPage.Append("    <tr style='height:200px'>");
            thirdPage.Append("        <td ></td>");
            thirdPage.Append("    </tr>");
            reqReport.Append(thirdPage);
            reqReport.Append("</table>");

            reqReport.Append("<table style='font-family :Verdana ;font-size:15pt;width:100%;'>");
            reqReport.Append("    <tr><td style='padding-left:1px'>");
            reqReport.Append("<i> Page 3 of 7 </i></td>");
            reqReport.Append(" <td style='padding-left:760px'> <i>Confidential </i></p></td>");
            reqReport.Append("    </tr>");
            reqReport.Append("</table>");

            reqReport.Append("<table style='font-family :Arial ;font-size:15pt;width:100%;'>");
            reqReport.Append("    <tr style='font-family :Arial ;font-size:15pt; width:100%;  text-align:center' ><td>");
            reqReport.Append("<i> " + footerPara[0].ToString() + "  </i> </p></td>");
            reqReport.Append("    </tr>");
            reqReport.Append("    <tr style='height:55px'>");
            reqReport.Append("        <td ></td>");
            reqReport.Append("    </tr>");
            reqReport.Append("</table>");

            reqReport.Append("</table>");
            #endregion 3

            #region 4

            reqReport.Append("<table style='font-family :Verdana ;font-size:16.5pt; width:100%;  text-align:justify'  background=" + UrlConstants.ApplicationBaseUrl + "Images/LetterHeads/" + OfferLetterImagePathpk + ".png>");

            //reqReport.Append("<table style='font-family :Verdana ;font-size:16.5pt;'>");
            StringBuilder firstPage25 = new StringBuilder();
            firstPage25.Append("    <tr style='height:150px'>");
            firstPage25.Append("        <td ></td>");
            firstPage25.Append("    </tr>");
            reqReport.Append(firstPage25);

            reqReport.Append("    <tr>");
            reqReport.Append("        <td style='padding-left:450px;' ><b> Annexure B </b></p></td>");
            reqReport.Append("    </tr>");
            reqReport.Append("    <tr>");
            reqReport.Append("        <td style='padding-left:1px;' ><b> Flexible Compensation Plan: </b></p></td>");
            reqReport.Append("    </tr>");

            reqReport.Append("    <tr>");
            reqReport.Append("        <td>Flexible Compensation Plan (FCP) is part of Annual Gross Salary, which allows employees to    ");
            reqReport.Append(" choose various tax beneficial components from the offered list in their appointment letter, depending     ");
            reqReport.Append(" upon their personal preferences and tax liabilities. Participation in FCP is optional.   </p></td> ");
            reqReport.Append("    </tr>");

            reqReport.Append("    <tr>");
            reqReport.Append("        <td>If you are interested in utilizing benefits under FCP, you will be required to declare the amount   ");
            reqReport.Append(" under each of the components chosen, subject to maximum limit specified for each component. </p></td>");
            reqReport.Append("    </tr>");

            reqReport.Append("    <tr>");
            reqReport.Append("        <td>If you are not interested to participate under FCP, you have to declare the same. The undeclared     ");
            reqReport.Append("   FCP component will be paid along with your monthly salary. </p></td> </tr>");

            reqReport.Append("    <tr>");
            reqReport.Append("        <td>FCP comprises of Leave Travel Assistance (LTA), Books and / Periodicals & Professional    ");
            reqReport.Append(" Development Reimbursement, Telephone Reimbursement and Food  ");
            reqReport.Append(" Voucher. The maximum limits on various FCP components are as per the company guidelines as detailed below.  </p></td>");
            reqReport.Append("    </tr>");
            reqReport.Append("</table>");

            //Add Headers
            GenericLookup flexiTitle = Facade.GetGenericLookupByType(LookupType.flexiTableHeader);

            //table data
            IList<GenericLookup> flexiTable1 = null;
            if (CandOfferDetail.Grade == "SGG7" || CandOfferDetail.Grade == "SGG8" || CandOfferDetail.Grade == "SGG9")
            {
                flexiTable1 = Facade.GetAllGenericLookupByLookupType(LookupType.SM1FlexiTable);
            }
            if (CandOfferDetail.Grade == "SGG5")
            {
                flexiTable1 = Facade.GetAllGenericLookupByLookupType(LookupType.SGG5FlexiTable);
            }
            if (CandOfferDetail.Grade == "SGG4a")
            {
                flexiTable1 = Facade.GetAllGenericLookupByLookupType(LookupType.SGG4aFlexiTable);
            }
            if (CandOfferDetail.Grade == "SGG2a" || CandOfferDetail.Grade == "SGG3" ||
                CandOfferDetail.Grade == "SGG3a" || CandOfferDetail.Grade == "SGG4")
            {
                flexiTable1 = Facade.GetAllGenericLookupByLookupType(LookupType.SGG2FlexiTable);
            }
            if (CandOfferDetail.Grade == "SGG5a")
            {
                flexiTable1 = Facade.GetAllGenericLookupByLookupType(LookupType.SGG5aFlexiTable);
            }
            if (CandOfferDetail.Grade == "SGG6")
            {
                flexiTable1 = Facade.GetAllGenericLookupByLookupType(LookupType.SGG6FlexiTable);
            }

            try
            {
                reqReport.Append("    <tr>");
                reqReport.Append("<table border='1' cellpadding='0' cellspacing='0' style='font-family :Verdana ;font-size:16.5pt;padding-left:20px;' width='100%'>");
                reqReport.Append("<tr>");
                reqReport.Append("<td style='padding-left:120px;'><b>" + flexiTitle.Name + "</b></td>");
                reqReport.Append("<td style='padding-left:15px;'><b>" + flexiTitle.Description + "</b></td>");
                reqReport.Append("</tr>");

                for (int i = 0; i < flexiTable1.Count; i++)
                {
                    if (flexiTable1[i].Name != "Medical Reimbursement")
                    {
                        reqReport.Append("<tr>");
                        reqReport.Append("<td style='padding-left:10px;'>" + flexiTable1[i].Name + "</td>");
                        reqReport.Append("<td style='padding-left:10px;'>" + flexiTable1[i].Description + "</td>");
                        reqReport.Append("</tr>");
                    }
                }
                reqReport.Append("</table>");
                reqReport.Append("    </tr>");
            }
            catch
            {
            }

            reqReport.Append("    <tr style='height:40px'>");
            reqReport.Append("       <td ><br></td>");
            reqReport.Append("    </tr>");

            reqReport.Append("<table style='font-family :Verdana ;font-size:16.5pt;'>");
            reqReport.Append("    <tr>");
            reqReport.Append("        <td>Note: The total FCP declaration under different components should not exceed the FCP amount mentioned in Annexure A.  ");
            reqReport.Append("   </p></td> </tr>");

            StringBuilder FourPage = new StringBuilder();
            //int trsize = 520;
            int trsize = 560;

            if (flexiTable1.Count <= 5)
            {
                FourPage.Append("    <tr style='height:" + trsize + "px'>");
                FourPage.Append("        <td ></td>");
                FourPage.Append("    </tr>");
            }
            else
            {
                for (int i = 6; i <= flexiTable1.Count; i++)
                {
                    trsize = trsize - 27; //25                    
                }
                FourPage.Append("    <tr style='height:" + trsize + "px'>");
                FourPage.Append("        <td ></td>");
                FourPage.Append("    </tr>");
            }
            reqReport.Append(FourPage);

            reqReport.Append("<table style='font-family :Verdana ;font-size:15pt;width:100%;'>");
            reqReport.Append("    <tr><td style='padding-left:1px'>");
            reqReport.Append("<i> Page 4 of 7 </i></td>");
            reqReport.Append(" <td style='padding-left:760px'> <i>Confidential </i></p></td>");
            reqReport.Append("    </tr>");
            reqReport.Append("</table>");

            reqReport.Append("<table style='font-family :Arial ;font-size:15pt;width:100%;'>");
            reqReport.Append("    <tr style='font-family :Arial ;font-size:15pt; width:100%;  text-align:center' ><td>");
            reqReport.Append("<i> " + footerPara[0].ToString() + "  </i> </p></td>");
            reqReport.Append("    </tr>");
            reqReport.Append("    <tr style='height:55px'>");
            reqReport.Append("        <td ></td>");
            reqReport.Append("    </tr>");
            reqReport.Append("</table>");

            reqReport.Append("</table>");
            #endregion 4

            #region 5
            reqReport.Append("<table style='font-family :Verdana ;font-size:16.5pt; width:100%;  text-align:justify'  background=" + UrlConstants.ApplicationBaseUrl + "Images/LetterHeads/" + OfferLetterImagePathpk + ".png>");
            StringBuilder firstPage24 = new StringBuilder();
            firstPage24.Append("    <tr style='height:150px'>");
            firstPage24.Append("        <td ></td>");
            firstPage24.Append("    </tr>");
            reqReport.Append(firstPage24);

            reqReport.Append("    <tr>");
            reqReport.Append("        <td style='padding-left:450px;' ><b> Annexure C </b></p></p></td>");
            reqReport.Append("    </tr>");
            reqReport.Append("    <tr>");
            reqReport.Append("        <td ><b> Variable Pay: </b></p></td>");
            reqReport.Append("    </tr>");

            reqReport.Append("    <tr>");
            reqReport.Append("        <td>The salary structure comprises of Variable Pay. Variable Pay is a part of total compensation and is   ");
            reqReport.Append(" designed to support high performance work culture.   </p></td> ");
            reqReport.Append("    </tr>");

            reqReport.Append("    <tr>");
            reqReport.Append("        <td>Variable Pay is paid based on parameters like-Employee Performance,Division Performance,   ");
            reqReport.Append(" Global KPI and Company Financial Index as relevant to grade and division.  </p></td>");
            reqReport.Append("    </tr>");

            reqReport.Append("    <tr>");
            reqReport.Append("        <td ><b> Eligibility: </b></p></td>");
            reqReport.Append("    </tr>");

            reqReport.Append("    <tr>");
            reqReport.Append("        <td>Employees joining the organization will get variable pay paid proportionately for the period from the   ");
            reqReport.Append(" date of joining to the cycle last date (March 31st or September 30th).  ");
            reqReport.Append(" Employees who have spent only 3 months or less will be considered for 100% payout as the time   ");
            reqReport.Append(" spent by then in the organization is very little or a formal evaluation.   ");
            reqReport.Append(" Employees resigning the organization will get the variable pay if they have completed the full term   ");
            reqReport.Append("   of 6 months (March 31st or September 30th). </p></td> </tr>");

            reqReport.Append("    <tr>");
            reqReport.Append("        <td>Pay out date: Variable Pay will be paid in the month of June and December along with monthly    ");
            reqReport.Append(" salary, every year proportionate to the period spent in the Organization.  </p></td>");
            reqReport.Append("    </tr>");

            StringBuilder FivePage = new StringBuilder();
            FivePage.Append("    <tr style='height:760px'>");
            FivePage.Append("        <td ></td>");
            FivePage.Append("    </tr>");
            reqReport.Append(FivePage);

            reqReport.Append("<table style='font-family :Verdana ;font-size:15pt;width:100%;'>");
            reqReport.Append("    <tr><td style='padding-left:1px'>");
            reqReport.Append("<i> Page 5 of 7 </i></td>");
            reqReport.Append(" <td style='padding-left:760px'> <i>Confidential </i></p></td>");
            reqReport.Append("    </tr>");
            reqReport.Append("</table>");

            reqReport.Append("<table style='font-family :Arial ;font-size:15pt;width:100%;'>");
            reqReport.Append("    <tr style='font-family :Arial ;font-size:15pt; width:100%;  text-align:center' ><td>");
            reqReport.Append("<i> " + footerPara[0].ToString() + "  </i> </p></td>");
            reqReport.Append("    </tr>");
            reqReport.Append("    <tr style='height:120px'>");
            reqReport.Append("        <td ></td>");
            reqReport.Append("    </tr>");
            reqReport.Append("</table>");

            reqReport.Append("</table>");

            #endregion 5

            #region 6
            reqReport.Append("<table style='font-family :Verdana ;font-size:16.5pt; width:100%;  text-align:justify'  background=" + UrlConstants.ApplicationBaseUrl + "Images/LetterHeads/" + OfferLetterImagePathpk + ".png>");

            StringBuilder firstPage26 = new StringBuilder();
            firstPage26.Append("    <tr style='height:150px'>");
            firstPage26.Append("        <td ></td>");
            firstPage26.Append("    </tr>");
            reqReport.Append(firstPage26);

            reqReport.Append("    <tr>");
            reqReport.Append("        <td style='padding-left:450px;' ><b> Annexure D </b></p></td>");
            reqReport.Append("    </tr>");
            reqReport.Append("    <tr>");
            reqReport.Append("        <td ><b> List of Documents to be submitted to HR: </b></p></td>");
            reqReport.Append("    </tr>");

            reqReport.Append("    <tr>");
            reqReport.Append("        <td>You are requested to bring along with you the following documents in original (and 2 copies each)    ");
            reqReport.Append(" on the date of joining and hand over the same to us:  </p></td> ");
            reqReport.Append("    </tr>");

            reqReport.Append("    <tr>");
            reqReport.Append("        <td > 1.	Employee Application Form (duly filled) </td>");
            reqReport.Append("    </tr>");
            reqReport.Append("    <tr>");
            reqReport.Append("        <td > 2.	Employment Verification Form (duly filled) </td>");
            reqReport.Append("    </tr>");
            reqReport.Append("    <tr>");
            reqReport.Append("        <td > 3.	Recent passport size photographs - 5 Nos. </td>");
            reqReport.Append("    </tr>");
            reqReport.Append("    <tr>");
            reqReport.Append("        <td > 4.	Education Certificates: </td>");
            reqReport.Append("    </tr>");

            reqReport.Append("    <tr>");
            reqReport.Append("        <td style='padding-left:50px;' > a.	10th Standard </td>");
            reqReport.Append("    </tr>");
            reqReport.Append("    <tr>");
            reqReport.Append("        <td style='padding-left:50px;' > b.	12th Standard / Diploma </td>");
            reqReport.Append("    </tr>");
            reqReport.Append("    <tr>");
            reqReport.Append("        <td style='padding-left:50px;' > c.	Bachelor's Degree (All semester mark sheets) </td>");
            reqReport.Append("    </tr>");
            reqReport.Append("    <tr>");
            reqReport.Append("        <td style='padding-left:50px;' > d.	Master's Degree / Post Graduation (All semester mark sheets) </td>");
            reqReport.Append("    </tr>");
            reqReport.Append("    <tr>");
            reqReport.Append("        <td style='padding-left:50px;' >  e.	Certifications if any </td>");
            reqReport.Append("    </tr>");
            reqReport.Append("    <tr>");
            reqReport.Append("        <td style='padding-left:50px;' > f.	Any other </td>");
            reqReport.Append("    </tr>");

            reqReport.Append("    <tr>");
            reqReport.Append("        <td > 5.	Experience certificates from all previous employers </td>");
            reqReport.Append("    </tr>");
            reqReport.Append("    <tr>");
            reqReport.Append("        <td > 6.	Relieving letter from your previous / last organization </td>");
            reqReport.Append("    </tr>");
            reqReport.Append("    <tr>");
            reqReport.Append("        <td > 7.	Salary breakup of previous / last organization  </td>");
            reqReport.Append("    </tr>");
            reqReport.Append("    <tr>");
            reqReport.Append("        <td > 8.	Copy of Passport </td>");
            reqReport.Append("    </tr>");
            reqReport.Append("    <tr>");
            reqReport.Append("        <td > 9.	Copy of PAN Card </td>");
            reqReport.Append("    </tr>");
            reqReport.Append("    <tr>");
            reqReport.Append("        <td > 10. Copy of Aadhar Card  </td>");
            reqReport.Append("    </tr>");
            reqReport.Append("    <tr>");
            reqReport.Append("        <td > </br></br> </td>");
            reqReport.Append("    </tr>");

            StringBuilder sixPage = new StringBuilder();
            //sixPage.Append("    <tr style='height:670px'>");
            sixPage.Append("    <tr style='height:640px'>");
            sixPage.Append("        <td ></td>");
            sixPage.Append("    </tr>");
            reqReport.Append(sixPage);

            reqReport.Append("<table style='font-family :Verdana ;font-size:15pt;width:100%;'>");
            reqReport.Append("    <tr><td style='padding-left:1px'>");
            reqReport.Append("<i> Page 6 of 7 </i></td>");
            reqReport.Append(" <td style='padding-left:760px'> <i>Confidential </i></p></td>");
            reqReport.Append("    </tr>");
            reqReport.Append("</table>");

            reqReport.Append("<table style='font-family :Arial ;font-size:15pt;width:100%;'>");
            reqReport.Append("    <tr style='font-family :Arial ;font-size:15pt; width:100%;  text-align:center' ><td>");
            reqReport.Append("<i> " + footerPara[0].ToString() + "  </i> </p></td>");
            reqReport.Append("    </tr>");
            reqReport.Append("    <tr style='height:85px'>");
            reqReport.Append("        <td ></td>");
            reqReport.Append("    </tr>");
            reqReport.Append("</table>");

            reqReport.Append("</table>");

            #endregion 6

            #region 7
            reqReport.Append("<table style='font-family :Verdana ;font-size:16.5pt; width:100%;  text-align:justify'  background=" + UrlConstants.ApplicationBaseUrl + "Images/LetterHeads/" + OfferLetterImagePath + ".png>");
            StringBuilder firstPage36 = new StringBuilder();
            firstPage36.Append("    <tr style='height:225px'>");
            firstPage36.Append("        <td ></td>");
            firstPage36.Append("    </tr>");
            reqReport.Append(firstPage36);

            reqReport.Append("    <tr>");
            reqReport.Append("        <td style='padding-left:450px;' ><b> Annexure E </b></p></td>");
            reqReport.Append("    </tr>");
            reqReport.Append("    <tr>");
            reqReport.Append("        <td>Date: " + DateTime.Now.ToString("dd/MM/yyyy") + "</p></td>");
            reqReport.Append("    </tr>");

            reqReport.Append("    <tr>");
            reqReport.Append("        <td>To, </td>");
            reqReport.Append("    </tr>");
            reqReport.Append("    <tr>");
            reqReport.Append("        <td >The Relationship Manager, </td>");
            reqReport.Append("    </tr>");
            reqReport.Append("    <tr>");
            reqReport.Append("        <td >Sakra World Hospital, Bangalore </p></td>");
            reqReport.Append("    </tr>");

            reqReport.Append("    <tr>");
            reqReport.Append("        <td > Dear Sir / Madam,  </p></td>");
            reqReport.Append("    </tr>");
            reqReport.Append("    <tr>");
            reqReport.Append("        <td ><u>Sub : Pre-Employment Medical Check-Up </u></p></td>");
            reqReport.Append("    </tr>");

            reqReport.Append("    <tr>");
            reqReport.Append("        <td>We are hereby referring <b>" + gender + " " + CandOfferDetail.FirstName + " " + CandOfferDetail.MiddleName + " " + CandOfferDetail.LastName + " </b> pre-employment Medical Check-up.  We request you     ");
            reqReport.Append(" to complete the pre-employment medical check-up in any of the following locations: </p></td> ");
            reqReport.Append("    </tr>");

            reqReport.Append("    <tr>");
            reqReport.Append("        <td>SAKRA WORLD HOSPITAL (A UNIT OF TAKSHASILA HOSPITAL OPERATING PRIVATE LIMITED)    ");
            reqReport.Append(" 52/2 & 52/3, Devarabeesanahalli, Varthur Hobli, Opposite Intel, Outer Ring Road, Marathahalli, Bangalore.-560103,  </p></td> ");
            reqReport.Append("    </tr>");

            reqReport.Append("    <tr>");
            reqReport.Append("        <td>For appointment, contact Phone: 080- 49694969 or Mobile: +91- 7899755408  </p></p></td>  ");
            reqReport.Append("    </tr>");

            reqReport.Append("    <tr>");
            reqReport.Append("        <td>Please send the bill to the following address: </p></td>  ");
            reqReport.Append("    </tr>");

            reqReport.Append("    <tr>");
            reqReport.Append("        <td>Sony India Software Centre Private Limited (SISCPL), 2nd Floor, Tower 2 B, Hibiscus, Embassy  ");
            reqReport.Append(" Tech Village, Outer Ring Road, Devarabisanahalli, Varthur Hobli,  ");
            reqReport.Append(" Bangalore - 560 037; Tel. No.:  +91 80 6707 8500    </p></td> ");
            reqReport.Append("    </tr>");


            reqReport.Append("    <tr style='height:40px'>");
            reqReport.Append("       <td ><br></td>");
            reqReport.Append("    </tr>");

            reqReport.Append("    <tr>");
            reqReport.Append("        <td>Yours sincerely,  </p></td>");
            reqReport.Append("    </tr>");

            reqReport.Append("    <tr><td>");
            reqReport.Append(" For, Sony India Software Centre Private Limited </td>");
            reqReport.Append("    </tr>");

            reqReport.Append("    <tr><td >");
            reqReport.Append("<img src='" + UrlConstants.ApplicationBaseUrl + "Images/LetterHeads/" + SignImage + "' visible='true' style='width: 210px;height: 110px;  '/>");
            reqReport.Append("    </td></tr>");

            //reqReport.Append("    <tr style='height:110px'>");
            //reqReport.Append("       <td ><br></td>");
            //reqReport.Append("    </tr>");

            reqReport.Append("    <tr><td>");
            reqReport.Append(" Siva Murugan Sermakani </td>");
            reqReport.Append("    </tr>");
            reqReport.Append("    <tr><td>");
            reqReport.Append("  Senior Manager - Talent Acquisition   </p></td>");
            reqReport.Append("    </tr>");

            reqReport.Append("    <tr><td>");
            reqReport.Append(" <b>Note:</b> Instructions for the candidate. </td>");
            reqReport.Append("    </tr>");
            reqReport.Append("    <tr><td style='padding-left:30px;' >");
            reqReport.Append(" 1.	Take prior appointment for Pre-Employment Medical Check-Up. </td>");
            reqReport.Append("    </tr>");
            reqReport.Append("    <tr><td style='padding-left:30px;' >");
            reqReport.Append(" 2.	 Fill in the feedback form given by the hospital (Mandatory) </td>");
            reqReport.Append("    </tr>");

            reqReport.Append("    <tr><td>");
            reqReport.Append("  For any query please contact Ms.Anu from Sakra Hospitals: +91-7899755408 </p></td>");
            reqReport.Append("    </tr>");

            StringBuilder firstPage9 = new StringBuilder();
            firstPage9.Append("    <tr style='height:15px'>");
            firstPage9.Append("        <td ></td>");
            firstPage9.Append("    </tr>");
            reqReport.Append(firstPage9);

            //reqReport.Append("<table style='font-family :Verdana ;font-size:15pt;width:100%;'>");
            //reqReport.Append("    <tr><td style='padding-left:1px'>");
            //reqReport.Append("<i> Page 7 of 7 </i></td>");
            //reqReport.Append(" <td style='padding-left:760px'> <i>Confidential </i></p></td>");
            //reqReport.Append("    </tr>");
            //reqReport.Append("</table>");

            //reqReport.Append("<table style='font-family :Arial ;font-size:15pt;width:100%;'>");
            //reqReport.Append("    <tr style='font-family :Arial ;font-size:15pt; width:100%;  text-align:center' ><td>");
            //reqReport.Append("<i> " + footerPara[0].ToString() + "  </i> </p></td>");
            //reqReport.Append("    </tr>");
            //reqReport.Append("</table>");

            reqReport.Append("    <tr style='font-family :Verdana ;font-size:15pt;width:100%;'>");
            reqReport.Append("        <td style='padding-left:1px'><i> Page 7 of 7 </i>");
            reqReport.Append(" &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  ");
            reqReport.Append(" &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  ");
            reqReport.Append(" &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  ");
            reqReport.Append(" &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  ");
            reqReport.Append(" &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  ");
            reqReport.Append(" &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  ");
            reqReport.Append(" &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  ");
            reqReport.Append(" &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; ");
            reqReport.Append(" <i>Confidential </i> </p></td>  ");
            reqReport.Append("   </tr>");

            reqReport.Append("    <tr style='font-family :Arial ;font-size:15pt; width:100%;  text-align:center' ><td>");
            reqReport.Append("<i> " + footerPara[0].ToString() + "  </i> </p></td>");
            reqReport.Append("    </tr>");

            StringBuilder firstPage92 = new StringBuilder();
            firstPage92.Append("    <tr style='height:129px'>");
            firstPage92.Append("        <td ></td>");
            firstPage92.Append("    </tr>");
            reqReport.Append(firstPage92);

            reqReport.Append("</table>");

            #endregion 7

            #endregion
            reqReport.Append("</table>");
            fullpath = UrlConstants.ApplicationBaseUrl + "Resources/Member/" + MemberId + "/" + "Offer Letter/" + Filename;
            try
            {
                PdfConverter pdfConverter = new PdfConverter();
                UnicodeEncoding unicodeEncoding = new UnicodeEncoding();
                pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
                pdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Normal;
                pdfConverter.PdfDocumentOptions.ShowHeader = false;
                pdfConverter.PdfDocumentOptions.ShowFooter = false;
                pdfConverter.PdfDocumentOptions.LeftMargin = 55;
                pdfConverter.PdfDocumentOptions.RightMargin = 50;
                pdfConverter.PdfDocumentOptions.TopMargin = 30;
                pdfConverter.PdfDocumentOptions.BottomMargin = 5;
                pdfConverter.PdfDocumentOptions.GenerateSelectablePdf = true;
                pdfConverter.PdfDocumentOptions.ShowHeader = false;
                pdfConverter.PdfHeaderOptions.DrawHeaderLine = false;
                pdfConverter.PdfFooterOptions.FooterTextColor = System.Drawing.Color.Black;
                pdfConverter.PdfFooterOptions.DrawFooterLine = false;
                pdfConverter.PdfFooterOptions.FooterText = "";
                pdfConverter.PdfFooterOptions.PageNumberText = "Page";
                pdfConverter.PdfFooterOptions.ShowPageNumber = true;
                pdfConverter.LicenseKey = ContextConstants.EPHTMLTOPDFKEY;


                string Filename1 = MemberId + ".html";
                string strFilePath = MiscUtil.GetMemberDocumentPath(this.Page, MemberId, Filename1, "Offer Letter", false);
                string physicalPath = HttpContext.Current.Request.PhysicalApplicationPath;
                fullpath = physicalPath + "Resources\\Member\\" + MemberId + "\\" + "Offer Letter\\" + Filename1;
                System.IO.StreamWriter file = new System.IO.StreamWriter(fullpath);
                file.WriteLine(reqReport.ToString());
                file.Close();
                byte[] arrBytData = pdfConverter.GetPdfFromUrlBytes(fullpath);


                System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
                response.Clear();
                response.AddHeader("Content-Type", "binary/octet-stream");
                response.AddHeader("Content-Disposition", "attachment; filename=" + Filename + "; size=" + arrBytData.Length.ToString());
                response.Flush();
                response.BinaryWrite(arrBytData);
                response.Flush();
                response.End();
            }
            catch
            { }
            fullpath = UrlConstants.ApplicationBaseUrl + "Resources/Member/" + MemberId + "/" + "Offer Letter/" + Filename;
        }
        return fullpath;
    }
    public string UploadDocument(int MemberId, string Filename, byte[] output, string Title)
    {
        string strFilePath = MiscUtil.GetMemberDocumentPath(this.Page, MemberId, Filename, "Compensation Annexure", false);

        IList<GenericLookup> gen = Facade.GetAllGenericLookup();

        var lookupId = (from gId in gen
                        where gId.Name == "Compensation Annexure"
                        select gId.Id).SingleOrDefault();

        BinaryWriter Writer = null;
        try
        {
            Writer = new BinaryWriter(File.OpenWrite(strFilePath));
            Writer.Write(output);
            Writer.Flush();
            Writer.Close();

            if (File.Exists(strFilePath))
            {
                MemberDocument memberDocument = Facade.GetMemberDocumentByMemberIdTypeAndFileName(MemberId, "Compensation Annexure", Filename);

                if (memberDocument == null)
                {
                    MemberDocument newDoc = new MemberDocument();
                    newDoc.FileName = Filename;//1.6
                    newDoc.FileTypeLookupId = Convert.ToInt32(lookupId);
                    newDoc.Description = "Compensation Annexure";
                    newDoc.MemberId = MemberId;
                    newDoc.Title = Title;
                    if (newDoc.Title.ToString() != string.Empty)
                    {
                        Facade.AddMemberDocument(newDoc);
                    }
                }
                else
                {

                }
            }
        }
        catch
        {
        }
        return strFilePath;
    }

    public string FileDownload(string candidateId, string memberHiringId)
    {
        try
        {
            string savedPdfPath = Server.MapPath("~/Templates/Download File/");
            string templateFilePath = "";
            string filename = "";
            StringBuilder text = new StringBuilder();
            /*******start*******Following Code is for PDF Generation*********Suraj adsule**********20160822*/

            OfferGeneration CandDetail = Facade.GetCandidateDetail_UsingMemberHiringId(Convert.ToInt32(memberHiringId), 1);
            if (CandDetail.DemandType.ToLower() == "permanent")
            {
                OfferGeneration CandOfferDetail = Facade.GetCandidateDetail_UsingMemberHiringId(Convert.ToInt32(memberHiringId),2);

                #region For Position SM1 and above
                templateFilePath = Server.MapPath(CandOfferDetail.FilePath);
                string candidateFullName = CandOfferDetail.FirstName + " " + CandOfferDetail.MiddleName + " " + CandOfferDetail.LastName;
                filename = "Offer_LetterTemplate-" + candidateFullName + ".pdf";
                savedPdfPath = savedPdfPath + filename;

                if(File.Exists(savedPdfPath))
                {
                    File.Delete(savedPdfPath);
                }
                using (FileStream fs = new FileStream(savedPdfPath, FileMode.Create, FileAccess.Write))
                {
                    //FileStream fs = new FileStream("PDFGeneratorSample.pdf", FileMode.Create, FileAccess.Write, FileShare.None);
                    //Added by Kanchan Yeware
                   //Document doc = new Document(PageSize.A4, 36, 36, 36, 150);//old by 9/Jan/2016
                   // Document doc = new Document(PageSize.A4, 30, 30, 30, 120);//new by 9/Jan/2016
                    Document doc = new Document(PageSize.A4, 30, 30, 20, 100);//old by 9/Jan/2016

                    //Document doc1 = new Document(PageSize.A4, 36, 25, 20, 100);//old by 9/Jan/2016

                    //Document doc = new Document(PageSize.A4, 20f, 20f, 20f, 20f);
                    PdfWriter writer = PdfWriter.GetInstance(doc, fs);
                    ITextExtractionStrategy strategy = new SimpleTextExtractionStrategy();           

                    //read pdf
                    using (PdfReader reader = new PdfReader(templateFilePath))
                    {
                        for (int i = 1; i <= reader.NumberOfPages; i++)
                        {
                            //reader.RemoveHeaderFooters(doc);
                            //doc.Save(@"C:\output.pdf");
                           
                            //reader.RemoveFields(RemoveHeaderFooters(doc));
                            //reader.RemoveUnusedObjects(PdfTextExtractor.GetTextFromPage(reader, i,strategy));

                           // text.Replace(PdfTextExtractor.GetTextFromPage(reader, i, strategy), StringSplitOptions.None);
                            text.Append(PdfTextExtractor.GetTextFromPage(reader, i));

                            //text1 = Encoding.UTF8.GetString(ASCIIEncoding.Convert(Encoding.Default, Encoding.UTF8, Encoding.Default.GetBytes(text1)));
                            //reader.RemoveFields();
                        }
                    }

                    //replace with actual data
                    if (CandOfferDetail != null)
                    {                       
                        text.Replace("[%Candidate_Name%]", candidateFullName);
                        text.Replace("[%PermanentAddressLine1%]", CandOfferDetail.PermanentAddressLine1);
                        text.Replace("[%PermanentAddressLine2%]", CandOfferDetail.PermanentAddressLine2);
                        text.Replace("[%PermanentCity%]", CandOfferDetail.PermanentCity);
                        if (CandOfferDetail.PermanentState != "")
                        {
                            CandOfferDetail.PermanentState = ',' + CandOfferDetail.PermanentState;
                        }
                        else
                        {
                            CandOfferDetail.PermanentState = "";
                        }
                        text.Replace("[%PermanentState%]", CandOfferDetail.PermanentState);
                        if (CandOfferDetail.PermanentZip != "")
                        {
                            CandOfferDetail.PermanentZip = ',' + " PIN-" +  CandOfferDetail.PermanentZip;
                        }
                        else
                        {
                            CandOfferDetail.PermanentZip = "";
                        }
                        text.Replace("[%PermanentZip%]", CandOfferDetail.PermanentZip);

                        //text.Replace("[%Candidate_Address%]", CandOfferDetail.PermanentAddressLine1 + "\n " + CandOfferDetail.PermanentAddressLine2 + "\n" +
                        //    CandOfferDetail.PermanentCity + " , " + CandOfferDetail.PermanentState + "-" + CandOfferDetail.PermanentZip);
                        text.Replace("[%Candidate_fName%]", Convert.ToString(CandOfferDetail.FirstName));
                        text.Replace("[%Designation%]", Convert.ToString(CandOfferDetail.CurrentPosition));
                        text.Replace("[%Grade%]", Convert.ToString(CandOfferDetail.Grade));
                        string dt1 = (CandOfferDetail.DateOfJoining == DateTime.MinValue )?string.Empty:Convert.ToString(CandOfferDetail.DateOfJoining.ToShortDateString());
                        text.Replace("[%Joining_Date%]", dt1);
                        string dt2 = (CandOfferDetail.DateOfJoining == DateTime.MinValue) ? string.Empty : Convert.ToString(CandOfferDetail.DateOfOffered.ToShortDateString());
                        text.Replace("[% Offered_Date%]", dt1);                       
                    }
                    else
                    {
                        text.Replace("[%%PermanentAddressLine1%]", string.Empty);
                        text.Replace("[%%PermanentAddressLine2%]", string.Empty);
                        text.Replace("[%PermanentCity%]", string.Empty);
                        text.Replace("[%PermanentState%]", string.Empty);
                        text.Replace("[%PermanentZip%]", string.Empty);
                        text.Replace("[%Candidate_Name%]", string.Empty);
                        text.Replace("[%Candidate_Address%]", string.Empty);
                        text.Replace("[%Candidate_fName%]", string.Empty);
                        text.Replace("[%Designation%]", string.Empty);
                        text.Replace("[%Grade%]", string.Empty);
                        text.Replace("[%Joining_Date%]", string.Empty);
                        text.Replace("[%CTC%]", string.Empty);
                        text.Replace("[% Offered_Date%]", string.Empty);       
                    }
                    //new code 20160909
                    string[] TemplateSplitWithTitle = text.ToString().Split(new string[] { "[%Title%]" }, StringSplitOptions.None);

                    var normalFont = FontFactory.GetFont("Arial", 11, iTextSharp.text.Font.NORMAL);
                    var boldFont = FontFactory.GetFont("Arial", 11, iTextSharp.text.Font.BOLD);

                    //var normalFont = FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL);
                    //var boldFont = FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD);

                    //for Annexure A

                    Paragraph annexureA = new Paragraph();  //Paragraph
                   // annexureA.Alignment = Element.ALIGN_CENTER;
                    annexureA.Font = boldFont;
                    GenericLookup Annex = Facade.GetGenericLookupByType(LookupType.AnnexureA);
                    annexureA.Add(Annex.Name);
                    string[] TemplateSplitForAnnexA=null;
                    try
                    {
                      TemplateSplitForAnnexA = TemplateSplitWithTitle[1].Split(new string[] { "[%Bold%]" }, StringSplitOptions.None);
                    }
                    catch
                    {
                    }
                    Paragraph AnnexAParagraph = new Paragraph();
                    AnnexAParagraph.Font = normalFont;
                    AnnexAParagraph.Add(TemplateSplitForAnnexA[0]);
                    Paragraph Compensation = new Paragraph();  //Paragraph
                    Compensation.Alignment = Element.ALIGN_CENTER;
                    Compensation.Font = boldFont;
                    GenericLookup Compensation_Structure = Facade.GetGenericLookupByType(LookupType.Compensation_Structure);
                    Compensation.Add(Compensation_Structure.Name);
                    AnnexAParagraph.Add(new Paragraph(Compensation));
                    string[] Template_CompensationTableSplit = TemplateSplitForAnnexA[1].ToString().Split(new string[] { "[%Table%]" }, StringSplitOptions.None);
                    decimal ctc = 0;
                    PdfPTable compensationTable = CompensationTable(CandOfferDetail.Basic, CandOfferDetail.HouseRent,
                        CandOfferDetail.Conveyance, CandOfferDetail.SpecialAllowance, CandOfferDetail.CompensationPlan,
                        CandOfferDetail.ProvidentFund, CandOfferDetail.VariablePay, out ctc);

                    //*********************Added by pravin khot on 6/Jan/2016***************
                    double AnnualTotal;
                    AnnualTotal = Convert.ToDouble(CandOfferDetail.OfferedSalary);
                    string AnnualTotaltext = string.Empty;
                    int no = Convert.ToInt32(AnnualTotal);
                    string strWords = "";
                    string strWordsonly = "";
                    strWords = strWords + NumberToText(no);
                    strWordsonly = " Only)";
                    strWords = strWords + strWordsonly;
                    AnnualTotaltext = strWords;
                    //*************************END&***********************************
                    AnnualTotaltext = '(' + AnnualTotaltext;
                    text.Replace("[%CTC%]", Convert.ToString(CandOfferDetail.OfferedSalary));
                    text.Replace("[%RsWord%]", Convert.ToString(AnnualTotaltext.Trim() ));

                    //text.Replace("[%CTC%]", Convert.ToString(ctc));
                    Paragraph compensationTablePara = new Paragraph();
                    compensationTablePara.Alignment = Element.ALIGN_CENTER;
                    compensationTablePara.Add(compensationTable);
                    AnnexAParagraph.Add(compensationTablePara);
                    try
                    {
                        AnnexAParagraph.Add(Template_CompensationTableSplit[1]);
                    }
                    catch
                    {
                    }
                    //end annexure A

                    #region Split1
                    string[] TemplateSplitForPoints = TemplateSplitWithTitle[0].Split(new string[] { "[%Bold%]" }, StringSplitOptions.None);
                    Paragraph p;
                    p = new Paragraph();
                    p.Font = normalFont;
                    string strPointsData = string.Empty;

                    strPointsData = TemplateSplitForPoints[0].ToString().Replace("[%CTC%]", Convert.ToString(CandOfferDetail.OfferedSalary));
                    strPointsData = strPointsData.ToString().Replace("[%RsWord%]", Convert.ToString(AnnualTotaltext));
              
                    //strPointsData = TemplateSplitForPoints[0].ToString().Replace("[%CTC%]", Convert.ToString(ctc));
                    p.Add(strPointsData);
                    GenericLookup Probation = Facade.GetGenericLookupByType(LookupType.Probation);
                    p.Add(new Phrase(Probation.Name, boldFont));
                    p.Add(new Phrase(TemplateSplitForPoints[1], normalFont));
                    GenericLookup NoticePeriod = Facade.GetGenericLookupByType(LookupType.Notice_Period);
                    p.Add(new Phrase(NoticePeriod.Name, boldFont));
                    p.Add(new Phrase(TemplateSplitForPoints[2], normalFont));

                    GenericLookup Duties = Facade.GetGenericLookupByType(LookupType.Duties);
                    p.Add(new Phrase(Duties.Name, boldFont));
                    p.Add(new Phrase(TemplateSplitForPoints[3], normalFont));

                    GenericLookup Training = Facade.GetGenericLookupByType(LookupType.Training);
                    p.Add(new Phrase(Training.Name, boldFont));
                    p.Add(new Phrase(TemplateSplitForPoints[4], normalFont));

                    GenericLookup Confidentiality_Fidelity = Facade.GetGenericLookupByType(LookupType.Confidentiality_Fidelity);
                    p.Add(new Phrase(Confidentiality_Fidelity.Name, boldFont));
                    p.Add(new Phrase(TemplateSplitForPoints[5], normalFont)); // commented by pravin khot on 11/Jan/2016
                    #endregion split1

                    #region or Annexure B
                    Paragraph annexureB = new Paragraph();  //Paragraph
                    annexureB.Alignment = Element.ALIGN_CENTER;
                    annexureB.Font = boldFont;
                    GenericLookup AnnexureB = Facade.GetGenericLookupByType(LookupType.AnnexureB);
                    annexureB.Add(AnnexureB.Name);

                    string[] TemplateSplitForAnnexB = TemplateSplitWithTitle[2].Split(new string[] { "[%Bold%]" }, StringSplitOptions.None);
                    Paragraph AnnexBParagraph = new Paragraph();
                    AnnexBParagraph.Font = normalFont;
                    GenericLookup FCP = Facade.GetGenericLookupByType(LookupType.FCP);
                    AnnexBParagraph.Add(new Phrase(FCP.Name, boldFont));
                    string[] Template_AnnexBSplit = TemplateSplitForAnnexB[1].ToString().Split(new string[] { "[%Table%]" }, StringSplitOptions.None);
                    AnnexBParagraph.Add(Template_AnnexBSplit[0]);

                    PdfPTable flexiTable = null;
                    if (CandOfferDetail.Grade == "SGG7" || CandOfferDetail.Grade == "SGG8")
                    {
                        IList<GenericLookup> flexiTable1 = Facade.GetAllGenericLookupByLookupType(LookupType.SM1FlexiTable);
                        flexiTable = FlexibleCompensationTable(flexiTable1);
                    }
                    if (CandOfferDetail.Grade == "SGG5")
                    {
                        IList<GenericLookup> flexiTable1 = Facade.GetAllGenericLookupByLookupType(LookupType.SGG5FlexiTable);
                        flexiTable = FlexibleCompensationTable(flexiTable1);
                    }
                    if (CandOfferDetail.Grade == "SGG4a")
                    {
                        IList<GenericLookup> flexiTable1 = Facade.GetAllGenericLookupByLookupType(LookupType.SGG4aFlexiTable);
                        flexiTable = FlexibleCompensationTable(flexiTable1);
                    }
                    if (CandOfferDetail.Grade == "SGG2a" || CandOfferDetail.Grade == "SGG3" ||
                        CandOfferDetail.Grade == "SGG3a" || CandOfferDetail.Grade == "SGG4")
                    {
                        IList<GenericLookup> flexiTable1 = Facade.GetAllGenericLookupByLookupType(LookupType.SGG2FlexiTable);
                        flexiTable = FlexibleCompensationTable(flexiTable1);
                    }
                    if (CandOfferDetail.Grade == "SGG5a")
                    {
                        IList<GenericLookup> flexiTable1 = Facade.GetAllGenericLookupByLookupType(LookupType.SGG5aFlexiTable);
                        flexiTable = FlexibleCompensationTable(flexiTable1);
                    }
                    if (CandOfferDetail.Grade == "SGG6")
                    {
                        IList<GenericLookup> flexiTable1 = Facade.GetAllGenericLookupByLookupType(LookupType.SGG6FlexiTable);
                        flexiTable = FlexibleCompensationTable(flexiTable1);
                    }
                    Paragraph flextablePara = new Paragraph();
                    flextablePara.Alignment = Element.ALIGN_CENTER;
                    flextablePara.Add(flexiTable);
                    AnnexBParagraph.Add(flextablePara);
                    AnnexBParagraph.Add(Template_AnnexBSplit[1]);
                    #endregion for Annexure B


                    #region for Annexure C
                    Paragraph annexureC = new Paragraph();  //Paragraph
                    annexureC.Alignment = Element.ALIGN_CENTER;
                    annexureC.Font = boldFont;
                    GenericLookup AnnexureC = Facade.GetGenericLookupByType(LookupType.AnnexureC);
                    annexureC.Add(AnnexureC.Name);

                    string[] TemplateSplitForAnnexC = TemplateSplitWithTitle[3].Split(new string[] { "[%Bold%]" }, StringSplitOptions.None);
                    Paragraph AnnexCParagraph = new Paragraph();
                    AnnexCParagraph.Font = normalFont;
                    GenericLookup VariablePay = Facade.GetGenericLookupByType(LookupType.VariablePay);
                    AnnexCParagraph.Add(new Phrase(VariablePay.Name, boldFont));
                    string[] Template_AnnexCSplit = TemplateSplitForAnnexC[1].ToString().Split(new string[] { "[%Table%]" }, StringSplitOptions.None);
                    AnnexCParagraph.Add(Template_AnnexCSplit[0]);
                    //PdfPTable varPayTable = VariablePayTable();
                    //Paragraph para_varPayTable = new Paragraph();
                    //para_varPayTable.Alignment = Element.ALIGN_CENTER;
                    //para_varPayTable.Add(varPayTable);
                    //AnnexCParagraph.Add(para_varPayTable);
                    //AnnexCParagraph.Add(Template_AnnexCSplit[1]);  // commented by pravin khot on 11/Jan/2016
                    #endregion annexure C

                    #region Annexure D
                    Paragraph annexureD = new Paragraph();  //Paragraph
                    annexureD.Alignment = Element.ALIGN_CENTER;
                    annexureD.Font = boldFont;
                    GenericLookup AnnexureD = Facade.GetGenericLookupByType(LookupType.AnnexureD);
                    annexureD.Add(AnnexureD.Name);

                    string[] TemplateSplitForAnnexD = TemplateSplitWithTitle[4].Split(new string[] { "[%Bold%]" }, StringSplitOptions.None);
                    Paragraph AnnexDParagraph = new Paragraph();
                    AnnexDParagraph.Font = normalFont;
                    AnnexDParagraph.Add(TemplateSplitForAnnexD[0]);
                    GenericLookup listDocument = Facade.GetGenericLookupByType(LookupType.ListofDocumentsToBeSsubmittedToHR);
                    AnnexDParagraph.Add(new Phrase(listDocument.Name, boldFont));
                    AnnexDParagraph.Add(TemplateSplitForAnnexD[1]);
                    #endregion annexure D

                    #region Annexure E
                    Paragraph annexureE = new Paragraph();  //Paragraph
                    annexureE.Alignment = Element.ALIGN_CENTER;
                    annexureE.Font = boldFont;
                    GenericLookup AnnexureE = Facade.GetGenericLookupByType(LookupType.AnnexureE);
                    annexureE.Add(AnnexureE.Name);

                    string[] TemplateSplitForAnnexE = TemplateSplitWithTitle[5].Split(new string[] { "[%Bold%]" }, StringSplitOptions.None);

                    Paragraph AnnexEParagraph = new Paragraph();
                    AnnexEParagraph.Font = normalFont;
                    AnnexEParagraph.Add(TemplateSplitForAnnexE[0]);
                    GenericLookup Note = Facade.GetGenericLookupByType(LookupType.Note);
                    AnnexEParagraph.Add(new Phrase(Note.Name, boldFont));
                    //AnnexEParagraph.Add(TemplateSplitForAnnexE[1]);// commented by pravin khot on 11/Jan/2016
                    //AnnexEParagraph.Add(TemplateSplitForAnnexE[0]);// added by pravin khot on 11/Jan/2016
                    #endregion annexure E

                    #region Footer

                    PDFFooter pdfFooter = new PDFFooter();
                    var page = new PDFFooter();
                    writer.PageEvent = page;
                    doc.Open();
                    doc.Add(new Paragraph(p));
                    doc.Add(annexureA);
                    doc.Add(new Paragraph(AnnexAParagraph));
                    doc.Add(new Paragraph(annexureB));
                    doc.Add(new Paragraph(AnnexBParagraph));
                    doc.Add(new Paragraph(annexureC));
                    doc.Add(new Paragraph(AnnexCParagraph));
                    doc.Add(new Paragraph(annexureD));
                    doc.Add(new Paragraph(AnnexDParagraph));
                    doc.Add(new Paragraph(annexureE));
                    doc.Add(new Paragraph(AnnexEParagraph));
                    doc.Close();
                }
                #endregion Footer
                #endregion
            }
            if (CandDetail.DemandType.ToLower() == "contract")
            {

                #region ContractHire
                var boldFont = FontFactory.GetFont("Garamond", 11, iTextSharp.text.Font.BOLD);
                var font = FontFactory.GetFont("Garamond", 11, iTextSharp.text.Font.NORMAL);
                //consultant info
                Company vendorDetails = Facade.GetVendorByCandidateId(Convert.ToInt32(candidateId));
                if (vendorDetails != null)
                {
                    filename = "Offer_LetterTemplate-" + vendorDetails.CompanyName + ".pdf";
                }
                else
                {
                    filename = "Offer_LetterTemplate.pdf";
                }
                savedPdfPath = savedPdfPath + filename;
                if (File.Exists(savedPdfPath))
                {
                    File.Delete(savedPdfPath);
                }
                using (FileStream fs = new FileStream(savedPdfPath, FileMode.Create, FileAccess.Write))
                {

                    //FileStream fs = new FileStream("PDFGeneratorSample.pdf", FileMode.Create, FileAccess.Write, FileShare.None);
                    Document doc = new Document(PageSize.A4, 36, 36, 36, 120);
                    PdfWriter writer = PdfWriter.GetInstance(doc, fs);
                    string sDate = string.Empty, eDate = string.Empty;
                    //pdf path
                    GenericLookup contractDetail = Facade.GetGenericLookupByType(LookupType.ContractTemplatePath);
                    templateFilePath = Server.MapPath(contractDetail.Name); // @"D:\Suraj\Documents\documents\SISCPL-WO Template.pdf";

                    //read pdf
                    using (PdfReader reader = new PdfReader(templateFilePath))
                    {
                        for (int i = 1; i <= reader.NumberOfPages; i++)
                        {
                            text.Append(PdfTextExtractor.GetTextFromPage(reader, i));

                        }
                    }
                    //replace data with actual values
                    if (vendorDetails != null)
                    {
                        text.Replace("[%Consultant_Name%]", vendorDetails.CompanyName);
                        text.Replace("[%Vendor_Name%]", vendorDetails.CompanyName);
                         sDate = (vendorDetails.ContractStartDate == DateTime.MinValue) ? string.Empty : Convert.ToString(vendorDetails.ContractStartDate.ToShortDateString());
                        text.Replace("[%Start_Date%]", sDate);
                         eDate = (vendorDetails.ContractEndDate == DateTime.MinValue) ? string.Empty : Convert.ToString(vendorDetails.ContractEndDate.ToShortDateString());
                        text.Replace("[%End_Date%]", eDate);
                        text.Replace("[%Charges%]", Convert.ToString(vendorDetails.ServiceFee));
                        text.Replace("[%Address%]", vendorDetails.Address1 + "\n" + vendorDetails.Address2 + "\n" + vendorDetails.City + "\n" + vendorDetails.Zip);
                        text.Replace("[%Contact_Number%]", Convert.ToString(vendorDetails.OfficePhone));
                    }
                    else
                    {
                        text.Replace("[%Vendor_Name%]", string.Empty);
                        text.Replace("[%Consultant_Name%]", string.Empty);
                        text.Replace("[%Start_Date%]", string.Empty);
                        text.Replace("[%End_Date%]", string.Empty);
                        text.Replace("[%Charges%]", string.Empty);
                        text.Replace("[%Address%]", string.Empty);
                        text.Replace("[%Contact_Number%]", string.Empty);
                    }

                    #region table
                    //split data according 
                    string[] templateSplitData = text.ToString().Split(new string[] { "[%Bold%]" }, StringSplitOptions.None);
                    string[] tableSplitData = templateSplitData[2].ToString().Split(new string[] { "[%Table%]" }, StringSplitOptions.None);

                    //for creating and adding data into pdf
                    PdfPTable table = new PdfPTable(4);
                    table.TotalWidth = 400f;
                    //fix the absolute width of the table
                    table.LockedWidth = true;

                    //relative col widths in proportions - 1/3 and 2/3
                    float[] widths = new float[] { 6f, 2f, 2f, 2f };
                    table.SetWidths(widths);
                    table.HorizontalAlignment = 0;
                    //leave a gap before and after the table
                    table.SpacingBefore = 20f;
                    table.SpacingAfter = 25f;

                    IList<GenericLookup> contractTableTitle = Facade.GetAllGenericLookupByLookupType(LookupType.ContractTableTitle);
                    for (int i = 0; i < contractTableTitle.Count; i++)
                    {
                        table.AddCell(new Phrase(contractTableTitle[i].Name, boldFont));
                        table.AddCell(new Phrase(contractTableTitle[i].Description, boldFont));
                    }

                    if (vendorDetails != null)
                    {
                        table.AddCell(vendorDetails.CompanyName);
                        table.AddCell(Convert.ToString(sDate));
                        table.AddCell(Convert.ToString(eDate));
                        table.AddCell(Convert.ToString(vendorDetails.ServiceFee));
                    }
                    else
                    {
                        PdfPCell cell = new PdfPCell(new Phrase("No Details found"));
                        cell.Colspan = 4;
                        cell.HorizontalAlignment = 1;
                        table.AddCell(cell);
                    }
                    #endregion
                    #region documentWrite
                    doc.Open();
                    doc.Add(new Paragraph(templateSplitData[0].ToString(), font));
                    GenericLookup Contract_Sub = Facade.GetGenericLookupByType(LookupType.Contract_Sub);
                    doc.Add(new Paragraph(Contract_Sub.Name, boldFont));
                    doc.Add(new Paragraph(templateSplitData[1].ToString(), font));
                    GenericLookup KindAtt = Facade.GetGenericLookupByType(LookupType.Contract_KIND_ATTENTION);
                    doc.Add(new Paragraph(KindAtt.Name, boldFont));
                    doc.Add(new Paragraph(tableSplitData[0].ToString(), font));
                    doc.Add(table);
                    doc.Add(new Paragraph(tableSplitData[1].ToString(), font));
                    GenericLookup Contract_TermsConditions = Facade.GetGenericLookupByType(LookupType.Contract_TermsConditions);
                    doc.Add(new Paragraph(Contract_TermsConditions.Name, boldFont));
                    doc.Add(new Paragraph(templateSplitData[3].ToString()));
                    GenericLookup Location = Facade.GetGenericLookupByType(LookupType.Location);
                    doc.Add(new Paragraph(Location.Name, boldFont));
                    doc.Add(new Paragraph(templateSplitData[4].ToString()));
                    GenericLookup Termination = Facade.GetGenericLookupByType(LookupType.Termination);
                    doc.Add(new Paragraph(Termination.Name, boldFont));
                    doc.Add(new Paragraph(templateSplitData[5].ToString()));
                    GenericLookup travel = Facade.GetGenericLookupByType(LookupType.Travel_Accommodation_Living_expenses);
                    doc.Add(new Paragraph(travel.Name, boldFont));
                    doc.Add(new Paragraph(templateSplitData[6].ToString(), font));

                    GenericLookup Calendar = Facade.GetGenericLookupByType(LookupType.Calendar);
                    doc.Add(new Paragraph(Calendar.Name, boldFont));
                    doc.Add(new Paragraph(templateSplitData[7].ToString(), font));

                    GenericLookup Billing = Facade.GetGenericLookupByType(LookupType.Billing);
                    doc.Add(new Paragraph(Billing.Name, boldFont));
                    doc.Add(new Paragraph(templateSplitData[8].ToString(), font));

                    GenericLookup Timesheet = Facade.GetGenericLookupByType(LookupType.Timesheet);
                    doc.Add(new Paragraph(Timesheet.Name, boldFont));
                    doc.Add(new Paragraph(templateSplitData[9].ToString(), font));

                    GenericLookup Taxes = Facade.GetGenericLookupByType(LookupType.Taxes);
                    doc.Add(new Paragraph(Taxes.Name, boldFont));
                    doc.Add(new Paragraph(templateSplitData[10].ToString(), font));

                    GenericLookup Non_Disclosure_Agreement = Facade.GetGenericLookupByType(LookupType.Non_Disclosure_Agreement);
                    doc.Add(new Paragraph(Non_Disclosure_Agreement.Name, boldFont));
                    doc.Add(new Paragraph(templateSplitData[11].ToString(), font));

                    GenericLookup Background_Verification = Facade.GetGenericLookupByType(LookupType.Background_Verification);
                    doc.Add(new Paragraph(Background_Verification.Name, boldFont));
                    doc.Add(new Paragraph(templateSplitData[12].ToString(), font));

                    GenericLookup Trial_Period = Facade.GetGenericLookupByType(LookupType.Trial_Period);
                    doc.Add(new Paragraph(Trial_Period.Name, boldFont));
                    doc.Add(new Paragraph(templateSplitData[13].ToString(), font));

                    GenericLookup ShiftAllowance = Facade.GetGenericLookupByType(LookupType.ShiftAllowance);
                    doc.Add(new Paragraph(ShiftAllowance.Name, boldFont));
                    doc.Add(new Paragraph(templateSplitData[14].ToString(), font));

                    GenericLookup YoursSincerely = Facade.GetGenericLookupByType(LookupType.YoursSincerely);
                    doc.Add(new Paragraph(YoursSincerely.Name, boldFont));
                    doc.Add(new Paragraph(templateSplitData[15].ToString(), font));

                    GenericLookup Sony = Facade.GetGenericLookupByType(LookupType.ForSonyIndiaSoftwareCentrePvtLtd);
                    doc.Add(new Paragraph(Sony.Name, boldFont));
                    doc.Add(new Paragraph(templateSplitData[16].ToString(), font));

                    GenericLookup AuthorizedSignatory = Facade.GetGenericLookupByType(LookupType.AuthorizedSignatory);
                    doc.Add(new Paragraph(AuthorizedSignatory.Name, boldFont));
                    doc.Add(new Paragraph(templateSplitData[17].ToString(), font));

                    GenericLookup Accepted = Facade.GetGenericLookupByType(LookupType.Accepted);
                    doc.Add(new Paragraph(Accepted.Name, boldFont));
                    doc.Add(new Paragraph(templateSplitData[18].ToString(), font));

                    GenericLookup For = Facade.GetGenericLookupByType(LookupType.For);
                    doc.Add(new Paragraph(For.Name, boldFont));
                    doc.Add(new Paragraph(templateSplitData[19].ToString(), font));

                    GenericLookup AuthorizedSign = Facade.GetGenericLookupByType(LookupType.AuthorizedSignatory);
                    doc.Add(new Paragraph(AuthorizedSign.Name, boldFont));
                    doc.Add(new Paragraph(templateSplitData[20].ToString(), font));
                    doc.Close();
                }
                #endregion
                #endregion
            }
            #region Download PDF
            return filename;
            #endregion

            /*******End*******Following Code is for PDF Generation*********Suraj adsule**********20160822*/
        }
        catch (Exception e)
        {
            return "";
        }
    }

    //FUNCTION added by pravin khot on 6/Jan/2017*******
    public static string NumberToText(int n)
    {
        int flag = 0;
        if ((n % 100000) > 0)
        {
            flag = 1;
        }
        if (n < 0)
            return "Minus " + NumberToText(-n);
        else if (n == 0)
            return "";
        else if (n <= 19)
            return new string[] {"One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", 
         "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", 
         "Seventeen", "Eighteen", "Nineteen"}[n - 1] + " ";
        else if (n <= 99)
            return new string[] {"Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", 
         "Eighty", "Ninety"}[n / 10 - 2] + " " + NumberToText(n % 10);
        else if (n <= 199)
            return "One Hundred " + NumberToText(n % 100);
        else if (n <= 999)
            return NumberToText(n / 100) + "Hundred " + NumberToText(n % 100);
        else if (n <= 1999)
            return "One Thousand " + NumberToText(n % 1000);
        else if (n <= 99999)
            return NumberToText(n / 1000) + "Thousand " + NumberToText(n % 1000);
        else if (n <= 199999)
            //*****Code modify and added by pravin khot on 22/March/2016***********
            return "One Lakh " + NumberToText(n % 100000);
        else
            if (flag==1)
                return NumberToText(n / 100000) + "Lakh " + NumberToText(n % 100000);
            else
                return NumberToText(n / 100000) + "Lakh's " + NumberToText(n % 100000);

        //else if (n <= 1999999)
        //    return "One Million " + NumberToText(n % 1000000);
        //else if (n <= 999999999)
        //    return NumberToText(n / 1000000) + "Millions " + NumberToText(n % 1000000);
        //else if (n <= 1999999999)
        //    return "One Billion " + NumberToText(n % 1000000000);
        //else
        //    return NumberToText(n / 1000000000) + "Billions " + NumberToText(n % 1000000000);
        //*******************END CODE*************************************************
    }
    //******************END******************************************      

    private PdfPTable CompensationTable(decimal basic, decimal houseRent, decimal transport, decimal special, decimal compensation, decimal pf, decimal variable, out decimal ctc)
    {
        #region Compensation Structure Table
        var boldFont = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 12);
        //for creating and adding data into pdf
        PdfPTable table = new PdfPTable(2);
        table.TotalWidth = 400f;
        //fix the absolute width of the table
        table.LockedWidth = true;

        //relative col widths in proportions - 1/3 and 2/3
        float[] widths = new float[] { 7f, 5f };
        table.SetWidths(widths);
        table.HorizontalAlignment = 0;
        //leave a gap before and after the table
        table.SpacingBefore = 20f;
        table.SpacingAfter = 25f;

        //Add Headers
        GenericLookup Component = Facade.GetGenericLookupByType(LookupType.Component);
        PdfPCell cell = new PdfPCell(new Phrase(Component.Name, boldFont));
        cell.BackgroundColor = BaseColor.GRAY;
        table.AddCell(cell);

        GenericLookup AmountInRS = Facade.GetGenericLookupByType(LookupType.AmountInRS);
        cell = new PdfPCell(new Phrase(AmountInRS.Name, boldFont));
        cell.BackgroundColor = BaseColor.GRAY;
        table.AddCell(cell);

        //Sub Title
        GenericLookup MonthlyEntitlements = Facade.GetGenericLookupByType(LookupType.MonthlyEntitlements);
        cell = new PdfPCell(new Phrase(MonthlyEntitlements.Name, boldFont));
        cell.Colspan = 2;
        cell.HorizontalAlignment = 1;       
        table.AddCell(cell);

        ////For Basic
        GenericLookup Basic = Facade.GetGenericLookupByType(LookupType.Basic);
        table.AddCell(Basic.Name);
        table.AddCell(Convert.ToString(Math.Round(basic, 2)));

        ////For Basic
        //GenericLookup Basic = Facade.GetGenericLookupByType(LookupType.Basic);
        //table.AddCell(Basic.Name);
        //cell = new PdfPCell(new Phrase(Convert.ToString(Math.Round(basic, 2)), boldFont));
        //cell.HorizontalAlignment = PdfPCell.ALIGN_JUSTIFIED; 
        //cell.VerticalAlignment = PdfPCell.ALIGN_JUSTIFIED;
        //table.AddCell(cell);

        //House Rent Allowances
        GenericLookup houseRentAllow = Facade.GetGenericLookupByType(LookupType.HouseRentAllowance);
        table.AddCell(houseRentAllow.Name);
        table.AddCell(Convert.ToString(Math.Round(houseRent, 2)));

        //Transport Allowances
        //GenericLookup TransportAllowance = Facade.GetGenericLookupByType(LookupType.TransportAllowance);
        //table.AddCell(TransportAllowance.Name);
        //table.AddCell(Convert.ToString(Math.Round(transport, 2)));

        //Special Allowances
        GenericLookup SpecialAllowance = Facade.GetGenericLookupByType(LookupType.SpecialAllowance);
        table.AddCell(SpecialAllowance.Name);
        table.AddCell(Convert.ToString(Math.Round(special, 2)));

        decimal totalPerAnnumA = basic + houseRent + special;
        decimal totalPerMonth = totalPerAnnumA / 12;

        //Total Per Month
        GenericLookup TotalPerMonth = Facade.GetGenericLookupByType(LookupType.TotalPerMonth);
        cell = new PdfPCell(new Phrase(TotalPerMonth.Name, boldFont));
        cell.BackgroundColor = BaseColor.GRAY;
        table.AddCell(cell);

        cell = new PdfPCell(new Phrase(Convert.ToString(Math.Round(totalPerMonth, 2))));
        cell.BackgroundColor = BaseColor.GRAY;
        table.AddCell(cell);

        //Total Per Annum (A) 
        GenericLookup TotalPerAnnumA = Facade.GetGenericLookupByType(LookupType.TotalPerAnnumA);
        cell = new PdfPCell(new Phrase(TotalPerAnnumA.Name, boldFont));
        cell.BackgroundColor = BaseColor.GRAY;
        table.AddCell(cell);

        cell = new PdfPCell(new Phrase(Convert.ToString(Math.Round(totalPerAnnumA, 2))));
        cell.BackgroundColor = BaseColor.GRAY;
        table.AddCell(cell);

        //Annual Entitlements 
        GenericLookup AnnualEnt = Facade.GetGenericLookupByType(LookupType.AnnualEntitlements);
        cell = new PdfPCell(new Phrase(AnnualEnt.Name, boldFont));
        cell.Colspan = 2;
        cell.HorizontalAlignment = 1;
        table.AddCell(cell);

        //For Flexible Compensation Plan (FCP) 
        GenericLookup FCP = Facade.GetGenericLookupByType(LookupType.FCP);
        table.AddCell(FCP.Name);
        table.AddCell(Convert.ToString(Math.Round(compensation, 2)));

        //Provident Fund (PF) 
        GenericLookup PF = Facade.GetGenericLookupByType(LookupType.PF);
        table.AddCell(PF.Name);
        table.AddCell(Convert.ToString(Math.Round(pf, 2)));

        decimal totalPerAnnumB = compensation + pf;
        Math.Round(totalPerAnnumB, 2);

        //Total Per Annum (B) 
        GenericLookup TotalPerAnnumB = Facade.GetGenericLookupByType(LookupType.TotalPerAnnumB);
        cell = new PdfPCell(new Phrase(TotalPerAnnumB.Name, boldFont));
        cell.BackgroundColor = BaseColor.GRAY;
        table.AddCell(cell);
        cell = new PdfPCell(new Phrase(Convert.ToString(totalPerAnnumB)));
        cell.BackgroundColor = BaseColor.GRAY;
        table.AddCell(cell);



        //Total Gross (A+B) 
        decimal AB = totalPerAnnumA + totalPerAnnumB;
        Math.Round(AB, 2);
        GenericLookup TotalGross = Facade.GetGenericLookupByType(LookupType.TotalGross);
        cell = new PdfPCell(new Phrase(TotalGross.Name, boldFont));
        cell.BackgroundColor = BaseColor.GRAY;
        table.AddCell(cell);
        cell = new PdfPCell(new Phrase(Convert.ToString(AB)));
        cell.BackgroundColor = BaseColor.GRAY;
        table.AddCell(cell);

        //Annual Variable Pay Baseline (C) **  
        GenericLookup AnnualVariablePay = Facade.GetGenericLookupByType(LookupType.AnnualVariablePay);
        cell = new PdfPCell(new Phrase(AnnualVariablePay.Name, boldFont));
        table.AddCell(cell);
        Math.Round(variable, 2);
        table.AddCell(Convert.ToString(variable));

        //Total Annual CTC (A+B+C)  
        ctc = totalPerAnnumA + totalPerAnnumB + variable;
        GenericLookup TotalAnnualCTC = Facade.GetGenericLookupByType(LookupType.TotalAnnualCTC);
        cell = new PdfPCell(new Phrase(TotalAnnualCTC.Name, boldFont));
        cell.BackgroundColor = BaseColor.GRAY;
        table.AddCell(cell);
        cell = new PdfPCell(new Phrase(Convert.ToString(Math.Round(ctc, 2))));
        cell.BackgroundColor = BaseColor.GRAY;
        table.AddCell(cell);

        #endregion
        return table;
    }
    
    private PdfPTable FlexibleCompensationTable(IList<GenericLookup> flexiTable)//string grade, string foodVoucher, string booksAndProffesionalDev, string medical, string telephone, string lta, string carLease
    {
        #region Flexible Compensation Table
        var normalFont = FontFactory.GetFont("Arial", 11, iTextSharp.text.Font.NORMAL);
        var boldFont = FontFactory.GetFont("Arial", 11, iTextSharp.text.Font.BOLD);
        //for creating and adding data into pdf
        PdfPTable table = new PdfPTable(2);
        table.TotalWidth = 400f;
        //fix the absolute width of the table
        table.LockedWidth = true;

        //relative col widths in proportions - 1/3 and 2/3
        float[] widths = new float[] { 7f, 5f };
        table.SetWidths(widths);
        table.HorizontalAlignment = 0;
        //leave a gap before and after the table
        table.SpacingBefore = 20f;
        table.SpacingAfter = 25f;


        //Add Headers
        GenericLookup flexiTitle = Facade.GetGenericLookupByType(LookupType.flexiTableHeader);
        table.AddCell(new Phrase(flexiTitle.Name, boldFont));
        table.AddCell(new Phrase(flexiTitle.Description, boldFont));

        //table data
        for (int i = 0; i < flexiTable.Count; i++)
        {
            table.AddCell(new Phrase(flexiTable[i].Name, normalFont));
            table.AddCell(new Phrase(flexiTable[i].Description, normalFont));
        }
        #endregion
        return table;
    }

    private PdfPTable VariablePayTable()
    {
        var normalFont = FontFactory.GetFont("Arial", 11, iTextSharp.text.Font.NORMAL);
        var boldFont = FontFactory.GetFont("Arial", 11, iTextSharp.text.Font.BOLD);
        #region Variable Pay Table
        //for creating and adding data into pdf
        PdfPTable table = new PdfPTable(4);
        table.TotalWidth = 400f;
        //fix the absolute width of the table
        table.LockedWidth = true;

        //relative col widths in proportions - 1/3 and 2/3
        float[] widths = new float[] { 6f, 2f, 4f, 3f };
        table.SetWidths(widths);
        table.HorizontalAlignment = 0;
        //leave a gap before and after the table
        table.SpacingBefore = 20f;
        table.SpacingAfter = 25f;

        // Title
        GenericLookup flexiTitle = Facade.GetGenericLookupByType(LookupType.VariablePayTableTitle1);
        PdfPCell cell = new PdfPCell(new Phrase(flexiTitle.Name, boldFont));
        cell.Colspan = 2;
        cell.HorizontalAlignment = 1;
        table.AddCell(cell);

        PdfPCell cell1 = new PdfPCell(new Phrase(flexiTitle.Description, boldFont));
        cell1.Colspan = 2;
        cell1.HorizontalAlignment = 1;
        table.AddCell(cell1);

        //For Sub Title
        IList<GenericLookup> varTableSubTitle = Facade.GetAllGenericLookupByLookupType(LookupType.VariablePayTableTitle2);
        for (int i = 0; i < varTableSubTitle.Count; i++)
        {
            cell = new PdfPCell(new Phrase(varTableSubTitle[i].Name, boldFont));
            table.AddCell(cell);
            cell = new PdfPCell(new Phrase(varTableSubTitle[i].Description, boldFont));
            table.AddCell(cell);
        }

        //Table data
        IList<GenericLookup> varTable = Facade.GetAllGenericLookupByLookupType(LookupType.VariablePayTable);
        for (int i = 0; i < varTable.Count; i++)
        {
            if (varTable[i].SortOrder == 2)
            {
                cell = new PdfPCell(new Phrase(varTable[i].Name, normalFont));
                cell.Rowspan = 6;
                table.AddCell(cell);


                cell = new PdfPCell(new Phrase(varTable[i].Description, normalFont));
                cell.Rowspan = 6;
                table.AddCell(cell);
            }
            else if (varTable[i].SortOrder == 9)
            {
                cell = new PdfPCell(new Phrase(varTable[i].Name, normalFont));
                cell.Rowspan = 2;
                table.AddCell(cell);


                cell = new PdfPCell(new Phrase(varTable[i].Description, normalFont));
                cell.Rowspan = 2;
                table.AddCell(cell);
            }
            else
            {
                table.AddCell(new Phrase(varTable[i].Name, normalFont));
                table.AddCell(new Phrase(varTable[i].Description, normalFont));
            }
        }
        #endregion
        return table;
    }

    private void PlaceUpDownArrow()
    {
        try
        {
            LinkButton lnk = (LinkButton)lsvOfferList.FindControl(txtSortColumn.Text);
            HtmlTableCell im = (HtmlTableCell)lnk.Parent;
            im.EnableViewState = false;
            im.Attributes.Add("class", (txtSortOrder.Text == "ASC" ? "Ascending" : "Descending"));
        }
        catch
        {
        }

    }

    protected void lsvOfferList_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
        {
            //JobPosting jobPosting = ((ListViewDataItem)e.Item).DataItem as JobPosting;
            MemberHiringDetails memberHiringDetails = ((ListViewDataItem)e.Item).DataItem as MemberHiringDetails;
            int ColLocation = 0;
            string ststus = Convert.ToString(odsOfferList.SelectParameters["status"].DefaultValue);
            if (memberHiringDetails != null)
            {
                string strDemandValue = Facade.GetDemandTypeValue(memberHiringDetails.Id, memberHiringDetails.JobPostingId);
                if (strDemandValue == "Permanent")
                {
                    Label lblcandidatename = (Label)e.Item.FindControl("lblCandidateName");
                    Label lblJobTitle = (Label)e.Item.FindControl("lblJobTitle");
                    Label lblGrade = (Label)e.Item.FindControl("lblGrade");
                    Label lblCreatedOn = (Label)e.Item.FindControl("lblCreatedOn");
                    Label lblOfferCreated = (Label)e.Item.FindControl("lblOfferCreated");
                    Label lblStage = (Label)e.Item.FindControl("lblStage");
                    LinkButton lnkReqApprove = (LinkButton)e.Item.FindControl("hlkReqApprove");
                    //HyperLink lnkReqReject = (HyperLink)e.Item.FindControl("hlkReqReject");
                    HyperLink lnkJobTitle = (HyperLink)e.Item.FindControl("lnkJobTitle");
                    TableCell tcCell = (TableCell)e.Item.FindControl("thAction");

                    LinkButton lnkGenerateOfferLetter = (LinkButton)e.Item.FindControl("lnkGenerateOfferLetter");
                    lnkGenerateOfferLetter.CommandArgument =memberHiringDetails.CandidateId.ToString();
                    if (lnkGenerateOfferLetter != null)
                    {
                        ScriptManager.GetCurrent(Page).RegisterPostBackControl(lnkGenerateOfferLetter);
                    }

                    Label lblTemplate = (Label)e.Item.FindControl("lblTemplate");
                    Label lblAction = (Label)e.Item.FindControl("lblAction");
                    //SecureUrl urlEditJob = UrlHelper.BuildSecureUrl("../" + (UrlConstants.Requisition.MEMBER_OFFER_VIEW_PAGE.Replace("~/", "").Replace("//", "/")), string.Empty, memberHiringDetails.Id.ToString(), StringHelper.Convert(memberHiringDetails.Id), UrlConstants.PARAM_SITEMAP_PARENT_ID, "13", UrlConstants.PARAM_JOB_DISABLE, "true");
                    
                    lblcandidatename.Text = Convert.ToString(memberHiringDetails.ApplicantName);
                    lblJobTitle.Text = Convert.ToString(memberHiringDetails.JobTitle);
                    if (memberHiringDetails.Grade == "Please Select")
                    {
                        lblGrade.Text = "";
                    }
                    else
                    {
                        lblGrade.Text = Convert.ToString(memberHiringDetails.Grade);
                    }
                  
                    lblCreatedOn.Text = (memberHiringDetails.OfferedDate== null) ? string.Empty : Convert.ToString(memberHiringDetails.OfferedDate.ToShortDateString());
                    lblOfferCreated.Text = Convert.ToString(memberHiringDetails.OfferedSalary);

                    lnkReqApprove.Visible = false ;
                    if (memberHiringDetails.ApprovalType == 4)
                    {
                        lnkReqApprove.Visible = true;
                        lblStage.Text = "Offer Approved by Division Head";
                    }
                    else if (memberHiringDetails.ApprovalType == 3)
                    {
                        if (memberHiringDetails.IsApprove == true)
                        {
                            lblStage.Text = "Offer Approved by HR Head";
                        }
                        else
                        {
                            lblStage.Text = "Offer Rejected by HR Head";
                        }
                    }

                    else if (memberHiringDetails.ApprovalType == 2)
                    {
                        if (memberHiringDetails.IsApprove == true)
                        {
                            lblStage.Text = "Offer Approved by Recruitment Manager";
                        }
                        else
                        {
                            lblStage.Text = "Offer Rejected by Recruitment Manager";
                        }
                    }
                    else
                    {
                        lnkReqApprove.Visible = false;
                        lblStage.Text = "Offer Submitted by Recruiter";
                    }

                    //lnkReqApprove.Attributes.Add("onclick", "EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/OfferApproval1.aspx?MID=" + memberHiringDetails.Id + "&STATUS=Offer Approved by Recruiter Manager&ID=" + CurrentMember.Id + "','700px','350px'); return false;");
                    //lnkReqReject.Attributes.Add("onclick", "var input=confirm('Are you sure want to send back the offer?'); if (input==true) { EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/OfferApproval1.aspx?MID=" + memberHiringDetails.Id + "&STATUS=Offer Rejected by Recruiter Manager&ID=" + CurrentMember.Id + "','700px','350px');} return false;");
                    //lnkJobTitle.Attributes.Add("onclick", "window.open('" + urlEditJob.ToString() + "','View');");

                    int Id = CurrentMember.Id;
                  
                    //lnkReqReject.Visible = false;
                    lnkJobTitle.Attributes.Add("onclick", "EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/ViewOffer.aspx?MID=" + CurrentMember.Id + "&ID=" + memberHiringDetails.Id + "','820px','600px'); return false;");
                    //lnkReqReject.Attributes.Add("onclick", "EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/CommonHiringDetails.aspx?StatusId=" + statusId + "&JID=" + memberHiringDetails.JobPostingId + "&Canid=" + memberHiringDetails.CandidateId + "','900px','700px'); return false;");
                    lnkJobTitle.Text = "View";
                   // string strPath = FileDownload(Convert.ToString(memberHiringDetails.CandidateId), Convert.ToString(memberHiringDetails.Id));
                    //strPath = "../Templates/Download File/" + strPath;

                    //lnkReqApprove.Attributes.Add("href", strPath);
                    //lnkReqApprove.Attributes.Add("target", "_blank");

                }
                else
                {
                    Label lblcandidatename = (Label)e.Item.FindControl("lblCandidateName");
                    Label lblJobTitle = (Label)e.Item.FindControl("lblJobTitle");
                    Label lblGrade = (Label)e.Item.FindControl("lblGrade");
                    Label lblCreatedOn = (Label)e.Item.FindControl("lblCreatedOn");
                    Label lblOfferCreated = (Label)e.Item.FindControl("lblOfferCreated");
                    Label lblStage = (Label)e.Item.FindControl("lblStage");
                    LinkButton lnkReqApprove = (LinkButton)e.Item.FindControl("hlkReqApprove");
                    //HyperLink lnkReqReject = (HyperLink)e.Item.FindControl("hlkReqReject");
                    HyperLink lnkJobTitle = (HyperLink)e.Item.FindControl("lnkJobTitle");
                    TableCell tcCell = (TableCell)e.Item.FindControl("thAction");

                    Label lblTemplate = (Label)e.Item.FindControl("lblTemplate");
                    Label lblAction = (Label)e.Item.FindControl("lblAction");
                    //SecureUrl urlEditJob = UrlHelper.BuildSecureUrl("../" + (UrlConstants.Requisition.MEMBER_OFFER_VIEW_PAGE.Replace("~/", "").Replace("//", "/")), string.Empty, memberHiringDetails.Id.ToString(), StringHelper.Convert(memberHiringDetails.Id), UrlConstants.PARAM_SITEMAP_PARENT_ID, "13", UrlConstants.PARAM_JOB_DISABLE, "true");


                    lblcandidatename.Text = Convert.ToString(memberHiringDetails.ApplicantName);
                    lblJobTitle.Text = Convert.ToString(memberHiringDetails.JobTitle);
                    lblGrade.Text = ""; //Convert.ToString(memberHiringDetails.Grade);
                    lblCreatedOn.Text = (memberHiringDetails.OfferedDate == null) ? string.Empty : Convert.ToString(memberHiringDetails.OfferedDate.ToShortDateString());
                    lblOfferCreated.Text = Convert.ToString(memberHiringDetails.OfferedSalary);

                    lnkReqApprove.Visible = false;
                    if (memberHiringDetails.ApprovalType == 3)
                    {
                        if (memberHiringDetails.IsApprove == true)
                        {
                            lnkReqApprove.Visible = true;
                            lblStage.Text = "Offer Approved by HR Head";
                        }
                        else
                        {
                            lblStage.Text = "Offer Rejected by HR Head";
                        }
                    }
                    else if (memberHiringDetails.ApprovalType == 2)
                    {
                        if (memberHiringDetails.IsApprove == true)
                        {
                            lblStage.Text = "Offer Approved by Recruitment Manager";
                        }
                        else
                        {
                            lblStage.Text = "Offer Rejected by Recruitment Manager";
                        }
                    }
                    else
                    {
                        lnkReqApprove.Visible = false;
                        lblStage.Text = "Offer Submitted by Recruiter";
                    }
                    int Id = CurrentMember.Id;
                
                    //lnkReqReject.Visible = false;
                    lnkJobTitle.Attributes.Add("onclick", "EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/ViewOffer.aspx?MID=" + CurrentMember.Id + "&ID=" + memberHiringDetails.Id + "','820px','350px'); return false;");
                    //lnkReqReject.Attributes.Add("onclick", "EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/CommonHiringDetails.aspx?StatusId=" + statusId + "&JID=" + memberHiringDetails.JobPostingId + "&Canid=" + memberHiringDetails.CandidateId + "','900px','700px'); return false;");
                    lnkJobTitle.Text = "View";
                    lnkReqApprove.Visible = false;
                    //string strPath = FileDownload(Convert.ToString(memberHiringDetails.CandidateId),Convert.ToString(memberHiringDetails.Id));

                    //strPath = "../Templates/Download File/" + strPath;

                    //lnkReqApprove.Attributes.Add("href", strPath);
                    //lnkReqApprove.Attributes.Add("target", "_blank");
                }
            }
        }
    }
    private void SetDataSourceParameters()
    {
        if (SortOrder.Text == string.Empty && SortColumn.Text == string.Empty)
        {
            SortOrder.Text = "asc";
            SortColumn.Text = "c.candidatename";
        }
        odsOfferList.SelectParameters["MemberId"].DefaultValue = Convert.ToString(CurrentMember.Id);
        odsOfferList.SelectParameters["SortOrder"].DefaultValue = SortOrder.Text.ToString();
        odsOfferList.SelectParameters["SortColumn"].DefaultValue = SortColumn.Text.ToString();
        odsOfferList.SelectParameters["sortExpression"].DefaultValue = SortOrder.Text.ToString();
        odsOfferList.SelectParameters["maximumRows"].DefaultValue = "150";
        odsOfferList.SelectParameters["startRowIndex"].DefaultValue = "0";
    }

    protected void lsvOfferList_PreRender(object sender, EventArgs e)
    {
        ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvOfferList.FindControl("pagerControl");
        if (PagerControl != null)
        {
            DataPager pager = (DataPager)PagerControl.FindControl("pager");
            if (pager != null)
            {
                DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                //if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
                if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, "100");
            }
            HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
            if (hdnRowPerPageName != null) hdnRowPerPageName.Value = "OfferApprovalRowPerPage";
        }

        PlaceUpDownArrow();

        if (IsPostBack)
        {
            if (lsvOfferList.Items.Count == 0)
            {
                lsvOfferList.DataSource = null;
                lsvOfferList.DataBind();
            }
        }



    }
}


public class PDFFooter : PdfPageEventHelper
{
    PdfContentByte cb;
    PdfTemplate template;
    // write on top of document
    public override void OnOpenDocument(PdfWriter writer, Document document)
    {
        base.OnOpenDocument(writer, document);
        cb = writer.DirectContent;
        template = cb.CreateTemplate(50, 50);
        //PdfPTable tabFot = new PdfPTable(new float[] { 1F });
        //tabFot.SpacingAfter = 10F;
        //PdfPCell cell;
        //tabFot.TotalWidth = 300F;
        //cell = new PdfPCell(new Phrase("Header"));
        //tabFot.AddCell(cell);
        //tabFot.WriteSelectedRows(0, -1, 150, document.Top, writer.DirectContent);
    }

    // write on start of each page
    public override void OnStartPage(PdfWriter writer, Document document)
    {
        //base.OnStartPage(writer, document);
    }

    // write on end of each page
    public override void OnEndPage(PdfWriter writer, Document document)
    {
        BaseFont baseFont = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
        int pageN = writer.PageNumber;
        String text = "Page " + pageN.ToString();
        float len = baseFont.GetWidthPoint(text, 12);

        iTextSharp.text.Rectangle pageSize = document.PageSize;
        cb.SetRGBColorFill(100, 100, 100);

        #region Footer text
        cb.BeginText();
        cb.SetFontAndSize(baseFont, 8);
        cb.SetTextMatrix(46, 50);

        ColumnText ct = new ColumnText(cb);
        var font1 = FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.ITALIC);
        ct.SetSimpleColumn(new Phrase(new Chunk(text, font1)),
                     35, 70, 530, 36, 25, Element.ALIGN_CENTER | Element.ALIGN_TOP);    //35, 90, 530, 36, 25
         Paragraph footerPara = new Paragraph();
        footerPara.Add("Please maintain strict confidentially of the terms and conditions" +
        "of your employment. You are advised that the Management takes a very serious view of such disclosure" +
        "and you will be liable for disciplinary action for breach of this condition of service.");
        footerPara.Alignment = Element.ALIGN_CENTER;
        footerPara.Font = font1;
        ct.AddElement(footerPara);
        int status = ct.Go();
        cb.EndText();
        cb.AddTemplate(template, document.LeftMargin, pageSize.GetBottom(document.BottomMargin));//+ len
        #endregion
        #region Page Number
        ColumnText ct1 = new ColumnText(cb);
        cb.BeginText();
        cb.SetFontAndSize(baseFont, 8);
        cb.SetTextMatrix(35, 15);

        cb.ShowText(text);
        cb.EndText();
        cb.AddTemplate(template, document.LeftMargin, pageSize.GetBottom(document.BottomMargin));
        #endregion
        #region Confidential
        cb.BeginText();
        cb.SetFontAndSize(baseFont, 8);
        cb.SetTextMatrix(500, 15);//470, pageSize.GetBottom(document.BottomMargin)
        cb.ShowText("Confidential");
        cb.EndText();
        cb.AddTemplate(template, document.LeftMargin, pageSize.GetBottom(document.BottomMargin));// 50, 15);
        #endregion
    }

    //write on close of document
    public override void OnCloseDocument(PdfWriter writer, Document document)
    {
        base.OnCloseDocument(writer, document);
    }

}
public class PDFContractFooter : PdfPageEventHelper
{
    PdfContentByte cb;
    PdfTemplate template;

    // write on end of each page
    public override void OnEndPage(PdfWriter writer, Document document)
    {
        BaseFont baseFont = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
        String text = "Confidential";
         var font1 = FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.ITALIC);
        iTextSharp.text.Rectangle pageSize = document.PageSize;

        // cb.SetRGBColorFill(100, 100, 100);

        cb.BeginText();
        cb.SetFontAndSize(baseFont, 10);//this.RunDateFont.BaseFont, this.RunDateFont.Size
        cb.SetTextMatrix(document.RightMargin, pageSize.GetBottom(document.BottomMargin));
       // Paragraph footerPara = new Paragraph();
        //footerPara.Add(text
        cb.ShowText(text);

        cb.EndText();

        cb.AddTemplate(template, document.LeftMargin, pageSize.GetBottom(document.BottomMargin));
    }
}