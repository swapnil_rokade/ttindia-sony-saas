﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RequisitionList.ascx.cs"
    Inherits="TPS360.Web.UI.RequisitionList" %>
<%@ Register Src="~/Controls/DateRangePicker.ascx" TagName="DateRangePicker" TagPrefix="ucl" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Controls/PagerControl.ascx" TagName="Pager" TagPrefix="ucl" %>
<%@ Register Src="~/Controls/ModalTemplate.ascx" TagName="Template" TagPrefix="ucl" %>
<%@ Register Src="~/Controls/RequisitionAssignedManagers.ascx" TagName="Managers"
    TagPrefix="ucl" %>
<%@ Register Src="~/Controls/CountryStatePicker.ascx" TagName="CountryState" TagPrefix="ucl" %>
<%@ Register Src ="~/Controls/MultipleItemPicker.ascx" TagName ="MultipleSelection" TagPrefix="ucl" %>


<style>
    .dropdown-menu:after
    {
        border-bottom: 7px solid  #FFFFFF;
        border-left: 7px solid transparent;
        border-right: 7px solid transparent;
        content: "";
        display: inline-block;
        left: 80%;
        position: absolute;
        top: -6px;
    }
    .dropdown-menu::before
    {
        position: absolute;
        top: -7px;
        left: 80%;
        display: inline-block;
        border-right: 7px solid transparent;
        border-bottom: 7px solid #CCC;
        border-left: 7px solid transparent;
        border-bottom-color: rgba(0, 0, 0, 0.2);
        content: '';
    }
    .pnlHide
    {
    }
    .pnlHide:first-child
    {
        overflow: hidden;
    }
</style>

<script type="text/javascript" language="javascript" src="../js/AjaxVariables.js"></script>

<script type="text/javascript" language="javascript" src="../js/AjaxScript.js"></script>

<script type="text/javascript">
document.body.onclick = function(e)
  {
  $('#ctl00_cphHomeMaster_uclRequisition_pnlSearchBoxContent').css({'overflow-y':'inherit','overflow-x':'inherit'});
  $('#ctl00_cphHomeMaster_uclRequisition_pnlSearchBoxContent div:first').css({'overflow-y':'inherit','overflow-x':'inherit'});
  }
    //To Keep the scroll bar in the Exact Place
    Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(PageLoadedReq);
    function PageLoadedReq(sender, args) {
        try {
        var hdnScroll = document.getElementById('ctl00_cphHomeMaster_uclRequisition_hdnScrollPos');
        var bigDiv = document.getElementById('bigDiv');
        bigDiv.scrollLeft = hdnScroll.value;
        var background = $find("mpeModal")._backgroundElement;
        background.onclick = function()
            {
                CloseModal(); 
            }
        }
        catch (e) {
        }
    }
    function onShownManagers() {
        var background = $find("MPEManagers")._backgroundElement;
        background.onclick = function() { $find("MPEManagers").hide(); }
    }
    function SetScrollPosition() {
        var hdnScroll = document.getElementById('ctl00_cphHomeMaster_uclRequisition_hdnScrollPos');
        var bigDiv = document.getElementById('bigDiv');
        hdnScroll.value = bigDiv.scrollLeft;
    }
  
    
    function RefreshList()
    { 
        var hdnDoPostPack=document .getElementById ('<%=hdnDoPostPack.ClientID %>');
        hdnDoPostPack .value="1";
        __doPostBack('ctl00_cphHomeMaster_uclRequisition_upcandidateList',''); return false ;
    }
    $('#ctl00_cphHomeMaster_uclRequisition_lsvJobPosting_lnkNoOfOpening').mouseover(function() {
        //$('#ctl00_cphHomeMaster_uclRequisition_lsvJobPosting_lnkNoOfOpening').append('<div>Handler for .mouseover() called.</div>');
        console.log('asdsaweqw');
        alert('asdasd');
    });

</script>

<asp:HiddenField ID="hdnScrollPos" runat="server" />
<asp:ObjectDataSource ID="odsRequisitionList" runat="server" SelectMethod="GetPagedRequisitionList"
    OnSelecting="odsRequisitionList_Selecting" TypeName="TPS360.Web.UI.JobPostingDataSource"
    SelectCountMethod="GetRequisitionListCount" EnablePaging="True" SortParameterName="sortExpression">
    <SelectParameters>
        <asp:Parameter Name="IsTemplate" DefaultValue="False" Type="Boolean" Direction="Input" />
        <asp:Parameter Name="memberId" />
        <asp:Parameter Name="IsCompanyContact" Type="Boolean" DefaultValue="False" />
        <asp:Parameter Name="CompanyContactId" Type="Int32" DefaultValue="0" />
        <asp:ControlParameter ControlID="txtJobTitle" Name="JobTitle" PropertyName="Text"
            Type="String" />
        <asp:ControlParameter ControlID="txtReqCode" Name="ReqCode" PropertyName="Text" Type="String" />
        <asp:ControlParameter ControlID="ddlJobStatus" Name="jobStatus" PropertyName="SelectedItems"
            Type="String" />
        <asp:ControlParameter ControlID="dtPstedDate" Name="JobPostingFromDate" PropertyName="StartDate"
            Type="String" />
        <asp:ControlParameter ControlID="dtPstedDate" Name="JobPostingToDate" PropertyName="EndDate"
            Type="String" />
        <asp:ControlParameter ControlID="ddlReqCreator" Name="ReqCreator" PropertyName="SelectedValue"
            Type="String" />
        <asp:ControlParameter ControlID="ddlEmployee" Name="employee" PropertyName="SelectedValue"
            Type="String" />
        <asp:ControlParameter ControlID="TxtCity" Name="City" PropertyName="Text" Type="String" />
        <asp:ControlParameter ControlID="uclCountryState" Name="StateID" PropertyName="SelectedStateId"
            Type="String" />
        <asp:ControlParameter ControlID="uclCountryState" Name="CountryID" PropertyName="SelectedCountryId"
            Type="String" />
        <asp:ControlParameter ControlID="ddlEndClient" Name="endClients" PropertyName="SelectedItems"
            Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>
<asp:Panel ID="pnlSearchBoxBody" runat="server" CssClass="well well-small nomargin"
    DefaultButton="btnSearch">
    <asp:Panel ID="pnlSearchRegion" runat="server">
        <asp:CollapsiblePanelExtender ID="cpeExperience" runat="server" TargetControlID="pnlSearchBoxContent"
            ExpandControlID="pnlSearchBoxHeader" CollapseControlID="pnlSearchBoxHeader" CollapsedImage="~/Images/expand-plus.png"
            ExpandedImage="~/Images/collapse-minus.png" Collapsed="true" ImageControlID="imgSearchBoxToggle"
            SuppressPostBack="True">
        </asp:CollapsiblePanelExtender>
        <asp:Panel ID="pnlSearchBoxHeader" runat="server">
            <div class="SearchBoxContainer">
                <div class="SearchTitleContainer">
                    <div class="ArrowContainer">
                        <asp:ImageButton ID="imgSearchBoxToggle" runat="server" ImageUrl="~/Images/expand-plus.png"
                            AlternateText="collapse" />
                    </div>
                    <div class="TitleContainer">
                        Filter Options
                    </div>
                </div>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlSearchBoxContent" runat="server"  CssClass ="filter-section" Style=";" Height="0">
            <div class="TableRow spacer">
                <div class="FormLeftColumn" style="width: 50%;">
                    <div class="TableRow">
                    <%--// Changes Start By Vishal Tripathy--%>
                        <div class="TableFormLeble" style="width: 40%;">
                            <asp:Label EnableViewState="false" ID="lblJobTitle" runat="server" Text="Requisition Job Title"></asp:Label>:
                        </div>
                        <%--// Changes End By Vishal Tripathy--%>
                        <div class="TableFormContent">
                            <asp:TextBox ID="txtJobTitle" runat="server" CssClass="SearchTextbox"></asp:TextBox>
                        </div>
                    </div>
                    <div class="TableRow">
                        <div class="TableFormLeble" style="width: 40%;">
                            <asp:Label EnableViewState="false" ID="lblReqCode" runat="server" Text="Req Code"></asp:Label>:
                        </div>
                        <div class="TableFormContent">
                            <asp:TextBox ID="txtReqCode" runat="server" CssClass="SearchTextbox"></asp:TextBox>
                        </div>
                    </div>
                    <div class="TableRow" style="white-space: nowrap">
                        <div class="TableFormLeble" style="width: 40%;">
                        <%--Changes start By Vishal Tripathy--%>
                            <asp:Label EnableViewState="false" ID="Label1" runat="server" Text="Submitted On"></asp:Label>:
                            <%--Changes end By Vishal Tripathy--%>
                        </div>
                        <div class="TableFormContent">
                            <ucl:DateRangePicker ID="dtPstedDate" runat="server" />
                        </div>
                    </div>
                    <div class="TableRow">
                        <div class="TableFormLeble" style="width: 40%;">
                            <asp:Label EnableViewState="false" ID="lblJobStatusHeader" runat="server" Text="Requisition Status"></asp:Label>:
                        </div>
                        <div class="TableFormContent">
                        <ucl:MultipleSelection ID="ddlJobStatus" runat ="server" />
                        
                       <%--     <asp:DropDownList EnableViewState="true" ID="ddlJobStatus" runat="server" CssClass="CommonDropDownList"
                                AutoPostBack="false">
                            </asp:DropDownList>--%>
                        </div>
                    </div>
                    
                    
                   
                </div>
                <div class="FormRightColumn" style="width: 49%">
                    <div class="TableRow" style="white-space: nowrap;">
                    <div class="TableRow">
                        <div class="TableFormLeble" style="width: 40%;">
                        <%--Changes start by Vishal Tripathy--%>
                            <asp:Label EnableViewState="false" ID="lblCreator" runat="server" Text="Submitted By"></asp:Label>:
                            <%--Changes end by Vishal Tripathy--%>
                        </div>
                        <div class="TableFormContent">
                            <asp:DropDownList ID="ddlReqCreator" runat="server" CssClass="CommonDropDownList">
                            </asp:DropDownList>
                        </div>
                    </div>
                    
                     <div class="TableRow">
                        <div class="TableFormLeble" style="width: 40%;">
                        <%--Changes start By Vishal Tripathy--%>
                            <asp:Label EnableViewState="false" ID="lblByEmployee" runat="server" Text="Recruiter"></asp:Label>:
                        <%--Changes end By Vishal Tripathy    --%>
                        </div>
                        <div class="TableFormContent">
                            <asp:DropDownList ID="ddlEmployee" CssClass="CommonDropDownList" runat="server">
                            </asp:DropDownList>
                             <asp:TextBox EnableViewState="false" ID="txtCity" CssClass="SearchTextbox" runat="server" Visible="false"></asp:TextBox>
                        </div>
                    </div>
                     <%--   Changes start By Vishal Tripathy--%>
                      <%-- <div class="TableFormLeble" style="width: 40%;">
                            <asp:Label EnableViewState="false" ID="lblByLocation" runat="server" Text="City"></asp:Label>:
                        </div>--%>
                        <%--<div class="TableFormContent">
                           
                        </div>--%>
                     <%--   Changes end by Vishal tripathy--%>
                    </div>
                    <ucl:CountryState ID="uclCountryState" runat="server" FirstOption="Please Select" ShowAndHideState="false"
                        TableFormLabel_Width="40%" />
                    <div id="divBU" runat="server">
                    <div class="TableRow">
                        <div class="TableFormLeble" style="width: 40%;">
                            <asp:Label EnableViewState="true" ID="lblByEndClientsHeader" Visible="false"  runat="server" Text="Account"></asp:Label>:
                        </div>
                        <div class="TableFormContent">
                            <%--<asp:DropDownList ID="ddlEndClient" CssClass="CommonDropDownList" runat="server">
                            </asp:DropDownList>--%>
                            <ucl:MultipleSelection ID="ddlEndClient" runat ="server"  Visible="false"/>
                        </div>
                    </div></div>
                </div>
                <div class="TableRow" style="text-align: center; padding-top: 5px; padding-bottom: 5px">
                    <asp:LinkButton ID="btnSearch" runat="server" Text="Search" AlternateText="Search"
                        CssClass="btn btn-primary" ValidationGroup="RequisitionList" EnableViewState="false"
                        OnClick="btnSearch_Click"><i class="icon-search icon-white"></i> Search</asp:LinkButton>
                    <asp:Button ID="btnClear" runat="server" Text="Clear" AlternateText="Clear" CssClass="btn"
                        EnableViewState="false" OnClick="btnClear_Click" />
                </div>
            </div>
        </asp:Panel>
    </asp:Panel>
</asp:Panel>


<br />
<div>
    <asp:UpdatePanel ID="up" runat="server">
        <ContentTemplate>
            <ajaxToolkit:AlwaysVisibleControlExtender ID="avceMessage" runat="server" TargetControlID="lblMessage">
            </ajaxToolkit:AlwaysVisibleControlExtender>
            <asp:Label ID="lblMessage" runat="server" EnableViewState="false"></asp:Label>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>
<asp:UpdatePanel ID="upcandidateList" runat="server">
    <ContentTemplate>
        <asp:HiddenField ID="hdnDoPostPack" runat="server" />
        <asp:HiddenField ID="hdnSortColumn" runat="server" />
        <asp:HiddenField ID="hdnSortOrder" runat="server" />
        <div class="GridContainer">
            <div style="overflow-X: scroll; overflow-y: hidden; padding-bottom:70px" id="bigDiv" onscroll='SetScrollPosition()'>
                <asp:ListView ID="lsvJobPosting" runat="server" EnableViewState="true" DataKeyNames="Id"
                    OnItemDataBound="lsvJobPosting_ItemDataBound" OnItemCommand="lsvJobPosting_ItemCommand"
                    OnPreRender="lsvJobPosting_PreRender" DataSourceID="odsRequisitionList">
                    <LayoutTemplate>
                        <table id="tlbTemplate" runat="server" class="Grid" cellspacing="0" border="0">
                            <tr id="trHeadLevel" runat="server">
                                
                                <th runat="server" id="thReqCode" style="white-space: nowrap; width: 70px !important">
                                    <asp:LinkButton ID="lnkReqCode" runat="server" CommandName="Sort" ToolTip="Sort By Req Code"
                                        CommandArgument="[J].[JobPostingCode]" Text="Req Code" TabIndex="3" />
                                </th>
                               
                    
                                    <%--Changes start by vishal Tripathy--%>
                                <th style="white-space: nowrap; min-width: 140px; width:120px;">
                                    <asp:LinkButton ID="btnJobTitle" runat="server" CommandName="Sort" ToolTip="Sort By Requisition Title"
                                        CommandArgument="[J].[JobTitle]" Text="Requisition Title" TabIndex="6" />
                                </th>
                              
                              
                                 <%--Changes end By Vishal Tripathy--%>
                                <th  id="thNoOfOpening" runat="server" style="width: 97px !important">
                                    <asp:LinkButton ID="lnkNoOfOpening" runat="server" CommandName="Sort" ToolTip="# of Openings"
                                       Text="# of Openings" CommandArgument="[J].NoOfOpenings" TabIndex="8" />
                                </th>
                                <%--Changes start by vishal Tripathy--%>
                                <th id="thLevels" runat="server" style="width: 100px !important">
                                   <%-- <asp:LinkButton ID="lnkLevel" runat="server" Text="Total" ToolTip="Sort By Total Candidates"
                                        CommandName="Sort" CommandArgument="[1]"></asp:LinkButton>--%>
                                      Candidates
                                </th>
                                <%--Changes end By Vishal Tripathy--%>
                                
                                <th style="width: 40px !important">
                                    <asp:LinkButton ID="lnkTeam" runat="server" Text="Team" ToolTip="Sort By Team" CommandArgument="[JMC].[ManagersCount]"
                                        CommandName="Sort"></asp:LinkButton>
                                </th>
                                <%--Changes start By Vishal Tripathy--%>
                                <th style="white-space: nowrap; min-width: 50px; width: 135px !important">
                                    <asp:LinkButton ID="btnJobStatus" runat="server" Text="Requisition Status" CommandName="Sort"
                                        CommandArgument="[GL].[Name]" TabIndex="7" Width="100%" />
                                </th>
                                <%--Changes end By Vishal Tripathy--%>
                                <th style="white-space: nowrap; width:112px;" runat="server" id="thAssigningManager">
                                    Assigning<br /> Manager
                                </th>
                                  <%--******************************************************************--%>
                                <%--Rupesh Kadam Start--%>
                                 <th style="white-space: nowrap; width:110px" runat="server" id="th2">
                                       Last Approver
                                </th>
                                <th style="white-space: nowrap; width:110px" runat="server" id="th4">
                                       Next Approver
                                </th>
                                <th style="white-space: nowrap; width:110px" runat="server" id="th1">
                                    Approval Stage
                                </th>
                                
                                <th style="white-space: nowrap; width:120px" runat="server" id="th3">
                                    Submitted By
                                </th>
                                
                                <th style="white-space: nowrap; width: 135px !important">
                                <%--Changes start by Vishal Tripathy--%>
                                    <asp:LinkButton ID="btnPostedDate" runat="server" CommandName="Sort" ToolTip="Sort By Post Date"
                                        CommandArgument="[J].[PostedDate]" Text="Submitted On" TabIndex="2" />
                                    <%--    Changes end by Vishal Tripathy--%>
                                </th>
                                <%--Rupesh Kadam End--%>
                                <%--******************************************************************--%>
                                <th style="text-align: left; white-space: nowrap; width: 65px !important;" id="thAction"
                                    runat="server">
                                    Action
                                </th>
                            </tr>
                             <tr id="itemPlaceholder" runat="server">
                           </tr>
                           <tr class="Pager">
                               <td colspan="13" runat="server" id="tdPager">
                                <ucl:Pager ID="pagerControl" runat="server" EnableViewState="true" />
                               </td>
                           </tr>
                           <%-- <tr>
                                <td colspan="16" style="padding: 0px 0px 0px 0px; border-style: none none none none">
                                </td>
                            </tr>
                            <tr id="itemPlaceholder" runat="server">
                            </tr>
                            <tr class="Pager">
                                <td colspan="16" runat="server" id="tdPager">
                                    <ucl:Pager ID="pagerControl" runat="server" EnableViewState="true" />
                                </td>
                            </tr>--%>
                        </table>
                    </LayoutTemplate>
                    <EmptyDataTemplate>
                        <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                            <tr>
                                <td>
                                    No data was returned.
                                </td>
                            </tr>
                        </table>
                    </EmptyDataTemplate>
                    <ItemTemplate>
                        <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                            
                            <%--Changes start by vishal tripathy--%>
                            <td id="tdJobCode" runat="server">
                                <asp:Label ID="lblJobCode" runat="server" />
                               
                            </td>
                            <%--Changes end by vishal tripathy--%>
                        
                            <td>
                                <asp:HyperLink ID="lnkJobTitle" runat="server" Target="_blank" TabIndex="8" Style="cursor: pointer;"></asp:HyperLink>
                                    <%--Changes start by vishal tripathy--%>
                                   
                                    <%--Changes end by vishal tripathy--%>
                            </td>
                            
                             <td>
                                <asp:Label ID="lblNoOfOpenings" runat="server" />
                            </td>
                            <td id="tdLevels" runat="server">
                                <asp:HyperLink ID="tdLevel" runat="server"></asp:HyperLink>
                            </td>
                            <td>
                                <asp:LinkButton ID="lnkTeamCount" runat="server" CommandName="Managers"></asp:LinkButton>
                            </td>
                            <td>
                                <asp:Label ID="lblJobStatus" runat="server" Style="min-height: 140px;" />
                                <asp:DropDownList ID="ddlJobStatus" runat="server" CssClass="CommonDropDownList"
                                    Style="min-width: 90px" Width="140px">
                                </asp:DropDownList>
                            </td>
                            <td runat="server" id="tdAssigningManager">
                                <asp:Label ID="lblAssignedManager" runat="server" />
                            </td>
                            <%--******************************************************************--%>
                                <%--Rupesh Kadam Start--%>
                                <td runat="server" id="tdApprovingManger">
                                <asp:Label ID="lblApprovingManager" runat="server" />
                            </td>
                            
                            <td runat="server" id="tdNextApprovingManger">
                                <asp:Label ID="lblNextApprovingManager" runat="server" />
                            </td>
                            <td runat="server" id="tdApprovalStatus">
                                <asp:Label ID="lblApprovalStatus" runat="server" />
                            </td>
                            
                              <td runat="server" id="td1" style="width:100px">
                                <asp:Label ID="lblPostedBy" runat="server" />
                            </td>
                            <td runat="server" id="tdpostte" style="width:100px">
                                <asp:Label ID="lblPostedDate" runat="server"></asp:Label>
                            </td>
                                <%--Rupesh Kadam End--%>
                            <%--******************************************************************--%>       
                            <td style="overflow: inherit;" runat="server" id="tdAction" enableviewstate="true">
                                <ul class="nav" style="margin-bottom: 0px; padding-left: 0px;">
                                    <li class="dropdown"><a data-toggle="dropdown" class="dropdown-toggle" style="width: 30px;
                                        margin-top: -15px;" href="#">
                                        <div class="container">
                                            <button class="btn btn-mini" type="button">
                                                <i class="icon icon-cog"></i><span class="caret"></span>
                                            </button>
                                        </div>
                                    </a>
                                        <ul class="dropdown-menu" style="margin-left: -175px; margin-top: -25px;">
                                            <li>
                                                <asp:HyperLink ID="hlkOpenHiringMatrix" runat="server"  Text="Open Hiring Matrix"></asp:HyperLink>
                                            </li>
                                            <li>
                                                <asp:LinkButton ID="btnEdit" Text="Edit Requisition" CommandName="EditItem" runat="server"></asp:LinkButton>
                                            </li>
                                             <!--*********[Kanchan Yeware] - [Add IJP Referral Link Button] – [9-Sept-2016] – Start *****************-->
                                            <li>
                                                <asp:LinkButton ID="lnkIJPReferralLink" runat="server" Text="IJP Referral Link"></asp:LinkButton>
                                            </li>
                                            <li>
                                                <asp:LinkButton ID="lnkShowInExternal" runat="server" CommandName="ExternalItem" Text="Forward to Talent Aquisition Team"></asp:LinkButton>
                                            </li>                                           
                                            <!--*********[Kanchan Yeware] - [Add IJP Referral Link Button] – [9-Sept-2016] – End *****************-->
                                            <li>
                                                <asp:LinkButton ID="btnDelete" Text="Delete" CommandName="DeleteItem" runat="server"></asp:LinkButton>
                                            </li>
                                            <li class="divider" id="liDivider" runat="server"></li>
                                            <li>
                                                <asp:LinkButton ID="lnkEmployeeReferralLink" runat="server" Text="Employee Referral Link"></asp:LinkButton>
                                           </li>
                                            <%--  <li>
                                                <asp:HyperLink ID="hlkCancel" runat="server"  Text="Cancel"></asp:HyperLink>
                                            </li>--%>
                                              <%-- <li>
                                                <asp:HyperLink ID="hlkClose" runat="server"  Text="Close"></asp:HyperLink>
                                            </li>--%>
                                              <li>
                                                <asp:LinkButton ID="lnkCancel" Text="Cancel" CommandName="Cancel" runat="server"></asp:LinkButton>
                                            </li>
                                              <li>
                                                <asp:LinkButton ID="lnkClose" Text="Close" runat="server"></asp:LinkButton>
                                            </li>
                                               <li>
                                                <asp:HyperLink ID="hlkPutOnHold" runat="server"  Text="Put On-Hold"></asp:HyperLink>
                                            </li>                                          
                                             <li>
                                                <asp:LinkButton ID="btnCandidatePortal" Text="Candidate Portal" CommandName="CandidatePortal" runat="server"></asp:LinkButton>
                                            </li>
                                              <li>
                                                <asp:LinkButton ID="btnVendorPortal" Text="Publish To Vendor Portal"  runat="server"></asp:LinkButton>
                                            </li><%--CommandName="VendorPortal"--%>
                                              <li>
                                                <asp:LinkButton ID="btnEmployeeReferral" Text="Employee Referral Portal" CommandName="EmployeeReferral" runat="server"></asp:LinkButton>
                                            </li>
                                            
                                        </ul>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:ListView>
            </div>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
