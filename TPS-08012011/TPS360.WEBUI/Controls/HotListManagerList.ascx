﻿
<%@ Control Language="C#" AutoEventWireup="true" CodeFile="HotListManagerList.ascx.cs"
    Inherits="TPS360.Web.UI.HotListManagerList" %>

  <asp:UpdatePanel ID="upAssign" runat ="server" >
                    <ContentTemplate >
                    <div class="TabPanelHeader" >Assign Managers</div>
                         <div class="TableRow" style="text-align: center">
                            <asp:Label ID="lblAddEmployee" runat="server" EnableViewState="false"></asp:Label>
                        </div>
                        <div class="TableRow" style="text-align: left;">
                            <div class="TableFormLeble">
                                <asp:Label ID="lblSelectEmployee" runat="server" Text="Select Manager" ></asp:Label>:
                            </div>
                            <div class="TableFormContent">
                                <asp:ListBox ID="lstSelectEmployee" CssClass="CommonDropDownList" runat="server" Width = "250px"
                                    Height="120px" SelectionMode="Multiple"></asp:ListBox>
                                <span style="vertical-align: top;">
                                    <asp:Button ID="btnAdd" Text="Add" runat="server" OnClick="btnAdd_Click" CssClass="CommonButton" />
                                    (Add multiple by holding CTRL) </span>
                            </div>
                        </div>
                         <div class="TabPanelHeader" >List of Assigned Managers</div>
                        <div class="GridContainer">
                            <asp:ListView ID="lsvEmployees" runat="server" OnPreRender="lsvEmployees_PreRender"
                                OnItemCommand="lsvEmployees_ItemCommand" OnItemDataBound="lsvEmployees_ItemDataBound">
                                <LayoutTemplate>
                                    <table id="tlbTemplate" runat="server" class="Grid" cellspacing="0" border="0">
                                        <tr>
                                            <th>
                                                <asp:Label ID="lbllsvName" runat="server" Text="Name"></asp:Label>
                                            </th>
                                            <th>
                                                <asp:Label ID="lbllsvPosition" runat="server" Text="Email"></asp:Label>
                                            </th>
                                            <th style =" width : 120px;">
                                                <asp:Label ID="lbllsvCurrentCompany" runat="server" Text="Date Assigned"></asp:Label>
                                            </th>
                                            <th style="text-align: center; width: 65px !important" id="thremove" runat="server">
                                                Remove
                                            </th>
                                        </tr>
                                        <tr id="itemPlaceholder" runat="server">
                                        </tr>
                                    </table>
                                </LayoutTemplate>
                                <EmptyDataTemplate>
                                    <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                                        <tr>
                                            <td>
                                                No manager available.
                                            </td>
                                        </tr>
                                    </table>
                                </EmptyDataTemplate>
                                <ItemTemplate>
                                    <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                                        <td>
                                            <asp:Label ID="lblName" runat="server" />
                                        </td>
                                        <td>
                                            <asp:LinkButton ID="lblEmail" runat="server" />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblDateAssigned" runat="server" />
                                        </td>
                                        <td style="text-align: center;" runat="server" id="tdremove">
                                            <asp:ImageButton ID="btnDelete" Visible="true" SkinID="sknDeleteButton" runat="server"
                                                CommandName="DeleteItem"></asp:ImageButton>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:ListView>
                        </div>
                    </ContentTemplate>
                    </asp:UpdatePanel>