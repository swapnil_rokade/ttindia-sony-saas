﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="IPAccessRules.ascx.cs"
    Inherits="TPS360.Web.UI.IPAccessRules" %>
<%@ Register Src="~/Controls/PagerControl.ascx" TagName="Pager" TagPrefix="ucl" %>

<script type="text/javascript">

    function TxtChanged(txt) {
        var cvAllowedIP = document.getElementById('<%= cvAllowedIP.ClientID %>');
        cvAllowedIP.IsValid = true;
        cvAllowedIP.style.display = "none";
    }

    function CheckedChaned(chkAll) {
        var cvAllowedIP = document.getElementById('<%= cvAllowedIP.ClientID %>');
        cvAllowedIP.IsValid = true;
        cvAllowedIP.style.display = "none";
        var txtAllowedId = document.getElementById('<%=txtAllowedIPAdress.ClientID %>');
        var txtAllowedIdTo=document.getElementById('<%=txtAllowedIPAddressTo.ClientID %>');
        if (chkAll.checked) 
        { 
        txtAllowedId.value = ""; 
        txtAllowedIdTo.value = "";
        txtAllowedId.disabled = true;
        txtAllowedIdTo.disabled = true; 
        }
        else 
        {
        txtAllowedId.disabled = false;
        txtAllowedIdTo.disabled = false;
        }
    }
    function ValidateIPAddress(source, args) {
        var txtAllowedId = document.getElementById('<%=txtAllowedIPAdress.ClientID %>');
        var chkAnyIP = document.getElementById('<%=chkAnyIP.ClientID %>');
        if (chkAnyIP.checked) args.IsValid = true;
        else {
            if (txtAllowedId.value.trim() == '') args.IsValid = false;
            else args.IsValid = true;
        }
        }
      function ValidateIPAddressTo(source, args) {
      
        var txtAllowedIdTo=document.getElementById('<%=txtAllowedIPAddressTo.ClientID %>');
        var chkAnyIP = document.getElementById('<%=chkAnyIP.ClientID %>');
        if (chkAnyIP.checked) args.IsValid = true;
        else {
            if (txtAllowedIdTo.value.trim() == '') args.IsValid = false;
            else args.IsValid = true;
        }
    }
</script>

<div style="padding: 15px;">
    <div class="TableRow" style="text-align: center">
        <asp:Label ID="lblMessage" runat="server" EnableViewState="false"></asp:Label>
        <asp:HiddenField ID="IPAccessRulesID" runat="server" />
        <asp:TextBox ID="txtSortColumn" runat="server" Visible="false"></asp:TextBox>
        <asp:TextBox ID="SortColumn" runat="server" EnableViewState="true" Visible="false" />
        <asp:TextBox ID="SortOrder" runat="server" EnableViewState="true" Visible="false" />
        <asp:TextBox ID="txtSortOrder" runat="server" Visible="false"></asp:TextBox>
    </div>
    <div class="TableRow">
        <div class="TableFormLeble">
            <asp:Label ID="lblRulesAppliesTo" runat="server" Text="Rule Applies To"></asp:Label>:
        </div>
        <div class="TableFormContent">
            <asp:DropDownList ID="ddlEmployees" runat="server" CssClass="CommonDropDownList">
            </asp:DropDownList>
        </div>
    </div>
    <div class="TableRow">
        <div class="TableFormLeble">
            <asp:Label ID="lblAllowedIPAddress" runat="server" Text="Allowed IP Address Range"></asp:Label>:
        </div>
        <div class="TableFormContent">
            <asp:TextBox ID="txtAllowedIPAdress" runat="server" CssClass="CommonTextBox" Width="100px" ValidationGroup="IPAccess"
                onclick="TxtChanged(this)" PlaceHolder="From"></asp:TextBox> -
            <asp:TextBox ID="txtAllowedIPAddressTo" runat="server" CssClass="CommonTextBox" Width="100px" ValidationGroup="IPAccess"
                onclick="TxtChanged(this)" PlaceHolder="To"></asp:TextBox>
            <asp:CheckBox ID="chkAnyIP" runat="server" Text="Any IP" onclick="CheckedChaned(this)"
                ValidationGroup="IPAccess" />
        </div>
    </div>
    <div class="TableRow">
        <div class="TableFormValidatorContent" style="margin-left: 42%">
            <asp:CustomValidator ID="cvAllowedIP" runat="server" ErrorMessage="Please enter IP address"
                Display="Dynamic" ValidationGroup="IPAccess" ClientValidationFunction="ValidateIPAddress"></asp:CustomValidator>
            <asp:RegularExpressionValidator ID="revAllowedIPAddress" runat="server" ControlToValidate="txtAllowedIPAdress"
                Display="Dynamic" ValidationGroup="IPAccess" ErrorMessage="Please enter Valid IP address From Range"
                ValidationExpression="^\b(?:\d{1,3}\.){3}\d{1,3}\b"></asp:RegularExpressionValidator>
        </div>
         <div class="TableFormValidatorContent" style="margin-left: 42%">
            <asp:CustomValidator ID="cvAllowedIPTo" runat="server" ErrorMessage="Please enter IP address"
                Display="Dynamic" ValidationGroup="IPAccess" ClientValidationFunction="ValidateIPAddressTo"></asp:CustomValidator>
            <asp:RegularExpressionValidator ID="revAlloweIPAddressTo" runat="server" ControlToValidate="txtAllowedIPAddressTo"
                Display="Dynamic" ValidationGroup="IPAccess" ErrorMessage="Please enter Valid IP address To Range"
                ValidationExpression="^\b(?:\d{1,3}\.){3}\d{1,3}\b"></asp:RegularExpressionValidator>
        </div>
    </div>
    <div class="TableRow" style="text-align: center">
        <div class="TableFormLeble">
        </div>
        <div class="TableFormContent">
            <asp:Button ID="btnSave" CssClass="CommonButton" runat="server" Text="Save" OnClick="btnSave_Click"
                ValidationGroup="IPAccess" />
        </div>
    </div>
    <asp:ObjectDataSource ID="odsIPRules" SortParameterName="sortExpression" runat="server"
        TypeName="TPS360.Web.UI.IPAccessRulesDataSource" EnablePaging="true" SelectCountMethod="GetListCount"
        SelectMethod="GetPaged">
        <SelectParameters>
            <asp:Parameter Name="SortOrder" DefaultValue="asc" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:UpdatePanel ID="pnlDynamic" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:ListView ID="lsvIPRules" runat="server" DataSourceID="odsIPRules" OnItemDataBound="lsvIPRules_ItemDataBound"
                DataKeyNames="Id" OnPreRender="lsvIPRules_PreRender" OnItemCommand="lsvIPRules_ItemCommand"
                EnableViewState="true">
                <LayoutTemplate>
                    <table id="tlbTemplate" runat="server" class="Grid" cellspacing="0" border="0">
                        <tr>
                            <th>
                                <asp:LinkButton ID="lnkUserId" runat="server" Text="Applies To" CommandName="Sort"
                                    CommandArgument="[M].[FirstName]"></asp:LinkButton>
                            </th>
                            <th>
                                <asp:LinkButton ID="lnkAllowedIP" runat="server" Text="IP Address Range From" CommandName="Sort"
                                    CommandArgument="[U].[UserIp]"></asp:LinkButton>
                            </th>
                            <th>
                                <asp:LinkButton ID="lnkAllowedIPTo" runat="server" Text="IP Address Range To" CommandName="Sort"
                                    CommandArgument="[U].[UserIpTo]"></asp:LinkButton>
                            </th>
                            <th style="width: 50px">
                                Action
                            </th>
                        </tr>
                        <tr id="itemPlaceholder" runat="server">
                        </tr>
                        <tr class="Pager">
                            <td colspan="4" runat="server">
                                <ucl:Pager ID="pagerControl" runat="server" EnableViewState="true" />
                            </td>
                        </tr>
                    </table>
                </LayoutTemplate>
                <EmptyDataTemplate>
                    <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                        <tr>
                            <td>
                                No IP Access Rules.
                            </td>
                        </tr>
                    </table>
                </EmptyDataTemplate>
                <ItemTemplate>
                    <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                        <td style="text-align: left;">
                            <asp:Label ID="lblAppliesTo" runat="server"></asp:Label>
                        </td>
                        <td style="text-align: left;">
                            <asp:Label ID="AllowedIP" runat="server"></asp:Label>
                        </td>
                        <td style="text-align: left;">
                            <asp:Label ID="lblAllowedIPTo" runat="server"></asp:Label>
                        </td>
                        <td>
                            <asp:ImageButton ID="btnEdit" SkinID="sknEditButton" runat="server" CommandName="EditItem">
                            </asp:ImageButton>
                            <asp:ImageButton ID="btnDelete" Visible="true" SkinID="sknDeleteButton" runat="server"
                                CommandName="DeleteItem"></asp:ImageButton>
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:ListView>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>
