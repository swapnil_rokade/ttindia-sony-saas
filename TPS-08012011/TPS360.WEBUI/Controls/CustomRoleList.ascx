﻿<%---------------------------------------------------------------------------------------------------------------------------------------------
    FileName: CustomRoleList.ascx
    Description:
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.         Date                Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1             24-Nov-2009         Sandeesh            Defect id: 11892 ; Changes made for deleting a role.
------------------------------------------------------------------------------------------------------------------------------------------- --%>

<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CustomRoleList.ascx.cs" Inherits="TPS360.Web.UI.ControlCustomRoleList" %>
<%@ Register src="../Controls/MenuSelector.ascx" tagname="MenuSelector" tagprefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src ="~/Controls/PagerControl.ascx" TagName ="Pager" TagPrefix ="ucl" %>  

<div style="padding: 15px;">
    <div class="MainBox" style="width: 100%;">
        <div class="BoxHeader">
            <div class="BoxHeaderContainer">
                <div class="TitleBoxLeft">
                    <asp:Image ID="imgArrow" runat="server" SkinID="sknArrow" AlternateText="" />
                </div>
                <div class="TitleBoxMid">
                    Custom Role List
                </div>
            </div>
        </div>
        <div class="HeaderDevider">
        </div>
        <div class="MasterBoxContainer">
            <div class="ChildBoxContainer">
                <div style="clear: both">
                </div>
                 <asp:TextBox ID="txtSortColumn" runat ="server" Visible ="false"  ></asp:TextBox>
    <asp:TextBox ID="txtSortOrder" runat ="server" Visible ="false"  ></asp:TextBox>
                <asp:ObjectDataSource ID="odsCustomRole" runat="server" SelectMethod="GetPaged" TypeName="TPS360.Web.UI.CustomRoleDataSource" SelectCountMethod="GetListCount" EnablePaging="True" SortParameterName="sortExpression"></asp:ObjectDataSource>
                <div class="BoxContainer">
                    <asp:UpdatePanel ID="updPanelCustomRole" runat="server"  UpdateMode="Conditional"><%--0.1--%>
                        <ContentTemplate>
                            <div>
                                <asp:Label ID="lblMessage" runat="server" EnableViewState="false"></asp:Label>
                            </div>
                            <div class="GridContainer">
                                <asp:ListView ID="lsvCustomRole" runat="server" DataSourceID="odsCustomRole" DataKeyNames="Id" OnItemDataBound="lsvCustomRole_ItemDataBound" OnItemCommand="lsvCustomRole_ItemCommand"   OnPreRender ="lsvCustomRole_PreRender">
                                    <LayoutTemplate>
                                        <table id="tlbTemplate" runat="server" class="Grid" cellspacing="0" border="0">
                                            <tr>         
                                                <th style ="width :25%;">
                                                    <asp:LinkButton ID="btnName" CausesValidation="false" runat="server" CommandName="Sort" CommandArgument="Name" Text="Name" />
                                                </th>
                                                <th style ="width :55%;">
                                                    <asp:LinkButton ID="btnDescription" CausesValidation="false" runat="server" CommandName="Sort" CommandArgument="Description" Text="Description" />
                                                </th>
                                                <th style ="width :12%;">
                                                    <asp:LinkButton ID="btnUpdateDate" CausesValidation="false" runat="server" CommandName="Sort" CommandArgument="UpdateDate" Text="Last Updated" />
                                                </th>
                                                <th style ="width :40px !important;">
                                                    Action
                                                </th>
                                            </tr>
                                            <tr ID="itemPlaceholder" runat="server">
                                            </tr>
                                            <tr class="Pager">
                                                <td colspan="4">
                                                  <ucl:Pager Id="pagerControl" runat ="server" EnableViewState ="true"  />
                                                </td>
                                            </tr>
                                        </table>
                                    </LayoutTemplate>                
                                    <EmptyDataTemplate>
                                        <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                                            <tr>
                                                <td>
                                                    No data was returned.
                                                 </td>
                                            </tr>
                                        </table>
                                    </EmptyDataTemplate>
                                    <ItemTemplate>
                                        <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                                            <td style="text-align: left; width :25%;">
                                                <asp:Label ID="lblName" runat="server" />
                                            </td>
                                            <td style="text-align: left; width :55%;">
                                                <asp:Label ID="lblDescription" runat="server" />
                                            </td>
                                            <td style="text-align: left; white-space: nowrap; width :12%;">
                                                <asp:Label ID="lblLastUpdated" runat="server" />
                                            </td>
                                            <td style ="width :8%; text-align: left;">
                                                <asp:ImageButton ID="btnEdit" SkinID="sknEditButton" runat="server" CommandName="EditItem" CausesValidation="false"></asp:ImageButton>
                                                <asp:ImageButton ID="btnDelete" SkinID="sknDeleteButton" runat="server" CommandName="DeleteItem" CausesValidation="false"></asp:ImageButton>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:ListView>   
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <asp:Image ID="imgRightTopImage" runat="server" SkinID="sknRightTopImage" AlternateText="" />
                <asp:Image ID="imgBoxLeftBottom" runat="server" SkinID="sknBoxLeftBottom" AlternateText="" />
                <asp:Image ID="imgBoxRightBottom" runat="server" SkinID="sknBoxRightBottom" AlternateText="" />
            </div>
        </div>
    </div>

</div>