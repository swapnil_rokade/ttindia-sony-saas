﻿<%-- 
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: RequisitionEditor.ascx
    Description: This is the user control page used to create new requisition.
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1             Sep-24-2008          Jagadish            Defect Id: 8674. Changed the label text form "Tele Communication" to 
                                                             "Telecommuting".    
    0.2             Oct-7-2008           Gopala Swamy J      Defect Id: 8962 I put one anchor tag which opens another window in separate browser
    0.3             Apr-02-2009          Rajendra Prasad     Defect ID: 10242; Added textbox in the place of dropdownlist to display No. of openings.                                                             
    0.4             Apr-02-2009          Sandeesh            Defect ID: 9226; Added validation control to validate the 'start date'          
    0.5             Apr-06-2009          Jagadish            Defect id: 10191; Restricted zip code text field to 6 characters.     
    0.6             Apr-13-2009          Rajendra Prasad     Defect ID: 10299; Added Range Validator for the Textbox "txtNoOfOpenings".                                                             
    0.7             Apr-20-2009             Nagarathna       DEfectId:9200 Added customvalidator. 
    0.8             May-13-2010          Sudarshan.R.        DefectID: 12809; changed min and max experience validator from integer to double
--------------------------------------------------------------------------------------------------------------------------------------------
--%>
<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RequisitionEditor.ascx.cs"
    Inherits="TPS360.Web.UI.cltRequisitionEditor" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Assembly="Infragistics35.Web.v12.2, Version=12.2.20122.1007, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.Web.UI.EditorControls" TagPrefix="ig" %>
<%@ Register Assembly="Infragistics35.Web.v12.2, Version=12.2.20122.1007, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.Web.UI.LayoutControls" TagPrefix="ig" %>
<%@ Register Src="~/Controls/CountryStatePicker.ascx" TagName="CountryState" TagPrefix="ucl" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Src="~/Controls/MultipleItemPicker.ascx" TagName="MultipleSelection"
    TagPrefix="ucl" %>
<link href="../assets/css/chosen.css" rel="Stylesheet" />

<script type="text/javascript" language="javascript" src="../js/AjaxScript.js"></script>

<script type="text/javascript" language="javascript" src="../js/AjaxVariables.js"></script>

<script type="text/javascript" src="http://code.jquery.com/jquery-1.8.0.min.js"></script>

<style>
    .text-label
    {
        color: #cdcdcd;
        font-weight: bold;
        font-size: small;
    }
</style>
<style>
    .EmailDiv
    {
        vertical-align: middle;
        text-align: left;
        border: solid 1px #CCC;
        padding-bottom: 2px;
        padding-top: 2px;
        margin-right: 4px;
        margin-bottom: 4px;
        width: 80%;
        background-image: -moz-linear-gradient(center top , #FFFFFF 20%, #F6F6F6 50%, #EEEEEE 52%, #F4F4F4 100%);
        border-radius: 3px;
    }
    .secondarySkillDiv
    {
        vertical-align: middle;
        text-align: left;
        border: solid 1px #CCC;
        padding-bottom: 2px;
        padding-top: 2px;
        margin-right: 4px;
        margin-bottom: 4px;
        width: 80%;
        background-image: -moz-linear-gradient(center top , #FFFFFF 20%, #F6F6F6 50%, #EEEEEE 52%, #F4F4F4 100%);
        border-radius: 3px;
    }
</style>
<%--<script runat ="server" >
    void ValidateEducationList(object sender, EventArgs e)
    {
        if (uclEducationList.ListItem.Items.Cast<ListItem>().Where(x => x.Selected == true).Count() == 0)
        {
            cvEduQualification.IsValid = false;
        }
    }
</script>--%>

<script language="javascript" type="text/javascript">
    $(document).ready(
            function() {
                function thisFunction() {
                    var Opendate = $find("<%=wdcOpenDate.ClientID%>").get_value();
                    var Enddate = $find("<%=wdcClosingDate.ClientID%>").get_value();

                    if (Enddate == null) {
                        var numberOfDaysToAdd = 30;
                        var TempDate = new Date();
                        TempDate.setDate(Opendate.getDate() + numberOfDaysToAdd);
                        var dd = TempDate.getDate();
                        var mm = TempDate.getMonth() + 1;
                        var y = TempDate.getFullYear();

                        var FormattedDate = mm + '/' + dd + '/' + y;
                        $find("<%=wdcClosingDate.ClientID%>").set_value(FormattedDate);
                    }
                }
                $(window).load(thisFunction);
            }
);
</script>

<script language="javascript" type="text/javascript">

    function Clicked(email, parent) {
        //$('#<%= hfSkillSetNames.ClientID %>').val($('#<%= hfSkillSetNames.ClientID %>').val().replace('<item><skill>' +email + '</skill></item>','').replace(',,',','));
        $('#<%= hfSkillSetNames.ClientID %>').val($('#<%= hfSkillSetNames.ClientID %>').val().replace('<item><skill>' + email + '</skill></item>', '').replace(',,', ','));

    }
    function validateEmail(email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }

    function SkillAlreadyAdded(skill) {
        var result = false;
        // var skilllist= $('#<%= hfSkillSetNames.ClientID %>').val();
        var skilllist = $('#<%= hfSkillSetNames.ClientID %>').val().replace(/&/g, "-");
        xmlDoc = $.parseXML("<SkillList>" + skilllist + "</SkillList>");
        var $xml = $(xmlDoc);

        $xml.find("item").each(function() {

            // if($(this).find('skill').text().trim().toLowerCase()==skill .trim().toLowerCase()) result =true ;
            if ($(this).find('skill').text().trim().toLowerCase() == skill.trim().toLowerCase().replace(/&/g, "-")) result = true;
        });
        return result;
    }

    //-----------------------------Rishi Code Start--------------------------------------------------//
    function SecondarySkillAlreadyAdded(skill) {
        var result = false;
        var skilllist = $('#<%= hfSecondarySkillSetNames.ClientID %>').val().replace(/&/g, "-");
        xmlDoc = $.parseXML("<SkillList>" + skilllist + "</SkillList>");
        var $xml = $(xmlDoc);

        $xml.find("item").each(function() {
            if ($(this).find('skill').text().trim().toLowerCase() == skill.trim().toLowerCase().replace(/&/g, "-")) result = true;
        });
        return result;
    }

    function SecondarySkillClicked(email, parent) {
        $('#<%= hfSecondarySkillSetNames.ClientID %>').val($('#<%= hfSecondarySkillSetNames.ClientID %>').val().replace('<item><skill>' + email + '</skill></item>', '').replace(',,', ','));
    }
    //------------------------Rishi Code End--------------------------------------------------------//


    Sys.Application.add_load(function() {
        $('.close').click(function() {
            alert($(this).previous().val());
        });
        $('#<%= btnAddSkill.ClientID %>').click(function() {

            var txtSkillName = $('#<%= txtSkillName.ClientID %>');
            if (txtSkillName.val().trim() != '') {

                if (!SkillAlreadyAdded(txtSkillName.val())) {
                    var text = txtSkillName.val();
                    $('#<%= divContent.ClientID %>').append("<div class='EmailDiv'><span>" + text + "</span><button type='button' class='close' data-dismiss='alert' onclick=javascript:Clicked('" + text + "')>\u00D7</button></div>");
                    $('#<%= hfSkillSetNames.ClientID %>').val($('#<%= hfSkillSetNames.ClientID %>').val() + '<item><skill>' + text + '</skill></item>');
                    txtSkillName.val('');
                }
                else ShowMessage('<%= lblMessage.ClientID %>', 'Skill already added', true);

            }
            return false;
        });

        //--------------------------Rishi Code Start----------------------------------------//
        $('#<%= btnAddSecondarySkill.ClientID %>').click(function() {

            var txtSecondarySkillName = $('#<%= txtSecondarySkillName.ClientID %>');
            if (txtSecondarySkillName.val().trim() != '') {

                if (!SecondarySkillAlreadyAdded(txtSecondarySkillName.val())) {
                    var text = txtSecondarySkillName.val();
                    $('#<%= divSecondarySkillContent.ClientID %>').append("<div class='secondarySkillDiv'><span>" + text + "</span><button type='button' class='close' data-dismiss='alert' onclick=javascript:SecondarySkillClicked('" + text + "')>\u00D7</button></div>");
                    $('#<%= hfSecondarySkillSetNames.ClientID %>').val($('#<%= hfSecondarySkillSetNames.ClientID %>').val() + '<item><skill>' + text + '</skill></item>');
                    txtSecondarySkillName.val('');
                }
                else ShowMessage('<%= lblMessage.ClientID %>', 'Skill already added', true);

            }
            return false;
        });
        //--------------------------Rishi Code End------------------------------------------//


    });


    function RemoveRow(row) {

        $(row).parent().parent().remove();
    }

    Sys.Application.add_load(function() {
        $('input[rel=Numeric]').keypress(function(event) { return allownumeric(event, $(this)); });
        $('input[rel=Integer]').keypress(function(event) { return allowInteger(event, $(this)); });
        // SetDefaultText (document .getElementById ('<%=txtSalary.ClientID %>'));
        //     SetDefaultText (document .getElementById ('<%=txtMaxPayRate.ClientID %>'));
    });
    function RemoveDefaultText(e) {
        if (e.value == e.title) { e.value = ''; e.className = 'CommonTextBox'; }
    }
    function SetDefaultText(e) {
        if (e.value == '') {
            e.value = e.title;
            e.className = 'text-label';
        }
    }
    function ValidateSkill(souce, eventArgs) {
        if ($('#<%= hfSkillSetNames.ClientID %>').val() == '')
            eventArgs.IsValid = false;
    }
    function CompanyContact_Selected(source, eventArgs) {
        $('#divContactEmaill').css({ 'display': 'none' });

        hdnSelectedContactText = document.getElementById("<%= hdnSelectedContactText.ClientID%>");
        hdnSelectedContactValue = document.getElementById("<%= hdnSelectedContactValue.ClientID%>");
        hdnSelectedContactText.value = eventArgs.get_text();
        hdnSelectedContactValue.value = eventArgs.get_value();
    }
    function ValidatePayRate() {

    }

    function MakeEnableDisable(option, control, clearSelectIndex) {
        var _Control = document.getElementById(control);
        _Control.disabled = option;
        if (option && clearSelectIndex) {
            _Control.selectedIndex = 0;
        }
    }



    function checkEnter(e) {
        try {
            var keycode;
            if (window.event) keycode = window.event.keyCode;
            else if (event) keycode = event.keyCode;
            else if (e) keycode = e.which;
            else return true;

            if ((keycode == 13)) {
                var s;
                s = document.getElementById(btnSave);
                s.UseSubmitBehaviour = true;

                //__doPostBack('btnSave','OnClick');
                return true;
            }
            else {
                return false;
            }
            return true;
        }
        catch (e1) {
        }
    }

    //0.7 statrs

    function fnValidateSalary(source, args) {
        // debugger;

        var txtValidCurrency = $get('<%= txtSalary.ClientID %>').value.trim();
        if ((txtValidCurrency != "" && (!isNaN(txtValidCurrency) || txtValidCurrency == $get('<%= txtSalary.ClientID %>').title))) // || (txtValidMaxCurrency != "" && txtValidMaxCurrency > 0 && !isNaN(txtValidMaxCurrency)))   
        {
            if (args.Value == 0)
                args.IsValid = false;
            else
                args.IsValid = true;
        }
        else args.IsValid = false;
    }

    function fnValidateMaxPayRate(source, args) {
        // debugger;
        var txtValidCurrency = $get('<%= txtMaxPayRate.ClientID %>').value.trim();
        if ((txtValidCurrency != "" && (!isNaN(txtValidCurrency) || txtValidCurrency == $get('<%= txtMaxPayRate.ClientID %>').title))) // || (txtValidMaxCurrency != "" && txtValidMaxCurrency > 0 && !isNaN(txtValidMaxCurrency)))   
        {
            if (args.Value == 0)
                args.IsValid = false;
            else
                args.IsValid = true;
        }
        else args.IsValid = false;
    }

    function FilterNumeric(txtID) {

        var txt = document.getElementById(txtID);
        var t = txt.value + '';
        for (var i = 0; i < t.length; i++) {
            var st = t.substr(i, 1);
            if (st >= 0 && st <= 9) continue;
            else { txt.value = ""; break; }
        }

    }

    function numeric_only(evt) {
        var e = event || evt; // for trans-browser compatibility
        var charCode = e.which || e.keyCode;

        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;

    }
    function Decimal_only(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;
        if ((charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 46))
            return false;

        return true;
    }


    function LockWheel() {
        document.onmousewheel = function() { stopWheel(); } /* IE7, IE8 */
        if (document.addEventListener) { /* Chrome, Safari, Firefox */
            document.addEventListener('DOMMouseScroll', stopWheel, false);
        }
    }

    function stopWheel(e) {
        if (!e) { e = window.event; } /* IE7, IE8, Chrome, Safari */
        if (e.preventDefault) { e.preventDefault(); } /* Chrome, Safari, Firefox */
        e.returnValue = false; /* IE7, IE8 */
    }
    function ContactfocusLost() {

        if ($('#<%=hdnSelectedContactText.ClientID %>').val() != $('#<%=txtBUcontact.ClientID %>').val()) {
            $('#<%=hdnSelectedContactValue.ClientID %>').val('0');
        }
        var hdnSelectedClientValue = document.getElementById('<%=hdnSelectedContactValue.ClientID %>');
        if (hdnSelectedClientValue.value.trim() == '' || hdnSelectedClientValue.value == '0') {
            $('#divContactEmaill').css({ 'display': '' });
            $('#<%=txtContactEmail.ClientID %>').focus();
        }
    }
    function ReleaseWheel() {
        document.onmousewheel = null;  /* IE7, IE8 */
        if (document.addEventListener) { /* Chrome, Safari, Firefox */
            document.removeEventListener('DOMMouseScroll', stopWheel, false);
        }
    }

    function RemoveCalender() {
        //var control = $find('<%= wdcStartDate.ClientID %>');  
        var control = document.getElementById('<%= wdcStartDate.ClientID %>');
        control.visible = false;

    }

    function RemoveResourceCalender() {
        //var control = $find('<%= wdcStartDate.ClientID %>');  
        var control = document.getElementById('<%= wdcResourceStartDate.ClientID %>');
        control.visible = false;

    }
    //for LandT
    function ValidateChkList(source, args) {
        var chkListaTipoModificaciones = document.getElementById('<%= ddlQualification.ClientID %>');
        var chkLista = chkListaTipoModificaciones.getElementsByTagName("input");
        for (var i = 0; i < chkLista.length; i++) {
            if (chkLista[i].checked) {
                args.IsValid = true;
                return;
            }
        }
        args.IsValid = false;
    }
    //for LandT
    Sys.Application.add_load(function() {

        //  $('.TableRow').show();
        // $('#divContactEmail').css({'display':'none'});

        if ($('#<%=hdnApplicationType.ClientID %>').val() == '') {
            $('.RequiredField').css({ 'display': 'none' });
            $('#spjobtitle').css({ 'display': '' });
            $('#SpBUContacts').css({ 'display': '' });
            $('#SpBU').css({ 'display': '' });
            $('#splocation').css({ 'display': '' });
            $('#SpPositionRequestReason').css({ 'display': '' });

            $('#SpResourceStartDate').css({ 'display': '' });
            $('#SpResourceEndDate').css({ 'display': '' });           
        }

    });
    //For BU contact
    Sys.Application.add_load(function() {
        var hdnSelectedClientValue = document.getElementById('<%=hdnSelectedContactValue.ClientID %>');
        $('#<%=ddlBU.ClientID %>').change(function() {
            if ($('#<%=ddlBU.ClientID %>').prop("selectedIndex") == 0) {
                $('#<%=txtBUcontact.ClientID %>').prop("disabled", true);
                $('#<%=txtBUcontact.ClientID %>').val('');
                $('#<%=hdnSelectedContactText.ClientID %>').val('');
                if (hdnSelectedClientValue.value.trim() == '' || hdnSelectedClientValue.value == '0') {
                    $('#divContactEmaill').css({ 'display': 'none' });
                    $('#<%=txtContactEmail.ClientID %>').val('');
                }

            } else {
                $('#<%=txtBUcontact.ClientID %>').prop("disabled", false);
                $('#<%=txtContactEmail.ClientID %>').attr("disabled", false);

            }
        });

    });


    function RemoveRow(row) {

        $(row).parent().parent().remove();
    }
    function ClientSelected(source, eventArgs) {


        hdnSelectedClient = document.getElementById('<%= hdnSelectedClient.ClientID %>');
        hdnSelectedClientText = document.getElementById('<%=hdnSelectedClientText.ClientID %>');
        hdnSelectedClientText.value = eventArgs.get_text();
        hdnSelectedClient.value = eventArgs.get_value();

    }
    function ValidateExperience(source, eventArgs) {
        var MinExp = document.getElementById('<%= txtExpRequiredMin.ClientID %>');
        var MaxExp = document.getElementById('<%= txtExpRequiredMax.ClientID %>');
        if (MinExp.value != '' || MaxExp.value != '') {
            eventArgs.IsValid = true;
        } else {
            eventArgs.IsValid = false;
        }

    }

    function wdcOpenDate_ValueChanged(sender, eventArgs) {
        //var Opendate = $find("<%=wdcOpenDate.ClientID%>").get_value();
        ////    var TempDate= new Date(Opendate);
        ////    var numberOfDaysToAdd = 30;
        ////    TempDate.setDate(Opendate.getDate() + numberOfDaysToAdd);
        ////    var temp=TempDate.getDate();
        ////    if(temp>30)
        ////    {
        ////    //TempDate.setMonth(Opendate.getMonth());
        ////    var mm = TempDate.getMonth()+1;
        ////    }
        ////    else
        ////    {
        ////    TempDate.setMonth(Opendate.getMonth()+2);
        ////    var mm = TempDate.getMonth();
        ////    }
        ////    var dd = TempDate.getDate();
        ////   
        ////   // var mm = TempDate.getMonth();
        ////    var y = TempDate.getFullYear();

        ////    var FormattedDate = dd + '/'+ mm + '/'+ y;
        ////    alert(FormattedDate);
        ////    $find("<%=wdcClosingDate.ClientID%>").set_value(FormattedDate);
        ////    
        var Opendate = $find("<%=wdcOpenDate.ClientID%>").get_value();
        var TempDate = new Date(Opendate);
        var numberOfDaysToAdd = 30;
        TempDate.setDate(Opendate.getDate() + numberOfDaysToAdd);

        var dd = TempDate.getDate();
        var mm = TempDate.getMonth() + 1;
        var y = TempDate.getFullYear();

        var FormattedDate = dd + '/' + mm + '/' + y;

        $find("<%=wdcClosingDate.ClientID%>").set_value(FormattedDate);
        //    

    }

    //0.7  ends
</script>

<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
<meta name="CODE_LANGUAGE" content="C#">
<meta name="vs_defaultClientScript" content="JavaScript">
<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">

<script type="text/javascript" language="javascript" src="../js/AjaxVariables.js"></script>

<script type="text/javascript" language="javascript" src="../js/AjaxScript.js"></script>

<asp:HiddenField ID="hdnApplicationType" runat="server" Value="LandT" />
<asp:HiddenField ID="hdnSelectedClientText" runat="server" />
<asp:HiddenField ID="hdnSelectedClient" runat="server" Value="0" />


<div style="margin: 0 auto;">
    <asp:Panel ID="pnlRequisition" runat="server">
        <div class="TableRow" style="text-align: left" id="div" runat="server">
            <asp:UpdatePanel ID="l" runat="server">
                <ContentTemplate>
                    <asp:Label ID="lblMessage" runat="server" EnableViewState="false"></asp:Label>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div class="TableRow">
            <div class="FormLeftColumn">
            
            <asp:Panel ID="pnlRequisition1" runat="server">
            
            
                <div class="TableRow">
                    <asp:Panel ID="pnlJobtitle" runat="server">
                        <div class="TableFormLeble">
                            <asp:Label ID="lblJobTitle" runat="server" Text="Requisition Title"></asp:Label>:
                        </div>
                        <div class="TableFormContent">
                            <asp:TextBox ID="txtJobTitle" runat="server" CssClass="CommonTextBox" MaxLength="60"
                                TabIndex="1" ValidationGroup="ValGrpJobTitle" onkeypress="checkEnter(event)"></asp:TextBox>
                            <span class="RequiredField" id="spjobtitle">*</span>
                        </div>
                    </asp:Panel>
                </div>
                <div class="TableRow">
                    <div class="TableFormValidatorContent" style="margin-left: 42%">
                        <asp:RequiredFieldValidator ID="rfvJobTitle" runat="server" ControlToValidate="txtJobTitle"
                            SetFocusOnError="true" ErrorMessage="Please Enter Requisition title." Display="Dynamic"
                            ValidationGroup="ValGrpJobTitle"></asp:RequiredFieldValidator>
                    </div>
                </div>
                
                <div class="TableRow">
                    <div class="TableFormLeble">
                        <asp:Label ID="Label3" runat="server" Text="Demand Type"></asp:Label>:
                    </div>
                    
                    
                    
                    <asp:UpdatePanel ID="upselected" runat="server">
                        <ContentTemplate>
                            <div class="TableFormContent" style="vertical-align: top">
                                <asp:DropDownList ID="ddlDemandType" AutoPostBack="true" OnSelectedIndexChanged="OnSelectedIndexChanged"
                                    runat="server" CssClass="chzn-select" TabIndex="2" Width="40%">
                                </asp:DropDownList>
                                <span class="" style="font-family: Tahoma; font-size: 10pt; color: #FF0000;">*</span>
                            </div>
                            </div>
                            
                    
                    
                       <div class="TableRow">
                            <div class="TableFormValidatorContent" style="margin-left: 42%">
                           <asp:RequiredFieldValidator ID="teqDemandType" runat="server" ControlToValidate="ddlDemandType"
                            InitialValue="0" SetFocusOnError="true" ErrorMessage="Please Select Demand Type."
                            Display="Dynamic" ValidationGroup="ValGrpJobTitle"></asp:RequiredFieldValidator>
                        </div>
                        </div>
                    
                            <%-- Changes start by vishal tripathy--%>
                            <div class="TableRow">
                                <div class="TableFormLeble">
                                    <asp:Label ID="lblGradeType" runat="server" Visible="false" Text="Grade :"></asp:Label>
                                </div>
                                <div class="TableFormContent" style="vertical-align: top">
                                    <asp:DropDownList ID="ddlGrade"  TabIndex="3" runat="server" Visible="false" CssClass="chzn-select"
                                        Width="40%">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="ddlDemandType" />
                        </Triggers>
                    </asp:UpdatePanel>
                    <%--  Changes end  by vishal tripathy--%>
                   <%-- <div class="TableRow" align="center" style="padding-left: 77px">
                        <asp:RequiredFieldValidator ID="teqDemandType" runat="server" ControlToValidate="ddlDemandType"
                            InitialValue="0" SetFocusOnError="true" ErrorMessage="Please Select Demand Type."
                            Display="Dynamic" ValidationGroup="ValGrpJobTitle"></asp:RequiredFieldValidator>
                    </div>--%>
                    
                    
                    <div class="TableRow">
                        <asp:Panel ID="Panel4" runat="server">
                            <div class="TableFormLeble">
                                <asp:Label ID="Label11" runat="server" Text="Reason for Hire"></asp:Label>:
                            </div>
                            <div class="TableFormContent">
                                <asp:TextBox ID="txtAreaReasonForHire" runat="server" CssClass="CommonTextBox" TextMode="multiline"
                                     TabIndex="4" Columns="80" Rows="5"></asp:TextBox>
                                <span class="" style="font-family: Tahoma; font-size: 10pt; color: #FF0000; vertical-align: top;">
                                    *</span>
                                <!-- <span class="" style="font-family: Tahoma; font-size: 10pt; color: #FF0000;" id="Span4">
                                *</span> -->
                            </div>
                        </asp:Panel>
                        <div class="TableRow">
                            <div class="TableFormValidatorContent" style="margin-left: 42%">
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtAreaReasonForHire"
                                    ValidationGroup="ValGrpJobTitle" Text="Please Enter Reason To Hire."></asp:RequiredFieldValidator>
                            </div>
                        </div>
                    </div>
                    
                    
                       <div class="TableRow">
                        <div class="TableFormLeble">
                            <asp:Label EnableViewState="false" ID="lblRequisitionType" runat="server" Text="Position Request Reason"></asp:Label>:
                        </div>
                        <div class="TableFormContent" style="vertical-align: top">
                            <asp:DropDownList ID="ddlRequisitionType" runat="server" CssClass="chzn-select" TabIndex="5"
                                Width="40%">
                            </asp:DropDownList>
                           <span class="RequiredField" id="SpPositionRequestReason">*</span>
                        </div>
                       </div>
                        <div class="TableRow">
                            <div class="TableFormValidatorContent" style="margin-left: 42%">
                                <asp:RequiredFieldValidator ID="rfvPosReqReason" runat="server" ControlToValidate="ddlRequisitionType"
                                    ErrorMessage="Please select Position Request Reason." ValidationGroup="ValGrpJobTitle" Display="Dynamic"
                                    InitialValue="0"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                    
                    
                      <div class="TableRow">
                        <div class="TableFormLeble">
                            <asp:Label ID="Label14" runat="server" Text="Billability"></asp:Label>:
                        </div>
                        <div class="TableFormContent" style="vertical-align: top">
                            <asp:DropDownList ID="ddlBillability" runat="server" CssClass="chzn-select" TabIndex="6" 
                                Width="40%">
                            </asp:DropDownList>
                            <span class="RequiredField">*</span>
                        </div>
                    </div>
                    
                      <div class="TableRow">
                        <div class="TableFormLeble">
                            <asp:Label ID="Label13" runat="server" Text="Priority"></asp:Label>:
                        </div>
                        <div class="TableFormContent" style="vertical-align: top">
                            <asp:DropDownList ID="ddlPriority" runat="server" CssClass="chzn-select" TabIndex="7"
                                Width="40%">
                            </asp:DropDownList>
                            <span class="RequiredField">*</span>
                        </div>
                    </div>
                  <div id="div4" runat="server" style="display: none">
                   <div class="TableRow">
                        <div class="TableFormLeble">
                            <asp:Label ID="lblOpenDate" runat="server" Text="Availability Date"></asp:Label>:
                        </div>
                        <div class="TableFormContent" style="white-space: nowrap">
                            <div>
                                <div style="float: left" id="divOpenDate" runat="server">
                                    <%--<igsch:WebDateChooser ID="wdcOpenDate" runat="server" NullDateLabel="" EnableKeyboardNavigation="True"
                                    FocusOnInitialization="True" TabIndex="6" Editable="true">
                                    <CalendarLayout HideOtherMonthDays="True">
                                    </CalendarLayout>
                                </igsch:WebDateChooser>--%>
                                    <ig:WebDatePicker ID="wdcOpenDate" DropDownCalendarID="ddcOpenDate" runat="server" TabIndex="8">
                                        <ClientSideEvents ValueChanged="wdcOpenDate_ValueChanged" />
                                    </ig:WebDatePicker>
                                    <ig:WebMonthCalendar ID="ddcOpenDate" AllowNull="true" HideOtherMonthDays="true"
                                        runat="server">
                                    </ig:WebMonthCalendar>
                                </div>
                                <span class="RequiredField">*</span>
                            </div>
                        </div>
                    </div>
                 </div>
                
                </asp:Panel>
                
                
                 <asp:Panel ID="pnlRequisition2" runat="server"> 
                     <div class="TableRow" id="DivResourceStartDate" runat="server">
                        <div class="TableFormLeble">                           
                            <asp:Label ID="Label12" runat="server" Text="Resource Start Date"></asp:Label>:
                        </div>
                        <div class="TableFormContent" style="white-space: nowrap">
                            <div>
                               <div style="float: left" id="div5" runat="server">                      
                                    <ig:WebDatePicker ID="wdcResourceStartDate" DropDownCalendarID="ddcResourceStartDate" runat="server" TabIndex="9">
                                    </ig:WebDatePicker>
                                    <ig:WebMonthCalendar ID="ddcResourceStartDate" HideOtherMonthDays="true" AllowNull="true"
                                        runat="server">
                                    </ig:WebMonthCalendar>
                                </div>
                                <span class="RequiredField" id="SpResourceStartDate">*</span>                               
                                <div id="div6" runat="server" style="display: none">
                                    <div align="left" style="float: left">
                                        <asp:CheckBox ID="CheckBox1" runat="server" Text="ASAP" AutoPostBack="true" OnCheckedChanged="chkStartDate_CheckedChanged"
                                            TabIndex="5" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                  <div class="TableRow">
                        <div class="TableFormValidatorContent" style="margin-left: 42%">
                           <%-- <asp:CompareValidator ID="cmpResourceStartDate" runat="server" Operator="GreaterThanEqual"
                                Type="Date" ControlToValidate="wdcResourceStartDate" ErrorMessage="Resource Start date should not be before the current date"
                                Display="Dynamic" />--%>
                          <%--  <asp:CompareValidator ID="cmpResourceStartDate1" runat="server" Operator="GreaterThanEqual"
                                Type="Date" ControlToValidate="wdcResourceStartDate" ErrorMessage="Resource Start date should not be before the current date"
                                Display="Dynamic" />--%>
                            <asp:RequiredFieldValidator ID="rfvResourceStartDate" runat="server" ControlToValidate="wdcResourceStartDate"
                                SetFocusOnError="true" ErrorMessage="Please select Resource Start date." Display="Dynamic"
                                ValidationGroup="ValGrpJobTitle"></asp:RequiredFieldValidator>
                        </div>
                    </div>                    
                     <div class="TableRow">
                        <div class="TableFormValidatorContent" style="margin-left: 42%">
                           <%-- <asp:CompareValidator ID="cmpResourceDate1" ControlToCompare="wdcStartDate" ControlToValidate="wdcResourceStartDate"
                                Type="Date" Operator="GreaterThanEqual" ErrorMessage="Resource Start date should not be before the Req. Open Date"
                                ValidationGroup="ValGrpJobTitle" Display="Dynamic" runat="server"></asp:CompareValidator>--%>
                        </div>
                    </div>
                 <%-- <div class="TableRow">
                        <div class="TableFormValidatorContent" style="margin-left: 42%">
                            <asp:CompareValidator ID="CompareValidator2" runat="server" Operator="GreaterThanEqual"
                                Type="Date" ControlToValidate="wdcOpenDate" ErrorMessage="Open date should not be before the current date"
                                Display="Dynamic" ValidationGroup="ValGrpJobTitle" />
                            <asp:CompareValidator ID="CompareValidator3" runat="server" Operator="LessThanEqual"
                                Type="Date" ControlToValidate="wdcOpenDate" ErrorMessage="Open date should not be after the expected fullfillment date"
                                Display="Dynamic" ControlToCompare="wdcClosingDate" ValidationGroup="ValGrpJobTitle" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="wdcOpenDate"
                                SetFocusOnError="true" ErrorMessage="Please select open date." Display="Dynamic"
                                ValidationGroup="ValGrpJobTitle"></asp:RequiredFieldValidator>
                        </div>
                    </div>--%>
                   <div class="TableRow" id="DivResourceEndtDate" runat="server">
                        <div class="TableFormLeble">                     
                            <asp:Label ID="Label22" runat="server" Text="Resource End Date"></asp:Label>:
                        </div>
                        <div class="TableFormContent" style="white-space: nowrap">
                            <div>
                                <div style="float: left" id="div7" runat="server">                              
                                    <ig:WebDatePicker ID="wdcResourceEndDate" DropDownCalendarID="ddcResourceClosingDate" runat="server" TabIndex="10">
                                    </ig:WebDatePicker>
                                    <ig:WebMonthCalendar ID="ddcResourceClosingDate" AllowNull="true" HideOtherMonthDays="true"
                                        runat="server">
                                    </ig:WebMonthCalendar>
                                </div>
                                <span class="RequiredField" id="SpResourceEndDate">*</span>  
                            </div>
                        </div>
                    </div>
                    <div class="TableRow">
                        <div class="TableFormValidatorContent" style="margin-left: 42%">
                            <asp:RequiredFieldValidator ID="rfvResourceClosingDate" runat="server" ControlToValidate="wdcResourceEndDate"
                                SetFocusOnError="true" ErrorMessage="Please Select Resource End Date."
                                Display="Dynamic" ValidationGroup="ValGrpJobTitle"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="TableRow">
                        <div class="TableFormValidatorContent" style="margin-left: 42%">
                            <asp:CompareValidator ID="cmpResourceDate" ControlToCompare="wdcResourceStartDate" ControlToValidate="wdcResourceEndDate"
                                Type="Date" Operator="GreaterThanEqual" ErrorMessage="Resource End Date Should not before Resource Start Date."
                                ValidationGroup="ValGrpJobTitle" Display="Dynamic" runat="server"></asp:CompareValidator>
                        </div>
                    </div>
                    
                  </asp:Panel>
                    
                    
                    
<%--                    
  <asp:Panel ID="pnlRequisition2" runat="server">     --%>             
                    
                       
            <asp:Panel ID="pnlRequisition3" runat="server"> 
                        <asp:UpdatePanel ID="upSkill" runat="server">
                        <ContentTemplate>
                            <asp:Panel ID="pnlSkill" runat="server" DefaultButton="btnAddSkill">
                                <div class="TableRow">
                                    <div class="TableFormLeble">
                                        <asp:Label ID="lblSkills" runat="server" Text="Primary Skills" ></asp:Label>:
                                    </div>
                                    <div class="TableFormContent">
                                        <div id="divskill">
                                        </div>
                                        <asp:HiddenField ID="hfSkillSetNames" runat="server" Value="" />
                                        <asp:TextBox ID="txtSkillName" runat="server" AutoPostBack="false" CssClass="CommonTextBox"
                                            TabIndex="11" AutoComplete="OFF" onclick="LockWheel();" onBlur="ReleaseWheel();" />
                                        <ajaxToolkit:AutoCompleteExtender ID="txtSkillName_AutoCompleteExtender" runat="server"
                                            ServicePath="~/AjaxExtender.asmx" TargetControlID="txtSkillName" MinimumPrefixLength="1"
                                            ServiceMethod="GetSkills" EnableCaching="True" CompletionInterval="0" CompletionListCssClass="AutoCompleteBox"
                                            FirstRowSelected="True" CompletionListElementID="divskill" />
                                        <span class="" style="font-family: Tahoma; font-size: 10pt; color: #FF0000;" id="Span4">
                                            *</span>
                                        <asp:Button ID="btnAddSkill" runat="server" Text="Add" CssClass="CommonButton" TabIndex="12" />
                                    </div>
                                </div>
                            </asp:Panel>
                            <div class="TableRow">
                                <div class="TableFormValidatorContent" style="margin-left: 42%">
                                    <asp:CustomValidator ID="cvSkill" runat="server" ValidationGroup="ValGrpJobTitle"
                                        Display="Dynamic" ClientValidationFunction="ValidateSkill" ErrorMessage="Please select atleast one skill"></asp:CustomValidator>
                                    <asp:CustomValidator ID="CustomValidator1" runat="server" ValidationGroup="ValGrpJobTitle"
                                        Display="Dynamic" ClientValidationFunction="ValidateSkill" ErrorMessage="Please select atleast one skill"></asp:CustomValidator>
                                </div>
                            </div>
                            <div class="TableRow">
                                <div class="TableFormLeble">
                                </div>
                                <div class="TableFormContent" id="divContent" runat="server" enableviewstate="true"
                                    style="margin-left: 42%">
                                </div>
                            </div>
                        </ContentTemplate>
                       </asp:UpdatePanel>
                    
                    
                    <asp:UpdatePanel ID="secondarySkill" runat="server">
                        <ContentTemplate>
                            <asp:Panel ID="pnlSecondarySkill" runat="server" DefaultButton="btnAddSecondarySkill">
                                <div class="TableRow">
                                    <div class="TableFormLeble">
                                        <asp:Label ID="lblSecondarySkill" runat="server" Text="Secondary Skills"></asp:Label>:
                                    </div>
                                    <div class="TableFormContent">
                                        <div id="divSecondarySkill">
                                        </div>
                                        <asp:HiddenField ID="hfSecondarySkillSetNames" runat="server" Value="" />
                                        <asp:TextBox ID="txtSecondarySkillName" runat="server" AutoPostBack="false" CssClass="CommonTextBox"
                                            TabIndex="13" AutoComplete="OFF" onclick="LockWheel();" onBlur="ReleaseWheel();" />
                                        <ajaxToolkit:AutoCompleteExtender ID="txtSecondarySkillName_AutoCompleteExtender"
                                            runat="server" ServicePath="~/AjaxExtender.asmx" TargetControlID="txtSecondarySkillName"
                                            MinimumPrefixLength="1" ServiceMethod="GetSkills" EnableCaching="True" CompletionInterval="0"
                                            CompletionListCssClass="AutoCompleteBox" FirstRowSelected="True" CompletionListElementID="divSecondarySkill" />
                                        <asp:Button ID="btnAddSecondarySkill" runat="server" Text="Add" CssClass="CommonButton"
                                            TabIndex="14" />
                                    </div>
                                </div>
                            </asp:Panel>
                            <div class="TableRow">
                                <div class="TableFormValidatorContent" style="margin-left: 42%">
                                </div>
                            </div>
                            <div class="TableRow">
                                <div class="TableFormLeble">
                                </div>
                                <div class="TableFormContent" id="divSecondarySkillContent" runat="server" enableviewstate="true"
                                    style="margin-left: 42%">
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    
                    <div class="TableRow">
                        <div class="TableFormLeble">
                            <asp:Label ID="lblExpRequired" runat="server" Text="Experience Required (yrs)"></asp:Label>:
                        </div>
                        <div class="TableFormContent">
                            <div style="float: left;">
                                <div style="float: left;">
                                    <asp:Label ID="lblExpRequiredMin" runat="server" Text="Min"></asp:Label>
                                </div>
                                <div style="float: left; padding-left: 4px;">
                                    <asp:TextBox ID="txtExpRequiredMin" runat="server" CssClass="CommonTextBox" rel="Integer"
                                        Width="35px" MaxLength="3" TabIndex="15" ValidationGroup="ValGrpJobTitle"></asp:TextBox>
                                </div>
                            </div>
                            <div style="float: left;">
                                <div style="float: left;">
                                    <asp:Label ID="lblExpRequiredMax" runat="server" Text="Max"></asp:Label>
                                </div>
                                <div style="float: left; padding-left: 4px;">
                                    <asp:TextBox ID="txtExpRequiredMax" runat="server" CssClass="CommonTextBox" rel="Integer"
                                        Width="35" MaxLength="3" TabIndex="16" ValidationGroup="ValGrpJobTitle"></asp:TextBox>
                                    <span class="" style="font-family: Tahoma; font-size: 10pt; color: #FF0000;">*</span>
                                </div>
                            </div>
                            <div class="TableRow" style="padding-left: 130px">
                                 <asp:RequiredFieldValidator ID="reqExpMin" runat="server" ControlToValidate="txtExpRequiredMin"
                                    ValidationGroup="ValGrpJobTitle"  Display="Dynamic"  Text="Please Enter Min Experience"></asp:RequiredFieldValidator>&nbsp;
                                <asp:RequiredFieldValidator ID="reqExpRequiredmax" runat="server" ControlToValidate="txtExpRequiredMax"
                                    ValidationGroup="ValGrpJobTitle" Display="Dynamic"  Text="Please Enter Max Experience"></asp:RequiredFieldValidator>
                                
                            </div>
                        </div>
                    </div>
                    <div class="TableRow">
                        <div class="TableFormValidatorContent" style="margin-left: 42%">
                            <asp:CompareValidator ID="cvExpRequiredMin" runat="server" ControlToValidate="txtExpRequiredMin"
                                Display="Dynamic" ErrorMessage="Invalid Number" Operator="DataTypeCheck" Type="Double"></asp:CompareValidator>
                            <asp:CompareValidator ID="cvExpRequiredMax" runat="server" ControlToValidate="txtExpRequiredMax"
                                Display="Dynamic" ErrorMessage="Invalid Number" Operator="DataTypeCheck" Type="Double"></asp:CompareValidator>
                            <asp:CompareValidator ID="cmpExperienceValidator" runat="server" ControlToValidate="txtExpRequiredMax"
                                ControlToCompare="txtExpRequiredMin" Display="Dynamic" ErrorMessage="Minimum experience should be less than or equal to maximum experience<br/>"
                                Operator="GreaterThanEqual" ValidationGroup="ValGrpJobTitle" Type="Double"></asp:CompareValidator>
                            <asp:CustomValidator runat="server" Display="Dynamic" ValidationGroup="ValGrpJobTitle"
                                ID="cvExperience" ErrorMessage="Please Enter Min or Max Experience" ClientValidationFunction="ValidateExperience">
                            </asp:CustomValidator>
                        </div>
                    </div>
                    <div class="TableRow">
                        <div class="TableFormLeble">
                            <asp:Label ID="Label8" runat="server" Text="Relevant Experience Required (yrs)"></asp:Label>:
                        </div>
                        <div class="TableFormContent">
                            <div style="float: left;">
                                <div style="float: left;">
                                    <asp:Label ID="Label9" runat="server" Text="Min"></asp:Label>
                                </div>
                                <div style="float: left; padding-left: 4px;">
                                    <asp:TextBox ID="txtRelExpMin" runat="server" CssClass="CommonTextBox" rel="Integer"
                                        Width="35px" MaxLength="3" TabIndex="17" ValidationGroup="ValGrpJobTitle"></asp:TextBox>
                                </div>
                            </div>
                            <div style="float: left;">
                                <div style="float: left;">
                                    <asp:Label ID="Label10" runat="server" Text="Max"></asp:Label>
                                </div>
                                <div style="float: left; padding-left: 4px;">
                                    <asp:TextBox ID="txtRelExpMax" runat="server" CssClass="CommonTextBox" rel="Integer"
                                        Width="35" MaxLength="3" TabIndex="18" ValidationGroup="ValGrpJobTitle"></asp:TextBox>
                                    <%--<span class="" style="font-family: Tahoma; font-size: 10pt; color: #FF0000;">*</span>--%>
                                </div>
                                <div class="TableRow" style="padding-right: 40px;">
                                    <%--<asp:RequiredFieldValidator ID="ReqExpMax" runat="server" ControlToValidate="txtRelExpMax"
                                    ValidationGroup="ValGrpJobTitle" Text="Please Enter Relevant Experience"></asp:RequiredFieldValidator>--%>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    
                    
                    <%--Added for LandT--%>
                    <%--Rishi Code Start , Added Div to hide unwanted controls from page--%>
                   <div style="display: none">
                        <div class="TableRow">
                            <div class="TableFormLeble">
                                <asp:Label ID="lblBU" runat="server" Text="Competency"></asp:Label>:
                            </div>
                            <div class="TableFormContent" style="vertical-align: top">
                        <asp:DropDownList ID="ddlBU" runat="server" CssClass="chzn-select" TabIndex="19" Width="40%" AutoPostBack="true" OnSelectedIndexChanged="ddlBU_SelectedIndexChanged">
                        </asp:DropDownList>
                                <span class="RequiredField" id="SpBU">*</span>
                            </div>
                        </div>
                   <%-- </div>--%>
                        <div class="TableRow">
                        <div class="TableFormValidatorContent" style="margin-left: 42%">
                            <asp:RequiredFieldValidator ID="rfvBU" runat="server" ControlToValidate="ddlBU" ErrorMessage="Please Select Competency."
                                InitialValue="0" Display="Dynamic" ValidationGroup="ValGrpJobTitle"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                   
                    <%--Added for LandT--%>
                    
                <%--*****************Added by pravin khot on 9/Aug/2016******************************--%>
                    <div class="TableRow">
                    <div class="TableFormLeble">
                        <asp:Label ID="lblBUContacts" runat="server" Text="Competency Manager"></asp:Label>:
                    </div>
                    <div class="TableFormContent" style="vertical-align: top">
                        <asp:DropDownList ID="ddlBUContacts" runat="server" CssClass="chzn-select" Width="40%" TabIndex="20" >
                        </asp:DropDownList>
                        <span class="RequiredField" id="SpBUContacts">*</span>
                    </div>
                </div>
                     <div class="TableRow">
                    <div class="TableFormValidatorContent" style="margin-left: 42%">
                        <asp:RequiredFieldValidator ID="RFVBUContacts" runat="server" ControlToValidate="ddlBUContacts" ErrorMessage="Please Select Competency Manager."
                            InitialValue="0" Display="Dynamic" ValidationGroup="ValGrpJobTitle"></asp:RequiredFieldValidator>
                    </div>
                </div>
                </div>
              <%--  *************************end*********************--%>
              
                <%--*****************Added by pravin khot on 8/Nov/2016******************************--%>
                
                       <div class="TableRow">
                                <div class="TableFormLeble">
                                    <asp:Label EnableViewState="false" ID="lblLocation" runat="server"  Text="Reporting Location"></asp:Label>:
                                </div>
                                <div class="TableFormContent">
                                    <asp:DropDownList EnableViewState="true" ID="ddllocation" runat="server" CssClass="chzn-select" Width="40%"
                                        TabIndex="21" >
                                    </asp:DropDownList>
                                    <asp:HiddenField ID="HiddenField3" runat="server" />
                                    <span class="RequiredField" id="splocation">*</span>
                                </div>
                            </div>                            
                      <div class="TableRow">
                             <div class="TableFormValidatorContent" style="margin-left: 42%">
                              <asp:RequiredFieldValidator ID="rfvLocation" runat="server" ControlToValidate="ddllocation"
                               SetFocusOnError="true" InitialValue="0" ErrorMessage="Please Select Reporting Location." EnableViewState="False"
                               Display="Dynamic" ValidationGroup="ValGrpJobTitle"></asp:RequiredFieldValidator>
                              </div>
                      </div>
                 <%--  *************************end*********************--%>
              
                
              
                    <%--Rishi Code Start , Added Div to hide unwanted controls from page--%>
                  
                        <div id="divBUContact" class="TableRow" runat="server" visible="true" >
                            <div class="TableFormLeble">
                                <asp:Label ID="lblBUContact" runat="server" Text="BU Contact"></asp:Label>:
                            </div>
                            <%--<div class="TableFormContent" style="vertical-align: top">
                    <asp:DropDownList ID="ddlBUContact" TabIndex="2" runat="server" CssClass="chzn-select">
                    </asp:DropDownList>
                    <asp:HiddenField ID="hdnSelectedContactID" runat="server" />
                    <span class="RequiredField">*</span>
                </div>--%>
                            <div class="TableFormContent">
                                <div id="divBUcontact">
                                </div>
                                <asp:HiddenField ID="hdnSelectedContactValue" runat="server" />
                                <asp:HiddenField ID="hdnSelectedContactText" runat="server" />
                                <asp:HiddenField ID="hfBUcontact" runat="server" Value="" />
                                <asp:TextBox ID="txtBUcontact" runat="server" AutoPostBack="false" CssClass="CommonTextBox"
                                    TabIndex="2" AutoComplete="OFF" onclick="LockWheel();" onBlur="ReleaseWheel(); ContactfocusLost();" />
                                <ajaxToolkit:AutoCompleteExtender ID="BUAutoComplete" runat="server" ServicePath="~/AjaxExtender.asmx"
                                    TargetControlID="txtBUcontact" MinimumPrefixLength="1" ServiceMethod="GetContact"
                                    EnableCaching="False" CompletionInterval="0" CompletionListCssClass="AutoCompleteBox"
                                    FirstRowSelected="True" CompletionListElementID="divBUcontact" OnClientItemSelected="CompanyContact_Selected" />
                                <span class="RequiredField">*</span>
                            </div>
                        </div>
                        <div class="TableRow">
                            <div class="TableFormValidatorContent" style="margin-left: 42%">
                                <asp:RequiredFieldValidator ID="rfvBUcontact" runat="server" ControlToValidate="txtBUcontact"
                                    ErrorMessage="Please Enter BU Contact." Display="Dynamic" ValidationGroup="ValGrpJobTitle"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        
                        
                        
                        <div id="divContactEmaill">
                            <div class="TableFormLeble">
                                <asp:Label ID="lblcontactemail" runat="server" Text="New Contact Email"></asp:Label>:
                            </div>
                            <div class="TableFormContent">
                                <asp:TextBox ID="txtContactEmail" runat="server" EnableViewState="false" OnChange='CheckUserAvailability()'
                                    AutoComplete="OFF" CssClass="CommonTextBox" TabIndex="1" ValidationGroup="ValGrpJobTitle"></asp:TextBox>
                            </div>
                            <div class="TableRow">
                                <div class="TableFormValidatorContent" style="margin-left: 42%">
                                    <div id="divAvailableUser" style="color: Green; display: none" enableviewstate="true">
                                        User name available<br />
                                    </div>
                                    <div id="divNotAvailable" style="color: Red; display: none" enableviewstate="true">
                                        User name not available<br />
                                    </div>
                                    <asp:RegularExpressionValidator ID="revUserName" runat="server" ControlToValidate="txtContactEmail"
                                        Display="Dynamic" ErrorMessage="Please enter valid email address." ValidationExpression="\w+([-+.!#$%&*+-/=?^_`{|}~'\\\s]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                        SetFocusOnError="true" ValidationGroup="ValGrpJobTitle"></asp:RegularExpressionValidator>
                                </div>
                            </div>
                        </div>
                        
                      <div style="display: none">
                        <div class="TableRow">
                            <div class="TableFormLeble">
                                <asp:Label ID="lblNoOfOpenings" runat="server" Text="No. of Openings"></asp:Label>:
                            </div>
                            <div class="TableFormContent">
                                <asp:TextBox ID="txtNoOfOpenings" runat="server" CssClass="CommonTextBox" MaxLength="4"
                                    Width="50px" TabIndex="3" ValidationGroup="ValGrpJobTitle" rel="Integer"></asp:TextBox><span
                                        class="RequiredField">*</span>
                            </div>
                        </div>
                    </div>
                    
                    
                    
                    
                    <div class="TableRow">
                        <div class="TableRow">
                            <div class="TableFormValidatorContent" style="margin-left: 42%">
                                <asp:RequiredFieldValidator ID="rfvnoofopenings" runat="server" ControlToValidate="txtNoOfOpenings"
                                    SetFocusOnError="true" ErrorMessage="Please Enter No Of Openings." Display="Dynamic"
                                    ValidationGroup="ValGrpJobTitle"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="TableFormValidatorContent" style="margin-left: 42%;">
                            <asp:RangeValidator ID="RVOpenings" runat="server" Display="Dynamic" ControlToValidate="txtNoOfOpenings"
                                ErrorMessage="Please enter valid integer Number more than '0'" MaximumValue="9999"
                                MinimumValue="1" Type="Integer" ValidationGroup="ValGrpJobTitle"></asp:RangeValidator>
                        </div>
                    </div>
                    
                    
                    <%--Rishi Code Start , Added Div to hide unwanted controls from page--%>
                    <div style="display: none">
                        <div class="TableRow">
                            <div class="TableFormLeble">
                                <asp:Label ID="lblCity" runat="server" Text="City"></asp:Label>:
                            </div>
                            <div class="TableFormContent">
                                <asp:DropDownList ID="ddlCity" CssClass="chzn-select" runat="server" TabIndex="8">
                                </asp:DropDownList>
                                <span runat="server" id="spnddlCity" class="RequiredField">*</span>
                                <div id="dvcityfill">
                                </div>
                                <asp:HiddenField ID="hdcityfill" runat="server" Value="" />
                                <asp:TextBox EnableViewState="false" ID="txtCity" runat="server" placeholder="City"
                                    AutoPostBack="false" AutoComplete="OFF" TabIndex="8" CssClass="CommonTextBox">
                                </asp:TextBox>
                                <ajaxToolkit:AutoCompleteExtender ID="txtCityfill_AutoCompleteExtender" runat="server"
                                    ServicePath="~/AjaxExtender.asmx" TargetControlID="txtCity" MinimumPrefixLength="1"
                                    ServiceMethod="GetLocationName" EnableCaching="True" CompletionInterval="0" CompletionListCssClass="AutoCompleteBox"
                                    FirstRowSelected="True" CompletionListElementID="dvcityfill" />
                                <span runat="server" id="spnTxtCity" class="RequiredField">*</span> &nbsp;&nbsp;
                            </div>
                        </div>
                    </div>
                    <div>
                        <div id="divZipCode" style="display: none" runat="server">
                            <asp:Label ID="lblZipCode" runat="server" Text="Zip/Postal Code"></asp:Label>:
                            <asp:TextBox ID="txtZipCode" runat="server" CssClass="CommonTextBox" Width="80px"
                                MaxLength="6" TabIndex="9" OnKeyPress="return numeric_only(this)"></asp:TextBox>
                            <%--0.5--%>
                            &nbsp;&nbsp; <a id="divZip" runat="server" href="http://zip4.usps.com/zip4/welcome.jsp"
                                target="_blank" tabindex="24">ZIP code lookup</a> <a id="divPinCode" runat="server"
                                    visible="false" href="http://www.indiapost.gov.in/Pin/Pinsearch.aspx " target="_blank"
                                    tabindex="10">PIN Code Lookup</a>
                            <div class="TableFormValidatorContent" style="margin-left: 64.5%">
                                <asp:RegularExpressionValidator ID="revZIPCode" runat="server" ControlToValidate="txtZipCode"
                                    Display="Dynamic" ErrorMessage="Please enter a valid zip code" ValidationExpression="^[0-9-]*$"
                                    ValidationGroup="ValGrpJobTitle"></asp:RegularExpressionValidator><%--(\d{6}(-\d{4})?)|(\d{5}(-\d{4})?)--%>
                            </div>
                        </div>
                    </div>
                    <div class="TableRow">
                        <div class="TableFormValidatorContent" style="margin-left: 42%">
                            <asp:RequiredFieldValidator ID="rfvCity" runat="server" ControlToValidate="ddlCity"
                                SetFocusOnError="true" ErrorMessage="Please Enter City." InitialValue="0" Display="Dynamic"
                                ValidationGroup="ValGrpJobTitle"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <%-- *************Code change by pravin khot on 11/Feb/2016(ShowAndHideState="true")***************--%>
                    <ucl:CountryState ID="uclCountryState" ShowAndHideState="true" runat="server" FirstOption="Please Select"
                        CountryTabIndex="11" ApplyDefaultSiteSetting="true" IsCountryRequired="true" />
                    <%--  **********************************END*******************************************--%>
                    
                    </asp:Panel>
                    
                </div>
                
                
                
                
                
                
                
                
                  <asp:Panel ID="pnlRequisition4" runat="server"> 
                
                
                <div class="FormRightColumn">
                    <%--Rishi Code Start , Added Div to hide unwanted controls from page--%>
                   
                    <div style="display: none">
                        <div class="TableRow">
                            <div class="TableFormLeble">
                                <asp:Label EnableViewState="false" ID="lblClientJobIdHeader" runat="server" Text="Client Job Id"></asp:Label>:
                            </div>
                            <div class="TableFormContent">
                                <asp:TextBox EnableViewState="false" ID="txtClientJobId" runat="server" CssClass="CommonTextBox"
                                    TabIndex="135"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <!-------------------------------------Rishi Code start ------------------------------------------>
                    <!-------------------------------------Rishi Code start ------------------------------------------>
                    <!-------------------------------------Rishi Code start ------------------------------------------>
                    <%--Rishi Code Start , Added Div to hide unwanted controls from page--%>
                    <div style="display: none">
                        <div class="TableRow">
                            <asp:Panel ID="Panel1" runat="server">
                                <div class="TableFormLeble">
                                    <asp:Label ID="Label1" runat="server" Text="Candidate Name"></asp:Label>:
                                </div>
                                <div class="TableFormContent">
                                    <asp:TextBox ID="txtCandidateName" runat="server" CssClass="CommonTextBox" MaxLength="60"
                                        TabIndex="15" EnableViewState="False" ValidationGroup="ValGrpJobTitle" onkeypress="checkEnter(event)"></asp:TextBox>
                                    <span class="RequiredField" id="Span1">*</span>
                                </div>
                            </asp:Panel>
                        </div>
                    </div>
                    <div class="TableRow">
                        <asp:Panel ID="Panel2" runat="server">
                            <div class="TableFormLeble">
                                <asp:Label ID="Label4" runat="server" Text="Project Name"></asp:Label>:
                            </div>
                            <div class="TableFormContent">
                                <asp:TextBox ID="txtProjectName" runat="server" CssClass="CommonTextBox" MaxLength="60"
                                    TabIndex="22" EnableViewState="False" ValidationGroup="ValGrpJobTitle" onkeypress="checkEnter(event)"></asp:TextBox>
                                <%--<span class="" style="font-family: Tahoma; font-size: 10pt; color: #FF0000;">*</span>--%>
                            </div>
                            <div class="TableRow">
                                <%--<div class="TableFormValidatorContent" style="margin-left: 42%">
                                <asp:RequiredFieldValidator ID="reqProjectName" runat="server" ControlToValidate="txtProjectName"
                                    ValidationGroup="ValGrpJobTitle" Text="Please Enter Project Name"></asp:RequiredFieldValidator>
                            </div>--%>
                            </div>
                        </asp:Panel>
                    </div>
                    <div class="TableRow">
                        <asp:Panel ID="Panel3" runat="server">
                            <div class="TableFormLeble">
                                <asp:Label ID="Label5" runat="server" Text="Customer info"></asp:Label>:
                            </div>
                            <div class="TableFormContent">
                                <asp:TextBox ID="txtCustInfo" runat="server" CssClass="CommonTextBox" TextMode="multiline"
                                    TabIndex="23" Columns="80" Rows="5"></asp:TextBox>
                                <%--  <span class="" style="font-family: Tahoma; vertical-align: top; font-size: 10pt;
                                color: #FF0000;">*</span>--%>
                            </div>
                            <%-- <div class="TableRow">
                            <div class="TableFormValidatorContent" style="margin-left: 42%;">
                                <asp:RequiredFieldValidator ID="requiredCustomerInfo" runat="server" ControlToValidate="txtCustInfo"
                                    ValidationGroup="ValGrpJobTitle" Text="Please Enter Customer Info"></asp:RequiredFieldValidator>
                            </div>
                        </div>--%></asp:Panel>
                    </div>
                    
                          <div class="TableRow" style="text-align: left" id="div1" runat="server">
                        <asp:UpdatePanel ID="SuperOrgCodeUpdatePanel" runat="server">
                            <ContentTemplate>
                                <div class="TableRow">
                                    <div class="TableFormLeble">
                                        <asp:Label ID="Label2" runat="server" Text="Supervisory Organization Code"></asp:Label>:
                                    </div>
                                    <div class="TableFormContent" style="vertical-align: top">
                                        <asp:DropDownList ID="ddlSuperOrgCode" runat="server" CssClass="chzn-select" TabIndex="24"
                                            Width="40%" AutoPostBack="True" OnSelectedIndexChanged="SuperOrgCodeDropDown_Change">
                                        </asp:DropDownList>
                                        <span class="" style="font-family: Tahoma; font-size: 10pt; color: #FF0000;">*</span>
                                    </div>
                                    <div class="TableRow">
                                        <div class="TableFormValidatorContent" style="margin-left: 42%">
                                            <asp:RequiredFieldValidator ID="rfvSuperOrgCode" runat="server" ControlToValidate="ddlSuperOrgCode"
                                                ErrorMessage="Please select Supervisory Organization Code." ValidationGroup="ValGrpJobTitle"
                                                Display="Dynamic" InitialValue="0"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                </div>
                                <div class="TableRow">
                                    <div class="TableFormLeble">
                                        <asp:Label ID="lblSOShortText" runat="server"   Text="Supervisory Organization Short Text"></asp:Label>:
                                    </div>
                                    <div class="TableFormContent" style="vertical-align: top">
                                        <asp:Label ID="lblSuperOrgShortTxt" runat="server" Text=""></asp:Label>
                                    </div>
                                </div>
                                <div class="TableRow">
                                    <div class="TableFormLeble">
                                        <asp:Label ID="label53" runat="server" Text="Supervisory Organization Description"></asp:Label>:
                                    </div>
                                    <div class="TableFormContent" style="vertical-align: top">
                                        <asp:Label ID="lblSuperOrgDescrp" runat="server" Text=""></asp:Label>
                                    </div>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="ddlSuperOrgCode" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                    
                    <div class="TableRow">
                        <div class="TableFormLeble">
                            <asp:Label ID="Label15" runat="server" Text="Cost Center"></asp:Label>:
                        </div>
                        <div class="TableFormContent" style="vertical-align: top">
                            <asp:DropDownList ID="ddlCostCenter" runat="server" CssClass="chzn-select" TabIndex="25"
                                Width="40%">
                            </asp:DropDownList>
                            <span class="" style="font-family: Tahoma; font-size: 10pt; color: #FF0000;">*</span>
                        </div>
                        <div class="TableRow">
                            <div class="TableFormValidatorContent" style="margin-left: 42%">
                                <asp:RequiredFieldValidator ID="rfvCostCenter" runat="server" ControlToValidate="ddlCostCenter"
                                    ErrorMessage="Please select Cost Center." Display="Dynamic" ValidationGroup="ValGrpJobTitle" InitialValue="0"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                    </div>
                    
                    <div class="TableRow">
                        <div class="TableFormLeble">
                            <asp:Label ID="Label16" runat="server" Text="Region"></asp:Label>:
                        </div>
                        <div class="TableFormContent" style="vertical-align: top">
                            <asp:DropDownList ID="ddlRegion" runat="server" CssClass="chzn-select" TabIndex="26"
                                Width="40%">
                            </asp:DropDownList>
                            <span class="RequiredField">*</span>
                        </div>
                    </div>
                    
                     <div class="TableRow">
                                    <div class="TableFormLeble">
                                        <asp:Label ID="Label18" runat="server" Text="Division"></asp:Label>:
                                    </div>
                                    <div class="TableFormContent" style="vertical-align: top">
                                        <asp:DropDownList ID="ddlDivision" runat="server" CssClass="chzn-select" TabIndex="29"
                                            Width="40%" AutoPostBack="True" OnSelectedIndexChanged="Division_Change">
                                        </asp:DropDownList>
                                        <span class="" style="font-family: Tahoma; vertical-align: top; font-size: 10pt;color: #FF0000;">*</span>                                            
                                    </div>
                                    <div class="TableRow">
                                        <div class="TableFormValidatorContent" style="margin-left: 42%;">
                                            <asp:RequiredFieldValidator ID="reqDivision" runat="server" ControlToValidate="ddlDivision"
                                                ValidationGroup="ValGrpJobTitle" Display="Dynamic" Text="Please Select Division" InitialValue="0"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                </div>
                    
                    <div class="TableRow">
                        <div class="TableFormLeble">
                            <asp:Label ID="Label7" runat="server" Text="Individual Contributor/Management"></asp:Label>:
                        </div>
                        <div class="TableFormContent" style="vertical-align: top">
                            <asp:DropDownList ID="ddlIndivContribMgnt" runat="server" CssClass="chzn-select"
                                TabIndex="27" Width="40%"  AutoPostBack="True" OnSelectedIndexChanged="IndivContribMgnt_Change">
                            </asp:DropDownList>
                            <%--   <span class="" style="font-family: Tahoma; font-size: 10pt; color: #FF0000;">*</span>--%>
                        </div>
                        <%--  <div class="TableFormValidatorContent" style="margin-left: 42%;">
                        <asp:RequiredFieldValidator ID="reqControlManagement" runat="server" ControlToValidate="ddlIndivContribMgnt"
                            ValidationGroup="ValGrpJobTitle" Text="Please Select Contributor/Managerment"
                            InitialValue="0"></asp:RequiredFieldValidator>
                    </div>--%>
                    </div>
                    <div class="TableRow" style="text-align: left" id="div2" runat="server">
                        <asp:UpdatePanel ID="JobFmailyUpdatePanel" runat="server">
                            <ContentTemplate>
                                <div class="TableRow">
                                    <div class="TableFormLeble">
                                        <asp:Label ID="Label17" runat="server" Text="Job Family"></asp:Label>:
                                    </div>
                                    <div class="TableFormContent" style="vertical-align: top">
                                        <asp:DropDownList ID="ddlJobFamily" runat="server" CssClass="chzn-select" TabIndex="28"
                                            Width="40%" AutoPostBack="True" OnSelectedIndexChanged="JobFamilyDropDown_Change">
                                        </asp:DropDownList>
                                        <%-- <span class="" style="font-family: Tahoma; font-size: 10pt; color: #FF0000;">*</span>--%>
                                    </div>
                                    <%-- <div class="TableFormValidatorContent" style="margin-left: 42%;">
                                    <asp:RequiredFieldValidator ID="reqJobFamily" runat="server" ControlToValidate="ddlJobFamily"
                                        ValidationGroup="ValGrpJobTitle" Text="Please Select Job Family" InitialValue="0"></asp:RequiredFieldValidator>
                                </div>--%>
                                </div>
                                <div class="TableRow">
                                    <div class="TableFormLeble">
                                        <asp:Label ID="label19" runat="server" Text="Job Profile Code"></asp:Label>:
                                    </div>
                                    <div class="TableFormContent" style="vertical-align: top">
                                        <asp:Label ID="lblJobProfileCode" runat="server" Text=""></asp:Label>
                                    </div>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="ddlJobFamily" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                    
                    
                    
                    <div class="TableRow">
                        <asp:UpdatePanel ID="upddd" runat="server">
                            <ContentTemplate>
                               
                                <div class="TableRow" style="text-align: left" id="div3" runat="server">
                                    <div class="TableRow">
                                        <div class="TableFormLeble">
                                            <asp:Label ID="Label20" runat="server" Text="Candidate Manager GID"></asp:Label>:
                                        </div>
                                        <div class="TableFormContent" style="vertical-align: top">
                                            <%--<asp:DropDownList ID="ddlReportingMgrGID" runat="server" CssClass="chzn-select" TabIndex="17"
                                            Width="40%" AutoPostBack="True" OnSelectedIndexChanged="ReportingMgrGID_Change">
                                        </asp:DropDownList>
                                        --%>
                                            <asp:DropDownList ID="ddlReportingMgrGID" runat="server" CssClass="chzn-select" TabIndex="30"
                                                Width="40%" AutoPostBack="True" OnSelectedIndexChanged="ReportingMgrGID_Change">
                                            </asp:DropDownList>
                                            <span class="" style="font-family: Tahoma; font-size: 10pt; color: #FF0000;">*</span>
                                        </div>
                                        <div class="TableRow">
                                            <div class="TableFormValidatorContent" style="margin-left: 42%;">
                                                <asp:RequiredFieldValidator ID="reqReportingManager" runat="server" ControlToValidate="ddlReportingMgrGID"
                                                    ValidationGroup="ValGrpJobTitle" Display="Dynamic" Text="Please Select Reporting Manager" InitialValue="0"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="TableRow">
                                    <div class="TableFormLeble">
                                        <asp:Label ID="label21" runat="server" Text="Candidate Manager Name"></asp:Label>:
                                    </div>
                                    <div class="TableFormContent" style="vertical-align: top">
                                        <asp:Label ID="lblMgrName" runat="server" Text=""></asp:Label>
                                    </div>
                                </div>
                                <div class="TableRow">
                                    <div class="TableFormLeble">
                                        <asp:Label ID="label54" runat="server" Text="Org unit Head"></asp:Label>:
                                    </div>
                                    <div class="TableFormContent" style="vertical-align: top">
                                        <asp:Label ID="lblOrgUnitHead" runat="server" Text=""></asp:Label>
                                    </div>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="ddlDivision" />
                                <asp:PostBackTrigger ControlID="ddlReportingMgrGID" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                    <div class="TableRow">
                        <div class="TableFormLeble">
                            <asp:Label ID="Label6" runat="server" Visible="false" Text="GDC/GSC"></asp:Label>
                            <asp:Label ID="lblCreatedOn" runat="server" Text="Created On:"></asp:Label>
                        </div>
                       
                        <div class="TableFormContent" style="vertical-align: top">
                            <asp:DropDownList ID="ddlGdcGsc" runat="server" CssClass="chzn-select" TabIndex="137"
                                Visible="false" Width="40%">
                            </asp:DropDownList>
                            <asp:Label ID="lblCreatedOnTextValue" runat="server"></asp:Label>
                            <%--    <span class="" style="font-family: Tahoma; font-size: 10pt; color: #FF0000;">*</span>--%>
                        </div>
                        <div class="TableFormValidatorContent" style="margin-left: 42%;">
                            <%--  <asp:RequiredFieldValidator ID="reqGFC" runat="server" ControlToValidate="ddlGdcGsc"
                            ValidationGroup="ValGrpJobTitle" Text="Please Select GDC/GSC" InitialValue="0"></asp:RequiredFieldValidator>--%>
                        </div>
                    </div>
                  
                  
                   <div class="TableRow">
                        <div class="TableFormLeble">                           
                            <asp:Label ID="lblSubmittedOn" runat="server" Text="Submitted On:"></asp:Label>
                        </div>
                         <div class="TableFormContent" style="vertical-align: top">                         
                            <asp:Label ID="lblSubmittedOnTextValue" runat="server"></asp:Label>                          
                        </div>
                     </div>
                
                
                
                
                
                    <div class="TableRow">
                        <div class="TableFormLeble">
                            <%--Added For LandT--%>
                            <asp:Label ID="lblStartDate" runat="server" Text="Req. Open Date"></asp:Label>:
                        </div>
                        <div class="TableFormContent" style="white-space: nowrap">
                            <div>
                                <div style="float: left" id="divDate" runat="server">
                                    <%--<igsch:WebDateChooser ID="wdcStartDate" runat="server" NullDateLabel="" EnableKeyboardNavigation="True"
                                    AutoCloseUp="true" FocusOnInitialization="True" TabIndex="4" Editable="True">
                                    <CalendarLayout HideOtherMonthDays="True">
                                    </CalendarLayout>
                                </igsch:WebDateChooser>--%>
                                    <ig:WebDatePicker ID="wdcStartDate" DropDownCalendarID="ddcStartDate" runat="server" TabIndex="9">
                                    </ig:WebDatePicker>
                                    <ig:WebMonthCalendar ID="ddcStartDate" HideOtherMonthDays="true" AllowNull="true"
                                        runat="server">
                                    </ig:WebMonthCalendar>
                                </div>
                                <span class="RequiredField">*</span>
                                <%--for LandT--%>
                                <div id="divASAP" runat="server" style="display: none">
                                    <div align="left" style="float: left">
                                        <asp:CheckBox ID="chkStartDate" runat="server" Text="ASAP" AutoPostBack="true" OnCheckedChanged="chkStartDate_CheckedChanged"
                                            TabIndex="5" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="TableRow">
                        <div class="TableFormValidatorContent" style="margin-left: 42%">
                            <asp:CompareValidator ID="cmpStartDate" runat="server" Operator="GreaterThanEqual"
                                Type="Date" ControlToValidate="wdcStartDate" ErrorMessage="Start date should not be before the current date"
                                Display="Dynamic" />
                            <asp:RequiredFieldValidator ID="rfvStartDate" runat="server" ControlToValidate="wdcStartDate"
                                SetFocusOnError="true" ErrorMessage="Please select date of requisition." Display="Dynamic"
                                ValidationGroup="ValGrpJobTitle"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <%--Added for LandT--%>
                    <div class="TableRow">
                        <div class="TableFormValidatorContent" style="margin-left: 42%">
                            <asp:CompareValidator ID="cmpOpenDate" runat="server" Operator="GreaterThanEqual"
                                Type="Date" ControlToValidate="wdcOpenDate" ErrorMessage="Open date should not be before the current date"
                                Display="Dynamic" ValidationGroup="ValGrpJobTitle" />
                            <asp:CompareValidator ID="cmbopenwithExpectedFullfillment" runat="server" Operator="LessThanEqual"
                                Type="Date" ControlToValidate="wdcOpenDate" ErrorMessage="Open date should not be after the expected fullfillment date"
                                Display="Dynamic" ControlToCompare="wdcClosingDate" ValidationGroup="ValGrpJobTitle" />
                            <asp:RequiredFieldValidator ID="rfVOpendDate" runat="server" ControlToValidate="wdcOpenDate"
                                SetFocusOnError="true" ErrorMessage="Please select open date." Display="Dynamic"
                                ValidationGroup="ValGrpJobTitle"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="TableRow">
                        <div class="TableFormLeble">
                            <%--for LandT--%>
                            <asp:Label ID="lblClosingDate" runat="server" Text="Req. Expected Fulfillment Date"></asp:Label>:
                        </div>
                        <div class="TableFormContent" style="white-space: nowrap">
                            <div>
                                <div style="float: left" id="divExpFulfillmentDate" runat="server">
                                    <%-- <igsch:WebDateChooser ID="wdcClosingDate" runat="server" NullDateLabel="" EnableKeyboardNavigation="True"
                                    FocusOnInitialization="True" TabIndex="7" Editable="true">
                                    <CalendarLayout HideOtherMonthDays="True">
                                    </CalendarLayout>
                                </igsch:WebDateChooser>--%>
                                    <ig:WebDatePicker ID="wdcClosingDate" DropDownCalendarID="ddcClosingDate" runat="server" TabIndex="10">
                                    </ig:WebDatePicker>
                                    <ig:WebMonthCalendar ID="ddcClosingDate" AllowNull="true" HideOtherMonthDays="true"
                                        runat="server">
                                    </ig:WebMonthCalendar>
                                </div>
                                <span class="RequiredField">*</span>
                            </div>
                        </div>
                    </div>
                    <div class="TableRow">
                        <div class="TableFormValidatorContent" style="margin-left: 42%">
                            <asp:RequiredFieldValidator ID="rfvClosingDate" runat="server" ControlToValidate="wdcClosingDate"
                                SetFocusOnError="true" ErrorMessage="Please Select Expected Fulfillment Date."
                                Display="Dynamic" ValidationGroup="ValGrpJobTitle"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="TableRow">
                        <div class="TableFormValidatorContent" style="margin-left: 42%">
                            <asp:CompareValidator ID="cmpDate" ControlToCompare="wdcStartDate" ControlToValidate="wdcClosingDate"
                                Type="Date" Operator="GreaterThanEqual" ErrorMessage="Expected Fulfillment Date Should not before Date of Requisition."
                                ValidationGroup="ValGrpJobTitle" Display="Dynamic" runat="server"></asp:CompareValidator>
                        </div>
                    </div>
                    </div>
                <!---------------------------------------Rishi Code End ------------------------------------------------->
                <!---------------------------------------Rishi Code End ------------------------------------------------->
                <!---------------------------------------Rishi Code End ------------------------------------------------->
                <%--Added for LandT--%>
                <%--Rishi Code Start , Added Div to hide unwanted controls from page--%>
                <div style="display: none">
                    <div class="TableRow">
                        <div class="TableFormLeble">
                            <asp:Label ID="lblJobCategory" runat="server" Text="Job Category"></asp:Label>:
                        </div>
                        <div class="TableFormContent" style="vertical-align: top">
                            <asp:DropDownList ID="ddlJbCategory" runat="server" CssClass="chzn-select" TabIndex="137"
                                Width="40%">
                            </asp:DropDownList>
                            <span class="RequiredField">*</span>
                        </div>
                    </div>
                    <div class="TableRow">
                        <div class="TableFormValidatorContent" style="margin-left: 42%">
                            <asp:RequiredFieldValidator ID="rfvJobCategory" runat="server" ControlToValidate="ddlJbCategory"
                                SetFocusOnError="true" InitialValue="0" ErrorMessage="Please Select Job Category."
                                Display="Dynamic" ValidationGroup="ValGrpJobTitle"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                </div>
                <%--Added for LandT--%>
                <div class="TableRow" runat="server" id="trjoblocation">
                    <div class="TableFormLeble">
                        <asp:Label ID="lblJobLocation" runat="server" Text="Job Location"></asp:Label>:
                    </div>
                    <div class="TableFormContent" style="vertical-align: top">
                        <asp:DropDownList ID="ddlJobLocation" runat="server" CssClass="chzn-select" TabIndex="20">
                        </asp:DropDownList>
                        <span class="RequiredField">*</span>
                    </div>
                    <div class="TableFormValidatorContent" style="margin-left: 42%">
                        <asp:RequiredFieldValidator ID="rfvJobLocation" runat="server" ControlToValidate="ddlJobLocation"
                            SetFocusOnError="true" ErrorMessage="Please Select Job Location." InitialValue="0"
                            Display="Dynamic" ValidationGroup="ValGrpJobTitle"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div id="dvBillrate" runat="server">
                    <div class="TableRow">
                        <div class="TableFormLeble">
                            <asp:Label ID="lblSalary" runat="server" Text="Bill Rate"></asp:Label>:
                        </div>
                        <div class="TableFormContent" style="vertical-align: top">
                            <div id="divOpen" style="display: none">
                                <asp:CheckBox ID="chkSalary" runat="server" TabIndex="19" Checked="false" /></div>
                            <asp:TextBox ID="txtSalary" runat="server" CssClass="CommonTextBox" Width="80px"
                                rel="Numeric" TabIndex="20"></asp:TextBox>
                            <div id="divMaxBillRate" runat="server" style="display: none">
                                <asp:TextBox ID="txtMaxPayRate" runat="server" CssClass="CommonTextBox" Width="80px"
                                    TabIndex="21" Enabled="false" ToolTip="Max" rel="Numeric"></asp:TextBox></div>
                            <div id="divSalary" runat="server" style="display: none">
                                <asp:DropDownList ID="ddlSalary" runat="server" CssClass="CommonDropDownList" TabIndex="21"
                                    Enabled="false" Width="70px">
                                    <asp:ListItem Value="4">Yearly</asp:ListItem>
                                    <asp:ListItem Value="3">Monthly</asp:ListItem>
                                    <asp:ListItem Value="2">Daily</asp:ListItem>
                                    <asp:ListItem Value="1" Selected="True">Hourly</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <asp:DropDownList ID="ddlSalaryCurrency" runat="server" CssClass="CommonDropDownList"
                                TabIndex="22" Visible="false" Width="70px">
                            </asp:DropDownList>
                            <asp:Label runat="server" ID="lblSalaryCurrency" Text="$USD"></asp:Label>
                            <span class="RequiredField">*</span>
                            <asp:Label ID="lblPayrateCurrency" runat="server" Width="70px"></asp:Label>
                        </div>
                    </div>
                </div>
                <div id="divClientBudget" visible="false" runat="server">
                    <div class="TableRow">
                        <div class="TableFormLeble">
                            <asp:Label ID="lblClientBudget" runat="server" Text="Client Budget"></asp:Label>:
                        </div>
                        <div class="TableFormContent" style="vertical-align: top">
                            <div style="float: left;">
                                <div style="float: left;">
                                    <asp:Label ID="lblClientBudgetMin" runat="server" Text="Min"></asp:Label>
                                </div>
                                <div style="float: left; padding-left: 4px;">
                                    <asp:TextBox ID="txtClientBudgetMin" runat="server" CssClass="CommonTextBox" rel="Integer"
                                        Width="35px" TabIndex="26" ValidationGroup="ValGrpJobTitle"></asp:TextBox>
                                </div>
                            </div>
                            <div style="float: left;">
                                <div style="float: left;">
                                    <asp:Label ID="lblClientBudgetMax" runat="server" Text="Max"></asp:Label>
                                </div>
                                <div style="float: left; padding-left: 4px;">
                                    <asp:TextBox ID="txtClientBudgetMax" runat="server" CssClass="CommonTextBox" rel="Integer"
                                        Width="35" TabIndex="27" ValidationGroup="ValGrpJobTitle"></asp:TextBox><span class="RequiredField">*</span>
                                </div>
                            </div>
                            <%--</div>
                    <div class="TableFormContent" style="vertical-align: top">--%>
                            <div style="float: right; text-align: right; padding-right: 15px;">
                                <div style="float: right; padding-right: 4px;">
                                    <asp:DropDownList ID="ddlpaycycle" runat="server" CssClass="CommonDropDownList" runat="server"
                                        Width="55px">
                                    </asp:DropDownList>
                                </div>
                                <div style="float: right; padding-right: 4px;">
                                    <asp:DropDownList ID="ddlCurrency" CssClass="CommonDropDownList" runat="server" Width="55px">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="TableRow">
                    <div class="TableFormValidatorContent" style="margin-left: 42%">
                        <asp:RequiredFieldValidator ID="rfvBillRate" runat="server" ControlToValidate="txtSalary"
                            SetFocusOnError="true" ErrorMessage="Please Enter Bill Rate." Display="Dynamic"
                            ValidationGroup="ValGrpJobTitle"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <%--Added for LandT--%>
                <div class="TableRow" runat="server" id="trsalesgroup">
                    <div class="TableFormLeble">
                        <asp:Label ID="lblSalesGroup" runat="server" Text="Sales Group"></asp:Label>:
                    </div>
                    <div class="TableFormContent" style="vertical-align: top">
                        <asp:DropDownList ID="ddlSalesGroup" runat="server" CssClass="chzn-select" TabIndex="24">
                        </asp:DropDownList>
                        <span class="RequiredField">*</span>
                    </div>
                    <div class="TableFormValidatorContent" style="margin-left: 42%">
                        <asp:RequiredFieldValidator ID="rfvSalesGroup" runat="server" ControlToValidate="ddlSalesGroup"
                            SetFocusOnError="true" InitialValue="0" ErrorMessage="Please Select Sales Group."
                            Display="Dynamic" ValidationGroup="ValGrpJobTitle"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <%--Added for LandT--%>
                <div class="TableRow" runat="server" id="trsalesregion">
                    <div class="TableFormLeble">
                        <asp:Label ID="lblSalesRegion" runat="server" Text="Sales Region"></asp:Label>:
                    </div>
                    <div class="TableFormContent" style="vertical-align: top">
                        <asp:DropDownList ID="ddlSalesRegion" runat="server" TabIndex="23" CssClass="chzn-select"
                            Width="200px">
                        </asp:DropDownList>
                        <span class="RequiredField">*</span>
                    </div>
                    <div class="TableFormValidatorContent" style="margin-left: 42%">
                        <asp:RequiredFieldValidator ID="rfvSalesRegion" runat="server" ControlToValidate="ddlSalesRegion"
                            SetFocusOnError="true" InitialValue="0" ErrorMessage="Please Select Sales Region."
                            Display="Dynamic" ValidationGroup="ValGrpJobTitle"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <%--Rishi Code Start , Added Div to hide unwanted controls from page--%>
               <div style="display: none">
                    <div class="TableRow">
                        <div class="TableFormLeble">
                            <asp:Label ID="lblEducationQualification" runat="server" Text="Educational Qualification"></asp:Label>:
                        </div>
                        <div class="TableFormContent">
                            <ucl:MultipleSelection ID="uclEducationList" runat="server" />
                            <div style="border: solid 1px #7f7f7f; border-collapse: collapse; color: #333333;
                                width: 20%; height: 100px; overflow: auto; float: left; display: none">
                                <asp:CheckBoxList ID="ddlQualification" runat="server" ValidationGroup="ValGrpJobTitle"
                                    AutoPostBack="false" TabIndex="25">
                                </asp:CheckBoxList>
                            </div>
                            <div style="display: inline-block">
                                <span class="RequiredField">*</span>
                            </div>
                        </div>
                    </div>                    .
              </div>
                <div class="TableRow">
                    <div class="TableFormValidatorContent" style="margin-left: 42%">
                        <%--                        <asp:CustomValidator ID="cvEduQualification" runat="server"  OnServerValidate ="ValidateEducationList"
                    Display="Dynamic" EnableClientScript="true" ErrorMessage="Select at least one Educational Qualification"
                    ValidationGroup="ValGrpJobTitle"></asp:CustomValidator>--%>
                    </div>
                </div>
                <%--<div class="TableRow">
                    <div class="TableFormLeble">
                        <asp:Label EnableViewState="false" ID="lblBranch" runat="server" Text="Branch"></asp:Label>:
                    </div>
                    <div class="TableFormContent">
                        <asp:DropDownList ID="ddlbranch" runat="server" CssClass="chzn-select" TabIndex="14"
                            Width="40%">
                        </asp:DropDownList>
                    </div>
                </div>
           
                <div class="TableRow">
                    <div class="TableFormLeble">
                        <asp:Label EnableViewState="false" ID="lblGrade" runat="server" Text="Grade"></asp:Label>:
                    </div>
                    <div class="TableFormContent">
                        <asp:DropDownList ID="ddlgrade" runat="server" CssClass="chzn-select" TabIndex="15"
                            Width="40%">
                        </asp:DropDownList>
                    </div>
                </div>--%>
                 </asp:Panel>
                
            </div>
        </div>
    </asp:Panel>
</div>
<script src="../assets/js/chosen.jquery.js" type="text/javascript"></script>

<script type="text/javascript">
    Sys.Application.add_load(function() {
        $(".chzn-select").chosen();
        $(".chzn-select-deselect").chosen({ allow_single_deselect: true });
        $('#<%=ddlBU.ClientID %>').chosen().change(function() {
            $find('<%=BUAutoComplete.ClientID%>').set_contextKey($(this).find('option:selected').val());
        });
        $('#divContactEmaill').css({ 'display': 'none' });
    }); 
</script>

