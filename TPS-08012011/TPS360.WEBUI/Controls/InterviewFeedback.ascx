﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="InterviewFeedback.ascx.cs" Inherits="TPS360.Web.UI.ControlsInterviewFeedback" %>

<%@ Register Src="~/Controls/PagerControl.ascx" TagName="Pager" TagPrefix="ucl" %>
 <asp:UpdatePanel ID="upNotes" runat ="server" >
 <ContentTemplate >
 
 <asp:HiddenField ID="hdnInterviewFeebackId" runat ="server" />
 <div class="TableRow" style="text-align:left">
        <asp:Label EnableViewState="false" ID="lblMessage" runat="server"></asp:Label>
    </div>
    <div class="TableRow">

        <div class="TableFormLeble">
            <asp:Label ID="lblTitle" runat="server" Text="Title"></asp:Label>:
        </div>
        <div class="TableFormContent">
        <asp:TextBox ID="txtTitle" runat="server" CssClass="CommonTextBox"  ValidationGroup ="NoteValidation" ></asp:TextBox>
            <span class="RequiredField">*</span>
        </div>
    </div>
   
    <div class ="TableRow">
        <div  class ="TableFormValidatorContent" style="text-align:center">
            <asp:RequiredFieldValidator ID ="rfvTitle" runat ="server" Display ="Dynamic"  ErrorMessage ="Please Enter Title." ControlToValidate ="txtTitle" ValidationGroup ="NoteValidation"></asp:RequiredFieldValidator> 
        </div>
    </div>
    
    <div class="TableRow">
        <div class="TableFormLeble">
            <asp:Label ID="lblRound" runat="server" Text="Round"></asp:Label>:
        </div>
        <div class="TableFormContent">
        <asp:DropDownList ID="ddlInterviewRounds" runat="server" CssClass="CommonDropDownList" ValidationGroup ="NoteValidation" ></asp:DropDownList>
            <%--<span class="RequiredField">*</span>--%>
        </div>
    </div>
    
    <div class="TableRow">
        <div class="TableFormLeble">
            <asp:Label ID="lblFeedback" runat="server" Text="Feedback"></asp:Label>:
        </div>
        <div class="TableFormContent">
        <asp:TextBox ID="txtNotes" runat="server" CssClass="CommonTextBox" TextMode="MultiLine" Rows="6" ValidationGroup ="NoteValidation" ></asp:TextBox>
            <span class="RequiredField">*</span>
        </div>
    </div>
    
    <div class ="TableRow">
        <div  class ="TableFormValidatorContent" style="text-align:center">
            <asp:RequiredFieldValidator ID ="rfvNotes" runat ="server" Display ="Dynamic"  ErrorMessage ="Please Enter Feedback." ControlToValidate ="txtNotes" ValidationGroup ="NoteValidation"></asp:RequiredFieldValidator> 
        </div>
    </div>
     <div class ="TableRow">
            <div class="TableFormLeble" style="width:20%">
            </div>
            <div class="TableFormContent" style="margin-left:45%">
                <asp:Button ID="btnSave" CssClass="CommonButton" runat="server" Text="Save"  OnClick="btnSave_Click"  ValidationGroup ="NoteValidation" />
                </div>
    </div>
   <div class="TableRow" style ="max-height : 400px;" >
             <div class="TabPanelHeader">
                    List of Feedbacks
                </div>
            <asp:ListView ID="lsvFeedback" runat="server" OnItemDataBound="lsvFeedback_ItemDataBound" OnItemCommand="lsvFeedback_ItemCommand" OnPreRender="lsv_Feedback_OnRender">
                <LayoutTemplate>
                    <table id="tblFeedback" runat="server" class="Grid" cellspacing="0" border="0">
                        <tr>
                            <th style="width: 20%; white-space: nowrap;">
                                Created Date
                            </th>
                            <th style="width: 20%; white-space: nowrap;">
                                Title
                            </th>
                            <th style="width: 20%; white-space: nowrap;">
                                Round
                            </th>
                            <th style="width: 20%; white-space: nowrap;">
                                Feedback
                            </th>
                            <th style="width: 20%; white-space: nowrap;">
                                Created By
                            </th>
                            <th style="width: 20%; white-space: nowrap;">
                                Action
                            </th>
                        </tr>      
                        <tr id="itemPlaceholder" runat="server">
                        </tr>
                    </table>
                </LayoutTemplate>
                <EmptyDataTemplate>
                    <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                        <tr>
                            <td>
                                No Feedback Available.
                            </td>
                        </tr>
                    </table>
                </EmptyDataTemplate>
                <ItemTemplate>
                    <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                        <td>
                            <asp:Label ID="lblCreatedDate" runat="server" />
                        </td>
                        <td>
                            <asp:Label ID="lblTitle" runat="server" />
                        </td>
                        <td>
                            <asp:Label ID="lblRound" runat="server" />
                        </td>
                        <td>
                            <asp:Label ID="lblFeedback" runat="server" />
                        </td>
                         <td>
                            <asp:Label ID="lblCreatedBy" runat="server" />
                        </td>
                        <td style="text-align: center;" runat ="server" id="tdAction">
                            <asp:ImageButton ID="btnEdit" SkinID="sknEditButton" runat="server" CommandName="EditItem">
                            </asp:ImageButton>
                            <asp:ImageButton ID="btnDelete" SkinID="sknDeleteButton" runat="server" CommandName="DeleteItem">
                            </asp:ImageButton>

                        </td>
                    </tr>
                </ItemTemplate>
            </asp:ListView>
   </div>  


</ContentTemplate>
 </asp:UpdatePanel>