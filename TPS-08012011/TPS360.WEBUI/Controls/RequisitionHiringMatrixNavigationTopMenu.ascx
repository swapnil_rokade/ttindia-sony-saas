﻿<%-- 
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName:RequisitionHiringMatrixNavigationTopMenu.ascx
    Description: This is the page which is used to present tab controls for requisation  
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    0.1             Jan-6-2008          Gopala Swamy          Defect id: 9664; changed the "lblTitle"s font-size and "uwtRequisitionHiringMatrixNavigationTopMenu"s tab menus font-size
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

--%>


<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RequisitionHiringMatrixNavigationTopMenu.ascx.cs"
    Inherits="TPS360.Web.UI.cltRequisitionHiringMatrixNavigationTopMenu" %>
<%@ Register src="../Controls/RequisitionHiringMatrix.ascx" tagname="RequisitionHiringMatrix" tagprefix="uc1" %>
<%@ Register Src = "~/Controls/RequisitionStatusReport.ascx" TagName ="RequisitionStatusReport" TagPrefix ="uc1" %>
<%@ Register Src ="~/Controls/RejectCandidateList.ascx" TagName ="Rejection" TagPrefix ="uc1" %>
<%@ Register Src ="~/Controls/RejectDetails.ascx" TagName ="RejectCandidate" TagPrefix ="ucl" %>
<%@ Register Src ="~/Controls/HiringDetails.ascx" TagName ="HiringDetails" TagPrefix ="ucl" %>
<%@ Register Src ="~/Controls/SubmissionDetails.ascx" TagName ="SubmissionDetails" TagPrefix ="ucl" %>
<%@ Register Src ="~/Controls/JoiningDetails.ascx" TagName ="JoiningDetails" TagPrefix ="ucl" %>
<%@ Register Src ="~/Controls/CandidateActionLog.ascx" TagName ="ActionLog" TagPrefix ="ucl" %>
<%@ Register Src ="~/Controls/ModalTemplate.ascx" TagName ="Template" TagPrefix ="ucl" %>


   <%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>  
    
     <ajaxToolkit:ModalPopupExtender ID ="mpeRejectCandidate" BehaviorID ="MPEReject" runat ="server" BackgroundCssClass ="DarkModalBackground" DropShadow ="false" PopupControlID ="pnlRejectCandidate" TargetControlID ="lbltar" OnCancelScript ="closepopups()"></ajaxToolkit:ModalPopupExtender>
     <ajaxToolkit:ModalPopupExtender ID ="mpeHiringDetails" BehaviorID ="MPEHiring" runat ="server" BackgroundCssClass ="DarkModalBackground" DropShadow ="false" PopupControlID ="pnlHiringDetails" TargetControlID ="lblhiringtar" OnCancelScript ="closepopups()"></ajaxToolkit:ModalPopupExtender>
     <ajaxToolkit:ModalPopupExtender ID ="mpeSubmissionDetails" BehaviorID ="MPESubmission" runat ="server" BackgroundCssClass ="DarkModalBackground" DropShadow ="false" PopupControlID ="pnlSubmissionDetails" TargetControlID ="lblsubmissiontar"  OnCancelScript ="closepopups()"></ajaxToolkit:ModalPopupExtender>
     <ajaxToolkit:ModalPopupExtender ID ="mpeJoiningDetails" BehaviorID ="MPEJoin" runat ="server" BackgroundCssClass ="DarkModalBackground" DropShadow ="false" PopupControlID ="pnlJoiningDetails" TargetControlID ="lblJoiningtar" OnCancelScript ="closepopups()"></ajaxToolkit:ModalPopupExtender>

 
<script type ="text/javascript" >

       
</script>
<asp:UpdatePanel >
<ContentTemplate >
 <ajaxToolkit:ModalPopupExtender ID ="mpeActionLog" BehaviorID ="MPEAction" runat ="server"  BackgroundCssClass ="DarkModalBackground" DropShadow ="false" PopupControlID ="pnlActionLog" TargetControlID ="lblTarAction"  OnCancelScript ="closepopups()"></ajaxToolkit:ModalPopupExtender>

<asp:Panel ID ="pnlActionLog" runat ="server" style=" display :none " >
<asp:Label ID ="lblTarAction" runat ="server" ></asp:Label>
<ucl:Template id="uclTemplateActionLog" runat ="server" ContentDisplay ="table-cell"   ModalBehaviourId= "MPEAction"  ContentHeight ="400px" ContentWidth ="800px"  >
  <Contents>
      <ucl:ActionLog runat ="server"  ID ="uclActionLog"/>
  </Contents> 
</ucl:Template>
</asp:Panel>
</ContentTemplate>
</asp:UpdatePanel>

<asp:Panel ID ="pnlRejectCandidate" runat ="server" style=" display :none " >
<asp:Label ID ="lbltar" runat ="server" ></asp:Label>
<ucl:Template id="uclTemplateReject" runat ="server" ContentDisplay ="table-cell"   ModalBehaviourId= "MPEReject"  ContentHeight ="250px" ContentWidth ="500px"  >
  <Contents>
      <ucl:RejectCandidate ID="uclRejectCandidate" runat ="server" />
  </Contents> 
</ucl:Template>
</asp:Panel>


<asp:Panel ID ="pnlHiringDetails" runat ="server" style=" display :none " >
<asp:Label ID ="lblhiringtar" runat ="server" ></asp:Label>
<ucl:Template id="uclTemplateHiringDetails" runat ="server" ModalTitle ="Offer Details" ContentDisplay ="table-cell"   ModalBehaviourId= "MPEHiring"  ContentHeight ="400px" ContentWidth ="600px"  >
  <Contents>
      <ucl:HiringDetails ID="uclHiringDetails" runat ="server" />
  </Contents> 
</ucl:Template>
</asp:Panel>


<asp:Panel ID ="pnlSubmissionDetails" runat ="server" style=" display :none " >
<asp:Label ID ="lblsubmissiontar" runat ="server" ></asp:Label>
<ucl:Template id="uclTemplateSubmissions" runat ="server" ModalTitle ="Submission Details" ContentDisplay ="table-cell"   ModalBehaviourId= "MPESubmission"  ContentHeight ="400px" ContentWidth ="600px"  >
  <Contents>
      <ucl:SubmissionDetails ID="uclSubmissionDetails" runat ="server" />
  </Contents> 
</ucl:Template>
</asp:Panel>

<asp:Panel ID ="pnlJoiningDetails" runat ="server" style=" display :none " >
<asp:Label ID ="lblJoiningtar" runat ="server" ></asp:Label>
<ucl:Template id="uclTemplateJoining" runat ="server" ModalTitle ="Joining Details" ContentDisplay ="table-cell"   ModalBehaviourId= "MPEJoin"  ContentHeight ="400px" ContentWidth ="600px"  >
  <Contents>
      <ucl:JoiningDetails ID ="uclJoiningDetails" runat ="server" />
  </Contents> 
</ucl:Template>
</asp:Panel>



<uc1:RequisitionHiringMatrix ID ="uclHiringMatrix" runat ="server" />
<%--
<div id="divJobTitle">--%>
<%--<div class="SpecializedTitle" style="padding-bottom:25px; text-align :left ;">
    <asp:Label ID="lblTitle" runat="server" Font-Size="X-Large" Font-Bold="true"></asp:Label> 
    <asp:HyperLink ID="lnkTitle" runat="server" Font-Size="X-Large" Target="_blank"></asp:HyperLink>
</div>--%>
<%--</div>--%>



