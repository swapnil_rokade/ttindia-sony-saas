﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using TPS360.Common.BusinessEntities;
using TPS360.Web.UI;
using TPS360.Common.Shared;
using System.Collections.Generic;

public partial class Controls_ChangeReqStatus : ATSBaseControl
{

    public int JobPostingID
    {
        get
        {
            if (CurrentJobPostingId == 0) return Convert.ToInt32(hdCurrentJobPostingID.Value);
            else return CurrentJobPostingId;
        }
        set
        {
            hdCurrentJobPostingID.Value = value.ToString();
        }
    }
    private JobPosting _currentJobPosting = null;
    public JobPosting  Current_JobPosting
    {
      
        get
        {
            return Facade.GetJobPostingById(JobPostingID);
        }

    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {


            JobPosting job = Current_JobPosting;
            MiscUtil.PopulateRequistionStatus(ddlJobStatus, Facade);
            ddlJobStatus.Items.Insert(0, new ListItem("Please Select", "0"));
            ddlJobStatus.SelectedValue = job.JobStatus.ToString();
            Label lbModalTitle= (Label ) this.Page.Master.FindControl("lbModalTitle");
            if(lbModalTitle !=null) lbModalTitle.Text = job.JobTitle + " - Change Requisition Status";


                    bool isAssingned = false;
                    JobPostingHiringTeam team = Facade.GetJobPostingHiringTeamByMemberId(base.CurrentMember.Id, Current_JobPosting.Id);
                    if (team != null) isAssingned = true;
            if(((Current_JobPosting.AllowRecruitersToChangeStatus == true && isAssingned) || Current_JobPosting.CreatorId == base.CurrentMember.Id))
            {
                ddlJobStatus.Enabled = true;
                divMessage.Visible = false;

            }
            else
            {
                ddlJobStatus.Enabled = false;
                btnChangeJobStatus.Text = "Close";
                btnChangeJobStatus.OnClientClick = "javascript:parent.CloseModal(); return false;";
            }
        }
      
    }
    protected void btnChangeJobStatus_Click(object sender, EventArgs e)
    {
        if (btnChangeJobStatus.Text == "Close") return;
        bool isStatusChanged = false;
        string message = string.Empty;
        if (Convert.ToInt32(ddlJobStatus.SelectedValue) > 0)
        {
                try
                {
                    JobPosting jobPosting = Current_JobPosting;
                    jobPosting.JobStatus = Int32.Parse(ddlJobStatus.SelectedValue);
                    jobPosting.UpdatorId = base.CurrentMember.Id;
                    Facade.UpdateJobPosting(jobPosting);
                    MiscUtil.EventLogForRequisition(EventLogForRequisition.RequisitionStatusChange,JobPostingID ,  CurrentMember.Id, Facade);
                }
                catch (ArgumentException ax)
                {
                    MiscUtil.ShowMessage(lblMessage, ax.Message, true);
                }
            
                MiscUtil.ShowMessage(lblMessage, "Successfully Updated Status", false);
        }
        else
        {
            message = "Please select Job Status.";
            MiscUtil.ShowMessage(lblMessage, message, false);
        }

       

    }

}
