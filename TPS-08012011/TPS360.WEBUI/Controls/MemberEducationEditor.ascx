﻿
<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MemberEducationEditor.ascx.cs" 
    Inherits="TPS360.Web.UI.ControlMemberEducationEditor"   %>
    <%@ Register Src="~/Controls/CountryStatePicker.ascx" TagName="CountryState" TagPrefix="ucl" %>
<%@ Register Src ="~/Controls/PagerControl.ascx" TagName ="Pager" TagPrefix ="ucl" %>  
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Controls/DateRangePicker.ascx" TagName = "DateRangePicker" TagPrefix ="ucl" %>
<script type ="text/javascript" language ="javascript"  src="../js/AjaxVariables.js"></script>
<script type ="text/javascript" language ="javascript" src="../js/AjaxScript.js" ></script>
<asp:UpdatePanel ID ="upEdution" runat ="server" >
<ContentTemplate >
  <div class="TableRow" style="text-align: center">
        <asp:Label EnableViewState="false" ID="lblMessage" runat="server"></asp:Label>
    </div>
<div style="width:100%">
    <div id="divEducationInputBlock" runat ="server" >
    <div class="TabPanelHeader" >
        Add Education Record
    </div>
      <div class="TableRow">
        <div class="TableFormLeble">
            <asp:Label EnableViewState="false" ID="lblInstituteName" runat="server" Text="University/College/School Name"></asp:Label>:
        </div>
        <div class="TableFormContent">
            <asp:TextBox EnableViewState="true" ID="txtInstituteName" runat="server" CssClass="CommonTextBox"
                ValidationGroup="EducationInfo" Width="350px" TabIndex ="1" ></asp:TextBox> <%--0.2 EnableViewState is set to True --%>
            <span class="RequiredField">*</span>
        </div>
    </div>
      <div class="TableRow">
        <div class="TableFormValidatorContent" style="float:right;width: 57.5%;">  <%--0.4 added enabled property --%>
            <asp:RequiredFieldValidator ID="rfvInstituteName" runat="server" ControlToValidate="txtInstituteName" 
                ErrorMessage="Please enter Institution name" EnableViewState="False" Display="Dynamic"
                ValidationGroup="EducationInfo" ></asp:RequiredFieldValidator>
        </div>
    </div>
    
    
      <div class="TableRow">
        <div class="TableFormLeble">
            <asp:Label EnableViewState="false" ID="lblLevelOfEducation" runat="server" Text="Level of Education"></asp:Label>:
        </div>
        <div class="TableFormContent">
            <asp:DropDownList EnableViewState="true" ID="ddlLevelOfEducation" runat="server"
                CssClass="CommonDropDownListx" TabIndex ="2">
            </asp:DropDownList>
            <span class="RequiredField">*</span>
        </div>
    </div>
    <div class="TableRow">
        <div class="TableFormValidatorContent" style="float:right;width: 57.5%;">  <%--0.4 added enabled property --%>
            <asp:CompareValidator ID="rfvLevelOfEducation" runat="server" ControlToValidate="ddlLevelOfEducation" 
                ErrorMessage="Please select Level Of Education" EnableViewState="False" Display="Dynamic" ValueToCompare="0" Operator="GreaterThan"
                ValidationGroup="EducationInfo" ></asp:CompareValidator>
        </div>
    </div>
      <div class="TableRow">
        <div class="TableFormLeble">
            <asp:Label EnableViewState="false" ID="lblDegree" runat="server" Text="Degree/Exam Title"></asp:Label>:
        </div>
        <div class="TableFormContent">
            <asp:TextBox EnableViewState="false" TabIndex ="3" ID="txtDegree" runat="server" CssClass="CommonTextBox">
            </asp:TextBox>
            
        </div>
    </div>
   
      <div class="TableRow">
        <div class="TableFormLeble">
            <asp:Label EnableViewState="false" ID="lblMajor" runat="server" Text="Major"></asp:Label>:
        </div>
        <div class="TableFormContent">
            <asp:TextBox EnableViewState="false" ID="txtMajor" runat="server" CssClass="CommonTextBox"
                Width="250px" TabIndex ="4"></asp:TextBox>
        </div>
    </div>
      <div class="TableRow">
        <div class="TableFormLeble">
            <asp:Label EnableViewState="false" ID="lblGPA" runat="server" Text="Passing %"></asp:Label>:
        </div>
        <div class="TableFormContent">
            <asp:TextBox EnableViewState="false" ID="txtGPA" runat="server" CssClass="CommonTextBox"
                Width="50px" TabIndex ="5"></asp:TextBox>&nbsp;&nbsp;
       
        </div>
    </div>
      <div class="TableRow">
        <div class="TableFormValidatorContent" style="float:right;width: 57.5%;">
            <asp:RegularExpressionValidator runat="server" ID="revGPA" ControlToValidate="txtGPA"
                EnableViewState="false" ErrorMessage="Please enter valid GPA score" ValidationExpression="([0-9]*\.[0-9]+|[0-9]*)" Display ="Dynamic" 
                SetFocusOnError="true" />
        </div>
    </div>
     
      <div class="TableRow">
        <div class="TableFormLeble">
            <asp:Label EnableViewState="false" ID="lblDuration" runat="server" Text="Duration"></asp:Label>:
        </div>
        <div class="TableFormContent" style="float: left;">
           <ucl:DateRangePicker ID="dtDuration" runat ="server"  />
        </div>
    </div>
      <div class="TableRow">
        <div class="TableFormValidatorContent" style="float:right;width: 57.5%;">
               <asp:Label ID="LblDatevalidate" runat="server" Text="" ForeColor="Red"></asp:Label>
        </div>
    </div>
   
      <div class="TableRow">
        <div class="TableFormLeble">
            <asp:Label EnableViewState="false" ID="lblCity" runat="server" Text="City"></asp:Label>:
        </div>
        <div class="TableFormContent">
            <asp:TextBox EnableViewState="true" ID="txtCity" runat="server" CssClass="CommonTextBox" TabIndex ="9"></asp:TextBox> <%--0.2 EnableViewState is set to True --%>
        </div>
    </div>
  <ucl:CountryState ID="uclCountryState" runat="server" FirstOption="Please Select" ApplyDefaultSiteSetting="True"  /> 
      <div class="TableRow">
        <div class="TableFormLeble">
            <asp:Label EnableViewState="false" ID="lblAccomplishment" runat="server" Text="Notes"></asp:Label>:
        </div>
        <div class="TableFormContent">
            <asp:TextBox EnableViewState="true" ID="txtAccomplishment" runat="server" TextMode="MultiLine"
                CssClass="CommonTextBox" Rows="5" Width="350px" TabIndex ="12"></asp:TextBox> <%--0.2 EnableViewState is set to True --%>
        </div>
    </div>
      <div class="TableRow">
        <div class="TableFormLeble">
        </div>
        <div class="TableFormContent">
            <asp:CheckBox runat="server" ID="chkHighestEducation" TabIndex ="13" Text="Is Highest Education?"  style=" display :inline ;" />
        </div>
    </div>
      <div class="TableRow" style="text-align: center">
        <asp:Button ID="btnSave" CssClass="CommonButton" runat="server" Text="Save" ValidationGroup="EducationInfo" 
            OnClick="btnSave_Click" TabIndex ="14"/> 
    </div>
      <br />
      <div class="TabPanelHeader" >
        Education History
    </div>
   </div>
   
   <strong>Note:</strong> <span style="color:red">*</span> indicates highest education.
    <div class="GridContainer" style="text-align: left;">
    
    <asp:TextBox ID ="hdnSortColumn" runat ="server" Visible ="false"  />
<asp:TextBox ID ="hdnSortOrder" runat ="server" Visible ="false" />
        <asp:ObjectDataSource ID="odsEducationList" runat="server" SelectMethod="GetPagedByMemberId"
            TypeName="TPS360.Web.UI.MemberEducationDataSource" EnablePaging="true" SelectCountMethod="GetListCount"
            SortParameterName="sortExpression">
            <SelectParameters>
                                <asp:Parameter Name="memberId" DefaultValue="0" Type="Int32" />
                            </SelectParameters>
            </asp:ObjectDataSource>
        <asp:ListView ID="lsvEducation" runat="server" DataSourceID="odsEducationList" DataKeyNames="Id"
            OnItemDataBound="lsvEducation_ItemDataBound" 
            OnItemCommand="lsvEducation_ItemCommand" OnPreRender ="lsvEducation_PreRender" >
            <LayoutTemplate>
                <table id="tlbTemplate" runat="server" class="Grid" cellspacing="0" border="0">
                    <tr>
                         <th style="width: 10%; white-space: nowrap;"><asp:LinkButton ID="lnkLevel" runat="server" CommandName="Sort" ToolTip="Sort By Level" CommandArgument="Name" Text="Level"  TabIndex ="15"/></th>
                         <th style="width: 15%; white-space: nowrap;"><asp:LinkButton ID="btnInstitutionName" runat="server" ToolTip="Sort By Institution Name" CommandName="Sort" CommandArgument="InstituteName" Text="School"  TabIndex ="16"/></th>
                         <th style="width: 10%; white-space: nowrap;"><asp:LinkButton ID="btnDegree" runat="server" CommandName="Sort" ToolTip="Sort By Degree Title" CommandArgument="DegreeTitle" Text="Degree" TabIndex ="17"/></th>
                         <th style="width: 10%; white-space: nowrap;"> <asp:LinkButton ID="btnMajor" runat="server" CommandName="Sort" CommandArgument="MajorSubjects" Text="Major" ToolTip="Sort By Major" TabIndex ="18"/></th>
                         <th style="width: 10%; white-space: nowrap;" runat ="server" id="thGPA">Passing %</th>
                         <th style="width: 15%; white-space: nowrap;">Duration</th>
                         <th style="width: 10%; white-space: nowrap;" runat ="server" id="thLocation"> Location</th>
                         <th style="width: 10%; white-space: nowrap;" runat ="server" id="thNotes">Notes</th>
                        <th style="text-align: center; width: 10%;" runat ="server" id ="thAction">Action</th>
                    </tr>
                    <tr id="itemPlaceholder" runat="server">
                    </tr>
                    <tr class="Pager">
                        <td colspan="9" runat ="server" id="tdPager">
                             <ucl:Pager Id="pagerControl" runat ="server" EnableViewState ="true"  />
                        </td>
                    </tr>
                </table>
            </LayoutTemplate>
            <EmptyDataTemplate>
                <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                    <tr>
                        <td>No Education data available.</td>
                    </tr>
                </table>
            </EmptyDataTemplate>
            <ItemTemplate>
                <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                    <td>  <asp:Label ID="lblLevel" runat="server" /><span id ="lblHigherEducation" runat ="server" visible ="false"  class="RequiredField">*</span> </td>
                    <td><asp:Label ID="lblInstitutionName" runat="server" /></td>
                    <td><asp:Label ID="lblDegree" runat="server" /></td>
                    <td><asp:Label ID="lblMajor" runat="server" /></td>
                    <td runat ="server" id="tdGPA"><asp:Label ID="lblGPA" runat="server" /></td>
                    <td style="white-space: nowrap;"><asp:Label ID="lblDuration" runat="server" /> </td>
                    <td style="white-space: nowrap;" runat ="server" id="tdLocation"><asp:Label ID="lblLocation" runat="server" /></td>
                    <td style="white-space: nowrap;" runat ="server" id="tdNotes"><asp:Label ID="lblNotes" runat="server" /></td>
                    <td style="text-align: center;" runat ="server" id="tdAction">
                        <asp:ImageButton ID="btnEdit" SkinID="sknEditButton" CausesValidation="false" runat="server"
                            CommandName="EditItem" TabIndex ="19" OnClientClick ="ScrollTop()"></asp:ImageButton>
                        <asp:ImageButton ID="btnDelete" Visible="true" SkinID="sknDeleteButton" runat="server"
                            CommandName="DeleteItem" TabIndex ="19"></asp:ImageButton>
                    </td>
                </tr>
            </ItemTemplate>
        </asp:ListView>
    </div>
    
</div>

</ContentTemplate> 
</asp:UpdatePanel> 

