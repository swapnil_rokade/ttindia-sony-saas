﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: MemberExperienceEditor.ascx.cs
    Description: This is the user control page used in resume builder to allow user to provide experience information.
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1             Nov-21-2008            Jagadish           Defect id : 8941; Changed the code to achive disabling or enabling the 'To' calender control
                                                              depends on 'Till Present' check box status.
                                                              Added a method 'chkDurationTill_CheckedChanged(object sender, EventArgs e)'.
  
    0.2             Apr-29-2009           Rajendra Prasad     Defect id: 10397;Added new method PopulateSiteSettings() to display
                                                              ddlcountry selected value according to sitesettings.
    0.3             Jul-06-2009           Veda                Defect id: 10838 :Not able to save Experience details when member Id is zero.
    0.4             July-08-2009          Gopala Swamy J      Defect id:10847;    
 *  0.5             Jul-30-2009           Veda                Defect id: 11132; Async mode is set to on 
 *  0.6             Sep-18-2009           Veda                Defect id: 11502; Default site settings country is not displayed.
 *  0.7             Feb-03-2009           Sudarshan           Defect id: 12116; Changed "ALL" to "0" in PopulateSiteSettings();
 *  0.8             Apr-21-2010           Ganapati Bhat       Defect id: 12656; Introduced Try,Catch in PrepareEditView()
 *  0.9             May-24-2010           Basavaraj Angadi    Defect : 12811 ; Code has been added in Page_Load Method to avoid display Deleted Rows in the Grid.
------------------------------------------------------------------------------------------------------------------------------------------- 
*/

using System;
using System.Collections;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Web.UI.Helper;
using TPS360.Common.Shared;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Web.UI;
using AjaxControlToolkit;
namespace TPS360.Web.UI
{
    public partial class ControlMemberExperienceEditor : BaseControl
    {
        #region Member Variables

        private static int _memberId = 0;
        private static int _experienceId = 0;
        private static string _memberrole = string.Empty;
        MemberExperience _memberExperience;
        private static bool _isCareerPortal = false;
        private bool IsManager = false;
        private bool IsOverviewPage = false;
        #endregion

        #region Properties

        MemberExperience CurrentMemberExperience
        {
            get
            {
                // _experienceId = 0;
                if (_memberExperience == null)
                {
                    if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_ID]))
                    {
                        _experienceId = Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_ID]);
                    }


                    if (_experienceId > 0)
                    {
                        _memberExperience = Facade.GetMemberExperienceById(_experienceId);
                    }
                    else
                    {
                        _memberExperience = new MemberExperience();
                    }
                }

                return _memberExperience;
            }
        }

        public string MemberRole
        {
            set { _memberrole = value; }
            get { return _memberrole; }
        }

        public bool IsCareerPortal
        {
            set { _isCareerPortal = value; }
            get { return _isCareerPortal; }
        }

        #endregion

        #region Methods

        private void GetMemberId()
        {
            //From Member Profile
            if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]))
            {
                hfMemberId.Value = Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID];
                _isCareerPortal = false;
            }
            //From Member Login
            else
            {
                hfMemberId.Value = base.CurrentMember.Id.ToString();
                _isCareerPortal = true;
            }
            _memberId = Int32.Parse(hfMemberId.Value);
        }

        private void BindList()
        {
            Image imgAll = (Image)lsvExperience.FindControl("imgAll");
           if(imgAll!=null) imgAll.Visible = false ;
            hdnExpandRowIds.Value = "";
            odsExperienceList.SelectParameters["memberId"].DefaultValue= _memberId.ToString();
            this.lsvExperience.DataSourceID = "odsExperienceList";
            this.lsvExperience.DataBind();
        }

        private void SaveMemberExperience()
        {
            if (IsValid) 
            {
                try
                {
                    MemberExperience memberExperience = BuildMemberExperience();
                    if (txtCompanyName.Text.Trim().ToString() != string.Empty && txtJobTitle.Text.Trim().ToString() != string.Empty  )
                    {
                        if (memberExperience.MemberId > 0) 
                        {
                            if (memberExperience.IsNew)
                            {
                                    Facade.AddMemberExperience(memberExperience);
                                    MiscUtil.ShowMessage(lblMessage, "Experience has been added successfully.", false);
                            }
                            else
                            {
                                Facade.UpdateMemberExperience(memberExperience);
                                MiscUtil.AddActivity(_memberrole, _memberId, base.CurrentMember.Id, ActivityType.UpdateExperience, Facade);
                                MiscUtil.ShowMessage(lblMessage, "Experience has been updated successfully.", false);
                            }
                            Facade.UpdateUpdateDateByMemberId(_memberId);
                            ClearText();
                            BindList();
                            _experienceId = 0;
                        }
                    }
                }
                catch (ArgumentException ex)
                {
                    MiscUtil.ShowMessage(lblMessage, ex.Message, true);
                }
            }
        }

        private MemberExperience BuildMemberExperience()
        {
            MemberExperience memberExperience = CurrentMemberExperience;

            memberExperience.CompanyName = txtCompanyName.Text = MiscUtil.RemoveScript(txtCompanyName.Text.Trim());
            memberExperience.FunctionalCategoryLookupId = Int32.Parse(ddlFunctionalCategory.SelectedValue);
            memberExperience.PositionName = txtJobTitle.Text = MiscUtil.RemoveScript(txtJobTitle.Text);
            memberExperience.CountryLookupId = uclCountryState.SelectedCountryId; 
            memberExperience.City = txtExperienceCity.Text = MiscUtil.RemoveScript(txtExperienceCity.Text.Trim());
            memberExperience.DateFrom = wdcEmpDurationFromDate.Text.Trim() =="" ? DateTime .MinValue  : Convert.ToDateTime(wdcEmpDurationFromDate.Text.Trim());

            if (!chkDurationTill.Checked)
            {
                memberExperience.IsTillDate = chkDurationTill.Checked;
                memberExperience.DateTo =wdcEmpDurationToDate.Text.Trim()=="" ? DateTime .MinValue : Convert.ToDateTime(wdcEmpDurationToDate.Text.Trim());
            }
            else
            {
                memberExperience.IsTillDate = chkDurationTill.Checked;
                memberExperience.DateTo = DateTime.Now.Date;
            }

            memberExperience.Responsibilities = txtProjectDetails.Text = MiscUtil.RemoveScript(txtProjectDetails.Text.Trim ());
            if (_memberId > 0)
            {
                memberExperience.MemberId = _memberId;
            }
            else
            {
                memberExperience.MemberId = base.CurrentMember.Id;
            }
            memberExperience.CreatorId = base.CurrentMember.Id;
            memberExperience.UpdatorId = base.CurrentMember.Id;
            memberExperience.StateId = uclCountryState.SelectedStateId;

            return memberExperience;
        }

        private void PopulateSiteSettings(DropDownList ddlCountry)
        {
            if (SiteSetting != null)
                ddlCountry.SelectedValue = (SiteSetting[DefaultSiteSetting.Country.ToString()].ToString());
        }
       
        private void PrepareView()
        {
            MiscUtil.PopulateCategory(ddlFunctionalCategory, Facade);
        }

        private void PrepareEditView()
        {
            MemberExperience memberExperience = Facade.GetMemberExperienceById(_experienceId); ;
            if (memberExperience != null)
            {
                try        
                {
                    txtCompanyName.Text =MiscUtil .RemoveScript ( memberExperience.CompanyName,string .Empty );
                    txtJobTitle.Text =MiscUtil .RemoveScript ( memberExperience.PositionName,string .Empty );
                    txtExperienceCity.Text = MiscUtil .RemoveScript (memberExperience.City,string .Empty );
                    uclCountryState.SelectedCountryId = memberExperience.CountryLookupId;
                    uclCountryState.SelectedStateId = memberExperience.StateId;
                    wdcEmpDurationFromDate.Value = memberExperience.DateFrom;
                    chkDurationTill.Checked = memberExperience.IsTillDate;

                    if (memberExperience.IsTillDate)
                    {
                        wdcEmpDurationToDate.Value = string.Empty;
                        wdcEmpDurationToDate.Enabled = false; 
                    }
                    else
                    {
                        wdcEmpDurationToDate.Value = memberExperience.DateTo;
                        wdcEmpDurationToDate.Enabled = true;  
                    }

                    txtProjectDetails.Text = MiscUtil.RemoveScript(memberExperience.Responsibilities, string.Empty);
                    ddlFunctionalCategory.SelectedValue = memberExperience.FunctionalCategoryLookupId.ToString();
                }
                catch (ArgumentOutOfRangeException e)    
                {
                    MiscUtil.ShowMessage(lblMessage, e.Message, true);
                    ddlFunctionalCategory.SelectedIndex = 0;
                }                                       
            }
        }

        private void ClearText()
        {
            txtCompanyName.Text = "";
            txtJobTitle.Text = "";
            uclCountryState.SelectedCountryId = 0;
            ddlFunctionalCategory.SelectedIndex = 0;
            txtExperienceCity.Text = "";
            wdcEmpDurationFromDate.Value = string.Empty;
            chkDurationTill.Checked = false;
            wdcEmpDurationToDate.Value = string.Empty;
            txtProjectDetails.Text = "";
        }

        private void PlaceUpDownArrow()
        {
            try
            {
                LinkButton lnk = (LinkButton)lsvExperience.FindControl(hdnSortColumn.Text );
                System.Web.UI.HtmlControls.HtmlTableCell im = (System.Web.UI.HtmlControls.HtmlTableCell)lnk.Parent;
                im.EnableViewState = false;
                im.Attributes.Add("class", (hdnSortOrder.Text  == "ASC" ? "Ascending" : "Descending"));


            }
            catch
            {
            }
        }
        #endregion

        #region Events

        #region Page event

        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (Page.Request.Url.AbsoluteUri.ToString().Contains("TPS360Overview.aspx"))
            {
                IsOverviewPage = true;
            }
            GetMemberId();
            MemberManager Manager = new MemberManager();
            Manager = Facade.GetMemberManagerByMemberIdAndManagerId(_memberId, CurrentMember.Id);
            if (Manager != null || IsUserAdmin)
                IsManager = true;
            else
                IsManager = false;
            
            if (!IsPostBack)
            {
                hdnSortColumn.Text = "btnDuration";
                hdnSortOrder.Text = "Desc";
                BindList();
                if (IsOverviewPage)
                {
                    divExperienceInputBlock.Visible = false;
                    return;
                }
                PrepareView();
            }
            //string pagesize = "";
            //pagesize = (Request.Cookies["ExperienceRowPerPage"] == null ? "" : Request.Cookies["ExperienceRowPerPage"].Value); ;
            // ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvExperience.FindControl("pagerControl");
            // if (PagerControl != null)
            // {
            //     DataPager pager = (DataPager)PagerControl.FindControl("pager");
            //     if (pager != null)   pager.PageSize = Convert.ToInt32(pagesize == "" ? "20" : pagesize);
            // }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
                rfvCompanyName.Enabled = true;
                rfvJobTitle.Enabled = true;
                SaveMemberExperience();
                Clear();
                uclCountryState.SelectedCountryId = 0;
        }

        private void Clear()
        {
            wdcEmpDurationFromDate.Value =  null;
            wdcEmpDurationToDate.Value = null;
            wdcEmpDurationToDate.Enabled = true;
        }
     
        #endregion

        #region ListView Events

        protected void lsvExperience_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            bool _IsAdmin = base.IsUserAdmin; 
            
            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                MemberExperienceDetail memberExperience = ((ListViewDataItem)e.Item).DataItem as MemberExperienceDetail;

                if (memberExperience != null)
                {
                    Label lblEmployerName = (Label)e.Item.FindControl("lblEmployerName");
                    Label lblJobTitle = (Label)e.Item.FindControl("lblJobTitle");
                    Label lblDuration = (Label)e.Item.FindControl("lblDuration");
                    Label lblLocation = (Label)e.Item.FindControl("lblLocation");
                    Label lblIndustryCategory = (Label)e.Item.FindControl("lblIndustryCategory");
                    Label lblFunctionalCategory = (Label)e.Item.FindControl("lblFunctionalCategory");
                    Label lblProjectdetails = (Label)e.Item.FindControl("lblProjectdetails");
                    Image imgShowHide = (Image)e.Item.FindControl("imgShowHide");
                    System.Web.UI.HtmlControls.HtmlTableRow row = (System.Web.UI.HtmlControls.HtmlTableRow)e.Item.FindControl("row");
                    System.Web.UI.HtmlControls.HtmlTableRow trDetail = (System.Web.UI.HtmlControls.HtmlTableRow)e.Item.FindControl("trDetail");
                    if (memberExperience.Responsibilities.Trim() != "")
                    {
                        Image imgAll = (Image)lsvExperience.FindControl("imgAll");
                        imgAll.Visible = true;
                        row.Attributes.Add("onclick", "javascript:ShowOrHide('" + trDetail.ClientID + "','"+ imgShowHide .ClientID +"');");
                        imgShowHide.Visible = true;
                        if( ! hdnExpandRowIds.Value.Contains (trDetail .ClientID )) hdnExpandRowIds.Value += trDetail.ClientID + ";" + imgShowHide.ClientID + ",";
                    }
                    else
                    {
                        imgShowHide.Visible = false;
                    }
                    ImageButton btnEdit = (ImageButton)e.Item.FindControl("btnEdit");
                    ImageButton btnDelete = (ImageButton)e.Item.FindControl("btnDelete");

                    lblEmployerName.Text = memberExperience.CompanyName;
                    lblEmployerName.ToolTip = memberExperience.Responsibilities;

                    
                    if (!memberExperience.IsTillDate)
                    {
                        lblDuration.Text = memberExperience.DateFrom.Year > 1900 ? memberExperience.DateFrom.ToShortDateString() : ""; ;
                        if (lblDuration.Text != "" && memberExperience.DateTo.Year > 1900) lblDuration.Text += " To ";
                        lblDuration.Text += memberExperience.DateTo.Year  > 1900 ? memberExperience.DateTo.ToShortDateString() : "";
                    }
                    else
                    {
                        lblDuration.Text = memberExperience.DateFrom.Year  > 1900 ? memberExperience.DateFrom.ToShortDateString() : "";
                        lblDuration.Text +=" To Till Date";
                    }
                    lblJobTitle.Text = memberExperience.PositionName;
                    string strLocation = memberExperience.City;
                    if (memberExperience.StateId!= 0) 
                    {
                        string state = Facade.GetStateNameById(memberExperience.StateId);
                        if (state != null&& state !=string .Empty )
                        {
                            if (strLocation != "") strLocation += ", ";
                            strLocation = strLocation  + state ; 
                        }
                    }
                    lblLocation.Text = strLocation;
                    lblFunctionalCategory.Text = memberExperience.FunctionalCategory;
                    lblProjectdetails.Text = memberExperience.Responsibilities.Replace("\n", "</br>");
                    btnDelete.OnClientClick = "return ConfirmDelete('experience entry')";
                    if (IsManager )
                        btnDelete.Visible = true;
                    else
                        btnDelete.Visible = MiscUtil.IsDeleteButtonVisible(_IsAdmin, _isCareerPortal);
                    btnEdit.CommandArgument = btnDelete.CommandArgument = StringHelper.Convert(memberExperience.Id);

                    if (IsOverviewPage)
                    {
                        System.Web.UI.HtmlControls.HtmlTableCell thAction = (System.Web.UI.HtmlControls.HtmlTableCell)lsvExperience.FindControl("thAction");
                        System.Web.UI.HtmlControls.HtmlTableCell tdAction = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdAction");
                        thAction.Visible = tdAction.Visible = !IsOverviewPage;


                        System.Web.UI.HtmlControls.HtmlTableCell tdPager = (System.Web.UI.HtmlControls.HtmlTableCell)lsvExperience.FindControl("tdPager");
                        tdPager.ColSpan = 6;
                    }
                }
            }
        }
  
        protected void lsvExperience_PreRender(object sender, EventArgs e)
        {
            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvExperience.FindControl("pagerControl");
              if (PagerControl != null)
              {
                  DataPager pager = (DataPager)PagerControl.FindControl("pager");
                  if (pager != null)
                  {
                      DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                      if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
                  }
                 // HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
                 // if (hdnRowPerPageName != null) hdnRowPerPageName.Value = "ExperienceRowPerPage";
              }
              PlaceUpDownArrow();


              Image imgAll = (Image)lsvExperience.FindControl("imgAll");
              if (imgAll != null)
              {
                  imgAll.Attributes.Remove("onclick");
                  imgAll.Attributes.Add("onclick", "ExpandAll('" + hdnExpandRowIds.ClientID + "','" + imgAll.ClientID + "')");
              }
              if (lsvExperience.Items.Count == 0)
              {
                  lsvExperience.DataSource = null;
                  lsvExperience .DataBind ();
              }
        }

        protected void lsvExperience_ItemCommand(object sender, ListViewCommandEventArgs e)
        {   try
            {
                if (e.CommandName == "Sort")
                {
                    LinkButton lnkbutton = (LinkButton)e.CommandSource;
                    if (hdnSortColumn.Text == lnkbutton.ID)
                    {
                        if (hdnSortOrder.Text == "ASC") hdnSortOrder.Text = "DESC";
                        else hdnSortOrder.Text = "ASC";
                    }
                    else
                    {
                        hdnSortColumn.Text = lnkbutton.ID;
                        hdnSortOrder.Text = "ASC";
                    }
                }
            }
            catch
            {
            }
            int id;

            int.TryParse(e.CommandArgument.ToString(), out id);

            if (id > 0)
            {
                if (string.Equals(e.CommandName, "EditItem"))
                {
                    txtCompanyName.Focus();
                    _experienceId = id;
                    PrepareEditView();
                }
                else if (string.Equals(e.CommandName, "DeleteItem"))
                {
                    try
                    {
                        if (Facade.DeleteMemberExperienceById(id))
                        {
                            BindList();
                            uclCountryState.SelectedCountryId = 0;
                            MiscUtil.AddActivity(_memberrole,_memberId,base.CurrentMember.Id,ActivityType.DeleteExperience,Facade);
                            MiscUtil.ShowMessage(lblMessage, "Experience has been deleted successfully.", false);
                           
                        }
                    }
                    catch (ArgumentException ex)
                    {
                        MiscUtil.ShowMessage(lblMessage, ex.Message, true);
                    }
                }
            }            
        }

        protected void chkDurationTill_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox chkDT = sender as CheckBox;
            wdcEmpDurationToDate.Enabled = !(chkDT.Checked);            
        }
       
        #endregion


        #endregion        
        
       
}
}
