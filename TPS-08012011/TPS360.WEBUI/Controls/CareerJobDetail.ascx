﻿ <%-- 
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName            :   CareerJobDetail.ascx.cs
    Description         :   Popup window .
    Created By          :   Pravin
    Created On          :   18/Jan/2016
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
 ---------------------------------------------------------------------------------------------------------------------------------------------------------------------        
--%>

<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CareerJobDetail.ascx.cs" Inherits="TPS360.Web.UI.Controls_CareerJobDetail" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
 
<style>
    </style>
    
    
<div class="TabPanelHeader">
    Basic Details</div>
    
     <script language="javascript" type="text/javascript">
         function Redirect() {
             alert('1');
             window.location = a; //"http://www.tutorialspoint.com";
         }
</script>
<div id="divBasicInfo" runat="server" style="padding-left: 25px;" visible="false">
</div>
<div id = "divBasicInfor_New" runat = "server" style = "padding-left: 25px;">
<table  width="100%"; style="text-align :left; font:Arial;  font-size :13px;">
<tr>
<td>
<asp:Label ID="Label1" runat="server" Text="Job Title "></asp:Label>:<asp:Label ID="lbljobtitle" runat="server" Text=""></asp:Label>
</td>
</tr>

<%--<tr>
<td>
<asp:Label ID="Label2" runat="server" Text="No. of Openings "></asp:Label>:<asp:Label ID="lblNoOfOpenings" runat="server" Text=""></asp:Label>
 </td>
</tr>--%>

<%--<tr>
<td>
<asp:Label ID="Label4" runat="server" Text="Date of Requisition"></asp:Label>:<asp:Label ID="lblDateOfRequisition" runat="server" Text=""></asp:Label>
 </td>
</tr>--%>

<%--<tr>
<td>
<asp:Label ID="Label8" runat="server" Text="Open Date "></asp:Label>:<asp:Label ID="lblopendate" runat="server" Text=""></asp:Label>
 </td>
</tr>
--%>
<%--<tr>
<td>
<asp:Label ID="Label6" runat="server" Text="Expected Fulfillment Date "></asp:Label>:<asp:Label ID="lblfulfilldate" runat="server" Text=""></asp:Label>
 </td>
</tr>--%>

<tr>
<td>
<asp:Label ID="Label10" runat="server" Text="City "></asp:Label>:<asp:Label ID="lblcity" runat="server" Text=""></asp:Label>
  </td>
</tr>

<tr>
<td>
<asp:Label ID="Label12" runat="server" Text="Country "></asp:Label>:<asp:Label ID="lblcountry" runat="server" Text=""></asp:Label>
  </td>
</tr>

<tr>
<td>
<asp:Label ID="Label14" runat="server" Text="Experience Required  "></asp:Label>:<asp:Label ID="lblexperence" runat="server" Text=""></asp:Label>
 </td>
</tr>

<tr>
<td>
<asp:Label ID="Label16" runat="server" Text="Education "></asp:Label>:<asp:Label ID="lbleducation" runat="server" Text=""></asp:Label>
 </td>
</tr>
</table>

</div>




<div id="divJobDescription" runat="server">
    <div class="TabPanelHeader">
        Job Description:</div>
    <div style="overflow: auto" style="padding-left: 25px; white-space: normal">
        <div id="divDescription" runat="server" style="margin-left: 5%; width: 90%; white-space: normal;">
        </div>
    </div>
</div>
<div id="divClientBrief" runat="server">
    <div class="TabPanelHeader">
        Client Brief:</div>
    <div style="overflow: auto" style="padding-left: 25px;">
        <div id="divCBrief" runat="server" style="margin-left: 5%; width: 95%; white-space: normal;">
        </div>
    </div>
</div>
<div id="divjobskill" runat="server">
    <div class="TabPanelHeader">
        Required Skills:</div>
    <div runat="server" id="divSkillsContent" style="padding-left: 50px;">
    </div>
</div>
<div id="divMQP" runat="server">
    <div class="TabPanelHeader">
        Minimum Qualifying Parameters:</div>
    <div style="padding-left: 50px;" id="divMQPContent" runat="server">
    </div>
</div>
<div id="divDocuments" runat="server">
    <div class="TabPanelHeader">
        Documents:</div>
    <div style="padding-left: 50px;" id="divDocumentContent" runat="server">
    </div>
</div>
<div id="divbenefits" runat="server">
    <div class="TabPanelHeader">
        Benefits:</div>
    <div style="padding-left: 50px;" runat="server" id="divBenefitsContent">
    </div>
</div>

<%--************ code comment by pravin khot on 18/Jan/2016******************--%>
<%--<div id="divHiringTeam" visible="false" runat="server">
<div class="TabPanelHeader">
    Hiring Team:
    </div>
    </div>
<div class="GridContainer" style="padding-left: 25px;">
    <asp:ListView ID="lsvAssignedTeam" runat="server" DataKeyNames="Id" OnItemDataBound="lsvAssignedTeam_ItemDataBound"
    OnPreRender="lsvAssignedTeam_PreRender">
        <LayoutTemplate>
            <table id="tlbTemplate" runat="server" class="Grid" cellspacing="0" border="0">
                <tr>
                    <th style="white-space: nowrap;">
                        Name
                    </th>
                    <th style="white-space: nowrap;">
                        Email
                    </th>
                    <th style="white-space: nowrap;">
                        Role
                    </th>
                </tr>
                <tr id="itemPlaceholder" runat="server">
                </tr>
            </table>
        </LayoutTemplate>
        <EmptyDataTemplate>
            <table id="tblEmptyData" style="" class="EmptyDataTable alert alert-warning" runat="server">
                <tr>
                    <td>
                        No recruiters assigned.
                    </td>
                </tr>
            </table>
        </EmptyDataTemplate>
        <ItemTemplate>
            <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
            
                <td style="text-align: left">
                    <asp:Label ID="lblMemberName" runat="server" />
                </td>
                <td style="text-align: left">
                    <asp:Label ID="lblMemberEmail" runat="server" />
                </td>
                <td style="text-align: left">
                    <asp:Label ID="lblEmployeeType" runat="server" />
                </td>
            </tr>
        </ItemTemplate>
    </asp:ListView>
</div>--%>
<%--<div id="divAddiNotes" runat="server">
    <div class="TabPanelHeader">
        Additional Notes:</div>
    <div id="divAdditionalNotes" runat="server" style="padding-left: 25px; white-space: normal">
    </div>
</div>--%>
<%--***************************end***********************************--%>
