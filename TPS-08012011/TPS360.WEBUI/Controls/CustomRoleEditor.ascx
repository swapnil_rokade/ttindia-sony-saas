﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CustomRoleEditor.ascx.cs" Inherits="TPS360.Web.UI.ControlCustomRoleEditor" %>
<%@ Register src="../Controls/MenuSelector.ascx" tagname="MenuSelector" tagprefix="uc1" %>
<div style="padding: 15px;">
    <div class="MainBox" style="width: 100%;">
        <div class="BoxHeader">
            <div class="BoxHeaderContainer">
                <div class="TitleBoxLeft">
                    <asp:Image ID="imgArrow" runat="server" SkinID="sknArrow" AlternateText="" />
                </div>
                <div class="TitleBoxMid">
                    Custom Role Editor
                </div>
            </div>
        </div>
        <div class="HeaderDevider">
        </div>
        <div class="MasterBoxContainer">
            <div class="ChildBoxContainer">
                <div style="clear: both">
                </div>
                <div class="BoxContainer">
                    <asp:UpdatePanel ID="pnlDynamic" runat="server">
                        <ContentTemplate>
                            <div class="TableRow" style="text-align: center">
                                <asp:Label ID="lblMessage" runat="server"></asp:Label>
                            </div>
                            <div class="TableRow">
                                <div class="TableFormLeble">
                                    <asp:Label ID="lblName" runat="server" Text="Name"></asp:Label>:
                                </div>
                                <div class="TableFormContent">
                                    <asp:TextBox ID="txtName" runat="server" CssClass="CommonTextBox"></asp:TextBox>
                                   <span class="RequiredField">*</span>
                                </div>
                            </div>
                            <div class="TableRow">
                                <div class="TableFormValidatorContent" style =" margin-left :42%">
                                    <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="txtName" ErrorMessage="Please enter name." EnableViewState="False" Display="Dynamic" ValidationGroup ="RoleEditor"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="TableRow">
                                <div class="TableFormLeble">
                                    <asp:Label ID="lblDescription" runat="server" Text="Description"></asp:Label>:
                                </div>
                                <div class="TableFormContent">
                                    <asp:TextBox ID="txtDescription" Rows="4" Width="300px" runat="server" CssClass="CommonTextBox" TextMode="MultiLine"></asp:TextBox>
                                </div>
                            </div>
                            <div class="TableRow" style="text-align: center">
                                <div class="TableFormLeble">
                                   <asp:Label ID="lblMenuAccess" runat="server" Text="Application Access"></asp:Label>: 
                                </div>
                                <div class="TableFormContent" style =" float :left ">
                                    <div style="width:250px; text-align:left; vertical-align:top">
                                        <uc1:MenuSelector ID="ctlMenuSelector" runat="server" />
                                    </div>
                                </div>
                            </div>
                            <br />
                            <div class="TableRow" style="text-align: center">
                                <div class="TableFormLeble">
                                </div>
                                <div class="TableFormContent">
                                <asp:Button ID="btnSave" CssClass="CommonButton" runat="server" Text="Save" OnClick="btnSave_Click" ValidationGroup ="RoleEditor" />
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <asp:Image ID="imgRightTopImage" runat="server" SkinID="sknRightTopImage" AlternateText="" />
                <asp:Image ID="imgBoxLeftBottom" runat="server" SkinID="sknBoxLeftBottom" AlternateText="" />
                <asp:Image ID="imgBoxRightBottom" runat="server" SkinID="sknBoxRightBottom" AlternateText="" />
            </div>
        </div>
    </div>
</div>