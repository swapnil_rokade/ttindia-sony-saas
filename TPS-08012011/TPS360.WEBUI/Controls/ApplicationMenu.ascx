﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ApplicationMenu.ascx.cs"
    Inherits="TPS360.Web.UI.ApplicationMenu" EnableViewState="false" %>

	<div class="nav-collapse">
		<ul class="nav">
<li class="dropdown"></li>
            <asp:Repeater ID="rptTopMenu" runat="server" OnItemDataBound="rptTopMenu_ItemDataBound">
                <ItemTemplate>
                    <li class="dropdown" runat="server" id="liId" rel="home">
                        <a class="dropdown-toggle" data-toggle="dropdown" runat="server" id="lnkMenuName">
                            <asp:Label ID="lblMenuName" runat="server" ></asp:Label>
                        </a>
                        <ul id="ulSubMenu" runat="server" class="dropdown-menu">
                        </ul>
                   </li>
                </ItemTemplate>
            </asp:Repeater>               
        </ul> 
    </div> 
          
    
    <asp:SiteMapDataSource runat="server" ID="ApplicationSiteMap" SiteMapProvider="SqlSiteMap"
    ShowStartingNode="false" />
