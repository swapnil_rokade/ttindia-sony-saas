﻿<%--Suraj Adsule
20160804
candidateJoiningForm--%>
<%@ Control AutoEventWireup="true" CodeFile="CandidateJoiningJobDetails.ascx.cs"
    Inherits="Controls_CandidateJoiningJobDetails" Language="C#" %>
<link href="../assets/css/chosen.css" rel="Stylesheet" />

<script type="text/javascript" src="../assets/js/chosen.jquery.js"></script>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Assembly="Infragistics35.Web.v12.2, Version=12.2.20122.1007, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.Web.UI.EditorControls" TagPrefix="ig" %>
<%@ Register Assembly="Infragistics35.Web.v12.2, Version=12.2.20122.1007, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.Web.UI.LayoutControls" TagPrefix="ig" %>
<style>
    .TableFormLeble
    {
        width: 180px;
        font-size: 13px !important;
        font-weight: normal !important;
    }
    html, #Body, #WrapperOuter, #Footer
    {
        background: none !important;
    }
    html, #Body, #WrapperOuter, #Footer
    {
        background: transparent !important;
    }
</style>
<div style="margin: 0 auto;">
    <asp:Panel ID="pnlCandidateJobDetail" runat="server">
        <asp:Label ID="lblMessage" runat="server" EnableViewState="false"></asp:Label>
        <div class="TableRow">
            <div class="FormLeftColumn">
                <div class="TableRow">
                    <div class="TableFormLeble">
                        <asp:Label ID="lblDemandId" runat="server" Text="Demand ID"></asp:Label>
                        :
                    </div>
                    <div class="TableFormContent">
                        <asp:Label ID="lblDemandIdValue" runat="server" Enabled="false" Style="color: #444444;
                            font-size: 13px; font-weight: normal;"></asp:Label>
                    </div>
                    <%--<div class="TableRow">
                                    <div class="TableFormValidatorContent" style="margin-left: 42%">
                                        <asp:RequiredFieldValidator ID="rfvDemandID" runat="server" ControlToValidate="lblDemandIdValue"
                                            ErrorMessage="Demand ID is not provided" ValidationGroup="ValGrpJobTitle"
                                            InitialValue="0"></asp:RequiredFieldValidator>
                                    </div>
                      </div>--%>
                    <br />
                </div>
                <div class="TableRow">
                    <div class="TableFormLeble">
                        <asp:Label ID="lblJobRestriction" runat="server" Text="Job Restrictions"></asp:Label>
                        :
                    </div>
                    <div class="TableFormContent">
                        <asp:DropDownList ID="ddlJobRestriction" runat="server" CssClass="chzn-select" TabIndex="14"
                            Width="40%">
                        </asp:DropDownList>
                        <span id="Span2" class="RequiredField">*</span>
                    </div>
                    <div class="TableRow">
                        <div class="TableFormValidatorContent" style="margin-left: 42%">
                            <asp:RequiredFieldValidator ID="rfvJobRestriction" runat="server" ControlToValidate="ddlJobRestriction"
                                Display="Dynamic" ErrorMessage="Please select job restiction " InitialValue="0"
                                ValidationGroup="ValGrpJobTitle"></asp:RequiredFieldValidator>
                        </div>
                        <br />
                    </div>
                </div>
                <div class="TableRow">
                    <asp:Panel ID="Panel4" runat="server">
                        <div class="TableFormLeble">
                            <asp:Label ID="lblEmployeeType" runat="server" Text="Worker Type/Employee Type"></asp:Label>
                            :
                        </div>
                        <div class="TableFormContent">
                            <asp:DropDownList ID="ddlEmployeeType" runat="server" CssClass="chzn-select" TabIndex="14"
                                Width="40%">
                            </asp:DropDownList>
                            <span id="Span1" class="RequiredField">*</span>
                        </div>
                        <div class="TableRow">
                            <div class="TableFormValidatorContent" style="margin-left: 42%">
                                <asp:RequiredFieldValidator ID="rfvEmployeeType" runat="server" ControlToValidate="ddlEmployeeType"
                                    Display="Dynamic" ErrorMessage="Please select Employee Type" InitialValue="0"
                                    ValidationGroup="ValGrpJobTitle"></asp:RequiredFieldValidator>
                            </div>
                            <br />
                        </div>
                    </asp:Panel>
                </div>
            </div>
            <div class="FormRightColumn">
                <div class="TableRow">
                    <div class="TableFormLeble">
                        <asp:Label ID="lblRequesitionTitle" runat="server" Text="Requisition Title"></asp:Label>
                        :
                    </div>
                    <div class="TableFormContent" style="vertical-align: top">
                        <asp:Label ID="lblRequisitionTitleValue" runat="server" Style="color: #444444; font-size: 13px;
                            font-weight: normal;">
                        </asp:Label>
                    </div>
                    <%--<div class="TableRow">
                                    <div class="TableFormValidatorContent" style="margin-left: 42%">
                                        <asp:RequiredFieldValidator ID="rfvRequisitionTitle" runat="server" ControlToValidate="lblRequisitionTitleValue"
                                            ErrorMessage="Requisition Title is not available" ValidationGroup="ValGrpJobTitle"
                                            InitialValue="0"></asp:RequiredFieldValidator>
                                    </div>
                      </div>--%>
                    <br />
                </div>
                <div class="TableRow">
                    <div class="TableFormLeble">
                        <asp:Label ID="lblDateOfJoining" runat="server" Text="Date Of Joining"></asp:Label>
                        :
                    </div>
                    <div class="TableFormContent" style="vertical-align: top">
                        <ig:WebDatePicker ID="wdcOpenDate" runat="server" DropDownCalendarID="ddcOpenDate"
                            Style="float: left;">
                            <ClientSideEvents ValueChanged="wdcOpenDate_ValueChanged" />
                        </ig:WebDatePicker>
                        <span class="RequiredField">*</span>
                        <ig:WebMonthCalendar ID="ddcOpenDate" runat="server" AllowNull="true" HideOtherMonthDays="true">
                        </ig:WebMonthCalendar>
                    </div>
                    <div class="TableRow">
                        <div class="TableFormValidatorContent" style="margin-left: 42%">
                            <asp:RequiredFieldValidator ID="rfvDoJ" runat="server" ControlToValidate="wdcOpenDate"
                                Display="Dynamic" ErrorMessage="Please select Date of Joining" ValidationGroup="ValGrpJobTitle">
                            </asp:RequiredFieldValidator>
                            <%--Display="Dynamic"--%>
                            <asp:CompareValidator ID="cvDoJ" runat="server" ControlToValidate="wdcOpenDate" Display="Dynamic"
                                ErrorMessage="Please select today's date or future date only." Operator="GreaterThanEqual"
                                Type="String" ValidationGroup="ValGrpJobTitle">
                            </asp:CompareValidator>
                        </div>
                        <br />
                    </div>
                </div>
                <div class="TableRow">
                    <div class="TableFormLeble">
                        <asp:Label ID="lblWorkerSubType" runat="server" Text="Worker Sub Type"></asp:Label>
                        :
                    </div>
                    <div class="TableFormContent" style="vertical-align: top">
                        <asp:DropDownList ID="ddlWorkerSubType" runat="server" CssClass="chzn-select" TabIndex="17"
                            Width="40%">
                        </asp:DropDownList>
                        <%--<span class="RequiredField">*</span>--%>
                    </div>
                    <div class="TableRow">
                        <%--<div class="TableFormValidatorContent" style="margin-left: 42%">
                                        <asp:RequiredFieldValidator ID="rfvWorkerSubType" runat="server" ControlToValidate="ddlWorkerSubType"
                                            ErrorMessage="Please select Worker sub type." ValidationGroup="ValGrpJobTitle"
                                            InitialValue="0"></asp:RequiredFieldValidator>
                                    </div>--%>
                        <br />
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlCandidatePayInfo" runat="server">
        <div class="TableRow">
            <div class="FormLeftColumn">
                <div class="TableRow">
                    <div class="TableFormLeble">
                        <asp:Label ID="lblPayGroup" runat="server" Text="Pay Group"></asp:Label>
                        :
                    </div>
                    <div class="TableFormContent">
                        <asp:DropDownList ID="ddlPayGroup" runat="server" CssClass="chzn-select" TabIndex="14"
                            Width="40%">
                        </asp:DropDownList>
                        <span id="spPayGroup" class="RequiredField">*</span>
                    </div>
                    <div class="TableRow">
                        <div class="TableFormValidatorContent" style="margin-left: 42%">
                            <asp:RequiredFieldValidator ID="rfvPayGroup" runat="server" ControlToValidate="ddlPayGroup"
                                Display="Dynamic" ErrorMessage="Please select Pay group." InitialValue="0" ValidationGroup="ValGrpJobTitle"></asp:RequiredFieldValidator>
                        </div>
                        <br />
                    </div>
                </div>
                <div class="TableRow">
                    <div class="TableFormLeble">
                        <asp:Label ID="lblCurrency" runat="server" Text="Currency"></asp:Label>
                        :
                    </div>
                    <div class="TableFormContent">
                        <asp:DropDownList ID="ddlCurrency" runat="server" CssClass="chzn-select" TabIndex="14"
                            Width="40%">
                        </asp:DropDownList>
                        <span id="SpanCurrency" class="RequiredField">*</span>
                    </div>
                    <div class="TableRow">
                        <div class="TableFormValidatorContent" style="margin-left: 42%">
                            <asp:RequiredFieldValidator ID="rfvCurrency" runat="server" ControlToValidate="ddlCurrency"
                                Display="Dynamic" ErrorMessage="Please select currency" InitialValue="0" ValidationGroup="ValGrpJobTitle"></asp:RequiredFieldValidator>
                        </div>
                        <br />
                    </div>
                </div>
                <div class="TableRow">
                    <div class="TableFormLeble">
                        <asp:Label ID="lblCompensationPlan" runat="server" Text="Compensation Plan"></asp:Label>
                        :
                    </div>
                    <div class="TableFormContent">
                        <asp:DropDownList ID="ddlCompensationPlan" runat="server" CssClass="chzn-select"
                            TabIndex="14" Width="40%">
                        </asp:DropDownList>
                        <span id="Span3" class="RequiredField">*</span>
                    </div>
                    <div class="TableRow">
                        <div class="TableFormValidatorContent" style="margin-left: 42%">
                            <asp:RequiredFieldValidator ID="rfvCompensationPlan" runat="server" ControlToValidate="ddlCompensationPlan"
                                Display="Dynamic" ErrorMessage="Please select compensation plan" InitialValue="0"
                                ValidationGroup="ValGrpJobTitle"></asp:RequiredFieldValidator>
                        </div>
                        <br />
                    </div>
                </div>
            </div>
            <div class="FormRightColumn">
                <div class="TableRow">
                    <div class="TableFormLeble">
                        <asp:Label ID="lblPayGroupGrade" runat="server" Text="Pay Group Grade"></asp:Label>
                        :
                    </div>
                    <div class="TableFormContent">
                        <asp:DropDownList ID="ddlPayGroupGrade" runat="server" CssClass="chzn-select" TabIndex="14"
                            Width="40%">
                        </asp:DropDownList>
                        <span id="Span4" class="RequiredField">*</span>
                    </div>
                    <div class="TableRow">
                        <div class="TableFormValidatorContent" style="margin-left: 42%">
                            <asp:RequiredFieldValidator ID="rfvPayGroupGrade" runat="server" ControlToValidate="ddlPayGroupGrade"
                                Display="Dynamic" ErrorMessage="Please select pay group grade" InitialValue="0"
                                ValidationGroup="ValGrpJobTitle"></asp:RequiredFieldValidator>
                        </div>
                        <br />
                    </div>
                </div>
                <div class="TableRow">
                    <div class="TableFormLeble">
                        <asp:Label ID="lblFrequency" runat="server" Text="Frequency"></asp:Label>
                        :
                    </div>
                    <div class="TableFormContent" style="vertical-align: top">
                        <asp:DropDownList ID="ddlFrequency" runat="server" CssClass="chzn-select" TabIndex="14"
                            Width="40%">
                        </asp:DropDownList>
                        <span class="RequiredField">*</span>
                    </div>
                    <div class="TableRow">
                        <div class="TableFormValidatorContent" style="margin-left: 42%">
                            <asp:RequiredFieldValidator ID="rfvFrequency" runat="server" ControlToValidate="ddlFrequency"
                                Display="Dynamic" ErrorMessage="Please select frequency" InitialValue="0" ValidationGroup="ValGrpJobTitle"></asp:RequiredFieldValidator>
                        </div>
                        <br />
                    </div>
                </div>
                <div class="TableRow">
                    <div class="TableFormLeble">
                        <asp:Label ID="lblMaximumFTE" runat="server" Text="Maximum FTE%"></asp:Label>
                        :
                    </div>
                    <div class="TableFormContent" style="vertical-align: top">
                        <asp:TextBox ID="txtMaximumFTE" runat="server" CssClass="CommonTextBox" MaxLength="5"></asp:TextBox>
                        <span id="spMaximumFTE" class="RequiredField">*</span>
                    </div>
                    <div class="TableRow">
                        <div class="TableFormValidatorContent" style="margin-left: 42%">
                            <asp:RequiredFieldValidator ID="rfvMaximumFTE" runat="server" ControlToValidate="txtMaximumFTE"
                                Display="Dynamic" ErrorMessage="Please Enter Maximum FTE" ValidationGroup="ValGrpJobTitle"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="revMaximumFTE" runat="server" ControlToValidate="txtMaximumFTE"
                                Display="Dynamic" ErrorMessage="Please Enter Numbers Only" ValidationExpression="^\d+$"
                                ValidationGroup="ValGrpJobTitle">        
                            </asp:RegularExpressionValidator>
                            <%-- ^[0-9]$    ^([0-9]{4,4})$* InitialValue="0"   <asp:CompareValidator ID="cvMaximumFTE" runat="server" Operator="DataTypeCheck" Type="Integer" Display="Dynamic"
                                            ControlToValidate="txtMaximumFTE" ErrorMessage="Value must be a whole number" />--%>
                        </div>
                        <br />
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlCanididateJoiningNotes" runat="server">
        <div class="TableRow" style="float: left; width: 100%;">
            <asp:Label ID="lblNotes" runat="server" Style="padding-left: 145px; padding-top: 0px;
                color: #444444; font-size: 13px; font-weight: normal; float: left;" Text="Notes:"></asp:Label>
            <div class="TableFormContent" style="float: left;">
                <asp:TextBox ID="txtNotes" runat="server" Columns="80" CssClass="CommonTextBox" Rows="5"
                    TextMode="multiline" Width="407px"></asp:TextBox>
            </div>
        </div>
    </asp:Panel>
    <br />
    <div style="padding-left: 350px;">
        <asp:Button ID="btnSubmit" runat="server" OnClick="btnSubmit_Click" Text="Submit"
            ValidationGroup="ValGrpJobTitle" />
    </div>
</div>

<script type="text/javascript">
    Sys.Application.add_load(function() {
        $(".chzn-select").chosen();
    });
</script>

<script>
    function wdcOpenDate_ValueChanged(sender, eventArgs) {

        var Opendate = $find("<%=wdcOpenDate.ClientID%>").get_value();
        var TempDate = new Date(Opendate);
        var numberOfDaysToAdd = 30;
        TempDate.setDate(Opendate.getDate() + numberOfDaysToAdd);

        var dd = TempDate.getDate();
        var mm = TempDate.getMonth() + 1;
        var y = TempDate.getFullYear();


        var FormattedDate = dd + '/' + mm + '/' + y;
    }
</script>

