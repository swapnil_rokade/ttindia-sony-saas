/*<%-- 
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: VendorApplicants.ascx.cs
    Description: This is the user control page used to display the ooption like ToDo,PDF,Print and many more
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.         Date              Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1            15/March/2017     pravin khot         Added new column -ID,Contact Number,NP
-------------------------------------------------------------------------------------------------------------------------------------------       

--%>*/
using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Web.UI.Helper;
using TPS360.Web.UI;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using TPS360.BusinessFacade;
using System.Collections.Generic;
using TPS360.Common;
using System.IO;
using ExpertPdf.HtmlToPdf;
using System.Drawing;

public partial class VendorApplicants : BaseControl, IWidget
{
    private bool _IsMemberMailAccountAvailable = false;
    private bool isAccess = false;
    #region Properties

    private IWidgetHost _Host;

    public IWidgetHost Host
    {
        get { return _Host; }
        set { _Host = value; }
    }

    private string _userId;
    public string UserId
    {
        get { return _userId; }
        set { _userId = value; }
    }

    #endregion

    #region Events

    protected void Page_Load(object sender, EventArgs e)
    {
        _IsMemberMailAccountAvailable = MiscUtil.IsValidMailSetting(Facade, base.CurrentMember.Id);
        (this as IWidget).HideSettings();
        ArrayList AList = Facade.GetAllMemberPrivilegeIdsByMemberId(CurrentMember.Id);
        if (AList.Contains(50))
            isAccess = true;
        else
            isAccess = false ;
        odsCandidateList.SelectParameters["SortOrder"].DefaultValue = SortOrder.Text.ToString();
        odsCandidateList.SelectParameters["memberId"].DefaultValue = base.CurrentMember.Id.ToString();
        odsCandidateList.SelectParameters["IsVendorContact"].DefaultValue = IsUserVendor.ToString();
        odsCandidateList.SelectParameters["IsVendor"].DefaultValue = "false";

       
        if (!IsPostBack)
        {
            //uclDateRange .setDateRange (DateTime.Now.AddDays(-6), DateTime.Now);
            GetWidgetData();
            txtSortColumn.Text = "btnName";
            txtSortOrder.Text = "ASC";
            PlaceUpDownArrow();
        }
        string pagesize = "";
        pagesize = (Request.Cookies["DashboardCandidateListRowPerPage"] == null ? "" : Request.Cookies["DashboardCandidateListRowPerPage"].Value); ;
        ASP.controls_smallpagercontrol_ascx PagerControl = (ASP.controls_smallpagercontrol_ascx)this.lsvCandidateList .FindControl("pagerControl");
        if (PagerControl != null)
        {
            DataPager pager = (DataPager)PagerControl.FindControl("pager");
            if (pager != null) pager.PageSize = Convert.ToInt32(pagesize == "" ? "10" : pagesize);
        }
   
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        (this as IWidget).HideSettings();
    }

    #region ListView Events
    protected void lsvCandidateList_PreRender(object sender, EventArgs e)
    {
        lsvCandidateList.DataBind();
        ASP.controls_smallpagercontrol_ascx PagerControl = (ASP.controls_smallpagercontrol_ascx)this.lsvCandidateList .FindControl("pagerControl");
        if (PagerControl != null)
        {
            DataPager pager = (DataPager)PagerControl.FindControl("pager");
            if (pager != null)
            {
                if (pager.Controls.Count >= 1)
                {
                    DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                    if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
                }
            }
            HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
            if (hdnRowPerPageName != null) hdnRowPerPageName.Value = "DashboardCandidateListRowPerPage";
        }
        PlaceUpDownArrow();

        System.Web.UI.HtmlControls.HtmlTable tblEmptyData = (System.Web.UI.HtmlControls.HtmlTable)lsvCandidateList.FindControl("tblEmptyData");
        if (lsvCandidateList.Controls.Count == 0 || tblEmptyData !=null)
        {
            lsvCandidateList.DataSource = null;
            lsvCandidateList.DataBind();
            ExportButtons.Visible = false;
        }

    }
    protected void lsvCandidateList_ItemDataBound(object sender, ListViewItemEventArgs e)
    {

        if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
        {
            Candidate candidatInfo = ((ListViewDataItem)e.Item).DataItem as Candidate;

            if (candidatInfo != null)
            {
                HyperLink lnkCandidateName = (HyperLink)e.Item.FindControl("lnkCandidateName");
                Label lblDateCreated = (Label)e.Item.FindControl("lblDateCreated");
                Label lblEmail = (Label)e.Item.FindControl("lblEmail");
                Label lblID = (Label)e.Item.FindControl("lblID");
                Label lblContactNumber = (Label)e.Item.FindControl("lblContactNumber");
                Label lblAvailability = (Label)e.Item.FindControl("lblAvailability");
                Label lblNP = (Label)e.Item.FindControl("lblNP");
                string strFullName = MiscUtil.GetFirstAndLastName(candidatInfo.FirstName, candidatInfo.LastName);
                if (strFullName.Trim() == "")
                    strFullName = "No Candidate Name";
             
                    ControlHelper.SetHyperLink(lnkCandidateName, UrlConstants.Vendor.OVERVIEW, string.Empty, strFullName, UrlConstants.PARAM_MEMBER_ID, StringHelper.Convert(candidatInfo.Id), UrlConstants.PARAM_SITEMAP_ID, UrlConstants.Candidate.VENDOR_CANDIDATE_OVERVIEW_SITEMAP_ID, UrlConstants.PARAM_SITEMAP_PARENT_ID, "653");
                
                lblDateCreated.Text = candidatInfo.CreateDate .ToShortDateString ();
                lblEmail.Text = candidatInfo.PrimaryEmail;
                //*************Added by pravin khot on 15/March/2017***********
                CandidateOverviewDetails overviewdetail = Facade.GetCandidateOverviewDetails(candidatInfo.Id);
                lblID.Text = candidatInfo.Id.ToString();
                lblContactNumber.Text = overviewdetail.Mobile;
                lblAvailability.Text = overviewdetail.Availability;
                lblNP.Text = overviewdetail.NoticePeriod;
                //*****************END********************************
            }
        }
    }
    protected void lsvCandidateList_ItemCommand(object sender, ListViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Sort")
            {
                LinkButton lnkbutton = (LinkButton)e.CommandSource;
                if (txtSortColumn.Text == lnkbutton.ID)
                {
                    if (txtSortOrder.Text == "ASC") txtSortOrder.Text = "DESC";
                    else  txtSortOrder.Text = "ASC";
                }
                else
                {
                    txtSortColumn.Text = lnkbutton.ID;
                    txtSortOrder.Text = "ASC";
                }
                

                if (SortColumn.Text == string.Empty || SortColumn.Text != e.CommandArgument.ToString())
                    SortOrder.Text = "asc";
                else
                    SortOrder.Text = SortOrder.Text == "asc" ? "desc" : "asc";
                SortColumn.Text = e.CommandArgument.ToString();
                odsCandidateList.SelectParameters["SortOrder"].DefaultValue = SortOrder.Text.ToString();
            }
        }
        catch
        {
        }
    }
    #endregion

  

    #endregion
    protected void btnExportToPDF_Click(object sender, EventArgs e)
    {
        repor("pdf");
    }
    protected void btnExportToExcel_Click(object sender, EventArgs e)
    {
        repor("excel");
    }
    protected void btnExportToWord_Click(object sender, EventArgs e)
    {
        repor("word");
    }
    public void repor(string rep)
    {
        GenerateVendorMySubmissionsReport(rep);
    }
    private void GenerateVendorMySubmissionsReport(string format)
    {
        if (string.Equals(format, "word"))
        {
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment;filename=VendorMyCandidatesReport-" + DateTime.Now.ToString("yyyy-MM-dd") + ".doc");
            Response.ContentEncoding = System.Text.Encoding.ASCII;
            Response.ContentType = "application/msword";
            Response.Output.Write(GetVendorMySubmissionsReportTable());
            Response.Flush();
            Response.End();
        }
        else if (string.Equals(format, "excel"))
        {
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment;filename=VendorMyCandidatesReport-" + DateTime.Now.ToString("yyyy-MM-dd") + ".xls");
            Response.ContentEncoding = System.Text.Encoding.ASCII;
            Response.ContentType = "application/msexcel";
            Response.Output.Write(GetVendorMySubmissionsReportTable());
            Response.Flush();
            Response.End();
        }
        else if (string.Equals(format, "pdf"))
        {
            PdfConverter pdfConverter = new PdfConverter();
            UnicodeEncoding unicodeEncoding = new UnicodeEncoding();
            pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A3;
            pdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Normal;
            pdfConverter.PdfDocumentOptions.ShowHeader = false;
            pdfConverter.PdfDocumentOptions.ShowFooter = false;
            pdfConverter.PdfDocumentOptions.LeftMargin = 15;
            pdfConverter.PdfDocumentOptions.RightMargin = 5;
            pdfConverter.PdfDocumentOptions.TopMargin = 15;
            pdfConverter.PdfDocumentOptions.BottomMargin = 5;
            pdfConverter.PdfDocumentOptions.GenerateSelectablePdf = true;
            pdfConverter.PdfDocumentOptions.ShowHeader = false;
            pdfConverter.PdfHeaderOptions.DrawHeaderLine = false;
            pdfConverter.PdfFooterOptions.FooterTextColor = Color.Black;
            pdfConverter.PdfFooterOptions.DrawFooterLine = true;
            pdfConverter.PdfFooterOptions.PageNumberText = "Page";
            pdfConverter.PdfFooterOptions.ShowPageNumber = true;

            pdfConverter.LicenseKey = ContextConstants.EPHTMLTOPDFKEY;
            byte[] downloadBytes = pdfConverter.GetPdfBytesFromHtmlStringWithTempFile(GetVendorMySubmissionsReportTable(), "");
            System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
            response.Clear();
            response.AddHeader("Content-Type", "binary/octet-stream");
            response.AddHeader("Content-Disposition", "attachment; filename=VendorMyCandidatesReport-" + DateTime.Now.ToString("yyyy-MM-dd") + ".pdf; size=" + downloadBytes.Length.ToString());
            response.Flush();
            response.BinaryWrite(downloadBytes);
            response.Flush();
            response.End();
        }
    }
    private string GetVendorMySubmissionsReportTable()
    {
        StringBuilder sourcingReport = new StringBuilder();
        CandidateDataSource CandidateDataSource = new CandidateDataSource();

        IList<Candidate> Candidate = CandidateDataSource.GetPaged(CurrentMember.Id, DateTime.MinValue, DateTime.MinValue, "asc", false,true ,  "", 0, 100);

        if (Candidate != null)
        {
            sourcingReport.Append("<table style='font-family : calibriz;width:100%; border-collapse:collapse; border-spacing: 1px;' align='left' border = '1' bordercolor='#000000'  cellspacing='0' cellpadding='0'>");

            sourcingReport.Append(" <tr>");
            sourcingReport.Append("     <th>Date Create</th>");
            sourcingReport.Append("     <th>ID</th>");
            sourcingReport.Append("     <th>Name</th>");
            sourcingReport.Append("     <th>EMail</th>");
            sourcingReport.Append("     <th>Contact Number</th>");
            sourcingReport.Append("     <th>Availability</th>");
            sourcingReport.Append("     <th>Notice Period</th>");
            sourcingReport.Append(" </tr>");

            foreach (Candidate info in Candidate)
            {
                if (info != null)
                {
                    string strFullName = MiscUtil.GetFirstAndLastName(info.FirstName, info.LastName);
                    if (strFullName.Trim() == "")
                        strFullName = "No Candidate Name";

                    sourcingReport.Append(" <tr>");
                    sourcingReport.Append("     <td>" + info.CreateDate.ToShortDateString() + "&nbsp;</td>");
                    sourcingReport.Append("     <td>" + info.Id.ToString() + "&nbsp;</td>");
                    sourcingReport.Append("     <td>" + strFullName.ToString() + "&nbsp;</td>");
                    CandidateOverviewDetails overviewdetail = Facade.GetCandidateOverviewDetails(info.Id);
                    sourcingReport.Append("     <td>" + info.PrimaryEmail.ToString() + "&nbsp;</td>");
                    sourcingReport.Append("     <td>" + overviewdetail.Mobile.ToString() + "&nbsp;</td>");
                    sourcingReport.Append("     <td>" + overviewdetail.Availability.ToString() + "&nbsp;</td>");
                    sourcingReport.Append("     <td>" + overviewdetail.NoticePeriod.ToString() + "&nbsp;</td>");
                }
            }
        }
        sourcingReport.Append("    <tr>");
        sourcingReport.Append(" </table>");
        return sourcingReport.ToString();
    }   
    #region Methods
    private void PlaceUpDownArrow()
    {
        try
        {
            LinkButton lnk = (LinkButton)lsvCandidateList.FindControl(txtSortColumn.Text);
            System.Web.UI.HtmlControls.HtmlTableCell im = (System.Web.UI.HtmlControls.HtmlTableCell)lnk.Parent;
            im.EnableViewState = false;
            im.Attributes.Add("class", (txtSortOrder.Text == "ASC" ? "Ascending" : "Descending"));
        }
        catch
        {
        }

    }
    public void GetWidgetData()
    {
        if (SortOrder.Text == string.Empty && SortColumn.Text == string.Empty)
        {
            SortOrder.Text = "asc";
            SortColumn.Text = "[C].[FirstName]";
        }
        odsCandidateList.SelectParameters["SortOrder"].DefaultValue = SortOrder.Text.ToString();
        odsCandidateList.SelectParameters["memberId"].DefaultValue = base.CurrentMember.Id.ToString();
        odsCandidateList.SelectParameters["IsVendorContact"].DefaultValue = IsUserVendor.ToString();
        odsCandidateList.SelectParameters["IsVendor"].DefaultValue = "false";
        this.lsvCandidateList.DataBind();
    }

    #endregion

    #region Implementation

    void IWidget.Init(IWidgetHost host)
    {
        this.Host = host;
    }

    void IWidget.ShowSettings()
    {
        pnlSettings.Visible = true;
    }
    void IWidget.HideSettings()
    {
        pnlSettings.Visible = false;
    }
    void IWidget.Minimized()
    {
    }
    void IWidget.Maximized()
    {
        if (lsvCandidateList != null)
        {
            if (lsvCandidateList.Items.Count == 0)
            {
                lsvCandidateList.DataSource = null;
                lsvCandidateList.DataBind();
            }
            else GetWidgetData();
        }
    }
    void IWidget.Closed()
    {
    }

    #endregion
   
}
