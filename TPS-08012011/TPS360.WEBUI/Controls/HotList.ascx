﻿<%------------------------------------------------------------------------------------------------------------------------------------
    FileName: Controls/HotList.ascx
    Description: This is the user control page used for HotList.
    Created By: 
    Created On:
    Modification Log:
    -----------------------------------------------------------------------------------------------------------------------------------
    Ver.No.        Date          Author           Modification
    -----------------------------------------------------------------------------------------------------------------------------------
    0.1            Jan-28-2009   Shivanand        Defect Id 8879; CommandArgument is written for sirtable columns in listview "lsvHotList".
    0.2            Apr-21-2010   Sudarshan.R.     Defect Id 12665; Merged the two columns Edit and Delete into one column Action
    
---------------------------------------------------------------------------------------------------------------------------------------

--%>
<%@ Control Language="C#" AutoEventWireup="true" CodeFile="HotList.ascx.cs" Inherits="TPS360.Web.UI.ctlHotList" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Controls/PagerControl.ascx" TagName="Pager" TagPrefix="ucl" %>

<asp:UpdatePanel ID="pnlHotList" runat="server">
    <ContentTemplate>
        <asp:TextBox ID="SortColumn" runat="server" EnableViewState="true" Visible="false" />
        <asp:TextBox ID="SortOrder" runat="server" EnableViewState="true" Visible="false" />
        <asp:HiddenField ID="hdnSortColumn" runat="server" EnableViewState="true" />
        <asp:HiddenField ID="hdnSortOrder" runat="server" EnableViewState="true" />
        <div class="TableRow" style="text-align: center;">
            <asp:Label ID="lblMessage" runat="server"></asp:Label>
        </div>
        <div class="TabPanelHeader" >
       Add New Hot List
    </div>
        <asp:Panel ID="pnldefault" runat="server" DefaultButton="btnSave">
            <div class="TableRow">
                <div class="TableFormLeble">
                    <asp:Label ID="lblGroupName" runat="server" Text="Hot List Name"></asp:Label>:
                </div>
                <div class="TableFormContent">
                    <asp:TextBox ID="txtGroupName" runat="server" CssClass="CommonTextBox" ValidationGroup="HotListEditor"
                        Width="350"></asp:TextBox>
                    <span class="RequiredField">*</span>
                </div>
            </div>
            <div class="TableRow">
                <div class="TableFormValidatorContent" style="margin-left: 42%">
                    <asp:RequiredFieldValidator ID="rfvGroupName" runat="server" ControlToValidate="txtGroupName"
                        ErrorMessage="Please enter hot list name" ValidationGroup="HotListEditor" EnableViewState="False"
                        Display="Dynamic"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="TableRow">
                <div class="TableFormLeble" style="text-align: right;">
                    <asp:Label ID="lblGroupDesc" runat="server" Text="Description"></asp:Label>:
                </div>
                <div class="TableFormContent">
                    <asp:TextBox ID="txtGroupDesc" runat="server" CssClass="" ValidationGroup="HotListEditor"
                        TextMode="MultiLine" Height="120" Width="350" Style="resize: None;"></asp:TextBox>
                </div>
            </div>
            <div class="TableRow" style="text-align: center;">
                <asp:Button ID="btnSave" CssClass="CommonButton" runat="server" Text="Save" OnClick="btnSaveHotList_Click"
                    ValidationGroup="HotListEditor" Style="height: 25px" />
            </div>
        </asp:Panel>
      <div class="TabPanelHeader" >
        List of Hot Lists
    </div>
        <div class="GridContainer" style="text-align: left; margin-bottom: 38px;">
            <asp:ObjectDataSource ID="odsHotList" runat="server" SelectMethod="GetPagedByMemberIdandGroupType"
                TypeName="TPS360.Web.UI.HotlistDataSource" EnablePaging="true" SelectCountMethod="GetListCount"
                SortParameterName="sortExpression" OnLoad="odsHotList_Load">
                <SelectParameters>
                    <asp:Parameter Name="memberId" DefaultValue="0" />
                    <asp:Parameter Name="groupType" DefaultValue="0" />
                    <asp:Parameter Name="SortOrder" DefaultValue="" />
                </SelectParameters>
            </asp:ObjectDataSource>
            <asp:ListView ID="lsvHotList" runat="server" DataKeyNames="Id" DataSourceID="odsHotList"
                OnItemDataBound="lsvHotList_ItemDataBound" OnItemCommand="lsvHotList_ItemCommand"
                OnPreRender="lsvHotList_PreRender">
                <LayoutTemplate>
                    <table id="tlbTemplate" runat="server" class="Grid" cellspacing="0" border="0">
                        <tr>
                            <th >
                                <asp:LinkButton ID="btnGroupName" runat="server" Text="Name" CommandName="Sort" ToolTip="Sort By Name" CommandArgument="[M].[Name]" />
                            </th>
                            <th>
                                Description
                            </th>
                            <th  style =" width : 100px !important">
                                <asp:LinkButton ID="btnNoOfCandidates" runat="server" Text="# of Candidates" CommandName="Sort" ToolTip="Sort By # of Candidates"
                                    CommandArgument="CandidateCount"  />
                            </th>
                            <th  >
                                <asp:LinkButton ID="btnOwner" runat="server" Text="Manager" CommandName="Sort" ToolTip="Sort By Manager" CommandArgument="[ME].[FirstName]+[ME].[MiddleName]+[ME].[LastName]" />
                            </th>
                            <th  style =" width : 45px !important">
                                Action
                            </th>
                        </tr>
                        <tr id="itemPlaceholder" runat="server">
                        </tr>
                        <tr class="Pager">
                            <td colspan="5">
                                <ucl:Pager ID="pagerControl" runat="server" EnableViewState="true" />
                            </td>
                        </tr>
                    </table>
                </LayoutTemplate>
                <EmptyDataTemplate>
                    <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                        <tr>
                            <td>
                                No data was returned.
                            </td>
                        </tr>
                    </table>
                </EmptyDataTemplate>
                <ItemTemplate>
                    <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                        <td>
                            <asp:Label ID="lblGroupName" runat="server" />
                        </td>
                        <td>
                            <asp:Label ID="lblDescription" runat="server" />
                        </td>
                        <td style =" text-align :center " >
                            <asp:Label ID="lblNoofCandidate" runat="server"  />
                        </td>
                        <td>
                            <asp:Label ID="lblOwner" runat="server" />
                        </td>
                        <td style="text-align: center; white-space: nowrap;">
                            <asp:ImageButton ID="btnEdit" SkinID="sknEditButton" CausesValidation="false" runat="server"
                                CommandName="EditItem" OnClientClick ="ScrollTop()"></asp:ImageButton>
                            <asp:ImageButton ID="btnDelete" Visible="true" SkinID="sknDeleteButton" runat="server"
                                CommandName="DeleteItem"></asp:ImageButton>
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:ListView>
        </div>
      
    </ContentTemplate>
</asp:UpdatePanel>
