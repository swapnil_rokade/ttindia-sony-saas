/*<%-- 
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: VendorApplicants.ascx.cs
    Description: This is the user control page used to display the ooption like ToDo,PDF,Print and many more
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.         Date              Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1            15/March/2017     pravin khot         Added new column -ID,Contact Number,NP
-------------------------------------------------------------------------------------------------------------------------------------------       

--%>*/
using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Web.UI.Helper;
using TPS360.Web.UI;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using TPS360.BusinessFacade;
using System.Collections.Generic;
using TPS360.Common;
using System.IO;
using ExpertPdf.HtmlToPdf;
using System.Drawing;

public partial class VendorActiveJobOpenings : BaseControl
{
    private bool _IsMemberMailAccountAvailable = false;
    private bool isAccess = false;
    private int VendorId
    {
        get
        {
            if (CurrentMember != null)
            {
                CompanyContact com = new CompanyContact();
                com = Facade.GetCompanyContactByMemberId(CurrentMember.Id);
                return com.CompanyId;
            }
            else
            {
                return 0;
            }

        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //uclVendorSubmissions.ContactId = base.CurrentMember.Id;
        hdnVendorId.Value = VendorId.ToString();
        if (SiteSetting[DefaultSiteSetting.CompanyName.ToString()].ToString() != null)
        {
            hdnEmployeeDashboard.Value = SiteSetting[DefaultSiteSetting.CompanyName.ToString()].ToString();
            this.Page.Title = hdnEmployeeDashboard.Value + " - Dashboard";

        }
        string pagesize = "";
        pagesize = (Request.Cookies["DashboardVendorJobPostingRowPerPage"] == null ? "" : Request.Cookies["DashboardVendorJobPostingRowPerPage"].Value); ;
        ASP.controls_smallpagercontrol_ascx PagerControl = (ASP.controls_smallpagercontrol_ascx)this.lsvJobPosting.FindControl("pagerControl");
        if (PagerControl != null)
        {
            DataPager pager = (DataPager)PagerControl.FindControl("pager");
            if (pager != null) pager.PageSize = Convert.ToInt32(pagesize == "" ? "10" : pagesize);
        }

    }
    protected void lsvJobPosting_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
        {
            JobPosting jobPosting = ((ListViewDataItem)e.Item).DataItem as JobPosting;
            if (jobPosting != null)
            {
                Label lblPostedDate = (Label)e.Item.FindControl("lblPostedDate");
                HyperLink lnkJobTitle = (HyperLink)e.Item.FindControl("lnkJobTitle");
                Label lblExperience = (Label)e.Item.FindControl("lblExperience");
                Label lblLocation = (Label)e.Item.FindControl("lblLocation");
                Label lblDueDate = (Label)e.Item.FindControl("lblDueDate");
                Label lblDueinDays = (Label)e.Item.FindControl("lblDueinDays");
               
                lblPostedDate.Text = jobPosting.PostedDate.ToShortDateString();
                lnkJobTitle.Text = jobPosting.JobTitle;
                lnkJobTitle.Attributes.Add("onclick", "javascript:EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/JobDetail.aspx?JID=" + jobPosting.Id + "&FromPage=Vendor','700px','570px')");
                JobPosting js = Facade.GetJobPostingById(jobPosting.Id);
                if (js != null)
                {
                    lblExperience.Text = js.MinExpRequired + " - " + js.MaxExpRequired + " Years";
                    lblLocation.Text = jobPosting.City;
                    lblDueDate.Text = js.FinalHiredDate.ToShortDateString();
                    

                    DateTime d1 = DateTime.Now;
                    DateTime d2 = js.FinalHiredDate;

                    TimeSpan t = d2 - d1;
                    int NrOfDays = Convert.ToInt32(t.TotalDays);
                    lblDueinDays.Text = NrOfDays.ToString();
                }
            }
        }
    }

    protected void lsvJobPosting_ItemCommand(object sender, ListViewCommandEventArgs e)
    {

        try
        {
            if (e.CommandName == "Sort")
            {
                LinkButton lnkbutton = (LinkButton)e.CommandSource;
                if (hdnSortColumn.Value == lnkbutton.ID)
                {
                    if (hdnSortOrder.Value == "ASC") hdnSortOrder.Value = "DESC";
                    else hdnSortOrder.Value = "ASC";
                }
                else
                {
                    hdnSortColumn.Value = lnkbutton.ID;
                    hdnSortOrder.Value = "ASC";
                }
            }
        }
        catch
        {
        }

    }

    protected void lsvJobPosting_PreRender(object sender, EventArgs e)
    {
        ASP.controls_smallpagercontrol_ascx PagerControl = (ASP.controls_smallpagercontrol_ascx)this.lsvJobPosting.FindControl("pagerControl");
        if (PagerControl != null)
        {
            DataPager pager = (DataPager)PagerControl.FindControl("pager");
            if (pager != null)
            {
                if (pager.Controls.Count >= 1)
                {
                    DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                    if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
                }
            }
            HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
            if (hdnRowPerPageName != null) hdnRowPerPageName.Value = "DashboardVendorJobPostingRowPerPage";
        }
        else
        {
            ExportButtons.Visible = false;
        }
        PlaceUpDownArrow();
    }
    protected void odsRequisitionList_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {

    }
    private void PlaceUpDownArrow()
    {
        try
        {
            LinkButton lnk = (LinkButton)lsvJobPosting.FindControl(hdnSortColumn.Value);
            System.Web.UI.HtmlControls.HtmlTableCell im = (System.Web.UI.HtmlControls.HtmlTableCell)lnk.Parent;
            im.EnableViewState = false;
            if (lnk.CommandArgument.Contains("[1]") || lnk.CommandArgument.Contains("[JMC].[ManagersCount]"))
            {
                im.Attributes.Add("class", (hdnSortOrder.Value == "ASC" ? "Descending" : "Ascending"));
            }
            else
                im.Attributes.Add("class", (hdnSortOrder.Value == "ASC" ? "Ascending" : "Descending"));
        }
        catch
        {
        }

    }
    protected void btnExportToPDF_Click(object sender, EventArgs e)
    {
        repor("pdf");
    }
    protected void btnExportToExcel_Click(object sender, EventArgs e)
    {
        repor("excel");
    }
    protected void btnExportToWord_Click(object sender, EventArgs e)
    {
        repor("word");
    }
    public void repor(string rep)
    {
        GenerateVendorActiveJobOpeningsReport(rep);
    }
    private void GenerateVendorActiveJobOpeningsReport(string format)
    {
        if (string.Equals(format, "word"))
        {
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment;filename=VendorActiveJobOpeningsReport-" + DateTime.Now.ToString("yyyy-MM-dd") + ".doc");
            Response.ContentEncoding = System.Text.Encoding.ASCII;
            Response.ContentType = "application/msword";
            Response.Output.Write(GetVendorActiveJobOpeningsReportTable());
            Response.Flush();
            Response.End();
        }
        else if (string.Equals(format, "excel"))
        {
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment;filename=VendorActiveJobOpeningsReport-" + DateTime.Now.ToString("yyyy-MM-dd") + ".xls");
            Response.ContentEncoding = System.Text.Encoding.ASCII;
            Response.ContentType = "application/msexcel";
            Response.Output.Write(GetVendorActiveJobOpeningsReportTable());
            Response.Flush();
            Response.End();
        }
        else if (string.Equals(format, "pdf"))
        {
            PdfConverter pdfConverter = new PdfConverter();
            UnicodeEncoding unicodeEncoding = new UnicodeEncoding();
            pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A3;
            pdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Normal;
            pdfConverter.PdfDocumentOptions.ShowHeader = false;
            pdfConverter.PdfDocumentOptions.ShowFooter = false;
            pdfConverter.PdfDocumentOptions.LeftMargin = 15;
            pdfConverter.PdfDocumentOptions.RightMargin = 5;
            pdfConverter.PdfDocumentOptions.TopMargin = 15;
            pdfConverter.PdfDocumentOptions.BottomMargin = 5;
            pdfConverter.PdfDocumentOptions.GenerateSelectablePdf = true;
            pdfConverter.PdfDocumentOptions.ShowHeader = false;
            pdfConverter.PdfHeaderOptions.DrawHeaderLine = false;
            pdfConverter.PdfFooterOptions.FooterTextColor = Color.Black;
            pdfConverter.PdfFooterOptions.DrawFooterLine = true;
            pdfConverter.PdfFooterOptions.PageNumberText = "Page";
            pdfConverter.PdfFooterOptions.ShowPageNumber = true;

            pdfConverter.LicenseKey = ContextConstants.EPHTMLTOPDFKEY;
            byte[] downloadBytes = pdfConverter.GetPdfBytesFromHtmlStringWithTempFile(GetVendorActiveJobOpeningsReportTable(), "");
            System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
            response.Clear();
            response.AddHeader("Content-Type", "binary/octet-stream");
            response.AddHeader("Content-Disposition", "attachment; filename=VendorActiveJobOpeningsReport-" + DateTime.Now.ToString("yyyy-MM-dd") + ".pdf; size=" + downloadBytes.Length.ToString());
            response.Flush();
            response.BinaryWrite(downloadBytes);
            response.Flush();
            response.End();
        }
    }
    private string GetVendorActiveJobOpeningsReportTable()
    {
        StringBuilder sourcingReport = new StringBuilder();
        JobPostingDataSource JobPostingDataSource = new JobPostingDataSource();

        IList<JobPosting> JobPosting = JobPostingDataSource.GetPagedRequisitionListForActiveJobOpenings(string.Empty, string.Empty, string.Empty, string.Empty, null, Convert.ToString(hdnVendorId.Value), null, -1, -1);

        if (JobPosting != null)
        {
            sourcingReport.Append("<table style='font-family : calibriz;width:100%; border-collapse:collapse; border-spacing: 1px;' align='left' border = '1' bordercolor='#000000'  cellspacing='0' cellpadding='0'>");

            sourcingReport.Append(" <tr>");
            sourcingReport.Append("     <th>Date Posted</th>");
            sourcingReport.Append("     <th>Job Title</th>");
            sourcingReport.Append("     <th>Experience</th>");
            sourcingReport.Append("     <th>Location</th>");
            sourcingReport.Append("     <th>Due Date</th>");
            sourcingReport.Append("     <th>Due in Days</th>");
            sourcingReport.Append(" </tr>");
           
            DateTime d1 = DateTime.Now;
            foreach (JobPosting info in JobPosting)
            {
                 int NrOfDay =0;
                 if (info != null)
                 {
                     int count = Convert.ToInt32(info.NoofSubmissions) - Convert.ToInt32(Convert.ToInt32(info.Rejected) + Convert.ToInt32(info.Joined));
                     sourcingReport.Append(" <tr>");
                     sourcingReport.Append("     <td>" + info.PostedDate.ToShortDateString() + "&nbsp;</td>");
                     sourcingReport.Append("     <td>" + info.JobTitle.ToString() + "&nbsp;</td>");
                     JobPosting js = Facade.GetJobPostingById(info.Id);
                     if (js != null)
                     {
                         DateTime d11 = DateTime.Now;
                         DateTime d21 = js.FinalHiredDate;

                         TimeSpan t1 = d21 - d11;
                         NrOfDay = Convert.ToInt32(t1.TotalDays);

                         sourcingReport.Append("     <td>" + js.MinExpRequired + " - " + js.MaxExpRequired + " Years" + "&nbsp;</td>");
                         sourcingReport.Append("     <td>" + info.City.ToString() + "&nbsp;</td>");
                         sourcingReport.Append("     <td>" + js.FinalHiredDate.ToShortDateString() + "&nbsp;</td>");
                         sourcingReport.Append("     <td>" + NrOfDay.ToString() + "&nbsp;</td>");
                     }
                 }
            }
        }
        sourcingReport.Append("    <tr>");
        sourcingReport.Append(" </table>");
        return sourcingReport.ToString();
    }   
   
}
