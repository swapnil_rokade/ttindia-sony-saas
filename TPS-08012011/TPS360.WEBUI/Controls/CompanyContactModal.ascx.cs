﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using TPS360.Common.BusinessEntities;
using TPS360.Web.UI.Helper;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Web.UI;
using System.Text.RegularExpressions;
using ExpertPdf.HtmlToPdf;
using System.Drawing;
using System.IO;

namespace TPS360.Web.UI
{
    public partial class CompanyContactModal : CompanyBaseControl 
    {
        #region Member Variables

        #endregion

        #region Properties
        private int company_contactId;
        public int CompanyContactId
        {
            get
            {
                if (Helper.Url.SecureUrl[UrlConstants.PARAM_CONTACT_ID] != null)
                {
                    return Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_CONTACT_ID].ToString ());
                }
                return 0;
            }
        }
        #endregion

        #region Methods
      
        #endregion

        #region PageEvents
        protected void Page_Load(object sender, EventArgs e)
        {
            lblZipCode.Text = "Zip/Postal Code";
            if (!IsPostBack)
            {
                if (CompanyContactId > 0) PopulateData();
            }
            
        }
        public string getModalTitle()
        {
            return hdnTitle.Value;
        }
        private void PopulateData()
        {
            CompanyContact contact = Facade.GetCompanyContactById(CompanyContactId);
            hdnTitle.Value = "Contact Details - " + contact.FirstName + " " + contact.LastName;
            Label lbModalTitle = (Label)this.Page.Master.FindControl("lbModalTitle");
            if (lbModalTitle != null) lbModalTitle.Text = hdnTitle.Value;
            lblEmail.Text = contact.Email;
            //lblNoBulkEmail.Text = contact.NoBulkEmail ? "Yes" : "No";
            lblFirstName.Text = contact.FirstName;
            lblLastName.Text = contact.LastName;
            lblTitlevalue.Text = contact.Title;
            lblAddress1value.Text = contact.Address1;
            lblAddress2Value.Text = contact.Address2;
            lblCityValue.Text = contact.City;
            if(contact .StateId >0)
            {
                State st=Facade .GetStateById (contact .StateId );
                if(st!=null) lblStateValue.Text = st.Name ;
            }
            if(contact .CountryId >0)
            { Country coun=Facade .GetCountryById (contact .CountryId );
                if(coun!=null )        lblCountryValue.Text =coun .Name ;
            }
            lblZipValue.Text = contact.ZipCode;

            lblOfficePhoneValue.Text = contact.OfficePhone;
            if(contact .OfficePhone !=string .Empty && contact .OfficePhoneExtension != string .Empty )
                    lblOfficePhoneValue.Text  +="x";
            lblOfficePhoneValue.Text  += contact.OfficePhoneExtension;
            lblMobilePhoneValue.Text = contact.MobilePhone;
            lblFaxValue.Text = contact.Fax;
            lblIsPrimary.Text = contact.IsPrimaryContact ? "Yes" : "No";
           // lblIsOwner.Text = contact.IsOwner ? "Yes" : "No";
            lblContactRemarks.Text = contact.ContactRemarks;
        }
      
        protected void Page_PreRender(object sender, EventArgs e)
        {
           
        }
        #endregion

     
        #region Button Events
      
        #endregion
        

       
}
}