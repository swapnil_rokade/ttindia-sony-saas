﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using TPS360.Web.UI;
using TPS360.Common.Shared;
using TPS360.Common.BusinessEntities;
using TPS360.Web.UI.Helper;
using TPS360.Common.Helper;
using System.Collections.Generic;
// Vishal Tripathy Start Date 17th August 2016.
//Description: Modal for when recruiter submmites the offer then it goes to recruitermanager.Or when Hr Head rejects the data.


public partial class Controls_OfferApproval : BaseControl
{
    #region Member Variables
    bool ShowAction = false;
    public int HeaderAdd = 0;
    ArrayList _permittedMenuIdList;
    public string RequisitionType = "Master";
    public static bool IsAccss = false;
    #endregion
    #region Page Load Events
    protected void Page_Load(object sender, EventArgs e)
    {
        string pagesize = "";
        if (hdnDoPostPack.Value == "1")
        {
            lsvOfferList.DataBind() ;
            hdnDoPostPack.Value = "0";
            return;
        }
        if (!IsPostBack)
        {

           
              
            //string message = Helper.Url.SecureUrl[UrlConstants.PARAM_MSG];
            //if (!StringHelper.IsBlank(message))
            //{
            //    MiscUtil.ShowMessage(lblMessage, message, false);
            //}
            //hdnSortColumn.Value = "btnPostedDate";
            //hdnSortOrder.Value = "DESC";
            //PlaceUpDownArrow();
        }
        
        
        SetDataSourceParameters();
    
        ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvOfferList.FindControl("pagerControl");
        if (PagerControl != null)
        {
            DataPager pager = (DataPager)PagerControl.FindControl("pager");
            if (pager != null)
            {
                pager.PageSize = Convert.ToInt32(pagesize == "" ? "20" : pagesize);
            }
        }

        txtSortColumn.Text = "ApplicantName";
        txtSortOrder.Text = "DESC";
      
        PlaceUpDownArrow();

    }
    #endregion
    #region Methods
    private void BindList()
    {
        this.lsvOfferList.DataBind();

    }
    private void PlaceUpDownArrow()
    {
        try
        {
            LinkButton lnk = (LinkButton)lsvOfferList.FindControl(txtSortColumn.Text);
            HtmlTableCell im = (HtmlTableCell)lnk.Parent;
            im.EnableViewState = false;
            im.Attributes.Add("class", (txtSortOrder.Text == "ASC" ? "Ascending" : "Descending"));

        }
        catch
        {
        }

    }

    private void SetDataSourceParameters()
    {
        if (SortOrder.Text == string.Empty && SortColumn.Text == string.Empty)
        {
            SortOrder.Text = "asc";
            SortColumn.Text = "c.candidatename";
        }
        odsOfferList.SelectParameters["MemberId"].DefaultValue = Convert.ToString(CurrentMember.Id);
        odsOfferList.SelectParameters["SortOrder"].DefaultValue = SortOrder.Text.ToString();
        odsOfferList.SelectParameters["SortColumn"].DefaultValue = SortColumn.Text.ToString();
        odsOfferList.SelectParameters["sortExpression"].DefaultValue = SortOrder.Text.ToString();
        odsOfferList.SelectParameters["maximumRows"].DefaultValue = "200";
        odsOfferList.SelectParameters["startRowIndex"].DefaultValue = "0";
    }

    #endregion
    #region Events
    protected void lsvOfferList_ItemDataBound(object sender, ListViewItemEventArgs e)
    {

        
        if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
        {
            //JobPosting jobPosting = ((ListViewDataItem)e.Item).DataItem as JobPosting;
            MemberHiringDetails memberHiringDetails=((ListViewDataItem)e.Item).DataItem as MemberHiringDetails;
            int ColLocation = 0;
            string ststus = Convert.ToString(odsOfferList.SelectParameters["status"].DefaultValue);
            if (memberHiringDetails != null)
            {                
                Label lblcandidatename = (Label)e.Item.FindControl("lblCandidateName");
                Label lblJobTitle = (Label)e.Item.FindControl("lblJobTitle");
                Label lblGrade = (Label)e.Item.FindControl("lblGrade");
                Label lblCreatedOn = (Label)e.Item.FindControl("lblCreatedOn");
                Label lblOfferCreated = (Label)e.Item.FindControl("lblOfferCreated");
                Label lblStage = (Label)e.Item.FindControl("lblStage");
                HyperLink lnkReqApprove = (HyperLink)e.Item.FindControl("hlkReqApprove");
                HyperLink lnkReqReject = (HyperLink)e.Item.FindControl("hlkReqReject");
                HyperLink lnkJobTitle = (HyperLink)e.Item.FindControl("lnkJobTitle");
                TableCell tcCell = (TableCell)e.Item.FindControl("thAction");

                Label lblTemplate = (Label)e.Item.FindControl("lblTemplate");
                Label lblAction = (Label)e.Item.FindControl("lblAction");
                //SecureUrl urlEditJob = UrlHelper.BuildSecureUrl("../" + (UrlConstants.Requisition.MEMBER_OFFER_VIEW_PAGE.Replace("~/", "").Replace("//", "/")), string.Empty, memberHiringDetails.Id.ToString(), StringHelper.Convert(memberHiringDetails.Id), UrlConstants.PARAM_SITEMAP_PARENT_ID, "13", UrlConstants.PARAM_JOB_DISABLE, "true");


                lblcandidatename.Text = Convert.ToString(memberHiringDetails.ApplicantName);
                lblJobTitle.Text = Convert.ToString(memberHiringDetails.JobTitle);
                if (memberHiringDetails.Grade == "Please Select")
                {
                    lblGrade.Text = "";
                }
                else
                {
                    lblGrade.Text = Convert.ToString(memberHiringDetails.Grade);
                }
          
                lblCreatedOn.Text = Convert.ToString(memberHiringDetails.OfferedDate);
                lblOfferCreated.Text=Convert.ToString(memberHiringDetails.OfferedSalary);

                if (memberHiringDetails.ApprovalType == 3)
                {
                    lblStage.Text = "Offer Rejected By Hr Head";
                }
                else
                {
                    lblStage.Text = "Offer Submiited";
                }
                //lnkReqApprove.Attributes.Add("onclick", "EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/OfferApproval1.aspx?MID=" + memberHiringDetails.Id + "&STATUS=Offer Approved by Recruiter Manager&ID=" + CurrentMember.Id + "','700px','350px'); return false;");
                //lnkReqReject.Attributes.Add("onclick", "var input=confirm('Are you sure want to send back the offer?'); if (input==true) { EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/OfferApproval1.aspx?MID=" + memberHiringDetails.Id + "&STATUS=Offer Rejected by Recruiter Manager&ID=" + CurrentMember.Id + "','700px','350px');} return false;");
                lnkReqApprove.Attributes.Add("onclick", "EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/OfferApproval1.aspx?MID=" + memberHiringDetails.Id + "&STATUS=1&ID=" + CurrentMember.Id + "','700px','350px'); return false;");
                lnkReqReject.Attributes.Add("onclick", "var input=confirm('Are you sure want to send back the offer?'); if (input==true) { EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/OfferApproval1.aspx?MID=" + memberHiringDetails.Id + "&STATUS=0&ID=" + CurrentMember.Id + "','700px','350px');} return false;");
                //lnkJobTitle.Attributes.Add("onclick", "window.open('" + urlEditJob.ToString() + "','View');");
                int Id = CurrentMember.Id;
                //lnkJobTitle.Attributes.Add("onclick", "EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/ViewOffer.aspx?MID=" + CurrentMember.Id + "&ID=" + memberHiringDetails.Id + "','700px','350px'); return false;");

                lnkJobTitle.Attributes.Add("onclick", "EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/ViewOffer.aspx?MID=" + CurrentMember.Id + "&ID=" + memberHiringDetails.Id + "','820px','600px'); return false;");
                lnkJobTitle.Text = "View";

                //lblDemandType.Text = jobPosting.DemandTypeText.ToString();
                //lblPriority.Text = jobPosting.PriorityText.ToString();
                //lblOrganizationUnit.Text = jobPosting.OrgnizationName.ToString();
                //lblPostedBy.Text = jobPosting.PostedBy.ToString();
                //lblUpdatedOn.Text = jobPosting.UpdateDate.ToShortDateString();
                //lnkReqApprove.Attributes.Add("onclick", "EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/RequsitionApproval.aspx?JID=" + jobPosting.Id + "&STATUS=Tower Head Approved&ID=" + CurrentMember.Id + "','700px','350px'); return false;");
                //lnkReqReject.Attributes.Add("onclick", "var input=confirm('Are you sure want to send back the requisition?'); if (input==true) { EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/RequsitionApproval.aspx?JID=" + jobPosting.Id + "&STATUS=Tower Head Rejected&ID=" + CurrentMember.Id + "','700px','350px');} return false;");
                //lblJobCode.Text = jobPosting.JobPostingCode.ToString();
                //lblPostedDate.Text = jobPosting.PostedDate.ToShortDateString();
                //SecureUrl urlEditJob = UrlHelper.BuildSecureUrl("../" + (UrlConstants.Requisition.JOB_POSTING_EDITOR_PAGE.Replace("~/", "").Replace("//", "/")), string.Empty, UrlConstants.PARAM_JOB_ID, StringHelper.Convert(jobPosting.Id), UrlConstants.PARAM_SITEMAP_PARENT_ID, "13", UrlConstants.PARAM_JOB_DISABLE, "true");
                //lnkJobTitle.Text = jobPosting.JobTitle;
                //lnkJobTitle.Attributes.Add("onclick", "window.open('" + urlEditJob.ToString() + "','View');");
                //Label lblApprovalStatus = (Label)e.Item.FindControl("lblApprovalStatus");
                //lblApprovalStatus.Text = jobPosting.ApprovalStatus;
                //Member MemberCreated = null;
                //if (jobPosting.CreatorId != 0) MemberCreated = Facade.GetMemberById(jobPosting.CreatorId);                  //0.5                
                //lblNoOfOpenings.Text = jobPosting.NoOfOpenings.ToString();
            }
        }

    }
   

    protected void lsvOfferList_ItemCommand(object sender, ListViewCommandEventArgs e)
    {

        try
        {
            if (e.CommandName == "Sort")
            {
                LinkButton lnkbutton = (LinkButton)e.CommandSource;
                if (txtSortColumn.Text == lnkbutton.ID)
                {
                    if (txtSortOrder.Text == "ASC") txtSortOrder.Text = "DESC";
                    else txtSortOrder.Text = "ASC";
                }
                else
                {
                    txtSortColumn.Text = lnkbutton.ID;
                    txtSortOrder.Text = "ASC";
                }
                if (SortColumn.Text == string.Empty || SortColumn.Text != e.CommandArgument.ToString())
                    SortOrder.Text = "asc";
                else SortOrder.Text = SortOrder.Text == "asc" ? "desc" : "asc";
                SortColumn.Text = e.CommandArgument.ToString();
                odsOfferList.SelectParameters["SortColumn"].DefaultValue = SortColumn.Text.ToString();
                odsOfferList.SelectParameters["SortOrder"].DefaultValue = SortOrder.Text.ToString();

            }
        }
        catch
        {
        }

    }
    protected void lsvOfferList_PreRender(object sender, EventArgs e)
    {

        ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvOfferList.FindControl("pagerControl");
        if (PagerControl != null)
        {
            DataPager pager = (DataPager)PagerControl.FindControl("pager");
            if (pager != null)
            {
                DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
            }
            HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
            if (hdnRowPerPageName != null) hdnRowPerPageName.Value = "OfferApprovalRowPerPage";
        }

        PlaceUpDownArrow();

        if (IsPostBack)
        {
            if (lsvOfferList.Items.Count == 0)
            {
                lsvOfferList.DataSource = null;
                lsvOfferList.DataBind();
            }
        }



    }

    #endregion

   
    protected void lsvOfferList_LayoutCreated(object sender, EventArgs e)
    {
        
                

    }
}




