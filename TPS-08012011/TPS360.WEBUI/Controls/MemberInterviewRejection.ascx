﻿<%@ Register Assembly="Infragistics35.Web.v12.2, Version=12.2.20122.1007, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.Web.UI.EditorControls" TagPrefix="ig" %>
<%@ Register Assembly="Infragistics35.Web.v12.2, Version=12.2.20122.1007, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.Web.UI.LayoutControls" TagPrefix="ig" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MemberInterviewRejection.ascx.cs"
    Inherits="TPS360.Web.UI.ControlMemberInterviewRejection" %>
<%--<%@ Register Assembly="Infragistics2.WebUI.WebDateChooser.v7.3, Version=7.3.20073.38, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.WebUI.WebSchedule" TagPrefix="igsch" %>--%>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Controls/PagerControl.ascx" TagName="Pager" TagPrefix="ucl" %>
<%@ Register Src="~/Controls/ConfirmationWindow.ascx" TagName="Confirm" TagPrefix="ucl" %>
<%@ Register Src="~/Controls/HtmlEditor.ascx" TagName="HtmlEditor" TagPrefix="ucl" %>

<script type="text/javascript" language="javascript" src="../js/AjaxVariables.js"></script>

<script type="text/javascript" language="javascript" src="../js/AjaxScript.js"></script>

<script src="../Scripts/fixWebkit.js" type="text/javascript"></script>

<link href="../assets/css/chosen.css" rel="Stylesheet" />
<style>
    .TableFormLeble
    {
        width: 33%;
    }
    .EmailDiv
    {
        vertical-align: middle;
        text-align: center;
        border: solid 1px #CCC;
        padding-bottom: 2px;
        padding-top: 2px;
        margin-right: 4px;
        margin-bottom: 4px;
        width: auto;
        float: left;
        background-image: -moz-linear-gradient(center top , #FFFFFF 20%, #F6F6F6 50%, #EEEEEE 52%, #F4F4F4 100%);
        border-radius: 3px;
    }
</style>


<div>
    <asp:TextBox ID="txtSortColumn" runat="server" Visible="false"></asp:TextBox>
    <asp:TextBox ID="txtSortOrder" runat="server" Visible="false"></asp:TextBox>
    <asp:UpdatePanel ID="upPanel" runat="server">
        <ContentTemplate>
            <div>
                <ajaxToolkit:ModalPopupExtender ID="InterviewModalPopupExtender" runat="server" BackgroundCssClass="modalBackground"
                    DropShadow="false" PopupControlID="lgnPanelInterviewer" TargetControlID="lblInterviewer"
                    BehaviorID="SIIE">
                </ajaxToolkit:ModalPopupExtender>
                <ajaxToolkit:ModalPopupExtender ID="CandidateModalPopupExtender" runat="server" BackgroundCssClass="modalBackground"
                    DropShadow="false" PopupControlID="lgnPanelCandidate" TargetControlID="lblCandidate"
                    BehaviorID="SICE">
                </ajaxToolkit:ModalPopupExtender>
                <asp:Label ID="lblInterviewer" runat="server"></asp:Label>
                <asp:Label ID="lblCandidate" runat="server"></asp:Label>
                <asp:Panel ID="lgnPanelInterviewer" runat="server" CssClass="modalPopup" Style="display: none;"
                    Width="700px" Height="400px">
                    <ucl:HtmlEditor ID="txtEmailInterviewerTemplate" runat="server" />
                    <br />
                    <asp:HiddenField ID="hdnInterviewTemp" runat="server" />
                    <div align="center">
                        <%--<asp:Button ID="Button1" runat="server" Text="Save" CssClass="CommonButton" Width="44px" />
                    --%><asp:Button ID="btnClose" runat="server" Text="Close" CssClass="CommonButton" />
                    </div>
                </asp:Panel>
            </div>
            <div>
                <asp:Panel ID="lgnPanelCandidate" runat="server" CssClass="modalPopup" Style="display: none;"
                    Width="700px" Height="400px">
                    <ucl:HtmlEditor ID="txtEmailCandidateTemplate" runat="server" />
                    <br />
                    <asp:HiddenField ID="hdnCandidateTemp" runat="server" />
                    <div align="center">
                        <%--<asp:Button ID="Button2" runat="server" Text="Save" CssClass="CommonButton" Width="44px" />
                    --%><asp:Button ID="Button3" runat="server" Text="Close" CssClass="CommonButton" />
                    </div>
                </asp:Panel>
            </div>
            <ucl:Confirm ID="uclConfirm" runat="server"></ucl:Confirm>
            <asp:HiddenField ID="hdnSelectedClientText" runat="server" />
            <asp:HiddenField ID="hdnSelectedClientValue" runat="server" />
            <asp:HiddenField ID="hfInterviewId" runat="server" />
            <asp:HiddenField ID="hfSourceId" runat="server" />
            <asp:HiddenField ID="hdnTmpFolder" runat="server" />
            
            <asp:HiddenField ID="hdnClear" runat="server" />
            <div class="TableRow" style="text-align: center">
                <asp:Label EnableViewState="false" ID="lblMessage" runat="server"></asp:Label>
            </div>
            <div>
                <asp:ObjectDataSource ID="odsInterviewRejection" runat="server" SelectMethod="GetPaged"
                    TypeName="TPS360.Web.UI.MemberInterviewDataSource" SelectCountMethod="GetListCount"
                    EnablePaging="True" SortParameterName="sortExpression">
                    <SelectParameters>
                        <asp:Parameter Name="CandidateId" DefaultValue="0" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <div class="TableRow" id="dvListView" runat="server">
                    <div class="TabPanelHeader">
                        List of Interviews Rejection
                    </div>
                    <asp:ListView ID="lsvInterviewRejection" runat="server" DataKeyNames="Id" 
                        EnableViewState="true" OnItemDataBound="lsvInterviewRejection_ItemDataBound" OnItemCommand="lsvInterviewRejection_ItemCommand"
                        OnPreRender="lsvInterviewRejection_PreRender">
                        <LayoutTemplate>
                            <table id="tlbTemplate" runat="server" class="Grid" cellspacing="0" border="0">
                                <tr>
                                    <th style="width: 90px !important;">
                                        <asp:LinkButton ID="btnDateTime" runat="server" ToolTip="Sort By Date & Time" CommandName="Sort"
                                            CommandArgument="[I].[StartDateTime]" Text="Date & Time" />
                                    </th>
                                    <th>
                                        <asp:LinkButton ID="btnJobTitle" runat="server" CommandName="Sort" CommandArgument="[JP].[JobTitle]"
                                            Text="Requisition" />
                                    </th>
                                    <th>
                                        <asp:LinkButton ID="btnTitle" runat="server" CommandName="Sort" CommandArgument="[I].[Title]"
                                            Text="Interview Title" />
                                    </th>
                                    <th>
                                        <asp:LinkButton ID="btnRejectby" runat="server" CommandName="Sort" CommandArgument="[I].[Location]"
                                            Text="Rejected By" />
                                    </th>
                                    <th>
                                        <asp:LinkButton ID="btnReason" runat="server" CommandName="Sort" CommandArgument="[GL].[Name]"
                                            Text="Reason" />
                                    </th>
                                </tr>
                                <tr id="itemPlaceholder" runat="server">
                                </tr>
                                <tr class="Pager">
                                    <td id="tdPager" runat="server" colspan="5">
                                        <ucl:Pager ID="pagerControl" runat="server" EnableViewState="true" />
                                    </td>
                                </tr>
                            </table>
                        </LayoutTemplate>
                        <EmptyDataTemplate>
                            <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                                <tr>
                                    <td>
                                        No interview rejection data available.
                                    </td>
                                </tr>
                            </table>
                        </EmptyDataTemplate>
                        <ItemTemplate>
                            <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                                <td>
                                    <asp:Label ID="lblDateTime" runat="server" />
                                </td>
                                <td>
                                    <asp:LinkButton ID="lblJobTitle" runat="server"></asp:LinkButton>
                                </td>
                                <td>
                                    <asp:Label ID="lblTitle" runat="server" />
                                </td>
                                <td>
                                    <asp:Label ID="lblRejectedBy" runat="server" />
                                </td>
                                <td>
                                    <asp:Label ID="lblReason" runat="server" />
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:ListView>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>
