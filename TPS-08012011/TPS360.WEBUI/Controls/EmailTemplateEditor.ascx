﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EmailTemplateEditor.ascx.cs" Inherits="TPS360.Web.UI.EmailTemplateEditor"  %>

     <%@ Register Assembly="Infragistics2.WebUI.WebHtmlEditor.v7.3, Version=7.3.20073.38, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.WebUI.WebHtmlEditor" TagPrefix="ighedit" %>
     <%@ Register Src ="~/Controls/ConfirmationWindow.ascx" TagName ="Confirmm" TagPrefix ="uc2" %>
    <%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
    <%@ Register Src ="~/Controls/HtmlEditor.ascx" TagName ="HtmlEditor"  TagPrefix ="ucl" %>

       <asp:UpdatePanel id="updatepanelbuttoss" runat="server"  >
    <ContentTemplate>
<div id ="divs" runat ="server" visible ="false" >
<uc2:Confirmm ID="uclConfirmm" runat="server" ></uc2:Confirmm></div>
</ContentTemplate>
</asp:UpdatePanel>
<div class="GridContainer" style="width: 100%;" >

<asp:HiddenField ID ="hdnDefaultMailTemplate" runat ="server" Value ="" />
<asp:HiddenField ID ="hdnSubject" runat="server" Value ="" />
<asp:HiddenField ID ="hdnSignature" runat ="server" Value ="" />
<asp:HiddenField ID ="hdnClear" runat ="server" Value ="0" />
    <div align="left" >
        
<div style ="text-indent:25px;" >
        The form below displays the email that will be sent to each candidate found by the agent. The "&lt;candidate_first_name&gt;" text will be replaced with the candidate's first name.</div>
</div>
    
    <div class ="TableRow">
        <div class ="TableFormLeble">From:
        </div>
        <div class ="TableFormContent">
            <%--<asp:DropDownList ID="ddlFrom" runat ="server" CssClass ="CommonDropDownList"></asp:DropDownList>--%>
            <asp:Label ID="lblFrom" runat ="server" ></asp:Label>
        </div>
    </div>
    <div class ="TableRow">
        <div class ="TableFormLeble">Subject:
        </div>
        <div class ="TableFormContent">
            <asp:TextBox ID="txtSubject" runat ="server" CssClass ="CommonTextBox" Width =" 300px"></asp:TextBox>
              <span class="RequiredField">*</span>
        </div>
    </div>
    <div class ="TableRow">
    <div class ="TableFormValidator">
    <asp:RequiredFieldValidator ID ="rfvSubject" runat ="server" ControlToValidate ="txtSubject" Display ="Dynamic" ErrorMessage ="Please enter subject." ValidationGroup ="vgEmailtemplate" ></asp:RequiredFieldValidator>
    </div>
    </div>
    <div class ="TableRow" style="text-align: left; padding-top :15px"><strong>Message:</strong>
    
        <div class="TableFormContent"  >
                     <ucl:HtmlEditor ID ="txtEmailTemplate" runat ="server"  />
                      <%-- <ighedit:WebHtmlEditor ID="txtEmailTemplate" runat="server" Width="775px" Height="170px"  >
                        <Toolbar>
                            <ighedit:ToolbarImage Type="DoubleSeparator" runat="server" /> 
                            <ighedit:ToolbarButton Type="Bold" runat="server" /> 
                            <ighedit:ToolbarButton Type="Italic" runat="server" />
                            <ighedit:ToolbarButton Type="Underline" runat="server" /> 
                            <ighedit:ToolbarButton Type="Strikethrough" runat="server" />
                            <ighedit:ToolbarImage Type="Separator" runat="server" /> 
                            <ighedit:ToolbarButton Type="Subscript" runat="server" /> 
                            <ighedit:ToolbarButton Type="Superscript" runat="server" /> 
                            <ighedit:ToolbarImage Type="Separator" runat="server" /> 
                            <ighedit:ToolbarButton Type="JustifyLeft" runat="server" />
                            <ighedit:ToolbarButton Type="JustifyCenter" runat="server" /> 
                            <ighedit:ToolbarButton Type="JustifyRight" runat="server" /> 
                            <ighedit:ToolbarImage Type="Separator" runat="server" />
                            <ighedit:ToolbarButton Type="Outdent" runat="server" /> 
                            <ighedit:ToolbarButton Type="Indent" runat="server" />
                            <ighedit:ToolbarImage Type="Separator" runat="server" /> 
                            <ighedit:ToolbarButton Type="UnorderedList" runat="server" /> 
                            <ighedit:ToolbarButton Type="OrderedList" runat="server" /> 
                            <ighedit:ToolbarImage Type="Separator" runat="server" /> 
                            <ighedit:ToolbarButton Type="Cut" runat="server" />
                            <ighedit:ToolbarButton Type="Copy" runat="server" />
                            <ighedit:ToolbarButton Type="Paste" runat="server" />
                            <ighedit:ToolbarImage Type="Separator" runat="server" />
                            <ighedit:ToolbarButton Type="Undo" runat="server" />
                            <ighedit:ToolbarButton Type="Redo" runat="server" />
                            <ighedit:ToolbarImage Type="Separator" runat="server" />
                            <ighedit:ToolbarDialogButton Type="FontColor" runat="server" />
                            <ighedit:ToolbarDialogButton Type="FontHighlight" runat="server" /> 
                            <ighedit:ToolbarDialogButton Type="SpecialCharacter" runat="server" />
                            <ighedit:ToolbarMenuButton Type="InsertTable" runat="server" /> 
                            <ighedit:ToolbarImage Type="Separator" runat="server" /> 
                            <ighedit:ToolbarButton Type="InsertLink" runat="server" /> 
                            <ighedit:ToolbarButton Type="RemoveLink" runat="server" /> 
                            <ighedit:ToolbarImage Type="Separator" runat="server" /> 
                            <ighedit:ToolbarButton Type="WordCount" runat="server" />
                            <ighedit:ToolbarButton Type="Preview" runat="server" />
                            <ighedit:ToolbarImage Type=RowSeparator runat="server" /> 
                            <ighedit:ToolbarImage Type="DoubleSeparator" runat="server" />
                            <ighedit:ToolbarDropDown Type="FontName" runat="server" />
                            <ighedit:ToolbarDropDown Type="FontSize" runat="server" /> 
                            <ighedit:ToolbarDropDown Type="FontFormatting" runat="server" />
                            <ighedit:ToolbarDropDown Type="FontStyle" runat="server" /> 
                       </Toolbar> 
                  </ighedit:WebHtmlEditor>--%>
        </div>  
    </div>

    <div class ="TableRow" ALIGN="center" >
        <asp:Button ID ="btnSave" runat ="server" CssClass ="CommonButton" Text ="Save" 
            onclick="btnSave_Click"  ValidationGroup ="vgEmailtemplate"/>
        <asp:Button ID ="btnResetTemplate" runat ="server" CssClass ="CommonButton" 
            Text ="Reset Template" onclick="btnResetTemplate_Click" />
      
    </div>


</div>
