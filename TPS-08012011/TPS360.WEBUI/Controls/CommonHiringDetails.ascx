﻿<%-- -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: CommonHiringDetails.ascx
    Description: This is the page which is used Offer detials Enter.
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date           Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1                 22/Feb/2016    pravin khot        Added RegularExpressionValidator ID - { REVtxtBasic ,REVtxtHra,REVtxtRetentionBonus,REVtxtLTA,
                                                                         REVtxtPerformanceLinkedIncentive,REVtxtMedical,REVtxtSpecialAllowance,REVtxtConveyence,
                                                                         REVtxtFlexiBenefitsPlan,REVtxtPF,REVtxtESIC,REVtxtBonus,REVtxtRoleAllowance,REVtxtGratuity,
                                                                         REVtxtSiteAllowance,REVtxtEducationalAllowance,REVtxtRelocationAllowance,REVtxtReimbursement,
                                                                         REVtxtSalesIncentive,REVtxtMaximunAnnualIncentive,REVtxtCarAllowance
    0.2                22/Feb/2016     pravin khot       add property -[rel="ValidDigit"]  ID= txtSalary ,txtSalary_Commission  
                                                         add javascript [$('input[rel=ValidDigit]').keypress]   
    0.3                8/March/2016    pravin khot       rename label using Salary Components.                                                      
    0.4                02/Dec/2016     Prasanth Kumar G  Issue id : 1030                                                                                                                      
-------------------------------------------------------------------------------------------------------------------------------------------       

--%>
<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CommonHiringDetails.ascx.cs"
    Inherits="TPS360.Web.UI.CommonHiringDetails" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Assembly="Infragistics35.Web.v12.2, Version=12.2.20122.1007, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.Web.UI.LayoutControls" TagPrefix="ig" %>
<%@ Register Assembly="Infragistics35.Web.v12.2, Version=12.2.20122.1007, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.Web.UI.EditorControls" TagPrefix="ig" %>
    <%@ Register Src ="~/Controls/HtmlEditor.ascx" TagName ="HtmlEditor" TagPrefix ="ucl" %>
    <%@ Register Src="~/Controls/ConfirmationWindow.ascx" TagName="Confirm" TagPrefix="ucl" %>
<style type="text/css">
    .CalendarCSS
    {
        background-color: Gray;
        color: Blue;
        border-color: Maroon;
    }
    .custom-calendar .ajax__calendar_container
    {
        background-color: #ffc; /* pale yellow */
        border: solid 1px #666;
    }
    .custom-calendar .ajax__calendar_title
    {
        background-color: #cf9; /* pale green */
        height: 20px;
        color: #333;
    }
    .custom-calendar .ajax__calendar_prev, .custom-calendar .ajax__calendar_next
    {
        background-color: #aaa; /* darker gray */
        height: 20px;
        width: 20px;
    }
    .custom-calendar .ajax__calendar_today
    {
        background-color: #cff; /* pale blue */
        height: 20px;
    }
    .custom-calendar .ajax__calendar_days table thead tr td
    {
        background-color: #ff9; /* dark yellow */
        color: #333;
    }
    .custom-calendar .ajax__calendar_day
    {
        color: #333; /* normal day - darker gray color */
    }
    .custom-calendar .ajax__calendar_other .ajax__calendar_day
    {
        display: none;
        color: #666; /* day not actually in this month - lighter gray color */
    }
    .calendarDropdown
    {
        margin-left: -18px;
        margin-bottom: -3px;
    }
</style>

<script type="text/javascript" language="javascript" src="../js/ManagementBandOnChange.js"></script>

<script type="text/javascript" language="javascript" src="../js/AjaxScript.js"></script>

<script language="javascript" type="text/javascript">

    function OnSelectedIndexChange(ddlVendorContact, hdntemp) {

        var e = document.getElementById(ddlVendorContact);
        var hdnSelectedContact = document.getElementById(hdntemp);
        hdnSelectedContact.value = e.options[e.selectedIndex].value;

    }

    function WebDateChooser1_InitializeDateChooser(oDateChooser) {
        // used to set "drop-down block" variable if calendar was already opened
        oDateChooser._myMouseDown = function() {
            var me = igdrp_getComboById('<%=wdcDateOffered.ClientID%>');
            if (me && me.isDropDownVisible())
                me._calOpenedTime = new Date().getTime();
        }
        // used to open 
        oDateChooser._myClick = function() {
            var me = igdrp_getComboById('<%=wdcDateOffered.ClientID%>');
            if (me && !me.isDropDownVisible() && (!me._calOpenedTime || me._calOpenedTime + 500 < new Date().getTime()))
                me.setDropDownVisible(true);
        }
        oDateChooser.inputBox.onmousedown = oDateChooser._myMouseDown;
        oDateChooser.inputBox.onclick = oDateChooser._myClick;
        //alert('ok');
    }
    function fnValidateSalary(source, args) {
        // debugger;
        var txtValidCurrency = $get('<%= txtSalary.ClientID %>').value.trim();
        if ((txtValidCurrency != "" && txtValidCurrency >= 0 && !isNaN(txtValidCurrency))) // || (txtValidMaxCurrency != "" && txtValidMaxCurrency > 0 && !isNaN(txtValidMaxCurrency)))   
        {
            args.IsValid = true;
        }
        else args.IsValid = false;
    }

    function fnValidateSalaryCommission(source, args) {
        var txtValidCurrency_Commission = $get('<%= txtSalary_Commission.ClientID %>').value.trim();
        if ((txtValidCurrency_Commission != "" && txtValidCurrency_Commission >= 0 && !isNaN(txtValidCurrency_Commission))) // || (txtValidMaxCurrency != "" && txtValidMaxCurrency > 0 && !isNaN(txtValidMaxCurrency)))   
        {
            args.IsValid = true;
        }
        else args.IsValid = false;
    }


    $(document).ready(function() {
        // Sys.Application.add_load(function() {
        //$('input[rel=sal]').keypress(function(event) { return allownumeric(event, $(this)); });
        $('input[rel=ValidDigit]').keypress(function(event) { return allownumeric(event, $(this)); }); //Function added by pravin khot on 22/Feb/2016

        $('input[rel=sal]').each(function() {

            //            if($(this).val().trim()=='')$(this).val('0');
            //            $(this).change(function(){
            //                if($(this).val().trim()=='')$(this).val('0');
            $(this).change(function() {
                calculateGrandTotal();
                calculateGrandTotal1(1);
                calculateGrandTotal1(2);
                calculateGrandTotal1(3);
            });
        });
        $('input[rel=sal1]').each(function() {
            $(this).change(function() {
                calculateGrandTotal1(1);
            });
        });
        $('input[rel=sal2]').each(function() {
            $(this).change(function() {
                calculateGrandTotal1(2);
            });
        });
        $('input[rel=sal3]').each(function() {
            $(this).change(function() {
                calculateGrandTotal1(3);
            });
        });
    });


    function calculateGrandTotal() {
        var total = 0;
        $('input[rel=sal]').each(function() {
            total = total + parseFloat($(this).val() == '' ? 0 : $(this).val());
        });
        $('input[rel=Totalsal]').val(total.toFixed(2));
    }
    function calculateGrandTotal1(chk) {
        var total = parseFloat($('input[rel=Totalsal]').val());
        if (chk == 1) {
            $('input[rel=sal1]').each(function() {
                total = total - parseFloat($(this).val() == '' ? 0 : $(this).val());
            });
        }
        if (chk == 2) {
            $('input[rel=sal2]').each(function() {
                total = total - parseFloat($(this).val() == '' ? 0 : $(this).val());
            });
        }
        if (chk == 3) {
            $('input[rel=sal3]').each(function() {
                total = total - parseFloat($(this).val() == '' ? 0 : $(this).val());
            });
        }
        $('input[rel=Totalsal]').val(total.toFixed(2));
    }
    function ValidateSourceDescription(sender, args) {
        var ddlSource = $("#<%=ddlSource.ClientID %>");
        args.IsValid = true;
        //        if($('#<%=ddlSource.ClientID %>').prop("selectedIndex")==2)
        //        {
        //            if( $('#<%=ddlVendorContact.ClientID %>').prop("selectedIndex")<=0) args .IsValid=false ;
        //        }
        if ($(ddlSource).find('option:selected').text().trim() == "Job Portals") {
            if ($('#<%=ddlSourceDescription.ClientID %>').prop("selectedIndex") < 0) args.IsValid = false;

        }
        else if ($(ddlSource).find('option:selected').text().trim() == "Employee Referral") {
            if ($('#<%=ddlEmployeeReferrer.ClientID %>').prop("selectedIndex") <= 0) args.IsValid = false;
        }
        else {
            if ($('#<%= txtsrcdesc.ClientID %>').val().trim() == "") args.IsValid = false;
        }
        //        if($('#<%=ddlSource.ClientID %>').prop("selectedIndex")>0 && $('#<%=ddlEmployeeReferrer.ClientID %>').prop("selectedIndex")>0 && $('#<%=ddlSourceDescription.ClientID %>').prop("selectedIndex")>0 && $('#<%= txtsrcdesc.ClientID %>').val().trim()=="")
        //        {
        //           args.IsValid=false;
        //        }

    }

 
    //    function ValidateSourceDescription(sender,args)
    //    {
    //        args .IsValid=true;
    //         if($('#<%=ddlSource.ClientID %>').prop("selectedIndex")==7)
    //        {
    //           if( $('#<%=ddlSourceDescription.ClientID %>').prop("selectedIndex")<0) args .IsValid=false ;
    //        }
    //        else if($('#<%=ddlSource.ClientID %>').prop("selectedIndex")==10)
    //        {
    //            if( $('#<%=ddlEmployeeReferrer.ClientID %>').prop("selectedIndex")<=0) args .IsValid=false ;
    //        }
    //        else 
    //        {
    //            if( $('#<%= txtsrcdesc.ClientID %>').val().trim()=="") args .IsValid=false ;
    //        }
    //    
    //        
    //   }        


    function showHideSource(i) {

        var ddlSource = $("#<%=ddlSource.ClientID %>");
        $("#<%=divEmpReferral.ClientID %>").css("display", "none");
        $("#<%=divVendor.ClientID %>").css("display", "none");
        $("#<%=txtsrcdesc.ClientID %>").css("display", "none");
        $("#<%=ddlSourceDescription.ClientID %>").css("display", "none");


        if ($(ddlSource).find('option:selected').text().trim() == "Job Portals") {
            $("#<%=ddlSourceDescription.ClientID %>").css("display", "");
        }

        else if ($(ddlSource).find('option:selected').text().trim() == "Employee Referral") {
            //$("#<%=txtsrcdesc.ClientID %>").css("display", "");  
            $("#<%=divEmpReferral.ClientID %>").css("display", "");   //main
        }
        //        else  if ($(ddlSource).find('option:selected').text().trim() == "Placement Consultants")
        //        {
        //            $("#<%=divVendor.ClientID %>").css("display", "");            
        //        }
        else {
            $("#<%=txtsrcdesc.ClientID %>").css("display", "");
            if (i == 0)
                $("#<%=txtsrcdesc.ClientID %>").val('');
        }
    }

    Sys.Application.add_load(function() {
        $('input').click(function() {
            $(this).select();
        });
        
        $("#<%=ddlSource.ClientID %>").change(function() {
            showHideSource(0);
        });
        showHideSource(1);
    });
</script>

<script type="text/javascript">
    function onlyNumbers(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode

        if (charCode == 46) {
            var inputValue = $(this).val()
            if (inputValue.indexOf('.') < 1) {
                return true;
            }
            return false;
        }
        if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }
     <asp:HiddenField ID="hdnrmvCandidate" runat="server" />
      
</script>
<style type="text/css">
    .front
    {
        position: relative;
    }
   .FormRightColumn
    {
        width: 55% !important;
        margin-top: -205px !important;
    }
    .txtFormulaValue
    {
        text-align: right;
    }
    .txtTotalCTC
    {
        text-align: right;
    }
    html, #Body, #WrapperOuter, #Footer
    {
        background: none !important;
    }
    html, #Body, #WrapperOuter, #Footer
    {
        background: transparent !important;
    }
</style>
<asp:UpdatePanel ID="uppane" runat="server">
    <ContentTemplate>
        <asp:HiddenField ID="hdnBulkAction" runat="server" Value="" />
        <asp:HiddenField ID="hfMemberId" runat="server" />
        <asp:HiddenField ID="hfJobPostingId" runat="server" />
        <asp:HiddenField ID="hfMemberHiringDetailsId" runat="server" Value="0" />
        <asp:HiddenField ID="hfStatusId" runat="server" />
        <asp:HiddenField ID="hfCurrentMemberId" runat="server" />
        <asp:HiddenField ID="hdnSelectedMBand" runat="server" Value="0" />
        <asp:HiddenField ID="hdnSelectedBand" runat="server" Value="0" />

        <ucl:Confirm ID="uclConfirm" runat="server"></ucl:Confirm>
        <div class='noGradeSalTemp' style="text-align: center; font-family: Tahoma; font-size: 10pt;
            color: #FF0000; padding: 12px;">
            <asp:Label ID="lblMessage" runat="server"></asp:Label>
        </div>
        <div class="TableRow">
            <div class="FormLeftColumn">
                <div class="TableRow">
                    <div class="TableFormLeble">
                        Demand Type:
                    </div>
                    <div class="TableFormContent">
                        <asp:TextBox ID="txtJobTitle" runat="server" Enabled="false" CssClass="CommonTextBox demandType"
                            TabIndex="1" /> 
                    </div>
                </div>
                 <div class="TableRow">
                    <div class="TableFormLeble">
                         <asp:Label ID="lblDesignation" runat="server" Text="Designation:"></asp:Label>
                    </div>
                    <div class="TableFormContent">
                        <asp:TextBox ID="txtPosition" runat="server"  CssClass="CommonTextBox" TabIndex="2"></asp:TextBox>
                    </div>
                </div> 
                <div class="TableRow">
                    <div class="TableFormLeble">
                        Reporting Manager:
                    </div>
                    <div class="TableFormContent">
                        <asp:TextBox ID="txtSupervisor" runat="server"  Enabled="false" CssClass="CommonTextBox" TabIndex="3" />
                    </div>
                </div>
                
                  <div class="TableRow">
                    <div class="TableFormLeble">
                          Requisition Name :
                    </div>
                    <div class="TableFormContent">
                        <asp:TextBox ID="txtRequisitionName" runat="server" Enabled="false" CssClass="CommonTextBox" TabIndex="4"></asp:TextBox>
                    </div>
                </div>
                
                     
                
                 <div class="TableRow">
                    <div class="TableFormLeble">
                         Project Name :
                    </div>
                    <div class="TableFormContent">
                        <asp:TextBox ID="txtProjectName" runat="server" Enabled="false" CssClass="CommonTextBox" TabIndex="5"></asp:TextBox>
                    </div>
                </div>        
                
                
                               
                    <div class="TableRow">
                        <div class="TableFormLeble">                          
                            <asp:Label ID="lblResourceDate" runat="server" Text="Resource Date:"></asp:Label>
                        </div>                       
                        <div class="TableFormContent" style="vertical-align:bottom">                           
                            <asp:Label ID="lblResourceOnTextValue" runat="server"></asp:Label>                         
                        </div>                       
                    </div>    
                  
            <%--    <div class="TableRow">
                   <div class="TableFormLeble">
                        
                    </div>
                </div>--%>
                
                   
            </div>     
              
            <div class="FormRightColumn" style="width: 60% !important;">
                <div style="display: none">
                    <div class="TableRow">
                        <div class="TableFormLeble">
                            Offer Accepted:
                        </div>
                        <div class="TableFormContent" style="padding-left: 150px;">
                            <asp:CheckBox ID="chkOfferAccepted" runat="server" Checked="true" TabIndex="5" />
                        </div>
                    </div>
                </div>
                
                <div style="display: none">
                    <div class="TableRow">
                        <div class="TableFormLeble">
                            Commission/Referral:
                        </div>
                        <div class="TableFormContent">
                            <%-- //************************Code modify by pravin khot on 22/Feb/2016 ******add property -[rel="ValidDigit"]**********--%>
                            <asp:TextBox ID="txtSalary_Commission" runat="server" rel="ValidDigit" CssClass="CommonTextBox salarybox"
                                Width="60px" TabIndex="6"></asp:TextBox>
                            <%-- //********************************************END**************************************************--%>
                            <asp:DropDownList ID="ddlSalaryCurrent_Commission" runat="server" Width="60px" CssClass="CommonDropDownList"
                                TabIndex="7">
                            </asp:DropDownList>
                            <asp:Label ID="lblPayrateCurrency_Commission" runat="server"></asp:Label>
                        </div>
                    </div>
                </div>
               <%-- <div class="TableRow">
                    <div class="TableFormValidatorContent" style="margin-left: 42%">
                        <asp:RegularExpressionValidator ID="regCommission" runat="server" ControlToValidate="txtSalary_Commission"
                            Display="Dynamic" ErrorMessage="Please enter Numeric values" ValidationExpression="(^\d*\.?\d*[0-9]+\d*$)|(^[0-9]+\d*\.\d*$)"
                            ValidationGroup="ValOfferDetails"></asp:RegularExpressionValidator>
                    </div>
                </div>--%>
                <div style="display: none">           
                    <div class="TableRow">
                        <div class="TableFormLeble">
                            Source:
                        </div>
                        <div class="TableFormContent">
                            <asp:DropDownList ID="ddlSource" runat="server" Width="100px" CssClass="CommonDropDownList"
                                EnableViewState="true">
                            </asp:DropDownList>
                            <span class="RequiredField">*</span>
                        </div>
                    </div>
             </div>
                    
               
             
                <%-- <div class="TableRow">
                    <div class="TableFormValidatorContent" style="margin-left: 42%">
                        <asp:RequiredFieldValidator ID="rfvBU" runat="server" ControlToValidate="ddlSource"
                            SetFocusOnError="true" ErrorMessage="Please Select Source." InitialValue="0"
                            Display="Dynamic" ValidationGroup="ValOfferDetails"></asp:RequiredFieldValidator>
                    </div>
                </div>--%>
                <div style="display: none">
                    <div class="TableRow">
                        <div class="TableFormLeble">
                            Source Description:
                        </div>
                        <div class="TableFormContent">
                            <div id="divVendor" runat="server" style="display: none; float: left;">
                                <asp:DropDownList ID="ddlVendor" runat="server" CssClass="chzn-select" Width="90px"
                                    TabIndex="10">
                                </asp:DropDownList>
                                <asp:HiddenField runat="server" ID="hdntemp" Value="0" />
                                <asp:DropDownList ID="ddlVendorContact" runat="server" CssClass="chzn-select" Width="80px"
                                    AutoPostBack="true" TabIndex="11">
                                    <%--onchange="OnSelectedIndexChange(ddlVendorContact.ClientID,hdntemp.ClientID)">--%>
                                </asp:DropDownList>
                                <asp:HiddenField ID="hdnSelectedVendorContact" runat="server" Value="0" />
                            </div>
                            <div id="divEmpReferral" runat="server" style="display: none; float: left;">
                                <asp:DropDownList ID="ddlEmployeeReferrer" runat="server" CssClass="chzn-select"
                                    Width="120px">
                                </asp:DropDownList>
                                <asp:HiddenField ID="hdnSelectedEmployeeReferrer" runat="server" Value="0" />
                            </div>
                            <asp:DropDownList ID="ddlSourceDescription" runat="server" Width="100px" CssClass="CommonDropDownList"
                                EnableViewState="true" TabIndex="13">
                            </asp:DropDownList>
                            <asp:TextBox ID="txtsrcdesc" runat="server" CssClass="CommonTextBox" Style="display: none"></asp:TextBox>
                            <span class="RequiredField">*</span>
                        </div>
                    </div>
                </div>
                <%-- <div class="TableRow">
                    <div class="TableFormValidatorContent" style="margin-left: 42%">
                        <asp:CustomValidator runat="server" Display="Dynamic" ValidationGroup="ValOfferDetails"
                            ID="CVSourceDescription" ErrorMessage="Please Select Source Description" ClientValidationFunction="ValidateSourceDescription"></asp:CustomValidator>
                    </div>
                </div>--%>
                <div style="display: none">
                    <div class="TableRow">
                        <div class="TableFormLeble">
                            Active Recruiter:
                        </div>
                        <div class="TableFormContent">
                            <asp:DropDownList ID="ddlActiveRecruiter" runat="server" Width="165px" CssClass="CommonDropDownList"
                                EnableViewState="true" TabIndex="15">
                            </asp:DropDownList>
                            <span class="RequiredField">*</span>
                        </div>
                    </div>
                </div>
            
            
                   
                    
                <div class="TableRow">
                    <div class="TableFormLeble">
                        Location:
                    </div>
                    <div class="TableFormContent">
                        <asp:DropDownList ID="ddlLocation" runat="server" Width="160px" CssClass="CommonDropDownList"
                            EnableViewState="true" TabIndex="16">
                        </asp:DropDownList>
                        <span class="RequiredField">*</span>
                    </div>
                    <div class="TableFormValidatorContent" style="margin-left: 42%">
                        <asp:RequiredFieldValidator ID="rfvLocation" runat="server" ControlToValidate="ddlLocation"
                            Display="Dynamic" ErrorMessage="Please Select Location." ValidationGroup="ValOfferDetails"
                            InitialValue="0"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div id='hideGrade' runat="server">
                    <div class="TableRow">
                        <div class="TableFormLeble">
                            Grade:
                        </div>
                        <div class="TableFormContent">
                            <asp:DropDownList ID="ddlSalGrade" runat="server" Width="160px" CssClass="CommonDropDownList"
                                EnableViewState="true" TabIndex="16">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="TableRow">
                    <div class="TableFormLeble">
                        <asp:Label ID="lblCTC" Text="CTC:" runat="server"></asp:Label>        
                    </div>
                    <div class="TableFormContent">
                        <%-- //************************Code modify by pravin khot on 22/Feb/2016 ******add property -[rel="ValidDigit"]**********--%>
                        <asp:TextBox ID="txtSalary" runat="server" CssClass="CommonTextBox" rel="ValidDigit"
                            TabIndex="2" Width="80px" onchange="fnSalaryAnnexure()" style="text-align: right;"></asp:TextBox>
                        <%--                      //***************************************END****************************************************  --%>
                        <asp:DropDownList ID="ddlSalary" runat="server" CssClass="CommonDropDownList" Width="65px"
                            TabIndex="3">
                            <asp:ListItem Value="4" Selected="True">Yearly</asp:ListItem>
                            <asp:ListItem Value="3">Monthly</asp:ListItem>
                            <asp:ListItem Value="2">Daily</asp:ListItem>
                            <asp:ListItem Value="1">Hourly</asp:ListItem>
                        </asp:DropDownList>
                        <asp:DropDownList ID="ddlSalaryCurrency" runat="server" Width="65px" CssClass="CommonDropDownList"
                            TabIndex="4">
                        </asp:DropDownList>
                        <asp:Label ID="lblPayrateCurrency" Visible="false" runat="server"></asp:Label>
                        <span class="RequiredField">*</span>
                    </div>
                </div>
                
                  
                <div class="TableRow">
                    <div class="TableFormValidatorContent" style="margin-left: 42%">
                        <asp:RegularExpressionValidator ID="rgvSalary" runat="server" ControlToValidate="txtSalary"
                            Display="Dynamic" ErrorMessage="Please enter Numeric values." ValidationExpression="(^\d*\.?\d*[0-9]+\d*$)|(^[0-9]+\d*\.\d*$)"
                            ValidationGroup="ValOfferDetails"></asp:RegularExpressionValidator>
                    </div>
                    <div class="TableFormValidatorContent" style="margin-left: 42%">
                        <asp:RequiredFieldValidator ID="rfvSalary" runat="server" ControlToValidate="txtSalary"
                            Display="Dynamic" ErrorMessage="Please enter Salary." ValidationGroup="ValOfferDetails"></asp:RequiredFieldValidator>
                    </div>
                </div>
                
                             
           <div class="TableRow"  id="div13" runat="server" style="display: none">
           <div class="TableFormLeble">
                            <asp:Label ID="lblOpenDate" runat="server" Text="Date of Offer"></asp:Label>:
                            
                        </div>
              <div class="TableFormContent" style="white-space: nowrap">
               <div>
                <div style="float: left" id="divOpenDate" runat="server">
                     <ig:WebDatePicker ID="wdcDateOffered" DropDownCalendarID="ddcwdcDateOffered" runat="server">  </ig:WebDatePicker>
                     <ig:WebMonthCalendar ID="ddcwdcDateOffered" AllowNull="true" HideOtherMonthDays="true" runat="server">  </ig:WebMonthCalendar>
                </div>
                                 
                </div>
               </div>
               </div>         
                        
                    <div class="TableRow">              
                         <div class="TableFormLeble">
                            <asp:Label ID="Label1" runat="server" Text="Expected DOJ"></asp:Label>:
                    
                        </div>
                        <div class="TableFormContent" style="white-space: nowrap">
                            <div>
                                <div style="float: left" id="div12" runat="server">

                                    <ig:WebDatePicker ID="wdcJoiningDate" DropDownCalendarID="ddcJoiningDate" runat="server">
                                      
                                    </ig:WebDatePicker>
                                    <ig:WebMonthCalendar ID="ddcJoiningDate" AllowNull="true" HideOtherMonthDays="true"
                                        runat="server">
                                    </ig:WebMonthCalendar>
                                </div>
                                
                            </div>
                        </div>
              </div>
              
                   <div class="TableRow">
                        <div class="TableFormLeble">
                            Source:
                        </div>
                        <div class="TableFormContent">
                           <asp:TextBox ID="lblsourcedescription" runat="server" TextMode="MultiLine" ReadOnly="true" CssClass="CommonTextBox" TabIndex="10"></asp:TextBox>
                           <%-- <asp:Label ID="lblsourcedescription" runat="server" class="TableFormContent"></asp:Label>  --%>                         
                        </div>
                    </div>
                             
              
            </div>
            
            
        </div>
        <div  class ="TableRow" align="left"    style =" width:98.4%" > Reference Information:</div>
        <div class="TableFormContent"  >
            <ucl:HtmlEditor ID="WHEJobDescription" runat ="server" EnableViewState ="true"  /><div style ="  float :right ;" ><span class="RequiredField"></span></div>
        </div>
<%--       <asp:CustomValidator ID="cvDescription" runat ="server" ErrorMessage ="Please enter description" ValidationGroup ="pk" ClientValidationFunction ="validateDescription" EnableClientScript ="true"  ></asp:CustomValidator>
--%>        
        <div class="TableRow" style="padding: 10px !important" id="calculateSal" runat="server">
            <div class="TableFormContent" style="text-align: center; margin-top: 50px;">
                <div id="calculateSection" runat="server">
                    <asp:Button ID="btnCalculate" runat="server" Text="Calculate" ValidationGroup="pk" OnClick="btnCalculate_Click"
                        CssClass="CommonButton" OnClientClick="SetIframeSize();" />
                </div>
            </div>
        </div>
        <br />
        <br />
        <br />
        <!-- code modify by pravin khot on 8/March/2016 (rename label using Salary Components)------------------------>
        <div id='IsContract' runat="server">
            <div class="TableRow">
                <div class="TabPanelHeader">
                    Salary Components:
                </div>
                <div class="FormLeftColumn">
                    <div class="TableRow">
                        <div class="TableFormLeble">
                            Basic:
                        </div>
                        <div class="TableFormContent">
                            <asp:TextBox ID="txtBasic" runat="server" CssClass="CommonTextBox" Width="80px" rel="sal"
                                TabIndex="8" Text="0"></asp:TextBox>
                        </div>
                    </div>
                    <%-- *****************Code added by pravin khot on 22/Feb/2016*************--%>
                    <div class="TableRow">
                        <div class="TableFormValidatorContent" style="margin-left: 42%">
                            <asp:RegularExpressionValidator ID="REVtxtBasic" runat="server" ControlToValidate="txtBasic"
                                Display="Dynamic" ErrorMessage="Please enter Numeric values" ValidationExpression="(^\d*\.?\d*[0-9]+\d*$)|(^[0-9]+\d*\.\d*$)"
                                ValidationGroup="ValOfferDetails"></asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <%-- ***********************************END***********************************--%>
                    <div class="TableRow">
                        <div class="TableFormLeble">
                            HRA:
                        </div>
                        <div class="TableFormContent">
                            <asp:TextBox ID="txtHra" runat="server" CssClass="CommonTextBox" Width="80px" rel="sal"
                                TabIndex="9" Text="0"></asp:TextBox>
                        </div>
                    </div>
                    <%-- *****************Code added by pravin khot on 22/Feb/2016*************--%>
                    <div class="TableRow">
                        <div class="TableFormValidatorContent" style="margin-left: 42%">
                            <asp:RegularExpressionValidator ID="REVtxtHra" runat="server" ControlToValidate="txtHra"
                                Display="Dynamic" ErrorMessage="Please enter Numeric values" ValidationExpression="(^\d*\.?\d*[0-9]+\d*$)|(^[0-9]+\d*\.\d*$)"
                                ValidationGroup="ValOfferDetails"></asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <%-- ***********************************END***********************************--%>
                    <div id="Div2" class="TableRow" runat="server">
                        <div class="TableFormLeble">
                            <%--E.A (Fixed):--%>
                            Retention Bonus:
                        </div>
                        <div class="TableFormContent">
                            <asp:TextBox ID="txtRetentionBonus" runat="server" CssClass="CommonTextBox" Width="80px"
                                rel="sal" TabIndex="10" Text="0"></asp:TextBox>
                        </div>
                    </div>
                    <%-- *****************Code added by pravin khot on 22/Feb/2016*************--%>
                    <div class="TableRow">
                        <div class="TableFormValidatorContent" style="margin-left: 42%">
                            <asp:RegularExpressionValidator ID="REVtxtRetentionBonus" runat="server" ControlToValidate="txtRetentionBonus"
                                Display="Dynamic" ErrorMessage="Please enter Numeric values" ValidationExpression="(^\d*\.?\d*[0-9]+\d*$)|(^[0-9]+\d*\.\d*$)"
                                ValidationGroup="ValOfferDetails"></asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <%-- ***********************************END***********************************--%>
                    <div id="Div3" class="TableRow" runat="server">
                        <div class="TableFormLeble">
                            <%--  LTC/LTA:--%>
                            LTA:
                        </div>
                        <div class="TableFormContent">
                            <asp:TextBox ID="txtLTA" runat="server" CssClass="CommonTextBox" Width="80px" rel="sal"
                                TabIndex="11" Text="0"></asp:TextBox>
                        </div>
                    </div>
                    <%-- *****************Code added by pravin khot on 22/Feb/2016*************--%>
                    <div class="TableRow">
                        <div class="TableFormValidatorContent" style="margin-left: 42%">
                            <asp:RegularExpressionValidator ID="REVtxtLTA" runat="server" ControlToValidate="txtLTA"
                                Display="Dynamic" ErrorMessage="Please enter Numeric values" ValidationExpression="(^\d*\.?\d*[0-9]+\d*$)|(^[0-9]+\d*\.\d*$)"
                                ValidationGroup="ValOfferDetails"></asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <%-- ***********************************END***********************************--%>
                    <div class="TableRow">
                        <div class="TableFormLeble">
                            <%--Skill Allowance:--%>
                            Entertainment Allowance:
                        </div>
                        <div class="TableFormContent">
                            <asp:TextBox ID="txtPerformanceLinkedIncentive" runat="server" CssClass="CommonTextBox"
                                Width="80px" rel="sal" TabIndex="12" Text="0"></asp:TextBox>
                        </div>
                    </div>
                    <%-- *****************Code added by pravin khot on 22/Feb/2016*************--%>
                    <div class="TableRow">
                        <div class="TableFormValidatorContent" style="margin-left: 42%">
                            <asp:RegularExpressionValidator ID="REVtxtPerformanceLinkedIncentive" runat="server"
                                ControlToValidate="txtPerformanceLinkedIncentive" Display="Dynamic" ErrorMessage="Please enter Numeric values"
                                ValidationExpression="(^\d*\.?\d*[0-9]+\d*$)|(^[0-9]+\d*\.\d*$)" ValidationGroup="ValOfferDetails"></asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <%-- ***********************************END***********************************--%>
                    <div class="TableRow">
                        <div class="TableFormLeble">
                            <%-- Medical Allow/Reimbursement:--%>
                            Medical Insurance Premium:
                        </div>
                        <div class="TableFormContent">
                            <asp:TextBox ID="txtMedical" runat="server" CssClass="CommonTextBox" Width="80px"
                                rel="sal" TabIndex="13" Text="0"></asp:TextBox>
                        </div>
                    </div>
                    <%-- *****************Code added by pravin khot on 22/Feb/2016*************--%>
                    <div class="TableRow">
                        <div class="TableFormValidatorContent" style="margin-left: 42%">
                            <asp:RegularExpressionValidator ID="REVtxtMedical" runat="server" ControlToValidate="txtMedical"
                                Display="Dynamic" ErrorMessage="Please enter Numeric values" ValidationExpression="(^\d*\.?\d*[0-9]+\d*$)|(^[0-9]+\d*\.\d*$)"
                                ValidationGroup="ValOfferDetails"></asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <%-- ***********************************END***********************************--%>
                    <div class="TableRow">
                        <div class="TableFormLeble">
                            <%-- Food Allow/Meal Vouchers:--%>
                            Special Allowance:
                        </div>
                        <div class="TableFormContent">
                            <asp:TextBox ID="txtSpecialAllowance" runat="server" CssClass="CommonTextBox" Width="80px"
                                rel="sal" TabIndex="14" Text="0"></asp:TextBox>
                        </div>
                    </div>
                    <%-- *****************Code added by pravin khot on 22/Feb/2016*************--%>
                    <div class="TableRow">
                        <div class="TableFormValidatorContent" style="margin-left: 42%">
                            <asp:RegularExpressionValidator ID="REVtxtSpecialAllowance" runat="server" ControlToValidate="txtSpecialAllowance"
                                Display="Dynamic" ErrorMessage="Please enter Numeric values" ValidationExpression="(^\d*\.?\d*[0-9]+\d*$)|(^[0-9]+\d*\.\d*$)"
                                ValidationGroup="ValOfferDetails"></asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <%-- ***********************************END***********************************--%>
                    <div class="TableRow">
                        <div class="TableFormLeble">
                            Conveyance:
                        </div>
                        <div class="TableFormContent">
                            <asp:TextBox ID="txtConveyence" runat="server" CssClass="CommonTextBox" Width="80px"
                                rel="sal" TabIndex="15" Text="0"></asp:TextBox>
                        </div>
                    </div>
                    <%-- *****************Code added by pravin khot on 22/Feb/2016*************--%>
                    <div class="TableRow">
                        <div class="TableFormValidatorContent" style="margin-left: 42%">
                            <asp:RegularExpressionValidator ID="REVtxtConveyence" runat="server" ControlToValidate="txtConveyence"
                                Display="Dynamic" ErrorMessage="Please enter Numeric values" ValidationExpression="(^\d*\.?\d*[0-9]+\d*$)|(^[0-9]+\d*\.\d*$)"
                                ValidationGroup="ValOfferDetails"></asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <%-- ***********************************END***********************************--%>
                </div>
                <div class="FormRightColumn">
                    <div id="Div101" class="TableRow" runat="server">
                        <div id="Div1" class="TableRow" runat="server" visible="false">
                            <div class="TableFormLeble">
                                Flexi Benefits Plan:
                            </div>
                            <div class="TableFormContent">
                                <asp:TextBox ID="txtFlexiBenefitsPlan" runat="server" CssClass="CommonTextBox" Width="80px"
                                    rel="sal" TabIndex="16" Text="0"></asp:TextBox>
                            </div>
                        </div>
                        <%-- *****************Code added by pravin khot on 22/Feb/2016*************--%>
                        <div class="TableRow">
                            <div class="TableFormValidatorContent" style="margin-left: 42%">
                                <asp:RegularExpressionValidator ID="REVtxtFlexiBenefitsPlan" runat="server" ControlToValidate="txtFlexiBenefitsPlan"
                                    Display="Dynamic" ErrorMessage="Please enter Numeric values" ValidationExpression="(^\d*\.?\d*[0-9]+\d*$)|(^[0-9]+\d*\.\d*$)"
                                    ValidationGroup="ValOfferDetails"></asp:RegularExpressionValidator>
                            </div>
                        </div>
                        <%-- ***********************************END***********************************--%>
                        <div class="TableRow">
                            <div style="width: 250px" class="TableFormLeble">
                                <%--PF:--%>
                                PPF Allowance:
                            </div>
                            <div class="TableFormContent">
                                <asp:TextBox ID="txtPF" runat="server" CssClass="CommonTextBox" Width="80px" rel="sal1"
                                    TabIndex="17" Text="0"></asp:TextBox>
                            </div>
                        </div>
                        <%-- *****************Code added by pravin khot on 22/Feb/2016*************--%>
                        <div class="TableRow">
                            <div class="TableFormValidatorContent" style="margin-left: 42%">
                                <asp:RegularExpressionValidator ID="REVtxtPF" runat="server" ControlToValidate="txtPF"
                                    Display="Dynamic" ErrorMessage="Please enter Numeric values" ValidationExpression="(^\d*\.?\d*[0-9]+\d*$)|(^[0-9]+\d*\.\d*$)"
                                    ValidationGroup="ValOfferDetails"></asp:RegularExpressionValidator>
                            </div>
                        </div>
                        <%-- ***********************************END***********************************--%>
                        <div class="TableRow">
                            <div style="width: 250px" class="TableFormLeble">
                                <%-- ESIC:--%>
                                Professional Development:
                            </div>
                            <div class="TableFormContent">
                                <asp:TextBox ID="txtESIC" runat="server" CssClass="CommonTextBox" Width="80px" rel="sal2"
                                    TabIndex="18" Text="0"></asp:TextBox>
                            </div>
                        </div>
                        <%-- *****************Code added by pravin khot on 22/Feb/2016*************--%>
                        <div class="TableRow">
                            <div class="TableFormValidatorContent" style="margin-left: 42%">
                                <asp:RegularExpressionValidator ID="REVtxtESIC" runat="server" ControlToValidate="txtESIC"
                                    Display="Dynamic" ErrorMessage="Please enter Numeric values" ValidationExpression="(^\d*\.?\d*[0-9]+\d*$)|(^[0-9]+\d*\.\d*$)"
                                    ValidationGroup="ValOfferDetails"></asp:RegularExpressionValidator>
                            </div>
                        </div>
                        <%-- ***********************************END***********************************--%>
                        <div class="TableRow">
                            <div style="width: 250px" class="TableFormLeble">
                                <%-- Statutory Bonus:--%>
                                Festival Bonus:
                            </div>
                            <div class="TableFormContent">
                                <asp:TextBox ID="txtBonus" runat="server" CssClass="CommonTextBox" Width="80px" TabIndex="19"
                                    Text="0"></asp:TextBox>
                            </div>
                        </div>
                        <%-- *****************Code added by pravin khot on 22/Feb/2016*************--%>
                        <div class="TableRow">
                            <div class="TableFormValidatorContent" style="margin-left: 42%">
                                <asp:RegularExpressionValidator ID="REVtxtBonus" runat="server" ControlToValidate="txtBonus"
                                    Display="Dynamic" ErrorMessage="Please enter Numeric values" ValidationExpression="(^\d*\.?\d*[0-9]+\d*$)|(^[0-9]+\d*\.\d*$)"
                                    ValidationGroup="ValOfferDetails"></asp:RegularExpressionValidator>
                            </div>
                        </div>
                        <%-- ***********************************END***********************************--%>
                        <div id="Div4" class="TableRow" runat="server">
                            <div style="width: 250px" class="TableFormLeble">
                                <%--  Group Insurance:--%>
                                Child Education:
                            </div>
                            <div class="TableFormContent">
                                <asp:TextBox ID="txtRoleAllowance" runat="server" CssClass="CommonTextBox" Width="80px"
                                    TabIndex="20" Text="0"></asp:TextBox>
                            </div>
                        </div>
                        <%-- *****************Code added by pravin khot on 22/Feb/2016*************--%>
                        <div class="TableRow">
                            <div class="TableFormValidatorContent" style="margin-left: 42%">
                                <asp:RegularExpressionValidator ID="REVtxtRoleAllowance" runat="server" ControlToValidate="txtRoleAllowance"
                                    Display="Dynamic" ErrorMessage="Please enter Numeric values" ValidationExpression="(^\d*\.?\d*[0-9]+\d*$)|(^[0-9]+\d*\.\d*$)"
                                    ValidationGroup="ValOfferDetails"></asp:RegularExpressionValidator>
                            </div>
                        </div>
                        <%-- ***********************************END***********************************--%>
                        <div class="TableRow">
                            <div style="width: 250px" class="TableFormLeble">
                                <%--Gratuity:--%>
                                Fuel and Maintenance (F&M) – Car:
                            </div>
                            <div class="TableFormContent">
                                <asp:TextBox ID="txtGratuity" runat="server" CssClass="CommonTextBox" Width="80px"
                                    TabIndex="21" Text="0"></asp:TextBox>
                            </div>
                        </div>
                        <%-- *****************Code added by pravin khot on 22/Feb/2016*************--%>
                        <div class="TableRow">
                            <div class="TableFormValidatorContent" style="margin-left: 42%">
                                <asp:RegularExpressionValidator ID="REVtxtGratuity" runat="server" ControlToValidate="txtGratuity"
                                    Display="Dynamic" ErrorMessage="Please enter Numeric values" ValidationExpression="(^\d*\.?\d*[0-9]+\d*$)|(^[0-9]+\d*\.\d*$)"
                                    ValidationGroup="ValOfferDetails"></asp:RegularExpressionValidator>
                            </div>
                        </div>
                        <%-- ***********************************END***********************************--%>
                        <div id="Div5" class="TableRow" runat="server">
                            <%--<div class="TableFormLeble">
                       Site Allowance:
                    </div>--%>
                            <div style="width: 250px" class="TableFormLeble">
                                <%-- Professional Tax:--%>
                                City Allowance:
                            </div>
                            <div class="TableFormContent">
                                <asp:TextBox ID="txtSiteAllowance" runat="server" CssClass="CommonTextBox" Width="80px"
                                    rel="sal3" TabIndex="22" Text="0"></asp:TextBox>
                            </div>
                        </div>
                        <%-- *****************Code added by pravin khot on 22/Feb/2016*************--%>
                        <div class="TableRow">
                            <div class="TableFormValidatorContent" style="margin-left: 42%">
                                <asp:RegularExpressionValidator ID="REVtxtSiteAllowance" runat="server" ControlToValidate="txtSiteAllowance"
                                    Display="Dynamic" ErrorMessage="Please enter Numeric values" ValidationExpression="(^\d*\.?\d*[0-9]+\d*$)|(^[0-9]+\d*\.\d*$)"
                                    ValidationGroup="ValOfferDetails"></asp:RegularExpressionValidator>
                            </div>
                        </div>
                        <%-- ***********************************END***********************************--%>
                        <div id="Div6" class="TableRow" runat="server" visible="false">
                            <div class="TableFormLeble">
                                Educational Allowance:
                            </div>
                            <div class="TableFormContent">
                                <asp:TextBox ID="txtEducationalAllowance" runat="server" CssClass="CommonTextBox"
                                    Width="80px" rel="sal" TabIndex="23" Text="0"></asp:TextBox>
                            </div>
                        </div>
                        <%-- *****************Code added by pravin khot on 22/Feb/2016*************--%>
                        <div class="TableRow">
                            <div class="TableFormValidatorContent" style="margin-left: 42%">
                                <asp:RegularExpressionValidator ID="REVtxtEducationalAllowance" runat="server" ControlToValidate="txtEducationalAllowance"
                                    Display="Dynamic" ErrorMessage="Please enter Numeric values" ValidationExpression="(^\d*\.?\d*[0-9]+\d*$)|(^[0-9]+\d*\.\d*$)"
                                    ValidationGroup="ValOfferDetails"></asp:RegularExpressionValidator>
                            </div>
                        </div>
                        <%-- ***********************************END***********************************--%>
                        <div id="Div7" class="TableRow" runat="server" visible="false">
                            <div class="TableFormLeble">
                                Relocation Allowance:
                            </div>
                            <div class="TableFormContent">
                                <asp:TextBox ID="txtRelocationAllowance" runat="server" CssClass="CommonTextBox"
                                    Width="80px" rel="sal" TabIndex="24" Text="0"></asp:TextBox>
                            </div>
                        </div>
                        <%-- *****************Code added by pravin khot on 22/Feb/2016*************--%>
                        <div class="TableRow">
                            <div class="TableFormValidatorContent" style="margin-left: 42%">
                                <asp:RegularExpressionValidator ID="REVtxtRelocationAllowance" runat="server" ControlToValidate="txtRelocationAllowance"
                                    Display="Dynamic" ErrorMessage="Please enter Numeric values" ValidationExpression="(^\d*\.?\d*[0-9]+\d*$)|(^[0-9]+\d*\.\d*$)"
                                    ValidationGroup="ValOfferDetails"></asp:RegularExpressionValidator>
                            </div>
                        </div>
                        <%-- ***********************************END***********************************--%>
                        <div id="Div8" class="TableRow" runat="server" visible="false">
                            <div class="TableFormLeble">
                                Medical Reimbursement:
                                <%-- Reimbursement:--%>
                            </div>
                            <div class="TableFormContent">
                                <asp:TextBox ID="txtReimbursement" runat="server" CssClass="CommonTextBox" Width="80px"
                                    rel="sal" TabIndex="25" Text="0"></asp:TextBox>
                            </div>
                        </div>
                        <%-- *****************Code added by pravin khot on 22/Feb/2016*************--%>
                        <div class="TableRow">
                            <div class="TableFormValidatorContent" style="margin-left: 42%">
                                <asp:RegularExpressionValidator ID="REVtxtReimbursement" runat="server" ControlToValidate="txtReimbursement"
                                    Display="Dynamic" ErrorMessage="Please enter Numeric values" ValidationExpression="(^\d*\.?\d*[0-9]+\d*$)|(^[0-9]+\d*\.\d*$)"
                                    ValidationGroup="ValOfferDetails"></asp:RegularExpressionValidator>
                            </div>
                        </div>
                        <%-- ***********************************END***********************************--%>
                        <div id="Div9" class="TableRow" runat="server" visible="false">
                            <div style="width: 250px" class="TableFormLeble">
                                Annual Performance Bonus:
                                <%--  Sales Incentive:--%>
                            </div>
                            <div class="TableFormContent">
                                <asp:TextBox ID="txtSalesIncentive" runat="server" CssClass="CommonTextBox" Width="80px"
                                    rel="sal" TabIndex="23" Text="0"></asp:TextBox>
                            </div>
                        </div>
                        <%-- *****************Code added by pravin khot on 22/Feb/2016*************--%>
                        <div class="TableRow">
                            <div class="TableFormValidatorContent" style="margin-left: 42%">
                                <asp:RegularExpressionValidator ID="REVtxtSalesIncentive" runat="server" ControlToValidate="txtSalesIncentive"
                                    Display="Dynamic" ErrorMessage="Please enter Numeric values" ValidationExpression="(^\d*\.?\d*[0-9]+\d*$)|(^[0-9]+\d*\.\d*$)"
                                    ValidationGroup="ValOfferDetails"></asp:RegularExpressionValidator>
                            </div>
                        </div>
                        <%-- ***********************************END***********************************--%>
                        <div id="Div10" class="TableRow" runat="server" visible="false">
                            <div class="TableFormLeble">
                                Maximun Annual Incentive:
                            </div>
                            <div class="TableFormContent">
                                <asp:TextBox ID="txtMaximunAnnualIncentive" runat="server" CssClass="CommonTextBox"
                                    Width="80px" rel="sal" Text="0"></asp:TextBox>
                            </div>
                        </div>
                        <%-- *****************Code added by pravin khot on 22/Feb/2016*************--%>
                        <div class="TableRow">
                            <div class="TableFormValidatorContent" style="margin-left: 42%">
                                <asp:RegularExpressionValidator ID="REVtxtMaximunAnnualIncentive" runat="server"
                                    ControlToValidate="txtMaximunAnnualIncentive" Display="Dynamic" ErrorMessage="Please enter Numeric values"
                                    ValidationExpression="(^\d*\.?\d*[0-9]+\d*$)|(^[0-9]+\d*\.\d*$)" ValidationGroup="ValOfferDetails"></asp:RegularExpressionValidator>
                            </div>
                        </div>
                        <%-- ***********************************END***********************************--%>
                        <div id="Div11" class="TableRow" runat="server" visible="false">
                            <div class="TableFormLeble">
                                Car Allowance:
                            </div>
                            <div class="TableFormContent">
                                <asp:TextBox ID="txtCarAllowance" runat="server" CssClass="CommonTextBox" Width="80px"
                                    rel="sal" Text="0"></asp:TextBox>
                            </div>
                        </div>
                        <%-- *****************Code added by pravin khot on 22/Feb/2016*************--%>
                        <div class="TableRow">
                            <div class="TableFormValidatorContent" style="margin-left: 42%">
                                <asp:RegularExpressionValidator ID="REVtxtCarAllowance" runat="server" ControlToValidate="txtCarAllowance"
                                    Display="Dynamic" ErrorMessage="Please enter Numeric values" ValidationExpression="(^\d*\.?\d*[0-9]+\d*$)|(^[0-9]+\d*\.\d*$)"
                                    ValidationGroup="ValOfferDetails"></asp:RegularExpressionValidator>
                            </div>
                        </div>
                        <%-- ***********************************END***********************************--%>
                        <div class="TableRow">
                            <div style="width: 250px" class="TableFormLeble">
                                <%-- Net Payout:--%>
                                CTC :
                            </div>
                            <div class="TableFormContent">
                                <asp:TextBox ID="txtGrandTotal" runat="server" CssClass="CommonTextBox" Width="80px"
                                    rel="Totalsal" Enabled="false" TabIndex="25" Text="0"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id='IsPermanent' runat="server" class='clsPermanent'>
            <div class="TableRow" id="hideHeader" runat="server">
                <div class="TabPanelHeader">
                    Salary Components:
                </div>
            </div>
            <div style="width: 90%; margin: 0 auto;">
                <asp:GridView ID="grvSalaryTemplate" runat="server" AutoGenerateColumns="False">
                    <Columns>
                        <asp:BoundField DataField="Heads" HeaderText="Heads" />
                        <asp:TemplateField HeaderText="Value">
                            <ItemTemplate>
                                <asp:TextBox ID="txtFormulaValue" CssClass="txtFormulaValue" runat="server" onkeypress="return onlyNumbers(this);"></asp:TextBox>
                        </ItemTemplate>                             
                        </asp:TemplateField>                     
                    </Columns>
                </asp:GridView>
            </div>
        </div>
        <div id="Div22" class="TableRow toggleBtn" align="center" style="padding: 20px;"
            runat="server">
            <%--<input type ="button" id ="txt" onclick ="mainScreen.OfferSaveClicked()" value ="Save" class ="CommonButton" />
            <input type ="button" id="btnRemove" value ="Remove Hiring Details"  class ="CommonButton" onclick ="mainScreen.RemoveHiringDetails()" />--%>
            <asp:Button ID="btnSave" runat="server" Text="Save and Submit" OnClick="btnSave_Click" CssClass="CommonButton"
                ValidationGroup="ValOfferDetails"   />  <%--OnClientClick="javascript:ValidateRemoveCandidates()"--%>
            <asp:Button ID="btnRemove" runat="server" Text="Remove Offer Details" OnClick="btnRemove_Click"
                CssClass="CommonButton" />
        </div>
        <div>
            <br />
            <br />
            <br />
        </div>
    </ContentTemplate>
</asp:UpdatePanel>

<script type="text/javascript">
    $(document).ready(function() {
        if ($('.demandType').val() == "Contract") {
            $('#iframe', window.parent.document);  //.width('900px')
            $('#iframe', window.parent.document);  //.height('370px');
            $("#<%=hideGrade.ClientID %>").css("display", "none");
        }
        else if ($('.demandType').val() == "Permanent" && $get('<%= txtSalary.ClientID %>').value.trim() == "") {
            $('#iframe', window.parent.document); //.width('900px');
            $('#iframe', window.parent.document); //.height('700px');
        }
    });
    function SetIframeSize() {
        var delay = 500;
        setTimeout(function() {
            if ($(".noGradeSalTemp span").text() == "" && $get('<%= txtSalary.ClientID %>').value.trim() == "") {
                $('#iframe', window.parent.document); //.width('900px');
                $('#iframe', window.parent.document); //.height('700px');
            }
            else if ($(".noGradeSalTemp span").text() != null && $(".noGradeSalTemp span").text() != "") {
                $('#iframe', window.parent.document); //.width('900px');
                $('#iframe', window.parent.document); //.height('700px');
            }
            else {
                $('#iframe', window.parent.document); //.width('900px');
                $('#iframe', window.parent.document); //.height('700px');
            }
        }, delay);
    }
    //Restrict the user to key-in chrectors and other special charectors
    function onlyNumbers(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode

        if (charCode == 46) {
            var inputValue = $(this).val()
            if (inputValue.indexOf('.') < 1) {
                return true;
            }
            return false;
        }
        if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }
</script>

