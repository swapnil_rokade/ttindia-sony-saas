﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="HRApproval.ascx.cs" Inherits="Controls_HRApproval" %>
<%@ Register Src="~/Controls/DateRangePicker.ascx" TagName="DateRangePicker" TagPrefix="ucl" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Controls/PagerControl.ascx" TagName="Pager" TagPrefix="ucl" %>
<%@ Register Src="~/Controls/ModalTemplate.ascx" TagName="Template" TagPrefix="ucl" %>
<%@ Register Src="~/Controls/RequisitionAssignedManagers.ascx" TagName="Managers"
    TagPrefix="ucl" %>
<%@ Register Src="~/Controls/CountryStatePicker.ascx" TagName="CountryState" TagPrefix="ucl" %>
<%@ Register Src="~/Controls/MultipleItemPicker.ascx" TagName="MultipleSelection"
    TagPrefix="ucl" %>
<style>
    .dropdown-menu:after
    {
        border-bottom: 7px solid #FFFFFF;
        border-left: 7px solid transparent;
        border-right: 7px solid transparent;
        content: "";
        left: 80%;
        position: absolute;
        top: -6px;
    }
    .dropdown-menu::before
    {
        position: absolute;
        top: -7px;
        left: 80%;
        border-right: 7px solid transparent;
        border-bottom: 7px solid #CCC;
        border-left: 7px solid transparent;
        content: '';
    }
    .pnlHide
    {
    }
    .pnlHide:first-child
    {
        overflow: hidden;
    }
</style>

<script type="text/javascript" language="javascript" src="../js/AjaxVariables.js"></script>

<script type="text/javascript" language="javascript" src="../js/AjaxScript.js"></script>

<script type="text/javascript">
    document.body.onclick = function(e) {
        $('#ctl00_cphHomeMaster_uclOfferApproval_pnlSearchBoxContent').css({ 'overflow-y': 'inherit', 'overflow-x': 'inherit' });
        $('#ctl00_cphHomeMaster_uclOfferApproval_pnlSearchBoxContent div:first').css({ 'overflow-y': 'inherit', 'overflow-x': 'inherit' });
    }
    //To Keep the scroll bar in the Exact Place
    Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(PageLoadedReq);
    function PageLoadedReq(sender, args) {
        try {
            var hdnScroll = document.getElementById('ctl00_cphHomeMaster_uclOfferApproval_hdnScrollPos');
            var bigDiv = document.getElementById('bigDiv');
            bigDiv.scrollLeft = hdnScroll.value;
            var background = $find("mpeModal")._backgroundElement;
            background.onclick = function() {
                CloseModal();
            }
        }
        catch (e) {
        }
    }
    function onShownManagers() {
        var background = $find("MPEManagers")._backgroundElement;
        background.onclick = function() { $find("MPEManagers").hide(); }
    }
    function SetScrollPosition() {
        var hdnScroll = document.getElementById('ctl00_cphHomeMaster_uclOfferApproval_hdnScrollPos');
        var bigDiv = document.getElementById('bigDiv');
        hdnScroll.value = bigDiv.scrollLeft;
    }


    function RefreshList() {
        var hdnDoPostPack = document.getElementById('<%=hdnDoPostPack.ClientID %>');
        hdnDoPostPack.value = "1";
        __doPostBack('ctl00_cphHomeMaster_uclOfferApproval_upcandidateList', ''); return false;
    }
   

</script>

<asp:HiddenField ID="hdnScrollPos" runat="server" />
<asp:ObjectDataSource ID="odsOfferList" runat="server" SelectMethod="GetPagedHROfferedList"
    TypeName="TPS360.Web.UI.OfferDataSource" SelectCountMethod="GetOfferedHRListCount">
    <SelectParameters>
        <asp:Parameter Name="memberId" />
        <asp:Parameter Name="approvalType" />
        <asp:Parameter Name="SortOrder" DefaultValue="asc" />
        <asp:Parameter Name="SortColumn" />
        <asp:Parameter Name="sortExpression" />
        <asp:Parameter Name="maximumRows" />
        <asp:Parameter Name="startRowIndex" />
    </SelectParameters>
</asp:ObjectDataSource>
<br />
<div>
    <asp:UpdatePanel ID="up" runat="server">
        <ContentTemplate>
            <ajaxToolkit:AlwaysVisibleControlExtender ID="avceMessage" runat="server" TargetControlID="lblMessage">
            </ajaxToolkit:AlwaysVisibleControlExtender>
            <asp:Label ID="lblMessage" runat="server" EnableViewState="false"></asp:Label>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>
<asp:UpdatePanel ID="upmemberList" runat="server">
    <ContentTemplate>
        <asp:HiddenField ID="hdnDoPostPack" runat="server" />
        <asp:HiddenField ID="hdnSortColumn" runat="server" />
        <asp:HiddenField ID="hdnSortOrder" runat="server" />
        <asp:TextBox ID="txtSortColumn" runat="server" Visible="false"></asp:TextBox>
        <asp:TextBox ID="txtSortOrder" runat="server" Visible="false"></asp:TextBox>
        <asp:TextBox ID="SortColumn" runat="server" EnableViewState="true" Visible="false" />
        <asp:TextBox ID="SortOrder" runat="server" EnableViewState="true" Visible="false" />
        <div class="GridContainer">
            <div style="overflow: auto" id="bigDiv" onscroll='SetScrollPosition()' style="width: 500px;">
                <asp:ListView ID="lsvOfferList" EnableViewState="true" DataKeyNames="Id" DataSourceID="odsOfferList"
                    runat="server" Style="width: 450px;" 
                    OnItemDataBound="lsvOfferList_ItemDataBound" 
                    onitemcommand="lsvOfferList_ItemCommand" onprerender="lsvOfferList_PreRender">
                    <LayoutTemplate>
                        <table id="tlbTemplate" runat="server" class="Grid" cellspacing="0" border="0">
                            <tr id="trHeadLevel" runat="server">
                                <th runat="server" id="th3" style="white-space: nowrap; text-align: left; width: 140px !important">
                                    <asp:LinkButton ID="lnkBtnName" runat="server" TabIndex="17" ToolTip="Sort By Name"
                                        CommandName="Sort" CommandArgument="[C].[candidatename]" Text="Candidate Name" />
                                </th>
                                <th runat="server" id="thReqCode" style="white-space: nowrap; text-align: left; width: 190px !important">
                                    JobTitle
                                </th>
                                <th style="white-space: nowrap; text-align: left;" runat="server" id="th1">
                                    Grade
                                </th>
                                <th style="white-space: nowrap; text-align: left;" runat="server" id="th4">
                                    Offer Created On
                                </th>
                                <th style="white-space: nowrap; text-align: center;" runat="server" id="th2">
                                    Over All CTC
                                </th>
                                <th style="white-space: nowrap; text-align: left;" runat="server" id="th5">
                                    Stage
                                </th>
                                <th style="white-space: nowrap; text-align: left;" runat="server" id="th6">
                                    View
                                </th>
                                <th style="text-align: center; white-space: nowrap; width: 45px !important;" id="thAction"
                                    runat="server">
                                    Action
                                </th>
                            </tr>
                            <tr>
                                <td style="padding: 0px 0px 0px 0px; border-style: none none none none">
                                </td>
                            </tr>
                            <tr id="itemPlaceholder" runat="server">
                            </tr>
                            <tr class="Pager">
                                <td runat="server" id="tdPager" colspan="8">
                                    <ucl:Pager ID="pagerControl" runat="server" EnableViewState="true" />
                                </td>
                            </tr>
                        </table>
                    </LayoutTemplate>
                    <EmptyDataTemplate>
                        <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                            <tr>
                                <td>
                                    No data was returned.
                                </td>
                            </tr>
                        </table>
                    </EmptyDataTemplate>
                    <ItemTemplate>
                        <%-- <table id="tlbTemplate" runat="server" class="Grid" cellspacing="0" border="0">--%>
                        <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'
                            align="center" style="width: 1000px;">
                            <td id="td1" runat="server">
                                <asp:Label ID="lblcandidatename" runat="server" />
                            </td>
                            <td id="tdJobCode" runat="server">
                                <asp:Label ID="lblJobTitle" runat="server" />
                            </td>
                            <td>
                                <asp:Label ID="lblGrade" runat="server" />
                            </td>
                            <td id="td2" runat="server">
                                <asp:Label ID="lblCreatedOn" runat="server" />
                            </td>
                            <td style="text-align:right !important;" >
                                <asp:Label ID="lblOfferCreated" runat="server" />
                            </td>
                            <td>
                                <asp:Label ID="lblStage" runat="server" />
                            </td>
                            <td>
                                <asp:HyperLink ID="lnkJobTitle" runat="server" Target="_blank" TabIndex="8" Style="cursor: pointer;"></asp:HyperLink>
                            </td>
                            <td id="Td3" style="overflow: inherit;" runat="server" lid="tdAction" enableviewstate="true">
                                <ul class="nav" style="margin-bottom: 0px; padding-left: 0px;">
                                    <li class="dropdown"><a data-toggle="dropdown" class="dropdown-toggle" style="width: 30px;
                                        margin-top: -15px;" href="#">
                                        <div class="container">
                                            <button class="btn btn-mini" type="button">
                                                <i class="icon icon-cog"></i><span class="caret"></span>
                                            </button>
                                        </div>
                                    </a>
                                        <ul class="dropdown-menu" style="margin-left: -120px; margin-top: -25px;">
                                            <li>
                                                <asp:HyperLink ID="hlkReqApprove" runat="server" Text="Approve"></asp:HyperLink>
                                            </li>
                                            <li>
                                                <asp:HyperLink ID="hlkReqReject" runat="server" Text="Send Back"></asp:HyperLink>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                        <%--<tr>
                            <td  runat="server" id="tdPager">
                                </td>
                            </tr>--%>
                        <%--   </table>--%>
                    </ItemTemplate>
                </asp:ListView>
            </div>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
