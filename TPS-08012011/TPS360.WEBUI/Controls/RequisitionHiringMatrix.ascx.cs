/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: RequisitionHiringMatrix.ascx.cs
    Description: This is the user control page used for member interview functionalities
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.         Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1            Jan-15-2009          Yogeesh Bhat        Defect ID: 9706; Changes made in lsvCandidateList_ItemDataBound()
    0.2            Feb-25-2009          Yogeesh Bhat        Defect ID: 9623; Changes made in Page_Load()
    0.3            Mar-03-2009          Yogeesh Bhat        Defect Id 10046, 10047: Changes made in lsvCandidateList_ItemDataBound() and SetButtonLevel()
 *  0.4            Mar-24-2009          N.Srilakshmi        Defect Id 10202; Changes made in lsvCandidateList_ItemDataBound       
 *  0.5            Mar-31-2009          A.S.Rajendra        Defect Id 10219; Changes made in SetButtonLevel();
    0.6            Apr-07-2009          Yogeesh Bhat        Defect Id:10239; Changes made in btnSubmitToClient_Click()
 *  0.7            Apr-13-2009          Sandeesh            Defect Id:10345 Changes made to display the confirmatiom message
    0.8            Apr-20-2009          Shivanand           Defect #10361;  In the method lsvCandidateList_ItemDataBound(), Hyperlink is provided for "ApplicantName" based on 
                                                                             Role.
    0.9            May-05-2009          Yogeesh Bhat        DefectId 10044; Changes made in odsHiringMatrix_Load()
    1.0            May-06-2009          Yogeesh Bhat        Defect Id: 10437, 10438 Changes made in MoveApplicantFromCurrentLevel() and odsHiringMatrix_Load()
 *  1.1            May-15-2009          Veda                Defect Id: 10435, Instead of having a whole new window open when a "Remarks" link is clicked, a small window should appear with the remarks text box. 
 *  1.2            May-21-2009          N.Srilakshmi        Defect Id: 10430; Changes made in lsvCandidateList_ItemDataBound  
    1.3            May-22-2009          Yogeesh Bhat        Defect Id: 10489; Changes made in odsHiringMatrix_Load()
    1.4            May-27-2009          Veda                Defect Id: 10435, Instead of having a whole new window open when a "Remarks" link is clicked, a small (ajax) popup should appear with the remarks text box. 
    1.5            Jun-03-2009          N.Srilakshmi        Defect Id: 10430; Changes made in lsvCandidateList_ItemDataBound  
    1.6            Jun-08-2009          Veda                Defect Id: 10535, Displyaing all three level remarks.  
    1.7            Jul-01-2009          Veda                Defect Id: 10832, Colon provided against the remarks.
 *  1.8            Sept-24-2009         Nagarathna V.B      Enha#11473;passing current user for send email method call
 *  1.9            Nov-06-2009          Rajendra            Defect Id; 11500; Code added in the method odsHiringMatrix_Load() & SaveInterviewRemarks() methods.
    2.0            Nov-26-2009          Gopala Swamy J      Defect Id:11551 ; Put if statement 
 * 2.1             Dec-5-2009           Sandeesh            Defect Id:11637 ;For displaying the remarks after saving the Remarks
 * 2.2             May-07-2010          Basavaraj A         Defect 12581 ; Added the method 'GetCandidateEducationById' to display education Details for Candidate And added the code to Display the ResumeSuource in front of the Candidate name
-------------------------------------------------------------------------------------------------------------------------------------------        
*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using TPS360.Common;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Web.UI.Helper;
using WebChart;
using System.Configuration;
using AjaxControlToolkit;
using System.Data.SqlClient;
using System.Linq;
using System.Data;
using System.Web.Configuration;

namespace TPS360.Web.UI
{
    public partial class cltRequisitionHiringMatrix : ATSBaseControl
    {
        

        #region Member Variables

        protected WebChart.ChartControl LineChart;
        SecureUrl url;
        public int MemberId;
        public int JobId;
        public int interviewLevel;
        public MemberHiringProcess memberHiringProcess;
        private bool _IsMemberMailAccountAvailable = false;
        private string CurrentJobPostingSkillNames = "";
        bool Remove = false;
        private bool isAccess = false;
        DropDownList dummyList = new DropDownList();
        public int  HiringMatrixInterviewLevel
        {
            get
            {
                if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_SELECTEDTAB]))
                {
                    return Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_SELECTEDTAB]);
                }
                return 0;
            }
        }
        public int _level = 0;
        public int CurrentLevelId
        {
            get
            {
                return _level;
            }
            set
            {
                _level = value;
            }
        }
        public string BackFrom
        {
            get
            {
                if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_MSG]))
                {
                    return Helper.Url.SecureUrl[UrlConstants.PARAM_MSG];
                }
                return string.Empty;
            }
        }
        public event Action OnMove;

        #endregion

        #region Methods

        private void HiringDetailsAdded(string MemberID,int StatusId,bool IsAdded)
        {
            txtSelectedIds.Text = "";
            var webConfig = WebConfigurationManager.OpenWebConfiguration("~");
            var offer = webConfig.AppSettings.Settings["Offer"];
            if (IsAdded)
            {
                if (StatusId > 0)
                {
                    Facade.MemberJobCart_MoveToNextLevel((hdncurretn.Value != string.Empty ? Convert.ToInt32(hdncurretn.Value) : 0), base.CurrentMember.Id, MemberID, CurrentJobPosting.Id, StatusId.ToString());
                    Facade.UpdateCandidateRequisitionStatus(CurrentJobPosting.Id, MemberID, CurrentMember.Id, StatusId);
                    MiscUtil.EventLogForCandidateStatus(EventLogForCandidate.CandidateStatusChange, CurrentJobPosting.Id, MemberID, CurrentMember.Id, offer.Value , Facade);
                    BindList();
                    RefreshCandidateCountinLeftPanel();
                    MiscUtil.ShowMessage(lblMessage, "Selected candidate(s) moved Successfully.", false);
                }
                else
                {
                    BindList();
                    MiscUtil.ShowMessage(lblMessage, "Offer Details Updated Successfully.", false);
                }
            }
            else
            {
                MiscUtil.ShowMessage(lblMessage, "Offer Details Removed Successfully.", false);
            }
        }
        private void JoiningDetailsAdded(string MemberID, int StatusId, bool IsAdded)
        {
            txtSelectedIds.Text = "";
            var webConfig = WebConfigurationManager.OpenWebConfiguration("~");
            var join = webConfig.AppSettings.Settings["Join"];
            if (IsAdded)
            {
                if (StatusId > 0)
                {
                    Facade.MemberJobCart_MoveToNextLevel((hdncurretn.Value != string.Empty ? Convert.ToInt32(hdncurretn.Value) : 0), base.CurrentMember.Id, MemberID, CurrentJobPosting.Id, StatusId.ToString());
                    Facade.UpdateCandidateRequisitionStatus(CurrentJobPosting.Id, MemberID, CurrentMember.Id, StatusId);
                    MiscUtil.EventLogForCandidateStatus(EventLogForCandidate.CandidateStatusChange, CurrentJobPosting.Id, MemberID, CurrentMember.Id, join.Value , Facade);
                    BindList();
                    RefreshCandidateCountinLeftPanel();
                    MiscUtil.ShowMessage(lblMessage, "Selected candidate(s) moved Successfully.", false);
                }
                else
                {
                    BindList();
                    MiscUtil.ShowMessage(lblMessage, "Joining Details Updated Successfully.", false);
                }
            }
            else
            {
                MiscUtil.ShowMessage(lblMessage, "Joining Details Removed Successfully.", false);
            }
        }
        private void UndoRejectDetails()
        {
            RefreshCandidateCountinLeftPanel();
        }
        private void RejectDetailsAdded(int StatusId, string MemberId)
        {
            if (StatusId > 0)
            {
               // 
                //Facade.UpdateCandidateRequisitionStatus(CurrentJobPosting.Id, MemberId, CurrentMember.Id, StatusId);
               // MiscUtil.EventLogForCandidate(EventLogForCandidate.CandidateRejected, CurrentJobPostingId , MemberId , CurrentMember.Id, Facade);
                BindList();
                RefreshCandidateCountinLeftPanel();
                MiscUtil.ShowMessage(lblMessage, "Successfully rejected selected candidate(s).", false);
            }
            else
            {
                MiscUtil.ShowMessage(lblMessage, "Rejection Details Updated Successfully.", false);
            }



            //BindList();
            //RefreshCandidateCountinLeftPanel();
            txtSelectedIds.Text = "";
            
        }
        private void SubmissionDetailsAdded(int StatusId,string MemberId)
        {
            txtSelectedIds.Text = "";
            var webConfig = WebConfigurationManager.OpenWebConfiguration("~");
            var submit = webConfig.AppSettings.Settings["Submission"];
            if (StatusId > 0)
            {
                Facade.MemberJobCart_MoveToNextLevel((hdncurretn.Value != string.Empty ? Convert.ToInt32(hdncurretn.Value) : 0), base.CurrentMember.Id, MemberId, CurrentJobPosting.Id, StatusId.ToString());
                Facade.UpdateCandidateRequisitionStatus(CurrentJobPosting.Id, MemberId, CurrentMember.Id, StatusId);
                MiscUtil.EventLogForCandidateStatus(EventLogForCandidate.CandidateStatusChange, CurrentJobPosting.Id, MemberId, CurrentMember.Id, submit.Value , Facade);
                BindList();
                RefreshCandidateCountinLeftPanel();
                MiscUtil.ShowMessage(lblMessage, "Selected candidate(s) moved Successfully.", false);
            }
            else
            {
                MiscUtil.ShowMessage(lblMessage, "Submission Details Updated Successfully.", false);
            }
        }
        private DateTime getTime(DateTime date, int duration)
        {
            if (duration > 0)
            {
                duration = duration / 60;
            }
            return date.AddMinutes(Convert.ToDouble(duration));
        }

        //private void ShowRequisitionBriefDescription()
        //{
        //    JobPosting jobPosting = CurrentJobPosting;
        //    if (jobPosting != null)
        //    {
        //        lblJobTitle.Text =MiscUtil .RemoveScript ( jobPosting.JobTitle);
        //        lblJobLocation.Text =MiscUtil .RemoveScript ( jobPosting.City) + ((!string.IsNullOrEmpty(jobPosting.City) && jobPosting.StateId > 0) ? ", " : string.Empty) + MiscUtil.GetStateNameById(jobPosting.StateId, Facade);
        //        lblJobYearOfExp.Text =MiscUtil .RemoveScript ( jobPosting.MinExpRequired.ToString());
        //        if (lblJobYearOfExp.Text.Trim() != "" && jobPosting.MaxExpRequired.ToString().Trim() != "") lblJobYearOfExp.Text += " - ";
        //        lblJobYearOfExp.Text +=MiscUtil .RemoveScript ( jobPosting.MaxExpRequired.ToString());
        //        lblJobEducation.Text = MiscUtil.SplitValues(jobPosting.RequiredDegreeLookupId, ',', Facade);
        //        lblJobSalary.Text = string.Empty;
        //        if (MiscUtil .RemoveScript ( jobPosting.PayRate) == "Open")
        //            lblJobSalary.Text =MiscUtil .RemoveScript ( jobPosting.PayRate);
        //        else
        //        {
        //            lblJobSalary.Text =MiscUtil .RemoveScript ( jobPosting.PayRate) + "  " +MiscUtil .RemoveScript ( jobPosting.PayCycle);
        //            GenericLookup _PayRateCurrencyLookup = Facade.GetGenericLookupById(jobPosting.PayRateCurrencyLookupId);
        //            if (_PayRateCurrencyLookup != null)
        //                lblJobSalary.Text = _PayRateCurrencyLookup.Name + "  " +MiscUtil .RemoveScript ( jobPosting.PayRate )+ (_PayRateCurrencyLookup.Name == "INR" ? " Lacs" : "") + " /" + (jobPosting.PayCycle == "1" ? " Hour" : jobPosting.PayCycle == "2" ? " Day" : jobPosting.PayCycle == "3" ? " Month" : " Year");
        //        }
        //        if (jobPosting.JobSkillLookUpId.Trim() != string.Empty)
        //        {
        //            string skill =MiscUtil .RemoveScript ( CurrentJobPostingSkillNames);//MiscUtil.SplitSkillValues(jobPosting.JobSkillLookUpId, '!', Facade);
        //         lblSkills.Text = skill.Length <= 50 ? skill : (skill.Substring(0, 50) + "...");
        //         lblSkills.ToolTip = skill;
        //        }
        //    }
        //}

        //private void EnableDiableButtons()
        //{
        //    bool visible = lsvCandidateList != null && lsvCandidateList.Items.Count > 0;
        //    if (hdnNextLevelName .Value .Trim ()!= "")
        //    {
        //        //btnMoveToNextLevel.Visible = visible;
        //        btnSubmitToClient.Visible = visible;
        //    }
        //    //if(hdnPreviousLevelName.Value .Trim ()!="")   btnRemoveFromThisLevel.Visible = visible;
        //    btnEmail.Visible = ddlLevels.Visible = btnMoveToNextLevel .Visible = visible;
        //}
      
        private void SetButtonLevel()
        {

            //btnSubmitToClient.Visible = true;
            HiringMatrixLevels level = Facade.HiringMatrixLevel_GetNextLevelById(HiringMatrixInterviewLevel);
            if (level != null)
            {
                //btnMoveToNextLevel.Text = "Move To " + level.Name;
                hdnNextLevelName.Value = level.Name;
            }
            HiringMatrixLevels PreLevel = Facade.HiringMatrixLevel_GetPreviousLevelById(HiringMatrixInterviewLevel);
            if (PreLevel != null) hdnPreviousLevelName.Value = PreLevel.Name;
            else hdnPreviousLevelName.Value = ""; //btnRemoveFromThisLevel.Visible = false;
            HiringMatrixLevels levels = Facade.HiringMatrixLevel_GetLastLevel();
           
            if (levels.Id == HiringMatrixInterviewLevel)
            {
               // btnMoveToNextLevel.Visible = false;
                //btnSubmitToClient.Visible = false;
            }
            else if((levels.SortingOrder==0) &&(levels.Id == HiringMatrixInterviewLevel))
            {
                //btnSubmitToClient.Visible = true;
            }

           
        }
        private void LoadHiringMatrixLevels()
        {
            IList<HiringMatrixLevels> level = Facade.GetAllHiringMatrixLevels();//.Where(x => x.Id != HiringMatrixInterviewLevel).ToList();

            ddlLevels.DataSource = level;
            ddlLevels.DataTextField = "Name";
            ddlLevels.DataValueField = "Id";
            ddlLevels.DataBind();
            ddlLevels = (DropDownList)MiscUtil.RemoveScriptForDropDown(ddlLevels);
            //ddlLevels.Items.Add(new ListItem("Rejected", "Reject"));

            //if (hdnNextLevelName.Value.Trim() != string.Empty)
            //{
            //    HiringMatrixLevels lev = Facade.HiringMatrixLevel_GetNextLevelById(HiringMatrixInterviewLevel);
            //    if (lev != null) ddlLevels.SelectedValue = lev.Id.ToString();
            //}
            //else
            //{
            //    HiringMatrixLevels lev = Facade.HiringMatrixLevel_GetPreviousLevelById(HiringMatrixInterviewLevel);
            //    if (lev != null) ddlLevels.SelectedValue = lev.Id.ToString();
            //}


        }

        protected void ddlLevels_SelectedIndexChanged(object sender, EventArgs e)
        {
            hdnMoving.Value = ddlLevels.SelectedValue;
            btnMoveToNextLevel_Click(sender, e);

            ControlHelper.SelectListByValue(ddlLevels, odsHiringMatrix.SelectParameters["SelectionStepLookupId"].DefaultValue);
        }
        private void PrepareView()
        {
            //ShowRequisitionBriefDescription();
           //SetButtonLevel();
           LoadHiringMatrixLevels();
        }
       
        private void UncheckAllCheckBox()
        {
            if (lsvCandidateList != null)
            {
                foreach (ListViewDataItem item in lsvCandidateList.Items)
                {
                    CheckBox chkCandidate = (CheckBox)item.FindControl("chkCandidate");
                    if (chkCandidate != null) chkCandidate.Checked = false;
                }
            }
        }
       
        private int getNumberOfSelectedCandidates()
        {
            char[] delim = { ',' };
            string[] canArr = txtSelectedIds.Text.Split(delim, StringSplitOptions.RemoveEmptyEntries);
            return canArr == null ? 0 : canArr.Length;
        }

        private void SubmitToClient(string MemberIds)
        {
            if (base.CurrentJobPosting.ClientId > 0)
            {
                Company CurrentCompany = Facade.GetCompanyById(base.CurrentJobPosting.ClientId);

                if (CurrentCompany != null)
                {
                    if (CurrentCompany.PrimaryContact.Email == null)
                    {
                        string strInterval = string.Empty;

                        AlwaysVisibleControlExtender avce = new AlwaysVisibleControlExtender();
                        avce.TargetControlID = lblMessage.ID;
                        avce.ScrollEffectDuration = .1f;

                        lblMessage.ForeColor = System.Drawing.Color.Black;

                        lblMessage.Font.Bold = false;

                        lblMessage.Text = "<div id=\"divParent\" style=\"width: 100%; padding: 5px;\">";
                        lblMessage.Text += "<div style=\"float: left; width: 80%; text-align: left;\">";
                        lblMessage.Text += "<img src=\"../Images/messagebox_info.png\" />";
                        lblMessage.Text += "  " + "Please add primary contact for <b>" + CurrentCompany.CompanyName + "</b> before submission";
                        lblMessage.Text += "</div>";

                        lblMessage.Text += "<div style=\"float: right; width: 18%; text-align: right; padding-right: 10px; white-space: nowrap\">";
                        lblMessage.Text += " <a href=\"javascript:void(0)\" onclick=\"javascript:hideMeClick('" + lblMessage.ClientID + "')\" title=\"Hide Me\">Hide Me</a></div>";
                        lblMessage.Text += "</div>";
                        strInterval = ConfigurationManager.AppSettings["AlertInterval"].ToString();
                        ScriptManager.RegisterClientScriptBlock(lblMessage, typeof(MiscUtil), "MiscUtil",
                           "window.setTimeout(function(){hideMeClick('" + lblMessage.ClientID + "');}, '" + strInterval + "')", true);

                        lblMessage.ForeColor = System.Drawing.Color.Red;
                        lblMessage.CssClass = "CommonAlertMessage";

                    }
                    else
                    {
                        string applicantIds = MemberIds;
                        if (!string.IsNullOrEmpty(applicantIds))
                            Helper.Url.Redirect(UrlConstants.ATS.ATS_SUBMITTOCLIENT, string.Empty, UrlConstants.PARAM_HIRING_MATRIX_INTERVIEW_LAVEL, (hdncurretn.Value).ToString(), UrlConstants.PARAM_JOB_ID, base.CurrentJobPostingId.ToString(), UrlConstants.PARAM_APPLICANT_ID, applicantIds);
                        else
                            MiscUtil.ShowMessage(lblMessage, "Please select at least one candidate", true);
                    }
                }
                else
                {
                    if (SiteSetting[DefaultSiteSetting.ApplicationEdition.ToString()].ToString() == UIConstants.INTERNAL_HIRING)
                    {
                        MiscUtil.ShowMessage(lblMessage, "Please associate the Requisition with a Department before submitting", true);
                    }
                    else MiscUtil.ShowMessage(lblMessage, "Please associate the Requisition with a Client before submitting", true);
                }
            }
            else
            {
                if (SiteSetting[DefaultSiteSetting.ApplicationEdition.ToString()].ToString() == UIConstants.INTERNAL_HIRING)
                {
                    MiscUtil.ShowMessage(lblMessage, "Please associate the Requisition with a Department before submitting", true);
                }
                else
                {
                    MiscUtil.ShowMessage(lblMessage, "Please associate the Requisition with a Client before submitting", true);
                }
            }
        }

        private string HighLightText(string OriginalText, string Compare)
        {
            if (Compare != "" && OriginalText != "")
            {
                if (OriginalText.ToLower().Contains(Compare.ToLower()))
                {
                    OriginalText = OriginalText.ToLower().Replace(Compare.ToLower(), "<span class=highlight>" + Compare + "</span>");
                    return OriginalText;

                }
            }
            return OriginalText;
        }

        private string HighLightExactText(string OriginalText, string Compare)
        {
            if (OriginalText != null)
            {
                if (Compare.ToLower() == OriginalText.ToLower() && OriginalText.Trim() != "")
                {
                    OriginalText = "<span class=highlight>" + OriginalText + "</span>";
                    return OriginalText;
                }
            }
            return OriginalText;
        }

        private string HighLightSkillName(string text)
        {
            char[] delim = { ',' };
            string[] oriArr = CurrentJobPostingSkillNames.Trim().Split(delim, StringSplitOptions.RemoveEmptyEntries);
            string[] curArr = text.Split(',');
            string ret = string.Empty;
            if (oriArr.Length > 0 && curArr.Length > 0)
            {
                foreach (string s in curArr)
                {
                    if (ret != string.Empty) ret += ",";
                    if (oriArr.Contains(s.Trim()))
                    {
                        ret += "<span class=highlight>" + s + "</span>";
                    }
                    else ret += s;
                }

            }
            else
                return text;
            return ret;
        }


        public void SetTabText()
        {
            //Infragistics.WebUI.UltraWebTab.UltraWebTab uwtRequisitionHiringMatrixNavigationTopMenu = (Infragistics.WebUI.UltraWebTab.UltraWebTab)this.Page.Master.FindControl("cphHomeMaster").FindControl("ucRequisitionHiringMatrixNavigationTopMenu").FindControl("uwtRequisitionHiringMatrixNavigationTopMenu");


            int[] hiringMatrixCount = Facade.GetHiringMatrixMemberCount(base.CurrentJobPostingId);
            IList<HiringMatrixLevels> hiringMatrixLevels = Facade.GetAllHiringMatrixLevels();
            for (int i = 0; i < hiringMatrixLevels.Count; i++)
            {
                try
                {
                    //uwtRequisitionHiringMatrixNavigationTopMenu.Tabs[i].Text = hiringMatrixLevels[i].Name + " (" + hiringMatrixCount[i].ToString() + ")";
                }
                catch
                {
                }
            }
            //uwtRequisitionHiringMatrixNavigationTopMenu.Tabs[hiringMatrixLevels.Count].Text = "Rejected (" + Facade.GetRejectCandidateCount(base .CurrentJobPostingId ) + ")";
        }

        private void PlaceUpDownArrow()
        {
            try
            {

                LinkButton lnk = (LinkButton)lsvCandidateList.FindControl(txtSortColumn.Text);
                ImageButton imgUp = new ImageButton();
                imgUp.ID = "imgArrow";
                imgUp.Style.Add("disploay", "inline");
                imgUp.Style.Add("float", "right");
                lnk.Style.Add("float", "left");
                lnk.Style.Add("disploay", "inline");
                imgUp.Enabled = false;
                if (lnk.CommandArgument.Contains("Experience"))
                {
                    if (txtSortOrder.Text == "ASC") imgUp.ImageUrl = "../Images/downarrow-header.gif";
                    else imgUp.ImageUrl = "../Images/uparrow-header.gif";
                }
                else
                {
                    if (txtSortOrder.Text == "ASC") imgUp.ImageUrl = "../Images/uparrow-header.gif";
                    else imgUp.ImageUrl = "../Images/downarrow-header.gif";
                }
                ImageButton im = (ImageButton)lnk.Parent.FindControl("imgArrow");
                if (im == null) lnk.Parent.Controls.Add(imgUp);

            }
            catch
            {
            }

        }

        private bool CheckSelectedList(int ID)
        {
            char[] del = { ',' };
            string[] arrID = txtSelectedIds.Text.Split(del, StringSplitOptions.RemoveEmptyEntries);
            foreach (string s in arrID)
            {
                int d = 0;
                Int32.TryParse(s, out d);
                if (d == ID)
                {
                    return true;
                }
            }
            return false;
        }

        protected void lnkReqStatus_Click(object sender, EventArgs e)
        {

            LinkButton lnkButton = (LinkButton)sender;
            switch (lnkButton.CommandArgument)
            {
                case "ChangeStatus":
                    mpeChangeStatus.Enabled = true;
                    uclTemplate.ModalTitle = base.CurrentJobPosting.JobTitle + " - Change Requisition Status";
                    //lblJobStatusHeader.Text =
                    mpeChangeStatus.Show();
                    break;
                case "Notes":
                    mpeNotes.Enabled = true;
                   uclTemplateNotes .ModalTitle = base.CurrentJobPosting.JobTitle + " - Notes";
                    uclNotes.Bind();
                    mpeNotes.Show();
                    break;
                case "AssignedManagers":
                    mpeManagers.Enabled = true;
                  uclTemplateManagers .ModalTitle  = base.CurrentJobPosting.JobTitle + " - Assigned Team";
                  uclManagers.JobPostingId = CurrentJobPostingId;
                    mpeManagers.Show();
                    break;
                case "HiringLog":
                    ASP.controls_candidateactionlog_ascx actionLog = (ASP.controls_candidateactionlog_ascx)this.Page.Master.FindControl("cphHomeMaster").FindControl("ucRequisitionHiringMatrixNavigationTopMenu").FindControl("uclTemplateActionLog").FindControl("uclActionLog");
                    AjaxControlToolkit.ModalPopupExtender ext = (AjaxControlToolkit.ModalPopupExtender)this.Page.Master.FindControl("cphHomeMaster").FindControl("ucRequisitionHiringMatrixNavigationTopMenu").FindControl("mpeActionLog");
                    ASP.controls_modaltemplate_ascx modaltemplate = (ASP.controls_modaltemplate_ascx)this.Page.Master.FindControl("cphHomeMaster").FindControl("ucRequisitionHiringMatrixNavigationTopMenu").FindControl("uclTemplateActionLog");
                    modaltemplate.ModalTitle = actionLog.getModalTitle();
                    actionLog.MemberID = "";
                    
                    ext.Enabled = true;
                    ext.Show();
                    break;
            }
        }
       
        #endregion

        #region Events
        
        public void MessageAnswered(object sender, ConfirmationWindow .MsgBoxEventArgs e )
        {
            if (e.Answer == ConfirmationWindow.enmAnswer.OK)
            {
                if (txtSelectedIds.Text.Trim() != "")
                {
                  ASP .controls_confirmationwindow_ascx msg = (ASP .controls_confirmationwindow_ascx )sender;
                  if (msg.Args == "undo")
                  {
                      Facade.MemberJobCart_MoveToNextLevel((hdncurretn.Value != string.Empty ? Convert.ToInt32(hdncurretn.Value) : 0), base.CurrentMember.Id, txtSelectedIds .Text .Trim (), CurrentJobPosting.Id, hdnMoving .Value );
                      Facade.DeleteRejectCandidateByMemberIdAndJobPostingID(Int32.Parse(txtSelectedIds .Text .Trim ()), CurrentJobPostingId);
                      RefreshCandidateCountinLeftPanel();
                      hdnMoving.Value = "";
                      txtSelectedIds.Text = "";
                      BindList();
                  }
                  else
                  {
                      ASP.controls_rejectdetails_ascx candidate = (ASP.controls_rejectdetails_ascx)this.Page.Master.FindControl("cphHomeMaster").FindControl("ucRequisitionHiringMatrixNavigationTopMenu").FindControl ("uclTemplateReject").FindControl("uclRejectCandidate");
                      ASP.controls_modaltemplate_ascx modalTemlate = (ASP.controls_modaltemplate_ascx)this.Page.Master.FindControl("cphHomeMaster").FindControl("ucRequisitionHiringMatrixNavigationTopMenu").FindControl("uclTemplateReject");
                      AjaxControlToolkit.ModalPopupExtender ext = (AjaxControlToolkit.ModalPopupExtender)this.Page.Master.FindControl("cphHomeMaster").FindControl("ucRequisitionHiringMatrixNavigationTopMenu").FindControl("mpeRejectCandidate");
                      candidate.MemberID = txtSelectedIds.Text;
                      candidate.HiringMatrixLevel = hdncurretn.Value != string.Empty ? Convert.ToInt32(hdncurretn.Value) : 0;
                      candidate.JobPostingId = CurrentJobPostingId;
                      //Label lblRejectTitle = (Label)this.Page.Master.FindControl("cphHomeMaster").FindControl("ucRequisitionHiringMatrixNavigationTopMenu").FindControl("lblRejectTitle");
                      //lblRejectTitle.Text =
                      candidate.StatusId =  Convert .ToInt32 ( hdnMoving.Value==""?"0":hdnMoving .Value );
                     modalTemlate .ModalTitle = candidate.getModalTitle();
                      ext.Enabled = true;
                      ext.Show();
                      txtSelectedIds.Text = "";
                      BindList();
                  }
                    
                }
                else MiscUtil.ShowMessage(lblMessage, "Please select at least one candidate", true);
                
            }
       }
        public void FillDummyList()
        {
           IList <HiringMatrixLevels> levels = new List <HiringMatrixLevels>();
            levels = Facade.GetAllHiringMatrixLevels();
            //HiringMatrixLevels l = new HiringMatrixLevels();
            //l.Id = -1;
            //l.Name = "Rejected";
            //levels.Add(l);
            dummyList.DataSource = levels;
            dummyList.DataTextField = "Name";
            dummyList.DataValueField = "Id";
            dummyList.DataBind();
            dummyList = (DropDownList)MiscUtil.RemoveScriptForDropDown(dummyList);
            //dummyList.Items.Add(new ListItem("Rejected", "Reject"));

        }
        protected void Page_Load(object sender, EventArgs e)
        {
           
            //ddlLevels.Attributes.Add("onmouseover", "this.title=this.options[this.selectedIndex].title");
            //uclHiringDetails.HiringDetailsAdded += HiringDetailsAdded;
            //submission.JobPostingId = CurrentJobPostingId;
            uclRejection.UndoRejectDetails += UndoRejectDetails;
            this.Page.MaintainScrollPositionOnPostBack = true;
            odsHiringMatrix.SelectParameters["JobPostingId"].DefaultValue = CurrentJobPosting.Id.ToString();
            _IsMemberMailAccountAvailable = MiscUtil.IsValidMailSetting(Facade, base.CurrentMember.Id);
            CurrentJobPostingSkillNames = Facade.JobPosting_GetRequiredSkillNamesByJobPostingID(CurrentJobPostingId);
            SecureUrl url = UrlHelper.BuildSecureUrl(UrlConstants.ApplicationBaseUrl + "Dashboard/" + UrlConstants.Dashboard.NEWMAIL, "", UrlConstants.PARAM_JOB_ID, base.CurrentJobPostingId.ToString(), UrlConstants.PARAM_PAGE_FROM, "JobDetail");
            uclConfirm.MsgBoxAnswered += MessageAnswered;
            FillDummyList();
            
            ArrayList AList = Facade.GetAllMemberPrivilegeIdsByMemberId(CurrentMember.Id);
            if (AList.Contains(403))
                isAccess = true;
            else
                isAccess = false;
                if (!Page.IsPostBack)
                {
                    lblCurrentJobPostingID.Text = CurrentJobPostingId.ToString();
                    //hdnJobPostingId.Text  = CurrentJobPostingId.ToString();
                    PrepareView();
                    //AjaxControlToolkit.Utility.SetFocusOnLoad(btnMoveToNextLevel);
                    Session["PreciseSearch"] = null;   
                    string message = Helper.Url.SecureUrl[UrlConstants.PARAM_MSG]; 
                    if (!StringHelper.IsBlank(message))
                    {
                        if(message !="BackFromSubmit")
                        MiscUtil.ShowMessage(lblMessage, message, false);
                    }
                    else if (Session["SentStatus"] != null)
                    {
                        message = Session["SentStatus"].ToString();
                        MiscUtil.ShowMessage(lblMessage, message, false);
                        Session["SentStatus"] = null;
                    }
                    if (Session["Error"] != "")
                    {
                        if (Convert.ToInt32(Session["Error"]) == 1)
                        {
                            MiscUtil.ShowMessage(lblMessage, "Remarks saved successfully.", false);
                        }
                        else if (Convert.ToInt32(Session["Error"]) == 2)
                        {
                            MiscUtil.ShowMessage(lblMessage, "Remarks updated successfully.", false);
                        }
                        Session.Remove("Error");
                    }
                    txtSortColumn.Text = "lnkHeaderRelevance";
                    txtSortOrder.Text = "DESC";
                    PlaceUpDownArrow();
                }
                else
                {
                    MiscUtil.LoadAllControlState(Request, this, true);      
                    Session["PreciseSearch"] = null;   
                }
            string pagesize = "";
            pagesize = (Request.Cookies["HiringMatrixRowPerPage"] == null ? "" : Request.Cookies["HiringMatrixRowPerPage"].Value); ;
              ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvCandidateList  .FindControl("pagerControl");
              if (PagerControl != null)
              {
                  DataPager pager = (DataPager)PagerControl.FindControl("pager");
                  if (pager != null)  pager.PageSize = Convert.ToInt32(pagesize == "" ? "20" : pagesize);
              }
              //lnkReqTitle.Text = CurrentJobPosting.JobTitle+" - "+CurrentJobPosting .JobPostingCode ;
              ControlHelper.SetHyperLink(lnkReqTitle, UrlConstants.Requisition.JOB_POSTING_INTERNAL_PREVIEW_PAGE, string.Empty, CurrentJobPosting.JobTitle+" - "+CurrentJobPosting .JobPostingCode  , UrlConstants.PARAM_JOB_ID, StringHelper.Convert(CurrentJobPostingId ));
               this.Page.Title = hdnPageTitle.Value;
              if (!IsPostBack)
              {
                  hdnCurname.Value = "All";
                  this.Page.Title = CurrentJobPosting.JobTitle + " - " + CurrentJobPosting.JobPostingCode;
                  hdnPageTitle.Value = this.Page.Title;
                  IList<HiringMatrixLevels> level = Facade.GetAllHiringMatrixLevelsWithCount(CurrentJobPostingId); ;
                 // hdnRejectCount.Value = Facade.GetRejectCandidateCount(CurrentJobPostingId).ToString();
                  hdnCount.Value = level.Sum(x => x.Count).ToString();
                  HiringMatrixLevels l1 = new HiringMatrixLevels();
                  l1.Id = 0;
                  l1.Name = "All";
                  l1.Count = Convert.ToInt32(hdnCount.Value);// Convert.ToInt32(hdnRejectCount.Value) + 
                  level.Insert(0, l1);
                  //HiringMatrixLevels l2 = new HiringMatrixLevels();
                  //l2.Id = -1;
                  //l2.Name = "Rejected";
                  //l2.Count = Convert.ToInt32(hdnRejectCount.Value);
                  //level.Add(l2);
                  lvNavigationItems.DataSource = level;// Facade.GetAllHiringMatrixLevelsWithCount(CurrentJobPostingId);
                  lvNavigationItems.DataBind();
                  hdnCount.Value = "0";
                 // hdnRejectCount.Value = "0";
                  //dvReje .Visible =false ;
                  if (BackFrom == "BackFromSubmit" && HiringMatrixInterviewLevel > 0)
                  {
                      odsHiringMatrix.SelectParameters["JobPostingId"].DefaultValue = CurrentJobPosting.Id.ToString();
                      odsHiringMatrix.SelectParameters["SelectionStepLookupId"].DefaultValue = HiringMatrixInterviewLevel.ToString();
                      string LevelName = string.Empty;
                      foreach (ListViewDataItem item in lvNavigationItems.Items)
                      {
                          LinkButton button = (LinkButton)item.FindControl("lnkLeftMenuItem");
                          HiddenField hnSelectedLevel = (HiddenField)item.FindControl("hnSelectedLevel");
                          HtmlGenericControl spName = (HtmlGenericControl)item.FindControl("spName");
                          if (hnSelectedLevel.Value == HiringMatrixInterviewLevel.ToString())
                          {
                              button.CssClass = "active level";
                              button.Enabled = false;
                              LevelName = spName.InnerText;
                          }
                          else
                          {
                              button.CssClass = "level";
                              button.Enabled = true;
                          }
                          hdnCurname.Value = LevelName;
                          this.Page.Title = LevelName + " - " + CurrentJobPosting.JobTitle + " - " + CurrentJobPosting.JobPostingCode;
                          hdnPageTitle.Value = this.Page.Title;
                      }
                  }
              
              }
              
        }

        private void RefreshCandidateCountinLeftPanel()
        {
            IList<HiringMatrixLevels> level = Facade.GetAllHiringMatrixLevelsWithCount(CurrentJobPostingId); ;
          //  hdnRejectCount.Value = Facade.GetRejectCandidateCount(CurrentJobPostingId).ToString();
            hdnCount.Value = (level.Sum(x => x.Count)).ToString ();// + Convert .ToInt32 (hdnRejectCount .Value )).ToString ();

            foreach (ListViewDataItem item in lvNavigationItems.Items)
            {
                LinkButton lnkLeftMenuItem = (LinkButton)item.FindControl("lnkLeftMenuItem");
                HtmlGenericControl spCount = (HtmlGenericControl)item.FindControl("spCount");
                switch (lnkLeftMenuItem.CommandArgument)
                {
                    case "0":
                        spCount.InnerText = "("+hdnCount.Value +")";
                        break;
                    //case "-1":
                    //    spCount.InnerText = "("+hdnRejectCount.Value +")";
                    //    break;
                    default :
                        var count = level.Where(x => x.Id == Convert.ToInt32(lnkLeftMenuItem.CommandArgument));
                        if (count.Count() > 0) spCount.InnerText ="(" + count.ToList()[0].Count.ToString () +")";
                        break;
                }

               
            }
            hdnCount.Value = "0";
           // hdnRejectCount.Value = "0";
        }
        public void lnkLeftMenuItem_Click(object sender, EventArgs e)
        {
            LinkButton link = new LinkButton();
            link = (LinkButton)sender;
            string LevelName = string.Empty;
            
            //LevelName = spName.InnerText;
            foreach (ListViewDataItem item in lvNavigationItems.Items)
            {
                LinkButton button = (LinkButton)item.FindControl("lnkLeftMenuItem");
                HiddenField hnSelectedLevel = (HiddenField)item.FindControl("hnSelectedLevel");
                HtmlGenericControl spName = (HtmlGenericControl)item.FindControl ("dvspanName").FindControl("spName");
                if (hnSelectedLevel.Value == link .CommandArgument )
                {
                    button.CssClass = "active level";
                    button.Enabled = false;
                    LevelName = spName.InnerText;
                }
                else
                {
                    button.CssClass = "level";
                    button.Enabled = true;
                }
                //button.CssClass = "level";
                //button.Enabled = true;
            }
            //link.CssClass = "active level";
            //link.Enabled = false;
            odsHiringMatrix.SelectParameters["JobPostingId"].DefaultValue = CurrentJobPosting.Id.ToString();
            odsHiringMatrix.SelectParameters["SelectionStepLookupId"].DefaultValue = link.CommandArgument;
            hdncurretn .Value   = link.CommandArgument;
            ControlHelper.SelectListByValue(ddlLevels, link.CommandArgument);

            if (LevelName == "Rejected")
            {
                dvbulkAction.Visible = false;
                dvhirng.Visible = false ;
                uclRejection.showList();
            }
            else
            {
                dvbulkAction.Visible = true;
                dvhirng.Visible = true;
                uclRejection.hideList();
            }
            hdnCurname.Value = LevelName;
            this.Page.Title = LevelName  +" - " + CurrentJobPosting.JobTitle + " - " + CurrentJobPosting.JobPostingCode;
            hdnPageTitle.Value = this.Page.Title;
            txtSelectedIds.Text = "";

             ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvCandidateList.FindControl("pagerControl");
             if (PagerControl != null)
             {
                 DataPager pager = (DataPager)PagerControl.FindControl("pager");
                 if (pager != null)
                 {

                     pager.SetPageProperties(0, pager.MaximumRows, true);
                 }
             }
        }
        //public void lnkAll_Click(object sender, EventArgs e)
        //{
        //    LinkButton link = new LinkButton();
        //    link = (LinkButton)sender;
        //    link.CssClass = "active level";
        //    odsHiringMatrix.SelectParameters["JobPostingId"].DefaultValue = CurrentJobPosting.Id.ToString();
        //    odsHiringMatrix.SelectParameters["SelectionStepLookupId"].DefaultValue = "0";
        //    dvhirng.Visible = true ;
        //    dvReje.Visible = false ;
        //}
        //public void lnkRejected_Click(object sender, EventArgs e)
        //{
        //    LinkButton link = new LinkButton();
        //    link = (LinkButton)sender;
        //    link.CssClass = "active level";
        //    dvhirng.Visible = false;
        //    dvReje.Visible = true;
        //    uclRejection.RejectLevel = "Rejected";
        //}
        public void ddlStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList list = new DropDownList();
            list = (DropDownList)sender;
            var row = (ListViewDataItem )list.NamingContainer;
            int Id = Convert.ToInt32(((HiddenField)row.FindControl("hfCandidateId")).Value);
            int LevelId = Convert.ToInt32(((HiddenField)row.FindControl("hdnCurrentlevelId")).Value);
            string s = ((HiddenField)row.FindControl("hfstatuschangeId")).Value;
            int statusId = 0;
            hdncurretn.Value = LevelId.ToString();
            statusId = Convert.ToInt32(((HiddenField)row.FindControl("hfstatuschangeId")).Value);
            ControlHelper.SelectListByValue(list, statusId.ToString());
            if (list .SelectedItem .Text  == "Rejected")
            {
                hdnMoving.Value = list.SelectedValue;
                txtSelectedIds.Text = Id.ToString();
                hdnRemove.Value = "Remove";
                uclConfirm.AddMessage("Are you sure that you want to remove this candidate  from the Hiring Matrix?", ConfirmationWindow.enmMessageType.Attention, true, true,"");
                {
                }
            }
            else
            {

                
                //statusId = ((dr)row.FindControl("ddlStatus")).Value;
                if (Id > 0)
                {
                    ControlHelper.SelectListByValue(list, statusId.ToString());
                    var webConfig = WebConfigurationManager.OpenWebConfiguration("~");
                    var submit = webConfig.AppSettings.Settings["Submission"];
                    var offer = webConfig.AppSettings.Settings["Offer"];
                    var join = webConfig.AppSettings.Settings["Join"];
                    if (list.SelectedItem.Text == submit.Value)
                    {
                        ASP.controls_submissiondetails_ascx submission = (ASP.controls_submissiondetails_ascx)this.Page.Master.FindControl("cphHomeMaster").FindControl("ucRequisitionHiringMatrixNavigationTopMenu").FindControl("uclTemplateSubmissions").FindControl("uclSubmissionDetails");
                        AjaxControlToolkit.ModalPopupExtender ext = (AjaxControlToolkit.ModalPopupExtender)this.Page.Master.FindControl("cphHomeMaster").FindControl("ucRequisitionHiringMatrixNavigationTopMenu").FindControl("mpeSubmissionDetails");
                        submission.JobPostingId = CurrentJobPostingId;
                        submission.MemberID = Id.ToString();
                        submission.StatusId = statusId;

                        ext.Enabled = true;
                        ext.Show();
                    }
                    else if (list.SelectedItem.Text == offer.Value )
                    {
                        ASP.controls_hiringdetails_ascx offered = (ASP.controls_hiringdetails_ascx)this.Page.Master.FindControl("cphHomeMaster").FindControl("ucRequisitionHiringMatrixNavigationTopMenu").FindControl("uclTemplateHiringDetails").FindControl("uclHiringDetails");
                        AjaxControlToolkit.ModalPopupExtender ext = (AjaxControlToolkit.ModalPopupExtender)this.Page.Master.FindControl("cphHomeMaster").FindControl("ucRequisitionHiringMatrixNavigationTopMenu").FindControl("mpeHiringDetails");
                       
                        offered.JobPostingId = CurrentJobPostingId;
                        offered.MemberID = Id.ToString();
                        offered.StatusId = statusId;
                        ext.Enabled = true;
                        ext.Show();
                    }
                    else if (list.SelectedItem.Text == join.Value )
                    {
                        ASP.controls_joiningdetails_ascx joined = (ASP.controls_joiningdetails_ascx)this.Page.Master.FindControl("cphHomeMaster").FindControl("ucRequisitionHiringMatrixNavigationTopMenu").FindControl ("uclTemplateJoining").FindControl("uclJoiningDetails");
                        AjaxControlToolkit.ModalPopupExtender ext = (AjaxControlToolkit.ModalPopupExtender)this.Page.Master.FindControl("cphHomeMaster").FindControl("ucRequisitionHiringMatrixNavigationTopMenu").FindControl("mpeJoiningDetails");
                        joined.JobPostingId = CurrentJobPostingId;
                        joined.MemberID = Id.ToString();
                        joined.StatusId = statusId;
                        ext.Enabled = true;
                        ext.Show();
                    }
                    else
                    {
                        if (hdncurretn.Value == "-1")
                        {
                            txtSelectedIds.Text = Id.ToString();
                            hdnMoving.Value = statusId.ToString();
                            uclConfirm.AddMessage("Are you sure that you undo the rejection for this candidate?", ConfirmationWindow.enmMessageType.Attention, true, true, "undo");
                            {
                            }
                        }
                        else
                        {
                            Facade.MemberJobCart_MoveToNextLevel((hdncurretn.Value != string.Empty ? Convert.ToInt32(hdncurretn.Value) : 0), base.CurrentMember.Id, Id.ToString(), CurrentJobPosting.Id, statusId.ToString());
                           // Facade.UpdateCandidateRequisitionStatus(CurrentJobPosting.Id, Id.ToString(), CurrentMember.Id, statusId);
                            MiscUtil.EventLogForCandidateStatus(EventLogForCandidate.CandidateStatusChange, CurrentJobPostingId, Id.ToString(), CurrentMember.Id, list.SelectedItem.Text, Facade);
                            BindList();
                            RefreshCandidateCountinLeftPanel();
                            MiscUtil.ShowMessage(lblMessage, MiscUtil.GetMemberNameById(Id, Facade) + " successfully moved to " + list.SelectedItem.Text, false);
                        }
                    }
                    //foreach (ListViewDataItem item in lsvCandidateList.Items)
                    //{
                    //    DropDownList ddlstatus = (DropDownList)item.FindControl("ddlStatus");
                    //    ddlstatus.SelectedValue = HiringMatrixInterviewLevel.ToString();
                    //}
                }
            }
            
        }
        protected void btnEmail_Click(object sender, EventArgs e)
        {
            string applicantIds = txtSelectedIds.Text;
            if (applicantIds != string.Empty)
            {
                SecureUrl url = UrlHelper.BuildSecureUrl(UrlConstants.ApplicationBaseUrl + "Dashboard/" + UrlConstants.Dashboard.NEWMAIL, "", UrlConstants.PARAM_JOB_ID, base.CurrentJobPostingId.ToString(), UrlConstants.PARAM_PAGE_FROM, "HiringMatrix", UrlConstants.PARAM_SELECTED_IDS, applicantIds);
                UncheckAllCheckBox();
                txtSelectedIds.Text = "";
                ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "NewMail", "<script>btnMewMail_Click('" + url + "');</script>", false);
                
            }
            else
            {
                MiscUtil.ShowMessage(lblMessage, "Please select at least one candidate", true);
            }
        }
     
        private void  BindList()
        {
            lsvCandidateList.Items.Clear();
            lsvCandidateList.DataBind();
           // SetTabText();
           // EnableDiableButtons();
        }
        protected void btnMoveToNextLevel_Click(object sender, EventArgs e)
        {
            if (txtSelectedIds.Text.Trim() != "")
            {
                var webConfig = WebConfigurationManager.OpenWebConfiguration("~");
                var submit = webConfig.AppSettings.Settings["Submission"];
                var offer = webConfig.AppSettings.Settings["Offer"];
                var join = webConfig.AppSettings.Settings["Join"];
                if (ddlLevels.SelectedItem.Text == submit.Value)
                {
                    ASP.controls_submissiondetails_ascx submission = (ASP.controls_submissiondetails_ascx)this.Page.Master.FindControl("cphHomeMaster").FindControl("ucRequisitionHiringMatrixNavigationTopMenu").FindControl("uclTemplateSubmissions").FindControl("uclSubmissionDetails");
                    AjaxControlToolkit.ModalPopupExtender ext = (AjaxControlToolkit.ModalPopupExtender)this.Page.Master.FindControl("cphHomeMaster").FindControl("ucRequisitionHiringMatrixNavigationTopMenu").FindControl("mpeSubmissionDetails");
                    submission.BulkAction = "BulkAction";
                    submission.JobPostingId = CurrentJobPostingId;
                    submission.MemberID = txtSelectedIds.Text;

                    submission.StatusId = Convert.ToInt32(ddlLevels.SelectedValue);
                    ext.Enabled = true;
                    ext.Show();
                }
                else if (ddlLevels.SelectedItem.Text == offer.Value)
                {
                    ASP.controls_hiringdetails_ascx offered = (ASP.controls_hiringdetails_ascx)this.Page.Master.FindControl("cphHomeMaster").FindControl("ucRequisitionHiringMatrixNavigationTopMenu").FindControl("uclTemplateHiringDetails").FindControl("uclHiringDetails");
                    AjaxControlToolkit.ModalPopupExtender ext = (AjaxControlToolkit.ModalPopupExtender)this.Page.Master.FindControl("cphHomeMaster").FindControl("ucRequisitionHiringMatrixNavigationTopMenu").FindControl("mpeHiringDetails");
                  
                    offered.JobPostingId = CurrentJobPostingId;
                    offered.BulkAction = "BulkAction";
                    offered.MemberID = txtSelectedIds.Text;
                    offered.StatusId = Convert.ToInt32(ddlLevels.SelectedValue);
                    ext.Enabled = true;
                    ext.Show();
                }
                else if (ddlLevels.SelectedItem.Text == join.Value)
                {
                    ASP.controls_joiningdetails_ascx joined = (ASP.controls_joiningdetails_ascx)this.Page.Master.FindControl("cphHomeMaster").FindControl("ucRequisitionHiringMatrixNavigationTopMenu").FindControl("uclTemplateJoining").FindControl("uclJoiningDetails");
                    AjaxControlToolkit.ModalPopupExtender ext = (AjaxControlToolkit.ModalPopupExtender)this.Page.Master.FindControl("cphHomeMaster").FindControl("ucRequisitionHiringMatrixNavigationTopMenu").FindControl("mpeJoiningDetails");
                    joined.JobPostingId = CurrentJobPostingId;
                    joined.BulkAction = "BulkAction";
                    joined.MemberID = txtSelectedIds.Text;
                    joined.StatusId = Convert.ToInt32(ddlLevels.SelectedValue);
                    ext.Enabled = true;
                    ext.Show();
                }
                else if (ddlLevels.SelectedItem.Text == "Rejected")
                {
                    btnRemove_Click(sender, e);
                }
                else
                {
                    Facade.MemberJobCart_MoveToNextLevel((hdncurretn .Value !=string .Empty ? Convert .ToInt32 (hdncurretn .Value ):0) , base.CurrentMember.Id, txtSelectedIds.Text, CurrentJobPosting.Id, ddlLevels.SelectedValue);
                    Facade.UpdateCandidateRequisitionStatus(CurrentJobPosting.Id, txtSelectedIds.Text, CurrentMember.Id, Convert.ToInt32(ddlLevels.SelectedValue));
                    MiscUtil.EventLogForCandidateStatus(EventLogForCandidate.CandidateStatusChange, CurrentJobPostingId, txtSelectedIds.Text, CurrentMember.Id, ddlLevels.SelectedItem.Text, Facade);
                  
                    MiscUtil.ShowMessage(lblMessage, "Successfully moved " + getNumberOfSelectedCandidates() + " candidate(s) to " + MiscUtil.RemoveScript(ddlLevels.SelectedItem.Text), false);
                    txtSelectedIds.Text = "";
                    BindList();

                    
                    RefreshCandidateCountinLeftPanel();
                }
            }
            else MiscUtil.ShowMessage(lblMessage, "Please select at least one candidate", true);
            
        }
        
        //protected void btnRemoveFromThisLevel_Click(object sender, EventArgs e)
        //{
        //    string applicantIds = txtSelectedIds.Text;
        //    if (!string.IsNullOrEmpty(applicantIds))
        //    {
        //        Facade.MemberJobCart_MoveToNextLevel(HiringMatrixInterviewLevel, base.CurrentMember.Id, txtSelectedIds.Text, CurrentJobPosting.Id, ("Previous"));
        //        BindList();
        //        MiscUtil.ShowMessage(lblMessage, "Successfully moved " + getNumberOfSelectedCandidates() + " candidate(s) to " + hdnPreviousLevelName .Value , false);
        //    }
        //    else MiscUtil.ShowMessage(lblMessage, "Please select at least one candidate", true);
        //    txtSelectedIds.Text = "";
        //}

        protected void btnRemove_Click(object sender, EventArgs e)
        {
            hdnMoving.Value = ddlLevels.SelectedValue;// list.SelectedValue;
            //hdnRemove.Value = "Remove";
            hdncurretn.Value = ddlLevels.SelectedValue;
            string applicantIds = txtSelectedIds.Text;
            if (!string.IsNullOrEmpty(applicantIds))
            {
                uclConfirm.AddMessage("Are you sure that you want to reject this candidate(s)  from the Hiring Matrix?", ConfirmationWindow.enmMessageType.Attention, true, true, "");
            }
            else MiscUtil.ShowMessage(lblMessage, "Please select at least one candidate", true);
        }

        protected void btnSubmitToClient_Click(object sender, EventArgs e)
        {
            SubmitToClient(txtSelectedIds.Text);
        }
     
        #endregion

        #region Listview Event
        protected void lvNavigationItems_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                HiringMatrixLevels node = ((ListViewDataItem)e.Item).DataItem as HiringMatrixLevels;

                if (node != null)
                {
                    LinkButton  linkLeftMenuItem = (LinkButton )e.Item.FindControl("lnkLeftMenuItem");
                    HiddenField hnSelectedLevel = (HiddenField)e.Item.FindControl("hnSelectedLevel");
                    HtmlGenericControl spName = (HtmlGenericControl)e.Item.FindControl("spName");
                    HtmlGenericControl spCount = (HtmlGenericControl)e.Item.FindControl("spCount");
                    spName.InnerText = node.Name;
                    if (node.Name == "All")
                    {
                        linkLeftMenuItem.CssClass = "active level";
                        linkLeftMenuItem.Enabled = false;
                    }
                    
                    hnSelectedLevel.Value = node.Id.ToString ();
                    spCount.InnerText = "("+node.Count +")";
                    hdnCount.Value = hdnCount.Value != string.Empty ? (Convert.ToInt32(hdnCount.Value) + node.Count).ToString () : "0";
                    linkLeftMenuItem.CommandArgument = node.Id.ToString();
                    //if (node.Name == "Rejected")
                    //{
                    //    linkLeftMenuItem.CommandArgument = "Rejected";
                    //}
                }
            }
        }

        protected void lsvCandidateList_ItemDataBound(object sender, ListViewItemEventArgs e)
        {

            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {

                HiringMatrix hiringMatrix = ((ListViewDataItem)e.Item).DataItem as HiringMatrix;
                System.Web.UI.HtmlControls.HtmlTableRow row = (System.Web.UI.HtmlControls.HtmlTableRow)e.Item.FindControl("row");
                System.Web.UI.HtmlControls.HtmlTableRow trCandidateDetailss = (System.Web.UI.HtmlControls.HtmlTableRow)e.Item.FindControl("trCandidateDetails");
                System.Web.UI.WebControls.Image imgShowHide = (System.Web.UI.WebControls.Image)e.Item.FindControl("imgShowHide");
                System.Web.UI.WebControls.Image imgContextMenu = (System.Web.UI.WebControls.Image)e.Item.FindControl("imgContextMenu");
                System.Web.UI.HtmlControls.HtmlGenericControl divContextMenu = (System.Web.UI.HtmlControls.HtmlGenericControl)e.Item.FindControl("divContextMenu");
                System.Web.UI.HtmlControls.HtmlGenericControl ulList = (System.Web.UI.HtmlControls.HtmlGenericControl)e.Item.FindControl("ulList");
                System.Web.UI.HtmlControls.HtmlGenericControl icon = (System.Web.UI.HtmlControls.HtmlGenericControl)e.Item.FindControl("icon");
                System.Web.UI.HtmlControls.HtmlImage imgList = (System.Web.UI.HtmlControls.HtmlImage)e.Item.FindControl("imgList");


                HyperLink hlnCandidateName = (HyperLink)e.Item.FindControl("hlnCandidateName");
                ControlHelper.SetHyperLink(hlnCandidateName, UrlConstants.Candidate.OVERVIEW, string.Empty, hiringMatrix .FirstName , UrlConstants.PARAM_MEMBER_ID, StringHelper.Convert(hiringMatrix.MemberId), UrlConstants.PARAM_SITEMAP_ID, UrlConstants.Candidate.OVERVIEW_SITEMAP_ID, UrlConstants.PARAM_SITEMAP_PARENT_ID, UrlConstants.Candidate.OVERVIEW_SITEMAP_PARENTID, UrlConstants.PARAM_JOB_ID, CurrentJobPostingId.ToString());  //1.2
                LinkButton lblCandidateEmail = (LinkButton)e.Item.FindControl("lblCandidateEmail");
               // lblCandidateEmail.Attributes.Add("onclick", "javascript:ShowOrHide('" + trCandidateDetailss.ClientID + "','" + imgShowHide.ClientID + "');");
//
                //Binding PrimaryEmail
                if (_IsMemberMailAccountAvailable)
                {
                    lblCandidateEmail.Text = hiringMatrix.PrimaryEmail;
                    SecureUrl url = UrlHelper.BuildSecureUrl(UrlConstants.ApplicationBaseUrl + UrlConstants.Dashboard.DIR + UrlConstants.Dashboard.NEWMAIL, string.Empty, UrlConstants.PARAM_MEMBER_ID, hiringMatrix.MemberId.ToString());
                    lblCandidateEmail.OnClientClick = "window.open('" + url + "','NewMail','height=670,width=770,toolbar=no,directories=no,status=no,linemenubar=no,scrollbar=no,resizeable=no,modal=no');";

                }
                else lblCandidateEmail.Text = "<a href=mailto:" + hiringMatrix.PrimaryEmail + ">" + hiringMatrix.PrimaryEmail + "</a>";
                HiddenField hfCandidateId = (HiddenField)e.Item.FindControl("hfCandidateId");
                hfCandidateId.Value = hiringMatrix.MemberId.ToString();
                HiddenField hfstatuschangeId = (HiddenField)e.Item.FindControl("hfstatuschangeId");
                HiddenField hdnCurrentlevelId = (HiddenField)e.Item.FindControl("hdnCurrentlevelId");
                hdnCurrentlevelId.Value = hiringMatrix.CurrentLevel.ToString();
                DropDownList ddlStatus = (DropDownList)e.Item.FindControl("ddlStatus");
                ddlStatus.DataValueField = dummyList.DataValueField;
                ddlStatus.DataTextField = dummyList.DataTextField;
                ddlStatus.DataSource = dummyList.DataSource;
                ddlStatus.DataBind();
                ControlHelper.SelectListByValue(ddlStatus, hdnCurrentlevelId.Value);
                ddlStatus.Attributes.Add("onchange", "javascript:Status_OnChange('" + ddlStatus.ClientID + "','" + hfstatuschangeId.ClientID + "');");

                Label lblCandidateCurrentPosition = (Label)e.Item.FindControl("lblCandidateCurrentPosition");
                //Adding Current Position
                lblCandidateCurrentPosition.Text = HighLightText(hiringMatrix.CurrentPosition, CurrentJobPosting.JobTitle);
                lblCandidateEmail.ToolTip = hiringMatrix.PrimaryEmail;

                Label lblCandidateExperience = (Label)e.Item.FindControl("lblCandidateExperience");
                if (!string.IsNullOrEmpty(hiringMatrix.TotalExperienceYears))
                {
                    if (CurrentJobPosting.MinExpRequired != "" && Convert.ToDouble(CurrentJobPosting.MinExpRequired == "" ? "0" : CurrentJobPosting.MinExpRequired) <= Convert.ToDouble(hiringMatrix.TotalExperienceYears) && Convert.ToDouble(CurrentJobPosting.MaxExpRequired == "" ? "1000" : CurrentJobPosting.MaxExpRequired) >= Convert.ToDouble(hiringMatrix.TotalExperienceYears))
                        lblCandidateExperience.Text = "<span class=highlight>" + hiringMatrix.TotalExperienceYears + "</span>";
                    else lblCandidateExperience.Text = hiringMatrix.TotalExperienceYears;

                }
                Label lblCity = (Label)e.Item.FindControl("lblCity");
                lblCity.Text = HighLightExactText(hiringMatrix.CurrentCity, CurrentJobPosting.City);
                if (lblCity.Text.Trim() != "" && hiringMatrix.CurrentState != "") lblCity.Text += ", ";
                if (hiringMatrix.CurrentStateID > 0 && hiringMatrix.CurrentStateID == CurrentJobPosting.StateId)
                    lblCity.Text += "<span class=highlight>" + hiringMatrix.CurrentState + "</span>";
                else lblCity.Text += hiringMatrix.CurrentState;

                //System.Web.UI.HtmlControls.HtmlGenericControl relevance_bar = (System.Web.UI.HtmlControls.HtmlGenericControl)e.Item.FindControl("relevance_bar");
                //relevance_bar.Style.Add(HtmlTextWriterStyle.Width, Convert.ToDouble(hiringMatrix.MatchingPercentage).ToString("#,#0") + "%");
                System.Web.UI.HtmlControls.HtmlGenericControl relevance_text = (System.Web.UI.HtmlControls.HtmlGenericControl)e.Item.FindControl("relevance_text");
                //relevance_text.InnerText = Convert.ToDouble(hiringMatrix.MatchingPercentage).ToString("#,#0") + "%";
                relevance_text.Style[HtmlTextWriterStyle.Width] = Convert.ToDouble(hiringMatrix.MatchingPercentage).ToString("#,#0") + "%"; 


                System.Web.UI.HtmlControls.HtmlGenericControl liInterview = (System.Web.UI.HtmlControls.HtmlGenericControl)e.Item.FindControl("liInterview");
                if (liInterview != null)
                {
                    if (isAccess)
                    {
                        HyperLink lnkSchedule = (HyperLink)e.Item.FindControl("lnkSchedule");
                        if (lnkSchedule != null)
                        {
                            SecureUrl url = UrlHelper.BuildSecureUrl(UrlConstants.ATS.ATS_INTERNALINTERVIEW_SCHEDULE, string.Empty, UrlConstants.PARAM_MEMBER_ID, hiringMatrix.MemberId.ToString(), UrlConstants.PARAM_JOB_ID, base.CurrentJobPostingId.ToString(), UrlConstants.PARAM_SITEMAP_ID, "403");
                            lnkSchedule.NavigateUrl = url.ToString();
                        }
                        liInterview.Visible = true;
                    }
                    else
                        liInterview.Visible = false;
                }

                LinkButton lnkEmailrequisition = (LinkButton)e.Item.FindControl("lnkEmailrequisition");
                LinkButton lnkSubmit = (LinkButton)e.Item.FindControl("lnkSubmit");
                LinkButton lnkHiringDetails = (LinkButton)e.Item.FindControl("lnkHiringDetails");
                LinkButton lnkJoiningDetails = (LinkButton)e.Item.FindControl("lnkJoiningDetails");
                LinkButton lnkActionLog = (LinkButton)e.Item.FindControl("lnkHiringLog");
                LinkButton lnkEditSubmissionDetails = (LinkButton)e.Item.FindControl("lnkEditSubmissionDetails");
                lnkActionLog.CommandArgument = lnkJoiningDetails.CommandArgument = lnkEditSubmissionDetails.CommandArgument =  lnkHiringDetails.CommandArgument =   lnkEmailrequisition.CommandArgument = lnkSubmit.CommandArgument = hiringMatrix.MemberId.ToString();

                lnkJoiningDetails.Parent.Visible = hiringMatrix.HasJoiningDetails;
                lnkHiringDetails.Parent.Visible = hiringMatrix.HasOfferDetails;
                lnkEditSubmissionDetails.Parent.Visible = hiringMatrix.HasSubmissionDetails;

                System.Web.UI.HtmlControls.HtmlGenericControl intervewImg = (System.Web.UI.HtmlControls.HtmlGenericControl)e.Item.FindControl("intervewImg");
                intervewImg.Visible = hiringMatrix.HasUpcommingInterview;
                /*
                HyperLink hlnCandidateName = (HyperLink)e.Item.FindControl("hlnCandidateName");
                LinkButton lblCandidateEmail = (LinkButton)e.Item.FindControl("lblCandidateEmail");
                DropDownList ddlStatus = (DropDownList)e.Item.FindControl("ddlStatus");
                CheckBox chkCandidate = (CheckBox)e.Item.FindControl("chkCandidate");
                HiringMatrix hiringMatrix = ((ListViewDataItem)e.Item).DataItem as HiringMatrix;
                if (hiringMatrix != null)
                {
                    System.Web.UI.HtmlControls.HtmlTableRow row = (System.Web.UI.HtmlControls.HtmlTableRow)e.Item.FindControl("row");
                    System.Web.UI.HtmlControls.HtmlTableRow trCandidateDetailss = (System.Web.UI.HtmlControls.HtmlTableRow)e.Item.FindControl("trCandidateDetails");
                    System.Web.UI.WebControls.Image imgShowHide = (System.Web.UI.WebControls.Image)e.Item.FindControl("imgShowHide");
                    System.Web.UI.WebControls.Image imgContextMenu = (System.Web.UI.WebControls.Image)e.Item.FindControl("imgContextMenu");
                    System.Web.UI.HtmlControls.HtmlGenericControl divContextMenu = (System.Web.UI.HtmlControls.HtmlGenericControl)e.Item.FindControl("divContextMenu");
                    System.Web.UI.HtmlControls.HtmlGenericControl ulList = (System.Web.UI.HtmlControls.HtmlGenericControl)e.Item.FindControl("ulList");
                    System.Web.UI.HtmlControls.HtmlGenericControl icon = (System.Web.UI.HtmlControls.HtmlGenericControl)e.Item.FindControl("icon");
                    System.Web.UI.HtmlControls.HtmlImage imgList = (System.Web.UI.HtmlControls.HtmlImage)e.Item.FindControl("imgList");
                    LinkButton lnkMoveToNextLevel = (LinkButton)e.Item.FindControl("lnkMoveToNextLevel");
                    LinkButton lnkMoveToPreviousLevel = (LinkButton)e.Item.FindControl("lnkMoveToPreviousLevel");
                    LinkButton lnkRemove = (LinkButton)e.Item.FindControl("lnkRemove");
                    LinkButton lnkEmailrequisition = (LinkButton)e.Item.FindControl("lnkEmailrequisition");
                    LinkButton lnkSubmit = (LinkButton)e.Item.FindControl("lnkSubmit");
                    LinkButton lnkHiringDetails = (LinkButton)e.Item.FindControl("lnkHiringDetails");
                    LinkButton lnkJoiningDetails = (LinkButton)e.Item.FindControl("lnkJoiningDetails");
                    LinkButton lnkActionLog = (LinkButton)e.Item.FindControl("lnkHiringLog");
                    LinkButton lnkRejectCandidate = (LinkButton)e.Item.FindControl("lnkRejectCandidate");
                    LinkButton lnkEditSubmissionDetails = (LinkButton)e.Item.FindControl("lnkEditSubmissionDetails");
                    System.Web.UI.HtmlControls.HtmlGenericControl liEditSubmissionDetails = (System.Web.UI.HtmlControls.HtmlGenericControl)e.Item.FindControl("liEditSubmissionDetails");
                    HiddenField hfstatuschangeId = (HiddenField)e.Item.FindControl("hfstatuschangeId");
                    LinkButton lnkEditSource = (LinkButton)e.Item.FindControl("lnkEditSource");
                    HiddenField hfSourceId = (HiddenField)e.Item.FindControl("hfSourceId");


                    Label lblSourcedBy = (Label)e.Item.FindControl("lblSourcedBy");
                    if (hiringMatrix.SourceID != 0 && hiringMatrix.SourceName != "") { lblSourcedBy.Text = hiringMatrix.SourceName ;  }
                    else lblSourcedBy.Text = hiringMatrix.AddedBy;
                    //lnkHiringDetails.OnClientClick = "javascript:EditHiringDetailsClicked('" + CurrentMember.Id + "','" + hiringMatrix.MemberId + "','" + CurrentJobPostingId + "','" + row.ClientID + "'); return false;";

                    MemberSubmission submissions= Facade.GetMemberSubmissionsByMemberIDAndJobPostingId(hiringMatrix.MemberId, CurrentJobPostingId);
                    //lnkEditSubmissionDetails.OnClientClick = "javascript:EditSubmissionDetailsClicked('" + CurrentMember.Id + "','" + hiringMatrix.MemberId + "','" + CurrentJobPostingId + "','" + row.ClientID + "'); return false;";
                    if(submissions ==null)
                    {
                        liEditSubmissionDetails .Style .Add ("display","none");// =false ;
                    }

                    
                   lnkActionLog .CommandArgument = lnkJoiningDetails .CommandArgument = lnkEditSubmissionDetails.CommandArgument =lnkRejectCandidate.CommandArgument = lnkHiringDetails.CommandArgument = lnkMoveToNextLevel.CommandArgument = lnkMoveToPreviousLevel.CommandArgument = lnkEmailrequisition.CommandArgument = lnkSubmit.CommandArgument = hiringMatrix.MemberId.ToString();
                    row.Attributes.Add("onclick", "javascript:ShowOrHide('" + trCandidateDetailss.ClientID + "','" + imgShowHide.ClientID + "');");
                    row.Attributes.Add("onmouseover", "javascript:ShowOrHideContextMenu('" + imgContextMenu.ClientID + "','visible','" + divContextMenu.ClientID + "');");
                    row.Attributes.Add("onmouseout", "javascript:ShowOrHideContextMenu('" + imgContextMenu.ClientID + "','hidden','" + divContextMenu.ClientID + "');");
                    imgContextMenu.Style.Add("visibility", "hidden");
                    imgContextMenu.Attributes.Add("onclick", "javascript:ShowOrHideContext('" + trCandidateDetailss.ClientID + "','" + imgShowHide.ClientID + "','" + divContextMenu.ClientID + "','" + imgContextMenu.ClientID  + "');");
                    hlnCandidateName.Attributes.Add("onclick", "javascript:ShowOrHide('" + trCandidateDetailss.ClientID + "','" + imgShowHide.ClientID + "');");
                    ddlStatus.Attributes.Add("onclick", "javascript:ShowOrHide('" + trCandidateDetailss.ClientID + "','" + imgShowHide.ClientID + "');");
                    lblCandidateEmail.Attributes.Add("onclick", "javascript:ShowOrHide('" + trCandidateDetailss.ClientID + "','" + imgShowHide.ClientID + "');");
                    chkCandidate.Attributes.Add("onclick", "javascript:SelectRowCheckbox('" + trCandidateDetailss.ClientID + "','" + imgShowHide.ClientID + "');");
                    chkCandidate.Checked = CheckSelectedList(hiringMatrix.MemberId);
                    divContextMenu.Attributes.Add("onmouseout", "onLeaveContextMenu('" + divContextMenu.ClientID + "')");
                   ulList.Attributes.Add("onmouseout", "onLeaveContextMenu('" + divContextMenu.ClientID + "')");
                    ulList.Attributes.Add("onmouseover", "javascript:ShowContextMenu('" + divContextMenu.ClientID + "')");
                    imgList.Attributes.Add("onmouseover", "javascript:ShowContextMenu('" + divContextMenu.ClientID + "')");

                    lnkMoveToNextLevel.Attributes.Add("onclick", "ListItemClick('" + divContextMenu.ClientID + "')");
                    lnkMoveToPreviousLevel.Attributes.Add("onclick", "ListItemClick('" + divContextMenu.ClientID + "')");
                    //lnkRemove.Attributes.Add("onclick", "ListItemClick('" + divContextMenu.ClientID + "')");
                    lnkEmailrequisition.Attributes.Add("onclick", "ListItemClick('" + divContextMenu.ClientID + "')");
                    lnkSubmit.Attributes.Add("onclick", "ListItemClick('" + divContextMenu.ClientID + "')");
                    lnkHiringDetails.Attributes.Add("onclick", "ListItemClick('" + divContextMenu.ClientID + "')");
                    lnkJoiningDetails.Attributes.Add("onclick", "ListItemClick('" + divContextMenu.ClientID + "')");
                   lnkActionLog.Attributes.Add("onclick", "ListItemClick('" + divContextMenu.ClientID + "')");
                    lnkRejectCandidate.Attributes.Add("onclick", "ListItemClick('" + divContextMenu.ClientID + "')");
                    ddlStatus.Attributes.Add("onchange", "javascript:Status_OnChange('" + ddlStatus.ClientID + "','" + hfstatuschangeId.ClientID + "');");
                    icon.Attributes.Add("onmouseout", "onLeaveContextMenu('" + divContextMenu.ClientID + "')");
                    icon.Attributes.Add("onmouseover", "javascript:ShowContextMenu('" + divContextMenu.ClientID + "')");
                    divContextMenu.Attributes.Add("onclick", "javascript:ShowOrHide('" + trCandidateDetailss.ClientID + "','" + imgShowHide.ClientID + "')");
                    System.Web.UI.HtmlControls.HtmlGenericControl liNextLevel = (System.Web.UI.HtmlControls.HtmlGenericControl)e.Item.FindControl("liNextLevel");
                    System.Web.UI.HtmlControls.HtmlGenericControl liPreviousLevel = (System.Web.UI.HtmlControls.HtmlGenericControl)e.Item.FindControl("liPreviousLevel");
                    System.Web.UI.HtmlControls.HtmlGenericControl liRemove = (System.Web.UI.HtmlControls.HtmlGenericControl)e.Item.FindControl("liRemove");
                    System.Web.UI.HtmlControls.HtmlGenericControl liEmail = (System.Web.UI.HtmlControls.HtmlGenericControl)e.Item.FindControl("liEmail");
                    System.Web.UI.HtmlControls.HtmlGenericControl liSubmit = (System.Web.UI.HtmlControls.HtmlGenericControl)e.Item.FindControl("liSubmit");
                    System.Web.UI.HtmlControls.HtmlGenericControl liHiringDetails = (System.Web.UI.HtmlControls.HtmlGenericControl)e.Item.FindControl("liHiringDetails");
                    System.Web.UI.HtmlControls.HtmlGenericControl liRejectCandidate = (System.Web.UI.HtmlControls.HtmlGenericControl)e.Item.FindControl("liRejectCandidate");
                    System.Web.UI.HtmlControls.HtmlGenericControl liJoiningDetails = (System.Web.UI.HtmlControls.HtmlGenericControl)e.Item.FindControl("liJoiningDetails");


                   // liNextLevel.Visible = hdnNextLevelName.Value.Trim() != ""; //btnMoveToNextLevel.Visible;
                   // liPreviousLevel.Visible = hdnPreviousLevelName.Value != ""; //btnRemoveFromThisLevel.Visible;
                    //liEmail.Visible = btnEmail.Visible;
                    //liSubmit.Visible = btnSubmitToClient.Visible;
                    liNextLevel.Attributes.Add("onmouseover", "javascript:ShowContextMenu('" + divContextMenu.ClientID + "')");
                    liPreviousLevel.Attributes.Add("onmouseover", "javascript:ShowContextMenu('" + divContextMenu.ClientID + "')");
                    liEmail.Attributes.Add("onmouseover", "javascript:ShowContextMenu('" + divContextMenu.ClientID + "')");
                    liSubmit.Attributes.Add("onmouseover", "javascript:ShowContextMenu('" + divContextMenu.ClientID + "')");
                    //liRemove.Attributes.Add("onmouseover", "javascript:ShowContextMenu('" + divContextMenu.ClientID + "')");
                    liHiringDetails.Attributes.Add("onmouseover", "javascript:ShowContextMenu('" + divContextMenu.ClientID + "')");
                    liJoiningDetails.Attributes.Add("onmouseover", "javascript:ShowContextMenu('" + divContextMenu.ClientID + "')");
                    liRejectCandidate.Attributes.Add("onmouseover", "javascript:ShowContextMenu('" + divContextMenu.ClientID + "')");
                    liEditSubmissionDetails.Attributes.Add("onmouseover", "javascript:ShowContextMenu('" + divContextMenu.ClientID + "')");

                    
                    hdnDetailRowIds.Value += trCandidateDetailss.ClientID + ";" + imgShowHide.ClientID + ",";
                    System.Web.UI.HtmlControls.HtmlTableCell tdDetail = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdDetail");
                    HiddenField hfCandidateId = (HiddenField)e.Item.FindControl("hfCandidateId");
                    hfCandidateId.Value = hiringMatrix.MemberId.ToString();
                    System.Web.UI.HtmlControls.HtmlGenericControl relevance_bar = (System.Web.UI.HtmlControls.HtmlGenericControl)e.Item.FindControl("relevance_bar");
                    relevance_bar.Style.Add(HtmlTextWriterStyle.Width, Convert.ToDouble(hiringMatrix.MatchingPercentage).ToString("#,#0") + "%");
                    System.Web.UI.HtmlControls.HtmlGenericControl relevance_text = (System.Web.UI.HtmlControls.HtmlGenericControl)e.Item.FindControl("relevance_text");
                    relevance_text.InnerText = Convert.ToDouble(hiringMatrix.MatchingPercentage).ToString("#,#0") + "%";
                    hdnDivContextMenuids.Value += divContextMenu.ClientID + ";";
                    Label lblCandidateExperience = (Label)e.Item.FindControl("lblCandidateExperience");
                    Label lblCandidateCurrentPosition = (Label)e.Item.FindControl("lblCandidateCurrentPosition");
                    Label lblCandidateCurrentCompany = (Label)e.Item.FindControl("lblCandidateCurrentCompany");
                    Label lblCandidateWorkStatus = (Label)e.Item.FindControl("lblCandidateWorkStatus");
                    Label lblCandidateCurrentSalary = (Label)e.Item.FindControl("lblCandidateCurrentSalary");
                    Label lblCandidateExpectedSalary = (Label)e.Item.FindControl("lblCandidateExpectedSalary");
                    Label lblCandidateLastUpdate = (Label)e.Item.FindControl("lblCandidateLastUpdate");
                    Label lblCandidateAvailability = (Label)e.Item.FindControl("lblCandidateAvailability");
                    Label lblCandidateMobile = (Label)e.Item.FindControl("lblCandidateMobile");
                    Label lblCity = (Label)e.Item.FindControl("lblCity");
                    Label lblCandidateSkills = (Label)e.Item.FindControl("lblskills");
                    Label lblHiringstatus = (Label)e.Item.FindControl("lblHiringstatus");
                    Label lblPrimaryManager = (Label)e.Item.FindControl("lblPrimaryManager");
                    Label lblCandidateObjective = (Label)e.Item.FindControl("lblCandidateObjective");
                    Label lblRemarks = (Label)e.Item.FindControl("lblRemarks");

                    System.Web.UI.HtmlControls.HtmlGenericControl divControl = (System.Web.UI.HtmlControls.HtmlGenericControl)e.Item.FindControl("hiringnotesdiv");
                    
                    LinkButton lnkHiringNotes = (LinkButton)e.Item.FindControl("lnkHiringNotes");
                    Label lblHiringNote = (Label)e.Item.FindControl("lblHiringNote");
                    
                    Label lblHighestDegree = (Label)e.Item.FindControl("lblHighestDegree");
                    Label lblCandidateType = (Label)e.Item.FindControl("lblCandidateType");
                    Label lblWorkSchedule = (Label)e.Item.FindControl("lblWorkSchedule");
                    Label lblEmptyInterview = (Label)e.Item.FindControl("lblEmptyInterview");
                    Label lblAddedBy = (Label)e.Item.FindControl("lblAddedBy");
                    lblAddedBy.Text = hiringMatrix.AddedBy + " on " + hiringMatrix.AddedOn.ToShortDateString() + " " + hiringMatrix.AddedOn.ToShortTimeString(); //Facade.GetMemberNameById(hiringMatrix.CreatorId) + " on " + hiringMatrix.CreateDate.ToShortDateString();
                    System.Web.UI.HtmlControls.HtmlTableCell tdInterview = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdInterview");
                    HiddenField hfHiringnote = (HiddenField)e.Item.FindControl("hfHiringnote");
                    HiddenField hfUpdatorName = (HiddenField)e.Item.FindControl("hfUpdatorName");
                    HiddenField hfUpdateTime = (HiddenField)e.Item.FindControl("hfUpdateTime");
                    if (hiringMatrix.NoteUpdatorName  != string.Empty)
                    {
                        divControl.InnerHtml = hiringMatrix.HiringNote.Replace("\n", "<br/>") + "<br/>" + "Last updated by " + hiringMatrix.NoteUpdatorName + " on " + hiringMatrix.NoteUpdatedDate.ToShortDateString() + " " + hiringMatrix.NoteUpdatedDate.ToShortTimeString() + ".<br/>";
                        //lblHiringNote.Text = hiringMatrix.HiringNote + "<br/>" + "Last updated by " + hiringMatrix.NoteUpdatorName + " on " + hiringMatrix.NoteUpdatedDate.ToShortDateString() + " " + hiringMatrix.NoteUpdatedDate.ToShortTimeString() + ".<br/>";
                        hfHiringnote.Value = hiringMatrix.HiringNote;
                        hfUpdatorName.Value = hiringMatrix.NoteUpdatorName;
                        hfUpdateTime.Value = hiringMatrix.NoteUpdatedDate.ToShortDateString() + " " + hiringMatrix.NoteUpdatedDate.ToShortTimeString();
                    }
                    lnkHiringNotes.OnClientClick = "SaveHiringNote('" + hfHiringnote.ClientID + "','" + hiringMatrix.MemberId + "','" + row.ClientID + "','" + hfUpdatorName.ClientID + "','" + hfUpdateTime.ClientID + "','" + CurrentJobPostingId + "','" + hiringMatrix.FirstName  +" " + hiringMatrix.LastName + "'); return  false;";



                    lnkEditSource.OnClientClick = "EditHiringMatrixSource('" + hfSourceId.ClientID + "','" + hiringMatrix.MemberId + "','" + row.ClientID + "','" + CurrentJobPostingId + "','" + hiringMatrix.FirstName + " " + hiringMatrix.LastName + "'); return false;";
                    //MiscUtil.PopulateCandidateRequisitionStatus(ddlStatus, Facade);
                    //if (hiringMatrix.StatusId > 0)
                    //{
                    //    ddlStatus.SelectedValue = hiringMatrix.StatusId.ToString();
                    //}
                    //ddlStatus.Items.AddRange(dummyList .Items .OfType <ListItem >().ToArray ());
                    ddlStatus.DataValueField = dummyList.DataValueField;
                    ddlStatus.DataTextField = dummyList.DataTextField;
                    ddlStatus.DataSource = dummyList.DataSource;
                    ddlStatus.DataBind();
                    System.Web.UI.HtmlControls.HtmlGenericControl bInterview = (System.Web.UI.HtmlControls.HtmlGenericControl)e.Item.FindControl("bInterview");
                    System.Web.UI.HtmlControls.HtmlGenericControl intervewImg = (System.Web.UI.HtmlControls.HtmlGenericControl)e.Item.FindControl("intervewImg");
                    HiddenField hdnCurrentlevelId=(HiddenField )e.Item .FindControl ("hdnCurrentlevelId");
                    hdnCurrentlevelId.Value = hiringMatrix.CurrentLevel.ToString();
                    ControlHelper.SelectListByValue(ddlStatus, hdnCurrentlevelId.Value);
                    
                    if (ddlStatus .SelectedItem .Text  == "Rejected")
                        chkCandidate.Enabled = false;
                    //ddlStatus.SelectedValue = hdnCurrentlevelId.Value;//HiringMatrixInterviewLevel.ToString();
                    IList<Interview> interviewlist = new List<Interview>();
                    
                    interviewlist = Facade.GetInterviewByMemberIdAndJobPostingID (hiringMatrix.MemberId,CurrentJobPostingId );
                    if (interviewlist != null)
                    {

                        if (interviewlist.Where(x => x.StartDateTime >= DateTime.Now).ToList().Count() > 0)
                        {
                            intervewImg.Visible = true;
                        }
                        hdnIsHide.Value = "false";
                        int i = 0;
                        interviewlist = interviewlist.OrderByDescending(f => f.StartDateTime).ToList();
                        foreach (Interview inter in interviewlist)
                        {
                            SecureUrl url = UrlHelper.BuildSecureUrl(UrlConstants.ATS.ATS_INTERVIEW_SCHEDULE, string.Empty, UrlConstants.PARAM_MEMBER_ID, hiringMatrix.MemberId.ToString(), UrlConstants.PARAM_INTERVIEWSCHEDULE_ID, inter.Id.ToString(), UrlConstants.PARAM_SITEMAP_ID, "403");
                            string datetime = string.Empty;
                            if (inter.AllDayEvent)
                            {
                                datetime = inter.StartDateTime.ToShortDateString() + " All Day" + "";
                            }
                            else
                            {
                                DateTime date = new DateTime();

                                date = getTime(inter.StartDateTime, inter.Duration);
                                if (date.Hour > 12 || inter.StartDateTime.Hour > 12)
                                    datetime = inter.StartDateTime.ToShortDateString() + " " + inter.StartDateTime.ToString("hh:mm tt") + " - " + getTime(inter.StartDateTime, inter.Duration).ToString("hh:mm tt") + "";
                                else
                                    datetime = inter.StartDateTime.ToShortDateString() + " " + inter.StartDateTime.ToString("hh:mm") + " - " + getTime(inter.StartDateTime, inter.Duration).ToString("hh:mm tt") + "";
                            }

                            if (isAccess)
                            {
                                if (lblEmptyInterview.Text.Trim() != "")
                                {
                                    lblEmptyInterview.Text = lblEmptyInterview.Text + ",<br/>";
                                }
                                lblEmptyInterview.Text = lblEmptyInterview.Text + "<a href='" + url + "' TARGET='_blank' >" + datetime + "</a>";
                            }
                            else
                                lblEmptyInterview.Text = lblEmptyInterview.Text + datetime;
                        }
                    }
                    else 
                    {
                        bInterview.Visible = false ;
                        lblEmptyInterview.Visible = false;
                       // intervewImg.Visible = false;
                    }
                    lblEmptyInterview.Text +="<br/>";
                    System.Web.UI.HtmlControls.HtmlTableRow trCandidateDetails = (System.Web.UI.HtmlControls.HtmlTableRow)e.Item.FindControl("trCandidateDetails");
                        //Adding Name
                    string _strFullName = hiringMatrix.FirstName + " " + hiringMatrix.MiddleName + " " + hiringMatrix.LastName;
                    if (_strFullName.Trim() == "")
                    {
                        _strFullName = "No Candidate Name";
                    }
                    ControlHelper.SetHyperLink(hlnCandidateName, UrlConstants.Candidate.OVERVIEW, string.Empty, _strFullName, UrlConstants.PARAM_MEMBER_ID, StringHelper.Convert(hiringMatrix.MemberId), UrlConstants.PARAM_SITEMAP_ID, UrlConstants.Candidate.OVERVIEW_SITEMAP_ID, UrlConstants.PARAM_SITEMAP_PARENT_ID, UrlConstants.Candidate.OVERVIEW_SITEMAP_PARENTID, UrlConstants.PARAM_JOB_ID, CurrentJobPostingId.ToString());  //1.2

                        //Adding Current Position
                        lblCandidateCurrentPosition.Text = HighLightText(hiringMatrix.CurrentPosition, CurrentJobPosting.JobTitle);

                        //Binding Primary Manager
                        lblPrimaryManager.Text = hiringMatrix.PrimaryManager; 

                        //Binding Location Values--> (City, State)
                        lblCity.Text = HighLightExactText(hiringMatrix.CurrentCity, CurrentJobPosting.City);
                        if (lblCity.Text.Trim () != "" && hiringMatrix.CurrentState != "") lblCity.Text += ", ";
                        if (hiringMatrix.CurrentStateID > 0 && hiringMatrix.CurrentStateID == CurrentJobPosting.StateId)
                            lblCity.Text += "<span class=highlight>" + hiringMatrix.CurrentState + "</span>";
                        else lblCity.Text += hiringMatrix.CurrentState;

                        //Adding Cell Phone Value
                        lblCandidateMobile.Text = hiringMatrix.CellPhone;

                        //Binding PrimaryEmail
                        if (_IsMemberMailAccountAvailable)
                        {
                            lblCandidateEmail.Text = hiringMatrix.PrimaryEmail;
                            SecureUrl url = UrlHelper.BuildSecureUrl(UrlConstants.ApplicationBaseUrl + UrlConstants.Dashboard.DIR + UrlConstants.Dashboard.NEWMAIL, string.Empty, UrlConstants.PARAM_MEMBER_ID, hiringMatrix.MemberId.ToString());
                            lblCandidateEmail.OnClientClick = "window.open('" + url + "','NewMail','height=670,width=770,toolbar=no,directories=no,status=no,linemenubar=no,scrollbar=no,resizeable=no,modal=no');";

                        }
                        else lblCandidateEmail.Text = "<a href=mailto:" + hiringMatrix.PrimaryEmail + ">" + hiringMatrix.PrimaryEmail + "</a>";

                        lblCandidateEmail.ToolTip = hiringMatrix.PrimaryEmail;
                        if (!string.IsNullOrEmpty(hiringMatrix.TotalExperienceYears))
                        {
                                if ( CurrentJobPosting.MinExpRequired  !="" && Convert.ToDouble(CurrentJobPosting.MinExpRequired == "" ? "0" : CurrentJobPosting.MinExpRequired) <= Convert.ToDouble(hiringMatrix.TotalExperienceYears) && Convert.ToDouble(CurrentJobPosting.MaxExpRequired == "" ? "1000" : CurrentJobPosting.MaxExpRequired) >= Convert.ToDouble(hiringMatrix.TotalExperienceYears))
                                    lblCandidateExperience.Text = "<span class=highlight>" + hiringMatrix.TotalExperienceYears + "</span>";
                                else lblCandidateExperience.Text = hiringMatrix.TotalExperienceYears;

                        }

                        lblCandidateCurrentCompany.Text = hiringMatrix.LastEmployer;// candidate.LastEmployer; // memberExtendedInformation.LastEmployer;

                        //work Authorization or Work Status
                        if (CurrentJobPosting.AuthorizationTypeLookupId != "" && ("," + CurrentJobPosting.AuthorizationTypeLookupId + ",").Contains("," + hiringMatrix.WorkStatusID + ","))
                            lblCandidateWorkStatus.Text = "<span class=highlight>" + hiringMatrix.WorkAuthorization + "</span>";
                        else lblCandidateWorkStatus.Text = hiringMatrix.WorkAuthorization;

                        //Binding Availability
                        if (hiringMatrix.AvailabilityText == "Available From") lblCandidateAvailability.Text = hiringMatrix.AvailabilityText + " " + hiringMatrix.AvailableFrom.ToShortDateString();
                        else lblCandidateAvailability.Text = hiringMatrix.AvailabilityText;
                        // Loading Current And Expected Salary
                        Country country = null;
                        if (SiteSetting[DefaultSiteSetting.Country.ToString()] != null)
                            country = Facade.GetCountryById(Convert.ToInt32(SiteSetting[DefaultSiteSetting.Country.ToString()]));

                        string[] payCycle = { "", " / Hour", " / Day", " / Month", " / Year" };
                        lblCandidateCurrentSalary.Text = "";
                        lblCandidateExpectedSalary.Text = "";

                        string expectedrate = "";
                        string expectedmaxRate = "";

                        if (hiringMatrix.CurrentYearlyRate != null && hiringMatrix.CurrentYearlyRate != 0) lblCandidateCurrentSalary.Text = hiringMatrix.CurrentYearlyCurrency  + " " + hiringMatrix.CurrentYearlyRate.ToString() + (hiringMatrix.CurrentYearlyCurrency =="INR" ? " Lacs" : "") +  payCycle[hiringMatrix.CurrentSalaryPayCycle];
                        if (hiringMatrix.ExpectedYearlyRate != null && hiringMatrix.ExpectedYearlyRate != 0 && hiringMatrix.ExpectedYearlyMaxRate != null && hiringMatrix.ExpectedYearlyMaxRate != 0) lblCandidateExpectedSalary.Text = hiringMatrix.ExpectedYearlyCurrency + " " + hiringMatrix.ExpectedYearlyRate.ToString() + " - " + hiringMatrix.ExpectedYearlyMaxRate.ToString() + (hiringMatrix.ExpectedYearlyCurrency =="INR" ? " Lacs":"") +  payCycle[hiringMatrix.ExpectedSalaryPayCycle];

                        if (CurrentJobPosting.PayRate != "" && CurrentJobPosting.PayRate != "Open" && Convert.ToDouble(CurrentJobPosting.PayRate) > 0)
                        {
                            if (Convert.ToDecimal(CurrentJobPosting.PayRate) >= hiringMatrix.ExpectedYearlyRate && Convert.ToDecimal(CurrentJobPosting.PayRate) <= hiringMatrix.ExpectedYearlyMaxRate)
                            {
                                if (hiringMatrix.ExpectedYearlyRate != null && hiringMatrix.ExpectedYearlyRate != 0 && hiringMatrix.ExpectedYearlyMaxRate != null && hiringMatrix.ExpectedYearlyMaxRate != 0) lblCandidateExpectedSalary.Text = "<span class=highlight>" + lblCandidateExpectedSalary.Text + "</span>";
                            }
                        }
                        ////Binding Skills
                        string skill = hiringMatrix.Skills;
                        lblCandidateSkills.Text = skill.Length <= 50 ? skill : (skill.Substring(0, 50) + "...");
                        lblCandidateSkills.Text = HighLightSkillName(lblCandidateSkills.Text);
                        lblCandidateSkills.ToolTip = skill;

                        ////BindingLast updated Date
                        lblCandidateLastUpdate.Text = hiringMatrix.UpdateDate.ToShortDateString();// candidate.UpdateDate.ToShortDateString();
                        //Binding Remarks
                        try
                        {
                            lblCandidateObjective.Text = (hiringMatrix.Objective.Length <= 50 ? MiscUtil.RemoveScript(hiringMatrix.Objective) : MiscUtil.RemoveScript(hiringMatrix.Objective.Substring(0, 50) + "..."));//1.1
                            lblCandidateObjective.ToolTip = MiscUtil.RemoveScript(hiringMatrix.Objective == null ? "" : hiringMatrix.Objective);
                        }
                        catch
                        {
                        }
                        if (hiringMatrix.Remarks != null)
                        {
                            lblRemarks.Text = ((hiringMatrix.Remarks.Length <= 50) ? hiringMatrix.Remarks : (hiringMatrix.Remarks.Substring(0, 50) + " ..."));
                            lblRemarks.ToolTip = hiringMatrix.Remarks;
                        }
                        //Building CandidateType
                        lblCandidateType.Text = hiringMatrix.MemberType;

                        //Binding Work Schedule
                        if (hiringMatrix.WorkScheduleLookupId == CurrentJobPosting.WorkScheduleLookupId && hiringMatrix.WorkScheduleLookupId != 0)
                        {
                            lblWorkSchedule.Text = "<span class=highlight>" + hiringMatrix.WorkSchedule + "</span>";
                        }
                        else lblWorkSchedule.Text = hiringMatrix.WorkSchedule;

                        //Binding Highest Degree
                        if (hiringMatrix.EducationLevelIDs != "" && Convert.ToInt32(hiringMatrix.EducationLevelIDs) > 0)
                        {
                            if (CurrentJobPosting.RequiredDegreeLookupId != "" && ("," + CurrentJobPosting.RequiredDegreeLookupId + ",").Contains(hiringMatrix.EducationLevelIDs))
                            {
                                lblHighestDegree.Text = "<span class=highlight>" + hiringMatrix.HighestDegree + "</span>";
                            }
                            else lblHighestDegree.Text = hiringMatrix.HighestDegree;
                        }
                        else lblHighestDegree.Text = hiringMatrix.HighestDegree;
                        System.Web.UI.HtmlControls.HtmlGenericControl liInterview = (System.Web.UI.HtmlControls.HtmlGenericControl)e.Item.FindControl("liInterview");
                        if (liInterview != null)
                        {
                            if (isAccess)
                            {
                                HyperLink lnkSchedule = (HyperLink)e.Item.FindControl("lnkSchedule");
                                if (lnkSchedule != null)
                                {
                                    SecureUrl url = UrlHelper.BuildSecureUrl(UrlConstants.ATS.ATS_INTERNALINTERVIEW_SCHEDULE, string.Empty, UrlConstants.PARAM_MEMBER_ID, hiringMatrix.MemberId.ToString(), UrlConstants.PARAM_JOB_ID, base.CurrentJobPostingId.ToString(), UrlConstants.PARAM_SITEMAP_ID, "403");
                                    lnkSchedule.NavigateUrl = url.ToString();
                                }
                                liInterview.Visible = true;
                            }
                            else
                                liInterview.Visible = false;
                        }


                        MemberHiringDetails memHiringDetails = Facade.GetMemberHiringDetailsByMemberIdAndJobPostingID(hiringMatrix.MemberId, base.CurrentJobPostingId);
                        if (memHiringDetails != null)
                        {
                            //row.Style.Add (HtmlTextWriterStyle.BackgroundColor, "#A9F5BC");
                            //row.Attributes["class"] = "hiredrow";
                            lnkHiringDetails.Text = "Edit Offer Details";
                            lnkRejectCandidate.Text = "Reject Candidate";
                        }
                        else liHiringDetails.Style .Add ("display","none");// = false;
                        MemberJoiningDetail joinDetails = Facade.GetMemberJoiningDetailsByMemberIdAndJobPostingID(hiringMatrix.MemberId, CurrentJobPostingId);
                        if (joinDetails != null)
                            lnkJoiningDetails.Text = "Edit Joining Details";
                        else
                            liJoiningDetails.Style.Add("display", "none");
                }
                 */
            }
        }
       
        protected void lsvCandidateList_PreRender(object sender, EventArgs e)
        {
            try
            {
                ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvCandidateList.FindControl("pagerControl");
                if (PagerControl != null)
                {
                    DataPager pager = (DataPager)PagerControl.FindControl("pager");
                    if (pager != null)
                    {
                        DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                        if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
                    }
                    HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
                    if (hdnRowPerPageName != null) hdnRowPerPageName.Value = "HiringMatrixRowPerPage";
                }
                //EnableDiableButtons();
                PlaceUpDownArrow();
                //System.Web.UI.HtmlControls.HtmlTableCell thInterview = (System.Web.UI.HtmlControls.HtmlTableCell)lsvCandidateList.FindControl("thInterview");
                //foreach (ListViewDataItem item in lsvCandidateList.Items)
                //{
                //    //System.Web.UI.HtmlControls.HtmlTableCell tdInterview = (System.Web.UI.HtmlControls.HtmlTableCell)item.FindControl("tdInterview");
                //    System.Web.UI.HtmlControls.HtmlGenericControl bInterview = (System.Web.UI.HtmlControls.HtmlGenericControl)item.FindControl("bInterview");
                //    if (hdnIsHide .Value =="" )
                //    {
                //        bInterview.Visible = false;
                //    }
                //}
                if (lsvCandidateList.Controls.Count == 0)
                {
                    lsvCandidateList.DataSource = null;
                    lsvCandidateList.DataBind();
                }
                HtmlTable tblEmptyData = (HtmlTable)lsvCandidateList.Controls [0].FindControl("tblEmptyData");
                dvbulkAction.Visible = tblEmptyData == null;

                foreach (ListViewDataItem item in lsvCandidateList.Items)
                {
                    DropDownList ddlStatus = (DropDownList)item.FindControl("ddlStatus");
                    HiddenField hdnCurrentlevelId = (HiddenField)item.FindControl("hdnCurrentlevelId");
                    ControlHelper.SelectListByValue(ddlStatus, hdnCurrentlevelId.Value);
                }
            }
            catch
            {
            }
        }
    
        protected void lsvCandidateList_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Sort")
                {
                    LinkButton lnkbutton = (LinkButton)e.CommandSource;
                    if (txtSortColumn.Text == lnkbutton.ID)
                    {
                        if (txtSortOrder.Text == "ASC")txtSortOrder.Text = "DESC";
                        else txtSortOrder.Text = "ASC";
                    }
                    else
                    {
                        txtSortColumn.Text = lnkbutton.ID;
                        txtSortOrder.Text = "ASC";
                    }
                }
            }
            catch
            {
            }
            int id;
            int.TryParse(e.CommandArgument.ToString(), out id);
            if (id > 0)
            {
                if (string.Equals(e.CommandName, "MoveToNextLevel"))
                {
                    Facade.MemberJobCart_MoveToNextLevel(HiringMatrixInterviewLevel, base.CurrentMember.Id, e.CommandArgument.ToString(), CurrentJobPosting.Id,"Next");
                    BindList();
                    MiscUtil.ShowMessage(lblMessage, "Successfully moved 1 candidate to " + hdnNextLevelName .Value , false);
                }
                else if (string.Equals(e.CommandName, "MoveToPreviousLevel"))
                {
                    txtSelectedIds.Text = e.CommandArgument.ToString();
                    Facade.MemberJobCart_MoveToNextLevel(HiringMatrixInterviewLevel, base.CurrentMember.Id, txtSelectedIds.Text, CurrentJobPosting.Id, ("Previous"));
                    BindList();
                    MiscUtil.ShowMessage(lblMessage, "Successfully moved 1 candidate to " + hdnPreviousLevelName .Value , false);
                }
                else if (string.Equals(e.CommandName, "Remove"))
                {
                    txtSelectedIds.Text = e.CommandArgument.ToString();
                    hdnRemove.Value = "Remove";
                    uclConfirm.AddMessage("Are you sure that you want to remove this candidate  from the Hiring Matrix?", ConfirmationWindow.enmMessageType.Attention, true, true, "");
                    {
                    }
                }
               
                else if (string.Equals(e.CommandName, "EmailRequisition"))
                {
                    SecureUrl url = UrlHelper.BuildSecureUrl(UrlConstants.ApplicationBaseUrl + "Dashboard/" + UrlConstants.Dashboard.NEWMAIL, "", UrlConstants.PARAM_JOB_ID, base.CurrentJobPostingId.ToString(), UrlConstants.PARAM_PAGE_FROM, "HiringMatrix", UrlConstants.PARAM_SELECTED_IDS, e.CommandArgument.ToString());
                    ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "NewMail", "<script>btnMewMail_Click('" + url + "');</script>", false);
                }
                else if (string.Equals(e.CommandName, "SubmitToClient"))
                {
                    SubmitToClient(e.CommandArgument.ToString());
                }
                else if (string.Equals(e.CommandName, "HiringDetails"))
                {
                    ASP.controls_hiringdetails_ascx hireDetails = (ASP.controls_hiringdetails_ascx)this.Page.Master.FindControl("cphHomeMaster").FindControl("ucRequisitionHiringMatrixNavigationTopMenu").FindControl("uclTemplateHiringDetails").FindControl("uclHiringDetails");
                    AjaxControlToolkit.ModalPopupExtender ext = (AjaxControlToolkit.ModalPopupExtender)this.Page.Master.FindControl("cphHomeMaster").FindControl("ucRequisitionHiringMatrixNavigationTopMenu").FindControl("mpeHiringDetails");
                    hireDetails.JobPostingId = CurrentJobPostingId;
                    hireDetails .MemberID =e.CommandArgument .ToString ();
                    hireDetails.StatusId = 0;
                    ext.Enabled = true;
                    ext.Show();
                }
                else if (string.Equals(e.CommandName, "RejectCandidate"))
                {
                    ASP.controls_rejectdetails_ascx candidate = (ASP.controls_rejectdetails_ascx)this.Page.Master.FindControl("cphHomeMaster").FindControl("ucRequisitionHiringMatrixNavigationTopMenu").FindControl("uclTemplateReject").FindControl("uclRejectCandidate");
                    AjaxControlToolkit.ModalPopupExtender ext = (AjaxControlToolkit.ModalPopupExtender)this.Page.Master.FindControl("cphHomeMaster").FindControl("ucRequisitionHiringMatrixNavigationTopMenu").FindControl("mpeRejectCandidate");
                    candidate.MemberID = e.CommandArgument.ToString();
                    candidate.HiringMatrixLevel = HiringMatrixInterviewLevel;
                    candidate.JobPostingId = CurrentJobPostingId;
                    ext.Enabled = true;
                    ext.Show();
                }
                else if(string.Equals (e.CommandName ,"SubmissionDetails"))
                {
                    ASP.controls_submissiondetails_ascx submission = (ASP.controls_submissiondetails_ascx)this.Page.Master.FindControl("cphHomeMaster").FindControl("ucRequisitionHiringMatrixNavigationTopMenu").FindControl("uclTemplateSubmissions").FindControl("uclSubmissionDetails");
                    AjaxControlToolkit.ModalPopupExtender ext = (AjaxControlToolkit.ModalPopupExtender)this.Page.Master.FindControl("cphHomeMaster").FindControl("ucRequisitionHiringMatrixNavigationTopMenu").FindControl("mpeSubmissionDetails");
                    submission.JobPostingId = CurrentJobPostingId;
                    submission.MemberID = e.CommandArgument .ToString ();
                    
                    ext.Enabled = true;
                    ext.Show();
                }
                else if (string.Equals(e.CommandName, "JoiningDetails"))
                {
                    ASP.controls_joiningdetails_ascx joined = (ASP.controls_joiningdetails_ascx)this.Page.Master.FindControl("cphHomeMaster").FindControl("ucRequisitionHiringMatrixNavigationTopMenu").FindControl ("uclTemplateJoining").FindControl("uclJoiningDetails");
                    AjaxControlToolkit.ModalPopupExtender ext = (AjaxControlToolkit.ModalPopupExtender)this.Page.Master.FindControl("cphHomeMaster").FindControl("ucRequisitionHiringMatrixNavigationTopMenu").FindControl("mpeJoiningDetails");
                    joined.JobPostingId = CurrentJobPostingId;
                    joined.MemberID = e.CommandArgument.ToString();
                    ext.Enabled = true;
                    ext.Show();
                }
                else if (string.Equals(e.CommandName, "HiringLog"))
                {
                    ASP.controls_candidateactionlog_ascx actionLog = (ASP.controls_candidateactionlog_ascx)this.Page.Master.FindControl("cphHomeMaster").FindControl("ucRequisitionHiringMatrixNavigationTopMenu").FindControl("uclTemplateActionLog").FindControl("uclActionLog");
                    ASP.controls_modaltemplate_ascx modalTemplate = (ASP.controls_modaltemplate_ascx)this.Page.Master.FindControl("cphHomeMaster").FindControl("ucRequisitionHiringMatrixNavigationTopMenu").FindControl("uclTemplateActionLog");
                    AjaxControlToolkit.ModalPopupExtender ext = (AjaxControlToolkit.ModalPopupExtender)this.Page.Master.FindControl("cphHomeMaster").FindControl("ucRequisitionHiringMatrixNavigationTopMenu").FindControl("mpeActionLog");
              
                    actionLog.MemberID = e.CommandArgument.ToString();
                    modalTemplate .ModalTitle = actionLog.getModalTitleWithCandidateName();
                    ext.Enabled = true;
                    ext.Show();
                }
            }
        }
      
        T Cast<T>(object obj, T type) 
        { return (T)obj; }

        #endregion                       
      
}

    public class LineChartEntity
    {
        public string XAxis
        {
            get;
            set;
        }
        public int YAxis
        {
            get;
            set;
        }
    }

}
