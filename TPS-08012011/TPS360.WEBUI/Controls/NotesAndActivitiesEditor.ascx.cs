﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: NotesAndActivitiesEditor.ascx.cs
    Description: This is the user control page used for member notes edit functionality
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author               Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1            Oct-16-2008           Yogeesh Bhat         Defect ID: 8968; Changes made in lsvNotesList_ItemDataBound() method.
                                                              (checked member object for null)
    0.2            Nov-14-2008           Jagadish             Defect ID: 9070; Alert message was repeating. Changed the code to show appropriate message 
                                                              and disabled the delete button when edit button for any document clicked.
    0.3            Feb-03-2009           Kalyani Pallagani    Defect Id : 9756; Added code to clear the note and noteid when note has been deleted
-------------------------------------------------------------------------------------------------------------------------------------------        
*/

using System;
using System.Text;
using System.Web.UI.WebControls;
using TPS360.BusinessFacade;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Web.UI.Helper;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Generic;
namespace TPS360.Web.UI
{
    public partial class ControlNotesAndActivitiesEditor : BaseControl
    {
        #region Member Variables

        private int _memberId 
        {
            
            
            get 
            {
                int mId = 0;

                if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]))
                {
                    mId = Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]);

                }

                return mId;
            }
        }
        CommonNote _comonNotes = null;

        //public int memberId
        //{
        //    set { _memberId = value; }
        //}

        #endregion

        #region Properties

        public int CommonNoteId
        {
            get
            {
                return (int)(ViewState[this.ClientID + "_CommonNoteId"] ?? 0);
            }
            set
            {
                ViewState[this.ClientID + "_CommonNoteId"] = value;
            }
        }
        private CommonNote CurrentNote
        {
            get
            {
                if (_comonNotes == null)
                {
                    if (CommonNoteId > 0)
                    {
                        _comonNotes = Facade.GetCommonNoteById(CommonNoteId);
                    }
                    
                    if (_comonNotes == null)
                    {
                        _comonNotes = new CommonNote();
                    }
                }

                return _comonNotes;
            }
        }
        #endregion

        #region Methods

        private void BindList()
        {
            odsNotesList.SelectParameters["memberId"].DefaultValue = _memberId.ToString();
            this.lsvNotesList.DataBind();
        }
        private void SaveNotsAndActivities()
        {
            if (IsValid)
            {
                try
                {
                    CommonNote notsAndActivities = BuildNotsAndActivities();
                    MemberNote memberNote = BuildMemberNot();
                    txtNotes.Text =MiscUtil .RemoveScript(txtNotes.Text);
                    if (notsAndActivities.IsNew)
           
                    {
                        if (txtNotes.Text.ToString() != string.Empty)
                        {
                            notsAndActivities = Facade.AddCommonNote(notsAndActivities);
                            memberNote.CommonNoteId = notsAndActivities.Id;
                            Facade.AddMemberNote(memberNote);
                            MiscUtil.ShowMessage(lblMessage, "Note has been added succesfully.", false);
                        }
                    }
                    else
                    {
                        if (txtNotes.Text.ToString() != string.Empty)
                        {
                            Facade.UpdateCommonNote(notsAndActivities);
                            MiscUtil.ShowMessage(lblMessage, "Note has been updated succesfully.", false);
                        }
                    }
                    //BindList();
                    ClearText();
                }
                catch (ArgumentException ex)
                {
                    MiscUtil.ShowMessage(lblMessage, ex.Message, true);
                }
            }

           
               
        }

        private CommonNote BuildNotsAndActivities()
        {
            CommonNote commonNote = null;
            if (!CurrentNote.IsNew)
            {
                commonNote = CurrentNote;
            }
            else
            {
                commonNote = new CommonNote();
            }
            commonNote.NoteCategoryLookupId = Convert.ToInt32(ddlNoteCategory.SelectedValue);
            commonNote.NoteDetail = MiscUtil.RemoveScript(txtNotes.Text.Trim());

            commonNote.CreatorId = base.CurrentMember.Id;
            commonNote.UpdatorId = base.CurrentMember.Id;
            return commonNote;
        }

        private MemberNote BuildMemberNot()
        {
            MemberNote memberNote = new MemberNote();

            if (_memberId > 0)
            {
                memberNote.MemberId = _memberId;
            }
            else
            {
                memberNote.MemberId = base.CurrentMember.Id;
            }
            memberNote.CreatorId = base.CurrentMember.Id;
            return memberNote;
        }

        private void PrepareView()
        {
            //if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]))
            //{
            //    _memberId = Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]);

            //}

            MiscUtil.PopulateNotesType(ddlNoteCategory, Facade);
            ListItem li=ddlNoteCategory.Items.FindByText("Added from Parser");
            if (li.Text == "Added from Parser")
                ddlNoteCategory.Items.Remove(li);
           
            


        }

        private void PrepareEditView()
        {
            CommonNote notesActivities = CurrentNote;
            ddlNoteCategory.SelectedValue = Convert.ToString(notesActivities.NoteCategoryLookupId);
            txtNotes.Text =MiscUtil .RemoveScript ( notesActivities.NoteDetail,string .Empty );
            



        }

        private void PlaceUpDownArrow()
        {
             
            try
            {
                LinkButton lnk = (LinkButton)lsvNotesList.FindControl(txtSortColumn.Text);
                System.Web.UI.HtmlControls.HtmlTableCell im = (System.Web.UI.HtmlControls.HtmlTableCell)lnk.Parent;
                im.EnableViewState = false;
                im.Attributes.Add("class", ( txtSortOrder.Text == "ASC" ? "Ascending" : "Descending"));
            }
            catch
            {
            }
        }


        
        
        //private bool CheckFileSize()
        //{
        //    decimal fileSize = Convert.ToDecimal(Convert.ToDecimal(fuDocument.FileContent.Length) / (1024 * 1024));
        //    if (fileSize > Convert.ToDecimal(3.0))
        //    {
        //        return false;
        //    }
        //    else
        //    {
        //        return true;
        //    }
        //}

        //private string GetDocumentLink(string strFileName, string strDocumenType)
        //{

        //    string strFilePath = MiscUtil.GetMemberDocumentPath(this.Page, _memberId, strFileName, strDocumenType, true);

        //    if (strFilePath != strFileName)
        //    {
        //        return "<a href='" + strFilePath + "' target='_blank'>" + strFileName + "</a>"; // 0.1
        //    }
        //    else
        //    {
        //        return strFileName + "(File not found)";
        //    }
        //}




    

        
        private void ClearText()
        {
            ddlNoteCategory.SelectedIndex = 0;
            txtNotes.Text = string.Empty;
            CommonNoteId = 0;
        }

        #endregion

        #region Events

        #region Page event

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                txtNotes.Focus();
                PrepareView();
                BindList();
                txtSortColumn.Text = "btnDateTime";
                txtSortOrder.Text = "Desc";
                PlaceUpDownArrow();
                

            }
            lblMessage.Text = "";
            lblMessage.CssClass = "";

            //Comment For Hiding Action Column for all users
            //bool _isAdmin = base.IsUserAdmin;
            //if (!_isAdmin)
            //{
            //    System.Web.UI.HtmlControls.HtmlTableCell thAction = (System.Web.UI.HtmlControls.HtmlTableCell)lsvNotesList.FindControl("thAction");
            //   if(thAction!=null) thAction.Visible = false;
            //}
            string pagesize = "";
            pagesize = (Request.Cookies["NotesAndActivitiesRowPerPage"] == null ? "" : Request.Cookies["NotesAndActivitiesRowPerPage"].Value);
            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvNotesList.FindControl("pagerControl_Requisition");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)PagerControl.FindControl("pager");
                if (pager != null) pager.PageSize = Convert.ToInt32(pagesize == "" ? "10" : pagesize);
            }



        }

    
         protected void btnSave_Click(object sender, EventArgs e)
        {
            SaveNotsAndActivities();
            PlaceUpDownArrow();
        }

        #endregion

        #region ListView Events

        protected void lsvNotesList_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            bool _isAdmin = base.IsUserAdmin;
           

              
            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                CommonNote commonNote = ((ListViewDataItem)e.Item).DataItem as CommonNote;

                if (commonNote != null)
                {
                    Label lblDateTime = (Label)e.Item.FindControl("lblDateTime");
                    Label lblManager = (Label)e.Item.FindControl("lblManager");
                    Label lblNoteCategory = (Label)e.Item.FindControl("lblNoteCategory");
                    Label lblNote = (Label)e.Item.FindControl("lblNote");

                    ImageButton btnEdit = (ImageButton)e.Item.FindControl("btnEdit");
                    ImageButton btnDelete = (ImageButton)e.Item.FindControl("btnDelete");

                    lblDateTime.Text = commonNote.CreateDate.ToShortDateString() + " " + commonNote.CreateDate.ToShortTimeString();
                    Member member = Facade.GetMemberById(commonNote.CreatorId);
                    lblManager.Text = "";   

                    if (member != null)     
                    {
                        lblManager.Text = member.FirstName + " " + member.MiddleName + " " + member.LastName;
                    }


                    if (commonNote.NoteCategoryLookupId > 0)
                    {
                        GenericLookup g = Facade.GetGenericLookupById(commonNote.NoteCategoryLookupId);
                        //Label lblDocument =e.Item.FindControl("lblDocument") as Label;
                        if (g != null)
                            lblNoteCategory.Text = g.Name;
                        //lblDocument.Text = GetDocumentLink(commonNote.Filename, g.Name);
                    }
                    if (commonNote.JobPostingId != null && commonNote.JobPostingId > 0)
                        lblNote .Text =commonNote .JobPostingCode +" - "+commonNote .JobTitle +"<br/>"+commonNote .NoteDetail.Replace("\r\n", "</br>");
                    else 
                        lblNote.Text = commonNote.NoteDetail.Replace("\r\n", "</br>");

                    //Comment For Hiding Action Column for all users
                    //if (!_isAdmin)
                    //{
                    //    System.Web.UI.HtmlControls.HtmlTableCell tdAction = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdAction");
                    //    tdAction.Visible = false;
                    //    System.Web.UI.HtmlControls.HtmlTableCell thAction = (System.Web.UI.HtmlControls.HtmlTableCell)lsvNotesList.FindControl("thAction");
                    //    if (thAction != null) thAction.Visible = false;
                    //}
                    //else
                    //{
                    //    btnDelete.Visible = _isAdmin;
                    //    btnDelete.OnClientClick = "return ConfirmDelete('note')";
                    //    btnEdit.CommandArgument = btnDelete.CommandArgument = StringHelper.Convert(commonNote.Id);
                    btnEdit.CommandArgument = StringHelper.Convert(commonNote.Id);
                    //}
                }


            }
          




        }
        protected void lsvNotesList_PreRender(object sender, EventArgs e)
        {

            System.Web.UI.HtmlControls.HtmlTableRow  row = (System.Web.UI.HtmlControls.HtmlTableRow )lsvNotesList.FindControl("row");
            if (row == null)
            {
                lsvNotesList.DataSource = null;
                lsvNotesList .DataBind ();
            }

            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvNotesList.FindControl("pagerControl_Requisition");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)PagerControl.FindControl("pager");
                if (pager != null)
                {
                    DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                    if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
                }
                HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
                if (hdnRowPerPageName != null)
                {
                   hdnRowPerPageName.Value = "NotesAndActivitiesRowPerPage";
                }
            }
            PlaceUpDownArrow();
           
           
        }
        protected void lsvNotesList_ItemCommand(object sender, ListViewCommandEventArgs e)
        {

            try
            {

                if (e.CommandName == "Sort")
                {
                    LinkButton lnkbutton = (LinkButton)e.CommandSource;
                    if (txtSortColumn.Text == lnkbutton.ID)
                    {
                        if (txtSortOrder.Text == "ASC") txtSortOrder.Text = "DESC";
                        else txtSortOrder.Text = "ASC";
                    }
                    else
                    {
                        txtSortColumn.Text = lnkbutton.ID;
                        txtSortOrder.Text = "ASC";
                    }
                }
            }
            catch
            {
            }

            int id;

            int.TryParse(e.CommandArgument.ToString(), out id);

            if (id > 0)
            {
                if (string.Equals(e.CommandName, "EditItem"))
                {
                    CommonNoteId = id;
                    PrepareEditView();
                    int rows = lsvNotesList.Items.Count;
                    //for (int i = 0; i < rows; i++)
                    //{
                    //    ListViewDataItem row = lsvNotesList.Items[i];
                    //    ImageButton delete = (ImageButton)row.FindControl("btnDelete");
                    //    delete.Enabled = false;
                    //}
                }
                else if (string.Equals(e.CommandName, "DeleteItem"))
                {
                    try
                    {
                        if (Facade.DeleteCommonNoteById(id))
                        {
                            BindList();
                            MiscUtil.ShowMessage(lblMessage, "Notes and activities has been successfully deleted.", false);
                        }
                        if (CommonNoteId  == id)
                            ClearText();

                    }
                    catch (ArgumentException ex)
                    {
                        MiscUtil.ShowMessage(lblMessage, ex.Message, true);
                    }
                }
            }
            BindList();
        }

        #endregion

    

        #endregion
    }
}
