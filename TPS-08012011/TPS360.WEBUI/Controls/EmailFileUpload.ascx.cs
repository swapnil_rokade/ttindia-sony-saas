/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: EmailFileUpload.ascx.cs
    Description: This is the user control page that used for uploading files.
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1              Oct-15-2008          Jagadish N            Defect id: 8850; When attaching two files of same name it was giving server error.
                                                                Fixed the error to upload file successfully.
    0.2              Nov-14-2008          Jagadish N            Defect id: 9055; Added a property 'UploadedFiles' to clear the session.
    0.3              Dec-15-2008          Jagadish N            Defect id: 9521; Changes made in SourceRowDataBound() method.
 *  0.4              Feb-02-2009          N.Srilakshmi         Defect ID: 9641,9642;Added triggers for btnUpload control in .ascx
    0.5              Mar-03-2009          N.Srilakshmi          Defect Id: 10209,10208,10205;changes made in SourceRowDataBound   
-------------------------------------------------------------------------------------------------------------------------------------------       
 */
using System;
using System.Collections;
using System.Collections.Specialized;
using System.IO;
using System.Web.UI.WebControls;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Web.UI.Helper;
using System.Web;

namespace TPS360.Web.UI
{
    public partial class ctlEmailFileUpload : BaseControl
    {
        private string _sessionKey;
        
        public string TempDirectory
        {
            get
            {
                return (string)Helper.Session.Get("tempDir");
            }
            set
            {
                Helper.Session.Set("tempDir", value);
            }
        }
        public string PreviewDir
        {
            get;
            set;
        }
        public string SessionKey
        {
            get
            {
                return _sessionKey;
            }
            set
            {
                _sessionKey = value;
            }
        }
        
        public StringDictionary Files
        {
            get
            {
                if (Helper.Session.Get(SessionKey) == null)
                {
                    Helper.Session.Set(SessionKey, new StringDictionary());
                }

                return (StringDictionary) Helper.Session.Get(SessionKey);
            }
        }

        public StringDictionary UploadedFiles
        {
            get
            {
                if (Helper.Session.Get("EmailFileUpload") == null)
                {
                    Helper.Session.Set("EmailFileUpload", new StringDictionary());
                }

                return (StringDictionary)Helper.Session.Get("EmailFileUpload");
            }
            set
            {
                Helper.Session.Set("EmailFileUpload", value);
            }
        }
        public string UploadedFilesText
        {
            get
            {
                return lblUploadedFilesText.Text;
            }
            set
            {
                lblUploadedFilesText.Text = value;
            }
        }

        public void BindFiles()
        {
            try
            {
                //bool f = System.Text.RegularExpressions.Regex.IsMatch(temp.Key.ToString(), @"^[a-zA-Z]+$");
                //foreach (DictionaryEntry temp in Files)
                //{
                //    if (temp.Key.ToString().Contains("$%") && temp.Value.ToString().Contains("EmailDocuments"))
                //    {
                //        Files.Remove(temp.Key.ToString());
                //    }
                //}
                string[] email;
                string emailid, emailvalue;
                foreach (DictionaryEntry temp in Files)
                {
                    if (temp.Key.ToString().Contains("$%"))
                    {
                        email = temp.Key.ToString().Split(new string[] { "$%" }, StringSplitOptions.RemoveEmptyEntries);
                        string[] tempfilename = temp.Key.ToString().Split(';');
                        if (email.Length == 1 || tempfilename.Length > 2)
                        {
                            emailid = tempfilename.Length > 2 ? email[email.Length - 1] : string.Empty;
                            emailvalue = tempfilename.Length > 2 ? temp.Value.ToString() : string.Empty;
                            if (tempfilename.Length > 2)
                            {
                                foreach (string tempkey in tempfilename)
                                {
                                    if (!tempkey.Contains("$%"))
                                        Files.Add(tempkey + "$%" + emailid, emailvalue);
                                }
                            }
                            Files.Remove(temp.Key.ToString());
                        }
                    }
                }
            }
            catch { }
            if (Files.Count > 0) gvFiles.DataSource = Files;
            else gvFiles.DataSource = null;
            gvFiles.DataBind();
            tdUploadedFiles.Visible = (Files.Count > 0);
            tdAttachFile.Visible = (Files.Count > 0); 
        }

        private string  GetTempFolder()
        {
            if (StringHelper.IsBlank(TempDirectory))
            {
                string dir = CurrentUserId.ToString();
                string path = Path.Combine(UrlConstants.TempDirectory, dir);

                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                TempDirectory = path;
            }
            if (hdnTmpFolder.Value == string.Empty)
            {
                DateTime dt = DateTime.Now;
                string s = dt.Year.ToString() + dt.Month.ToString() + dt.Day.ToString() + dt.Hour.ToString() + dt.Minute.ToString() + dt.Second.ToString() + dt.Millisecond.ToString() + "";
                if (!Directory.Exists(TempDirectory + "\\" + s))
                {
                    Directory.CreateDirectory(TempDirectory + "\\" + s);
                    hdnTmpFolder.Value = s;
                    PreviewDir = TempDirectory + "\\" + s;
                }
            }
            else
            {
                PreviewDir = TempDirectory + "\\" + hdnTmpFolder .Value ;
            }
            return PreviewDir;
        }
        private void UploadFile()
        {
            if (!filSource.HasFile)
            {
                return;
            }
            if (true)  //IsValid
            {
                PreviewDir = GetTempFolder();
                string fileName = Path.GetFileName(filSource.PostedFile.FileName);
                string filePath = Path.Combine(PreviewDir, fileName);
                filSource.PostedFile.SaveAs(filePath);
                if (Files.ContainsKey(fileName))
                   Files.Remove(fileName);
                Files.Add(fileName, filePath + "?" + filSource.PostedFile.ContentLength.ToString());
                BindFiles();
                txtTitle.Text = string.Empty;
              }
        }
        

        private void DeleteFile(string fileName)
        {
            PreviewDir = GetTempFolder();
            string filePath = Path.Combine(PreviewDir, fileName);
            File.Delete(filePath);
            Files.Remove(fileName);
        }
        public void setClear()
        {
            hdnClear.Value = "No";
        }
        protected void UploadClicked(object sender, EventArgs e)
        {
            UploadFile();
        }

        protected void SourceRowDataBound(object sender, GridViewRowEventArgs e)
        {
            string[] Filex;
            
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DictionaryEntry entry = (DictionaryEntry) e.Row.DataItem;

                HyperLink lnkFileName = (HyperLink)e.Row.FindControl("lnkFileName");
                Filex = entry.Value.ToString().Split((char)Convert.ToChar("?"));
                if (hdnTmpFolder.Value != string.Empty)
                {
                    if (File.Exists(Filex[0]))
                    {
                        string fileUrlName = Path.GetFileName(Filex[0]);
                        string fileUrl = "../Temp/" + CurrentUserId.ToString() + "/" + hdnTmpFolder.Value + "/" + fileUrlName;//System.Uri.EscapeDataString(Path.GetFileName(fileUrlName));
                        
                        if (File.Exists(UrlConstants.ApplicationPath  + fileUrl.Substring(3).Replace("/","\\").Replace ("%20"," ")))
                        {
                            Helper.Session.Set(SessionConstants.UserId, CurrentUserId.ToString());
                            ControlHelper.SetHyperLink(lnkFileName, fileUrl, entry.Key.ToString());
                        }
                        else
                        {
                            string DisplayResume = string.Empty;
                            string path = string.Empty;
                            path = DataBound(entry.Key.ToString(), out DisplayResume);
                            if (path != string.Empty && DisplayResume != string.Empty)
                                ControlHelper.SetHyperLink(lnkFileName, path, DisplayResume);  
                        }
                    }
                    else
                    {
                        try
                        {
                            string DisplayResume = string.Empty;
                            string path = string.Empty;
                            path = DataBound(entry.Key.ToString(), out DisplayResume);
                            if (path != string.Empty && DisplayResume != string.Empty)
                                ControlHelper.SetHyperLink(lnkFileName, path, DisplayResume);
                        }
                        catch { }
                    }
                }
                else
                {
                    string DisplayResume = string.Empty;
                    string path=string .Empty ;
                    path=DataBound(entry.Key.ToString(),out DisplayResume );
                    if(path !=string .Empty && DisplayResume !=string .Empty )
                        ControlHelper.SetHyperLink(lnkFileName, path, DisplayResume);   
                }

                ((ImageButton )e.Row.FindControl("btnDelete")).CommandArgument = StringHelper.Convert(((DictionaryEntry)e.Row.DataItem).Key);
            }
        }

        protected void SourceRowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (StringHelper.IsEqual(e.CommandName, "DeleteFile"))
            {
                DeleteFile(StringHelper.Convert(e.CommandArgument));
                BindFiles();
                tdUploadedFiles.Visible = (Files.Count > 0);
                tdAttachFile.Visible = (Files.Count > 0); 
            }
        }
        private string  DataBound(string Key,out string display)
        {
            string[] email;
            string path = string.Empty;
            string DisplayResume = string.Empty;
            if (Key.ToString().Contains("$%"))
            {
                email = Key.ToString().Split(new string[] { "$%" }, StringSplitOptions.RemoveEmptyEntries);
                if (email.Length == 2)
                {
                    try
                    {
                        MemberEmail memberEmail = Facade.GetMemberEmailById(Int32.Parse(email[1]));
                        if (memberEmail != null)
                        {
                            string fileName = email[0];
                            string s = "";
                            Label att = new Label();
                            
                            if (fileName.Contains("$$$"))
                            {
                                string[] filen = fileName.Split(new string[] { "$$$" }, StringSplitOptions.RemoveEmptyEntries);
                                s = "Resources/Member/" + memberEmail.SenderId + "/Outlook%20Sync/" + memberEmail.Id + "/" + fileName.Replace(" ", "%20") + "/" + filen[0].Replace(" ", "%20");//memberemail.recieverid
                                DisplayResume = filen[0];
                            }
                            else
                            {
                                s = "Resources/Member/" + memberEmail.SenderId + "/Outlook%20Sync/" + memberEmail.Id + "/" + fileName.Replace(" ", "%20");//memberemail.recieverid
                                DisplayResume = fileName;
                            }
                            path = UrlConstants.ApplicationBaseUrl + s;
                            SecureUrl url = UrlHelper.BuildSecureUrl(path, string.Empty);
                            
                        }
                    }
                    catch
                    {
                    }
                }
            }
            display = DisplayResume;
            return path;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                tdUploadedFiles.Visible = (Files.Count > 0);
                tdAttachFile.Visible = (Files.Count > 0); 
                if(hdnClear .Value   ==string .Empty )
                 UploadedFiles.Clear();
            }
        }
}
}