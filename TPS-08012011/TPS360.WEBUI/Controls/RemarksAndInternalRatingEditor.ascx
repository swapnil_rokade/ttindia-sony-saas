﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RemarksAndInternalRatingEditor.ascx.cs"
    Inherits="TPS360.Web.UI.ControlRemarksAndInternalRatingEditor" %>
    
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
    
    
    <script id="igClientScript" type="text/javascript">
    
    
    </script>
<div>
    <div class="TableRow" style="text-align: center">
        <asp:Label ID="lblMessage" runat="server"></asp:Label>
    </div>
   
    <div class ="TableRow">
        <div class ="TableFormLeble">
             <asp:DropDownList ID="ddlInternalRatings" runat="server" CssClass="CommonDropDownList"
                ValidationGroup="Remarks">
            </asp:DropDownList>
        </div>
        <div class ="TableFormContent">
            
            (<asp:Label ID="lblRatingLowest" runat="server" Text="1 = Lowest" />,
            <asp:Image runat="server" ID="imgRatringLowest" />) <br />
            (<asp:Label ID="lblRatingHeight" runat="server" Text="5 = Highest" />,
            <asp:Image runat="server" ID="imgRatingHeighest" />)
        </div>
    </div>
           
           
    
    <br />
    <div class="TableRow" style="text-align: center">
        <asp:Button ID="btnSave" CssClass="CommonButton" runat="server" Text="Save" ValidationGroup="Remarks"
            OnClick="btnSave_Click" />
    </div>
</div>
