﻿<%-- suraj adsule for IJP Report 20160919 --%>
<%@ Control Language="C#" AutoEventWireup="true" Inherits="TPS360.Web.UI.InternalJobPostingReport" CodeFile="InternalJobPostingReport.ascx.cs"  %>
<%@ Register Src="~/Controls/DateRangePicker.ascx" TagName="DateRangePicker" TagPrefix="ucl" %>
<%@ Register Src="~/Controls/PagerControl.ascx" TagName="Pager" TagPrefix="ucl" %>
<%@ Register Src="~/Controls/SmallPagerControl.ascx" TagName="SmallPager" TagPrefix="ucl" %>
<link href="../assets/css/chosen.css" rel="Stylesheet" />
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <link href="../assets/css/chosen.css" rel="Stylesheet" />
<link href="../Style/TabStyle.css" rel="Stylesheet" type="text/css" />
<asp:HiddenField ID="hdnScrollPos" runat="server" />
<asp:HiddenField ID="hdnIsPostback" runat="server" Value="0" />
<asp:HiddenField ID="hdnCurrentMemberJobpostId" runat="server" Value="0" />
<asp:HiddenField ID="hdnMyReport" runat="server" Value="0" />
<asp:TextBox ID="SortColumn" runat="server" EnableViewState="true" Visible="false" />
<asp:TextBox ID="SortOrder" runat="server" EnableViewState="true" Visible="false" />
<div class="well" style="clear: both; padding-top: 0px;">
    <div class="TabPanelHeader">
        Filter Options</div>
    <div style="text-align: center; vertical-align: middle">
        <asp:UpdatePanel ID="upFilter" runat="server">
            <ContentTemplate>
                <asp:HiddenField ID="hdnStartDate" runat="server" />
                <asp:HiddenField ID="hdnEndDate" runat="server" />
                <asp:Label ID="lblSubmissionDate" runat="server" Text="Submission Date:"></asp:Label>
                <ucl:DateRangePicker ID="dtReferralDate" runat="server" />
                <div style="text-align: left; display: inline; margin-top: 10px" id="divReferredBy"
                    runat="server">
                    <asp:Label ID="lblReferredby" runat="server" Text="Referred by:"></asp:Label>
                    <asp:DropDownList ID="ddlRefferedby" runat="server" CssClass="chzn-select" Width="200px"
                        Style="margin-top: 30px;" AutoPostBack="true" OnSelectedIndexChanged="ddlRefferedby_SelectedIndexChanged">
                    </asp:DropDownList>
                </div>
                <asp:Label ID="lblRequisition" runat="server" Text="Requisition:"></asp:Label>
                <div style="text-align: left; display: inline">
                    <asp:DropDownList ID="ddlRequisition" runat="server" CssClass="chzn-select" Width="200px">
                    </asp:DropDownList>
                    <asp:HiddenField ID="hdnRequisition" runat="server" />
                </div>
                <br />
                <asp:Label ID="lblTeam" runat="server" Visible="false" Text="Team:"></asp:Label>
                <div style="text-align: left; display: inline">
                    <asp:DropDownList ID="ddlTeam" runat="server" Visible="false" CssClass="chzn-select"
                        Width="200px">
                    </asp:DropDownList>
                </div>
                <br />
                <asp:Button ID="btnApply" runat="server" Text="Apply" CssClass="btn btn-primary"
                    OnClick="btnApply_Click" />
                <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btn" OnClick="btnReset_Click" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</div>
<div class="tabbable">
    <div class="tab-content">
        <div class="tab-pane active" id="tab2" style="width: 100%; overflow: auto;">
            <asp:UpdatePanel ID="upDetail" runat="server">
                <ContentTemplate>
                    <asp:TextBox ID="txtSortColumn" runat="server" Visible="false"></asp:TextBox>
                    <asp:TextBox ID="txtSortOrder" runat="server" Visible="false"></asp:TextBox>
                    <asp:ObjectDataSource ID="odsReferralDetial" runat="server" SelectMethod="GetPaged_ForInternalDetail"
                        TypeName="TPS360.Web.UI.EmployeeReferralDataSource" SelectCountMethod="GetListCount_ForDetailForIJP"
                        EnablePaging="True" SortParameterName="sortExpression">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="dtReferralDate" Name="StartDate" PropertyName="StartDate"
                                Type="DateTime" />
                            <asp:ControlParameter ControlID="dtReferralDate" Name="EndDate" PropertyName="EndDate"
                                Type="DateTime" />
                            <asp:ControlParameter ControlID="ddlRequisition" Name="JobPostingId" PropertyName="SelectedValue"
                                Type="String" />
                            <asp:ControlParameter ControlID="ddlRefferedby" Name="ReferrerID" PropertyName="SelectedValue"
                                Type="String" />
                            <asp:Parameter Name="IsMyReferralReport" Type="Boolean" DefaultValue="false" />
                            <asp:Parameter Name="CurentMemberJobPostingId" DbType="String" DefaultValue="" />
                            <asp:Parameter Name="IsTeamReport" Type="Boolean" DefaultValue="false" />
                            <asp:Parameter Name="TeamLeaderId" Type="Int32" DefaultValue="0" />
                            <asp:ControlParameter ControlID="ddlTeam" Name="TeamId" Type="Int32" PropertyName="SelectedValue"
                                DefaultValue="0" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                    <div class="TableRow" style="padding-bottom: 5px" id="divExportButtons" runat="server"
                        visible="true">
                        <div class="TabPanelHeader nomargin" visible="false" style="margin-bottom: 5px; border-style: none none none;">
                            Report Results</div>
                        <asp:ImageButton ID="btnExportToExcel" CssClass="btn" SkinID="sknExportToExcel" runat="server"
                            ToolTip="Export To Excel" OnClick="btnExportToExcel_Click" />
                        <asp:ImageButton ID="btnExportToPDF" Visible="false" CssClass="btn" SkinID="sknExportToPDF" runat="server"
                            ToolTip="Export To PDF" OnClick="btnExportToPDF_Click" />
                        <asp:ImageButton ID="btnExportToWord" Visible="false" CssClass="btn" SkinID="sknExportToWord" runat="server"
                            ToolTip="Export To Word" OnClick="btnExportToWord_Click" />
                    </div>
                    <asp:ListView ID="lsvReferralDetail" runat="server" DataKeyNames="Id" DataSourceID="odsReferralDetial"
                        OnItemDataBound="lsvReferralDetail_ItemDataBound" OnItemCommand="lsvReferralDetail_ItemCommand"
                        OnPreRender="lsvReferralDetail_PreRender">
                        <LayoutTemplate>
                            <table id="tlbTemplate" runat="server" class="Grid" cellspacing="0" border="0">
                                <tr>
                                    <th>
                                        <asp:LinkButton runat="server" ID="btnDate" CommandName="Sort" CommandArgument="[ER].[CreateDate]"
                                            Text="Date" ToolTip="Date" />
                                    </th>
                                    <th>
                                        <asp:LinkButton runat="server" ID="btnRequisition" CommandName="Sort" CommandArgument="[J].[JobTitle]"
                                            Text="Requisition" ToolTip="Requisition" />
                                    </th>
                                    <th>
                                        <asp:LinkButton runat="server" ID="btnReferrerPS" CommandName="Sort" CommandArgument="[C].[PSNumber]"
                                            Text="PS#" ToolTip="PS #" />
                                    </th>
                                    <th>
                                        <asp:LinkButton runat="server" ID="lnkId" CommandName="Sort" CommandArgument="[C].[Id]"
                                            Text="Candidate ID #" ToolTip="Candidate ID #" />
                                    </th>
                                    <th>
                                        <asp:LinkButton runat="server" ID="btnCandidate" CommandName="Sort" CommandArgument="[C].[CandidateName]"
                                            Text="Candidate" ToolTip="Candidate" />
                                    </th>
                                    <th>
                                        <asp:LinkButton runat="server" ID="lnkBUName" CommandName="Sort" CommandArgument="[COM].[CompanyName]"
                                            Text="BU" ToolTip="BU" />
                                    </th>
                                    <th>
                                        <asp:LinkButton runat="server" ID="lnkLocation" CommandName="Sort" CommandArgument="[C].[PermanentCity]"
                                            Text="Location" ToolTip="Location" />
                                    </th>
                                    <th>
                                        <asp:LinkButton runat="server" ID="btnCandidateStatus" CommandName="Sort" CommandArgument="[H].[Name]"
                                            Text="Candidate Status" ToolTip="Candidate Status" />
                                    </th>
                                    <th>
                                       <asp:Label runat="server" ID="btnAssignedRecruiters" Text="Assigned Recruiters" ToolTip="Assigned Recruiters" ></asp:Label>
                                       
                                    </th>
                                </tr>
                                <tr id="itemPlaceholder" runat="server">
                                </tr>
                                <tr class="Pager">
                                    <td id="tdpager" runat="server" colspan="9">
                                        <ucl:Pager ID="pagerControl" runat="server" EnableViewState="true" />
                                    </td>
                                </tr>
                            </table>
                        </LayoutTemplate>
                        <EmptyDataTemplate>
                            <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                                <tr>
                                    <td>
                                        No data was returned.
                                    </td>
                                </tr>
                            </table>
                        </EmptyDataTemplate>
                        <ItemTemplate>
                            <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                                <td>
                                    <asp:Label ID="lblDate" runat="server"></asp:Label>
                                </td>
                                <td>
                                    <asp:HyperLink ID="hlkJobTitle" runat="server"></asp:HyperLink>
                                </td>
                                <td>
                                    <asp:Label runat="server" ID="lblReferrerPS" />
                                </td>
                                <td>
                                    <asp:Label runat="server" ID="lblCandidateID" />
                                </td>
                                <td>
                                    <asp:HyperLink ID="hlnkCandidate" runat="server"></asp:HyperLink>
                                </td>
                                <td>
                                    <asp:Label ID="lblBU" runat="server"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblLocation" runat="server"></asp:Label>
                                </td>
                                 <td>
                                    <asp:Label ID="lblCandidateStatus" runat="server"></asp:Label>
                                </td>
                                
                                 <td>
                                    <asp:Label ID="lblAssignedRecruiters" runat="server"></asp:Label>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:ListView>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnExportToPDF" />
                    <asp:PostBackTrigger ControlID="btnExportToExcel" />
                    <asp:PostBackTrigger ControlID="btnExportToWord" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
</div>

<script src="../assets/js/chosen.jquery.js" type="text/javascript"></script>

<script type="text/javascript">
    Sys.Application.add_load(function() {
        $(".chzn-select").chosen();
        $(".chzn-select-deselect").chosen({ allow_single_deselect: true });
    });
</script>


