﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SendEmailList.ascx.cs"
    Inherits="TPS360.Web.UI.ControlSendEmailList" %>
<%@ Register Src="~/Controls/PagerControl.ascx" TagName="Pager" TagPrefix="ucl" %>
<asp:UpdatePanel ID="upSentMail" runat="server">
    <ContentTemplate>
        <asp:ObjectDataSource ID="odsEmailList" runat="server" SelectMethod="GetPaged" OnSelecting="odsEmailList_Selecting"
            TypeName="TPS360.Web.UI.MemberEmailDataSource" SelectCountMethod="GetListCount"
            EnablePaging="True" SortParameterName="sortExpression">
            <SelectParameters>
                <asp:Parameter Name="emailTypeLookupId" DefaultValue="" />
                <asp:Parameter Name="receiverId" DefaultValue="" />
                <asp:Parameter Name="senderId" DefaultValue="" />
                <asp:Parameter Name="company" DefaultValue="" />
                <asp:Parameter Name="AnyKey" DefaultValue="" />
            </SelectParameters>
        </asp:ObjectDataSource>
        <asp:HiddenField ID="hdnSortColumn" runat="server" />
        <asp:HiddenField ID="hdnSortOrder" runat="server" />
        <asp:HiddenField ID="checkings" runat="server" Value="" />
        <div class="HeaderDevider">
        </div>
        <div class="TableRow">
            <asp:Label ID="lblMessage" runat="server" EnableViewState="false"></asp:Label>
        </div>
        <div class="TableRow">
            <asp:Panel ID="pnlSear" runat="server" DefaultButton="btnSearchEmail">
                <div class="TableFormContent" style="margin-left: 170px;">
                    <asp:TextBox ID="txtSearchEmail" runat="server" CssClass="CommonTextBox"></asp:TextBox>
                    <asp:LinkButton ID="btnSearchEmail" runat="server" CssClass="btn btn-primary" Text="Search Email"
                        OnClick="btnSearchEmail_Click" ><i class="icon-search icon-white"></i> Search</asp:LinkButton>
                </div>
            </asp:Panel>
            <div id="drpselect" class="TableFormContent" style="margin-left: 80px;">
                <asp:Label ID="lblcontent" runat="server" Visible="false"></asp:Label>
                <asp:DropDownList ID="ddlcontact" runat="server" CssClass="CommonDropDownList" Visible="false"
                    OnSelectedIndexChanged="ddlcontact_SelectedIndexChanged" AutoPostBack="true">
                </asp:DropDownList>
            </div>
        </div>
        
        <div class="GridContainer">
            <asp:ListView ID="lsvSendEmail" runat="server" DataKeyNames="Id" OnItemDataBound="lsvSendEmail_ItemDataBound"
                OnPreRender="lsvSendEmail_PreRender" OnItemCommand="lsvSendEmail_ItemCommand">
                <LayoutTemplate>
                    <table id="tlbTemplate" runat="server" class="Grid" cellspacing="0" border="0">
                        <tr>
                            <th style="white-space: nowrap; width: 120px">
                                <asp:LinkButton ID="btnDateTime" runat="server" ToolTip="Sort By Date And Time" CommandName="Sort"
                                    CommandArgument="SentDate" Text="Date & Time" />
                            </th>
                            <th style="width: 10%; white-space: nowrap;">
                                <asp:LinkButton ID="btnFrom" runat="server" ToolTip="Sort By Sender Email Address"
                                    Width="70%" CommandName="Sort" CommandArgument="SenderEmail" Text="From" EnableViewState ="true"  />
                            </th>
                            <th align="left" id="thClientEmail" runat="server" visible="false">
                                <asp:LinkButton ID="lnkTo" runat="server" ToolTip="Sort By Receiver Email Address"
                                    Width="70%" CommandName="Sort" CommandArgument="ReceiverEmail" Text="To" />
                            </th>
                            <th style="width: 30%; white-space: nowrap;">
                                <asp:LinkButton ID="btnSubject" runat="server" ToolTip="Sort By Subject" Width="70%"
                                    CommandName="Sort" CommandArgument="Subject" Text="Subject" />
                            </th>
                            <th style="white-space: normal; width: 50%">
                                Body
                            </th>
                        </tr>
                        <tr id="itemPlaceholder" runat="server">
                        </tr>
                        <tr class="Pager">
                            <td colspan="4" id="tdPager" runat="server">
                                <ucl:Pager ID="pagerControl" runat="server" EnableViewState="true" />
                            </td>
                        </tr>
                    </table>
                </LayoutTemplate>
                <EmptyDataTemplate>
                    <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                        <tr>
                            <td>
                                No data was returned.
                            </td>
                        </tr>
                    </table>
                </EmptyDataTemplate>
                <ItemTemplate>
                    <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                        <td >
                            <asp:Label ID="lblDateTime" runat="server" Style="width: 120px; white-space: nowrap;" />
                            <asp:HiddenField ID="hdfMemberEmailId" runat="server" />
                        </td>
                        <td >
                            <asp:Label ID="lblSentTo" runat="server" />
                        </td>
                        <td  id="tdClientEmail" runat="server" visible="false">
                            <asp:Label ID="lblReceiver" runat="server"></asp:Label>
                        </td>
                        <td >
                            <asp:HyperLink ID="hlnkSubject" runat="server" CssClass="NormalLink" ></asp:HyperLink>
                        </td>
                        <td>
                            <asp:HyperLink ID="hlnkEmailBody" runat="server" CssClass="NormalLink" ></asp:HyperLink>
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:ListView>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
