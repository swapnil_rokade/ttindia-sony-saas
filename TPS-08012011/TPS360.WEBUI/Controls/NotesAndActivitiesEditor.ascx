﻿<%-- 
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: NotesAndActivitiesEditor.ascx
    Description: This is the user control page used to insert notes and activities.
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1             Jan-09-2008          Jagadish N            Defect id: 9073; Implemented sorting.
-------------------------------------------------------------------------------------------------------------------------------------------       
--%>
<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NotesAndActivitiesEditor.ascx.cs"
    Inherits="TPS360.Web.UI.ControlNotesAndActivitiesEditor" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Controls/PagerControl.ascx" TagName="Pager" TagPrefix="ucl" %>
<script type="text/javascript" language="javascript">

 window.onload = function(){
 document.getElementById("<%= txtNotes.ClientID %>").value="";
 document.getElementById("<%= ddlNoteCategory.ClientID %>").value="0";
 };
 </script>
<script runat="server">
    

   
    </script>

 
<asp:TextBox ID="txtSortColumn" runat="server" Visible="false"></asp:TextBox>
<asp:TextBox ID="txtSortOrder" runat="server" Visible="false"></asp:TextBox>
<div>
    <div class="TableRow" style="text-align: center">
        <asp:Label ID="lblMessage" runat="server"></asp:Label>
    </div>
    <div class="TabPanelHeader">
        Add Note
    </div>
    <div class="TableRow">
        <div class="TableFormLeble" ">
            <asp:Label ID="lblNoteCategory" runat="server" Text="Note Category"></asp:Label>:
        </div>
        <div class="TableFormContent">
            <asp:DropDownList ID="ddlNoteCategory" runat="server" CssClass="CommonDropDownList"
                ValidationGroup="NoteInfo">
            </asp:DropDownList>
             <span class="RequiredField">*</span>
        </div>
        </div>
         <div class="TableRow">
        <div class="TableFormValidatorContent" style="margin-left: 42%">
             <asp:CompareValidator ID="cvNoteCategory" ValidationGroup="NoteInfo" runat="server"
                        ControlToValidate="ddlNoteCategory" ErrorMessage="Please Select Note Category"
                        Operator="NotEqual" ValueToCompare="0" Display="Dynamic"></asp:CompareValidator>
        </div>
    </div>
        
        
    
    <div class="TableRow">
        <div class="TableFormLeble">
            <asp:Label ID="lblNotes" runat="server" Text="Note"></asp:Label>:
        </div>
        <div class="TableFormContent">
            <asp:TextBox ID="txtNotes" runat="server" TextMode="MultiLine" Rows="6" CssClass="CommonTextBox"
                ValidationGroup="NoteInfo" Width="350px">
            </asp:TextBox>
            <span class="RequiredField">*</span>
        </div>
        
        
    </div>
        
    <div class="TableRow">
        <div class="TableFormValidatorContent" style="margin-left: 42%">
            <asp:RequiredFieldValidator ID="rfvDegree" runat="server" ControlToValidate="txtNotes"
                ErrorMessage="Please enter note." EnableViewState="False" Display="Dynamic" ValidationGroup="NoteInfo">
            </asp:RequiredFieldValidator>
        </div>
    </div>
    
    
    <div id="divUploadResume" runat="server" enableviewstate="false" visible="false">
     <div class="TableRow" style="text-align: center">
         <div class ="TableFormLeble">
         <asp:Label ID="lblAttachDocument" runat="server" Text="Attach Document:"></asp:Label>:
         </div>
           <div class ="TableFormContent" > 
            <asp:FileUpload ID="fuDocument" size="40" runat="server" Style="float: left;" 
                        CssClass="CommonButton"/>
                        
            </div>
            
             
     </div>
      <div class="TableRow">
                <div class="TableFormValidatorContent" style="margin-left: 42%">
                    <asp:RegularExpressionValidator ID="revfuDocument" runat="server" ErrorMessage="Invalid File Format"
                        ControlToValidate="fuDocument" ValidationExpression="^.+\.((doc)|(DOC)|(txt)|(TXT)|(pdf)|(PDF)|(rtf)|(RTF)|(docx)|(DOCX))$"
                        Display="Dynamic"></asp:RegularExpressionValidator>
                    
                </div>
     </div>
     <%-- <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="ddlDocumentType" EventName="SelectedIndexChanged" />
                        </Triggers>--%>
    
    </div>
    
     
    <div class="TableRow" style="text-align: center">
        <asp:Button ID="btnSave" CssClass="CommonButton" runat="server" Text="Save" ValidationGroup="NoteInfo"
            OnClick="btnSave_Click" />
    </div>
    <br />
    <div class="TabPanelHeader">
        List of Notes
    </div>
    <div class="GridContainer" style="text-align: left;">
        <asp:ObjectDataSource ID="odsNotesList" runat="server" EnablePaging="false" SelectMethod="GetAllByMemberID"
            TypeName="TPS360.Web.UI.CommonNoteDataSource" SortParameterName="sortExpression">
            <SelectParameters>
                <asp:Parameter Name="memberId"/>
            </SelectParameters>
        </asp:ObjectDataSource>
        <asp:UpdatePanel>
            <asp:ListView ID="lsvNotesList" runat="server" DataSourceID="odsNotesList" DataKeyNames="Id"
                OnItemDataBound="lsvNotesList_ItemDataBound" OnItemCommand="lsvNotesList_ItemCommand"
                onPreRender="lsvNotesList_PreRender" EnableViewState="true" >
                <layouttemplate>
                <table id="tlbTemplate" runat="server" class="Grid" cellspacing="0" border="0">
                    <tr>
                        <th style="white-space: nowrap; width :120px !important">
                            <asp:LinkButton ID="btnDateTime" runat="server" ToolTip="Sort by create date" CommandName="Sort"  
                                CommandArgument="[C].[CreateDate]" Text="Date & Time" />
                        </th>
                        <th style="white-space: nowrap; ">
                            <asp:LinkButton ID="btnManager" runat="server" CommandName="Sort" CommandArgument="[M].[FirstName]"
                                Text="Manager" />
                        </th>
                        <th style="white-space: nowrap;  ">
                            <asp:LinkButton ID="btnNoteCategory" runat="server" CommandName="Sort" CommandArgument="[G].[Name]"
                                Text="Note Category" />
                        </th>
                        <th style="white-space:normal;  "> Note
                    
                        </th>
                        <th id="thDocument" runat="server"  style="white-space:normal;" visible="false">
                        <asp:LinkButton ID="lnkDocument"  runat="server" CommandName="Sort" CommandArgument=""
                                Text="Document" />
                        
                        
                        </th>
                        <th style="text-align: center;  " runat ="server" id="thAction"  width="50px" visible="True">Action</th>
                    </tr>
                    <tr id="itemPlaceholder" runat="server">
                    </tr>
                    <tr class="Pager">
                        <td id="tdpager" runat="server" colspan="5">
                            <ucl:Pager ID="pagerControl_Requisition" runat="server" EnableViewState="true" />
                        </td>
                    </tr>
                </table>
            </layouttemplate>
                <emptydatatemplate>
                <table id="tblEmptyData" class="EmptyDataTable alert alert-warning"  enableviewstate ="false" >
                    <tr>
                        <td>
                            No data was returned.
                        </td>
                    </tr>
                </table>
            </emptydatatemplate>
                <itemtemplate>
                <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                    <td>
                        <asp:Label ID="lblDateTime" runat="server"  />
                    </td>
                    <td>
                        <asp:Label ID="lblManager" runat="server" />
                    </td>
                    <td style="white-space: nowrap;">
                        <asp:Label ID="lblNoteCategory" runat="server" />
                    </td>
                    <td style="white-space: normal;">
                        <asp:Label ID="lblNote" runat="server" />
                    </td>
                     <td id="tdDocument" runat="server" visible="false" >
                      <asp:Label ID="lblDocument" runat="server"></asp:Label>
                      </td>
                    <td style="text-align: center;" runat ="server" id ="tdAction">
                        <asp:ImageButton ID="btnEdit" SkinID="sknEditButton" runat="server" Visible="True"
                            CommandName="EditItem"></asp:ImageButton>
                        <%--<asp:ImageButton ID="btnDelete" SkinID="sknDeleteButton" runat="server" Visible="True"
                            CommandName="DeleteItem"></asp:ImageButton>--%>
                    </td>
                </tr>
            </itemtemplate>
            </asp:ListView>
        </asp:UpdatePanel>
    </div>
</div>
