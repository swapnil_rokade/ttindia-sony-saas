﻿ 
<%@ Control Language="C#" AutoEventWireup="true" CodeFile="QuestionBank.ascx.cs" Inherits="TPS360.Web.UI.ControlsQuestionBank" %>

<%@ Register Src="~/Controls/PagerControl.ascx" TagName="Pager" TagPrefix="ucl" %>


<script type="text/javascript" language="javascript">

 window.onload = function(){
 document.getElementById("<%= txtQuestion.ClientID %>").value="";
 document.getElementById("<%= ddlAssessmentForm.ClientID %>").value="0";
 document.getElementById("<%= DdlAnswerType.ClientID %>").value="0";
 };
 </script>
 <asp:TextBox ID="txtSortColumn" runat="server" Visible="false"></asp:TextBox>
<asp:TextBox ID="txtSortOrder" runat="server" Visible="false"></asp:TextBox>
<div>
    <div class="TableRow" style="text-align: center">
        <asp:Label ID="lblMessage" runat="server"></asp:Label>
    </div>
    <div class="TabPanelHeader">
        Add Assessment
    </div>
    <div class="TableRow">
        <div class="TableFormLeble" ">
            <asp:Label ID="lblAssessmentForm" runat="server" Text="Assessment Form"></asp:Label>:
        </div>
        <div class="TableFormContent">
            <asp:DropDownList ID="ddlAssessmentForm" runat="server" CssClass="CommonDropDownList"
                ValidationGroup="AssesssmentInfo">
            </asp:DropDownList>
             <span class="RequiredField">*</span>
        </div>
        </div>
         <div class="TableRow">
        <div class="TableFormValidatorContent" style="margin-left: 42%">
             <asp:CompareValidator ID="cvAssessmentForm" ValidationGroup="AssesssmentInfo" runat="server"
                        ControlToValidate="ddlAssessmentForm" ErrorMessage="Please Select Assessment Form"
                        Operator="NotEqual" ValueToCompare="0" Display="Dynamic"></asp:CompareValidator>
        </div>
    </div>
        

    <div class="TableRow">
        <div class="TableFormLeble">
            <asp:Label ID="lblNotes" runat="server" Text="Question"></asp:Label>:
        </div>
        <div class="TableFormContent">
            <asp:TextBox ID="txtQuestion" runat="server" TextMode="MultiLine" Rows="3" CssClass="CommonTextBox"
                ValidationGroup="AssesssmentInfo" Width="350px">
            </asp:TextBox>
            <span class="RequiredField">*</span>
        </div>
        
        
    </div>
    
     <%--code added by pravin khot on 13/Jan/2016--%>
    <div class="TableRow">
        <div class="TableFormValidatorContent" style="margin-left: 42%">
            <asp:RequiredFieldValidator ID="rfvDegree" runat="server" ControlToValidate="txtQuestion"
                ErrorMessage="Please enter a Question." EnableViewState="False" Display="Dynamic" ValidationGroup="AssesssmentInfo">
            </asp:RequiredFieldValidator>
        </div>
    </div>
    <%--************************End******************************--%>
    
    
    
       <div class="TableRow">
            <div class="TableFormLeble" ">
                <asp:Label ID="lblAnswerType" runat="server" Text="Question Type "></asp:Label>:
            </div>
            <div class="TableFormContent">
                <asp:DropDownList ID="DdlAnswerType" runat="server" CssClass="CommonDropDownList"
                    ValidationGroup="AssesssmentInfo">
                </asp:DropDownList>
                 <span class="RequiredField">*</span>
            </div>
        </div>
    <div class="TableRow">
        <div class="TableFormValidatorContent" style="margin-left: 42%">
             <asp:CompareValidator ID="cvQustionType" ValidationGroup="AssesssmentInfo" runat="server"
                        ControlToValidate="DdlAnswerType" ErrorMessage="Please Select Question Type"
                        Operator="NotEqual" ValueToCompare="0" Display="Dynamic"></asp:CompareValidator>
        </div>
    </div>  
    
    <%--code commented by pravin khot on 13/Jan/2016--%>
   <%-- <div class="TableRow">
        <div class="TableFormValidatorContent" style="margin-left: 42%">
            <asp:RequiredFieldValidator ID="rfvDegree" runat="server" ControlToValidate="txtQuestion"
                ErrorMessage="Please enter a Question." EnableViewState="False" Display="Dynamic" ValidationGroup="AssesssmentInfo">
            </asp:RequiredFieldValidator>
        </div>
    </div>--%>
    <%--************************End******************************--%>
    
    <div id="divUploadResume" runat="server" enableviewstate="false" visible="false">
     <div class="TableRow" style="text-align: center">
         <div class ="TableFormLeble">
         <asp:Label ID="lblAttachDocument" runat="server" Text="Attach Document:"></asp:Label>:
         </div>
           <div class ="TableFormContent" > 
            <asp:FileUpload ID="fuDocument" size="40" runat="server" Style="float: left;" 
                        CssClass="CommonButton"/>
                        
            </div>
            
             
     </div>
      <div class="TableRow">
                <div class="TableFormValidatorContent" style="margin-left: 42%">
                    <asp:RegularExpressionValidator ID="revfuDocument" runat="server" ErrorMessage="Invalid File Format"
                        ControlToValidate="fuDocument" ValidationExpression="^.+\.((doc)|(DOC)|(txt)|(TXT)|(pdf)|(PDF)|(rtf)|(RTF)|(docx)|(DOCX))$"
                        Display="Dynamic"></asp:RegularExpressionValidator>
                    
                </div>
     </div>
     <%-- <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="ddlDocumentType" EventName="SelectedIndexChanged" />
                        </Triggers>--%>
    
    </div>
    
    </br>
    <div class="TableRow" style="text-align: center">
        <asp:Button ID="btnSave" CssClass="CommonButton" runat="server" Text="Save" ValidationGroup="AssesssmentInfo"
            OnClick="btnSave_Click" />
    </div>
    <br />
    <div class="TabPanelHeader">
        List of Assessments
    </div>
    <div class="GridContainer" style="text-align: left;">
        <asp:ObjectDataSource ID="odsQuestionBankList" runat="server" EnablePaging="false" SelectMethod="GetAllQuestionBank"
            TypeName="TPS360.Web.UI.QuestionBankDataSource" SortParameterName="sortExpression">
            <SelectParameters>
                <%--<asp:Parameter Name="memberId"/>--%>
            </SelectParameters>
        </asp:ObjectDataSource>
        <asp:UpdatePanel>
            <asp:ListView ID="lsvQuestionBankList" runat="server" DataSourceID="odsQuestionBankList" DataKeyNames="Id"
                OnItemDataBound="lsvQuestionBankList_ItemDataBound" OnItemCommand="lsvQuestionBankList_ItemCommand"
                onPreRender="lsvQuestionBankList_PreRender" EnableViewState="true" >
                <layouttemplate>
                <table id="tlbTemplate" runat="server" class="Grid" cellspacing="0" border="0">
                    <tr>
                        <th style="white-space: nowrap; width :200px !important">
                            <asp:LinkButton ID="btnAssessmentForm" runat="server" ToolTip="Sort by AssessmentForm" CommandName="Sort"  
                                CommandArgument="[Q].[QuestionBankType_Id]" Text="AssessmentForm" />
                        </th>
                        <th style="white-space: nowrap; ">
                            <asp:LinkButton ID="btnAnswerType" runat="server" CommandName="Sort" CommandArgument="[Q].[AnswerType_Id]"
                                Text="Answer Type" />
                        </th>
                        <th style="white-space: nowrap;  ">
                            <asp:LinkButton ID="btnQuestion" runat="server" CommandName="Sort" CommandArgument="[Q].[Question]"
                                Text="Question" />
                        </th>
                       <%-- <th style="white-space:normal;  "> Assessment Form
                    
                        </th>--%>
                       <%-- <th id="thDocument" runat="server"  style="white-space:normal;" visible="false">
                        <asp:LinkButton ID="lnkDocument"  runat="server" CommandName="Sort" CommandArgument=""
                                Text="Document" />
                        
                        
                        </th>--%>
                        <th style="text-align: center;  " runat ="server" id="thAction"  width="50px" visible="True">Action</th>
                    </tr>
                    <tr id="itemPlaceholder" runat="server">
                    </tr>
                    <tr class="Pager">
                        <td id="tdpager" runat="server" colspan="4">
                            <ucl:Pager ID="pagerControl_Requisition" runat="server" EnableViewState="true" />
                        </td>
                    </tr>
                </table>
            </layouttemplate>
                <emptydatatemplate>
                <table id="tblEmptyData" class="EmptyDataTable alert alert-warning"  enableviewstate ="false" >
                    <tr>
                        <td>
                            No data was returned.
                        </td>
                    </tr>
                </table>
            </emptydatatemplate>
                <itemtemplate>
                <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                    <td>
                        <asp:Label ID="lblAssessmentForm" runat="server"  />
                    </td>
                    <td>
                        <asp:Label ID="lblAnswer" runat="server" />
                    </td>
                    <td style="white-space: nowrap;">
                        <asp:Label ID="lblQuestion" runat="server" />
                    </td>
                  <%--  <td style="white-space: normal;">
                        <asp:Label ID="lblNote" runat="server" />
                    </td>
                     <td id="tdDocument" runat="server" visible="false" >
                      <asp:Label ID="lblDocument" runat="server"></asp:Label>
                      </td>--%>
                    <td style="text-align: center;" runat ="server" id ="tdAction">
                        <asp:ImageButton ID="btnEdit" SkinID="sknEditButton" runat="server" Visible="True"
                            CommandName="EditItem"></asp:ImageButton>
                        <asp:ImageButton ID="btnDelete" SkinID="sknDeleteButton" runat="server" Visible="True"
                            CommandName="DeleteItem"></asp:ImageButton>
                    </td>
                </tr>
            </itemtemplate>
            </asp:ListView>
        </asp:UpdatePanel>
    </div>
</div>