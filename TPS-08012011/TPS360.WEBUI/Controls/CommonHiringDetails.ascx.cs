﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: CommonHiringDetails.ascx.cs
    Description: 
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1                8/March/2016         pravin khot        16 lables and columns rearrange values (using Salary Components) ,replace function GetSumGrandTotal
    0.2                29Nov2016            Prasanth Kumar G   issue id 1041      
-------------------------------------------------------------------------------------------------------------------------------------------      
*/
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using TPS360.Common.BusinessEntities;
using TPS360.Web.UI.Helper;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Web.UI;
using System.Text.RegularExpressions;
using ExpertPdf.HtmlToPdf;
using System.Drawing;
using System.IO;

namespace TPS360.Web.UI
{
    public partial class CommonHiringDetails : ATSBaseControl
    {

        #region Member Variables

        #endregion

        #region Properties
        public string bulk = string.Empty;
        double temp = 0;
        double balancingAmount = 0;
        double BasicAmount = 0;
        double c7, c8, c9 = 0;
        double fcp, pf, VariablePay, final = 0;
        double total = 0;
        bool hideGridHeader = false;
        public string BulkAction
        {
            get
            {
                return hdnBulkAction.Value == string.Empty ? "" : hdnBulkAction.Value;
            }
            set
            {
                hdnBulkAction.Value = value.ToString();
                bulk = value;
            }
        }

        public string _MemberId = string.Empty;
        public string MemberID
        {
            get
            {
                return hfMemberId.Value == string.Empty ? "0" : hfMemberId.Value;
            }
            set
            {
                hfMemberId.Value = value;
                _MemberId = value;
                if (hdnBulkAction.Value == string.Empty)
                    PrepareEditView();
                else
                    btnRemove.Visible = false;
                hdnBulkAction.Value = string.Empty;

            }
        }
        public int _statusID = 0;
        public int StatusId
        {
            get
            {
                return Convert.ToInt32(hfStatusId.Value == string.Empty ? "0" : hfStatusId.Value);
            }
            set
            {

                hfStatusId.Value = value.ToString();
                _statusID = value;
            }
        }
        public int _JobPostingId = 0;
        public int JobPostingId
        {
            get
            {

                return Convert.ToInt32(hfJobPostingId.Value == string.Empty ? "0" : hfJobPostingId.Value);
            }
            set
            {
                ClearControls();
                hfJobPostingId.Value = value.ToString();
                _JobPostingId = value;
                // Prepareview();
            }
        }
        public delegate void HiringDetailsEventHandler(string MemberId, int StatusId, bool IsAdded);
        public event HiringDetailsEventHandler HiringDetailsAdded;
        #endregion

        #region Methods
        //public void PopulateBand(string ManagementBandId) 
        //{
        //    int MBId = Convert.ToInt32(ManagementBandId);
        //    ddlBand.Items.Clear();
        //    if (MBId > 0)
        //    {
        //        ddlBand.DataSource = Facade.GetAllManagementBandByManagementBandId(MBId);
        //    }
        //    else 
        //    {
        //        ddlBand.DataSource = Facade.GetAllBand();
        //    }
        //    ddlBand.DataTextField = "Band";
        //    ddlBand.DataValueField = "BandId";

        //    ddlBand.DataBind();
        //    ddlBand.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));
        //}
        private void Prepareview()
        {
            //*******Rishi Jagati**********Offer Generation********Start*******28-July-2016************//
            if (ddlLocation.SelectedIndex == -1)
            {
                MiscUtil.PopulateSalaryGrade(ddlSalGrade, Facade);
            }
            JobPosting js = Facade.GetJobPostingById(JobPostingId);
            if (js != null)
            {
                txtRequisitionName.Text = js.JobTitle + " [" + js.JobPostingCode + "]";
                txtProjectName.Text = js.ProjectName;            
            }
            ddlSource.Visible = false;
            CandidateOverviewDetails overviewdetail = Facade.GetCandidateOverviewDetails(Convert.ToInt32(hfMemberId.Value));
            if (overviewdetail != null)
            {
               
                lblsourcedescription.Text = overviewdetail.ResumeSourceName.Replace("<br/>"," - ") ;
            }

            //*******Rishi Jagati**********Offer Generation********End*********28-July-2016************//
            if (ddlSalaryCurrency.Items.Count == 0)
            {
                //hdnSelectedMBand.Value = "0";
                MiscUtil.PopulateCurrency(ddlSalaryCurrency, Facade);
                MiscUtil.PopulateCurrency(ddlSalaryCurrent_Commission, Facade);
                //MiscUtil.PopulateManagementBand(ddlManagementBand, Facade);
                MiscUtil.PopulateLocation(ddlLocation, Facade);
                GetDefaultsFromSiteSetting();
                //PopulateBand(hdnSelectedMBand.Value);

                ddlSource.DataSource = Facade.GetAllGenericLookupByLookupType(LookupType.SourceType);
                ddlSource.DataValueField = "Id";
                ddlSource.DataTextField = "Name";
                ddlSource.DataBind();

                ddlSource.SelectedItem.Value = overviewdetail.ResumeSourceId.ToString();

                ddlSourceDescription.DataSource = Facade.GetAllGenericLookupByLookupType(LookupType.SourceDesciption).OrderBy(x => x.SortOrder).ToList();
                ddlSourceDescription.DataValueField = "Id";
                ddlSourceDescription.DataTextField = "Name";
                ddlSourceDescription.DataBind();
                MiscUtil.PopulateMemberListWithEmailByRole(ddlActiveRecruiter, ContextConstants.ROLE_EMPLOYEE, Facade);
                ControlHelper.SelectListByValue(ddlActiveRecruiter, CurrentMember.Id.ToString());
                ddlActiveRecruiter.Items.RemoveAt(0);
                FillVendorDetails();
                FillEmployeeReferrerList();


                //if (ApplicationSource == ApplicationSource.GenisysApplication || ApplicationSource == ApplicationSource.SelectigenceApplication)
                //{
                //    ddlEmployeeReferrer.Visible = false;
                //    txtsrcdesc.Visible = true;
                //    CVSourceDescription.Enabled = false;
                //    ddlSourceDescription.Style.Add("display", "none");
                //    txtsrcdesc.Style.Add("display", "");


                //}

            }
        }
        private void PrepareEditView()
        {
            Prepareview();          
            if (hfMemberId.Value.Contains(",")) { btnRemove.Visible = false; ClearControls(); return; }
            MemberHiringDetails memberHiringDetails = Facade.GetMemberHiringDetailsByMemberIdAndJobPostingID(Convert.ToInt32(hfMemberId.Value), JobPostingId);
            if (memberHiringDetails != null)
            {
               
                btnRemove.Visible = true;
                hfMemberHiringDetailsId.Value = memberHiringDetails.Id.ToString();
                //txtPosition.Text = memberHiringDetails.OfferedPosition;
                WHEJobDescription.InnerHtml = memberHiringDetails.JobDescription.ToString();

                txtRequisitionName.Text = memberHiringDetails.JobTitle1 + " [" + memberHiringDetails.JobPostingCode + "]";
                //txtProjectName.Text = memberHiringDetails.pr;

                txtSalary.Text = memberHiringDetails.OfferedSalary;
                ControlHelper.SelectListByValue(ddlSalaryCurrency, memberHiringDetails.OfferedSalaryCurrency.ToString());
                ControlHelper.SelectListByValue(ddlSalary, memberHiringDetails.OfferedSalaryPayCycle.ToString());
                chkOfferAccepted.Checked = memberHiringDetails.OfferAccepted;
                //wdcJoiningDate.Value  = memberHiringDetails.JoiningDate ;
                if (memberHiringDetails.JoiningDate != DateTime.MinValue)
                {
                    //txtJoiningDate.Text = memberHiringDetails.JoiningDate.ToShortDateString();
                    //wdcJoiningDate.SelectedDate = memberHiringDetails.JoiningDate;
                    wdcJoiningDate.Text = memberHiringDetails.JoiningDate.ToShortDateString();
                }
                else
                { //txtJoiningDate.Text = ""; wdcJoiningDate.SelectedDate = null; 
                    wdcJoiningDate.Text = "";
                }
                if (memberHiringDetails.OfferedDate != DateTime.MinValue)
                {
                    //txtDateOffered.Text = memberHiringDetails.OfferedDate.ToShortDateString();
                    //wdcDateOffered.SelectedDate = memberHiringDetails.OfferedDate;

                    wdcDateOffered.Text = memberHiringDetails.OfferedDate.ToShortDateString();
                }
                else
                {
                    //txtDateOffered.Text = ""; wdcDateOffered.SelectedDate = null; 
                    wdcDateOffered.Text = "";
                }
                txtSalary_Commission.Text = memberHiringDetails.CommissionPayRate.ToString();
                ControlHelper.SelectListByValue(ddlSalaryCurrent_Commission, memberHiringDetails.CommissionCurrency.ToString());
                if (ddlSalaryCurrency.SelectedItem != null)
                {
                    if (ddlSalaryCurrency.SelectedItem.Text == "INR") lblPayrateCurrency.Text = "(Lacs)";
                    else lblPayrateCurrency.Text = "";
                }
                if (ddlSalaryCurrent_Commission.SelectedItem != null)
                {
                    if (ddlSalaryCurrent_Commission.SelectedItem.Text == "INR") lblPayrateCurrency_Commission.Text = "(Lacs)";
                    else lblPayrateCurrency_Commission.Text = "";
                }

                txtBasic.Text = memberHiringDetails.BasicEmbedded.ToString("N2").Replace(",", "");
                txtHra.Text = memberHiringDetails.HRAEmbedded.ToString("N2").Replace(",", "");
                txtConveyence.Text = memberHiringDetails.Conveyance.ToString("N2").Replace(",", "");
                txtMedical.Text = memberHiringDetails.Medical.ToString("N2").Replace(",", "");
                txtEducationalAllowance.Text = memberHiringDetails.EducationalAllowanceEmbedded.ToString("N2").Replace(",", "");
                txtLTA.Text = memberHiringDetails.LTAEmbedded.ToString("N2").Replace(",", "");
                txtPF.Text = memberHiringDetails.PFEmbedded.ToString("N2").Replace(",", "");
                txtGratuity.Text = memberHiringDetails.GratuityEmbedded.ToString("N2").Replace(",", "");
                txtMaximunAnnualIncentive.Text = memberHiringDetails.MaximumAnnualIncentive.ToString("N2").Replace(",", "");
                txtSalesIncentive.Text = memberHiringDetails.SalesIncentive.ToString("N2").Replace(",", "").Replace(",", "");
                txtCarAllowance.Text = memberHiringDetails.CarAllowance.ToString("N2").Replace(",", "");
                txtSpecialAllowance.Text = memberHiringDetails.SpecialAllowance.ToString("N2").Replace(",", "");
                txtRoleAllowance.Text = memberHiringDetails.RoleAllowance.ToString("N2").Replace(",", "");
                txtSiteAllowance.Text = memberHiringDetails.SiteAllowance.ToString("N2").Replace(",", "");
                txtRetentionBonus.Text = memberHiringDetails.RetentionBonus.ToString("N2").Replace(",", "");
                txtRelocationAllowance.Text = memberHiringDetails.RelocationAllowanceForCom.ToString("N2").Replace(",", "");
                txtReimbursement.Text = memberHiringDetails.Reimbursement.ToString("N2").Replace(",", "");
                txtESIC.Text = memberHiringDetails.ESIC.ToString("N2").Replace(",", "");
                txtBonus.Text = memberHiringDetails.Bonus.ToString("N2").Replace(",", "");
                txtPerformanceLinkedIncentive.Text = memberHiringDetails.PerformanceLinkedIncentive.ToString("N2").Replace(",", "");
                txtGrandTotal.Text = memberHiringDetails.GrandTotal.ToString("N2").Replace(",", "");
                txtFlexiBenefitsPlan.Text = memberHiringDetails.FlexiPay1.ToString("N2").Replace(",", "");

                ControlHelper.SelectListByValue(ddlActiveRecruiter, memberHiringDetails.ActiveRecruiterID.ToString());

                ControlHelper.SelectListByValue(ddlSource, memberHiringDetails.Source.ToString());

                CandidateOverviewDetails overviewdetail = Facade.GetCandidateOverviewDetails(Convert.ToInt32(hfMemberId.Value));
                if (overviewdetail != null)
                {
                    lblsourcedescription.Text = overviewdetail.ResumeSourceName.Replace("<br/>", " - "); ;
                }
                //if (ddlSource.SelectedItem.Text == "Employee Referral")
                //{
                //    txtsrcdesc.Style.Add("display", "none");
                //    ControlHelper.SelectListByValue(ddlEmployeeReferrer, memberHiringDetails.SourceDescription.ToString());
                //    ddlEmployeeReferrer.Style.Add("display", "");

                    //if (ApplicationSource == ApplicationSource.MainApplication)
                    //{
                    //ControlHelper.SelectListByValue(ddlEmployeeReferrer, memberHiringDetails.SourceDescription);
                    //hdnSelectedVendorContact.Value = memberHiringDetails.SourceDescription;
                    //}
                    //else if (ApplicationSource == ApplicationSource.GenisysApplication || ApplicationSource == ApplicationSource.SelectigenceApplication)
                    //{
                    //    txtsrcdesc.Text = memberHiringDetails.SourceDescription;
                    //    ddlSourceDescription.Style.Add("display", "none");
                    //    txtsrcdesc.Style.Add("display", "");
                    //    ddlEmployeeReferrer.Visible = false;
                    //    CVSourceDescription.Enabled = false;
                    //    CVSourceDescription.Visible = false;

                    //}
                //}
                //else if (ddlSource.SelectedIndex == 4)
                //{
                //    //CompanyContact con = Facade.GetCompanyContactByMemberId(Convert.ToInt32(memberHiringDetails.SourceDescription));
                //    //if (con != null)
                //    //{
                //    //    ControlHelper.SelectListByValue(ddlVendor, con.CompanyId.ToString());
                //    //    FillVendorContacts(con.CompanyId);
                //    //    ControlHelper.SelectListByValue(ddlVendorContact, memberHiringDetails.SourceDescription);
                //    //}
                //    string[] IDs = memberHiringDetails.SourceDescription.Split('-');
                //    if (IDs.Length > 1)
                //    {

                //        ControlHelper.SelectListByValue(ddlVendor, IDs[0]);
                //        FillVendorContacts(Convert.ToInt32(ddlVendor.SelectedValue == "" ? "0" : ddlVendor.SelectedValue));
                //        ControlHelper.SelectListByValue(ddlVendorContact, IDs[1]);
                //    }
                //    hdnSelectedVendorContact.Value = memberHiringDetails.SourceDescription;
                //    divVendor.Style.Add("display", "");
                //    ddlSourceDescription.Style.Add("display", "none");
                //    txtsrcdesc.Style.Add("display", "none");

                //}

                //else if (ddlSource.SelectedItem.Text == "Job Portals")
                //{
                //    txtsrcdesc.Style.Add("display", "none");
                //    ControlHelper.SelectListByValue(ddlSourceDescription, memberHiringDetails.SourceDescription.ToString());
                //    ddlSourceDescription.Style.Add("display", "");

                //}
                //else
                //{

                //    txtsrcdesc.Text = memberHiringDetails.SourceDescription;
                //    ddlSourceDescription.Style.Add("display", "none");
                //    txtsrcdesc.Style.Add("display", "");
                //    divVendor.Style.Add("display", "none");

                //}

                //else
                //{
                //    txtsrcdesc.Style.Add("display", "none");
                //    ControlHelper.SelectListByValue(ddlSourceDescription, memberHiringDetails.SourceDescription.ToString());
                //    ddlSourceDescription.Style.Add("display", "");

                //}
                //if (memberHiringDetails.ManagementBandId > 0) 
                //{
                //    ddlManagementBand.SelectedValue = memberHiringDetails.ManagementBandId.ToString();
                //}
                //PopulateBand(ddlManagementBand.SelectedValue);

                //if (memberHiringDetails.BandId > 0) 
                //{
                //    ddlBand.SelectedValue = memberHiringDetails.BandId.ToString();
                //}
                txtJobTitle.Text = memberHiringDetails.JobTitle1;
                txtSupervisor.Text = memberHiringDetails.Supervisor;
                if (memberHiringDetails.CityId > 0)
                {
                    ddlLocation.SelectedValue = memberHiringDetails.CityId.ToString();
                }

                //******************Rishi Jagati*******Offer Generation**********Start***************28-July-2016***********//
                if (ddlSalGrade.Items.FindByText(memberHiringDetails.Grade.ToString()) != null)
                {
                    ddlSalGrade.Items.FindByText(memberHiringDetails.Grade.ToString()).Selected = true;
                }

                IList<TPS360.Common.BusinessEntities.OfferGeneration> offeredTemplate = Facade.GetOfferedSalaryTemplate(Convert.ToInt32(memberHiringDetails.Id));
                if (offeredTemplate != null)
                {
                    IsPermanent.Visible = true;
                    hideGridHeader = true;
                    btnSave.Visible = true;
                    DisplayOfferedSalaryTemplate(offeredTemplate);
                }

                //******************Rishi Jagati*******Offer Generation**********End*****************28-July-2016***********//
            }

            else
            {
                btnRemove.Visible = false;
                ClearControls();
            }
        }

        public void DisplayOfferedSalaryTemplate(IList<TPS360.Common.BusinessEntities.OfferGeneration> offeredTemplate)
        {
            DataTable dt = new DataTable();
            DataRow dr = null;
            int itemCount = 0;

            dt.Columns.Add(new DataColumn("Heads", typeof(string)));

            for (int i = 0; i < offeredTemplate.Count; i++)
            {
                dr = dt.NewRow();
                dr["Heads"] = offeredTemplate[i].Heads.ToString();
                dt.Rows.Add(dr);
            }


            dr = dt.NewRow();
            dr["Heads"] = "Total CTC";
            dt.Rows.Add(dr);

            ViewState["CurrentTable"] = dt;

            grvSalaryTemplate.DataSource = dt;
            grvSalaryTemplate.DataBind();

            int rowCount = 1;
            foreach (GridViewRow row in grvSalaryTemplate.Rows)
            {
                if (rowCount <= offeredTemplate.Count)
                {
                    SetFormulaTextBoxValues(Convert.ToDouble(offeredTemplate[itemCount].Value), row);
                    SettxtTotalCTC(Convert.ToDouble(offeredTemplate[itemCount].Value), row);
                    itemCount++;
                }
                else
                {
                    SetBalancingAmount(row);
                }
                rowCount++;
            }
        }

        public void SetFormulaTextBoxValues(double value, GridViewRow row)
        {
            if (row.RowType == DataControlRowType.DataRow)
            {
                TextBox formula = row.Cells[1].FindControl("txtFormulaValue") as TextBox;
                formula.Text = Convert.ToString(value);
            }
        }
        public void SettxtTotalCTC(double value, GridViewRow row)
        {
            if (row.RowType == DataControlRowType.DataRow)
            {
                //TextBox txtTotalCTC = row.Cells[1].FindControl("txtTotalCTC") as TextBox;
                //txtTotalCTC.Text = Convert.ToString(value);
            }
        }

        private void FillEmployeeReferrerList()
        {

            ddlEmployeeReferrer.DataSource = Facade.GetAllEmployeeReferrerList();
            ddlEmployeeReferrer.DataValueField = "Id";
            ddlEmployeeReferrer.DataTextField = "Name";
            ddlEmployeeReferrer.DataBind();
            ddlEmployeeReferrer.Items.Insert(0, new ListItem("Select Employee", "0"));
        }
        private void FillVendorDetails()
        {
            ddlVendor.DataSource = Facade.GetAllClientsByStatus((int)CompanyStatus.Vendor);
            ddlVendor.DataTextField = "CompanyName";
            ddlVendor.DataValueField = "Id";
            ddlVendor.DataBind();
            FillVendorContacts(Convert.ToInt32(ddlVendor.SelectedValue == "" ? "0" : ddlVendor.SelectedValue));
        }
        private void FillVendorContacts(int CompanyId)
        {
            // if (ddlVendor.SelectedIndex > 0)
            {
                ddlVendorContact.DataSource = Facade.GetAllCompanyContactsByCompanyId(CompanyId);
                ddlVendorContact.DataTextField = "FirstName";
                ddlVendorContact.DataValueField = "MemberId";
                ddlVendorContact.DataBind();
                ddlVendorContact = (DropDownList)MiscUtil.RemoveScriptForDropDown(ddlVendorContact);
                //if (ddlVendorContact.Items.Count > 0)
                //    ddlVendorContact.Enabled = true;
                //else
                //    ddlVendorContact.Enabled = false;
            }
            //else
            //   ddlVendorContact.Enabled = false;

            ddlVendorContact.Items.Insert(0, new ListItem("Please Select", "0"));
        }
        private MemberHiringDetails BuildMemberHiringDetails()
        {
            string vendorvalues;
            MemberHiringDetails mem = new MemberHiringDetails();
            // mem.MemberId = MemberID == 0 ? Convert.ToInt32(hfMemberId.Value) : MemberID;
            mem.JobPostingId = JobPostingId;
            mem.OfferedPosition = txtPosition.Text;
            mem.OfferedSalary = txtSalary.Text.Trim();
            try
            {
                mem.OfferedSalaryPayCycle = Convert.ToInt32(ddlSalary.SelectedValue);
            }
            catch
            {
                mem.OfferedSalaryPayCycle = 4;
            }
            mem.OfferedSalaryCurrency = Convert.ToInt32(ddlSalaryCurrency.SelectedValue);
            //mem.OfferAccepted = chkOfferAccepted.Checked; //Line commented by Prasanth on 29Nov2016 issue id 1041
            //DateTime value;
            //DateTime.TryParse(txtJoiningDate.Text, out value);
            //wdcJoiningDate.SelectedDate = value;
            //if (txtJoiningDate.Text.Trim() != string.Empty && wdcJoiningDate.SelectedDate != null)
            //    mem.JoiningDate = Convert.ToDateTime(wdcJoiningDate.SelectedDate); txtJoiningDate.Text.Trim());
            //DateTime value1;
            //DateTime.TryParse(txtDateOffered.Text, out value1);
            //wdcDateOffered.SelectedDate = value1;
            //if (txtDateOffered.Text.Trim() != string.Empty && wdcDateOffered.SelectedDate != null)
            //    mem.OfferedDate  = Convert.ToDateTime(wdcDateOffered.SelectedDate);//
            if (wdcJoiningDate.Text != "")
                mem.JoiningDate = Convert.ToDateTime(wdcJoiningDate.Text);
            if (wdcDateOffered.Text != "")
                mem.OfferedDate = Convert.ToDateTime(wdcDateOffered.Text);
            mem.CommissionPayRate = txtSalary_Commission.Text.Trim();
            mem.CommissionCurrency = Convert.ToInt32(ddlSalaryCurrent_Commission.SelectedValue);
            mem.IsRemoved = false;

            mem.BasicEmbedded = Convert.ToDecimal(txtBasic.Text != "" ? txtBasic.Text.Trim() : "0.0");
            decimal Basic1 = Convert.ToDecimal(txtBasic.Text != "" ? txtBasic.Text.Trim() : "0.0");
            mem.HRAEmbedded = Convert.ToDecimal(txtHra.Text != "" ? txtHra.Text.Trim() : "0.0");
            decimal hra1 = Convert.ToDecimal(txtHra.Text != "" ? txtHra.Text.Trim() : "0.0");
            mem.Conveyance = Convert.ToDecimal(txtConveyence.Text != "" ? txtConveyence.Text.Trim() : "0.0");
            decimal Conveyance1 = Convert.ToDecimal(txtConveyence.Text != "" ? txtConveyence.Text.Trim() : "0.0");
            mem.LTAEmbedded = Convert.ToDecimal(txtLTA.Text != "" ? txtLTA.Text.Trim() : "0.0");
            decimal LTAEmbedded1 = Convert.ToDecimal(txtLTA.Text != "" ? txtLTA.Text.Trim() : "0.0");
            mem.PerformanceLinkedIncentive = Convert.ToDecimal(txtPerformanceLinkedIncentive.Text != "" ? txtPerformanceLinkedIncentive.Text.Trim() : "0.0");
            decimal PerformanceLinkedIncentive1 = Convert.ToDecimal(txtPerformanceLinkedIncentive.Text != "" ? txtPerformanceLinkedIncentive.Text.Trim() : "0.0");
            mem.Medical = Convert.ToDecimal(txtMedical.Text != "" ? txtMedical.Text.Trim() : "0.0");
            decimal Medical1 = Convert.ToDecimal(txtMedical.Text != "" ? txtMedical.Text.Trim() : "0.0");
            mem.SpecialAllowance = Convert.ToDecimal(txtSpecialAllowance.Text != "" ? txtSpecialAllowance.Text.Trim() : "0.0");
            decimal SpecialAllowance1 = Convert.ToDecimal(txtSpecialAllowance.Text != "" ? txtSpecialAllowance.Text.Trim() : "0.0");
            mem.RetentionBonus = Convert.ToDecimal(txtRetentionBonus.Text != "" ? txtRetentionBonus.Text.Trim() : "0.0");
            decimal RetentionBonus1 = Convert.ToDecimal(txtRetentionBonus.Text != "" ? txtRetentionBonus.Text.Trim() : "0.0");

            decimal totalA = Convert.ToDecimal(Basic1 + hra1 + Conveyance1 + LTAEmbedded1 + PerformanceLinkedIncentive1 + Medical1 + SpecialAllowance1 + RetentionBonus1);

            mem.PFEmbedded = Convert.ToDecimal(txtPF.Text != "" ? txtPF.Text.Trim() : "0.0");
            decimal PFEmbedded1 = Convert.ToDecimal(txtPF.Text != "" ? txtPF.Text.Trim() : "0.0");
            mem.ESIC = Convert.ToDecimal(txtESIC.Text != "" ? txtESIC.Text.Trim() : "0.0");
            decimal ESIC1 = Convert.ToDecimal(txtESIC.Text != "" ? txtESIC.Text.Trim() : "0.0");
            mem.Bonus = Convert.ToDecimal(txtBonus.Text != "" ? txtBonus.Text.Trim() : "0.0");
            decimal Bonus1 = Convert.ToDecimal(txtBonus.Text != "" ? txtBonus.Text.Trim() : "0.0");
            mem.RoleAllowance = Convert.ToDecimal(txtRoleAllowance.Text != "" ? txtRoleAllowance.Text.Trim() : "0.0");
            decimal RoleAllowance1 = Convert.ToDecimal(txtRoleAllowance.Text != "" ? txtRoleAllowance.Text.Trim() : "0.0");
            mem.GratuityEmbedded = Convert.ToDecimal(txtGratuity.Text != "" ? txtGratuity.Text.Trim() : "0.0");
            decimal GratuityEmbedded1 = Convert.ToDecimal(txtGratuity.Text != "" ? txtGratuity.Text.Trim() : "0.0");

            decimal totalB = Convert.ToDecimal(PFEmbedded1 + ESIC1 + Bonus1 + RoleAllowance1 + GratuityEmbedded1);

            mem.SiteAllowance = Convert.ToDecimal(txtSiteAllowance.Text != "" ? txtSiteAllowance.Text.Trim() : "0.0");
            decimal SiteAllowance1 = Convert.ToDecimal(txtSiteAllowance.Text != "" ? txtSiteAllowance.Text.Trim() : "0.0");
            //***********startcode added by pravin on 8/March/2016 using salary component  From here to upto end Fully Replace********************************
            mem.Reimbursement = Convert.ToDecimal(txtReimbursement.Text != "" ? txtReimbursement.Text.Trim() : "0.0");
            decimal Reimbursement1 = Convert.ToDecimal(txtReimbursement.Text != "" ? txtReimbursement.Text.Trim() : "0.0");

            mem.SalesIncentive = Convert.ToDecimal(txtSalesIncentive.Text != "" ? txtSalesIncentive.Text.Trim() : "0.0");
            decimal SalesIncentive = Convert.ToDecimal(txtSalesIncentive.Text != "" ? txtSalesIncentive.Text.Trim() : "0.0");
            //******************************End********************
            decimal annualCTC = Convert.ToDecimal(Basic1 + hra1 + Conveyance1 + LTAEmbedded1 + PerformanceLinkedIncentive1 + Medical1 + SpecialAllowance1 + RetentionBonus1 + PFEmbedded1 + ESIC1 + Bonus1 + RoleAllowance1 + GratuityEmbedded1);

            //mem.GrandTotal = Convert.ToDecimal(annualCTC);
            ////mem.GrandTotal = Convert.ToDecimal(annualCTC * 12);

            //mem.GrandTotal = Convert.ToDecimal(txtGrandTotal.Text.Trim());
            mem.GrandTotal = Convert.ToDecimal(GetSumGrandTotal());



            decimal totaldedution = Convert.ToDecimal(PFEmbedded1 + ESIC1 + SiteAllowance1);

            decimal netpayment = Convert.ToDecimal(Basic1 + hra1 + Conveyance1 + LTAEmbedded1 + PerformanceLinkedIncentive1 + Medical1 + SpecialAllowance1 + RetentionBonus1 + PFEmbedded1 + ESIC1 + Bonus1 + RoleAllowance1 + GratuityEmbedded1 - PFEmbedded1 - ESIC1 - SiteAllowance1);


            mem.CreatorId = base.CurrentMember.Id;
            mem.UpdatorId = base.CurrentMember.Id;
            mem.Source = Convert.ToInt32(ddlSource.SelectedValue);
            //mem.SourceDescription = ddlSourceDescription.SelectedValue;
            //if (ddlManagementBand.SelectedIndex > 0) 
            //{
            //    mem.ManagementBandId = Convert.ToInt32(ddlManagementBand.SelectedValue);
            //}
            //PopulateBand(ddlManagementBand.SelectedValue);

            //    int bandValue=0;
            //    if (hdnSelectedBand.Value != "0")
            //        bandValue = Convert.ToInt32(hdnSelectedBand.Value);
            //    else
            //    {
            //        MemberHiringDetails memberHiringDetails = Facade.GetMemberHiringDetailsByMemberIdAndJobPostingID(Convert.ToInt32(hfMemberId.Value), JobPostingId);
            //        bandValue = memberHiringDetails.BandId;
            //    }

            //    mem.BandId = bandValue;

            //if (bandValue > 0)
            //{
            //    mem.BandId = Convert.ToInt32(hdnSelectedBand.Value);
            //}
            //mem.BandId = Convert.ToInt32(ddlBand.SelectedValue);

            if (ddlSource.SelectedItem.Text == "Employee Referral")
            {
                //if (ApplicationSource == ApplicationSource.MainApplication)
                //{
                mem.SourceDescription = ddlEmployeeReferrer.SelectedValue;
                //}
                //else if (ApplicationSource == ApplicationSource.GenisysApplication)
                //{
                //  mem.SourceDescription = txtsrcdesc.Text;
                //}
            }
            else if (ddlSource.SelectedItem.Text == "Job Portals")
            {
                mem.SourceDescription = ddlSourceDescription.SelectedValue;
            }
            //else if (ddlSource.SelectedIndex == 4)
            //{
            //    vendorvalues = ddlVendor.SelectedValue;
            //    if (hdntemp.Value != "0")
            //    {
            //        vendorvalues = vendorvalues + "-" + hdntemp.Value;
            //    }
            //    //else if(hdnSelectedVendorContact.Value!="0")
            //    //{
            //    //    vendorvalues = vendorvalues + "-" + hdnSelectedVendorContact.Value;
            //    //}
            //    else
            //    {
            //        MemberHiringDetails memberHiringDetails = Facade.GetMemberHiringDetailsByMemberIdAndJobPostingID(Convert.ToInt32(hfMemberId.Value), JobPostingId);
            //        string[] IDs = memberHiringDetails.SourceDescription.Split('-');
            //        vendorvalues = vendorvalues + "-" + IDs[1];
            //    }
            //    mem.SourceDescription = vendorvalues;
            //    //if (hdntemp.Value != "0")
            //    //    mem.SourceDescription = ddlVendor.SelectedValue + '-' + hdntemp.Value;
            //}
            //else if (ddlSource.SelectedItem.Text.ToString() != "Job Portals")
            else
            {
                mem.SourceDescription = txtsrcdesc.Text;

            }

            mem.ActiveRecruiterID = Convert.ToInt32(ddlActiveRecruiter.SelectedValue);
            mem.CityId = Convert.ToInt32(ddlLocation.SelectedValue);
            mem.JobTitle1 = txtJobTitle.Text;
            mem.Supervisor = txtSupervisor.Text;

            //******************Rishi Jagati*****Offer Generation************Start************28-July-2016******************//
            if (ddlSalGrade.SelectedItem.Text != "Please Select")
            {
                mem.Grade = Convert.ToString(ddlSalGrade.SelectedItem.Text);
            }
            else
            {
                mem.Grade = string.Empty;
            }
            //******************Rishi Jagati*****Offer Generation************End**************28-July-2016******************//

            mem.RawDescription = WHEJobDescription.InnerHtml;
            string s = WHEJobDescription.InnerHtml;
            s = Regex.Replace(s, "<script.*?</script>", "", RegexOptions.Singleline | RegexOptions.IgnoreCase);
            mem.JobDescription = s.ToString();
            string rawDes = Regex.Replace(MiscUtil.RemoveScript(WHEJobDescription.InnerHtml.Trim(), string.Empty), @"<(.|\n)*?>", string.Empty);
            mem.RawDescription = HttpUtility.HtmlDecode(rawDes);
            return mem;
        }
        private void GetDefaultsFromSiteSetting()
        {

            if (SiteSetting != null)
                ddlSalaryCurrent_Commission.SelectedValue = ddlSalaryCurrency.SelectedValue = SiteSetting[DefaultSiteSetting.Currency.ToString()].ToString();
        }

        private decimal GetSumGrandTotal()
        {
            //****************Old code commented by pravin khot on 8/March/2016********************
            //decimal grdtot = 0;
            //grdtot = grdtot + Convert.ToDecimal(txtBasic.Text != "" ? txtBasic.Text.Trim() : "0.0");
            //grdtot = grdtot + Convert.ToDecimal(txtHra.Text != "" ? txtHra.Text.Trim() : "0.0");
            //grdtot = grdtot + Convert.ToDecimal(txtRetentionBonus.Text != "" ? txtRetentionBonus.Text.Trim() : "0.0");
            //grdtot = grdtot + Convert.ToDecimal(txtLTA.Text != "" ? txtLTA.Text.Trim() : "0.0");
            //grdtot = grdtot + Convert.ToDecimal(txtPerformanceLinkedIncentive.Text != "" ? txtPerformanceLinkedIncentive.Text.Trim() : "0.0");
            //grdtot = grdtot + Convert.ToDecimal(txtMedical.Text != "" ? txtMedical.Text.Trim() : "0.0");
            //grdtot = grdtot + Convert.ToDecimal(txtConveyence.Text != "" ? txtConveyence.Text.Trim() : "0.0");
            //grdtot = grdtot + Convert.ToDecimal(txtSpecialAllowance.Text != "" ? txtSpecialAllowance.Text.Trim() : "0.0");

            ////grdtot = grdtot + Convert.ToDecimal(txtRoleAllowance.Text.Trim());
            ////grdtot = grdtot + Convert.ToDecimal(txtSiteAllowance.Text.Trim());

            ////grdtot = grdtot + Convert.ToDecimal(txtRelocationAllowance.Text.Trim());
            ////grdtot = grdtot + Convert.ToDecimal(txtReimbursement.Text.Trim());
            //grdtot = grdtot - Convert.ToDecimal(txtPF.Text != "" ? txtPF.Text.Trim() : "0.0");
            //grdtot = grdtot - Convert.ToDecimal(txtESIC.Text != "" ? txtESIC.Text.Trim() : "0.0");
            ////grdtot = grdtot + Convert.ToDecimal(txtGratuity.Text.Trim());
            ////grdtot = grdtot + Convert.ToDecimal(txtBonus.Text.Trim());
            ////grdtot = grdtot + Convert.ToDecimal(txtRoleAllowance.Text.Trim());
            //grdtot = grdtot - Convert.ToDecimal(txtSiteAllowance.Text != "" ? txtSiteAllowance.Text.Trim() : "0.0");

            ////grdtot = grdtot + Convert.ToDecimal(txtSalesIncentive.Text.Trim());
            ////grdtot = grdtot + Convert.ToDecimal(txtFlexiBenefitsPlan.Text.Trim());
            //return grdtot;
            //*****************************END******************************
            //**************************New code added by pravin khot on 8/March/2016***********
            decimal grdtot = 0;

            //***********startcode added by pravin on 8/March/2016 using salary component From here to upto end Fully Replace********************************

            grdtot = grdtot + Convert.ToDecimal(txtBasic.Text != "" ? txtBasic.Text.Trim() : "0.0");
            grdtot = grdtot + Convert.ToDecimal(txtHra.Text != "" ? txtHra.Text.Trim() : "0.0");
            grdtot = grdtot + Convert.ToDecimal(txtRetentionBonus.Text != "" ? txtRetentionBonus.Text.Trim() : "0.0");
            grdtot = grdtot + Convert.ToDecimal(txtLTA.Text != "" ? txtLTA.Text.Trim() : "0.0");
            grdtot = grdtot + Convert.ToDecimal(txtPerformanceLinkedIncentive.Text != "" ? txtPerformanceLinkedIncentive.Text.Trim() : "0.0");
            grdtot = grdtot + Convert.ToDecimal(txtMedical.Text != "" ? txtMedical.Text.Trim() : "0.0");
            grdtot = grdtot + Convert.ToDecimal(txtSpecialAllowance.Text != "" ? txtSpecialAllowance.Text.Trim() : "0.0");
            grdtot = grdtot + Convert.ToDecimal(txtConveyence.Text != "" ? txtConveyence.Text.Trim() : "0.0");
            grdtot = grdtot + Convert.ToDecimal(txtPF.Text != "" ? txtPF.Text.Trim() : "0.0");
            grdtot = grdtot + Convert.ToDecimal(txtESIC.Text != "" ? txtESIC.Text.Trim() : "0.0");
            grdtot = grdtot + Convert.ToDecimal(txtBonus.Text.Trim());
            grdtot = grdtot + Convert.ToDecimal(txtRoleAllowance.Text.Trim());
            grdtot = grdtot + Convert.ToDecimal(txtGratuity.Text.Trim());
            grdtot = grdtot + Convert.ToDecimal(txtSiteAllowance.Text != "" ? txtSiteAllowance.Text.Trim() : "0.0");
            grdtot = grdtot + Convert.ToDecimal(txtSalesIncentive.Text.Trim());
            grdtot = grdtot + Convert.ToDecimal(txtReimbursement.Text.Trim());

            //grdtot = grdtot + Convert.ToDecimal(txtRoleAllowance.Text.Trim());
            //grdtot = grdtot + Convert.ToDecimal(txtSiteAllowance.Text.Trim());
            //grdtot = grdtot + Convert.ToDecimal(txtRelocationAllowance.Text.Trim());
            //grdtot = grdtot + Convert.ToDecimal(txtConveyence.Text.Trim());
            //grdtot = grdtot + Convert.ToDecimal(txtFlexiBenefitsPlan.Text.Trim());
            return grdtot;
            //******************************End********************
        }


        public void ClearControls()
        {
            try
            {
                //ddlManagementBand.SelectedIndex = 0;
                //ddlBand.SelectedIndex = 0;
                ddlLocation.SelectedIndex = 0;
                txtPerformanceLinkedIncentive.Text = "0";
                txtPosition.Text = "";
                txtSalary.Text = "";
                ControlHelper.SelectListByValue(ddlSalaryCurrency, "0");
                ControlHelper.SelectListByValue(ddlSource, "0");
                ControlHelper.SelectListByValue(ddlSourceDescription, "0");
                ControlHelper.SelectListByValue(ddlVendor, "0");
                ControlHelper.SelectListByValue(ddlVendorContact, "0");
                GetDefaultsFromSiteSetting();
                chkOfferAccepted.Checked = true;
                //wdcJoiningDate.SelectedDate = null;
                //txtJoiningDate.Text = "";
                //wdcDateOffered.SelectedDate = null;
                //txtDateOffered.Text = "";
                txtSalary_Commission.Text = "";
                //txtJoiningDate.Text = wdcDateOffered.Text = DateTime.Now.ToShortDateString();
                //wdcJoiningDate.SelectedDate = DateTime.Now;
                wdcJoiningDate.Text = DateTime.Now.ToString();
                wdcDateOffered.Text = DateTime.Now.ToString();
                if (ddlSalaryCurrency.SelectedItem != null)
                {
                    if (ddlSalaryCurrency.SelectedItem.Text == "INR") lblPayrateCurrency.Text = "(Lacs)";
                    else lblPayrateCurrency.Text = "";
                }
                if (ddlSalaryCurrent_Commission.SelectedItem != null)
                {
                    if (ddlSalaryCurrent_Commission.SelectedItem.Text == "INR") lblPayrateCurrency_Commission.Text = "(Lacs)";
                    else lblPayrateCurrency_Commission.Text = "";
                }
                txtBasic.Text = "0";
                txtHra.Text = "0";
                txtConveyence.Text = "0";
                txtMedical.Text = "0";
                txtEducationalAllowance.Text = "0";
                txtLTA.Text = "0";
                txtPF.Text = "0";
                txtGratuity.Text = "0";
                txtMaximunAnnualIncentive.Text = "0";
                txtSalesIncentive.Text = "0";
                txtCarAllowance.Text = "0";
                txtGrandTotal.Text = "0";
                txtsrcdesc.Text = "";
                txtSpecialAllowance.Text = "0";
                txtRoleAllowance.Text = "0";
                txtSiteAllowance.Text = "0";
                txtRetentionBonus.Text = "0";
                txtRelocationAllowance.Text = "0";
                txtReimbursement.Text = "0";
                txtESIC.Text = "0";
                txtBonus.Text = "0";

                //*********************Rishi Jagati****Offer Generation********Start*****28-July-2016***********//
                txtPosition.Text = "";
                txtSupervisor.Text = "";
                txtJobTitle.Text = "";
                ddlSalGrade.SelectedIndex = -1;
                //*********************Rishi Jagati****Offer Generation********End*******28-July-2016***********//

            }
            catch
            {
            }

        }
        #endregion

        #region PageEvents
       
        protected void Page_Load(object sender, EventArgs e)
        {
            uclConfirm.MsgBoxAnswered += MessageAnswered;
            //wdcJoiningDate.Format = wdcDateOffered .Format = System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern ; //"dd/MM/yyyy";
            //gvDate.MinimumValue = DateTime.Now.ToShortDateString();
            //gvDate.MaximumValue = DateTime.MaxValue.ToShortDateString();
            //gvdateOffered.MinimumValue = DateTime.Now.ToShortDateString();
            //gvdateOffered.MaximumValue = DateTime.MaxValue.ToShortDateString();
            ddlSalaryCurrency.Attributes.Add("onchange", "javascript:ShowYearlyCurrency('" + ddlSalaryCurrency.ClientID + "','" + lblPayrateCurrency.ClientID + "')");
            ddlSalaryCurrent_Commission.Attributes.Add("onchange", "javascript:ShowYearlyCurrency('" + ddlSalaryCurrent_Commission.ClientID + "','" + lblPayrateCurrency_Commission.ClientID + "')");
            ddlVendor.Attributes.Add("onchange", "return Company_OnChange('" + ddlVendor.ClientID + "','" + ddlVendorContact.ClientID + "','" + hdntemp.ClientID + "','MemberID')");
            ddlVendorContact.Attributes.Add("onchange", "return Contact_OnChange('" + ddlVendorContact.ClientID + "','" + hdntemp.ClientID + "')");
            //ddlManagementBand.Attributes.Add("onchange", "javascript:ManagementOnChange('" + ddlManagementBand.ClientID + "','" + ddlBand.ClientID + "','" + hdnSelectedMBand.ClientID + "')");
            //ddlBand.Attributes.Add("onchange", "javascript:BandOnChange('" + ddlBand.ClientID + "','" + hdnSelectedBand.ClientID + "')");
            //ddlVendor.Attributes.Add("onChange", "return OnSelectedIndexChange('"+ddlVendor.ClientID+"');");
            HiddenField hdncheck = new HiddenField();
            try
            {
                hdncheck = (HiddenField)this.Page.Master.FindControl("cphHomeMaster").FindControl("ucRequisitionHiringMatrixNavigationTopMenu").FindControl("uwtRequisitionHiringMatrixNavigationTopMenu").FindControl("uclRequisitionHiringMatrix").FindControl("hdnmodal");
            }
            catch
            {
            }
            if (!IsPostBack)
            {
                Label lbModalTitle = (Label)this.Page.Master.FindControl("lbModalTitle");
                if (lbModalTitle != null) lbModalTitle.Text = "Offer Details";

                //txtJoiningDate.Text = txtDateOffered.Text = DateTime.Now.ToShortDateString();
                Prepareview();
                hdncheck.Value = "";

                //********************************Rishi Jagati****Offer Generation******Start********26-July-2016******//
                GetGradeDemandType(JobPostingId);
                //********************************Rishi Jagati****Offer Generation******End**********26-July-2016******//

            }
            //else
            //{
            //    hdncheck.Value = "s"; 
            //    MemberHiringDetails memberHiringDetails = Facade.GetMemberHiringDetailsByMemberIdAndJobPostingID(Convert.ToInt32(hfMemberId.Value), JobPostingId);
            //    if (memberHiringDetails == null)
            //    {
            //        ddlManagementBand.SelectedValue = hdnSelectedMBand.Value;
            //        PopulateBand(hdnSelectedMBand.Value);
            //        if (hdnSelectedMBand.Value != "0" && (ddlBand.SelectedValue != "0" || hdnSelectedBand.Value != "0"))
            //        {
            //            foreach (ListItem item in ddlBand.Items)
            //            {
            //                if (hdnSelectedBand.Value == item.Value)
            //                {
            //                    ddlBand.SelectedValue = hdnSelectedBand.Value;
            //                    break;
            //                }
            //                else
            //                    ddlBand.SelectedValue = "0";
            //            }
            //        }
            //    }
            //    else
            //    {
            //        if (hdnSelectedMBand.Value == "0")
            //        {
            //            if (memberHiringDetails.ManagementBandId > 0)
            //            {
            //                ddlManagementBand.SelectedValue = memberHiringDetails.ManagementBandId.ToString();
            //            }
            //        }
            //        else
            //            ddlManagementBand.SelectedValue = hdnSelectedMBand.Value;

            //        PopulateBand(ddlManagementBand.SelectedValue);

            //        if (hdnSelectedBand.Value == "0")
            //        {
            //            if (memberHiringDetails.BandId > 0)
            //            {
            //                ddlBand.SelectedValue = memberHiringDetails.BandId.ToString();
            //            }
            //        }
            //        else
            //            ddlBand.SelectedValue = hdnSelectedBand.Value;
            //    }
            //}

        }

        //********************************Rishi Jagati****Offer Generation******Start********26-July-2016******//
        public void GetGradeDemandType(int JobPostingId)
        {
            txtProjectName.Enabled = false;
            IList<TPS360.Common.BusinessEntities.OfferGeneration> offerGeneration = Facade.GetGradeDemandTypeById(JobPostingId, 0);
            if (offerGeneration != null)
            {
                txtJobTitle.Text = offerGeneration[0].DemandType.ToString();
                txtJobTitle.Enabled = false;
            }          

            if (txtJobTitle.Text == "Contract")
            {
                calculateSection.Visible = false;
                IsContract.Visible = false;
                IsPermanent.Visible = false;
                calculateSal.Visible = false;
            }
            else if (hideGridHeader == true)
            {
                IsContract.Visible = false;
            }
            else
            {
                IsContract.Visible = false;
                IsPermanent.Visible = false;
                btnSave.Visible = false;
            }
            if (txtJobTitle.Text == "Contract")
            {
                JobPosting js = Facade.GetJobPostingById(JobPostingId);
                lblCTC.Text = "Amount";
                ddlSalary.Visible = false;
                lblResourceDate.Visible = true;
                lblResourceOnTextValue.Visible = true;
                lblResourceOnTextValue.Text = "From " + Convert.ToString(js.ResourceStartDate.ToShortDateString()) + " To " + Convert.ToString(js.ResourceEndDate.ToShortDateString());
            }
            else
            {
                lblResourceDate.Visible = false;
                lblResourceOnTextValue.Visible = false;
            }
               
            IList<TPS360.Common.BusinessEntities.OfferGeneration> offeredJobTitle = Facade.GetGradeDemandTypeById(JobPostingId, 1);
            if (offeredJobTitle != null)
            {
                txtPosition.Text = offeredJobTitle[0].DemandType.ToString();
                txtPosition.Enabled = true;
            }
            IList<TPS360.Common.BusinessEntities.OfferGeneration> offeredReportingManager = Facade.GetGradeDemandTypeById(JobPostingId, 2);
            if (offeredReportingManager != null)
            {
                txtSupervisor.Text = offeredReportingManager[0].DemandType.ToString();
                txtSupervisor.Enabled = false;
            }
            hideGridHeader = false;
        }
        //********************************Rishi Jagati****Offer Generation******End**********26-July-2016******//

        #endregion
        #region Button Events
        protected void btnSave_Click(object sender, EventArgs e)
        {
            uclConfirm.AddMessage("Are you sure , you want to Submit the offer details for approval ?", ConfirmationWindow.enmMessageType.Attention, true, true, "");
        }
        public void MessageAnswered(object sender, ConfirmationWindow.MsgBoxEventArgs e)
        {
            if (e.Answer == ConfirmationWindow.enmAnswer.OK)
            {
                btnSaveClick();
            }
        }
        public void btnSaveClick()
        {
            ddlVendorContact.Attributes.Add("onchange", "return Contact_OnChange('" + ddlVendorContact.ClientID + "','" + hdntemp.ClientID + "')");
            MemberHiringDetails memberHiringDetails = BuildMemberHiringDetails();
            int memberId = 0;
            Facade.AddMemberHiringDetails(memberHiringDetails, hfMemberId.Value);
            if (StatusId > 0)
            {
                Facade.MemberJobCart_MoveToNextLevel(0, base.CurrentMember.Id, hfMemberId.Value, JobPostingId, StatusId.ToString());
                Facade.UpdateCandidateRequisitionStatus(JobPostingId, hfMemberId.Value, base.CurrentMember.Id, StatusId);
            }

            //MiscUtil.EventLogForCandidate(EventLogForCandidate.CandidateOfferDetailsEntered, JobPostingId, hfMemberId.Value, CurrentMember.Id, Facade);
            if (HiringDetailsAdded != null) HiringDetailsAdded(hfMemberId.Value, Convert.ToInt32(hfStatusId.Value != string.Empty ? hfStatusId.Value : "0"), true);
            //ClearControls();

            if (txtJobTitle.Text == "Permanent")
            {
                SaveCalculatedSalary();
            
            }

            if (ViewState["MemberHiringId"] != null)
            {
                int id = Convert.ToInt32(ViewState["MemberHiringId"]);
                memberId = id;
                // Facade.SendRecruiterManagerEmail(Convert.ToInt32(base.CurrentMember.Id), id, "Offer Submitted By recruiter", "Approval", 1);
            }
            else
            {
                MemberHiringDetails memberHiringDetails1 = Facade.GetMemberHiringDetailsByMemberIdAndJobPostingID(Convert.ToInt32(hfMemberId.Value), JobPostingId);
                memberId = Convert.ToInt32(memberHiringDetails1.Id);
           }
            //Added by pravin khot on 19/Jan/2016**********
            string AdminEmailId = string.Empty;
            int AdminMemberid = 0;
            SiteSetting siteSetting = Facade.GetSiteSettingBySettingType((int)SettingType.SiteSetting);
            if (siteSetting != null)
            {
                Hashtable siteSettingTable = ObjectEncrypter.Decrypt<Hashtable>(siteSetting.SettingConfiguration);
                AdminEmailId = siteSettingTable[DefaultSiteSetting.AdminEmail.ToString()].ToString();
                AdminMemberid = Facade.GetMemberIdByEmail(AdminEmailId);
            }
            //**********************END*************************
            Facade.SendRecruiterManagerEmail(memberId, Convert.ToInt32(base.CurrentMember.Id), "1", "Approval", 1, AdminMemberid);

            ScriptManager.RegisterClientScriptBlock(lblPayrateCurrency, typeof(MiscUtil), "Show", "parent.ModalOperationCompleted('Offer Details Saved Successfully');", true);
        }

        protected void ddlVisible(DropDownList ddl, Label rfv, int i)
        {
            if (i == 0)
            {
                if (ddl.SelectedValue == "0")
                    rfv.Visible = true;
                else
                    rfv.Visible = false;
            }
            else
            {
                //if (hdnSelectedBand.Value == "0") //ddlManagementBand.SelectedValue == "0" || 
                if (ddl.SelectedValue == "0")
                    rfv.Visible = true;
                else
                    rfv.Visible = false;
            }
        }
        protected void btnRemove_Click(object sender, EventArgs e)
        {
            //*******Rishi Jagati**********Offer Generation********Start*******28-July-2016************//
            MiscUtil.PopulateSalaryGrade(ddlSalGrade, Facade);
            //*******Rishi Jagati**********Offer Generation********End*********28-July-2016************//

            if (Convert.ToInt32(hfMemberHiringDetailsId.Value) > 0)
            {
                ClearControls();
                //if (txtPosition.Text.Trim() == string.Empty && txtSalary.Text.Trim() == string.Empty && txtSalary_Commission.Text.Trim() == string.Empty)
                if (txtPosition.Text.Trim() == string.Empty && txtSalary.Text.Trim() == string.Empty && txtSalary_Commission.Text.Trim() == string.Empty && txtSupervisor.Text.Trim() == string.Empty && txtJobTitle.Text.Trim() == string.Empty)
                {
                    Facade.DeleteMemberHiringDetailsByID(Convert.ToInt32(hfMemberHiringDetailsId.Value));
                }
            }
            if (HiringDetailsAdded != null) HiringDetailsAdded(hfMemberId.Value, Convert.ToInt32(hfStatusId.Value != string.Empty ? hfStatusId.Value : "0"), false);
            ScriptManager.RegisterClientScriptBlock(lblPayrateCurrency, typeof(MiscUtil), "Show", "parent.ModalOperationCompleted('Offer Details Removed Successfully');", true);
        }

        protected void btnCalculate_Click(object sender, EventArgs e)
        {
            if (txtSalary.Text != "" && ddlSalGrade.SelectedIndex != -1)
            {
                btnSave.Visible = true;
            }
            string gradeValue = ddlSalGrade.SelectedItem.Text.ToString();
            if (ddlSalGrade.SelectedIndex != -1 && txtSalary.Text != "")
            {
                IsPermanent.Visible = true;
                IList<TPS360.Common.BusinessEntities.SalaryTemplate> salTemplate = Facade.GetSalaryTemplate(gradeValue);
                if (salTemplate != null)
                {
                    lblMessage.Text = "";
                    DisplaySalaryStructure(salTemplate);
                }
                else
                {
                    grvSalaryTemplate.DataSource = null;
                    grvSalaryTemplate.DataBind();
                    IsPermanent.Visible = false;
                    btnSave.Visible = false;
                    lblMessage.Text = "Please define Salary template for selected grade";
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "CallMyFunction", "SetIframeSize()", true);
                }
                if (txtSalary.Text == string.Empty)
                {
                    lblMessage.Text = "Please enter Salary ";
                }
            }
            //else
            //{
            //    if (ddlSalGrade.SelectedIndex != -1)
            //    {
            //        lblMessage.Text = "Please define Salary template for selected grade";
            //    }
            //    else if (txtSalary.Text != "")
            //    {
            //        lblMessage.Text = "Please enter Salary ";
            //    }
            //}
        }

        public void DisplaySalaryStructure(IList<TPS360.Common.BusinessEntities.SalaryTemplate> salTemplate)
        {
            DataTable dt = new DataTable();
            DataRow dr = null;
            int itemCount = 0;
            int rowCount = 1;

            dt.Columns.Add(new DataColumn("Heads", typeof(string)));

            for (int i = 0; i < salTemplate.Count; i++)
            {
                dr = dt.NewRow();
                dr["Heads"] = salTemplate[i].Heads.ToString();
                dt.Rows.Add(dr);
            }
            dr = dt.NewRow();
            dr["Heads"] = "Difference Amount";
            dt.Rows.Add(dr);

            dr = dt.NewRow();
            dr["Heads"] = "Total CTC";
            dt.Rows.Add(dr);

            ViewState["CurrentTable"] = dt;

            grvSalaryTemplate.DataSource = dt;
            grvSalaryTemplate.DataBind();

            int pk = 0;
            foreach (GridViewRow row in grvSalaryTemplate.Rows)
            {
                if (rowCount <= salTemplate.Count)
                {
                    SetTextBoxValues(salTemplate[itemCount].Heads.ToString(), salTemplate[itemCount].Calculation.ToString(), Convert.ToDouble(salTemplate[itemCount].Value), row);
                    itemCount++;
                }
                else
                {
                   
                    if (pk == 0)
                    {
                        SetBalancingAmount(row);
                        pk = 1;
                    }
                    else
                    {
                        SetTotalCTC(row);
                    }
                }
                rowCount++;
            }
        }

        public void SetTextBoxValues(string heads, string calculation, double value, GridViewRow row)
        {
            if (row.RowType == DataControlRowType.DataRow)
            {
                TextBox formula = row.Cells[1].FindControl("txtFormulaValue") as TextBox;
                if (calculation != "Fixed Amount")
                {
                   double sal = Convert.ToDouble(txtSalary.Text);
                    double cal = Convert.ToDouble(value);
                    double calculatedHeadValue = ((cal / 100) * sal);
                   
                    if (heads == "Basic")
                    {
                        BasicAmount = calculatedHeadValue;
                        c7 = BasicAmount;
                    }
                    if (heads == "PF")
                    {
                       calculatedHeadValue = ((cal / 100) * BasicAmount);
                       //c8 = calculatedHeadValue;
                    }
                    if (heads == "HRA")
                    {
                        calculatedHeadValue = ((cal / 100) * BasicAmount);
                        c9 = calculatedHeadValue;
                    }
                    if (heads == "Special Allowance")
                    {
                        string gradeValue = ddlSalGrade.SelectedItem.Text.ToString();
                        IList<TPS360.Common.BusinessEntities.SalaryTemplate> salTemplate = Facade.GetSalaryTemplate(gradeValue);
                        if (salTemplate != null)
                        {
                            for (int i = 0; i < salTemplate.Count; i++)
                            {
                                string HeadName = salTemplate[i].Heads.ToString();
                                if (HeadName == "Transport Allowance")
                                {
                                    c8 = salTemplate[i].Value;
                                }
                                if (HeadName == "FCP")
                                {
                                    fcp= salTemplate[i].Value;
                                }
                                if (HeadName == "PF")
                                {
                                    pf = ((salTemplate[i].Value / 100) * BasicAmount);
                                }
                                if (HeadName == "Variable Pay")
                                {
                                    VariablePay = ((salTemplate[i].Value / 100) * sal);
                                }
                            }
                        }
                        total = c7 + c8 + c9;
                        calculatedHeadValue = System.Math.Abs(sal - (fcp + pf) - VariablePay - (total));
                    }
                    //********ADDED NEW CODE ****************                                        
                    balancingAmount += calculatedHeadValue;
                    formula.Text = Convert.ToString(calculatedHeadValue);
                    //**************END*********************
                }
                else
                {
                    balancingAmount += value;
                    formula.Text = Convert.ToString(value);
                }
            }
        }

        public void SetBalancingAmount(GridViewRow row)
        {
            if (row.RowType == DataControlRowType.DataRow)
            {
                TextBox formula = row.Cells[1].FindControl("txtFormulaValue") as TextBox;
                double remainingAmt = Convert.ToDouble(txtSalary.Text) - balancingAmount;
                formula.Text = Convert.ToString(remainingAmt);                
            }
        }
        public void SetTotalCTC(GridViewRow row)
        {
            if (row.RowType == DataControlRowType.DataRow)
            {
                TextBox formula = row.Cells[1].FindControl("txtFormulaValue") as TextBox;
                double remainingAmt = Convert.ToDouble(txtSalary.Text);
                formula.Text = Convert.ToString(remainingAmt);
            }
        }
        #endregion

        public void SaveCalculatedSalary()
        {
            int rowCount = 1;
            MemberHiringDetails memberHiringDetails = Facade.GetMemberHiringDetailsByMemberIdAndJobPostingID(Convert.ToInt32(hfMemberId.Value), JobPostingId);
            int memberHiringId = Convert.ToInt32(memberHiringDetails.Id);

            foreach (GridViewRow row in grvSalaryTemplate.Rows)
            {
                try
                {
                    if (rowCount < grvSalaryTemplate.Rows.Count)
                    {
                        string gradeValue = ddlSalGrade.SelectedItem.Text.ToString();
                        string headValue = row.Cells[0].Text.ToString();
                        TextBox formula = row.Cells[1].FindControl("txtFormulaValue") as TextBox;
                        double formulaValue = Convert.ToDouble(formula.Text);

                        //TextBox TotalCTC = row.Cells[1].FindControl("txtTotalCTC") as TextBox;
                        //double formulaValue = Convert.ToDouble(TotalCTC.Text);

                        string demandType = txtJobTitle.Text.ToString();

                        Facade.AddOfferedSalaryTemplate(JobPostingId, memberHiringId, gradeValue, demandType, headValue, formulaValue);
                        ViewState.Add("MemberHiringId", memberHiringId);
                    }

                }
                catch (Exception ex)
                {
                    throw ex;
                }
                rowCount++;
            }
        }
    }
}