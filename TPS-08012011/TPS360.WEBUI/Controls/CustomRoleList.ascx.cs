﻿using System;
using System.Web.UI.WebControls;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Web.UI.Helper;

namespace TPS360.Web.UI
{
    public partial class ControlCustomRoleList : BaseControl
    {
        #region Member Variables

        #endregion

        #region Methods

        private void BindList()
        {
            this.lsvCustomRole.DataBind();
            //updPanelCustomRole.Update();
        }

        #endregion

        #region Events

        #region Page Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string message = Helper.Url.SecureUrl[UrlConstants.PARAM_MSG];

                if (!StringHelper.IsBlank(message))
                {
                    MiscUtil.ShowMessage(lblMessage, message, false);
                }
                txtSortColumn.Text = "btnName";
                txtSortOrder.Text = "ASC";
                PlaceUpDownArrow();
            }


            string pagesize = (Request.Cookies["CustomRoleListRowPerPage"] == null ? "" : Request.Cookies["CustomRoleListRowPerPage"].Value); ;
            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvCustomRole.FindControl("pagerControl");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)PagerControl.FindControl("pager");
                if (pager != null)
                {
                    pager.PageSize = Convert.ToInt32(pagesize == "" ? "20" : pagesize);
                }
            }


        }

        #endregion

        #region ListView Events
        private void PlaceUpDownArrow()
        {
            try
            {
                LinkButton lnk = (LinkButton)lsvCustomRole.FindControl(txtSortColumn.Text);
                System.Web.UI.HtmlControls.HtmlTableCell im = (System.Web.UI.HtmlControls.HtmlTableCell)lnk.Parent;
                im.EnableViewState = false;
                im.Attributes.Add("class",  ( txtSortOrder .Text  == "ASC" ? "Ascending" : "Descending"));
            }
            catch
            {
            }

        }
        protected void lsvCustomRole_PreRender(object sender, EventArgs e)
        {
            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvCustomRole.FindControl("pagerControl");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)PagerControl.FindControl("pager");
                if (pager != null)
                {
                    DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                    if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
                }
                HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
                if (hdnRowPerPageName != null) hdnRowPerPageName.Value = "CustomRoleListRowPerPage";
            }

            PlaceUpDownArrow();
        }
        protected void lsvCustomRole_ItemDataBound(object sender, ListViewItemEventArgs e)
        {

            bool _IsAdmin = base.IsUserAdmin;
            
            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                CustomRole customRole = ((ListViewDataItem)e.Item).DataItem as CustomRole;

                if (customRole != null)
                {
                    Label lblName = (Label)e.Item.FindControl("lblName");
                    Label lblDescription = (Label)e.Item.FindControl("lblDescription");
                    Label lblLastUpdated = (Label)e.Item.FindControl("lblLastUpdated");
                    ImageButton btnEdit = (ImageButton)e.Item.FindControl("btnEdit");
                    ImageButton btnDelete = (ImageButton)e.Item.FindControl("btnDelete");

                    lblName.Text = customRole.Name;
                    lblDescription.Text = customRole.Description;
                    lblLastUpdated.Text = StringHelper.Convert(UIConstants.SHORT_DATE_FORMAT, customRole.UpdateDate);

                    btnDelete.OnClientClick = "return ConfirmDelete('custom role')";
                    btnEdit.CommandArgument = btnDelete.CommandArgument = StringHelper.Convert(customRole.Id);
                    btnDelete.Visible = _IsAdmin;
                }
            }
        }

        protected void lsvCustomRole_ItemCommand(object sender, ListViewCommandEventArgs e)
        {

             try
              {
                  if (e.CommandName == "Sort")
                  {
                      LinkButton lnkbutton = (LinkButton)e.CommandSource;
                      if (txtSortColumn.Text == lnkbutton.ID)
                      {
                          if (txtSortOrder.Text == "ASC") txtSortOrder.Text = "DESC";
                          else txtSortOrder.Text = "ASC";
                      }
                      else
                      {
                          txtSortColumn.Text = lnkbutton.ID;
                          txtSortOrder.Text = "ASC";
                      }
                  }
              }
              catch
              {
              }
   
            int id;

            int.TryParse(e.CommandArgument.ToString(), out id);

            if (id > 0)
            {
                if (string.Equals(e.CommandName, "EditItem"))
                {
                    Helper.Url.Redirect(UrlConstants.Admin.CUSTOM_ROLE_EDITOR_PAGE, string.Empty, UrlConstants.PARAM_ID, StringHelper.Convert(id));
                }
                else if (string.Equals(e.CommandName, "DeleteItem"))
                {
                    try
                    {
                        if (Facade.DeleteCustomRoleById(id))
                        {
                            BindList();
                            MiscUtil.ShowMessage(lblMessage, "Role has been successfully deleted.", false);
                        }
                    }
                    catch (ArgumentException ex)
                    {
                        MiscUtil.ShowMessage(lblMessage, ex.Message, true);
                    }
                }
            }
        }

        #endregion

        #region Pager Events

        protected void PagerCommand(object sender, DataPagerCommandEventArgs e)
        {
            DataPager pager = e.Item.Pager;
            int newSartRowIndex;

            switch (e.CommandName)
            {
                case "Next":
                    {
                        newSartRowIndex = pager.StartRowIndex + pager.MaximumRows > pager.TotalRowCount ? pager.StartRowIndex : pager.StartRowIndex + pager.MaximumRows;
                        break;
                    }
                case "Previous":
                    {
                        newSartRowIndex = pager.StartRowIndex - pager.MaximumRows < 0 ? pager.StartRowIndex : pager.StartRowIndex - pager.MaximumRows;
                        break;
                    }
                case "Last":
                    {
                        newSartRowIndex = (pager.TotalRowCount / pager.MaximumRows) * pager.MaximumRows;
                        break;
                    }
                case "First":
                    {
                        newSartRowIndex = 0;
                        break;
                    }
                default:
                    {
                        newSartRowIndex = 0;
                        break;
                    }
            }

            e.NewMaximumRows = e.Item.Pager.MaximumRows;
            e.NewStartRowIndex = newSartRowIndex;
        }

        protected void CurrentPageChanged(object sender, EventArgs e)
        {
            TextBox txtCurrentPage = sender as TextBox;

            DataPager pager = this.lsvCustomRole.FindControl("pager") as DataPager;
            int startRowIndex = (int.Parse(txtCurrentPage.Text) - 1) * pager.MaximumRows;
            pager.SetPageProperties(startRowIndex, pager.MaximumRows, true);
        }

        #endregion

        #endregion
    }
}