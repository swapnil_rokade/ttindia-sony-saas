﻿/*
----------------------------------------------------------------------------------------------------------------------------------
    FileName: MemberCopyPasteResume.ascx.cs
    Description: This page is used to paste resume and to save it.
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.        Date              Author             Modification
    ------------------------------------------------------------------------------------------------------------------------------ 
    0.1            Jan-29-2009       Jagadish           Defect Id 9360; Added methods BuildMember(),BuildMemberDetail(),BuildMemberExInfo() and changes made in ParseSaveResume().
    0.2            Aug-11-2009       Gopala Swamy J     Defect Id:11204; Alert message has been modified
   --------------------------------------------------------------------------------------------------------------------------------  
*/
using System;
using System.Text.RegularExpressions;
using System.Web;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPSTool.Parser;
using ParserLibrary; // 0.1

namespace TPS360.Web.UI
{
    public partial class ControlMemberCopyPasteResume : BaseControl
    {
        #region Member Variables

        private int _memberId = 0;
        MemberObjectiveAndSummary _memberObjectiveAndSummary;
        private static string _memberrole = string.Empty;
        private int statusCopyPaste = 0;

        #endregion

        #region Properties
        
        public string MemberRole
        {
            set { _memberrole = value; }
            get { return _memberrole; }
        }

        MemberObjectiveAndSummary CurrentMemberObjectiveAndSummary
        {
            get
            {
                if (_memberObjectiveAndSummary == null)
                {
                    if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]))
                    {
                        _memberId = Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]);
                    }
                    else
                    {
                        _memberId = base.CurrentMember.Id;
                    }
                   
                    if (_memberId > 0)
                    {
                        _memberObjectiveAndSummary = Facade.GetMemberObjectiveAndSummaryByMemberId(_memberId);
                    }
                    
                    if (_memberObjectiveAndSummary == null)
                    {
                        _memberObjectiveAndSummary = new MemberObjectiveAndSummary();
                    }
                }

                return _memberObjectiveAndSummary;
            }
        }

        #endregion

        #region Methods
        private int _currentMemberId=0;
        private int Current_CandidateID
        {
            get
            {
                if (_currentMemberId > 0) return _currentMemberId;
                else
                {
                    if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]))
                    {
                        return Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]);
                    }
                    return 0;
                }
            }
            set
            { _currentMemberId = value; }
        }
        private void LoadQueryStringData()
        {
            if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]))
            {
                _memberId = Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]);
            }
            else
            {
                _memberId = base.CurrentMember.Id;
            }
            if (_memberId > 0)
            {
                hfMemberId.Value = _memberId.ToString();
            }
        }

        private void PrepareEditView()
        {
            //************Code added by pravin khot on 8/Feb/2016*************
            if (Request.Url.ToString().ToLower().Contains("candidateprofile.aspx"))
            {
                txtCopyPasteResume.Text = CurrentMemberObjectiveAndSummary.RawCopyPasteResume;
            }
            //******************End************************************
            else
            {
                txtCopyPasteResume.Text = CurrentMemberObjectiveAndSummary.CopyPasteResume;
            }           
        }

        private void SaveObjectiveSummary()
        {
            try
            {
                string message = string.Empty;
                MemberObjectiveAndSummary objectiveAndSummary = BuildObjectiveAndSummary();
                if (statusCopyPaste != 0)
                {
                    if (objectiveAndSummary.IsNew)
                    {
                        Facade.AddMemberObjectiveAndSummary(objectiveAndSummary);
                        message = "Copy/Paste resume has been added successfully.";
                    }
                    else
                    {
                        Facade.UpdateMemberObjectiveAndSummary(objectiveAndSummary);
                        message = "Copy/Paste resume has been updated successfully.";
                    }
                    Facade.UpdateUpdateDateByMemberId(_memberId);
                    if (statusCopyPaste == 2)
                    {
                        MiscUtil.AddActivity(_memberrole, _memberId, base.CurrentMember.Id, ActivityType.AddCopyPaste, Facade);
                    }
                    else if (statusCopyPaste == 3)
                    {
                        MiscUtil.AddActivity(_memberrole, _memberId, base.CurrentMember.Id, ActivityType.UpdateCopyPaste, Facade);
                    }
                    MiscUtil.ShowMessage(lblMessage, message, false);
                }
                else
                {
                    txtCopyPasteResume.Text = String.Empty;
                    MiscUtil.ShowMessage(lblMessage, "Please enter Copy/Paste resume.", true);
                }
            }
            catch (ArgumentException ex)
            {
                MiscUtil.ShowMessage(lblMessage, ex.Message, true);
            }
        }

   

        private  MemberObjectiveAndSummary BuildObjectiveAndSummary()
        {
            MemberObjectiveAndSummary objectiveAndSummary = CurrentMemberObjectiveAndSummary;
           
            string CopyPaste = txtCopyPasteResume.Text.Trim();
            if (IsValidCopyPasteResume())
            {
                //Add CopyPaste Resume
                if (objectiveAndSummary.CopyPasteResume==null || string.IsNullOrEmpty(objectiveAndSummary.CopyPasteResume.Trim()))
                {
                    statusCopyPaste = 2;
                }
                //Update CopyPaste Resume
                else
                {
                    statusCopyPaste = 3;
                }
            }
            else
            {
                statusCopyPaste = 0;
            }

            CopyPaste = Regex.Replace(CopyPaste, "<script.*?</script>", "", RegexOptions.Singleline | RegexOptions.IgnoreCase);
            objectiveAndSummary.CopyPasteResume = CopyPaste;

            string rawCopyPastResume = Regex.Replace(txtCopyPasteResume.Text.Trim(), @"<(.|\n)*?>", string.Empty);

            objectiveAndSummary.RawCopyPasteResume = HttpUtility.HtmlDecode(rawCopyPastResume);

            if (_memberId > 0)
            {
                objectiveAndSummary.MemberId = _memberId;
            }
            else
            {
                objectiveAndSummary.MemberId = base.CurrentMember.Id;
            }
            objectiveAndSummary.CreatorId = base.CurrentMember.Id;
            objectiveAndSummary.UpdatorId = base.CurrentMember.Id;
            return objectiveAndSummary;
        }

        private bool IsValidCopyPasteResume()
        {
            string copyPaste = HttpUtility.HtmlDecode(txtCopyPasteResume.Text).ToLower();
            copyPaste = copyPaste.Replace("<p>",string.Empty);
            copyPaste = copyPaste.Replace("</p>", string.Empty);
            copyPaste = copyPaste.Replace("\r\n", string.Empty);
            copyPaste = copyPaste.Trim();
            if (!string.IsNullOrEmpty(copyPaste))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion

      
        private string GetDocumentLink(string strFileName, string strDocumenType)
        {

            string strFilePath = MiscUtil.GetMemberDocumentPath(this.Page, Current_CandidateID, strFileName, strDocumenType, true);

            if (strFilePath != strFileName)
            {
                return "<a href='" + strFilePath + "' target='_blank'>" + strFileName + "</a>"; // 0.1
            }
            else
            {
                return strFileName + "(File not found)";
            }
        }
        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Request.RawUrl.ToString().ToLower().Contains("candidateportal"))
            {

                docFrame.Attributes.Add("src", "../CandidatePortal/ResumeUpload.aspx?CanId=" + Current_CandidateID);
            }
            else Current_CandidateID = CurrentMember.Id;
            _memberId = Int32.Parse(hfMemberId.Value);
            if (!IsPostBack)
            {
                MemberDocument memberResume = Facade.GetRecentResumeByMemberID(Current_CandidateID);
                if (memberResume == null)
                {
                    divResume.Style.Add("display", "none");
                    divUploadResume.Style.Add("display", "");
                }
                else
                {
                    //hlkUploadedDoc.Text = memberResume.FileName;
                    hlkUploadedDoc .Text  =GetDocumentLink(memberResume .FileName ,"Word Resume");
                }
                LoadQueryStringData();
                PrepareEditView();
            }
            if (Current_CandidateID > 0)
            {
                string name = MiscUtil.GetMemberNameById(Current_CandidateID, Facade);
                this.Page.Title = name + " - " + "Resume Builder";
            }

           
        }
        protected void btnSaveCopyPaste_Click(object sender, EventArgs e)
        {
            SaveObjectiveSummary();
        }

        #endregion
    }
}