﻿<!-- 
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: MemberPrivacyAvailability.ascx
    Description: This is the user control page used to display the availabilty of the member.
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date             Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1             Sep-11-2008          Shivanand           Defect id: 8653; Added label to display error message if availability date is not selected.    
                                                             Javascript function reference for "Submit" button is removed.
    0.2             Dec-09-2008          Jagadish            Defect id: 8857; Changed the style property of Div tag and set the enable 
                                                             property of button "btnSubmit" to false.
-------------------------------------------------------------------------------------------------------------------------------------------       

-->

<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MemberPrivacyAvailability.ascx.cs"
    Inherits="TPS360.Web.UI.ControlMemberPrivacyAvailability" %>
<%@ Register Assembly="Infragistics35.Web.v12.2, Version=12.2.20122.1007, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.Web.UI.EditorControls" TagPrefix="ig" %>

<%@ Register assembly="Infragistics35.Web.v12.2, Version=12.2.20122.1007, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" namespace="Infragistics.Web.UI.LayoutControls" tagprefix="ig" %>

<script type="text/javascript" language="javascript">
        
</script>

<div style="margin-left:100px">
    <div class="TableRow" style="text-align: center">
        <asp:Label ID="lblMessage" runat="server"></asp:Label>
    </div>
        
    <div style="width: 100%;">
        <div class="TableRow">
            <div class="FormLeftColumn" style="width: 60%;">
                <div class="TableRow">
                    <div class="TableFormContent" style="margin-left: auto; margin-right: auto">
                        <asp:RadioButtonList ID="rdbtnlstAvailability" AutoPostBack="true" 
                            RepeatColumns="1" RepeatDirection="Vertical"
                            runat="server" 
                            onselectedindexchanged="rdbtnlstAvailability_SelectedIndexChanged">
                        </asp:RadioButtonList>
                    </div>
                </div>
            </div>
            <%--<div class="FormRightColumn" style="width: 30%; margin-top: -85px; margin-right: 180px;">--%>
            <div class="FormRightColumn" style="width: 40%; margin-top: -56px; margin-right: 85px;"> <%--0.2--%>
                <div class="TableRow">
                    <div class="TableFormContent">
                       <%-- <igsch:WebDateChooser ID="wdcDateOfBirth" runat="server" Editable="True" NullDateLabel=""   CalendarLayout-HideOtherMonthDays ="true"  >
                        </igsch:WebDateChooser>--%> 
                        <ig:WebDatePicker ID="wdcDateOfBirth"  DropDownCalendarID="ddcEmpDurationFromDate" runat="server"></ig:WebDatePicker>
                        <ig:WebMonthCalendar  ID="ddcEmpDurationFromDate" AllowNull="true"  runat="server"></ig:WebMonthCalendar>                       
                    </div>
                    <div class="TableFormContent">
                        <asp:Label ID="lblErrorDate" runat="server" ForeColor="Red" Text=""></asp:Label> <!-- 0.1 -->                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="TableRow" style="text-align: center;">
        <asp:Button ID="btnSubmit" CssClass="CommonButton" runat="server" Text="Submit" OnClick="btnSubmit_Click"/> <!-- 0.1 and 0.2 -->
    </div>
</div>
