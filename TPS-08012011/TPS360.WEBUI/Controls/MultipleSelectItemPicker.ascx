﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MultipleSelectItemPicker.ascx.cs" Inherits="MultipleSelectItemPicker" %>




<%-- 
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: MultipleSelect2ItemPicker.ascx
    Description: 
    Created By: added by pravin khot from dispark
    Created On: 26/Feb/2016
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date             Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
   
-------------------------------------------------------------------------------------------------------------------------------------------       
--%>

<link href ="../assets/css/select2.css" rel ="Stylesheet" />
<asp:Panel ID="pnlRequisition" runat="server">
<asp:HiddenField ID="hdnSelectedListItems" runat ="server" EnableViewState ="true"  />
              <asp:ListBox ID="ddlItem" runat="server" CssClass="select2-container select2-container-multi" EnableViewState="true" SelectionMode ="Single" 
                AutoPostBack="false" Width="158px" TabIndex="26">
            </asp:ListBox>
  </asp:Panel>
<script src="../assets/js/select2.js" type ="text/javascript" ></script>
   <script type="text/javascript"> 

           Sys.Application.add_load(function() {
           $('#<%=ddlItem.ClientID%>').select2({
               placeholder: "Please Select"
           });

           $('#<%=ddlItem.ClientID %>').on('change', function() {
               var selectedUsers = $('#<%=hdnSelectedListItems.ClientID %>');
               selectedUsers.val($('#<%=ddlItem.ClientID %>').val());
           });

           $(document).ready(function() {
               $("#<%= pnlRequisition.ClientID %>").find("select").each(function(i) {
                   $(this).select2();
               });
           });
       });
    </script>