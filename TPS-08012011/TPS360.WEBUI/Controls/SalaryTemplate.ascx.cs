﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using TPS360.Web.UI;
using System.Data.Common;
using TPS360.Common.Helper;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Diagnostics;
using TPS360.Common.Shared;
using System.Collections.Generic;

public partial class Controls_SalaryTemplate : RequisitionBaseControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            MiscUtil.PopulateSalaryGrade(ddlSalGrade, Facade);
            MiscUtil.GetSalaryHeads(lstHeads, Facade);
            DisplaySalaryTemplate(lstHeads);
        }
    }
    public void DisplaySalaryTemplate(ListBox lstHeads)
    {
        DataTable dt = new DataTable();
        DataRow dr = null;
        dt.Columns.Add(new DataColumn("Heads", typeof(string)));

        for (int i = 0; i < lstHeads.Items.Count; i++)
        {
            dr = dt.NewRow();
            dr["Heads"] = lstHeads.Items[i].Text.ToString();
            dt.Rows.Add(dr);
        }

        ViewState["CurrentTable"] = dt;

        grvSalaryCalulator.DataSource = dt;
        grvSalaryCalulator.DataBind();
    }
    protected void btnSaveSalaryTemplate_Click(object sender, EventArgs e)
    {
        foreach (GridViewRow row in grvSalaryCalulator.Rows)
        {
            try
            {
                string gradeValue = ddlSalGrade.SelectedItem.Text.ToString();

                string headValue = row.Cells[0].Text.ToString();

                DropDownList calulation = row.Cells[1].FindControl("ddlCalculation") as DropDownList;
                string calulationValue = calulation.SelectedItem.Text.ToString();

                TextBox formula = row.Cells[2].FindControl("txtFormulaValue") as TextBox;
                double formulaValue = Convert.ToDouble(formula.Text);

                Facade.AddSalaryTemplate(headValue, calulationValue, formulaValue, gradeValue);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        MiscUtil.ShowMessage(lblMessage, "Successfully saved changes for Salary Template.", false);
    }
    protected void ddlSalGrade_SelectedIndexChanged(object sender, EventArgs e)
    {
        string gradeValue = ddlSalGrade.SelectedItem.Text.ToString();
        IList<TPS360.Common.BusinessEntities.SalaryTemplate> salTemplate = Facade.GetSalaryTemplate(gradeValue);
        if (salTemplate != null)
        {
            for (int k = 0; k < lstHeads.Items.Count; k++)
            {
                bool IsExist = false;
                string genericLookupHead = lstHeads.Items[k].Text.ToString();
                for (int j = 0; j < salTemplate.Count; j++)
                {
                    if (genericLookupHead == salTemplate[j].Heads.ToString())
                    {
                        IsExist = true;
                        break;
                    }
                }
                if (IsExist == false)
                {
                    salTemplate.Add(new TPS360.Common.BusinessEntities.SalaryTemplate { NewGrade = "", Heads = genericLookupHead, Value = 0, Calculation = "" });
                }
            }
            DisplayDefindedGradeTemplate(salTemplate);
        }
        else
        {
            DisplaySalaryTemplate(lstHeads);
        }
    }

    public void DisplayDefindedGradeTemplate(IList<TPS360.Common.BusinessEntities.SalaryTemplate> lstHeads)
    {
        DataTable dt = new DataTable();
        DataRow dr = null;
        int itemCount = 0;

        dt.Columns.Add(new DataColumn("Heads", typeof(string)));

        for (int i = 0; i < lstHeads.Count; i++)
        {
            dr = dt.NewRow();
            dr["Heads"] = lstHeads[i].Heads.ToString();
            dt.Rows.Add(dr);
        }

        ViewState["CurrentTable"] = dt;

        grvSalaryCalulator.DataSource = dt;
        grvSalaryCalulator.DataBind();

        foreach (GridViewRow row in grvSalaryCalulator.Rows)
        {
            SetDDLTextBoxValues(lstHeads[itemCount].Calculation.ToString(), Convert.ToDouble(lstHeads[itemCount].Value), row);
            itemCount++;
        }
    }

    public void SetDDLTextBoxValues(string calculation, double value, GridViewRow row)
    {
        if (row.RowType == DataControlRowType.DataRow)
        {
            DropDownList ddlCal = row.Cells[1].FindControl("ddlCalculation") as DropDownList;
            if (ddlCal.Items.Contains(ddlCal.Items.FindByText(calculation.ToString())))
            {
                ddlCal.Items.FindByText(calculation.ToString()).Selected = true;
            }

            TextBox formula = row.Cells[2].FindControl("txtFormulaValue") as TextBox;
            formula.Text = Convert.ToString(value);
        }
    }
}
