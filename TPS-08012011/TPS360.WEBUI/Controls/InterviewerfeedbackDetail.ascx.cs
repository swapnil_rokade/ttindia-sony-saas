﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: ControlsInterviewerfeedbackDetail.ascx.cs
    Description: This is the user control page used for member interviewer feedback details show
    Created By: pravin khot
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
 */


using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using TPS360.Web.UI.Helper;
using TPS360.Common.BusinessEntities;
using System.Text;
using TPS360.BusinessFacade;
using TPS360.Common.Helper;
using System.Text.RegularExpressions;
using System.Collections.Generic;
namespace TPS360.Web.UI
{
    public partial class ControlsInterviewerfeedbackDetail : BaseControl
    {
        private static int _memberId = 0;

        private bool IsNew = false;
        public int MemberId
        {
            set { _memberId = value; }
        }
        public int InterviewId 
        {
            get 
            {
                int _interviewId = 0;
                _interviewId = Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_INTERVIEWID].ToString());
                return _interviewId;
            }
        }
        public string InterviewTitle
        {
            get
            {
                string _interviewTitle = "";
                _interviewTitle = Helper.Url.SecureUrl[UrlConstants.PARAM_INTERVIEWTITLE].ToString();
                return _interviewTitle;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {

            
            
            if (!IsPostBack) 
            {
                PrepareView();
                
            }
                
           
        }
        #region Listview Events

        protected void lsvFeedback_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            bool _isAdmin = base.IsUserAdmin;
            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                InterviewFeedback feedback = ((ListViewDataItem)e.Item).DataItem as InterviewFeedback;

                if (feedback != null)
                {
                    Label lblInterviewerName = (Label)e.Item.FindControl("lblInterviewerName");
                    Label lblInterviewerType = (Label)e.Item.FindControl("lblInterviewerType");
                    ImageButton btnFeedback = (ImageButton)e.Item.FindControl("btnFeedback");
                    btnFeedback.ToolTip = "Show Interviewer Feedback/Assessment Report Form";

                    lblInterviewerName.Text = feedback.Title;
                    lblInterviewerType.Text =  feedback.Feedback;

                    btnFeedback.CommandArgument = StringHelper.Convert(feedback.InterviewId );

                    //SecureUrl Url1 = UrlHelper.BuildSecureUrl(UrlConstants.ApplicationBaseUrl + "Reports/InterviewFeedbackReports.aspx", "", UrlConstants.PARAM_INTERVIEWID, feedback.InterviewId.ToString(), UrlConstants.PARAM_INTERVIEWTITLE, feedback.Title);

                    //btnFeedback.Attributes.Add("onclick", "EditModal('" + Url1.ToString() + "','700px','570px'); return false;");

                }

            }
        }
        protected void lsvFeedback_ItemCommand(object sender, ListViewCommandEventArgs e) 
        { 
            int id;

            int.TryParse(e.CommandArgument.ToString(), out id);

                if (id > 0)
                {
                    if (string.Equals(e.CommandName, "EditItem"))
                    {                     
                        //InterviewFeedback feedback = Facade.InterviewFeedback_GetById(id);
                        InterviewFeedback feedback = new InterviewFeedback();
                        feedback.Email = "pravin.s298@gmail.com";
                        Session["key"] = "pk";

                        //SecureUrl Url1 = UrlHelper.BuildSecureUrl(UrlConstants.ApplicationBaseUrl + "Reports/InterviewFeedbackReports.aspx", "", UrlConstants.PARAM_INTERVIEWID, feedback.InterviewId.ToString(), UrlConstants.PARAM_INTERVIEWTITLE, feedback.Title);
                        SecureUrl Url1 = UrlHelper.BuildSecureUrl(UrlConstants.ApplicationBaseUrl + "Reports/InterviewFeedbackReports.aspx", "?id=123&Name='pk'");
                        String url = Url1.ToString();
                        HttpCookie cookie = new HttpCookie("PreviousPage");
                        cookie.Value = feedback.Email;
                        Response.Cookies.Add(cookie);

                       
                        Response.Redirect(url);                     
                                           
                    }
                    if (string.Equals(e.CommandName, "DeleteItem"))
                    {
                        Facade.InterviewFeedback_DeleteById(id);
                      
                        PopulateFeedback();
                        ClearControls();
                        
                    }
                    
                }
        }
        protected void lsv_Feedback_OnRender(object sender, EventArgs e) 
        { 
        
        }
        #endregion
        protected void btnSave_Click(object sender, EventArgs e) 
        {
            InterviewFeedback feedback = BuildInterviewfeedback();
            string msg = "";
            if (hdnInterviewFeebackId.Value != "" && hdnInterviewFeebackId.Value != null && hdnInterviewFeebackId.Value != "0")
            {

                feedback.Id = Convert.ToInt32(hdnInterviewFeebackId.Value);

                Facade.InterviewFeedback_Update(feedback);

                msg = "Updated";
            }
            else
            {
                Facade.InterviewFeedback_Add(feedback);

                msg = "Added";
            }

            MiscUtil.ShowMessage(lblMessage, "The Interview Feedback have been " + msg + " Sucessfully", false);
            PopulateFeedback();
            ClearControls();
        }

        public InterviewFeedback BuildInterviewfeedback() 
        {
            InterviewFeedback feedback = new InterviewFeedback();

            //feedback.Title = txtTitle.Text.Trim();
            //feedback.Feedback = txtNotes.Text.ToString().Trim();
            //feedback.InterviewRound = Convert.ToInt32(ddlInterviewRounds.SelectedValue);
            feedback.CreatorId = CurrentMember.Id;
            feedback.UpdatorId = CurrentMember.Id;
            feedback.InterviewId = InterviewId;

            return feedback;
        }
        public void ClearControls() 
        {
            //txtNotes.Text = "";
            //txtTitle.Text = "";
            //hdnInterviewFeebackId.Value = "";
            //MiscUtil.PopulateInterviewRounds(ddlInterviewRounds, Facade);
            //IsNew = true;
        }

        public void PopulateFeedback() 
        {
            lsvFeedback.DataSource = Facade.InterviewFeedback_GetByInterviewIdDetail(InterviewId);
            lsvFeedback.DataBind();
        }

        private void PrepareView()
        {
            PopulateFeedback();
            //MiscUtil.PopulateInterviewRounds(ddlInterviewRounds, Facade);
        }
        //protected void lsvInterviewFeedback_ItemDataBound(object sender, ListViewItemEventArgs e)
        //{ }
    }
}


