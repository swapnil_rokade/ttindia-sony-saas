﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: MemberEducationEditor.ascx.cs
    Description: This is the user control page used in resume builder to allow user to provide education information.
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1             Apr-29-2009           Rajendra Prasad     Defect id: 10397;Added new method PopulateSiteSettings() to display
                                                              ddlcountry selected value according to sitesettings.
    0.2             Jul-22-2009           Veda                Defect id: 11065; Added a new field 'State'. 
    0.3             Jul-30-2009           Veda                Defect id: 11132; Async mode is set to on 
    0.4             Sep-18-2009           Veda                Defect id: 11502; Default site settings country is not displayed.
 *  0.5             Oct-14-2009           Ranjit Kumar.I      Defect Id: 11590; Code commented for defect in lsvEducation_ItemCommand
    0.6             Nov-05-2009           Rajendra            Defect ID: 11595; Code added in methods PrepareEditView() & Clear().
    0.7             Nov-09-2009           Rajendra            Defect ID: 11595; Code added in Item Command;
 *  0.8             May-05-2010           Ganapati Bhat       Defect Id: 12753; Code Added in PrepareEditView()
------------------------------------------------------------------------------------------------------------------------------------------- 
*/
using System;
using System.Web.UI.WebControls;
using System.Collections;
using System.Text.RegularExpressions;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Web.UI.Helper;
using TPS360.Common.Shared;
using System.Collections.Generic;

namespace TPS360.Web.UI
{
    public partial class ControlMemberEducationEditor : BaseControl
    {
        #region Member Variables

        private static int _memberId = 0;
        private static int _educationId = 0;
        private static string _memberrole = string.Empty;
        MemberEducation _memberEducation;
        private bool IsManager = false;
        private bool IsOverviewPage = false;
        #endregion

        #region Properties

        public string MemberRole
        {
            set { _memberrole = value; }
            get { return _memberrole; }
        }

        private void GetMemberId()
        {
            //From Member Profile
            if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]))
            {
                _memberId = Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]);
            }
            //From Member Login
            else
            {
                _memberId = base.CurrentMember.Id;
            }
        }
        MemberEducation CurrentMemberEducation
        {
            get
            {
                if (_memberEducation == null)
                {
                    if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_ID]))
                    {
                        _educationId = Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_ID]);
                    }

                    if (_educationId > 0)
                    {
                        _memberEducation = Facade.GetMemberEducationById(_educationId);
                    }
                    else
                    {
                        _memberEducation = new MemberEducation();
                    }
                }

                return _memberEducation;
            }
        }

        #endregion

        #region Methods

        private void BindList()
        {
            if (_memberId > 0)
            {
                odsEducationList.SelectParameters["memberId"].DefaultValue = _memberId.ToString();
            }
            this.lsvEducation.DataSourceID = "odsEducationList";
            this.lsvEducation.DataBind();
        }

        private void SaveMemberEducation()
        {
            if (IsValid)
            {
                try
                {
                    MemberEducation memberEducation = BuildMemberEducation();
                    if (txtInstituteName.Text.Trim().ToString()!=string .Empty)
                    {
                        if (memberEducation.IsNew)
                        {
                                 Facade.AddMemberEducation(memberEducation);
                                 ClearText();
                                 MiscUtil.ShowMessage(lblMessage, "Education has been added successfully.", false);
                        }
                        else
                        {
                            Facade.UpdateMemberEducation(memberEducation);
                            MiscUtil.AddActivity(_memberrole, _memberId, base.CurrentMember.Id, ActivityType.UpdateEducation, Facade);
                           ClearText();
                            MiscUtil.ShowMessage(lblMessage, "Education has been updated successfully.", false);
                        }
                        Facade.UpdateUpdateDateByMemberId(_memberId);
                        BindList();
                        _educationId = 0;
                    }
                }
                catch (ArgumentException ex)
                {
                    MiscUtil.ShowMessage(lblMessage, ex.Message, true);
                }
            }
        }
        private MemberEducation BuildMemberEducation()
        {
            MemberEducation memberEducation = CurrentMemberEducation;

            memberEducation.InstituteName = txtInstituteName.Text=MiscUtil .RemoveScript (txtInstituteName.Text.Trim());
            memberEducation.LevelOfEducationLookupId = Convert.ToInt32(ddlLevelOfEducation.SelectedValue);
            memberEducation.DegreeTitle = txtDegree.Text = MiscUtil.RemoveScript(txtDegree.Text.Trim());
            memberEducation.AttendedFrom = dtDuration.StartDate;
            memberEducation.AttendedTo = dtDuration.EndDate;
            memberEducation.DegreeObtainedYear = dtDuration.EndDate.ToShortDateString();
            memberEducation.MajorSubjects = txtMajor.Text = MiscUtil.RemoveScript(txtMajor.Text.Trim());
            memberEducation.InstituteCity = txtCity.Text = MiscUtil.RemoveScript(txtCity.Text.Trim());
            memberEducation.InstituteCountryId = uclCountryState.SelectedCountryId;
            memberEducation.GPA = txtGPA.Text = MiscUtil.RemoveScript(txtGPA.Text.Trim());
            memberEducation.BriefDescription = txtAccomplishment.Text = MiscUtil.RemoveScript(txtAccomplishment.Text.Trim());
            memberEducation.IsHighestEducation = chkHighestEducation.Checked;
            if (_memberId > 0)
            {
                memberEducation.MemberId = _memberId;
            }
            else
            {
                memberEducation.MemberId = base.CurrentMember.Id;
            }
            memberEducation.CreatorId = base.CurrentMember.Id;
            memberEducation.UpdatorId = base.CurrentMember.Id;
            memberEducation.StateId = uclCountryState.SelectedStateId;
            return memberEducation;
        }

       
        private void PopulateSiteSettings(DropDownList ddlCountry)
        {
            if (SiteSetting != null)
                ddlCountry.SelectedValue = (SiteSetting[DefaultSiteSetting.Country.ToString()].ToString());
        }
       

        private void PrepareView()
        {
            MiscUtil.PopulateEducationQualification(ddlLevelOfEducation, Facade);
        }

        private void PrepareEditView()
        {
            MemberEducation memberEducation = Facade.GetMemberEducationById(_educationId);

            if (memberEducation != null)
            {
                txtInstituteName.Text =MiscUtil .RemoveScript ( memberEducation.InstituteName,string .Empty );
                txtMajor.Text = MiscUtil .RemoveScript ( memberEducation.MajorSubjects,string .Empty );
                txtDegree.Text = MiscUtil .RemoveScript ( memberEducation.DegreeTitle,string .Empty );
              if(memberEducation.LevelOfEducationLookupId>0)  ddlLevelOfEducation.SelectedValue = Convert.ToString(memberEducation.LevelOfEducationLookupId);

              dtDuration.setDateRange(memberEducation.AttendedFrom, memberEducation.AttendedTo);
                //wdcStartDate.Value   = memberEducation.AttendedFrom.Year <1900?DateTime .MinValue  :memberEducation .AttendedFrom;
                //wdcEndDate.Value = memberEducation.AttendedTo.Year <1900?DateTime .MinValue :memberEducation .AttendedTo ;
                txtGPA.Text =MiscUtil .RemoveScript (  memberEducation.GPA,string .Empty );
//                txtOutOf.Text =MiscUtil .RemoveScript (  memberEducation.GpaOutOf,string .Empty );
                txtCity.Text = MiscUtil .RemoveScript ( memberEducation.InstituteCity,string .Empty );
                uclCountryState.SelectedCountryId = memberEducation.InstituteCountryId;
                uclCountryState.SelectedStateId = memberEducation.StateId;
                txtAccomplishment.Text = MiscUtil.RemoveScript(memberEducation.BriefDescription, string.Empty);
                chkHighestEducation.Checked = memberEducation.IsHighestEducation;
            }
        }

        private void ClearText()
        {
          
            txtInstituteName.Text = "";
            txtMajor.Text = "";
            txtDegree.Text = "";
            ddlLevelOfEducation.SelectedIndex = 0;
            dtDuration.ClearRange();
            txtGPA.Text = "";
//            txtOutOf.Text = "";
            txtCity.Text = "";
            uclCountryState.SelectedCountryId = 0;
            txtAccomplishment.Text = "";
            chkHighestEducation.Checked = false;
        }
        private void PlaceUpDownArrow()
        {
            try
            {
                LinkButton lnk = (LinkButton)lsvEducation.FindControl(hdnSortColumn.Text );
                System.Web.UI.HtmlControls.HtmlTableCell im = (System.Web.UI.HtmlControls.HtmlTableCell)lnk.Parent;
                im.EnableViewState = false;
                im.Attributes.Add("class", (hdnSortOrder.Text  == "ASC" ? "Ascending" : "Descending"));
            }
            catch
            {
            }

        }
        #endregion

        #region Events

        #region Page event

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.Request.Url.AbsoluteUri.ToString().Contains("TPS360Overview.aspx"))
            {
                IsOverviewPage = true;
            }
            GetMemberId();
          
            MemberManager Manager = new MemberManager();
            Manager = Facade.GetMemberManagerByMemberIdAndManagerId(_memberId, CurrentMember.Id);
            if (Manager != null || IsUserAdmin)
                IsManager = true;
            else
                IsManager = false;

           
            if(!IsPostBack)
            {
                BindList();
                hdnSortColumn.Text = "lnkLevel";
                hdnSortOrder.Text = "Desc";
                if (IsOverviewPage)
                {
                    divEducationInputBlock.Visible = false;
                    return;
                }
                PrepareView();             
            }
            string pagesize = "";
            pagesize = (Request.Cookies["EducationRowPerPage"] == null ? "" : Request.Cookies["EducationRowPerPage"].Value); ;
              ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvEducation.FindControl("pagerControl");
              if (PagerControl != null)
              {
                  DataPager pager = (DataPager)PagerControl.FindControl("pager");
                  if (pager != null)   pager.PageSize = Convert.ToInt32(pagesize == "" ? "20" : pagesize);
              }
            
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            LblDatevalidate.Visible = false;
           /*
               
                if (wdcStartDate.Text.Trim() != "" && wdcEndDate.Text.Trim() != "")
                {
                    if (Convert.ToDateTime(wdcStartDate.Value) > DateTime.Now)
                    {
                        LblDatevalidate.Text = "Duration' Start date should not be after the current date.";
                        LblDatevalidate.Visible = true;
                        return;
                    }
                   
                    if (Convert.ToDateTime(wdcEndDate.Value) > DateTime.Now)
                    {
                        LblDatevalidate.Text = "Duration' End date should not be after the current date.";
                        LblDatevalidate.Visible = true;
                        return;
                    }
                  
                    if (Convert.ToDateTime(wdcEndDate.Value) < Convert.ToDateTime(wdcStartDate.Value))
                    {
                        LblDatevalidate.Text = "'Duration' Start date should not be after the End Date.";
                        LblDatevalidate.Visible = true;
                        return;
                    }
                   
                }
                rfvInstituteName.Enabled = true;
                //cvLevelOfEducation.Enabled = true;
            */
            
                SaveMemberEducation();
                uclCountryState.SelectedCountryId = 0;
        }

        #endregion

        #region ListView Events

        protected void lsvEducation_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                MemberEducation memberEducation = ((ListViewDataItem)e.Item).DataItem as MemberEducation;

                if (memberEducation != null)
                {
                    Label lblInstitutionName = (Label)e.Item.FindControl("lblInstitutionName");
                    Label lblDegree = (Label)e.Item.FindControl("lblDegree");
                    Label lblDuration = (Label)e.Item.FindControl("lblDuration");
                    Label lblMajor = (Label)e.Item.FindControl("lblMajor");
                    Label lblLevel = (Label)e.Item.FindControl("lblLevel");
                    Label lblGPA = (Label)e.Item.FindControl("lblGPA");
                    Label lblLocation = (Label)e.Item.FindControl("lblLocation");
                    Label lblNotes = (Label)e.Item.FindControl("lblNotes");
                    if (memberEducation.IsHighestEducation)
                    {
                        System.Web.UI.HtmlControls.HtmlGenericControl lblHigherEducation = (System.Web.UI.HtmlControls.HtmlGenericControl)e.Item.FindControl("lblHigherEducation");
                        lblHigherEducation.Visible = true;
                    }
                    if (memberEducation.LevelOfEducationLookupId > 0)
                    {
                        GenericLookup lookup = Facade.GetGenericLookupById(memberEducation.LevelOfEducationLookupId);
                        if (lookup != null) lblLevel.Text = lookup.Name;
                    }
                    if (memberEducation.GPA.Trim() != "")
                    {
                        lblGPA.Text = memberEducation.GPA;
                        if (lblGPA.Text != "" && memberEducation.GpaOutOf != "") lblGPA.Text += " / ";
                        lblGPA.Text += memberEducation.GpaOutOf;
                    }
                    //Biding Notes
                     //lblNotes.Text = memberEducation.BriefDescription.Length <= 15 ? memberEducation.BriefDescription :( memberEducation.BriefDescription.Substring (0,15) + "...");
                     lblNotes.Text = memberEducation.BriefDescription;

                    //Binding Location
                     lblLocation.Text = memberEducation.InstituteCity;
                    if(memberEducation .StateId >0)
                    {
                        string  state = Facade.GetStateNameById(memberEducation.StateId);
                        if (state != null && state !=string .Empty )
                        {
                            if (lblLocation.Text.Trim() != "" && state != "") lblLocation.Text += ", ";
                            lblLocation.Text += state;
                        }
                    }
                    ImageButton btnEdit = (ImageButton)e.Item.FindControl("btnEdit");
                    ImageButton btnDelete = (ImageButton)e.Item.FindControl("btnDelete");

                    lblInstitutionName.Text = memberEducation.InstituteName;
                    lblDegree.Text = memberEducation.DegreeTitle;
                    //Binding Duration
                    lblDuration.Text = memberEducation.AttendedFrom.Year == 1 || memberEducation.AttendedFrom.Year < 1900 ? "" : memberEducation.AttendedFrom.Year.ToString();
                    if (lblDuration.Text.Trim() != "" && memberEducation.AttendedTo.Year != 1 && memberEducation.AttendedTo.Year > 1900) lblDuration.Text += " - ";
                    lblDuration.Text += memberEducation.AttendedTo.Year != 1 && memberEducation.AttendedTo.Year >1900 ? memberEducation.AttendedTo.Year.ToString() : "";
                    lblMajor.Text = memberEducation.MajorSubjects;
                    
                    btnDelete.OnClientClick = "return ConfirmDelete('education entry')";
                    if (IsManager)
                        btnDelete.Visible = true;
                    else
                        btnDelete.Visible = false;
                    btnEdit.CommandArgument = btnDelete.CommandArgument = StringHelper.Convert(memberEducation.Id);

                    if (IsOverviewPage)
                    {
                        System.Web.UI.HtmlControls.HtmlTableCell thAction = (System.Web.UI.HtmlControls.HtmlTableCell)lsvEducation.FindControl("thAction");
                        System.Web.UI.HtmlControls.HtmlTableCell tdAction = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdAction");
                        System.Web.UI.HtmlControls.HtmlTableCell thGPA = (System.Web.UI.HtmlControls.HtmlTableCell)lsvEducation.FindControl("thGPA");
                        System.Web.UI.HtmlControls.HtmlTableCell tdGPA = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdGPA");
                        System.Web.UI.HtmlControls.HtmlTableCell thLocation = (System.Web.UI.HtmlControls.HtmlTableCell)lsvEducation.FindControl("thLocation");
                        System.Web.UI.HtmlControls.HtmlTableCell tdLocation = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdLocation");
                        System.Web.UI.HtmlControls.HtmlTableCell thNotes = (System.Web.UI.HtmlControls.HtmlTableCell)lsvEducation.FindControl("thNotes");
                        System.Web.UI.HtmlControls.HtmlTableCell tdNotes = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdNotes");
                        thAction.Visible = tdAction.Visible =thGPA .Visible =tdGPA .Visible =thLocation .Visible =tdLocation .Visible =thNotes .Visible =tdNotes .Visible = !IsOverviewPage;


                        System.Web.UI.HtmlControls.HtmlTableCell tdPager = (System.Web.UI.HtmlControls.HtmlTableCell)lsvEducation.FindControl("tdPager");
                        tdPager.ColSpan = 5;
                    }
                }
            }
        }
      
        protected void lsvEducation_PreRender(object sender, EventArgs e)
        {
            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvEducation.FindControl("pagerControl");
             if (PagerControl != null)
             {
                 DataPager pager = (DataPager)PagerControl.FindControl("pager");
                 if (pager != null)
                 {
                     DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                     if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
                 }
                 HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
                 if (hdnRowPerPageName != null) hdnRowPerPageName.Value = "EducationRowPerPage";
             }
             PlaceUpDownArrow();

             if (lsvEducation.Items.Count == 0)
             {
                 lsvEducation.DataSource = null;
                 lsvEducation.DataBind();
             }
        }

        protected void lsvEducation_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Sort")
                {
                    LinkButton lnkbutton = (LinkButton)e.CommandSource;
                    if (hdnSortColumn.Text  == lnkbutton.ID)
                    {
                        if (hdnSortOrder.Text == "ASC") hdnSortOrder.Text  = "DESC";
                        else  hdnSortOrder.Text  = "ASC";
                    }
                    else
                    {
                        hdnSortColumn.Text  = lnkbutton.ID;
                        hdnSortOrder.Text  = "ASC";
                    }
                }
            }
            catch
            {
            }
            int id;

            int.TryParse(e.CommandArgument.ToString(), out id);

            if (id > 0)
            {
                if (string.Equals(e.CommandName, "EditItem"))
                {
                    _educationId = id;

                    PrepareEditView();
                }
                else if (string.Equals(e.CommandName, "DeleteItem"))
                {
                    try
                    {
                        if (Facade.DeleteMemberEducationById(id))
                        {
                            BindList();
                            MiscUtil.AddActivity(_memberrole, _memberId, base.CurrentMember.Id, ActivityType.DeleteEducation, Facade);
                            if(_educationId ==id )
                                ClearText();
                            uclCountryState.SelectedCountryId = 0;
                            MiscUtil.ShowMessage(lblMessage, "Education has been successfully deleted.", false);
                        }
                    }
                    catch (ArgumentException ex)
                    {
                        MiscUtil.ShowMessage(lblMessage, ex.Message, true);
                    }
                }
            }
          
        }

        #endregion

        #endregion
    }
}
