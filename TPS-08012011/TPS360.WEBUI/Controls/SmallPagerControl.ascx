﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SmallPagerControl.ascx.cs" 
    Inherits="TPS360.Web.UI.SmallPagerControl"  EnableViewState ="true" %>
    <script >

Sys.Application.add_load(function() { 
try{
 $( '.pagerSlider' ).keypress( function (e) {
                 // allow backspace and delete 
                 if  (e.which == 8 || e.which == 0)
                     return  true;

                 //if the letter is not digit 
                 if  (e.which < 48 || e.which > 57)
                     return  false;

                 // check max range 
                 var dest = e.which - 48;
                 var result = this.value + dest.toString();
                 if  (parseInt (result,0) >parseInt ($(this).next().val()) || parseInt (result ,0)<=0) {
                     return  false;
                 }
             });
             }
             catch (e)
             {
             }
            
} );

    </script>
    <style>
    .pagerSlider
    {
    }
    </style>
 <asp:Label ID="lblMessage" runat ="server" EnableViewState ="false" ></asp:Label>
 <asp:HiddenField ID="hdnRowPerPageName" runat ="server" />
 <asp:HiddenField ID="hdnTotalRowCount" runat ="server" />
 <asp:HiddenField ID="hdnStartRowIndex" runat ="server" />
 <asp:HiddenField ID="hdnMaxRow" runat ="server" />
 <div class="Container" >
                                                <asp:DataPager runat="server" ID="pager" PageSize="10" EnableViewState ="true" >
                                                     <Fields>
                                                            <asp:TemplatePagerField OnPagerCommand="PagerCommand">
                                                                <PagerTemplate>
                                                                <div style =" width :100%;">
                                                                    <div style ="width :30%;  text-align :left ;  float :left ; white-space :nowrap ">
                                                                         <asp:DropDownList  Font-Size = "12px"  Height  = "20px" ID="ddlRowPerPage" runat ="server"   OnSelectedIndexChanged ="ddlRowPerPage_SelectedIndexChanged" EnableViewState ="true" CausesValidation ="false" AutoPostBack ="true"  >
                                                                            <asp:ListItem Value ="10" Selected ="True">10</asp:ListItem>
                                                                            <asp:ListItem Value ="20">20</asp:ListItem>
                                                                            <asp:ListItem Value ="50">50</asp:ListItem>
                                                                            <asp:ListItem Value ="75">75</asp:ListItem>
                                                                            <asp:ListItem Value ="100">100</asp:ListItem>
                                                                        </asp:DropDownList> 
                                                                    
                                                                     </div>
                                                                    
                                                                    <div style ="width :65%; float :right; text-align :center  "  align="left"  runat ="server" id="divPager"     >
                                                               
                                                                            <div class="Command"  style =" padding-top :3px;" > 
                                                                            
                                                                                 <asp:ImageButton ID="btnFirstDisabled" runat="server"  SkinID="sknFirstPageSmallIconDisabled" Enabled ="false" 
                                                                                    AlternateText="First Page"     Visible  ='<%# Container.StartRowIndex<=0 %>'/>
                                                                                <asp:ImageButton ID="btnPreviousDisabled" runat="server"  SkinID="sknPreviousPageSmallIconDisabled" Enabled ="false" 
                                                                                    AlternateText="Previous Page"  Visible ='<%# Container.StartRowIndex<=0 %>'/>
                                                                                    
                                                                                    
                                                                                    
                                                                                <asp:ImageButton ID="btnFirst" runat="server" CommandName="First" SkinID="sknFirstPageSmallIcon"
                                                                                    AlternateText="First Page" ToolTip="First Page"     Visible ='<%# Container.StartRowIndex>0 %>'/>
                                                                                <asp:ImageButton ID="btnPrevious" runat="server" CommandName="Previous" SkinID="sknPreviousPageSmallIcon"
                                                                                    AlternateText="Previous Page" ToolTip="Previous Page"   Visible ='<%# Container.StartRowIndex>0 %>'/>
                                                                            </div>
                                                                            <div class="Command"  >
                                                                            
                                                                           <b>   Page
                                                                            <asp:TextBox ID="txtSlider" CssClass ="pagerSlider" runat="server"  Font-Size ="12px" Height ="14px" EnableViewState ="true" Width ="25px"  Text='<%# 
                                                                                        Container.TotalRowCount > 0 
                                                                                            ?  Math.Ceiling(((double)(Container.StartRowIndex + Container.MaximumRows) / Container.MaximumRows)) 
                                                                                            : 0 
                                                                                        %>' AutoPostBack="true" OnTextChanged="CurrentPageChanged"   ReadOnly ='<%#   Container.StartRowIndex==0 && ( Math.Ceiling((double)(Container.StartRowIndex + Container.MaximumRows) ) >= Container.TotalRowCount) %>'  />
                                                                                          <asp:HiddenField ID="hdnMaxPageNumber" runat ="server"  Value =  '<%# Math.Ceiling((double)Container.TotalRowCount / Container.MaximumRows)%>' />
                                                                                   </b>of <b>
                                                                                        
                                                                                    <%# 
                                                                                    Math.Ceiling((double)Container.TotalRowCount / Container.MaximumRows)%>
                                                                        </div>
                                                                        
                                                                            <div class="Command" style =" padding-top :3px;" >
                                                                            
                                                                               <asp:ImageButton ID="btnNextDisabled" runat="server"  SkinID="sknNextPageSmallIconDisabled" Enabled ="false" 
                                                                                    AlternateText="Next Page"  Visible ='<%#  Math.Ceiling((double)(Container.StartRowIndex + Container.MaximumRows)  )>= Container.TotalRowCount%>'/>
                                                                                <asp:ImageButton ID="btnLastDisabled" runat="server"  SkinID="sknLastPageSmallIconDisabled" Enabled ="false" 
                                                                                    AlternateText="Last Page"  Visible ='<%# Math.Ceiling((double)(Container.StartRowIndex + Container.MaximumRows) ) >= Container.TotalRowCount %>' />
                                                                            
                                                                            
                                                                            
                                                                                <asp:ImageButton ID="btnNext" runat="server" CommandName="Next" SkinID="sknNextPageSmallIcon"
                                                                                    AlternateText="Next Page" ToolTip="Next Page" Visible ='<%#  Math.Ceiling((double)(Container.StartRowIndex + Container.MaximumRows)  )< Container.TotalRowCount%>'/>
                                                                                <asp:ImageButton ID="btnLast" runat="server" CommandName="Last" SkinID="sknLastPageSmallIcon"
                                                                                    AlternateText="Last Page" ToolTip="Last Page"   Visible ='<%# Math.Ceiling((double)(Container.StartRowIndex + Container.MaximumRows) ) < Container.TotalRowCount %>' />
                                                                            </div>
                                                                       <div style =" width :20%; float :right    ; text-align  :right  ;white-space :nowrap ; padding-right : 5px" >
                                                                  <div style ="display :none;">
                                                                   <%#  hdnTotalRowCount.Value  =Container.TotalRowCount.ToString () %>
                                                                    <%#  hdnStartRowIndex.Value = Container.StartRowIndex.ToString ()%>
                                                                    <%#  hdnMaxRow.Value = Container.MaximumRows.ToString ()%>
                                                                     
                                                                    
                                                                  </div>
                                                                              <b>
                                                                            <%# Container.TotalRowCount > 0 ? Math.Ceiling(((double)(Container.StartRowIndex ) ))+1  : 1 %>
                                                                            
                                                                        </b>-<b> <%#Container.TotalRowCount > (Math.Ceiling((double)(Container.StartRowIndex + Container.MaximumRows))) ? Math.Ceiling((double)(Container.StartRowIndex + Container.MaximumRows)) : Container.TotalRowCount%>
                                                                        </b>of <b>
                                                                            <%# Math.Ceiling((double)Container.TotalRowCount )%>
                                                                        </b> 
                                                                    </div>
                                                                    </div>
                                                                   
                                                                </div>   
                                                                 
                                                                </PagerTemplate>
                                                            </asp:TemplatePagerField>
                                                        </Fields>
                                                </asp:DataPager>
                                            </div>