using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Web.UI.Helper;
using TPS360.Web.UI;
using System.Collections;

public partial class VendorCandidates : BaseControl, IWidget
{
    private bool _IsMemberMailAccountAvailable = false;
    private bool isAccess = false;
    private bool _IsAccessToEmployee = true;
    private static string UrlForEmployee = string.Empty;
    private static int IdForSitemap = 0;
    private string currentSiteMapId = "0";
    public string  SelectedCandidateIds
    {
        get
        {
            return hdnSelectedIDS.Value;
        }
    }
    #region Properties

    private IWidgetHost _Host;

    public IWidgetHost Host
    {
        get { return _Host; }
        set { _Host = value; }
    }

    private string _userId;
    public string UserId
    {
        get { return _userId; }
        set { _userId = value; }
    }

    #endregion

    #region Events

    protected void Page_Load(object sender, EventArgs e)
    {
        currentSiteMapId = Helper.Url.SecureUrl[UrlConstants.PARAM_SITEMAP_PARENT_ID].ToString();
            
        CustomSiteMap CustomMap = new CustomSiteMap();
        CustomMap = Facade.GetAllCustomSiteMapByParentIdAndMemberPrivilege(358, CurrentMember.Id);
        if (CustomMap == null) _IsAccessToEmployee = false;
        else
        {

            IdForSitemap = CustomMap.Id;
            UrlForEmployee = "~/" + CustomMap.Url.ToString();
        }
        string s = hdnSelectedIDS.Value;
        _IsMemberMailAccountAvailable = MiscUtil.IsValidMailSetting(Facade, base.CurrentMember.Id);
        (this as IWidget).HideSettings();
        ArrayList AList = Facade.GetAllMemberPrivilegeIdsByMemberId(CurrentMember.Id);
        if (AList.Contains(50))
            isAccess = true;
        else
            isAccess = false ;
        odsCandidateList.SelectParameters["SortOrder"].DefaultValue = SortOrder.Text.ToString();
        if(IsUserVendor )
        odsCandidateList.SelectParameters["memberId"].DefaultValue = base.CurrentMember.Id.ToString();
        
        

       
        if (!IsPostBack)
        {
            GetWidgetData();
            txtSortColumn.Text = "btnName";
            txtSortOrder.Text = "ASC";
            PlaceUpDownArrow();
        }
        string pagesize = "";
        pagesize = (Request.Cookies["DashboardCandidateListRowPerPage"] == null ? "" : Request.Cookies["DashboardCandidateListRowPerPage"].Value); ;
        ASP.controls_pagercontrol_ascx  PagerControl = (ASP.controls_pagercontrol_ascx )this.lsvCandidateList .FindControl("pagerControl");
        if (PagerControl != null)
        {
            DataPager pager = (DataPager)PagerControl.FindControl("pager");
            if (pager != null) pager.PageSize = Convert.ToInt32(pagesize == "" ? "10" : pagesize);
        }
   
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        (this as IWidget).HideSettings();
    }

    #region ListView Events
    protected void lsvCandidateList_PreRender(object sender, EventArgs e)
    {
        lsvCandidateList.DataBind();
        ASP.controls_pagercontrol_ascx  PagerControl = (ASP.controls_pagercontrol_ascx )this.lsvCandidateList .FindControl("pagerControl");
        if (PagerControl != null)
        {
            DataPager pager = (DataPager)PagerControl.FindControl("pager");
            if (pager != null)
            {
                if (pager.Controls.Count >= 1)
                {
                    DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                    if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
                }
            }
            HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
            if (hdnRowPerPageName != null) hdnRowPerPageName.Value = "DashboardCandidateListRowPerPage";
        }
        PlaceUpDownArrow();


        System.Web.UI.HtmlControls.HtmlTable tblEmptyData = (System.Web.UI.HtmlControls.HtmlTable)lsvCandidateList.FindControl("tblEmptyData");
        if (lsvCandidateList.Controls.Count == 0 || tblEmptyData !=null)
        {
            lsvCandidateList.DataSource = null;
            lsvCandidateList.DataBind();
        }

    }
    protected void lsvCandidateList_ItemDataBound(object sender, ListViewItemEventArgs e)
    {

        if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
        {
            Candidate candidatInfo = ((ListViewDataItem)e.Item).DataItem as Candidate;

            if (candidatInfo != null)
            {
                HyperLink lnkCandidateName = (HyperLink)e.Item.FindControl("lnkCandidateName");
                LinkButton lblSource = (LinkButton)e.Item.FindControl("lblSource");
                Label lblCreatedDate = (Label)e.Item.FindControl("lblCreatedDate");
                lblCreatedDate.Text = candidatInfo.CreateDate.ToShortDateString();
                string strFullName = MiscUtil.GetFirstAndLastName(candidatInfo.FirstName, candidatInfo.LastName);
                if (IsUserVendor)
                    ControlHelper.SetHyperLink(lnkCandidateName, UrlConstants.Vendor.OVERVIEW, string.Empty, strFullName, UrlConstants.PARAM_MEMBER_ID, StringHelper.Convert(candidatInfo.Id), UrlConstants.PARAM_SITEMAP_ID, UrlConstants.Candidate.VENDOR_CANDIDATE_OVERVIEW_SITEMAP_ID, UrlConstants.PARAM_SITEMAP_PARENT_ID, "653");
                else
                {
                    if (_IsAccessToEmployee)
                    {
                        if (IdForSitemap > 0 && UrlForEmployee != string.Empty)
                            ControlHelper.SetHyperLink(lnkCandidateName, UrlForEmployee, string.Empty, candidatInfo .FirstName + " "  + candidatInfo.LastName , UrlConstants.PARAM_MEMBER_ID, StringHelper.Convert(candidatInfo.Id), UrlConstants.PARAM_SITEMAP_ID, IdForSitemap.ToString(), UrlConstants.PARAM_SITEMAP_PARENT_ID, currentSiteMapId);
                    }
                    else lnkCandidateName.Text = strFullName;
                }
                System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
                doc.LoadXml(candidatInfo.CreatorName);
                System.Xml.XmlNodeList list = doc.SelectNodes("Creator");
                if (list.Count == 1)
                {
                    //string s = list[0]["Name"].InnerText;
                    //string s1 = list[0]["ContactID"].InnerText;
                    //string s2 = list[0]["MemberID"].InnerText;

                    SecureUrl Modalurl = UrlHelper.BuildSecureUrl("../Modals/CompanyContact.aspx", string.Empty, UrlConstants.PARAM_CONTACT_ID, list[0]["ContactID"].InnerText);
                    lblSource.OnClientClick = "javascript:EditModal('" + Modalurl.ToString() + "','675px','450px'); return false;";
                    lblSource.Text = list[0]["Name"].InnerText;
                }
            }
        }
    }
    protected void lsvCandidateList_ItemCommand(object sender, ListViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Sort")
            {
                LinkButton lnkbutton = (LinkButton)e.CommandSource;
                if (txtSortColumn.Text == lnkbutton.ID)
                {
                    if (txtSortOrder.Text == "ASC") txtSortOrder.Text = "DESC";
                    else  txtSortOrder.Text = "ASC";
                }
                else
                {
                    txtSortColumn.Text = lnkbutton.ID;
                    txtSortOrder.Text = "ASC";
                }
                

                if (SortColumn.Text == string.Empty || SortColumn.Text != e.CommandArgument.ToString())
                    SortOrder.Text = "asc";
                else
                    SortOrder.Text = SortOrder.Text == "asc" ? "desc" : "asc";
                SortColumn.Text = e.CommandArgument.ToString();
                odsCandidateList.SelectParameters["SortOrder"].DefaultValue = SortOrder.Text.ToString();
            }
        }
        catch
        {
        }
    }
    #endregion

  

    #endregion

    #region Methods
    private void PlaceUpDownArrow()
    {
        try
        {
            LinkButton lnk = (LinkButton)lsvCandidateList.FindControl(txtSortColumn.Text);
            System.Web.UI.HtmlControls.HtmlTableCell im = (System.Web.UI.HtmlControls.HtmlTableCell)lnk.Parent;
            im.EnableViewState = false;
            im.Attributes.Add("class", (txtSortOrder.Text == "ASC" ? "Ascending" : "Descending"));
        }
        catch
        {
        }

    }
    public void GetWidgetData()
    {
        if (SortOrder.Text == string.Empty && SortColumn.Text == string.Empty)
        {
            SortOrder.Text = "asc";
            SortColumn.Text = "[C].[FirstName]";
        }
        odsCandidateList.SelectParameters["SortOrder"].DefaultValue = SortOrder.Text.ToString();
        odsCandidateList.SelectParameters["memberId"].DefaultValue = base.CurrentMember.Id.ToString();
        odsCandidateList.SelectParameters["IsVendorContact"].DefaultValue = IsUserVendor.ToString();
        odsCandidateList.SelectParameters["DateFrom"].DefaultValue = DateTime.MinValue.ToShortDateString();
        odsCandidateList.SelectParameters["DateTo"].DefaultValue = DateTime.MinValue.ToShortDateString();
        if (Request.Url.ToString().ToLower().Contains("sfa/vendorcandidates.aspx"))
        {
            string ID = Helper.Url.SecureUrl[UrlConstants.PARAM_COMPANY_ID];
            odsCandidateList.SelectParameters["memberId"].DefaultValue = ID;

            odsCandidateList.SelectParameters["IsVendor"].DefaultValue = "true";
        }
        else odsCandidateList.SelectParameters["IsVendor"].DefaultValue = "false";
        this.lsvCandidateList.DataBind();
    }

    #endregion

    #region Implementation

    void IWidget.Init(IWidgetHost host)
    {
        this.Host = host;
    }

    void IWidget.ShowSettings()
    {
        pnlSettings.Visible = true;
    }
    void IWidget.HideSettings()
    {
        pnlSettings.Visible = false;
    }
    void IWidget.Minimized()
    {
    }
    void IWidget.Maximized()
    {
        if (lsvCandidateList != null)
        {
            if (lsvCandidateList.Items.Count == 0)
            {
                lsvCandidateList.DataSource = null;
                lsvCandidateList.DataBind();
            }
            else GetWidgetData();
        }
    }
    void IWidget.Closed()
    {
    }

    #endregion
   
}
