﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName:RemarksAndInternalRatingEditor.ascx.cs
    Description: This is the user control page used to update Internal rating of an Employee.
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1             Nov-30-2008            Rajendra          Defect id: 11911;Added code in Page_Load Event.
 *  0.2             Dec-3-2009             Sandeesh          Defect id:11510 :If a candidate's internal rating is undefined, the default should be "None" instead of the current "-1".  
 *  0.3             Jan-4-2010            Basavaraj Angadi   Defect id:11993 ,Changes were made for Internal Rating for Employee,
------------------------------------------------------------------------------------------------------------------------------       

>*/


using System;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using System.Web.UI.WebControls;
using TPS360.Web.UI;
using System.Web.UI;
using System.Text.RegularExpressions;
namespace TPS360.Web.UI
{
    public partial class ControlRemarksAndInternalRatingEditor : BaseControl
    {
        #region Member Variables

        private int _memberId = 0;
        MemberExtendedInformation _memberExtendedInfo=null;
        private static string _memberrole = string.Empty;

        #endregion

        #region Properties

        public string MemberRole
        {
            set { _memberrole = value; }
            get { return _memberrole; }
        }

        private MemberExtendedInformation CurrentMemberExInfo
        {
            get
            {
                if (_memberExtendedInfo == null)
                {
                    if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]))
                    {
                        _memberId = Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]);
                    }

                    if (_memberId > 0)
                    {
                        _memberExtendedInfo = Facade.GetMemberExtendedInformationByMemberId(_memberId);
                    }
                    else
                    {
                        _memberExtendedInfo = Facade.GetMemberExtendedInformationByMemberId(base.CurrentMember.Id);
                    }

                    if (_memberExtendedInfo == null)
                    {
                        _memberExtendedInfo = new MemberExtendedInformation();
                    }
                }

                return _memberExtendedInfo;
            }
        }

        #endregion

        #region Methods

        private void SaveMemberExInfo()
        {

            string imgurl="";
            string msg;
            if (IsValid)
            {
                try
                {
                    MemberExtendedInformation memberExInfo = BuildMemberExInfo();

                    if (memberExInfo.IsNew)
                    {
                        Facade.AddMemberExtendedInformation(memberExInfo);
                        msg = "Internal rating has been saved successfully.";
                       // MiscUtil.ShowMessage(lblMessage, msg, false);
                                             
                    }
                    else
                    {
                        Facade.UpdateMemberExtendedInformation(memberExInfo);
                        msg = "Internal rating has been updated successfully.";
                       // MiscUtil.ShowMessage(lblMessage, msg, false);
                    }
                    GenericLookup rating = Facade.GetGenericLookupById(memberExInfo.InternalRatingLookupId);
                    if (rating != null && !string.IsNullOrEmpty(rating.Name))
                    {
                        imgurl = MiscUtil.GetInternalRatingImage(rating.Name).Replace ("~","..");
                    }
                    else imgurl = MiscUtil.GetInternalRatingImage("0").Replace("~", ".."); 
                    ScriptManager.RegisterClientScriptBlock(this, typeof(MiscUtil), "internalrating", ";parent.changeRating('" + imgurl + "');parent.ShowStatusMessage('" + msg + "',false);", true);
                }
                catch (ArgumentException ex)
                {
                    MiscUtil.ShowMessage(lblMessage, ex.Message, true);
                }
            }
        }
       
        private MemberExtendedInformation BuildMemberExInfo()
        {
            MemberExtendedInformation memberExInfo = CurrentMemberExInfo;

            memberExInfo.InternalRatingLookupId = Convert.ToInt32(ddlInternalRatings.SelectedValue);

            if (_memberId > 0)
            {
                memberExInfo.MemberId = _memberId;
            }
            memberExInfo.UpdatorId = base.CurrentMember.Id;

            return memberExInfo;
        }

        private void PrepareView()
        {
            if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]))
            {
                _memberId = Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]);

            }
            else
            {
                _memberId = Convert.ToInt32(base.CurrentMember.Id.ToString());
            }
            MiscUtil.PopulateInternalRating(ddlInternalRatings, Facade);
            imgRatringLowest.ImageUrl = MiscUtil.GetInternalRatingImage("1"); 
            imgRatingHeighest.ImageUrl = MiscUtil.GetInternalRatingImage("5");

        }

        private void PrepareEditView()
        {
            MemberExtendedInformation memberExInfo = CurrentMemberExInfo;

            if (_memberId > 0)
            {
                //ddlInternalRatings.SelectedValue = Convert.ToString(memberExInfo.InternalRatingLookupId);
                TPS360.Web.UI.Helper.ControlHelper.SelectListByValue (ddlInternalRatings, Convert.ToString(memberExInfo.InternalRatingLookupId));
            }
        }        

        #endregion

        #region Events

        #region Page event

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMessage.Text = lblMessage.CssClass = ""; 
            if (!IsPostBack)
            {
                PrepareView();
                ddlInternalRatings.SelectedIndex = 0; 
                PrepareEditView();
                if (Session["msg"] != null)
                {
                    MiscUtil.ShowMessage(lblMessage, Session["msg"].ToString(), false);
                    Session.Remove("msg");
                }
            }
           
        }
        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
               // PrepareView();
               // ddlInternalRatings.SelectedIndex = 0;
                PrepareEditView();
               
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            SaveMemberExInfo();


            
        }

        #endregion

        #endregion
    }
}
