﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: MemberChangePassword.ascx.cs
    Description: This is the user control page used to change password.
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1               Jun-03-2009          Rajendra A.S        Defect id: 10546; Error message has been changed in SavePassword().
-------------------------------------------------------------------------------------------------------------------------------------------       
*/

using System;
using System.Web.Security;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;

namespace TPS360.Web.UI
{
    public partial class ControlMemberChangePassword : BaseControl
    {
        #region Member Variables

        private int _memberId = 0;
        private static string _memberrole = string.Empty;

        #endregion

        #region Properties

        public Int32 MemberId
        {
            set { _memberId = value; }
        }
        
        public string MemberRole
        {
            set { _memberrole = value; }
            get { return _memberrole; }
        }

        #endregion

        #region Methods


        public void HideSaveButton()
        {
            btnSubmit.Visible = false;
        }

        public void ResetPassword()
        {
            SavePassword();
        }
        private void GetMemberId()
        {
            //From Member Profile
            if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]))
            {
                hfMemberId.Value = Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID];

            }
            //From Member Login
            else
            {
                hfMemberId.Value = base.CurrentMember.Id.ToString();
            }
            _memberId = Int32.Parse(hfMemberId.Value);
        }

        private void SavePassword()
        {
            try
            {
                Member member = Facade.GetMemberById(_memberId);
                MembershipUser user = Membership.GetUser(member.UserId);
                string strOldPassword = user.GetPassword();
                if (strOldPassword == txtOldPassword.Text)
                {
                    user.ChangePassword(strOldPassword, txtConfirmPassword.Text);
                    Membership.UpdateUser(user);
                    txtOldPassword.Text = String.Empty;
                    txtNewPassword.Text = String.Empty;
                    txtConfirmPassword.Text = String.Empty;
                    //MiscUtil.AddActivity(_memberrole,_memberId,base.CurrentMember.Id,ActivityType.ChangePassword ,Facade);
                    MiscUtil.ShowMessage(lblMessage, "New password has been saved successfully", false); //0.1
                    //System.Web.UI.ScriptManager.RegisterStartupScript(lblConfirmPassword, typeof(MiscUtil), "close", "parent.CloseAndShowMessage('New password has been saved successfully',false);", true);
                }
                else
                {
                    MiscUtil.ShowMessage(lblMessage, "Invalid old password", true);
                }
            }
            catch (ArgumentException ex)
            {
                MiscUtil.ShowMessage(lblMessage, ex.Message, true);
            }
        }

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            _memberId = Int32.Parse(hfMemberId.Value);
            if (!IsPostBack)
            {
                if (IsUserVendor) divHeader.Visible = false;
                GetMemberId();
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            SavePassword();
            
        }

        #endregion
    }
}