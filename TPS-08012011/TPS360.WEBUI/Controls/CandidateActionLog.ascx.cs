﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using TPS360.Common.BusinessEntities;
using TPS360.Web.UI.Helper;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Web.UI;
using System.Text.RegularExpressions;
using ExpertPdf.HtmlToPdf;
using System.Drawing;
using System.IO;

namespace TPS360.Web.UI
{
    public partial class CandidateActionLog : ATSBaseControl   
    {
       
        #region Member Variables
    
        #endregion

        #region Properties

        private int _jobPostingID;
        public int JobPostingID
        {
            get
            {
                return Convert.ToInt32(hfJobPostingId.Value);
            }
            set
            {
                hfJobPostingId.Value = value.ToString();
                _jobPostingID = value;
            }
        }
        public string _MemberId = string.Empty;
        public string MemberID
        {
            get
            {
                return hfMemberId.Value == string.Empty ? "0" : hfMemberId.Value;
            }
            set
            {
                lsvHiringLog.DataSourceID = "odsEventLog";
                hfMemberId.Value = value;
                _MemberId = value;
                if (JobPostingID >0)
                    odsEventLog.SelectParameters["JobPostingId"].DefaultValue = JobPostingID .ToString ();
                else odsEventLog.SelectParameters["JobPostingId"].DefaultValue = "0";
                odsEventLog.SelectParameters["CandidateId"].DefaultValue = MemberID ;


               lsvHiringLog .DataBind();
            }
        }
        public string  getModalTitle()
        {
            return Facade.GetJobPostingById(JobPostingID).JobTitle + " - Hiring Log";
        }
        public string  getModalTitleWithCandidateName()
        {
            return  "Hiring Log - " + MiscUtil.GetMemberNameById(Convert.ToInt32(_MemberId), Facade);
        }

        public void Bind()
        {
            lsvHiringLog.DataSourceID = "odsEventLog";
            //if (this.Page is RequisitionBasePage)
            //    lblCandidateName.Text = "Hiring Log - " + MiscUtil.GetMemberNameById(Convert.ToInt32(_MemberId), Facade);
            //else lblCandidateName.Text = "";
            if (this.Page is RequisitionBasePage || Page.Request.Url.AbsoluteUri.ToString().Contains("HiringMatrix.aspx")) odsEventLog.SelectParameters["JobPostingId"].DefaultValue = JobPostingID .ToString ();
            else odsEventLog.SelectParameters["JobPostingId"].DefaultValue = "0";
            odsEventLog.SelectParameters["CandidateId"].DefaultValue = MemberID;
           lsvHiringLog.DataBind();
        }
        public int _EmployeeId = 0;
        public int EmployeeId
        {
            get
            {
                return hfCreatorId.Value == string.Empty ? 0 : Convert.ToInt32(hfCreatorId.Value);
            }
            set
            {
                hfCreatorId.Value = value.ToString ();
                _EmployeeId = value;
                odsEventLog.SelectParameters["JobPostingId"].DefaultValue = "0";
                odsEventLog.SelectParameters["CandidateId"].DefaultValue = "0";
                odsEventLog.SelectParameters["CreatorId"].DefaultValue = value.ToString();
                lsvHiringLog.DataSourceID = "odsEventLog";
                lsvHiringLog.DataBind();
            }
        }
        #endregion

        #region Methods

        private void Prepareview()
        {
        }
        private void PrepareEditView()
        {
        }

        #endregion

        #region PageEvents
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!IsPostBack)
            {
                if (Request.Url.ToString().ToLower().Contains("sfa"))
                {
                    odsEventLog.SelectParameters["JobPostingId"].DefaultValue = "0";
                    odsEventLog.SelectParameters["CandidateId"].DefaultValue = "0";
                    odsEventLog.SelectParameters["ClientId"].DefaultValue = Helper.Url.SecureUrl[UrlConstants.PARAM_COMPANY_ID].ToString(); ;
                }
                hdnSortColumn.Value = "btnDate";
                hdnSortOrder.Value = "DESC";
                Label lbModalTitle = (Label)this.Page.Master.FindControl("lbModalTitle");
                if (MemberID != null && MemberID != "" && MemberID !="0")
                {
                    string canname = MiscUtil.GetMemberNameById(Convert.ToInt32 ( MemberID),Facade );
                    if (lbModalTitle != null) lbModalTitle.Text = "Hiring Log - " +  canname ;
                }
                else
                {
                    if (JobPostingID > 0)
                    {
                        JobPosting job = Facade.GetJobPostingById(JobPostingID);
                        if (lbModalTitle != null) lbModalTitle.Text = "Hiring Log - " + job.JobTitle;
                    }
                }
            }
            string Cookiename = "";
            if (Page.Request.Url.AbsoluteUri.ToString().Contains("HiringMatrix.aspx"))
            {
                if (Convert.ToInt32(MemberID) > 0)
                    Cookiename = "HiringMatrixHiringLogRowPerPage";
                else
                    Cookiename = "StatusNotesHiringLogRowPerPage";
            }
            else
            {
                if (EmployeeId > 0)
                    Cookiename = "EmployeeHiringLogRowPerPage";
                else
                    Cookiename = "CandidateHiringLogRowPerPage";
            }

            string pagesize = "";
            pagesize = (Request.Cookies[Cookiename] == null ? "" : Request.Cookies[Cookiename].Value); ;
           // ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)lsvHiringLog.FindControl("pagerControl");
           // if (PagerControl != null)
            {
                DataPager pager = (DataPager)lsvHiringLog.FindControl("pager");
                if (pager != null) pager.PageSize = Convert.ToInt32(pagesize == "" ? "10" : pagesize);
            }



        }

        private void PlaceUpDownArrow()
        {
            try
            {
                LinkButton lnk = (LinkButton)lsvHiringLog.FindControl(hdnSortColumn.Value);
                HtmlTableCell im = (HtmlTableCell)lnk.Parent;
                im.EnableViewState = false;
                im.Attributes.Add("class", (hdnSortOrder.Value == "ASC" ? "Ascending" : "Descending"));
            }
            catch
            {
            }

        }
       
        #endregion
        protected void lsvHiringLog_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                EventLogForRequisitionAndCandidate log = ((ListViewDataItem)e.Item).DataItem as EventLogForRequisitionAndCandidate;
                if (log != null)
                {
                    Label lblDate = (Label)e.Item.FindControl("lblDate");
                    Label lblUser = (Label)e.Item.FindControl("lblUser");
                    Label lblAction = (Label)e.Item.FindControl("lblAction");
                    Label lblrequisition = (Label)e.Item.FindControl("lblRequisition");
                    lblDate.Text = log.ActionDate.ToShortDateString()+" "+log.ActionDate.ToShortTimeString();
                    //if (this.Page is RequisitionBasePage || Page.Request.Url.AbsoluteUri.ToString().Contains("HiringMatrix.aspx"))
                    lblUser.Text = log.UserName;
                    // else 
                    lblrequisition.Text = log.JobTitle;
                    
                    lblAction.Text = log.ActionType;
                    if (this.Page is RequisitionBasePage || Page.Request.Url.AbsoluteUri.ToString().Contains("HiringMatrix.aspx"))
                    {
                        lblrequisition.Parent.Visible = false;
                    }
                    if ( Page.Request.Url.AbsoluteUri.ToString().ToLower().Contains("employee"))
                    {
                        lblUser.Parent.Visible = false;
                    }
                }
            }
        }
        protected void lsvHiringLog_PreRender(object sender, EventArgs e)
        {            
            if (lsvHiringLog != null)
            {
                LinkButton btnUser = (LinkButton)lsvHiringLog.FindControl("btnUser");
                HtmlTableCell tdpager = (HtmlTableCell)lsvHiringLog.FindControl("tdPager");
                if (this.Page is RequisitionBasePage == false && !Page.Request.Url.AbsoluteUri.ToString().Contains("HiringMatrix.aspx"))
                {
                    
                    if (btnUser != null)
                    {

                      /*  btnUser.CommandArgument = "[J].[JobTitle]";
                        btnUser.Text = "Requisition";*/
                        btnExport.Visible = true;
                    }
                    else btnExport.Visible = false;
                }
                else
                {
                    LinkButton btnRequisition = (LinkButton)lsvHiringLog.FindControl("btnRequisition");
                    if (btnRequisition != null)
                    {
                        btnRequisition.Parent.Visible = false;
                        btnExport.Visible = true;
                        
                        tdpager.ColSpan = 3;
                    }
                    else btnExport.Visible = false;
                }
                if (Page.Request.Url.AbsoluteUri.ToString().ToLower().Contains("employee"))
                {
                    if (btnUser!=null )
                    btnUser.Parent.Visible = false;
                    if (tdpager!=null )
                    tdpager.ColSpan = 3;                   
                }

            }

            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvHiringLog.FindControl("pagerControl");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)PagerControl.FindControl("pager");
                if (pager != null)
                {
                    DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                    if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
                }
                if (hdnRowPerPageName != null)
                {
                    if (Page.Request.Url.AbsoluteUri.ToString().Contains("HiringMatrix.aspx"))
                    {
                        if (Convert.ToInt32(MemberID) > 0)
                            hdnRowPerPageName.Value = "HiringMatrixHiringLogRowPerPage";
                        else
                            hdnRowPerPageName.Value = "StatusNotesHiringLogRowPerPage";
                    }
                    else
                    {
                        if (EmployeeId > 0)
                            hdnRowPerPageName.Value = "EmployeeHiringLogRowPerPage";
                        else
                            hdnRowPerPageName.Value = "CandidateHiringLogRowPerPage";
                    }
                }
            }

            PlaceUpDownArrow();

        }
        protected void lsvHiringLog_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Sort")
                {
                    LinkButton lnkbutton = (LinkButton)e.CommandSource;
                    if (hdnSortColumn.Value == lnkbutton.ID)
                    {
                        
                        if (hdnSortOrder.Value == "DESC") hdnSortOrder.Value = "ASC";
                        else if(hdnSortOrder .Value =="ASC") hdnSortOrder.Value = "DESC";
                    }
                    else
                    {
                        hdnSortColumn.Value = lnkbutton.ID;
                        hdnSortOrder.Value = "ASC";
                    }
                    try
                    {
                        if (Page.Request.Url.AbsoluteUri.ToString().Contains("HiringMatrix.aspx") )
                        {
                            AjaxControlToolkit.ModalPopupExtender ext = (AjaxControlToolkit.ModalPopupExtender)this.Page.Master.FindControl("cphHomeMaster").FindControl("ucRequisitionHiringMatrixNavigationTopMenu").FindControl("mpeActionLog");
                            ext.Enabled = true;
                            ext.Show();
                        }
                    }
                    catch
                    {
                    }
                }
            }
            catch
            {
            }
        }

     
        #region Button Events

        protected void btnExport_Click(object sender, EventArgs args)
        {
            string filename = "";
            if (JobPostingID >0)
            {
                if (Convert.ToInt32(MemberID) > 0)
                    filename = "CandidateHiringLog-" + Facade.GetJobPostingById(JobPostingID).JobPostingCode + "-" + MiscUtil.GetMemberNameById(Convert.ToInt32(MemberID), Facade).Replace(" ", "") + "-" + DateTime.Now.ToString("dd-MM-yyyy") + ".xls";
                else
                    filename = "ReqHiringLog-" + Facade .GetJobPostingById (JobPostingID ).JobPostingCode  + "-" + DateTime.Now.ToString("dd-MM-yyyy") + ".xls";
            }
            else
            {
                if (EmployeeId > 0)
                {
                    filename = "UserHiringLog-" + MiscUtil.GetMemberNameById(EmployeeId, Facade).Replace(" ", "") + "-" + DateTime.Now.ToString("dd-MM-yyyy") + ".xls";
                }
                else
                    filename = "CandidateHiringLog-" + MiscUtil.GetMemberNameById(Convert.ToInt32(MemberID), Facade).Replace(" ", "") + "-" + DateTime.Now.ToString("dd-MM-yyyy") + ".xls";
            }
            if (Request.Url.ToString().ToLower().Contains("sfa"))
            {
                if (SiteSetting[DefaultSiteSetting.ApplicationEdition.ToString()].ToString() == UIConstants.INTERNAL_HIRING)
                {
                    filename = "BUHiringLog-" + Facade.GetCompanyById(Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_COMPANY_ID].ToString())).CompanyName.Replace(" ", "") + "-" + DateTime.Now.ToString("dd-MM-yyyy") + ".xls";
                }
                else
                {
                    filename = "AccountHiringLog-" + Facade.GetCompanyById(Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_COMPANY_ID].ToString())).CompanyName.Replace(" ", "") + "-" + DateTime.Now.ToString("dd-MM-yyyy") + ".xls";
                }
            }
            Response.AddHeader("content-disposition", "attachment;filename=" + filename);
            Response.ContentEncoding = System.Text.Encoding.ASCII;
            Response.ContentType = "application/msexcel";
            Response.Output.Write(MiscUtil.getEventLogReport(((JobPostingID > 0) ? JobPostingID : 0), Convert.ToInt32(MemberID), EmployeeId, (Request.Url.ToString().ToLower().Contains("employee") ? false : true), (JobPostingID > 0 ? false : true), Facade)); 
            Response.Flush();
            Response.End();
        }

        protected void PagerCommand(object sender, DataPagerCommandEventArgs e)
        {

            DataPager pager = e.Item.Pager;
            int newSartRowIndex;

            switch (e.CommandName)
            {
                case "Next":
                    {
                        newSartRowIndex = pager.StartRowIndex + pager.MaximumRows >= pager.TotalRowCount ? pager.StartRowIndex : pager.StartRowIndex + pager.MaximumRows;
                        break;
                    }
                case "Previous":
                    {
                        newSartRowIndex = pager.StartRowIndex - pager.MaximumRows < 0 ? pager.StartRowIndex : pager.StartRowIndex - pager.MaximumRows;
                        break;
                    }
                case "Last":
                    {
                        newSartRowIndex = ((pager.TotalRowCount / pager.MaximumRows) * pager.MaximumRows) >= pager.TotalRowCount ? (((pager.TotalRowCount / pager.MaximumRows) - 1) * (pager.MaximumRows)) : ((pager.TotalRowCount / pager.MaximumRows) * pager.MaximumRows);
                        break;
                    }
                case "First":
                    {
                        newSartRowIndex = 0;
                        break;
                    }
                default:
                    {
                        newSartRowIndex = 0;
                        break;
                    }
            }

            e.NewMaximumRows = e.Item.Pager.MaximumRows;
            e.NewStartRowIndex = newSartRowIndex;

            if (Page.Request.Url.AbsoluteUri.ToString().Contains("HiringMatrix.aspx") )
            {
                AjaxControlToolkit.ModalPopupExtender ext = (AjaxControlToolkit.ModalPopupExtender)this.Page.Master.FindControl("cphHomeMaster").FindControl("ucRequisitionHiringMatrixNavigationTopMenu").FindControl("mpeActionLog");
                ext.Enabled = true;
                ext.Show();
            }
        }

        protected void CurrentPageChanged(object sender, EventArgs e)
        {

            DataPager pager = (DataPager)lsvHiringLog.FindControl("pager");
            if (Page.Request.Url.AbsoluteUri.ToString().Contains("HiringMatrix.aspx") && Convert.ToInt32(MemberID) > 0)
            {
                AjaxControlToolkit.ModalPopupExtender ext = (AjaxControlToolkit.ModalPopupExtender)this.Page.Master.FindControl("cphHomeMaster").FindControl("ucRequisitionHiringMatrixNavigationTopMenu").FindControl("mpeActionLog");
                ext.Enabled = true;
                ext.Show();
            }

            TextBox txtCurrentPage = sender as TextBox;
            int selectedpage = 0;
            Int32.TryParse(txtCurrentPage.Text, out selectedpage);

            if (selectedpage > 0 && Convert.ToDouble((pager.TotalRowCount) / Convert.ToDouble(pager.PageSize)) > selectedpage - 1)
            {
                int startRowIndex = (int.Parse(txtCurrentPage.Text) - 1) * pager.MaximumRows;
                pager.SetPageProperties(startRowIndex, pager.MaximumRows, true);

            }
            else
                MiscUtil.ShowMessage(lblMessage, "Please enter valid page number", true);


            if (Page.Request.Url.AbsoluteUri.ToString().Contains("HiringMatrix.aspx") )
            {
                AjaxControlToolkit.ModalPopupExtender ext = (AjaxControlToolkit.ModalPopupExtender)this.Page.Master.FindControl("cphHomeMaster").FindControl("ucRequisitionHiringMatrixNavigationTopMenu").FindControl("mpeActionLog");
                ext.Enabled = true;
                ext.Show();
            }
        }

        protected void ddlRowPerPage_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataPager pager = (DataPager)lsvHiringLog.FindControl("pager");
            TextBox txtCurrentPage = sender as TextBox;
            DropDownList ddlRowPerPage = sender as DropDownList;
            int startRowIndex = 0;
            pager.PageSize = Convert.ToInt32(ddlRowPerPage.SelectedValue);
            Response.Cookies[hdnRowPerPageName.Value].Value = pager.PageSize.ToString();
            pager.SetPageProperties(startRowIndex, Convert.ToInt32(ddlRowPerPage.SelectedValue), true);
            if (Page.Request.Url.AbsoluteUri.ToString().Contains("HiringMatrix.aspx") )
            {
                AjaxControlToolkit.ModalPopupExtender ext = (AjaxControlToolkit.ModalPopupExtender)this.Page.Master.FindControl("cphHomeMaster").FindControl("ucRequisitionHiringMatrixNavigationTopMenu").FindControl("mpeActionLog");
                ext.Enabled = true;
                ext.Show();
            }
        }
        #endregion
    }
}
