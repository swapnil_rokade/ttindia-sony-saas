﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using TPS360.Web.UI;

public partial class MultipleSelectItemPicker : BaseControl
{
    public ListBox ListItem
    {
        get
        {
            return ddlItem;
        }
    }

    public string SelectedItems
    {
        get
        {
            //string value = "";
            //foreach (ListItem item in ddlItem.Items)
            //{
            //    if (item.Selected)
            //    {
            //        if (value != string.Empty) value += ",";
            //        value += item.Value;
            //    }
            //}

            //return value;
            return hdnSelectedListItems.Value;
        }
        set
        {
            char[] delim = { ',' };
            string[] list = value.Split(delim, StringSplitOptions.RemoveEmptyEntries);
            foreach (ListItem item in ddlItem.Items)
            {
                if (list.Contains(item.Value.Trim()))
                    item.Selected = true;

            }
        }
    }
    private bool _Enabled = true;
    public bool Enabled
    {
        set
        {
            _Enabled = value;
            ddlItem.Enabled = value;// ddlCountry.Enabled = value;

        }
    }

    public object DataSource
    {
        set
        {
            ddlItem.DataSource = value;
        }
    }
    public void DataBind()
    {
        ddlItem.DataBind();
    }
    public string DataValueField
    {
        set
        {
            ddlItem.DataValueField = value;
        }
    }

    public string DataTextField
    {
        set
        {
            ddlItem.DataTextField = value;
        }
    }


    private string _FirstOption;
    public string FirstOption
    {
        set
        {
            _FirstOption = value;

        }
        get { return _FirstOption; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
    }

}
