﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using TPS360.Web.UI.Helper;
using TPS360.Common.BusinessEntities;
namespace TPS360.Web.UI
{
    public partial class ControlsInterviewFeedback : BaseControl
    {
        private static int _memberId = 0;

        private bool IsNew = false;
        public int MemberId
        {
            set { _memberId = value; }
        }
        public int InterviewId 
        {
            get 
            {
                int _interviewId = 0;
                _interviewId = Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_INTERVIEWID].ToString());
                return _interviewId;
            }
        }
        public string InterviewTitle
        {
            get
            {
                string _interviewTitle = "";
                _interviewTitle = Helper.Url.SecureUrl[UrlConstants.PARAM_INTERVIEWTITLE].ToString();
                return _interviewTitle;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {

            
            
            if (!IsPostBack) 
            {
                PrepareView();
                
            }
                
           
        }
        #region Listview Events

        protected void lsvFeedback_ItemDataBound(object sender, ListViewItemEventArgs e) 
        {
            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                InterviewFeedback feedback = ((ListViewDataItem)e.Item).DataItem as InterviewFeedback;

                if (feedback != null) 
                {
                    Label lblCreatedDate = (Label)e.Item.FindControl("lblCreatedDate");
                    Label lblRound = (Label)e.Item.FindControl("lblRound");
                    Label lblFeedback = (Label)e.Item.FindControl("lblFeedback");
                    Label lblCreatedBy = (Label)e.Item.FindControl("lblCreatedBy");
                    Label lblTitle = (Label)e.Item.FindControl("lblTitle");
                    ImageButton btnEdit = (ImageButton)e.Item.FindControl("btnEdit");

                    ImageButton btnDelete = (ImageButton)e.Item.FindControl("btnDelete");

                    lblCreatedBy.Text = feedback.SubmittedBy;
                    lblCreatedDate.Text = feedback.CreateDate.ToShortDateString();
                    lblFeedback.Text = feedback.Feedback;
                    if(feedback.InterviewRound!=0)
                    lblRound.Text = Facade.GetGenericLookupById(feedback.InterviewRound).Name;
                    lblTitle.Text = feedback.Title;
                    btnDelete.OnClientClick = "return ConfirmDelete('Feedback')";
                    btnDelete.CommandArgument = btnEdit.CommandArgument = Convert.ToString(feedback.Id);
                }

            }
        }
        protected void lsvFeedback_ItemCommand(object sender, ListViewCommandEventArgs e) 
        { 
            int id;

            int.TryParse(e.CommandArgument.ToString(), out id);

                if (id > 0)
                {
                    if (string.Equals(e.CommandName, "EditItem"))
                    {
                        IsNew = false;

                        InterviewFeedback feedback = Facade.InterviewFeedback_GetById(id);

                        txtTitle.Text = feedback.Title;

                        txtNotes.Text = feedback.Feedback;

                        ControlHelper.SelectListByValue(ddlInterviewRounds, feedback.InterviewRound.ToString());
                        hdnInterviewFeebackId.Value = feedback.Id.ToString();
                    }
                    if (string.Equals(e.CommandName, "DeleteItem"))
                    {
                        Facade.InterviewFeedback_DeleteById(id);

                        MiscUtil.ShowMessage(lblMessage, "The Interview Feedback have been deleted Sucessfully", false);

                        PopulateFeedback();
                        ClearControls();
                        
                    }

                    
                }
        }
        protected void lsv_Feedback_OnRender(object sender, EventArgs e) 
        { 
        
        }
        #endregion
        protected void btnSave_Click(object sender, EventArgs e) 
        {
            InterviewFeedback feedback = BuildInterviewfeedback();
            string msg = "";
            if (hdnInterviewFeebackId.Value != "" && hdnInterviewFeebackId.Value != null && hdnInterviewFeebackId.Value != "0")
            {

                feedback.Id = Convert.ToInt32(hdnInterviewFeebackId.Value);

                Facade.InterviewFeedback_Update(feedback);

                msg = "Updated";
            }
            else
            {
                Facade.InterviewFeedback_Add(feedback);

                msg = "Added";
            }

            MiscUtil.ShowMessage(lblMessage, "The Interview Feedback have been " + msg + " Sucessfully", false);
            PopulateFeedback();
            ClearControls();
        }

        public InterviewFeedback BuildInterviewfeedback() 
        {
            InterviewFeedback feedback = new InterviewFeedback();

            feedback.Title = txtTitle.Text.Trim();
            feedback.Feedback = txtNotes.Text.ToString().Trim();
            feedback.InterviewRound = Convert.ToInt32(ddlInterviewRounds.SelectedValue);
            feedback.CreatorId = CurrentMember.Id;
            feedback.UpdatorId = CurrentMember.Id;
            feedback.InterviewId = InterviewId;

            return feedback;
        }
        public void ClearControls() 
        {
            txtNotes.Text = "";
            txtTitle.Text = "";
            hdnInterviewFeebackId.Value = "";
            MiscUtil.PopulateInterviewRounds(ddlInterviewRounds, Facade);
            //IsNew = true;
        }

        public void PopulateFeedback() 
        {
            lsvFeedback.DataSource = Facade.InterviewFeedback_GetByInterviewId(InterviewId);
            lsvFeedback.DataBind();
        }

        private void PrepareView()
        {
            PopulateFeedback();
            MiscUtil.PopulateInterviewRounds(ddlInterviewRounds, Facade);
        }
        //protected void lsvInterviewFeedback_ItemDataBound(object sender, ListViewItemEventArgs e)
        //{ }
    }
}


