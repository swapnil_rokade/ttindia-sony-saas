﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: RequisitionPreviewPublish.ascx.cs
    Description: This is the user control page used for requisition preview and publish.
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author               Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1            Feb-04-2016             Sakthi	        Changes made in PublishRequisition()
 *  0.2           2/Fab/2016              pravin khot       remove comment by-1.(chkCandidatePortal .Visible..........),2.(chkCandidatePortal.Checked = 
 *  0.3           03/05/2016                Sakthi          Function "PublishRequisition" modified and "SendVendorEmail" function newly added for Vendor Mail.
    0.4            20/May/2016            pravin khot       modify assign value - senderMailId
    0.5             01/sept/2016        Suraj Adsule            IJP modification
 * -------------------------------------------------------------------------------------------------------------------------------------------        
*/
using System;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Web.UI.Helper;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Threading;
using System.Net;
using System.Net.Mail;
using System.IO;
using System.Web.UI.WebControls;
using System.Text;
using System.Web.UI;
namespace TPS360.Web.UI
{
    public partial class cltRequisitionPreviewPublish : RequisitionBaseControl
    {
        #region Member Variables 
        
        string strMemberEmails = "";
        bool  IsScheduled = false;
        bool _mailsetting = false;
        #endregion

        #region Properties
        private string DefaultCountryCode
        {
            get
            {
                return SiteSetting[DefaultSiteSetting.Country.ToString()].ToString();
            }

        }
        #endregion

        #region Methods

        private void AddCreatorToRequisitionHiringTeam()
        {
            JobPostingHiringTeam jobPostingHiringTeam = Facade.GetJobPostingHiringTeamByMemberId(base.CurrentJobPosting.CreatorId, base.CurrentJobPostingId);

            if (jobPostingHiringTeam == null)
            {
                jobPostingHiringTeam = new JobPostingHiringTeam();
                jobPostingHiringTeam.EmployeeType = EmployeeType.Internal.ToString();
                jobPostingHiringTeam.JobPostingId = base.CurrentJobPostingId;
                jobPostingHiringTeam.MemberId = base.CurrentJobPosting.CreatorId;
                jobPostingHiringTeam.CreatorId = base.CurrentMember.Id;
                Facade.AddJobPostingHiringTeam(jobPostingHiringTeam);
            }
        }

        private void RedirectToList(string message, int id)
        {
            string returnUrl = Helper.Url.SecureUrl.ReturnUrl;

            if (chkFindMatchingCandidates.Checked)
            {
                returnUrl = UrlConstants.ATS.ATS_PRECISESEARCH;
                Helper.Url.Redirect(returnUrl, null, UrlConstants.PARAM_MSG, message, UrlConstants.PARAM_JOB_ID, id.ToString());
                return;
            }

            if (StringHelper.IsBlank(returnUrl))
            {
                //0.10 Start

                if (Convert.ToInt32(Session["List"]) == 1)
                {
                    returnUrl = UrlConstants.Requisition.JOB_POSTING_OPEN_LIST_PAGE;
                }
                else if (Convert.ToInt32(Session["List"]) == 2)
                {
                    returnUrl = UrlConstants.Requisition.JOB_POSTING_MY_REQUISITION_LIST_PAGE;
                }
                else if (Convert.ToInt32(Session["List"]) == 3)
                {
                    returnUrl = UrlConstants.Requisition.JOB_POSTING_MASTER_LIST_PAGE;                   
                }
                else
                {
                    returnUrl = UrlConstants.Requisition.JOB_POSTING_MASTER_LIST_PAGE;
                }
               // Session.Abandon();//0.13

                //0.10 End
            }

            if (!StringHelper.IsBlank(message))
            {
                Helper.Url.Redirect(returnUrl, null, UrlConstants.PARAM_MSG, message);
            }
            else
            {
                Helper.Url.Redirect(returnUrl);
            }
        }

        private StringBuilder BuildSearchDisplayFormat(JobPostingSearchAgent agent)
        {
            StringBuilder DisplayFormat = new StringBuilder();
            DisplayFormat.Append("<table><tr><td style =\"vertical-align :top ;\">");
            if (agent.KeyWord != string.Empty)
                DisplayFormat.Append("<strong>Keywords:</strong></td><td>" + agent.KeyWord + "</td></tr><tr><td style =\"vertical-align :top ;\">");
            if (agent.JobTitle != string.Empty)
                DisplayFormat.Append("<strong>Job Title:</strong></td><td>" + agent.JobTitle + "</td></tr><tr><td style =\"vertical-align :top ;\">");
            if (agent.City != string.Empty)
                DisplayFormat.Append("<strong>City:</strong></td><td>" + agent.City + "</td></tr><tr><td style =\"vertical-align :top ;\">");
            if (agent.CountryId != 0)
                DisplayFormat.Append("<strong>Country:</strong></td><td>" + agent.CountryName + "</td></tr><tr><td style =\"vertical-align :top ;\">");
            if (agent .StateId  != 0)
                DisplayFormat.Append("<strong>State:</strong></td><td>" + agent.StateName + "</td></tr><tr><td style =\"vertical-align :top ;\">");
            if (agent .MinExperience  != string.Empty && agent .MaxExperience  != string.Empty)
                DisplayFormat.Append("<strong>Experience:</strong></td><td>" + agent.MinExperience + " - " + agent.MaxExperience + " Years" + "</td></tr><tr><td style =\"vertical-align :top ;\">");
            if (agent.SalaryFrom != string.Empty && agent.SalaryTo != string.Empty)
            {
                string[] payCycle = { "", "Hour", "Day", "Month", "Year" };
                DisplayFormat.Append("<strong>Salary Range:</strong></td><td>" + agent.CurrencyName + " " + agent.SalaryFrom + " - " + agent.SalaryTo + " " + (agent.CurrencyName == "INR" ? " Lacs/" : " /") + payCycle[Int32.Parse(agent.SalaryType)] + "</td></tr><tr><td style =\"vertical-align :top ;\">");
            }
            if(agent .HotListId !=0)
                DisplayFormat.Append("<strong>Hot List:</strong></td><td>" + agent.HotlistName + "</td></tr><tr><td style =\"vertical-align :top ;\">");
            if(agent .EducationLookupId !=string .Empty )
                DisplayFormat.Append("<strong>Education:</strong></td><td>" + agent.EducationTypeName + "</td></tr><tr><td style =\"vertical-align :top ;\">");
            if(agent .EmployementTypeLookUpId !=0)
                DisplayFormat.Append("<strong>Employment Type:</strong></td><td>" + agent.EmployementTypeName + "</td></tr><tr><td style =\"vertical-align :top ;\">");
            if(agent .ResumeLastUpdated!=string .Empty && agent .ResumeLastUpdated !="0"  )
                DisplayFormat.Append("<strong>Resume Last Updated:</strong></td><td>" + ((Int32.Parse(agent.ResumeLastUpdated) <= 90 && Int32.Parse(agent.ResumeLastUpdated) > 0) ? agent.ResumeLastUpdated + " Days" : (Int32.Parse(agent.ResumeLastUpdated) == 182 ? "6 Months" : "1 Year")) + "</td></tr><tr><td style =\"vertical-align :top ;\">");
            if(agent .CandidateTypeLookUpId !=0)
                DisplayFormat.Append("<strong>Candidate Type:</strong></td><td>" + agent.CandidateTypeName + "</td></tr><tr><td style =\"vertical-align :top ;\">");
            if(agent .WorkScheduleLookupId !=0)
                DisplayFormat.Append("<strong>Work Schedule:</strong></td><td>" + agent.WorkScheduleTypeName + "</td></tr><tr><td style =\"vertical-align :top ;\">");
            if(agent .WorkStatusLookUpId !=string .Empty )
                DisplayFormat.Append("<strong>Work Status:</strong></td><td>" + agent.WorkStatusTypeName + "</td></tr><tr><td style =\"vertical-align :top ;\">");
            if(agent .HiringStatusLookUpId !=string .Empty )
                DisplayFormat.Append("<strong>Hiring Status:</strong></td><td>" + agent.HiringStatusTypeName + "</td></tr><tr><td style =\"vertical-align :top ;\">");
            if(agent .AvailableAfter !=DateTime .MinValue )
                DisplayFormat.Append("<strong>Available After:</strong></td><td>" + agent.AvailableAfter.ToShortDateString() + "</td></tr><tr><td style =\"vertical-align :top ;\">");
            if (DefaultCountryCode != "")
            {
                Country country = Facade.GetCountryById(Convert.ToInt32(DefaultCountryCode));
                if (country != null)
                {
                    if (country.CountryCode == "US")
                    {
                        if (agent.SecurityClearance == true)
                            DisplayFormat.Append("<strong>Security Clearence:</strong></td><td>" + "Yes" + "</td></tr><tr><td style =\"vertical-align :top ;\">");
                    }
                }
            }
            
            //else
            //    DisplayFormat.Append("<strong>Security Clearence:</strong></td><td>" + "Yes" + "</td></tr><tr><td style =\"vertical-align :top ;\">");
            return DisplayFormat;
        }

        private StringBuilder BuildDefaultSearchDisplayFormat(JobPosting  job)
        {
            StringBuilder DisplayFormat = new StringBuilder();
            string Skills = MiscUtil.SplitSkillValues(job.JobSkillLookUpId, '!', Facade);
            DisplayFormat.Append("<table><tr><td style =\"vertical-align :top ;\">");
            if (Skills  != string.Empty)
                DisplayFormat.Append("<strong>Keywords:</strong></td><td>" +"\""+job .JobTitle +"\""+ (Skills!=string .Empty ? ","+Skills :string .Empty ) + "</td></tr><tr><td style =\"vertical-align :top ;\">");
            if (job.JobTitle  != string.Empty)
                DisplayFormat.Append("<strong>Job Title:</strong></td><td>" + job.JobTitle + "</td></tr><tr><td style =\"vertical-align :top ;\">");
            if (job.City  != string.Empty)
                DisplayFormat.Append("<strong>City:</strong></td><td>" + job.City + "</td></tr><tr><td style =\"vertical-align :top ;\">");
            if (job.CountryId != 0)
                DisplayFormat.Append("<strong>Country:</strong></td><td>" + MiscUtil.GetCountryNameById(job.CountryId, Facade) + "</td></tr><tr><td style =\"vertical-align :top ;\">");
            if (job .StateId  != 0)
                DisplayFormat.Append("<strong>State:</strong></td><td>" + MiscUtil.GetStateNameById(job.StateId, Facade) + "</td></tr><tr><td style =\"vertical-align :top ;\">");
            if (job .MinExpRequired  != string.Empty && job .MaxExpRequired  != string.Empty)
                DisplayFormat.Append("<strong>Experience:</strong></td><td>" + job.MinExpRequired + " - " + job.MaxExpRequired + " Years" + "</td></tr><tr><td style =\"vertical-align :top ;\">");
            if (job.RequiredDegreeLookupId != string.Empty)
            {
                string DegreeName = Facade.GetLookUPNamesByIds(job.RequiredDegreeLookupId);
                DisplayFormat.Append("<strong>Education:</strong></td><td>" + DegreeName + "</td></tr><tr><td style =\"vertical-align :top ;\">");
            }
            if (DefaultCountryCode != "")
            {
                Country country = Facade.GetCountryById(Convert.ToInt32(DefaultCountryCode));
                if (country != null)
                {
                    if (country.CountryCode == "US")
                    {
                        if (job.SecurityClearance == true)
                            DisplayFormat.Append("<strong>Security Clearence:</strong></td><td>" + "Yes" + "</td></tr><tr><td style =\"vertical-align :top ;\">");
                    }
                }
            }
            //if (job.SecurityClearance == true )
            //    DisplayFormat.Append("<strong>Security Clearence:</strong></td><td>" + "Yes" + "</td></tr><tr><td style =\"vertical-align :top ;\">");
            //else
            //    DisplayFormat.Append("<strong>Security Clearence:</strong></td><td>" + "Yes" + "</td></tr><tr><td style =\"vertical-align :top ;\">");

            return DisplayFormat;
        }

        private void SearchAgentAdded(JobPostingSearchAgent searchagent, bool isAdded)
        {
            StringBuilder builder = new StringBuilder();
            if (isAdded)
            {
               builder = BuildSearchDisplayFormat(searchagent);
               dvHiddenDetails.InnerHtml = builder.ToString().Substring(0, builder.ToString().Length - 39) + "</table>";
               chkEmailMatchingCandidates.Checked = true;
            }
            else
            {
                chkEmailMatchingCandidates.Checked = false;
            }
        }

        private JobPostingSearchAgent BuildSearchAgent()
        {
            JobPostingSearchAgent searchagent = new JobPostingSearchAgent();
            string Skills = MiscUtil.SplitSkillValues(CurrentJobPosting.JobSkillLookUpId, '!', Facade);
            searchagent .KeyWord  =(CurrentJobPosting .JobTitle !=string .Empty ? "\""+MiscUtil .RemoveScript (CurrentJobPosting .JobTitle )+"\"":string .Empty) +( Skills != string.Empty ?","+MiscUtil .RemoveScript (Skills) : string.Empty);
            searchagent.JobTitle = CurrentJobPosting.JobTitle != string.Empty ? MiscUtil.RemoveScript(CurrentJobPosting.JobTitle) : string.Empty;
            searchagent.JobPostingId = base.CurrentJobPostingId;
            searchagent.City = CurrentJobPosting.City != string.Empty ? MiscUtil.RemoveScript(CurrentJobPosting.City) : string.Empty;
            searchagent.CountryId = CurrentJobPosting.CountryId > 0 ? CurrentJobPosting.CountryId : 0;
            searchagent.StateId = CurrentJobPosting.StateId > 0 ? CurrentJobPosting.StateId : 0;
            searchagent.MinExperience = CurrentJobPosting.MinExpRequired != string.Empty ? MiscUtil.RemoveScript(CurrentJobPosting.MinExpRequired) : string.Empty;
            searchagent.MaxExperience = CurrentJobPosting.MaxExpRequired != string.Empty ? MiscUtil.RemoveScript(CurrentJobPosting.MaxExpRequired) : string.Empty;
            searchagent.SecurityClearance = CurrentJobPosting.SecurityClearance;
            searchagent.EducationLookupId = CurrentJobPosting.RequiredDegreeLookupId != string.Empty ? CurrentJobPosting.RequiredDegreeLookupId : string.Empty;
            searchagent.SalaryFrom = string.Empty;
            searchagent.SalaryTo = string.Empty;
            searchagent.CurrencyLookUpId = 0;
            searchagent.SalaryType = "0";
            searchagent.HotListId = 0;
            searchagent.EmployementTypeLookUpId = 0;
            searchagent.ResumeLastUpdated = "0";
            searchagent.CandidateTypeLookUpId = 0;
            searchagent.WorkScheduleLookupId = 0;
            searchagent.WorkStatusLookUpId = string.Empty;
            searchagent.HiringStatusLookUpId = string.Empty;
            searchagent.AvailableAfter = DateTime .MinValue ;
            searchagent.CreatorId = searchagent.UpdatorId = CurrentMember.Id;
            searchagent.IsRemoved = false;
            return searchagent;
        }

        private void LoadTimeDropDowns()
        {
            int intStartTime = 12;
            string strMin = "00";
            string strAMPM = "AM";
            //ArrayList arrTime = new ArrayList();
            for (int i = 1; i <= 48; i++)
            {
                ListItem item = new ListItem();
                if (i % 2 == 0)
                {
                    strMin = "30";
                }
                else
                {
                    strMin = "00";
                }
                string strTime = intStartTime.ToString() + ":" + strMin + " " + strAMPM;
                //arrTime.Add(strTime); //1.2
                item.Text = strTime;
                item.Value = ((i-1)/2) + "." + (i % 2 == 0 ? "5" : "0");
                if (intStartTime == 12 && i % 2 == 0)
                {
                    intStartTime = 1;
                }
                else if (i % 2 == 0)
                {
                    intStartTime++;
                }
                if (i == 24)
                {
                    strAMPM = "PM";
                }
                ddlTime.Items.Add(item);
            }
            //ddlStartTime.DataSource = ddlEndTime.DataSource = arrTime; //1.2
            
            //ddlStartTime.DataBind();
            //ddlEndTime.DataBind();
            //GetCurrentTime();
        }

        private SearchAgentSchedule BuildSearchAgentSchedule(SearchAgentSchedule schedule)
        {
            schedule.JobPostingId = base.CurrentJobPostingId;
            if (rdbNow.Checked)
            {
                schedule.StartDate  = DateTime.Now;
                schedule.NextSendDate = DateTime.Now;//.AddHours(0.5);
            }
            else if (rdbDate.Checked)
            {
                if (wdcStartDate.Value != null && wdcStartDate.Text != string.Empty)
                {
                    schedule.NextSendDate = schedule.StartDate  = Convert.ToDateTime(wdcStartDate.Value).AddHours(Convert.ToDouble(ddlTime.SelectedValue));
                }
            }
            if (rdbRunOnce.Checked)
                schedule.Repeat = "0";
            else if (rdbDaily.Checked)
                schedule.Repeat = "1";
            else if (rdbWeekly.Checked)
                schedule.Repeat = "7";
            if (wdcEndDate.Value != null && wdcEndDate.Text != string.Empty)
                schedule.EndDate = Convert.ToDateTime(wdcEndDate.Value).AddDays (1).AddSeconds (-1);
            schedule.LastSentDate = DateTime.MinValue;
            schedule.UpdatorId = CurrentMember.Id;
            schedule.IsRemoved = false;
            return schedule;
        }
        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {

            cvUpdateDate.ValueToCompare = DateTime.Today.ToShortDateString();


            uclSearchTerm.SearchAgentAdded += SearchAgentAdded;
            if (CurrentJobPostingId > 0)
            {
                SearchAgentEmailTemplate template = Facade.GetSearchAgentEmailTemplateByJobPostingId(CurrentJobPostingId);
                if (template == null)
                    _mailsetting = MiscUtil.IsValidMailSetting(Facade, CurrentMember.Id);
                else
                {
                    if (template.SenderId == CurrentMember.Id)
                        _mailsetting = MiscUtil.IsValidMailSetting(Facade, template.SenderId);
                    else
                        _mailsetting = true;

                }
            }
            if (!_mailsetting)
            {
                dv.Visible = true;
                uclConfirm.MsgBoxAnswered += MessageAnswered;
            }
            if (!Page.IsPostBack)
            {
                PrepareView();
                //*************Code remove comment by pravin khot on 2/Feb/2016****************
                chkCandidatePortal.Visible = Convert.ToBoolean(SiteSetting[DefaultSiteSetting.CandidatePortalJobPublishing.ToString()].ToString());
                //chkCandidatePortal .Visible = Convert.ToBoolean(SiteSetting[DefaultSiteSetting.CandidatePortalJobPublishing.ToString()].ToString());
                //****************************End*****************************************
                chkVendorPortal.Visible = Convert.ToBoolean(SiteSetting[DefaultSiteSetting.VendorPortaPublishing.ToString()].ToString());
                chkShowinEmployeeReferal.Visible = Convert.ToBoolean(SiteSetting[DefaultSiteSetting.EmployeeReferralPortaPublishing.ToString()].ToString());
                if (SiteSetting[DefaultSiteSetting.AutomaticEmailMatchinReqEditor.ToString()] != null)
                {
                    // chkEmailMatchingCandidates.Visible = Convert.ToBoolean(SiteSetting[DefaultSiteSetting.AutomaticEmailMatchinReqEditor.ToString()].ToString());
                }
                if (CurrentJobPosting.VendorList != null && CurrentJobPosting.VendorList != "")
                {
                    //uncVendorList.SelectedItems = CurrentJobPosting.VendorList.Trim().TrimEnd(',');
                    foreach (ListItem list in chkVendorList.Items)
                    {
                        string[] word = CurrentJobPosting.VendorList.Split(',');
                        foreach (string s in word)
                            if (list.Value == s)
                                list.Selected = true;
                    }
                }

                chkAllowToChangeStatus.Checked = CurrentJobPosting.AllowRecruitersToChangeStatus;
                chkVendorPortal.Checked = CurrentJobPosting.DisplayRequisitionInVendorPortal;
                //chkCandidatePortal.Checked = CurrentJobPosting.ShowInCandidatePortal;
                chkCandidatePortal.Checked = CurrentJobPosting.ShowInCandidatePortal; //remove comment by pravin khot on 2/Feb/2016
                chkShowinEmployeeReferal.Checked = CurrentJobPosting.ShowInEmployeeReferralPortal;
                LoadTimeDropDowns();
                wdcStartDate.Style.Add("display", "none");
                ddlStartDateEmpty.Enabled = false;
                ddlTime.Enabled = false;
                SecureUrl url = UrlHelper.BuildSecureUrl(UrlConstants.ApplicationBaseUrl + UrlConstants.Requisition.JOB_POSTING_INTERNAL_PREVIEW_PAGE.Substring(2, UrlConstants.Requisition.JOB_POSTING_INTERNAL_PREVIEW_PAGE.Length - 2).Replace("//", "/"), string.Empty, UrlConstants.PARAM_JOB_ID, StringHelper.Convert(base.CurrentJobPostingId));
                //btnPreview.OnClientClick = "window.open('" + url.ToString() + "')";
            }



            ArrayList Privilege = Facade.GetAllMemberPrivilegeIdsByMemberId(CurrentMember.Id);
            if (Privilege.Contains(54))
                chkFindMatchingCandidates.Enabled = true;
            else
                chkFindMatchingCandidates.Enabled = false;

            ShowSearchAgent(CurrentJobPostingId);
            if (!IsPostBack)
            {
                wdcStartDate.Value = DateTime.Now.ToShortDateString();
                SearchAgentSchedule schedule = new SearchAgentSchedule();

                if (CurrentJobPostingId > 0)
                {
                    schedule = Facade.GetSearchAgentScheduleByJobPostingId(base.CurrentJobPostingId);
                    if (schedule != null)
                    {
                        cvUpdateDate.ValueToCompare = schedule.StartDate.ToShortDateString();
                        IsScheduled = true;
                        hdnScheduleId.Value = schedule.Id.ToString();
                        wdcStartDate.Style.Add("display", "");
                        ddlStartDateEmpty.Style.Add("display", "none");
                        ddlTime.Enabled = true;
                        string time = string.Empty;
                        rdbDate.Checked = true;
                        if (schedule.StartDate.Minute > 30)
                        {
                            schedule.StartDate = schedule.StartDate.AddHours(1);
                            time = (schedule.StartDate.Hour) + ".0";
                        }
                        else if (schedule.StartDate.Minute == 0)
                        {
                            time = (schedule.StartDate.Hour) + ".0";
                        }
                        else
                        {
                            time = (schedule.StartDate.Hour) + ".5";
                        }
                        wdcStartDate.Value = schedule.StartDate.ToShortDateString();
                        ddlTime.SelectedValue = time;
                        if (schedule.Repeat == "0")
                        {
                            rdbRunOnce.Checked = true;
                            rdbDaily.Checked = false;
                            rdbWeekly.Checked = false;
                        }
                        else if (schedule.Repeat == "1")
                        {
                            rdbRunOnce.Checked = false;
                            rdbDaily.Checked = true;
                            rdbWeekly.Checked = false;
                        }
                        else
                        {
                            rdbWeekly.Checked = true;
                            rdbRunOnce.Checked = false;
                            rdbDaily.Checked = false;
                        }
                        if (schedule.EndDate != DateTime.MinValue)
                            wdcEndDate.Value = schedule.EndDate;
                    }
                    else
                    {
                        cvUpdateDate.ValueToCompare = DateTime.Now.ToShortDateString();
                        IsScheduled = false;
                        if (CurrentJobPosting.FinalHiredDate != DateTime.MinValue)
                            wdcEndDate.Value = CurrentJobPosting.FinalHiredDate;
                    }
                }


            }
            if (chkEmailMatchingCandidates.Checked)
                dvHiddenMatchingDetails.Style.Add("display", "");
            else
                dvHiddenMatchingDetails.Style.Add("display", "none");
            if (!rdbRunOnce.Checked)
            {
                wdcEndDate.Style.Add("display", "");
                ddlenddate.Style.Add("display", "none");
            }
            else
            {
                wdcEndDate.Style.Add("display", "none");
                ddlenddate.Style.Add("display", "");
                cvenddate.Enabled = false;

            }
            if (!IsPostBack)
            {

                if (CurrentJobPostingId == 0)
                {
                    dvemailMatching.Style.Add("display", "none");
                    dvSaveandpublish.Style.Add("display", "");
                }
                else
                {
                    dvemailMatching.Style.Add("display", "");
                    dvSaveandpublish.Style.Add("display", "none");
                }
            }
            //*******************Suraj Adsule******Start********For********20160901*********//
            IList<CustomRole> roleList = Facade.GetCustomRoleRequisition(3); //Using Hide workflow               
            string roleName = Facade.GetCustomRoleNameByMemberId(CurrentMember.Id);
            if (roleList != null)
            {
                foreach (CustomRole role in roleList)
                {
                    if (role.Id == CurrentMember.Id || roleName.ToLower() == "admin")
                    {
                        if (role.Id == CurrentMember.Id)
                        {
                            chkSendEmail.Visible = false;
                            chkFindMatchingCandidates.Visible = false;
                            chkAllowToChangeStatus.Visible = false;
                            chkCandidatePortal.Visible = false;
                            chkVendorPortal.Visible = false;
                            divVendor.Visible = false;
                            chkShowinEmployeeReferal.Visible = false;
                            chkEmailMatchingCandidates.Visible = false;
                        }
                        CheckIJP.Visible = true;
                        divIJP.Visible = true;
                        //CheckIJP.Checked = CurrentJobPosting.DisplayInIJP;
                        //if (!string.IsNullOrEmpty(Convert.ToString(CurrentJobPosting.IJPExpiryDate)))
                        //{
                        //    wdcOpenDate1.Value = CurrentJobPosting.IJPExpiryDate;
                        //}
                        break;
                    }
                    else
                    {

                    }
                }
            }
            //*******************Suraj Adsule******Start********For********20160901*********//

            //*******************pravin******Start********For********20160901*********//
            IList<CustomRole> roleLists = Facade.GetCustomRoleRequisition(2);

            if (roleLists != null)
            {
                foreach (CustomRole roles in roleLists)
                {
                    if (roles.Id == CurrentMember.Id)
                    {
                        chkSendEmail.Checked = true;
                    }
                }
            }
            //******************end******************************
            chkCandidatePortal.Visible = false;
            chkVendorPortal.Visible = false;
            divVendor.Visible = false;
            chkShowinEmployeeReferal.Visible = false;
        }         
       

        public void PrepareView()
        {
            //MiscUtil.PopulateCompanyByCompanyStatus(uncVendorList.ListItem, CompanyStatus.Vendor, Facade);
            //uncVendorList.ListItem.Items.RemoveAt(0); 
            MiscUtil.PopulateCompanyByCompanyStatus(chkVendorList, CompanyStatus.Vendor, Facade);
            //chkVendorList = (CheckBoxList)MiscUtil.RemoveScriptForCheckBoxList(chkVendorList);
            chkVendorList.Items.RemoveAt(0);
        }

        public void ShowSearchAgent(int JobPostingId )
        {
            JobPostingSearchAgent searchagent = new JobPostingSearchAgent();
            StringBuilder display = new StringBuilder();
            if (JobPostingId > 0)
            {
                searchagent = Facade.GetJobPostingSearchAgentByJobPostingId(JobPostingId);
                if (searchagent != null)
                {

                    display = BuildSearchDisplayFormat(searchagent);
                    dvHiddenDetails.InnerHtml = display.ToString().Substring(0, display.ToString().Length - 39) + "</table>";
                    if (!IsPostBack)
                        chkEmailMatchingCandidates.Checked = true;
                }
                else
                {
                    JobPosting job = new JobPosting();
                    job = Facade.GetJobPostingById(JobPostingId);
                    if (job != null)
                    {
                        display = BuildDefaultSearchDisplayFormat(job);
                        dvHiddenDetails.InnerHtml = display.ToString().Substring(0, display.ToString().Length - 39) + "</table>";
                    }
                    //if (!IsPostBack)
                      //  chkEmailMatchingCandidates.Checked = false;
                }
            }
        }
        public void ShowSearchAgent()
        {
            dvemailMatching.Style.Add("display", "");
            dvSaveandpublish.Style.Add("display", "none");
        }

        protected void lnkSaveAndReload_Click(object sender, EventArgs e)
        {
        }
        private SearchAgentEmailTemplate BuildSearchAgentEmailTemplate(SearchAgentEmailTemplate template, bool isNew)
        {
            string JobTitle = "";
          
            template.JobPostingID = CurrentJobPostingId;
            template.EmailBody = MiscUtil.getDefaultSearchAgentMailTemplate(CurrentJobPostingId, CurrentMember.Id, out JobTitle, Facade);
            template.Subject = MiscUtil.RemoveScript(JobTitle.Trim());
            if (isNew)
                template.CreatorId = template.SenderId = CurrentMember.Id;
            else template.UpdatorId = CurrentMember.Id;
            template.IsRemoved = false;
            return template;
        }
        private void SaveSearchAgent(JobPosting jobPosting)
        {
            JobPostingSearchAgent searchagent = new JobPostingSearchAgent();
            searchagent = Facade.GetJobPostingSearchAgentByJobPostingId(jobPosting .Id );
            if (searchagent != null)
            {
                if (!chkEmailMatchingCandidates.Checked)
                    Facade.DeleteJobPostingSearchAgentByJobPostingId(jobPosting.Id);
            }
            else
            {
                if (chkEmailMatchingCandidates.Checked)
                {
                    searchagent = BuildSearchAgent();
                    searchagent = Facade.AddJobPostingSearchAgent(searchagent);
                }
            }


            SearchAgentEmailTemplate template = Facade.GetSearchAgentEmailTemplateByJobPostingId(jobPosting.Id);
            if (template == null)
            {
                if (chkEmailMatchingCandidates.Checked)
                {
                    template = new SearchAgentEmailTemplate();
                    template = BuildSearchAgentEmailTemplate(template, true);
                    Facade.AddSearchAgentEmailTemplate(template);
                }
            }
            else
            {
                if (!chkEmailMatchingCandidates.Checked)
                    Facade.DeleteSearchAgentEmailTemplateByJobPostingId(jobPosting.Id);
            }

            SearchAgentSchedule schedule = new SearchAgentSchedule();
            schedule = Facade.GetSearchAgentScheduleByJobPostingId(jobPosting.Id);

            if (schedule != null)
            {
                schedule = BuildSearchAgentSchedule(schedule);
                if (!chkEmailMatchingCandidates.Checked)
                    Facade.DeleteSearchAgentScheduleByJobPostingId(jobPosting.Id);
                else
                    schedule = Facade.UpdateSearchAgentSchedule(schedule);
            }
            else
            {
                if (chkEmailMatchingCandidates.Checked)
                {
                    schedule = new SearchAgentSchedule();
                    schedule = BuildSearchAgentSchedule(schedule);
                    schedule.CreatorId = base.CurrentMember.Id;
                    schedule = Facade.AddSearchAgentSchedule(schedule);
                }
            }
        }


       public JobPosting  BuildJobPostingPreviewPublish(JobPosting jobPosting)
        {
            StringBuilder vendorList = new StringBuilder();
            foreach (ListItem list in chkVendorList.Items)
            {

                if (list.Selected)
                {
                    vendorList.Append(list.Value);
                    vendorList.Append(",");
                }
            }
            if (vendorList.ToString() != "")
                jobPosting.VendorList = vendorList.ToString().TrimEnd(',');
                jobPosting.ShowInCandidatePortal = chkCandidatePortal.Checked;
                jobPosting.ShowInEmployeeReferralPortal = chkShowinEmployeeReferal.Checked;
                jobPosting.AllowRecruitersToChangeStatus = chkAllowToChangeStatus.Checked;
                jobPosting.DisplayRequisitionInVendorPortal = chkVendorPortal.Checked;
                jobPosting.IsJobActive = true;

                jobPosting.WorkflowApproved = true;
                //*******************Suraj Adsule******Start********For********20160830*********//
                if (CheckIJP.Checked)
                {
                    jobPosting.DisplayInIJP = CheckIJP.Checked;
                    jobPosting.DisplayInExternal = false;
                    jobPosting.IJPExpiryDate = Convert.ToDateTime(wdcOpenDate1.Value);
                    jobPosting.ApprovedForIJP = true;
                }
                else if (!CheckIJP.Checked)
                {
                    DateTime? dt;
                    dt = (DateTime?)null;
                    jobPosting.DisplayInIJP = false;
                    jobPosting.DisplayInExternal = false;
                    jobPosting.IJPExpiryDate = dt;
                    jobPosting.ApprovedForIJP = false;
                }
                //*******************Suraj Adsule******End********For********20160830*********//
                return jobPosting;
        }

       public void PublishRequisition(JobPosting jobPosting, int chk)
       {
           bool isApprovedToPublish = true;
           bool IsError = false;
           string strSMTP = string.Empty;
           string strUser = string.Empty;
           string strPwd = string.Empty;
           int intPort = 25;
           bool boolsslEnable = false;
           Hashtable siteSettingTable = SiteSetting;

           bool IsPublished = false;
           //*****************Code modify by pravin khot on 20/May/2016**************
           // string senderMailId = SiteSetting[DefaultSiteSetting.AdminEmail.ToString()].ToString(); //OLD CODE
           string senderMailId = CurrentMember.PrimaryEmail; //NEW CODE
           //***************************END*********************************

           if (isApprovedToPublish)
           {
               int hireCount = 0;

               //*************Code Added by pravin khot on 17/Nov/2016*************
               if (chkSendEmail.Checked)
               {
                   if (jobPosting.Id > 0)
                   {
                       SendRequsitionApprovalEmailToTeam(jobPosting);
                   }
               }
               //******************END*****************************************

               //*************Code commented by pravin khot on 17/Nov/2016*************
               #region
               //if (chkSendEmail.Checked)
               //{
               //    if (jobPosting.Id > 0)
               //    {
               //        SendRequsitionApprovalEmailToTeam(jobPosting);
               //    }
               //    if (jobPosting .Id  > 0)
               //    {
               //        SendRequsitionApprovalEmailToTeam(jobPosting);

               //        IList<JobPostingHiringTeam> HiringTeamList = Facade.GetAllJobPostingHiringTeamByJobPostingId(jobPosting .Id );

               //        if (HiringTeamList != null)
               //        {
               //            hireCount = HiringTeamList.Count;
               //            for (int i = 0; i < HiringTeamList.Count; i++)
               //            {
               //                Member HiringMemberEmails = Facade.GetMemberById(HiringTeamList[i].MemberId);
               //                if (HiringMemberEmails != null )
               //                {
               //                    //strMemberEmails = HiringMemberEmails.FirstName + " " + HiringMemberEmails.LastName + "<" + HiringMemberEmails.PrimaryEmail + "> , " + strMemberEmails;
               //                    strMemberEmails = HiringMemberEmails.PrimaryEmail + " , " + strMemberEmails;                                           
               //                }
               //            }
               //        }
               //    }

               //    //if (base.CurrentJobPosting.CreatorId > 0)
               //    //{
               //    //    Member JobPostingCreator = Facade.GetMemberById(base.CurrentJobPosting.CreatorId);
               //    //    if (JobPostingCreator != null)
               //    //    {
               //    //        strMemberEmails = strMemberEmails + JobPostingCreator.FirstName + " " + JobPostingCreator.LastName + "<" + JobPostingCreator.PrimaryEmail + ">";
               //    //    }
               //    //}
               //    if (strMemberEmails == "")
               //    {
               //        strMemberEmails = CurrentMember.FirstName + " " + CurrentMember.LastName + "<" + CurrentMember.PrimaryEmail + ">";
               //    }
               //    string[] strarrMemberEmailIds = strMemberEmails.Split(',');

               //    if (SiteSetting != null)
               //    {
               //        strUser = siteSettingTable[DefaultSiteSetting.SMTPUser.ToString()].ToString();
               //        strPwd = siteSettingTable[DefaultSiteSetting.SMTPassword.ToString()].ToString();
               //        strSMTP = siteSettingTable[DefaultSiteSetting.SMTP.ToString()].ToString();
               //        if (siteSettingTable[DefaultSiteSetting.SMTPort.ToString()].ToString() != null && siteSettingTable[DefaultSiteSetting.SMTPort.ToString()].ToString() != "")
               //        {
               //            try
               //            {
               //                intPort = Convert.ToInt32(siteSettingTable[DefaultSiteSetting.SMTPort.ToString()].ToString());
               //            }
               //            catch (FormatException)
               //            {
               //                MiscUtil.ShowMessage(lblMessage, "SMTP Port data is incorrect in site settings.", true);
               //                return;
               //            }
               //        }

               //        if (siteSettingTable[DefaultSiteSetting.SMTPEnableSSL.ToString()].ToString() != null)
               //            boolsslEnable = Convert.ToBoolean(siteSettingTable[DefaultSiteSetting.SMTPEnableSSL.ToString()].ToString());
               //    }

               //    if (senderMailId != "")
               //    {
               //        for (int i = 0; i < strarrMemberEmailIds.Length; i++)
               //        {
               //            if (strUser != "" && strPwd != "" && strSMTP != "")
               //            {
               //                if (strarrMemberEmailIds[i].Trim() != "")
               //                {
               //                    IsError = SendEmail(strarrMemberEmailIds[i], strSMTP, strUser, strPwd, intPort, boolsslEnable, jobPosting);
               //                }
               //            }
               //            else
               //            {
               //                IsError = true;
               //                break;
               //            }
               //        }
               //        if (IsError)
               //        {
               //            RedirectToList("Please set SMTP credentials in site settings.", jobPosting.Id);
               //            return;
               //        }
               //    }
               //}
               #endregion
               //******************END*******************

               //************ Code Added By Sakthi On 03/05/2016 ******************
               if (chkVendorPortal.Checked)
               {

                   if (jobPosting.Id > 0)
                   {
                       string VendorList = jobPosting.VendorList.TrimEnd(',');
                       if (VendorList != "")
                       {
                           string[] word = VendorList.Split(',');
                           foreach (string s in word)
                           {
                               IList<CompanyContact> contact = Facade.GetAllCompanyContactByComapnyId(Convert.ToInt32(s));
                               if (contact != null)
                               {
                                   foreach (CompanyContact c in contact)
                                   {
                                       strMemberEmails = c.Email + ", " + strMemberEmails;
                                   }
                               }
                           }
                       }
                   }

                   string[] strarrMemberEmailIds = strMemberEmails.Split(',');

                   if (SiteSetting != null)
                   {
                       strUser = siteSettingTable[DefaultSiteSetting.SMTPUser.ToString()].ToString();
                       strPwd = siteSettingTable[DefaultSiteSetting.SMTPassword.ToString()].ToString();
                       strSMTP = siteSettingTable[DefaultSiteSetting.SMTP.ToString()].ToString();
                       if (siteSettingTable[DefaultSiteSetting.SMTPort.ToString()].ToString() != null && siteSettingTable[DefaultSiteSetting.SMTPort.ToString()].ToString() != "")
                       {
                           try
                           {
                               intPort = Convert.ToInt32(siteSettingTable[DefaultSiteSetting.SMTPort.ToString()].ToString());
                           }
                           catch (FormatException)
                           {
                               MiscUtil.ShowMessage(lblMessage, "SMTP Port data is incorrect in site settings.", true);
                               return;
                           }
                       }

                       if (siteSettingTable[DefaultSiteSetting.SMTPEnableSSL.ToString()].ToString() != null)
                           boolsslEnable = Convert.ToBoolean(siteSettingTable[DefaultSiteSetting.SMTPEnableSSL.ToString()].ToString());
                   }

                   if (senderMailId != "")
                   {
                       for (int i = 0; i < strarrMemberEmailIds.Length; i++)
                       {
                           if (strUser != "" && strPwd != "" && strSMTP != "")
                           {
                               if (strarrMemberEmailIds[i].Trim() != "")
                               {
                                   IsError = SendVendorEmail(strarrMemberEmailIds[i], strSMTP, strUser, strPwd, intPort, boolsslEnable, jobPosting);
                               }
                           }
                           else
                           {
                               IsError = true;
                               break;
                           }
                       }
                       if (IsError)
                       {
                           RedirectToList("Please set SMTP credentials in site settings.", jobPosting.Id);
                           return;
                       }
                   }

               }
               //************ Code END By Sakthi On 03/05/2016 ******************
               if (chk == 1)
               {
                   //MiscUtil.EventLogForRequisition(EventLogForRequisition.RequisitionPublished, CurrentJobPosting.Id, CurrentMember.Id, Facade);
                   // Code modified by Sakthi on Feb-04-2016 - if condition is added to check for Admin email and to display message accordingly.
                   if (senderMailId == "")
                       RedirectToList("Requisition has been published successfully. But Notification cannot able to send as Admin Email is not set in site settings.", jobPosting.Id);
                   else
                       RedirectToList("Requisition has been published successfully.", jobPosting.Id);
               }
               else
               {
                   RedirectToList("Successfully saved changes for Requisition.", jobPosting.Id);
               }
           }
           else
           {
               MiscUtil.ShowMessage(lblMessage, "Approval required to publish this requisition.", true);
           }
       }         
        

        public void SaveAgent(JobPosting jobposting)
        {
            if (chkEmailMatchingCandidates.Checked)
            {
                if (_mailsetting)
                {
                    SaveSearchAgent(jobposting);
                    //PublishRequisition();
                }
                else
                {
                    dv.Visible = true;
                    uclConfirm.AddMessage("Mail Server SMTP details have not been entered. Would you like to enter them now?", ConfirmationWindow.enmMessageType.Attention, true, true, "");
                }
            }
            else
            {
                SaveSearchAgent(jobposting);
                //PublishRequisition();
            }
        }
        protected void btnPublish_Click(object sender, EventArgs e)
        {

          
           
        }
        public void MessageAnswered(object sender, ConfirmationWindow.MsgBoxEventArgs e)
        {
            if (e.Answer == ConfirmationWindow.enmAnswer.OK)
            {
                SecureUrl url = UrlHelper.BuildSecureUrl(UrlConstants.ApplicationBaseUrl + "Employee/MailSetup.aspx", string.Empty);
                ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "Open", "<script>window.open('" + url + "');</script>", false);
            }

        }

        //***********Added  by pravin khot on 17/Nov/2016***************
        private bool SendRequsitionApprovalEmailToTeam(JobPosting jobposting)
        {
            string CCSendEmail="";
            string TOSendEmail="";
            string TOSendEmailNames="";
            int InternalJobPostingId = 0;
            if (jobposting.Id > 0)
            {
                //****************Code added by pravin khot on 17/Nov/2016***************
                            IList<CustomRole> roleList3 = Facade.GetCustomRoleRequisition(3);
                            if (roleList3 != null)
                            {
                                foreach (CustomRole role in roleList3)
                                {
                                    InternalJobPostingId = role.Id;
                                }
                            }                                                    
                //**************************END*********************************

                IList<JobPostingHiringTeam> HiringTeamList = Facade.GetAllJobPostingHiringTeamByJobPostingId(jobposting.Id);
                if (HiringTeamList != null)
                {            
                    for (int i = 0; i < HiringTeamList.Count; i++)
                    {
                        Member HiringMemberEmails = Facade.GetMemberById(HiringTeamList[i].MemberId);
                        if (HiringMemberEmails != null)
                        {
                            string MemberRole = Facade.GetCustomRoleNameByMemberId(HiringTeamList[i].MemberId);
                            if (InternalJobPostingId != HiringTeamList[i].MemberId)
                            {
                                if (MemberRole.ToString() == "Recruiter")
                                {
                                    TOSendEmail = HiringMemberEmails.PrimaryEmail + " , " + TOSendEmail;
                                    string Name = HiringMemberEmails.FirstName + " " + HiringMemberEmails.MiddleName + " " + HiringMemberEmails.LastName;
                                    TOSendEmailNames = TOSendEmailNames + " , " + Name;
                                }
                                else
                                {
                                    CCSendEmail = HiringMemberEmails.PrimaryEmail + " , " + CCSendEmail;
                                }
                            }                
                        }
                    }
                }
            }
            string senderMailId = CurrentMember.PrimaryEmail;
            Member member = Facade.GetMemberByMemberEmail(senderMailId);

            string strSenderName = member.FirstName + " " + member.LastName;

            string Subject = "";
            string EmailBody = "";

            Subject = "NOTIFICATION: Requisition " + jobposting.JobTitle + "-" + jobposting.JobPostingCode + " Published";

            EmailBody = "Dear " + TOSendEmailNames + " <br/><br/>" + strSenderName + " has published requisition " + jobposting.JobTitle + "-" + jobposting.JobPostingCode + ". Please go to your <b>My Requisition List</b> to view the requisition details." + "<br/><br/>Regards,<br/>" + strSenderName;
           // EmailBody = "Dear " + txtInfo.ToTitleCase(strUserName[0]) + ", <br/><br/>" + strSenderName + " has published requisition " + jobposting.JobTitle + "-" + jobposting.JobPostingCode + ". Please go to your <b>My Requisition List</b> to view the requisition details." + "<br/><br/>Regards,<br/>" + strSenderName;
            
            if (CCSendEmail != "" && TOSendEmail != "")
            {

                //Added by pravin khot on 19/Jan/2016**********
                string AdminEmailId = string.Empty;
                int AdminMemberid = 0;
                SiteSetting siteSetting = Facade.GetSiteSettingBySettingType((int)SettingType.SiteSetting);
                if (siteSetting != null)
                {
                    Hashtable siteSettingTable = ObjectEncrypter.Decrypt<Hashtable>(siteSetting.SettingConfiguration);
                    AdminEmailId = siteSettingTable[DefaultSiteSetting.AdminEmail.ToString()].ToString();
                    AdminMemberid = Facade.GetMemberIdByEmail(AdminEmailId);
                }
                MailQueueData.AddMailToMailQueue(AdminMemberid, TOSendEmail, Subject, EmailBody, "", CCSendEmail, null, Facade);
            }
            return false;
        }
        //*****************************END***********************

        /// <summary>
        /// Method to send email notification.
        /// </summary>
        /// <param name="strEmailId"></param>
        private bool SendEmail(string strEmailId,string strSMTP,string strUser,string strPwd,int intPort,bool boolsslEnable,JobPosting jobposting)   
        {
                bool strSendStatus = true;
                EmailHelper emailManager=new EmailHelper ();
                CultureInfo cultInfo = Thread.CurrentThread.CurrentCulture;
                TextInfo txtInfo = cultInfo.TextInfo;
                
                string strSenderName = txtInfo.ToTitleCase(base.CurrentMember.FirstName + " " + base.CurrentMember.LastName);
                //string[] strUserName = strEmailId.Split('<');
                string[] strUserName = strEmailId.Split(',');
                string Subject = "";
                string EmailBody = "";
                //MailMessage message = new MailMessage();
                //MailAddress mailAddressFrom =new MailAddress( SiteSetting[DefaultSiteSetting.SMTPUser.ToString()].ToString()); //new MailAddress(base.CurrentMember.PrimaryEmail);//1.8
                //MailAddress mailAddressTo = new MailAddress(strEmailId);
                //emailManager .From =SiteSetting[DefaultSiteSetting.SMTPUser.ToString()].ToString();
                //emailManager .To .Add (strEmailId );
                //emailManager.Subject = "NOTIFICATION: Requisition " + jobposting.JobTitle  + "-" + jobposting.JobPostingCode + " Published";
                //emailManager.Body = "Dear " + txtInfo.ToTitleCase(strUserName[0]) + ", <br/><br/>" + strSenderName + " has published requisition " +jobposting .JobTitle  + "-" + jobposting .JobPostingCode  + ". Please go to your <b>My Requisition List</b> to view the requisition details." + "<br/><br/>Regards,<br/>" + strSenderName; // 0.5
                Subject = "NOTIFICATION: Requisition " + jobposting.JobTitle + "-" + jobposting.JobPostingCode + " Published";
                EmailBody = "Dear " + txtInfo.ToTitleCase(strUserName[0]) + ", <br/><br/>" + strSenderName + " has published requisition " + jobposting.JobTitle + "-" + jobposting.JobPostingCode + ". Please go to your <b>My Requisition List</b> to view the requisition details." + "<br/><br/>Regards,<br/>" + strSenderName;

                //*****************Code modify by pravin khot on 20/May/2016**************
                // string senderMailId = SiteSetting[DefaultSiteSetting.AdminEmail.ToString()].ToString(); //OLD CODE
                string senderMailId = CurrentMember.PrimaryEmail; //NEW CODE
                //***************************END*********************************
                Member member = Facade.GetMemberByMemberEmail(senderMailId);
               
              //Added by pravin khot on 19/Jan/2016**********
            string AdminEmailId =string.Empty ;
            int AdminMemberid =0;
            SiteSetting siteSetting = Facade.GetSiteSettingBySettingType((int)SettingType.SiteSetting);
            if (siteSetting != null)
            {
                Hashtable siteSettingTable = ObjectEncrypter.Decrypt<Hashtable>(siteSetting.SettingConfiguration);
                AdminEmailId = siteSettingTable[DefaultSiteSetting.AdminEmail.ToString()].ToString();
                AdminMemberid = Facade.GetMemberIdByEmail(AdminEmailId);
            }
            MailQueueData.AddMailToMailQueue(AdminMemberid, strEmailId, Subject, EmailBody, "", "", null, Facade);
            
            //if (emailManager.Send() == "1")   strSendStatus = false;
            return false;                   
        }
        //***************** Function Added by Sakthi on 03/05/2016 ****************
        private bool SendVendorEmail(string strEmailId, string strSMTP, string strUser, string strPwd, int intPort, bool boolsslEnable, JobPosting jobposting)
        {
            Hashtable siteSettingTable = null;
            TPS360.BusinessFacade.Facade f1 = new TPS360.BusinessFacade.Facade();
            SiteSetting siteSetting = f1.GetSiteSettingBySettingType((int)SettingType.SiteSetting);
            siteSettingTable = ObjectEncrypter.Decrypt<Hashtable>(siteSetting.SettingConfiguration);
            string companyName = siteSettingTable[DefaultSiteSetting.CompanyName.ToString()].ToString();
            bool strSendStatus = true;
            EmailHelper emailManager = new EmailHelper();
            CultureInfo cultInfo = Thread.CurrentThread.CurrentCulture;
            TextInfo txtInfo = cultInfo.TextInfo;

            string strSenderName = txtInfo.ToTitleCase(base.CurrentMember.FirstName + " " + base.CurrentMember.LastName);
            //string[] strUserName = strEmailId.Split('<');
            string[] strUserName = strEmailId.Split(',');
            string Subject = "";
            StringBuilder EmailBody = new StringBuilder();
            //MailMessage message = new MailMessage();
            //MailAddress mailAddressFrom =new MailAddress( SiteSetting[DefaultSiteSetting.SMTPUser.ToString()].ToString()); //new MailAddress(base.CurrentMember.PrimaryEmail);//1.8
            //MailAddress mailAddressTo = new MailAddress(strEmailId);
            //emailManager .From =SiteSetting[DefaultSiteSetting.SMTPUser.ToString()].ToString();
            //emailManager .To .Add (strEmailId );
            //emailManager.Subject = "NOTIFICATION: Requisition " + jobposting.JobTitle  + "-" + jobposting.JobPostingCode + " Published";
            //emailManager.Body = "Dear " + txtInfo.ToTitleCase(strUserName[0]) + ", <br/><br/>" + strSenderName + " has published requisition " +jobposting .JobTitle  + "-" + jobposting .JobPostingCode  + ". Please go to your <b>My Requisition List</b> to view the requisition details." + "<br/><br/>Regards,<br/>" + strSenderName; // 0.5
            foreach (string VUser in strUserName)
            {
                try
                {
                    Member VMember = Facade.GetMemberByMemberEmail(VUser);
                    Subject = "NOTIFICATION: Requisition " + jobposting.JobTitle + "-" + jobposting.JobPostingCode + " Published";
                    EmailBody.Append("<head><title></title>");
                    EmailBody.Append("<style>body, td, p{font-family: Arial;font-size: 12px;} </style></head>");
                    EmailBody.Append("<body><P><STRONG>Dear " + VMember.FirstName + " " + VMember.LastName + ",</STRONG></P>");
                    EmailBody.Append("<P>You are invited to submit eligible candidates for the following open position with us.</P>");
                    EmailBody.Append("<table><tr>");
                    EmailBody.Append("<td width=\"100\"><STRONG>Job Title</STRONG></td><td width=\"10\" align=\"center\">:</td><td width=500> " + jobposting.JobTitle + "</td>");
                    EmailBody.Append("</tr><tr>");
                    EmailBody.Append("<td width=\"100\"><STRONG>No. of Openings</STRONG></td><td width=\"10\" align=\"center\">:</td><td width=500> " + jobposting.NoOfOpenings + "</td>");
                    EmailBody.Append("</tr><tr>");
                    EmailBody.Append("<td width=\"100\"><STRONG>Location</STRONG></td><td width=\"10\" align=\"center\">:</td><td width=500> " + jobposting.City + "</td>");
                    EmailBody.Append("</tr><tr>");
                    EmailBody.Append("<td width=\"100\"><STRONG>Job Description</STRONG></td><td width=\"10\" align=\"center\">:</td><td width=500> " + jobposting.JobDescription + "</td>");
                    EmailBody.Append("<tr/></table><P/>");
                    EmailBody.Append("<br/><br/>Warm Regards,<br/>Talent Acquisition Team<br>" + companyName);

                    string senderMailId = SiteSetting[DefaultSiteSetting.AdminEmail.ToString()].ToString();
                    Member member = Facade.GetMemberByMemberEmail(senderMailId);
                    MailQueueData.AddMailToMailQueue(member.Id, strEmailId, Subject, EmailBody.ToString(), "", "", null, Facade);
                }
                catch
                {
                }
            }
            //if (emailManager.Send() == "1")   strSendStatus = false;
            return false;
        }
        //***************** Function END by Sakthi on 03/05/2016 ****************
        #endregion

        public static string MakeTinyUrl(string Url)
        {
            try
            {
                if (Url.Length <= 30)
                {
                    return Url;
                }
                if (!Url.ToLower().StartsWith("http") && !Url.ToLower().StartsWith("ftp"))
                {
                    Url = "http://" + Url;
                }
                var request = WebRequest.Create("http://tinyurl.com/api-create.php?url=" + Url);
                var res = request.GetResponse();
                string text;
                using (var reader = new StreamReader(res.GetResponseStream()))
                {
                    text = reader.ReadToEnd();
                }
                return text;
            }
            catch (Exception)
            {
                return Url;
            }
        }

        public static bool PostTweet(string username, string password, string tweet)
        {
            bool IsPosted = true;

            string user = Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(username + ":" + password));
            byte[] bytes = System.Text.Encoding.ASCII.GetBytes("status=" + tweet);
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://twitter.com/statuses/update.xml");

            request.Method = "POST";
            request.ServicePoint.Expect100Continue = false; // thanks to argodev for this recent change!
            request.Headers.Add("Authorization", "Basic " + user);
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = bytes.Length;

            Stream reqStream = request.GetRequestStream();
            reqStream.Write(bytes, 0, bytes.Length);
            reqStream.Close();

            try
            {
                request.GetResponse();
            }
            catch
            {
                IsPosted = false;
            }
            
            return IsPosted;
        }

        //protected void chkEmailMatchingCandidates_CheckedChanged(object sender, EventArgs e)
        //{
        //    if (chkEmailMatchingCandidates.Checked)
        //    {
        //        dvHiddenMatchingDetails.Visible = true;
        //    }
        //    else
        //    {
        //        dvHiddenMatchingDetails.Visible = false;
        //    }
        //}
        protected void lnkEditMatchingDetails_Click(object sender, EventArgs e)
        {
            HiddenField hdnClear = (HiddenField)uclSearchTerm.FindControl("hdnPostback");
            hdnClear.Value = "1";
            mpeSearchTerm.Enabled = true;
            mpeSearchTerm.Show();
        }

        protected void lnkEditEmailTemplate_Click(object sender, EventArgs e)
        {
            
            mpeEmailEditor.Enabled = true;
            HiddenField hdnClear = (HiddenField)uclEmailEditor.FindControl("hdnClear");
            hdnClear.Value = "1";
            mpeEmailEditor.Show();
        }
}
}