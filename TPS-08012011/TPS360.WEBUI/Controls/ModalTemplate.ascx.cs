﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using TPS360.Common.BusinessEntities;
using TPS360.Web.UI;
using TPS360.Common.Shared;
using System.Collections.Generic;

public partial class ModalTemplate : UserControl 
{
    private ITemplate contents = null;

    [TemplateContainer(typeof(TemplateControl))]
    [PersistenceMode(PersistenceMode.InnerProperty)]
    [TemplateInstance(TemplateInstance.Single)]
    public ITemplate Contents
    {
        get
        {
            return contents;
        }
        set
        {
            contents = value;
        }
    }
    private string _title;
    public string ModalTitle
    {
        get
        {
            return _title;
        }
        set
        {
            _title = value;
            lbModalTitle.Text = value.ToString();

        }
    }

    private string display;
    public string ContentDisplay
    {
        get
        {
            return display;
        }
        set
        {
            display = value;
            divContent.Style.Add("display", value);
        }
    }
    private string _contentwidth;
    public string ContentWidth
    {
        get
        {
            return _contentwidth;
        }
        set
        {
            _contentwidth = value;
            divContent.Style.Add("width", value);
        }
    }
    private string _contentheight;
    public string ContentHeight
    {
        get
        {
            return _contentheight;
        }
        set
        {
            _contentheight = value;
            divContent.Style.Add("height", value);
        }
    }

    private string _ModalBehaviourId;
    public string ModalBehaviourId
    {
        get
        {
           return  _ModalBehaviourId;
        }
        set
        {
            _ModalBehaviourId = value;
            imgClose.Attributes.Add("onclick", "$find('"+value +"').hide();");
        }
    }


    void Page_Init()
    {
        if (contents != null)
            contents.InstantiateIn(ModalPlaceHolder);
    }


}
