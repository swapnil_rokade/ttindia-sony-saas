﻿using System;
using System.Web.Security;
using TPS360.Common.Helper;
using TPS360.Common.BusinessEntities;
using TPS360.Web.UI.Helper;
using TPS360.Common.Shared;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Web.UI;
using TPS360.Common;
using System.Text.RegularExpressions;
using System.Web;
using TPS360.Providers;
using System.Collections;
using System.Linq;
namespace TPS360.Web.UI
{
    public partial class RequisitionList : BaseControl
    {
        #region Member Variables
        IList<HiringMatrixLevels> hiringLevels = null;
        bool ShowAction = false;
        public int HeaderAdd = 0;
        ArrayList _permittedMenuIdList;
        public string RequisitionType = "Master";
        public static bool IsAccss = false;
        public string roleName;
        public DropDownList ddlJobStatusCommon
        {
            set;
            get;
        }
        #endregion

        #region Methods
        private void BindList()
        {
            this.lsvJobPosting.DataBind();

        }
        private void MemberPrivilege()
        {
            //SiteMapNodeCollection permittedNodeList = new SiteMapNodeCollection();

            //SqlSiteMapProvider provider = SiteMap.Providers["SqlSiteMap"] as SqlSiteMapProvider;
            //SiteMapNode root = provider.FindSiteMapNodeFromKey(StringHelper.Convert((int)SiteMapType.Requisition));

            //if (root != null)
            //{
            //    SiteMapNodeCollection nodeList = root.ChildNodes;

            //    if (nodeList != null && nodeList.Count > 0)
            //    {
            //        foreach (SiteMapNode node in nodeList)
            //        {
            //            if (IsInPermitedMenuList(Convert.ToInt32(node["Id"])))
            //            {
            //                if (node.Title == "New Requisition")
            //                {
            //                    lnkAddJobPosting.Visible = true;
            //                }
            //            }
            //        }
            //    }
            //}
        }
        private bool IsInPermitedMenuList(int menuId)
        {
            if (_permittedMenuIdList == null)
            {
                if (CurrentMember != null) _permittedMenuIdList = Facade.GetAllMemberPrivilegeIdsByMemberId(CurrentMember.Id);
            }

            if ((_permittedMenuIdList != null) && (_permittedMenuIdList.Count > 0))
            {
                if (_permittedMenuIdList.Contains(menuId))
                {
                    return true;
                }
            }

            return false;
        }

        private void SelectJobStatusList()
        {
            if (Request.Cookies["RequisitionStatus"] != null)
            {
                ddlJobStatus.SelectedItems = Request.Cookies["RequisitionStatus"].Value;
            }
            else
            {
                foreach (ListItem item in ddlJobStatus.ListItem.Items)
                {
                    item.Selected = false;
                }
            }
        }

        private void PrepareView()
        {


            MiscUtil.PopulateRequistionStatus(ddlJobStatus.ListItem, Facade);
            SelectJobStatusList();
            // if (Request.Cookies["RequisitionStatus"] != null)
            //   ddlJobStatus.ListItem .SelectedValue = Request.Cookies["RequisitionStatus"].Value;
            //else
            //  ControlHelper.SelectListByText(ddlJobStatus.ListItem , "Open");

            MiscUtil.PopulateMemberListByRole(ddlReqCreator, ContextConstants.ROLE_EMPLOYEE, Facade);
            MiscUtil.PopulateMemberListByRole(ddlEmployee, ContextConstants.ROLE_EMPLOYEE, Facade);
            int companyStatus = (int)MiscUtil.GetCompanyStatusFromDefaultSiteSetting(SiteSetting);
            MiscUtil.PopulateClients(ddlEndClient.ListItem, companyStatus, Facade);
            //ddlEndClient.Items[0].Value = "0";
            if (SiteSetting[DefaultSiteSetting.AssociateRequisitionsWithEntitiesFromModule.ToString()].ToString() == UIConstants.AccountType_Department)
            {
                lblByEndClientsHeader.Text = "BU";
                //ddlEndClient.Items[0].Text = "Select BU";
            }
            else if (SiteSetting[DefaultSiteSetting.AssociateRequisitionsWithEntitiesFromModule.ToString()].ToString() == UIConstants.AccountType_Client)
            {
                lblByEndClientsHeader.Text = "Account";
                //ddlEndClient.Items[0].Text = "Select Account";
            }
            else if (SiteSetting[DefaultSiteSetting.AssociateRequisitionsWithEntitiesFromModule.ToString()].ToString() == UIConstants.AccountType_Vendor)
            {
                lblByEndClientsHeader.Text = "Vendor";
                //ddlEndClient.Items[0].Text = "Select Vendor";
            }

        }


        #endregion

        #region Events

        #region Page Events
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            string pagesize = "";
            if (RequisitionType == "My")
            {
                pagesize = (Request.Cookies["MyRequisitionListRowPerPage"] == null ? "" : Request.Cookies["MyRequisitionListRowPerPage"].Value); ;
            }
            else
            {
                pagesize = (Request.Cookies["MasterRequisitionListRowPerPage"] == null ? "" : Request.Cookies["MasterRequisitionListRowPerPage"].Value); ;
            }
            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvJobPosting.FindControl("pagerControl");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)PagerControl.FindControl("pager");
                if (pager != null)
                {
                    pager.PageSize = Convert.ToInt32(pagesize == "" ? "20" : pagesize);
                    if (pager.Page.IsPostBack)
                        pager.SetPageProperties(0, pager.MaximumRows, true);
                }
            }

            this.lsvJobPosting.DataBind();
            uclCountryState.SelectedCountryId = uclCountryState.SelectedCountryId;
            uclCountryState.SelectedStateId = uclCountryState.SelectedStateId;
            AddHiringMatrixColumns();

            Response.Cookies["RequisitionStatus"].Value = ddlJobStatus.SelectedItems;
        }
        protected void btnClear_Click(object sender, EventArgs e)
        {
            txtJobTitle.Text = "";
            txtReqCode.Text = "";
            foreach (ListItem item in ddlJobStatus.ListItem.Items)
            {
                item.Selected = false;
            }
            Response.Cookies["RequisitionStatus"].Value = ddlJobStatus.SelectedItems;
            dtPstedDate.ClearRange();
            ddlReqCreator.SelectedIndex = 0;
            ddlEmployee.SelectedIndex = 0;
            //Changes Start By Vishal Tripathy
            //txtCity.Text = "";
            //Changes End By Vishal Tripathy

            uclCountryState.SelectedCountryId = 0;
            uclCountryState.SelectedStateId = 0;
            //ddlEndClient.SelectedIndex = 0;
            foreach (ListItem item in ddlEndClient.ListItem.Items)
            {
                item.Selected = false;
            }
            lsvJobPosting.DataBind();
            string pagesize = "";
            if (RequisitionType == "My")
            {
                pagesize = (Request.Cookies["MyRequisitionListRowPerPage"] == null ? "" : Request.Cookies["MyRequisitionListRowPerPage"].Value); ;
            }
            else
            {
                pagesize = (Request.Cookies["MasterRequisitionListRowPerPage"] == null ? "" : Request.Cookies["MasterRequisitionListRowPerPage"].Value); ;
            }
            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvJobPosting.FindControl("pagerControl");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)PagerControl.FindControl("pager");
                if (pager != null)
                {
                    pager.PageSize = Convert.ToInt32(pagesize == "" ? "20" : pagesize);
                }
            }
            AddHiringMatrixColumns();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            roleName = Facade.GetCustomRoleNameByMemberId(CurrentMember.Id);
            ddlJobStatusCommon = new DropDownList();
            MiscUtil.PopulateRequistionStatus(ddlJobStatusCommon, Facade);
            if (hdnDoPostPack.Value == "1")
            {
                lsvJobPosting.DataBind();
                hdnDoPostPack.Value = "0";
                return;
            }
            string testUrl = HttpContext.Current.Request.Url.Authority;
            ddlReqCreator.Attributes.Add("onmouseover", "this.title=this.options[this.selectedIndex].title");
            ddlEndClient.Attributes.Add("onmouseover", "this.title=this.options[this.selectedIndex].title");
            ddlEmployee.Attributes.Add("onmouseover", "this.title=this.options[this.selectedIndex].title");
            this.Page.MaintainScrollPositionOnPostBack = true;
            if (Page.Request.Url.AbsoluteUri.ToString().Contains("MasterRequisitionList.aspx")) { RequisitionType = "Master"; }
            else { RequisitionType = "My"; }
            //  AjaxControlToolkit.Utility.SetFocusOnLoad(lnkAddJobPosting);
            if (CurrentMember != null)
            {
                ArrayList Privilege = Facade.GetAllMemberPrivilegeIdsByMemberId(CurrentMember.Id);
                if (Privilege.Contains(56))
                    IsAccss = true;
                else IsAccss = false;
            }

            if (!IsPostBack)
            {
                PrepareView();

                string message = Helper.Url.SecureUrl[UrlConstants.PARAM_MSG];

                // ControlHelper.SetHyperLink(lnkAddJobPosting, UrlConstants.Requisition.JOB_POSTING_EDITOR_PAGE, string.Empty, "[ Add Requisition ]");

                if (!StringHelper.IsBlank(message))
                {
                    MiscUtil.ShowMessage(lblMessage, message, false);
                }
                hdnSortColumn.Value = "btnPostedDate";
                hdnSortOrder.Value = "DESC";
                PlaceUpDownArrow();
            }
            string pagesize = "";
            if (RequisitionType == "My")
            {
                pagesize = (Request.Cookies["MyRequisitionListRowPerPage"] == null ? "" : Request.Cookies["MyRequisitionListRowPerPage"].Value); ;
            }
            else
            {
                pagesize = (Request.Cookies["MasterRequisitionListRowPerPage"] == null ? "" : Request.Cookies["MasterRequisitionListRowPerPage"].Value); ;
            }
            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvJobPosting.FindControl("pagerControl");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)PagerControl.FindControl("pager");
                if (pager != null)
                {
                    pager.PageSize = Convert.ToInt32(pagesize == "" ? "20" : pagesize);
                }
            }
            if (Facade.GetCustomRoleNameByMemberId(CurrentMember.Id) == ContextConstants.ROLE_DEPARTMENT_CONTACT)
            {
                divBU.Visible = false;
            }
            AddHiringMatrixColumns();
            MemberPrivilege();
        }

        private void PlaceUpDownArrow()
        {
            try
            {
                LinkButton lnk = (LinkButton)lsvJobPosting.FindControl(hdnSortColumn.Value);
                System.Web.UI.HtmlControls.HtmlTableCell im = (System.Web.UI.HtmlControls.HtmlTableCell)lnk.Parent;
                im.EnableViewState = false;
                if (lnk.CommandArgument.Contains("[1]") || lnk.CommandArgument.Contains("[JMC].[ManagersCount]"))
                {
                    im.Attributes.Add("class", (hdnSortOrder.Value == "ASC" ? "Descending" : "Ascending"));
                }
                else
                    im.Attributes.Add("class", (hdnSortOrder.Value == "ASC" ? "Ascending" : "Descending"));
            }
            catch
            {
            }

        }
        #endregion

        #region ListView Events

        private void AddHiringMatrixColumns()
        {
            System.Web.UI.HtmlControls.HtmlTableCell thLevels = (System.Web.UI.HtmlControls.HtmlTableCell)this.lsvJobPosting.FindControl("thLevels");
            if (thLevels != null)
            {
                //thLevels.Controls.Clear();
                //IList<HiringMatrixLevels> hiringMatrixLevels = Facade.GetAllHiringMatrixLevels();
                //hiringLevels = hiringMatrixLevels;
                //int ColLocation = 0;

                //if (hiringMatrixLevels != null)
                //{
                //    if (hiringMatrixLevels.Count > 0)
                //    {
                //        foreach (HiringMatrixLevels levels in hiringMatrixLevels)
                //        {
                //            System.Web.UI.HtmlControls.HtmlTableCell col = new System.Web.UI.HtmlControls.HtmlTableCell("TH");
                //            LinkButton lnkLevel = new LinkButton();
                //            lnkLevel.Text = levels.Name.Trim();
                //            lnkLevel.CommandName = "Sort";
                //            lnkLevel.ID = "thLevel" + ColLocation;
                //            LinkButton lnktemp = (LinkButton)lsvJobPosting.FindControl(lnkLevel.ID);
                //            if (lnktemp != null) lsvJobPosting.Controls.Remove(lnktemp);
                //            lnkLevel.EnableViewState = false;
                //            lnkLevel.CommandArgument = "Level[" + levels.Id + "]";
                //            lnkLevel.EnableViewState = false;
                //            if (levels.SortingOrder == 0)
                //                thLevels.Controls.AddAt(ColLocation, lnkLevel);
                //            else
                //            {
                //                col.Controls.Add(lnkLevel);
                //                thLevels.Controls.AddAt(ColLocation, col);
                //            }
                //            ColLocation++;
                //        }
                //    }
                //}
                //System.Web.UI.HtmlControls.HtmlTableCell tdPager = (System.Web.UI.HtmlControls.HtmlTableCell)this.lsvJobPosting.FindControl("tdPager");
                //tdPager.ColSpan = Convert.ToInt32(tdPager.ColSpan) + hiringMatrixLevels.Count;
            }
        }
        public bool isMemberinHinringTeam(int memberid, int jobpostingid)
        {
            bool returnValue = false;

            JobPostingHiringTeam team = Facade.GetJobPostingHiringTeamByMemberId(memberid, jobpostingid);

            if (team != null)
            {
                returnValue = true;
            }
            else
            {
                returnValue = false;
            }

            return returnValue;
        }
        protected void lsvJobPosting_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            bool _isAdmin = base.IsUserAdmin;
            bool _isHiringManager = base.IsHiringManager;
            bool isAssingned = false;
      
            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                JobPosting jobPosting = ((ListViewDataItem)e.Item).DataItem as JobPosting;
                int ColLocation = 0;
                if (jobPosting != null)
                {
                    JobPostingHiringTeam team = Facade.GetJobPostingHiringTeamByMemberId(base.CurrentMember.Id, jobPosting.Id);
                    if (team != null) isAssingned = true;

                    //JobPostingHiringTeam ApprovalUser = Facade.GetJobPostingHiringTeamByMemberId(base.CurrentMember.Id, jobPosting.Id);
                    //if (ApprovalUser != null) isAssingned = true;

                    ColLocation = 0;
                    System.Web.UI.HtmlControls.HtmlTableCell tdLevels = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdLevels");
                    int i = 0;
                    LinkButton lnkTeamCount = (LinkButton)e.Item.FindControl("lnkTeamCount");
                    lnkTeamCount.Text = jobPosting.NoOfAssignedManagers.ToString();
                    lnkTeamCount.CommandArgument = jobPosting.Id.ToString();

                    lnkTeamCount.OnClientClick = "EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/JobPostingTeam.aspx?JID=" + jobPosting.Id + "','700px','570px'); return false;";
                    System.Web.UI.HtmlControls.HtmlTableCell thReqCode = (System.Web.UI.HtmlControls.HtmlTableCell)lsvJobPosting.FindControl("thReqCode");
                    System.Web.UI.HtmlControls.HtmlTableCell tdJobCode = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdJobCode");

                    // Label lblClientJobId = (Label)e.Item.FindControl("lblClientJobId");
                    HyperLink tdLevel = (HyperLink)e.Item.FindControl("tdLevel");
                    Hiring_Levels lev = jobPosting.HiringMatrixLevels[0];
                    tdLevel.Text = lev.CandidateCount.ToString();
                    // tdLevel.Target = "_blank";
                    HiringMatrixLevels l = Facade.HiringMatrixLevel_GetInitialLevel();// (lev.HiringMatrixLevelID);
                    HyperLink hlkOpenHiringMatrix = (HyperLink)e.Item.FindControl("hlkOpenHiringMatrix");

                    if (_isAdmin || jobPosting.CreatorId == base.CurrentMember.Id || isAssingned || _isHiringManager)
                    {
                        ControlHelper.SetHyperLink(tdLevel, UrlConstants.ATS.ATS_PRESELECTED_INTERVIEW_HIRINGMATRIX, string.Empty, lev.CandidateCount.ToString(), UrlConstants.PARAM_JOB_ID, StringHelper.Convert(jobPosting.Id), UrlConstants.PARAM_TAB, l.Id.ToString());
                        SecureUrl urlh = UrlHelper.BuildSecureUrl("../" + UrlConstants.ATS.ATS_PRESELECTED_INTERVIEW_HIRINGMATRIX.Replace("~/", ""), string.Empty, UrlConstants.PARAM_JOB_ID, StringHelper.Convert(jobPosting.Id), UrlConstants.PARAM_TAB, l.Id.ToString());
                        hlkOpenHiringMatrix.Attributes.Add("href", urlh.ToString());
                    }
                    else tdLevel.Enabled = false;// lnkLevel.Enabled = false;

                    //string role1 = Facade.GetCustomRoleNameByMemberId(base.CurrentMember.Id);
                    //if(role1 == "Hiring Manager")
                    //{
                    //    tdLevel.Enabled = false;
                    //    hlkOpenHiringMatrix.Visible = false;
                    //}

                    DropDownList ddlJobStatus = (DropDownList)e.Item.FindControl("ddlJobStatus");
                    Label lblJobStatus = (Label)e.Item.FindControl("lblJobStatus");
                    ddlJobStatus.Enabled = false;
                    GenericLookup _RequisitionStatusLookup = Facade.GetGenericLookupById(jobPosting.JobStatus);
                    if (_RequisitionStatusLookup != null) lblJobStatus.Text = _RequisitionStatusLookup.Name;

                    if (lblJobStatus.Text != "Draft")
                    {
                        if (jobPosting.ApprovalStatus == "Requisition Approved")
                        {
                            lblJobStatus.Text = "Open";                         
                            ddlJobStatus.DataValueField = ddlJobStatusCommon.DataValueField;
                            ddlJobStatus.DataTextField = ddlJobStatusCommon.DataTextField;
                            ddlJobStatus.DataSource = ddlJobStatusCommon.DataSource;
                            ddlJobStatus.DataBind();
                            ControlHelper.SelectListByValue(ddlJobStatus, jobPosting.JobStatus.ToString());
                            ddlJobStatus.Attributes.Add("onchange", "javascript:RequisitionJobStatusChanged('" + ddlJobStatus.ClientID + "','" + jobPosting.Id + "','" + base.CurrentMember.Id + "','" + lblMessage.ClientID + "','" + jobPosting.JobTitle + "')");
                            lblJobStatus.Visible = false;
                            ddlJobStatus.Visible = true;
                        }
                        else
                        {
                            //jobPosting.JobStatus = 1318;
                            lblJobStatus.Text = "Approval In Progress";                      
                            ddlJobStatus.DataValueField = ddlJobStatusCommon.DataValueField;
                            ddlJobStatus.DataTextField = ddlJobStatusCommon.DataTextField;
                            ddlJobStatus.DataSource = ddlJobStatusCommon.DataSource;
                            ddlJobStatus.DataBind();
                            ControlHelper.SelectListByValue(ddlJobStatus, jobPosting.JobStatus.ToString());
                            ddlJobStatus.Attributes.Add("onchange", "javascript:RequisitionJobStatusChanged('" + ddlJobStatus.ClientID + "','" + jobPosting.Id + "','" + base.CurrentMember.Id + "','" + lblMessage.ClientID + "','" + jobPosting.JobTitle + "')");
                            lblJobStatus.Visible = false;
                            ddlJobStatus.Visible = true;
                        }
                    }
                    else
                    {
                        lblJobStatus.Text = "Draft";
                        ddlJobStatus.DataValueField = ddlJobStatusCommon.DataValueField;
                        ddlJobStatus.DataTextField = ddlJobStatusCommon.DataTextField;
                        ddlJobStatus.DataSource = ddlJobStatusCommon.DataSource;
                        ddlJobStatus.DataBind();
                        ControlHelper.SelectListByValue(ddlJobStatus, jobPosting.JobStatus.ToString());
                        ddlJobStatus.Attributes.Add("onchange", "javascript:RequisitionJobStatusChanged('" + ddlJobStatus.ClientID + "','" + jobPosting.Id + "','" + base.CurrentMember.Id + "','" + lblMessage.ClientID + "','" + jobPosting.JobTitle + "')");
                        lblJobStatus.Visible = false;
                        ddlJobStatus.Visible = true;
                    }
                   
                    Label lblPostedDate = (Label)e.Item.FindControl("lblPostedDate");
                    Label lblApprovalStatus = (Label)e.Item.FindControl("lblApprovalStatus");
                    if (lblJobStatus.Text == "Draft")
                    {
                        lblPostedDate.Text = "";
                        lblApprovalStatus.Text = "Requisition Drafted";
                    }
                    else
                    {
                        lblPostedDate.Text = jobPosting.PostedDate.ToShortDateString();
                        lblApprovalStatus.Text = jobPosting.ApprovalStatus;
                        if (ddlJobStatus.SelectedItem.Text == "Close")
                        {
                            lblApprovalStatus.Text = "Close - " + jobPosting.ApprovalStatus;
                        }
                        else if (ddlJobStatus.SelectedItem.Text == "Cancel")
                        {
                            lblApprovalStatus.Text = "Cancel - " + jobPosting.ApprovalStatus;
                        }
                        else if (ddlJobStatus.SelectedItem.Text == "Put On-Hold")
                        {
                            lblApprovalStatus.Text = "Put On-Hold - " + jobPosting.ApprovalStatus;
                        }
                        //lblApprovalStatus.Text = (jobPosting.ApprovalStatus.Trim().ToLower() == "Requisition Approved") ? "Requisition Approved" : jobPosting.ApprovalStatus;
                    }

                    //if ((jobPosting.AllowRecruitersToChangeStatus == true && isAssingned) || jobPosting.CreatorId == base.CurrentMember.Id)
                    //{
                    //    ddlJobStatus.DataValueField = ddlJobStatusCommon.DataValueField;
                    //    ddlJobStatus.DataTextField = ddlJobStatusCommon.DataTextField;
                    //    ddlJobStatus.DataSource = ddlJobStatusCommon.DataSource;
                    //    ddlJobStatus.DataBind();
                    //    ControlHelper.SelectListByValue(ddlJobStatus, jobPosting.JobStatus.ToString());

                    //    ddlJobStatus.Attributes.Add("onchange", "javascript:RequisitionJobStatusChanged('" + ddlJobStatus.ClientID + "','" + jobPosting.Id + "','" + base.CurrentMember.Id + "','" + lblMessage.ClientID + "','" + jobPosting.JobTitle + "')");
                    //    lblJobStatus.Visible = false;
                    //    ddlJobStatus.Visible = true;
                    //}
                    //else
                    //{
                    //    ddlJobStatus.Visible = false;
                    //    lblJobStatus.Visible = true;
                    //}

                  
                    Label lblJobCode = (Label)e.Item.FindControl("lblJobCode");
                    HyperLink lnkJobTitle = (HyperLink)e.Item.FindControl("lnkJobTitle");
                    Label lblCity = (Label)e.Item.FindControl("lblCity");
                    Label lblClient = (Label)e.Item.FindControl("lblClient");       //0.4
                    Label lblNoOfOpenings = (Label)e.Item.FindControl("lblNoOfOpenings");
                    Label lblClientJobId = (Label)e.Item.FindControl("lblClientJobId");
                    Label lblPostedBy = (Label)e.Item.FindControl("lblPostedBy");
                    LinkButton btnEdit = (LinkButton)e.Item.FindControl("btnEdit");

                    //*********[Kanchan Yeware] - [Add IJP Referral Link Button] – [9-Sept-2016] – Start *****************
                    LinkButton lnkIJPReferralLink = (LinkButton)e.Item.FindControl("lnkIJPReferralLink");
                    LinkButton lnkShowInExternal = (LinkButton)e.Item.FindControl("lnkShowInExternal");
                    lnkShowInExternal.Visible = false;//added by pravin khot
                    //*********[Kanchan Yeware] - [Add IJP Referral Link Button] – [9-Sept-2016] – End *****************

                    // LinkButton btnCreateNewFromExisting = (LinkButton)e.Item.FindControl("btnCreateNewFromExisting");
                    LinkButton btnDelete = (LinkButton)e.Item.FindControl("btnDelete");
                    LinkButton lnkEmployeeReferralLink = (LinkButton)e.Item.FindControl("lnkEmployeeReferralLink");
                    // Changes start by vishal Tripathy
                    //lblClientJobId.Visible = false;
                    //lblCity.Visible = false;
                    // Changes end by vishal Tripathy
                    lblJobCode.Text = jobPosting.JobPostingCode.ToString();
                    
                    //ControlHelper.SetHyperLink(lnkJobTitle, UrlConstants.Requisition.JOB_POSTING_INTERNAL_PREVIEW_PAGE, string.Empty, MiscUtil.RemoveScript(jobPosting.JobTitle), UrlConstants.PARAM_JOB_ID, StringHelper.Convert(jobPosting.Id));
                    lnkJobTitle.Text = jobPosting.JobTitle;
                    lnkJobTitle.Attributes.Add("onclick", "EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/JobDetail.aspx?JID=" + jobPosting.Id + "&FromPage=JobDetail" + "','700px','570px'); return false;");
                    lblPostedBy.Text = jobPosting.PostedBy;

                    // lblCity.Text = jobPosting.City + ((!string.IsNullOrEmpty(jobPosting.City) && jobPosting.StateId > 0) ? ", " : string.Empty) + MiscUtil.GetStateNameById(jobPosting.StateId, Facade);
                    //GenericLookup _RequisitionStatusLookup = Facade.GetGenericLookupById(jobPosting.JobStatus);
                    //if (_RequisitionStatusLookup != null) lblJobStatus.Text = _RequisitionStatusLookup.Name;
                    ////lblClient.Text = "";
                    //{
                    //    if (jobPosting.ClientId > 0)
                    //    {
                    //        Company Client = Facade.GetCompanyById(jobPosting.ClientId);
                    //        //if (Client != null)
                    //        //    lblClient.Text = Client.CompanyName;
                    //    }
                    //}
                    System.Web.UI.HtmlControls.HtmlTableCell tdAction = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdAction");
                    System.Web.UI.HtmlControls.HtmlTableCell thAction = (System.Web.UI.HtmlControls.HtmlTableCell)lsvJobPosting.FindControl("thAction");

                    // if (!ShowAction) ShowAction = _isAdmin || jobPosting.CreatorId == CurrentMember.Id || _isHiringManager || isMemberinHinringTeam(CurrentMember.Id, jobPosting.Id);
                    //else
                    {
                        thAction.Visible = true;
                        tdAction.Visible = true;
                    }
                    if (!_isAdmin && jobPosting.CreatorId != CurrentMember.Id && !_isHiringManager)
                    {
                        btnDelete.Visible = false;
                        btnEdit.Visible = false;
                        //*********[Kanchan Yeware] - [Add IJP Referral Link Button] – [9-Sept-2016] – Start *****************
                        lnkIJPReferralLink.Visible = false;
                        lnkShowInExternal.Visible = false;
                        //btnCreateNewFromExisting.Visible = false;
                        //*********[Kanchan Yeware] - [Add IJP Referral Link Button] – [9-Sept-2016] – End *****************

                    }
                    //*********[Kanchan Yeware] - [Add IJP Referral Link Button] – [9-Sept-2016] – Start *****************
                    if (jobPosting.DisplayInIJP)
                        lnkIJPReferralLink.Attributes.Add("onclick", "EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/ReferralLink.aspx?JID=" + jobPosting.Id + "&FromPage=JobDetail" + "&Link=Internal','680px','360px'); return false;");
                    else
                    {
                        System.Web.UI.HtmlControls.HtmlContainerControl liDivider = (System.Web.UI.HtmlControls.HtmlContainerControl)e.Item.FindControl("liDivider");
                        if (liDivider != null) liDivider.Visible = false;
                        lnkIJPReferralLink.Visible = false;
                    }
                    //*********[Kanchan Yeware] - [Add IJP Referral Link Button] – [9-Sept-2016] – End *****************

                    //if (_isHiringManager)
                    //{
                    //    ShowAction = !_isHiringManager;
                    //    thAction.Visible = false;
                    //    tdAction.Visible = false;
                    //}

                    //********************************************************************
                    //Rupesh Kadam Start
                    Label lblApprovingManager = (Label)e.Item.FindControl("lblApprovingManager");
                    string ApprovingManagerName = "";
                    try
                    {
                        ApprovingManagerName = Facade.GetRequistionApprovingManager(jobPosting.Id).ToString();
                        if (ApprovingManagerName != null)
                        {
                            lblApprovingManager.Text = ApprovingManagerName;
                        }
                    }
                    catch
                    {
                        lblApprovingManager.Text = "";
                    }

                    Label lblNextApprovingManager = (Label)e.Item.FindControl("lblNextApprovingManager");
                    string NextApprovingManagerName = "";
                    try
                    {
                        NextApprovingManagerName = Facade.GetRequistionNextApprovingManager(jobPosting.Id).ToString();
                        if (NextApprovingManagerName != null)
                        {
                            lblNextApprovingManager.Text = NextApprovingManagerName;
                        }
                    }
                    catch
                    {
                        lblNextApprovingManager.Text = "";
                    }
                  

                    //Rupesh Kadam End
                    //********************************************************************
                    Label lblAssignedManager = (Label)e.Item.FindControl("lblAssignedManager");
                    Member MemberCreated = null;
                    if (jobPosting.CreatorId != 0) MemberCreated = Facade.GetMemberById(jobPosting.CreatorId);                  //0.5
                    if (MemberCreated != null)
                        lblAssignedManager.Text = MemberCreated.FirstName + " " + MemberCreated.LastName;   //0.5
                    try
                    {
                        System.Web.UI.HtmlControls.HtmlTableCell thAssigningManager = (System.Web.UI.HtmlControls.HtmlTableCell)this.lsvJobPosting.FindControl("thAssigningManager");
                        System.Web.UI.HtmlControls.HtmlTableCell tdAssigningManager = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdAssigningManager");
                        System.Web.UI.HtmlControls.HtmlTableCell thClient = (System.Web.UI.HtmlControls.HtmlTableCell)this.lsvJobPosting.FindControl("thClient");
                        System.Web.UI.HtmlControls.HtmlTableCell tdClient = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdClient");
                        if (RequisitionType == "Master")
                        {
                            thAssigningManager.Visible = false;
                            tdAssigningManager.Visible = false;
                            //thClient.Visible = false;
                            //tdClient.Visible = false;
                        }
                        else
                        {
                            thAssigningManager.Visible = true;
                            tdAssigningManager.Visible = true;
                            //thClient.Visible = false;
                            //tdClient.Visible = false;
                        }
                    }
                    catch
                    {
                    }

                     btnDelete.Visible = false; 
                    if (_isAdmin)
                    {
                        btnDelete.OnClientClick = "return ConfirmDelete('Requisition')";
                        btnDelete.Visible = false; 
                    }
                    else
                    {
                        btnDelete.Visible = false; 
                    }

                    btnEdit.CommandArgument = btnDelete.CommandArgument = StringHelper.Convert(jobPosting.Id);

                    if (jobPosting.ShowInEmployeeReferralPortal)
                    {
                         //edited by suraj for employee referral link 20161006
                        lnkEmployeeReferralLink.Attributes.Add("onclick", "EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/ReferralLink.aspx?JID=" + jobPosting.Id + "&FromPage=JobDetail&Link=Employee" + "','680px','360px'); return false;");
                        //end suraj
                    }                       
                    else
                    {
                        System.Web.UI.HtmlControls.HtmlContainerControl liDivider = (System.Web.UI.HtmlControls.HtmlContainerControl)e.Item.FindControl("liDivider");
                        if (liDivider != null) liDivider.Visible = false;
                        lnkEmployeeReferralLink.Visible = false;
                    }

                    if (!IsAccss)
                    {
                        btnEdit.Visible = false;
                        //                        btnCreateNewFromExisting.Visible = false;
                    }
                    //********************************************************************
                    //Rupesh Kadam Start
                    else
                    {
                        if (jobPosting.ApprovalStatus.ToLower() == "requisition approved")
                        {
                            btnEdit.Visible = false;
                            //*********[Kanchan Yeware] - [IJP Show for Edit in Avtion] – [5-Sept-2016] – Start *****************
                            lnkIJPReferralLink.Visible = false;
                            lnkShowInExternal.Visible = false;
                            int HRManagerID = Facade.GetHRManagerIDforIJP(CurrentMember.Id);
                            if (HRManagerID > 0)
                            {
                                btnEdit.Visible = true;
                                lnkShowInExternal.Visible = true;
                                lnkShowInExternal.CommandArgument = jobPosting.Id.ToString();

                                if (jobPosting.ApprovedForIJP)
                                    lnkIJPReferralLink.Visible = true;
                                else
                                    lnkIJPReferralLink.Visible = false;
                            }
                            int RMManagerID = Facade.GetRMManagerIDforIJP(CurrentMember.Id);
                            if (RMManagerID > 0)
                            {
                                btnEdit.Visible = true;
                            }
                            //*********[Kanchan Yeware] - [IJP Show for Edit in Avtion] – [5-Sept-2016] – END *****************
                        }
                    }
                    //Rupesh Kadam End
                    //********************************************************************
                    lblNoOfOpenings.Text = jobPosting.NoOfOpenings.ToString();

                    //lblClientJobId.Text = jobPosting.ClientJobId;
                    if (ApplicationSource == ApplicationSource.GenisysApplication)
                    {

                        thReqCode.Visible = true;
                        tdJobCode.Visible = true;

                    }

                    //*******************pravin******Start********For********30/March/2017*********//
                    LinkButton lnkCandidatePortal = (LinkButton)e.Item.FindControl("btnCandidatePortal");
                    LinkButton lnkVendorPortal = (LinkButton)e.Item.FindControl("btnVendorPortal");
                    LinkButton lnkEmployeeReferral = (LinkButton)e.Item.FindControl("btnEmployeeReferral");
                    LinkButton lnkClose = (LinkButton)e.Item.FindControl("lnkClose");
                    LinkButton lnkCancel = (LinkButton)e.Item.FindControl("lnkCancel");
                    HyperLink hlkPutOnHold = (HyperLink)e.Item.FindControl("hlkPutOnHold");

                    IList<CustomRole> CandidatePortalLists = Facade.GetCustomRoleRequisition(5);
                    IList<CustomRole> VendorPortalLists = Facade.GetCustomRoleRequisition(6);
                    IList<CustomRole> EmployeeReferralLists = Facade.GetCustomRoleRequisition(7);

                    lnkCancel.CommandArgument=lnkClose.CommandArgument = lnkCandidatePortal.CommandArgument = lnkVendorPortal.CommandArgument = lnkEmployeeReferral.CommandArgument = StringHelper.Convert(jobPosting.Id);
                    JobPosting CurrentJobPosting = Facade.GetJobPostingById(jobPosting.Id);
                    if (CurrentJobPosting != null)
                    {
                        if (CandidatePortalLists != null)
                        {
                            foreach (CustomRole role in CandidatePortalLists)
                            {
                                if (role.Name == roleName)
                                {
                                    lnkCandidatePortal.Visible = true;
                                    if (CurrentJobPosting.ShowInCandidatePortal)
                                    {
                                        lnkCandidatePortal.Text = "Disable Candidate Portal";
                                        lnkCandidatePortal.OnClientClick = "return ConfirmEnable('Disable Candidate Portal')";
                                    }
                                    else
                                    {
                                        lnkCandidatePortal.Text = "Enable Candidate Portal";
                                        lnkCandidatePortal.OnClientClick = "return ConfirmEnable('Enable Candidate Portal')";
                                    }
                                    break;
                                }
                                else
                                {
                                    lnkCandidatePortal.Visible = false;
                                }

                            }
                        }
                        if (VendorPortalLists != null)
                        {
                            foreach (CustomRole role in VendorPortalLists)
                            {
                                if (role.Name == roleName)
                                {
                                    lnkVendorPortal.Visible = true;
                                    //if (CurrentJobPosting.DisplayRequisitionInVendorPortal)
                                    //{
                                    //    lnkVendorPortal.Text = "Disable Vendor Portal";
                                    //    lnkVendorPortal.OnClientClick = "return ConfirmEnable('Disable Vendor Portal')";
                                    //    //lnkVendorPortal.Attributes.Add("onclick", "EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/DisplayRequisitionInVendorPortal.aspx?JID=" + jobPosting.Id + "&FromPage=JobDetail" + "&Link=Internal','680px','360px'); return false;");
                                    //}
                                    //else
                                    //{
                                    //    lnkVendorPortal.Text = "Enable Vendor Portal";
                                    //    //lnkVendorPortal.OnClientClick = "return ConfirmEnable('Enable Vendor Portal')";
                                    //    lnkVendorPortal.Attributes.Add("onclick", "EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/DisplayRequisitionInVendorPortal.aspx?JID=" + jobPosting.Id + "&FromPage=JobDetail" + "&Link=Internal','680px','360px'); return false;");
                                    //}
                                    lnkVendorPortal.Attributes.Add("onclick", "EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/DisplayRequisitionInVendorPortal.aspx?JID=" + jobPosting.Id + "&FromPage=JobDetail" + "&Link=Internal','680px','360px'); return false;");
                                    break;
                                }
                                else
                                {
                                    lnkVendorPortal.Visible = false;
                                }
                            }
                        }
                        if (EmployeeReferralLists != null)
                        {
                            foreach (CustomRole role in EmployeeReferralLists)
                            {
                                if (role.Name == roleName)
                                {
                                    lnkEmployeeReferral.Visible = true;
                                    if (CurrentJobPosting.ShowInEmployeeReferralPortal)
                                    {
                                        lnkEmployeeReferral.Text = "Disable Employee Referral Portal";
                                        lnkEmployeeReferral.OnClientClick = "return ConfirmEnable('Disable Employee Referral Portal')";                                    
                                    }
                                    else
                                    {
                                        lnkEmployeeReferral.Text = "Enable Employee Referral Portal";
                                        lnkEmployeeReferral.OnClientClick = "return ConfirmEnable('Enable Employee Referral Portal')";
                                    }
                                    break;
                                }
                                else
                                {
                                    lnkEmployeeReferral.Visible = false;
                                }
                            }
                        }
                    }
                    else
                    {
                        lnkCandidatePortal.Visible = false;
                        lnkVendorPortal.Visible = false;
                        lnkEmployeeReferral.Visible = false;
                    }



                    int candidatecount = 0;
                    candidatecount = Facade.GetCountOfHiringMatrixByStatusAndJobPostingId(jobPosting.Id);
                    if (_isAdmin)
                    {
                        if (lev.CandidateCount > 0)
                        {
                            if (candidatecount > 0)
                            {             
                                lnkClose.OnClientClick = "return alertmessage('Candidates added to the Requisition should be marked ‘Joined’ or ‘Rejected’ ')";   
                                lnkCancel.Attributes.Add("onclick", "EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/RequsitionStatusChange.aspx?JID=" + jobPosting.Id + "&STATUS=Cancel&ID=" + CurrentMember.Id + "','700px','350px'); return false;");
                            }
                            else
                            {            
                                lnkClose.Attributes.Add("onclick", "EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/RequsitionStatusChange.aspx?JID=" + jobPosting.Id + "&STATUS=Close&ID=" + CurrentMember.Id + "','700px','350px'); return false;");
                                lnkCancel.Attributes.Add("onclick", "EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/RequsitionStatusChange.aspx?JID=" + jobPosting.Id + "&STATUS=Cancel&ID=" + CurrentMember.Id + "','700px','350px'); return false;");
                            }
                        }
                        else
                        {                         
                            lnkClose.Attributes.Add("onclick", "EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/RequsitionStatusChange.aspx?JID=" + jobPosting.Id + "&STATUS=Close&ID=" + CurrentMember.Id + "','700px','350px'); return false;");
                            lnkCancel.Attributes.Add("onclick", "EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/RequsitionStatusChange.aspx?JID=" + jobPosting.Id + "&STATUS=Cancel&ID=" + CurrentMember.Id + "','700px','350px'); return false;");
                        }
                      
                        try
                        {
                            if (_RequisitionStatusLookup.Name == "Put On-Hold")
                            {
                                lnkCancel.Visible = false;
                                lnkClose.Visible = false;
                                lnkCandidatePortal.Visible = false;
                                lnkVendorPortal.Visible = false;
                                lnkEmployeeReferral.Visible = false;
                                hlkOpenHiringMatrix.Visible = false;
                                tdLevel.Enabled = false;
                                lnkTeamCount.Enabled = false;
                                btnEdit.Visible = false;
                                lnkEmployeeReferralLink.Visible = false;
                                lnkShowInExternal.Visible = false;

                                hlkPutOnHold.Text = "Remove On-Hold";
                                hlkPutOnHold.Attributes.Add("onclick", "EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/RequsitionStatusChange.aspx?JID=" + jobPosting.Id + "&STATUS=Remove On-Hold&ID=" + CurrentMember.Id + "','700px','430px'); return false;");
                            }
                            else
                            {
                                hlkPutOnHold.Attributes.Add("onclick", "EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/RequsitionStatusChange.aspx?JID=" + jobPosting.Id + "&STATUS=Put On-Hold&ID=" + CurrentMember.Id + "','700px','350px'); return false;");
                            }
                        }
                        catch
                        {

                        }

                        try
                        {
                            if (_RequisitionStatusLookup.Name == "Cancel" || _RequisitionStatusLookup.Name == "Close")
                            {
                                lnkCandidatePortal.Visible = false;
                                lnkVendorPortal.Visible = false;
                                lnkEmployeeReferral.Visible = false;
                                hlkOpenHiringMatrix.Visible = false;
                                tdLevel.Enabled = false;
                                lnkTeamCount.Enabled = false;
                                btnEdit.Visible = false;
                                lnkEmployeeReferralLink.Visible = false;
                                lnkShowInExternal.Visible = false;
                                lnkCancel.Visible = false;
                                lnkClose.Visible = false;
                                hlkPutOnHold.Visible = false;                              
                            }
                        }
                        catch
                        {
                        }
                    }
                    else
                    {
                        lnkCancel.Visible = false;
                        lnkClose.Visible = false;
                        hlkPutOnHold.Visible = false;
                        try
                        {
                            if (_RequisitionStatusLookup.Name == "Cancel" || _RequisitionStatusLookup.Name == "Close" || _RequisitionStatusLookup.Name == "Put On-Hold")
                            {
                                lnkCandidatePortal.Visible = false;
                                lnkVendorPortal.Visible = false;
                                lnkEmployeeReferral.Visible = false;
                                hlkOpenHiringMatrix.Visible = false;
                                tdLevel.Enabled = false;
                                lnkTeamCount.Enabled = false;
                                btnEdit.Visible = false;
                                lnkEmployeeReferralLink.Visible = false;
                                lnkShowInExternal.Visible = false;
                            }
                        }
                        catch
                        {
                        }
                    }                  
                 }
            }
        }
        protected void lsvJobPosting_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Sort")
                {
                    LinkButton lnkbutton = (LinkButton)e.CommandSource;
                    if (hdnSortColumn.Value == lnkbutton.ID)
                    {
                        if (hdnSortOrder.Value == "ASC") hdnSortOrder.Value = "DESC";
                        else hdnSortOrder.Value = "ASC";
                    }
                    else
                    {
                        hdnSortColumn.Value = lnkbutton.ID;
                        hdnSortOrder.Value = "ASC";
                    }
                }
            }
            catch
            {
            }
            int id;

            int.TryParse(e.CommandArgument.ToString(), out id);



            if (id > 0)
            {
                if (string.Equals(e.CommandName, "EditItem"))
                {
                    //Session.Add("List", 3);
                    Session.Add("List", 2); //modify by pravin khot on 17/Nov/2016
                    Helper.Url.Redirect(UrlConstants.Requisition.JOB_POSTING_EDITOR_PAGE, string.Empty, UrlConstants.PARAM_JOB_ID, StringHelper.Convert(id));
                }
                else if (string.Equals(e.CommandName, "Managers"))
                {
                    ////uclTemplateManagers.ModalTitle = uclManagers .setCurrentJobPosting (Convert .ToInt32 (e.CommandArgument )) + " - Assigned Team";
                    //uclManagers.JobPostingId = Convert.ToInt32(e.CommandArgument);
                    //uclTemplateManagers.ModalTitle = uclManagers.Job_Posting.JobTitle + " - Assigned Team";
                    //mpeManagers.Show();
                    //System.Web.UI.HtmlControls.HtmlTableCell thAction = (System.Web.UI.HtmlControls.HtmlTableCell)lsvJobPosting.FindControl("thAction");
                    //ShowAction = thAction.Visible;

                }
                else if (string.Equals(e.CommandName, "CreateNewFromExisting"))
                {
                    try
                    {
                        JobPosting jobPosting = new JobPosting();
                        int intReqCount = Facade.GetLastRequisitionCodeByMemberId(CurrentMember.Id);
                        string strMemberJobCount = "";
                        if (strMemberJobCount.Length < 4)
                            strMemberJobCount = (intReqCount + 1).ToString("#0000");
                        else strMemberJobCount = ((intReqCount + 1)).ToString();

                        string jobPostingCode = (base.CurrentMember.LastName.IsNullOrEmpty() ? (Convert.ToString(base.CurrentMember.FirstName.Substring(0, 2))) : ((Convert.ToString(base.CurrentMember.FirstName.Substring(0, 1)) + Convert.ToString(base.CurrentMember.LastName.Substring(0, 1))))).ToUpper() + strMemberJobCount;
                        jobPosting = Facade.CreateJobPostingFromExistingJob(id, jobPostingCode, base.CurrentMember.Id);//0.1 and 1.2
                        IList<JobPostingHiringTeam> JPHTT = Facade.GetAllJobPostingHiringTeamByJobPostingId(id);
                        bool IsCurrentMember = false;
                        if (JPHTT != null)
                        {
                            for (int i = 0; i < JPHTT.Count; i++)
                            {
                                if (JPHTT[i].MemberId == base.CurrentMember.Id)
                                {
                                    IsCurrentMember = true;
                                }
                            }
                            for (int i = 0; i < JPHTT.Count; i++)
                            {
                                JobPostingHiringTeam JPHT = new JobPostingHiringTeam();
                                JPHT.MemberId = JPHTT[i].MemberId;
                                JPHT.JobPostingId = jobPosting.Id;
                                JPHT.UpdateDate = System.DateTime.Now;
                                JPHT.UpdatorId = base.CurrentMember.Id;
                                JPHT.CreateDate = System.DateTime.Now;
                                JPHT.EmployeeType = "Internal";
                                JPHT.CreatorId = base.CurrentMember.Id;
                                Facade.AddJobPostingHiringTeam(JPHT);
                                MemberJobCart memberJobCart = new MemberJobCart();
                                memberJobCart.MemberId = base.CurrentMember.Id;
                                memberJobCart.JobPostingId = jobPosting.Id;
                                memberJobCart.ApplyDate = DateTime.Now;
                                Facade.AddMemberJobCart(memberJobCart);
                                Facade.UpdateMemberRequisitionCount(base.CurrentMember.Id, intReqCount + 1);
                                if (!IsCurrentMember)
                                {
                                    JobPostingHiringTeam JPHT1 = new JobPostingHiringTeam();
                                    JPHT1.MemberId = base.CurrentMember.Id;
                                    JPHT1.JobPostingId = jobPosting.Id;
                                    JPHT1.UpdateDate = System.DateTime.Now;
                                    JPHT1.UpdatorId = base.CurrentMember.Id;
                                    JPHT1.CreateDate = System.DateTime.Now;
                                    JPHT1.EmployeeType = "Internal";
                                    JPHT1.CreatorId = base.CurrentMember.Id;
                                    Facade.AddJobPostingHiringTeam(JPHT1);

                                    MemberJobCart memberJobCart1 = new MemberJobCart();
                                    memberJobCart1.MemberId = base.CurrentMember.Id;
                                    memberJobCart1.JobPostingId = jobPosting.Id;
                                    memberJobCart1.ApplyDate = DateTime.Now;
                                    Facade.AddMemberJobCart(memberJobCart1);
                                    Facade.UpdateMemberRequisitionCount(base.CurrentMember.Id, intReqCount + 1);
                                }
                            }
                        }
                        if (jobPosting != null)
                            Helper.Url.Redirect(UrlConstants.Requisition.JOB_POSTING_EDITOR_PAGE, string.Empty, UrlConstants.PARAM_JOB_ID, StringHelper.Convert(jobPosting.Id));
                    }
                    catch (ArgumentException ex)
                    {
                        MiscUtil.ShowMessage(lblMessage, ex.Message, true);
                    }
                }
                //else if (string.Equals(e.CommandName, "DeleteItem"))
                //{
                //    try
                //    {
                //        if (Facade.DeleteJobPostingById(id, base.CurrentMember.Id))
                //        {
                //            BindList();
                //            MiscUtil.ShowMessage(lblMessage, "Requisition has been successfully deleted.", false);
                //        }
                //        Facade.DeleteMemberJobCartById(base.CurrentMember.Id);
                //    }
                //    catch (ArgumentException ex)
                //    {
                //        MiscUtil.ShowMessage(lblMessage, ex.Message, true);
                //    }
                //}
                //*********[Kanchan Yeware] - [IJP Show for External Link in Action] – [9-Sept-2016] – Start *****************
                else if (string.Equals(e.CommandName, "ExternalItem"))
                {
                    JobPosting jobPosting = new JobPosting();
                    jobPosting.Id = id;
                    jobPosting.DisplayInExternal = true;
                    string roleName = Facade.GetCustomRoleNameByMemberId(CurrentMember.Id);
                    if (roleName.ToLower() == "hr manager")
                    {
                        jobPosting = Facade.UpdateJobPostingForIJPApproval(jobPosting);

                        //Added by pravin khot on 19/Jan/2016**********
                        string AdminEmailId = string.Empty;
                        int AdminMemberid = 0;
                        SiteSetting siteSetting = Facade.GetSiteSettingBySettingType((int)SettingType.SiteSetting);
                        if (siteSetting != null)
                        {
                            Hashtable siteSettingTable = ObjectEncrypter.Decrypt<Hashtable>(siteSetting.SettingConfiguration);
                            AdminEmailId = siteSettingTable[DefaultSiteSetting.AdminEmail.ToString()].ToString();
                            AdminMemberid = Facade.GetMemberIdByEmail(AdminEmailId);
                        }
                        //**********************END*************************
                        Facade.SendRequsitionApprovalEmail(base.CurrentMember.Id , jobPosting.Id, "Requsition Submitted RM", "", jobPosting.JobSkillLookUpId, GetFullRootUrl(), AdminMemberid);  //sendRequsitionApprovalEmail
                    }
                    BindList();
                    MiscUtil.ShowMessage(lblMessage, "Requisition has been successfully Forwarded to Talent Aquisition Team.", false);
                }
                //*********[Kanchan Yeware] - [IJP Show for External Link in Action] – [9-Sept-2016] – End *****************

                else if (string.Equals(e.CommandName, "CandidatePortal"))
                {
                    try
                    {
                        //JobPosting js = Facade.GetJobPostingById(id);
                        if (Facade.JobPostingUpdatePortal_ById(id, base.CurrentMember.Id, 1))
                        {                            
                            BindList();
                            //if (js.ShowInEmployeeReferralPortal)
                            //{
                            //    MiscUtil.ShowMessage(lblMessage, "Requisition has been successfully Added to CandidatePortal.", false);
                            //}
                            //else
                            //{
                            //    MiscUtil.ShowMessage(lblMessage, "Requisition has been successfully deleted.", false);
                            //}                            
                        }
                    }
                    catch (ArgumentException ex)
                    {
                        MiscUtil.ShowMessage(lblMessage, ex.Message, true);
                    }
                }
                else if (string.Equals(e.CommandName, "VendorPortal"))
                {
                    try
                    {
                        if (Facade.JobPostingUpdatePortal_ById(id, base.CurrentMember.Id, 2))
                        {
                            BindList();
                        }             
                    }
                    catch (ArgumentException ex)
                    {
                        MiscUtil.ShowMessage(lblMessage, ex.Message, true);
                    }
                }

                //else if (string.Equals(e.CommandName, "Close"))
                //{
                //    try
                //    {
                //        MiscUtil.ShowMessage(lblMessage, "jnjnjjjnjnjnj", true);
                //    }
                //    catch (ArgumentException ex)
                //    {
                //        MiscUtil.ShowMessage(lblMessage, ex.Message, true);
                //    }
                //}

                //else if (string.Equals(e.CommandName, "OnHold"))
                //{
                //    try
                //    {
                //        LinkButton lnkOnHold = (LinkButton)e.Item.FindControl("btnOnHold");
                //        lnkOnHold.Attributes.Add("onclick", "EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/RequsitionStatusChange.aspx?JID=" + id + "&STATUS=Remove On-Hold&ID=" + CurrentMember.Id + "','700px','350px'); return false;");

                //        //ddlVendorContact.Attributes.Add("onchange", "return Contact_OnChange('" + ddlVendorContact.ClientID + "','" + hdntemp.ClientID + "')");
                //        //lnkOnHold.Attributes.Add("onclick", "EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/RequsitionStatusChange.aspx?JID=" + jobPosting.Id + "&STATUS=Remove On-Hold&ID=" + CurrentMember.Id + "','700px','350px'); return false;");
                //    }
                //    catch (ArgumentException ex)
                //    {
                //        MiscUtil.ShowMessage(lblMessage, ex.Message, true);
                //    }
                //}
                else if (string.Equals(e.CommandName, "EmployeeReferral"))
                {
                    try
                    {
                        if (Facade.JobPostingUpdatePortal_ById(id, base.CurrentMember.Id, 3))
                        {
                            BindList();
                        }
                    }
                    catch (ArgumentException ex)
                    {
                        MiscUtil.ShowMessage(lblMessage, ex.Message, true);
                    }
                }
            }
        }
        private string GetFullRootUrl()
        {
            System.Web.HttpRequest request = System.Web.HttpContext.Current.Request;
            return request.Url.Scheme + "://" + request.Url.Authority + request.ApplicationPath + "//Login.aspx";
        }
        protected void lsvJobPosting_PreRender(object sender, EventArgs e)
        {
            try
            {
                ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvJobPosting.FindControl("pagerControl");
                if (PagerControl != null)
                {
                    DataPager pager = (DataPager)PagerControl.FindControl("pager");
                    if (pager != null)
                    {
                        DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                        if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
                    }

                    HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
                    if (hdnRowPerPageName != null)
                    {
                        if (RequisitionType == "My") hdnRowPerPageName.Value = "MyRequisitionListRowPerPage";
                        else hdnRowPerPageName.Value = "MasterRequisitionListRowPerPage";
                    }
                    if (RequisitionType == "Master")
                    {                        
                        System.Web.UI.HtmlControls.HtmlTableCell tdpager = (System.Web.UI.HtmlControls.HtmlTableCell)lsvJobPosting.FindControl("tdPager");
                        tdpager.ColSpan = 12;
                    }
                    else
                    {                       
                        System.Web.UI.HtmlControls.HtmlTableCell tdpager = (System.Web.UI.HtmlControls.HtmlTableCell)lsvJobPosting.FindControl("tdPager");
                        tdpager.ColSpan = 13;               
                    }
                }
                //if (ApplicationSource == ApplicationSource.GenisysApplication)
                //{
                //    System.Web.UI.HtmlControls.HtmlTableCell tdpager = (System.Web.UI.HtmlControls.HtmlTableCell)lsvJobPosting.FindControl("tdPager");
                //    //******************************************************************
                //    //Rupesh Kadam Start
                //    //tdpager.ColSpan = 10;
                //    tdpager.ColSpan = 12;
                //    //Rupesh Kadam End
                //    //******************************************************************

                //}
            }
            catch
            {
            }
            PlaceUpDownArrow();
            if (SiteSetting[DefaultSiteSetting.ApplicationEdition.ToString()].ToString() == UIConstants.INTERNAL_HIRING)
            {
                LinkButton lnkClient = (LinkButton)lsvJobPosting.FindControl("lnkClient");
                if (lnkClient != null)
                {
                    if (SiteSetting[DefaultSiteSetting.AssociateRequisitionsWithEntitiesFromModule.ToString()].ToString() == UIConstants.AccountType_Client)
                        lnkClient.Text = "Account";
                    else if (SiteSetting[DefaultSiteSetting.AssociateRequisitionsWithEntitiesFromModule.ToString()].ToString() == UIConstants.AccountType_Department)
                        lnkClient.Text = "BU";
                    else lnkClient.Text = "Vendor";
                }
            }
        }
        protected void odsRequisitionList_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            if (RequisitionType == "Master")
            {
                ShowAction = false;
                System.Web.UI.HtmlControls.HtmlTableCell thAction = (System.Web.UI.HtmlControls.HtmlTableCell)lsvJobPosting.FindControl("thAction");
                thAction.Visible = false;
            }
        }

        #endregion

        #endregion
    }
}