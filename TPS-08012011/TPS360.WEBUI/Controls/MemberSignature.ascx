﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MemberSignature.ascx.cs"
    Inherits="TPS360.Web.UI.ControlsMemberSignature" %>
<%@ Register Src="~/Controls/HtmlEditor.ascx" TagName="HtmlEditor" TagPrefix="ucl" %>
<%@ Register Assembly="Infragistics2.WebUI.WebHtmlEditor.v7.3, Version=7.3.20073.38, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.WebUI.WebHtmlEditor" TagPrefix="ighedit" %>
<div style="width: 100%;">
    <div class="TableRow" style="text-align: center">
        <asp:Label ID="lblMessage" runat="server"></asp:Label>
        <asp:HiddenField ID="hfMemberId" runat="server" Value="0" />
        <asp:HiddenField ID="hfMemberUserId" runat="server" />
        <asp:HiddenField ID="hfMemberLogoId" runat="server" />
        <asp:HiddenField ID="hfMemberLogoName" runat="server" />
        <asp:HiddenField ID="hfActiveLogoId" runat="server" Value="0" />
    </div>
    <div class="TabPanelHeader" style="margin-bottom: 5px;">
        My Email Signature
    </div>
    <div class="TableRow" style="width: 100%;">
        <div class="TableFormContent">
            <ucl:HtmlEditor ID="txtSignature" runat="server" />
        </div>
    </div>
    <div class="TableRow" style="width: 100%;">
        <asp:CheckBox ID="chkActive" runat="server" Text="Active/ Use in all Emails" />
    </div>
    <div class="TableRow" style="width: 100%;">
        <div class="TableFormContent" style="text-align: center">
            <asp:Button ID="btnSave" runat="server" CssClass="CommonButton" Text="Save" OnClick="btnSave_Click" />
        </div>
    </div>
</div>
