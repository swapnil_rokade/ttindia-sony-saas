﻿/*<%-- 
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: CandidateOverviewHeader.ascx.cs
    Description: This is the user control page used to display the ooption like ToDo,PDF,Print and many more
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.         Date              Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1             Oct-2-2008        Gopala Swamy J      Defect id: 8914; supllied correct Path to  UrlPdf Variable
    0.2             Oct-20-2008       Gopala Swamy J      Defect id: 8991; changed the From "Hour to salary " to " Yearls" salary 
    0.3             Dec-10-2008       Shivanand           Defect id: 9494; 
                                                                In the method LoadCandidateBasicData(), label "lblSSN" is popuplated 
                                                                from currentCandidateDetail object.
                                                                In the method LoadConsultantBasicData(), label "lblSSN" is popuplated 
                                                                from currentConsultantDetail object.
    0.4             Dec-19-2008       Yogeesh Bhat        Defect Id: 9534: Changes made in LoadConsultantBasicData() method
    0.5             Dec-24-2008       Yogeesh Bhat        Defect Id: 9602: Changes made in Page_Load()method; Made "ResumeShare" link visible false.
    0.6             Jan-13-2009       Jagadish            Defect Id: 9680; Commented the code that use hyperlink 'linkLastActivities'.
    0.7             Jan-15-2009       Jagadish            Defect ID: 9690,9729: Changed the hyperlink 'lnkJobs' to lable.
    0.8             Jan-19-2009       Jagadish            Defect ID: 9682: Made the visibility os the button 'btnSendHRMS' false if login user is not a 'Super admin'
    0.9             Feb-16-2009       Kalyani pallagani   Defect ID: 9885: Added LoadMemberVideo() method to load video resume for both candidate and consultant
    1.0             Mar-03-2009       Jagadish            Defect id: 10038; Changes made in the method 'Page_Load()'.
 *  1.1             Apr-16-2009       Rajendra            Defect id: 10308; Changes made in the Show error Message().
    1.2             Jul-08-2009       Veda                Defect id: 10845: Primary Manager" link can only be accessed by the Superuser and primary manager of that candidate. 
    1.3             Aug-07-2009       Gopala Swamy J      Defect id: 11209: Changed "LoadConsultantBasicData()" and "LoadCandidateBasicData()"
 *  1.4             Nov-19-2009       Sandeesh            Enhancement#11814 :AM Notification - When a recruiter adds a candidate to a req the app will send a notification email to the Account Manager
    1.5             Dec-23-2009       Rajendra            Defect id: 12036: Added code in Page_Load(), btnSaveNotes_Click();
    1.6             apr-16-2010        Nagarathna V.B     enhance:12140: removed "btnSendHRMS" button.
 *  1.7             Apr-22-2010       Ganapati Bhat       Defect id: 12668; Code Added in LoadCandidateBasicData()
 *  1.8             Apr-22-2010       Basavaraj A         Defect 11923 , Added code to btnSaveNotes_Click , to Restrict User who is already added to same requisition
 *  1.9             May-04-2010       Ganapati Bhat       Defect id:12750 ; Added Code in LoadCandidateBasicData to avoid overlapping Firstname & Lastname 
-------------------------------------------------------------------------------------------------------------------------------------------       

--%>*/
using System;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using TPS360.Common.Helper;
using TPS360.Web.UI.Helper;
using TPS360.Common.BusinessEntities;
using TPS360.Web.UI;
using TPS360.Common.Shared;
using TPS360.Common;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Net.Mail;
using System.Net;
using System.Threading;
using System.Globalization;
using System.Web.Security;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Web;
using TPS360.BusinessFacade;


namespace TPS360.Web.UI
{
    public partial class CompanyOverviewHeader: CompanyBaseControl
    {
        #region Member Variables

        
        #endregion

        #region Property
     
        #endregion

        #region Methods
        private void populateCompanydetails()
        {
           CompanyOverviewDetails overviewdetail= Facade .GetCompanyOverviewDetails(CurrentCompanyId);
           lblCompanyName.ToolTip=lblCompanyName.Text = CurrentCompany.CompanyName;
            SecureUrl Modalurl = UrlHelper.BuildSecureUrl("../Modals/CompanyContact.aspx", string.Empty, UrlConstants.PARAM_CONTACT_ID, CurrentCompany.PrimaryContact.Id.ToString());
            lnkPrimaryContact.OnClientClick = "javascript:EditModal('" + Modalurl.ToString() + "','675px','450px'); return false;";
            lnkPrimaryContact.ToolTip=lnkPrimaryContact.Text = CurrentCompany.PrimaryContact.FirstName + " " + CurrentCompany.PrimaryContact.LastName;
            lblLastupdated.ToolTip=lblLastupdated.Text = CurrentCompany.UpdateDate.ToShortDateString();
            lblPrimarymanager.ToolTip =lblPrimarymanager.Text = overviewdetail.PrimaryManager;
            lblRequisitions.ToolTip = lblRequisitions.Text = overviewdetail.RequisitionsCount.ToString();
            lblContacts.ToolTip=lblContacts.Text = overviewdetail.ContactsCount.ToString();
            lblDocuments.ToolTip=lblDocuments.Text = overviewdetail.DocumentsCount.ToString();
            lblStartDate.ToolTip = lblStartDate.Text = CurrentCompany.ContractStartDate==DateTime.MinValue?"": CurrentCompany.ContractStartDate.ToShortDateString();
            lblEndDate.ToolTip = lblEndDate.Text = CurrentCompany.ContractEndDate == DateTime.MinValue ? "" : CurrentCompany.ContractEndDate.ToShortDateString();
            lblServiceFee.ToolTip = lblServiceFee.Text =CurrentCompany.ServiceFee==0?"": CurrentCompany.ServiceFee.ToString();
            string currentparentid = Helper.Url.SecureUrl[UrlConstants.PARAM_SITEMAP_PARENT_ID];
            ArrayList AccessArray = Facade.GetAllMemberPrivilegeIdsByMemberId(CurrentMember.Id);
            int noteid = 0;
            int contactid = 0;
            int emailid = 0;
            int assignmanagerid = 0;
            switch (CurrentCompanyStatus)
            {
                case CompanyStatus .Client :
                    noteid = 249;
                    contactid = 238;
                    emailid = 592;
                    assignmanagerid = 618;
                    break;
                case CompanyStatus .Department :
                    noteid = 630;
                    contactid = 628;
                    emailid = 632;
                    assignmanagerid = 634;
                    break;
                case CompanyStatus .Vendor :
                    assignmanagerid = 668;
                    noteid = 646;
                    contactid = 645;
                    emailid = 647;
                    break;
            }


            if (AccessArray.Contains(noteid ))
            {
                SecureUrl url = UrlHelper.BuildSecureUrl("../SFA/CompanyNote.aspx", string.Empty, UrlConstants.PARAM_SITEMAP_ID, noteid .ToString (), UrlConstants.PARAM_COMPANY_ID , CurrentCompanyId .ToString (), UrlConstants.PARAM_SITEMAP_PARENT_ID, currentparentid);
                lnkAddNote.Attributes.Add("href", url.ToString());
            }
            else lnkAddNote.Visible = false;
            if (AccessArray.Contains(contactid ))
            {

                SecureUrl url = UrlHelper.BuildSecureUrl("../SFA/CompanyContact.aspx", string.Empty, UrlConstants.PARAM_SITEMAP_ID, contactid .ToString (), UrlConstants.PARAM_COMPANY_ID, CurrentCompanyId.ToString(), UrlConstants.PARAM_SITEMAP_PARENT_ID, currentparentid);
                lnkAddContact.Attributes.Add("href", url.ToString());
            }
            else lnkAddContact.Visible = false;

            if (AccessArray.Contains(emailid ))
            {
                SecureUrl url = UrlHelper.BuildSecureUrl("../SFA/CompanyEmail.aspx", string.Empty, UrlConstants.PARAM_SITEMAP_ID, emailid .ToString (), UrlConstants.PARAM_COMPANY_ID, CurrentCompanyId.ToString(), UrlConstants.PARAM_SITEMAP_PARENT_ID, currentparentid);
                lnkSendEmail.Attributes.Add("href", url.ToString());

            }
            else
            {
                //lnkSendEmail.Attributes.Add("href", "MailTo:" + overviewdetail.prim + ">" + overviewdetail.CandidateEmail);
            }

            if (AccessArray.Contains(assignmanagerid ))
            {

                SecureUrl url = UrlHelper.BuildSecureUrl("../SFA/CompanyAssignManager.aspx", string.Empty, UrlConstants.PARAM_SITEMAP_ID, assignmanagerid .ToString (), UrlConstants.PARAM_COMPANY_ID, CurrentCompanyId.ToString(), UrlConstants.PARAM_SITEMAP_PARENT_ID, currentparentid);
                lnkPrimaryManager.Attributes.Add("href", url.ToString());
                lnkAssignManager.Attributes.Add("href", url.ToString());
            }
            else lnkAssignManager.Visible = false;


            CommonNote commonNote = Facade.GetCommonNoteByCompanyIdAndNoteCategoryLookUpId(base.CurrentCompanyId, 403);
            if (commonNote != null)
            {
                if (IsUserAdmin)
                    txtRemarksR.Text = MiscUtil.RemoveScript(commonNote.NoteDetail, string.Empty);
                else
                {
                    lblRemark.Text = "&nbsp;" + commonNote.NoteDetail;
                    lblRemark.Visible = true;
                    txtRemarksR.Visible = false;
                }
            }
            else
            {
                if (IsUserAdmin)
                {
                    txtRemarksR.Visible = true;
                    txtRemarksR.ReadOnly = false;
                }
                else
                {
                    lblRemark.Visible = true;
                    txtRemarksR.Visible = false;
                }
            }




        }
        
        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack) {
                populateCompanydetails();
                if (SiteSetting[DefaultSiteSetting.ApplicationEdition.ToString()].ToString () == UIConstants.INTERNAL_HIRING)
                {
                    lblNameHeader.Text = "BU Name";
                }
                
            }
            string name = "";
            if (!IsPostBack)
            {
                switch (CurrentCompanyStatus)
                {
                    case CompanyStatus.Client:
                        name = "Account Name";
                        break;
                    case CompanyStatus.Department:
                        divOverviewColumnRequisition.Visible = true;
                        name = "BU Name";
                        dvStartdate.Visible = false;
                        dvEnddate.Visible = false;
                        dvServicefee.Visible = false;
                        break;
                    case CompanyStatus.Vendor:
                        name = "Vendor Name";
                        lblRequisitions.Visible = false;
                        lblReqTitle.Text = ".";
                        dvStartdate.Visible = true;
                        dvEnddate.Visible = true;
                        dvServicefee.Visible = true;
                        break;
                }
                lblNameHeader.Text = name;
            }
              
        }

        protected void btnOk_Click(object sender, EventArgs e)
        {
            CommonNote commonNote = Facade.GetCommonNoteByCompanyIdAndNoteCategoryLookUpId(base.CurrentCompanyId, UrlConstants.COMPANY_REMARK);

            if (commonNote != null)
            {
                commonNote.NoteDetail = MiscUtil.RemoveScript(txtRemarksR.Text.Trim());
                commonNote.UpdatorId = CurrentMember.Id;
                Facade.UpdateCommonNote(commonNote);
                MiscUtil.ShowMessage(lblMessage, "Successfully updated Remarks.", false);
            }
            else
            {
                commonNote = new CommonNote();
                CompanyNote companyNote = new CompanyNote();
                commonNote.NoteCategoryLookupId = UrlConstants.COMPANY_REMARK;
                commonNote.NoteDetail = MiscUtil.RemoveScript(txtRemarksR.Text.Trim());
                commonNote.CreatorId = base.CurrentMember.Id;


                commonNote = Facade.AddCommonNote(commonNote);

                companyNote.CompanyId = Convert.ToInt16(base.CurrentCompanyId);
                companyNote.CommonNoteId = Convert.ToInt16(commonNote.Id);
                Facade.AddCompanyNote(companyNote);
                MiscUtil.ShowMessage(lblMessage, "Successfully added Remarks.", false);
            }
            CurrentCompany.UpdateDate = DateTime.Now;
            Facade.UpdateCompany(CurrentCompany);
           // txtRemarksR.ReadOnly = true;
          //  btnOk.Style.Add("display", "none");
          //  btnCancel.Style.Add("display", "none");
        }
        #endregion
    }
}