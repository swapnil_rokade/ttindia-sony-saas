﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: Company.master.cs
    Description: 
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1            May-13-2009             Rajendra        Defect ID: 10393; Changes made in RenderCompany() and RenderMenu()    
-------------------------------------------------------------------------------------------------------------------------------------------        
*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Web.UI;

using TPS360.Common;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Web.UI.Helper;

using System.Web.Security;

namespace TPS360.Web.UI
{
    public partial class CompanyMaster : CompanyMasterPage
    {
        #region Variables

       

        #endregion

        #region Methods

        public override void RenderCurrentCompany()
        {
            Company currentCompany = base.CurrentCompany;

            if (currentCompany.IsNew)
            {
                
                RenderCompany(false);
            }
            else
            {
                
                RenderCompany(true);
            }
        }

        private void RenderCompany(bool isVisible)
        {  
            Company currentCompany = base.CurrentCompany;

            if (isVisible)
            {
                RenderMenu();
            }
            else
            {
                ctlOverviewMenu.Visible = false;//0.1
            }
        }
        private void RenderMenu()
        {
            ctlOverviewMenu.CurrentSiteMapType = (int)SiteMapType.CompanyOverviewMenu;
            ctlOverviewMenu.EnablePermissionChecking = true;
            ctlOverviewMenu.OverviewType = OverviewType.CompanyOverview;
            ctlOverviewMenu.Visible = true;//0.1
        }
       
        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Facade.IsAvailableSessionId(Session.SessionID) != 1)
                {
                    FormsAuthentication.SignOut();
                    FormsAuthentication.RedirectToLoginPage();
                    return;
                }
            }
            catch { }
            Response.Expires = -1;  //10573 
            if (!Page.IsPostBack)
            {
                if (!MiscUtil.ReturnDashboard(Request.RawUrl, CurrentMember.Id, Facade))
                {
                    Helper.Url.Redirect(UrlConstants.Dashboard.DEFAULT_PAGE);
                }
            }
        }

       
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            RenderCurrentCompany();
        }

        #endregion
    }
}