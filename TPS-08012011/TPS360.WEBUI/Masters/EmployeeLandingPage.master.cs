﻿using System;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Web.UI.Helper;
using System.Collections.Generic;
using TPS360.Common.Shared;
using System.Web.Security;
namespace TPS360.Web.UI
{
    public partial class EmployeeMaster : EmployeeMasterPage
    {
        #region Member Variables
        
        private static int _memberId = 0;

        #endregion
        
        #region Methods

        private void GetMemberId()
        {
            //From Member Profile
            if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]))
            {
                _memberId = Int32.Parse(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]);

            }
            //From Member Login
            else
            {
                _memberId = base.CurrentMember.Id;
            }
        }

        public void RenderCurrentEmployee()
        {
            Member currentEmployee = Facade.GetMemberById(_memberId);

            if (currentEmployee != null)
            {
                LoadEmployeeBasicData(currentEmployee);
                //RenderSpecializedMenu(false);
            }
            else
            {
                //lblTitle.Text = currentEmployee.FirstName;
                //RenderSpecializedMenu(false);
            }
        }

        private void LoadEmployeeBasicData(Member currentEmployee)
        {
            //lblEmployeeName.Text = MiscUtil.GetFullName(currentEmployee.FirstName, currentEmployee.MiddleName, currentEmployee.LastName);
            //string strFullName = MiscUtil.GetFullName(currentEmployee.FirstName, currentEmployee.MiddleName, currentEmployee.LastName);
            //ControlHelper.SetHyperLink(lnkEmployeeName, UrlConstants.Employee.EMPLOYEE_INTERNALOVERVIEW, string.Empty, strFullName, UrlConstants.PARAM_MEMBER_ID, StringHelper.Convert(_memberId));

            //lblMobile.Text = currentEmployee.CellPhone;
            //lblPhone.Text = currentEmployee.PrimaryPhone;
            //lblEmail.Text = currentEmployee.PrimaryEmail;
            //if (currentEmployee.ResumeSource != 0)
            //{
            //    lblResumeSource.Text = Common.EnumHelper.GetDescription(((TPS360.Common.Shared.ResumeSource)currentEmployee.ResumeSource));
            //}
            DateTime dtLastUpdate = currentEmployee.UpdateDate;
            //lblLastUpdated.Text = dtLastUpdate.ToLongDateString();

            //currentEmployee.ResumeSource
            MemberDetail currentEmployeeDetail = Facade.GetMemberDetailByMemberId(currentEmployee.Id);
            if (currentEmployeeDetail != null)
            {
                string strCurrentState = String.Empty;
                if (currentEmployeeDetail.CurrentStateId != 0)
                {
                    State currentState = Facade.GetStateById(currentEmployeeDetail.CurrentStateId);
                    strCurrentState = currentState.Name;
                }
              //  lblLocation.Text = MiscUtil.GetLocation(currentEmployeeDetail.CurrentCity, strCurrentState);
            }

            //MemberExtendedInformation currentEmployeeExInfo = Facade.GetMemberExtendedInformationByMemberId(currentEmployee.Id);
            //if (currentEmployeeExInfo != null)
            //{
            //   // lblExperience.Text = currentEmployeeExInfo.TotalExperienceYears;
            //    if (currentEmployeeExInfo.InternalRatingLookupId != 0)
            //    {
            //        GenericLookup rating = Facade.GetGenericLookupById(currentEmployeeExInfo.InternalRatingLookupId);
            //        if (rating != null)
            //        {
            //            imgInterRating.ImageUrl = MiscUtil.GetInternalRatingImage(rating.Name);
            //        }
            //    }
            //}

            //MemberManager primaryManager = Facade.GetMemberManagerPrimaryByMemberId(currentEmployee.Id);
            //if (primaryManager != null)
            //{
            //    Member membermanager = Facade.GetMemberById(primaryManager.ManagerId);
            //    if (membermanager != null)
            //    {
            //        lblPrimaryManager.Text = MiscUtil.GetFullName(membermanager.FirstName, membermanager.MiddleName, membermanager.LastName);
            //    }
            //}

            //IList<MemberDocument> employeeDocumentList = Facade.GetAllMemberDocumentByTypeAndMemberId(ContextConstants.MEMBER_DOCUMENT_TYPE_PHOTO, _memberId);
            //if (employeeDocumentList != null)
            //{
            //    MemberDocument memberPhoto = employeeDocumentList[0];
            //    string strFilePath = MiscUtil.GetMemberDocumentPath(this.Page, _memberId, memberPhoto.FileName, ContextConstants.MEMBER_DOCUMENT_TYPE_PHOTO, true);
            //    imgEmployeePicture.ImageUrl = strFilePath;
            //}

            //IList<MemberDocument> memberVideoDocumentList = Facade.GetAllMemberDocumentByTypeAndMemberId(ContextConstants.MEMBER_DOCUMENT_TYPE_VIDEORESUME, currentEmployee.Id);
            //if (memberVideoDocumentList != null && memberVideoDocumentList.Count > 0)
            //{
            //    MemberDocument memberVideo = memberVideoDocumentList[0];

            //    imgVideoResume.Visible = false;
            //    lnkVideoResume.Visible = true;

            //    lnkVideoResume.Target = "_blank";
            //    //lnkDocumentLink.Target
            //    SecureUrl url = UrlHelper.BuildSecureUrl(UrlConstants.TPS360_VEDIO_PLAYER, string.Empty, UrlConstants.PARAM_ID, memberVideo.Id.ToString());
            //    ControlHelper.SetHyperLink(lnkVideoResume, url, memberVideo.FileName);

            //   // string filePath = MiscUtil.GetMemberDocumentPath(this.Page, currentEmployee.Id, memberPhoto.FileName, ContextConstants.MEMBER_DOCUMENT_TYPE_VIDEORESUME, true);
            //   // imgEmployeePicture.ImageUrl = filePath;
            //}
            //else
            //{
            //    imgVideoResume.Visible = true;
            //    lnkVideoResume.Visible = false;
            //}


        }
        
        #endregion

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Facade.IsAvailableSessionId(Session.SessionID) != 1)
                {
                    FormsAuthentication.SignOut();
                    FormsAuthentication.RedirectToLoginPage();
                    return;
                }
            }
            catch { }

            Response.Expires = -1;  //10573 
            if (!IsPostBack)
            {
                GetMemberId();
                //RenderCurrentEmployee();
                ctlOverviewMenu.CurrentSiteMapType = (int)SiteMapType.EmployeeOverviewMenu;
                ctlOverviewMenu.EnablePermissionChecking = true;
                ctlOverviewMenu.OverviewType = OverviewType.EmployeeOverview;
                if (!MiscUtil.ReturnDashboard(Request.RawUrl, CurrentMember.Id, Facade))
                {
                    Helper.Url.Redirect(UrlConstants.Dashboard.DEFAULT_PAGE);
                }
            }
        }
        #endregion
    }
}