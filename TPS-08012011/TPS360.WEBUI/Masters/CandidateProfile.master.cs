﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using TPS360.Providers;
using System.Text;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Web.UI.Helper;
namespace TPS360.Web.UI
{
    public partial class CandidateProfile : CandidateMasterPage
    {
        string _currentNode = string.Empty;
        string _currentParentNode = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            //if (!IsUserVendor)
            //{
            //    try
            //    {
            //        if (Facade.IsAvailableSessionId(Session.SessionID) != 1)
            //        {
            //            FormsAuthentication.SignOut();
            //            FormsAuthentication.RedirectToLoginPage();
            //            return;
            //        }
            //    }
            //    catch { }
            //}

            //helpModalPopupExt.Show();
            Response.Expires = -1;  //10573 

            if (!IsPostBack)
            {
                string canid=Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID ].ToString();
              //  string candidatename = MiscUtil.GetMemberNameById(Convert .ToInt32(canid ) , Facade);
               // lblCandidateName.Text = candidatename;
                if(IsUserVendor )
                    uclOverview.CurrentSiteMapType = (int)SiteMapType.CandidateProfileMenuForVendor ;
                else   
                    uclOverview.CurrentSiteMapType = (int)SiteMapType.CandidateOverviewMenu;
                uclOverview.EnablePermissionChecking = true;
                uclOverview.OverviewType = OverviewType.CandidateOverview;
                if (!IsUserVendor)
                {
                    if (!MiscUtil.ReturnDashboard(Request.RawUrl, CurrentMember.Id, Facade))
                    {
                        Helper.Url.Redirect(UrlConstants.Dashboard.DEFAULT_PAGE);
                    }
                }
            }
        }
        protected void Page_PreRender(object sender, EventArgs e)
        {
            int CurrentCandidateID = Convert .ToInt32 ( Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID].ToString());
            if (CurrentCandidateID > 0)
            {
                string name = MiscUtil.GetMemberNameById( CurrentCandidateID, Facade);
                this.Page.Title = name + " - " + "Resume Builder";
                lblCandidateName.Text = "A" + CurrentCandidateID  + " - " + name;
              
            }
        }
       

    }
}