﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using TPS360.BusinessFacade;
using TPS360.Common.BusinessEntities;
using System.Text;
using System.Collections.Generic;
public partial class GraphicalReport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        string RType = Request.QueryString["RType"].ToString();
        if (RType == "ER")
        {
            EmployeeReferralReport();
        }
        if (RType == "VS")
        {
            VendorSubmissionReport();
        }
        if (RType == "PRByE")
        {
            ProductivityReportEmployee();
        }
        Response.End();
    }

    private void ProductivityReportEmployee()
    {

        string UID = Request.QueryString["UID"].ToString();

        string TeamID= Request.QueryString["TID"].ToString();
        string StartDate = Request.QueryString["SD"].ToString();
        string EndDate = Request.QueryString["ED"].ToString();

        IFacade Facade = new Facade();
        TPS360.Common.Shared.PagedRequest request = new TPS360.Common.Shared.PagedRequest();
       if(UID !="" &&  UID !="0") request.Conditions.Add("MemberID", UID);
       if (TeamID !=""&& TeamID != "0")  request.Conditions.Add("TeamID", TeamID);
       request.Conditions.Add("StartDate", new DateTime(Convert .ToInt32 ( StartDate.Substring(0, 4)),Convert .ToInt32 ( StartDate.Substring(4, 2)),Convert .ToInt32 ( StartDate.Substring(6, 2))).ToShortDateString());
       request.Conditions.Add("EndDate", new DateTime(Convert .ToInt32 (EndDate.Substring(0, 4)), Convert .ToInt32 (EndDate.Substring(4, 2)),Convert .ToInt32 ( EndDate.Substring(6, 2))).ToShortDateString());
        IList<EmployeeProductivity > list = Facade.GetEmployeeProductivityReport (request);
        StringBuilder ResultString = new StringBuilder();
        ResultString.Append("<Result>");

        if (list == null) list = new List<EmployeeProductivity>();
        foreach (EmployeeProductivity ep in list)
        {
            ResultString.Append("<Row>");
            ResultString.Append("<User>" + ep.EmployeeName  + "</User>");
            ResultString.Append("<NewCan>" + ep.NewCandidateCount  + "</NewCan>");
            ResultString.Append("<ReqPub>" + ep.JobCount  + "</ReqPub>");
            ResultString.Append("<CanSor>" + ep.CandidateSourcedCount  + "</CanSor>");
            ResultString.Append("<CanSub>" + ep.SubmissionCount  + "</CanSub>");
            ResultString.Append("<Inter>" + ep.InterviewsCount  + "</Inter>");
            ResultString.Append("<Offers>" + ep.OfferCount  + "</Offers>");
            ResultString.Append("<Rejected>" + ep.OfferRejectedCount  + "</Rejected>");
            ResultString.Append("<Joined>" + ep.JoinedCount  + "</Joined>");
            ResultString.Append("<Pending>"+ep.PendingJoiners+"</Pending>");
            ResultString.Append("</Row>");
        }


        IList<EmployeeProductivity > listByDate = Facade.GetEmployeeProductivityReportByDate(UID ,TeamID , Convert .ToInt32 (StartDate ),Convert .ToInt32 (EndDate ));
        foreach (EmployeeProductivity l in listByDate)
        {
            ResultString.Append("<ByDate>");
            ResultString.Append("<Year>" + l.EmployeeName .Substring (0,4) + "</Year>");
            ResultString.Append("<Month>" + l.EmployeeName.Substring(4,2) + "</Month>");
            ResultString.Append("<Day>" + l.EmployeeName.Substring(6,2) + "</Day>");
            ResultString.Append("<NewCan>" + l.NewCandidateCount.ToString() + "</NewCan>");
            ResultString.Append("<ReqPub>" + l.JobCount.ToString() + "</ReqPub>");
            ResultString.Append("<CanSor>" + l.CandidateSourcedCount.ToString() + "</CanSor>");
            ResultString.Append("<CanSub>" + l.SubmissionCount.ToString() + "</CanSub>");
            ResultString.Append("<Inter>" + l.InterviewsCount.ToString() + "</Inter>");
            ResultString.Append("<Offers>" + l.OfferCount.ToString() + "</Offers>");
            ResultString.Append("<Rejected>" + l.OfferRejectedCount.ToString() + "</Rejected>");
            ResultString.Append("<Joined>" + l.JoinedCount.ToString() + "</Joined>");
            ResultString.Append("<Pending>" + l.PendingJoiners + "</Pending>");
            ResultString.Append("</ByDate>");
        }
        ResultString.Append("</Result>");
        Response.Clear();
        Response.ContentType = "text/xml";

        Response.Write(ResultString.ToString());
        //end the response
        Response.End();
    }


    private void EmployeeReferralReport()
    {

        string JID = Request.QueryString["JID"].ToString();

        string ReferrerId = Request.QueryString["RID"].ToString();
        string StartDate = Request.QueryString["SD"].ToString();
        string EndDate = Request.QueryString["ED"].ToString();
        
        IFacade Facade = new Facade();
        IList<ListWithCount> list = Facade.EmployeeReferral_GetCountGroupbyDate(Convert .ToInt32 (JID ),Convert .ToInt32 (ReferrerId ),Convert .ToInt32 (StartDate ),Convert .ToInt32 (EndDate ));
        StringBuilder ResultString = new StringBuilder();
        ResultString.Append("<Result>");
        if (Convert.ToInt32(StartDate) > 0)
        {
            DateTime newstartdate = new DateTime(Convert.ToInt32(StartDate.Substring(0, 4)), Convert.ToInt32(StartDate.Substring(4, 2)), Convert.ToInt32(StartDate.Substring(6, 2)));
            DateTime newEnddate = new DateTime(Convert.ToInt32(EndDate.Substring(0, 4)), Convert.ToInt32(EndDate.Substring(4, 2)), Convert.ToInt32(EndDate.Substring(6, 2)));
            for (newstartdate = newstartdate; newstartdate <= newEnddate; newstartdate=newstartdate.AddDays(1))
            {
                ResultString.Append("<Row><Year>");
                ResultString.Append(newstartdate .Year .ToString ());
                ResultString.Append("</Year><Month>");
                ResultString.Append(newstartdate .Month .ToString ());
                ResultString.Append("</Month><Day>");
                ResultString.Append(newstartdate .Day .ToString ());
                ResultString.Append("</Day><Count>");
                var s = list.Where(x => Convert.ToDateTime(x.Name) == newstartdate);
                if (s.Count() > 0)
                    ResultString.Append(s.ToList()[0].Count.ToString());
                else ResultString.Append("0");
                ResultString.Append("</Count></Row>");
            }
        }
        else
        {
            foreach (ListWithCount l in list)
            {
                ResultString.Append("<Row><Year>");
                ResultString.Append(Convert.ToDateTime(l.Name).Year.ToString());
                ResultString.Append("</Year><Month>");
                ResultString.Append(Convert.ToDateTime(l.Name).Month.ToString());
                ResultString.Append("</Month><Day>");
                ResultString.Append(Convert.ToDateTime(l.Name).Day.ToString());
                ResultString.Append("</Day><Count>");
                ResultString.Append(l.Count.ToString());
                ResultString.Append("</Count></Row>");
            }
        }
        ResultString.Append("</Result>");
        Response.Clear();
        Response.ContentType = "text/xml";

        Response.Write(ResultString.ToString());
        //end the response
        Response.End();
    }

    private void VendorSubmissionReport()
    {

        string JID = Request.QueryString["JID"].ToString();

        string VendorID = Request.QueryString["VID"].ToString();
        string ContactID = Request.QueryString["CCID"].ToString();
        string StartDate = Request.QueryString["SD"].ToString();
        string EndDate = Request.QueryString["ED"].ToString();

        IFacade Facade = new Facade();
        IList<ListWithCount> list = Facade.GetAllVendorSubmissionsGroupByDate (Convert.ToInt32(JID), Convert.ToInt32(VendorID ),Convert .ToInt32 (ContactID ), Convert.ToInt32(StartDate), Convert.ToInt32(EndDate));
        StringBuilder ResultString = new StringBuilder();
        ResultString.Append("<Result>");
        if (Convert.ToInt32(StartDate) > 0)
        {
            DateTime newstartdate = new DateTime(Convert.ToInt32(StartDate.Substring(0, 4)), Convert.ToInt32(StartDate.Substring(4, 2)), Convert.ToInt32(StartDate.Substring(6, 2)));
            DateTime newEnddate = new DateTime(Convert.ToInt32(EndDate.Substring(0, 4)), Convert.ToInt32(EndDate.Substring(4, 2)), Convert.ToInt32(EndDate.Substring(6, 2)));
            for (newstartdate = newstartdate; newstartdate <= newEnddate; newstartdate = newstartdate.AddDays(1))
            {
                ResultString.Append("<Row><Year>");
                ResultString.Append(newstartdate.Year.ToString());
                ResultString.Append("</Year><Month>");
                ResultString.Append(newstartdate.Month.ToString());
                ResultString.Append("</Month><Day>");
                ResultString.Append(newstartdate.Day.ToString());
                ResultString.Append("</Day><Count>");
                var s = list.Where(x => Convert.ToDateTime(x.Name) == newstartdate);
                if (s.Count() > 0)
                    ResultString.Append(s.ToList()[0].Count.ToString());
                else ResultString.Append("0");
                ResultString.Append("</Count></Row>");
            }
        }
        else
        {
            foreach (ListWithCount l in list)
            {
                ResultString.Append("<Row><Year>");
                ResultString.Append(Convert.ToDateTime(l.Name).Year.ToString());
                ResultString.Append("</Year><Month>");
                ResultString.Append(Convert.ToDateTime(l.Name).Month.ToString());
                ResultString.Append("</Month><Day>");
                ResultString.Append(Convert.ToDateTime(l.Name).Day.ToString());
                ResultString.Append("</Day><Count>");
                ResultString.Append(l.Count.ToString());
                ResultString.Append("</Count></Row>");
            }
        }
        ResultString.Append("</Result>");
        Response.Clear();
        Response.ContentType = "text/xml";

        Response.Write(ResultString.ToString());
        //end the response
        Response.End();
    }
}
