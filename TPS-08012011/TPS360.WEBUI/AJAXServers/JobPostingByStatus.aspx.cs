﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using TPS360.Web.UI;
using TPS360.Common.BusinessEntities;
using TPS360.BusinessFacade;
using TPS360.Web.UI.Helper;
using System.Collections.Generic;
using System.Text;

public partial class JobPostingByStatus : BasePage
{
   
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            string selectedStatus = Request["selectedStatus"];
                {
                    Response.Clear();
                 
                    IFacade facade = new Facade();
                    ArrayList reqList = null;
                    reqList =facade .GetAllJobPostingListByStatusId (Convert .ToInt32(selectedStatus ));
                    JobPosting j = new JobPosting();
                    j.Id = 0;
                    j.JobTitle = "Any";
                    reqList.Insert(0, j);
                    string statesString = "<Status>";// countryStateXml.GetCompanyContactXMLString(Convert.ToInt32(selectedCompany.ToLower().Trim() == "all" ? "0" : selectedCompany), FillEmail);
      

                    foreach (JobPosting job in reqList)
                    {
                        statesString += "<Requisition><Id>";
                        statesString += job.Id.ToString() + "</Id><Name>";
                        statesString += job.JobPostingCode + " " + job.JobTitle + "</Name></Requisition>";
                    }
                    statesString += "</Status>";


                    Response.Clear();
                    Response.ContentType = "text/xml";

                    Response.Write(statesString);
                    //end the response
                    Response.End();
                }
               
            }

        }
    }

   
