﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using TPS360.Common.BusinessEntities;
using TPS360.BusinessFacade;
namespace TPS360.Web.UI
{
    public partial class CandidateRequisitionStatusAjaxServer : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string SelectedStatusId = Request["SelectedStatusId"];
                string CandidateId = Request["CandidateId"];
                string RequisitionId = Request["RequisitionId"];
                string UpdatorId = Request["UpdatorId"];
                if (SelectedStatusId != null  && CandidateId != null  && RequisitionId != null  && UpdatorId != null )
                {
                    if (SelectedStatusId != string.Empty && CandidateId != string.Empty && RequisitionId != string.Empty && UpdatorId != string.Empty)
                    {
                        Facade.UpdateByStatus(Convert.ToInt32(RequisitionId), CandidateId, Convert.ToInt32(SelectedStatusId), Convert.ToInt32(UpdatorId));
                        try
                        {
                            Facade.UpdateCandidateRequisitionStatus(Convert.ToInt32(RequisitionId), CandidateId, Convert.ToInt32(UpdatorId), Convert.ToInt32(SelectedStatusId));
                        }
                        catch
                        {
                        }

                    }
                }
            }
        }
    }
}
