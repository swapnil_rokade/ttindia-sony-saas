﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using TPS360.Common.BusinessEntities;
using TPS360.BusinessFacade;
using System.Collections.Generic;
namespace TPS360.Web.UI
{
    /// <summary>
    /// Summary description for AjaxServer.
    /// </summary>System.Web.UI.Page
    public partial class OfferedDetailsAjaxServer : BasePage  
    {
        //Sends the response back, with XML data.
        protected void Page_Load(object sender, System.EventArgs e)
        {
            if (!IsPostBack)
            {
                string SelectedCategory = Request["SelectedCategory"];
                if (SelectedCategory.Length > 0)
                {
                    IList<GenericLookup> list = null;
                    list = Facade.GetAllGenericLookupByLookupType(Convert.ToInt32(SelectedCategory) == 1113 ? TPS360 .Common .Shared .LookupType .OfferedGradeEmbedded: TPS360 .Common .Shared .LookupType .OfferedGradeMech);
                    Response.Clear();
                    string gradeString = "<Category><OfferGrade><Id>0</Id><Name>Please Select</Name></OfferGrade>";// countryStateXml.GetCompanyContactXMLString(Convert.ToInt32(selectedCompany.ToLower().Trim() == "all" ? "0" : selectedCompany), FillEmail);

                    foreach (GenericLookup glookup in list)
                    {
                        gradeString += "<OfferGrade><Id>";
                        gradeString += glookup.Id.ToString() + "</Id><Name>";
                        gradeString += glookup.Name + "</Name></OfferGrade>";
                    }
                    gradeString += "</Category>";
                    Response.Clear();
                    Response.ContentType = "text/xml";
                    Response.Write(gradeString);
                    //end the response
                    Response.End();
                }
                else
                {
                    //clears the response written into the buffer and end the response.
                    Response.Clear();
                    Response.End();
                }


            }
            else
            {
                //clears the response written into the buffer and end the response.
                Response.Clear();
                Response.End();
            }


        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        }
        #endregion
    }
}
