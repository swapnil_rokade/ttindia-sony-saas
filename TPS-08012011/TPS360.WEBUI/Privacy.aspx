﻿<%@ Page Language="C#" MasterPageFile="~/Public.master" AutoEventWireup="true" CodeFile="Privacy.aspx.cs"
    Inherits="TPS360.Web.UI.Privacy" Title="Talentrackr - Privacy Statement" %>

<asp:Content ID="cDefault" ContentPlaceHolderID="cphHomeMaster" runat="Server">
    <div class="CommonPageBox">
        <div class="CommonPageBoxHeader">
            Talentrackr - Privacy Statement
        </div>
        <div class="CommonPageBoxContent" >
            Talentrackr is committed to the responsible management of client information and
            abides by all rules, both ethical and legal, with respect to the use of such information.
            We strictly adhere to all industry guidelines, and are continuously monitoring and
            enhancing how we manage the use and security of client information to uphold your
            rights.<br />
            <br />
            This statement outlines the type of information we collect and store, our usage
            policy, and who to contact with questions or concerns.<br />
            <br />
            Talentrackr provides complete recruitment technology solutions, hardware and software,
            with consulting services to produce an economical platform for our clients to generate
            revenue and increase profitability.
            <br />
            <br />
            <strong>Personal</strong><br />
            On Talentrackr’s website, we will not collect any personally identifiable information
            about you (name, address, telephone number, email address) unless you provide it
            to us voluntarily. If you do not want your personal information collected, please
            do not submit it.<br />
            <br />
            If you do submit your email or postal addresses online, you may receive periodic
            mailings from us with information on new services or products. If you do not wish
            to receive such mailings, please let us know by contacting us at info@talentrackr.com
            or by the mail address listed at the bottom of this Statement.
            <br />
            <br />
            If you submit your telephone number online, you may receive telephone contact from
            us regarding information you have requested online. You may also receive telephone
            contact regarding new services or products. If you do not wish to receive telephone
            calls, please let us know by contacting us at info@talentrackr.com or by the mail address
            listed at the bottom of this Statement.
            <br />
            <br />
            If you have already submitted this information and would like for us to remove it
            from our files, please contact us at info@talentrackr.com or by the mail address listed
            at the bottom of this Statement. We will use reasonable efforts to delete your information
            from our existing files.
            <br />
            <br />
            We do not intend to sell your personally identifiable information to third parties
            unrelated to the specific operation of this site, such as companies seeking to contact
            you with offers of products or services. We may disclose your personally identifiable
            information if required to do so by law or in the good-faith belief that such action
            is necessary to (1) conform to the law or legal process; (2) protect or defend our
            rights or property or those of our users, and (3) act as immediately necessary in
            order to protect the safety of our users or the public.
            <br />
            <br />
            <strong>Non-Personally Identifiable Information Collected Automatically</strong><br />
            For each visitor to Talentrackr, our Web server automatically recognizes your domain,
            the type of Internet browser you are using, the type of computer operating system
            you are using, and the domain name of the Web site from which you linked to our
            site. This non-personally identifiable information is collected as a matter of course
            during the ad viewing process.
            <br />
            <br />
            <strong>Who to contact</strong><br />
            Any client may request removal of his or her name from Talentrackr marketing database
            by sending e-mail to info(at)talentrackr.com or contact us through contact page. You
            must provide your name, complete street address, city, state, ZIP code and e-mail
            address. We will use reasonable efforts to delete this information from our existing
            files.
        </div>
    </div>
    <br />
</asp:Content>