﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: RejectCandidateReports.aspx.cs
    Description: This is the page used to display the Reject candidate reports in excel,word,Pdf formats.
    Created By: pravin khot
    Created On: 14/March/2016
    Modification Log: 
    --------------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    --------------------------------------------------------------------------------------------------------------------------------------
 * ------------------------------------------------------------------------------------------------------------------------------------------- 
*/
using System;
using System.Web.Security;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Web.UI.Helper;
using TPS360.Common.Shared;
using TPS360.Common;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using ExpertPdf.HtmlToPdf;
using System.Drawing;
using System.Collections.Generic;
using System.Collections;
using System.Web.UI.HtmlControls;
using System.Linq;

namespace TPS360.Web.UI.Reports
{

    public partial class RejectCandidateReports : BasePage 
    {
        #region Members
        private bool _IsAccessToEmployee = true;
        private static string UrlForEmployee = string.Empty;
        private static int IdForSitemap = 0;

        string _cookiename;
        string CookieName
        {
            get { return _cookiename; }
            set { _cookiename = value; }
        }

        #endregion

        #region Methods
        private void PlaceUpDownArrow()
        {
            try
            {
                LinkButton lnk = (LinkButton)lsvRejectCandidateList.FindControl(txtSortColumn.Text);
                HtmlTableCell im = (HtmlTableCell)lnk.Parent;
                im.EnableViewState = false;
                im.Attributes.Add("class", (txtSortOrder.Text  == "ASC" ? "Ascending" : "Descending"));

            }
            catch
            {
            }

        }
        private void GenerateRejectCandidateReport(string format)
        {
            if (string.Equals(format, "word"))
            {

                Response.AddHeader("content-disposition", "attachment;filename=RejectCandidateReport-" + DateTime.Now.ToString("yyyy-MM-dd") + ".doc");
                Response.ContentEncoding = System.Text.Encoding.ASCII;
                Response.ContentType = "application/msword";
                Response.Output.Write(GetRejectCandidateReportTable());
                Response.Flush();
                Response.End();
               
            }
            else if (string.Equals(format, "excel"))
            {
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=RejectCandidateReport-" + DateTime.Now.ToString("yyyy-MM-dd") + ".xls");
                Response.ContentEncoding = System.Text.Encoding.ASCII;
                Response.ContentType = "application/msexcel";
                Response.Output.Write(GetRejectCandidateReportTable());
                Response.Flush();
                Response.End();
               
            }
            else if (string.Equals(format, "pdf"))
            {
                PdfConverter pdfConverter = new PdfConverter();
                UnicodeEncoding unicodeEncoding = new UnicodeEncoding();
                pdfConverter.PageWidth = 0; 
                pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
                pdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Normal;
                pdfConverter.PdfDocumentOptions.ShowHeader = false;
                pdfConverter.PdfDocumentOptions.ShowFooter = false;
                pdfConverter.PdfDocumentOptions.LeftMargin = 15;
                pdfConverter.PdfDocumentOptions.RightMargin = 5;
                pdfConverter.PdfDocumentOptions.TopMargin = 15;
                pdfConverter.PdfDocumentOptions.BottomMargin = 5;
                pdfConverter.PdfDocumentOptions.GenerateSelectablePdf = true;
                pdfConverter.PdfDocumentOptions.ShowHeader = false;
                pdfConverter.PdfHeaderOptions.DrawHeaderLine = false;
                pdfConverter.PdfFooterOptions.FooterTextColor = Color.Black;
                pdfConverter.PdfFooterOptions.DrawFooterLine = true;
                pdfConverter.PdfFooterOptions.PageNumberText = "Page";
                pdfConverter.PdfFooterOptions.ShowPageNumber = true;
                                   

                pdfConverter.LicenseKey = ContextConstants.EPHTMLTOPDFKEY;
                byte[] downloadBytes = pdfConverter.GetPdfBytesFromHtmlString(GetRejectCandidateReportTable());

                System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
                response.Clear();
                response.AddHeader("Content-Type", "binary/octet-stream");
                response.AddHeader("Content-Disposition", "attachment; filename=RejectCandidateReport-" + DateTime.Now.ToString("yyyy-MM-dd") + ".pdf; size=" + downloadBytes.Length.ToString());
                response.Flush();
                response.BinaryWrite(downloadBytes);
                response.Flush();
                response.End();
            }
        }

        private string GetRejectCandidateReportTable()
        {
            StringBuilder reqReport = new StringBuilder();
            RejectCandidateDataSource candidateDataSource = new RejectCandidateDataSource();
           
          

            IList<string> checkedList = new List<string>();
            foreach (ListItem item in chkColumnList.Items)
            {
                if (item.Selected)
                    checkedList.Add(item.Value);
            }

            IList<DynamicDictionary> candidateList1 = candidateDataSource.GetPagedReject(wdcCandidatesAdded.StartDate, wdcCandidatesAdded.EndDate, Int32.Parse(ddlAddedByEmployee.SelectedValue.Trim() == "" ? "0" : ddlAddedByEmployee.SelectedValue), 0,
               uclCountryState .SelectedCountryId , uclCountryState .SelectedStateId ,MiscUtil .RemoveScript ( txtCity.Text), DateTime.MinValue, DateTime.MinValue, 0, 0,
               0, 0, Int32.Parse(ddlCandidatesWorkPermit.SelectedValue), Int32.Parse(ddlWorlSchedule.SelectedValue), Int32.Parse(ddlCandidatesGender.SelectedValue),
               Int32.Parse(ddlCandidatesMaritalStatus.SelectedValue), Int32.Parse(ddlCandidatesEducation.SelectedValue), 0, false, false, false, Int32.Parse(ddlAssignedManager.SelectedValue), ddlRequisition.SelectedValue.ToString(), "", -1, -1, checkedList);

            if (candidateList1 != null)
            {
                reqReport.Append("<table style='font-family : calibriz;width:100%; border-collapse:collapse; border-spacing: 1px;' align='left' border = '1' bordercolor='#000000'  cellspacing='0' cellpadding='0'>");

                reqReport.Append("    <tr>");
                //reqReport.Append("        <td align=left>&nbsp;<img src='" + Server.MapPath(UIConstants.EMAIL_COMPANY_LOGO) + "' style='height:56px;width:56px;'/></td>");
                reqReport.Append("        <td align=left  width='300' height='75'>&nbsp;<img src='" + UrlConstants.ApplicationBaseUrl + "Images/logo-left-75px.png' style= style='height:56px;width:56px'/></td>"); //0.1
                reqReport.Append("    </tr>");

                reqReport.Append(" <tr>");
                //0.1 starts
                if (lsvRejectCandidateList.Items.Count > 0)
                {
                    foreach (ListItem chkItem in chkColumnList.Items)
                    {
                        HtmlTableCell thHeaderTitle = (HtmlTableCell)lsvRejectCandidateList.FindControl("th" + chkItem.Value);
                        if (thHeaderTitle != null)
                        {
                            if (chkItem.Selected)
                            {
                                reqReport.Append("<th><b>" + chkItem.Text + "</b></th>");
                            }
                        }
                    }
                }
                else
                {
                    foreach (ListItem chkItem in chkColumnList.Items)
                    {
                        if (chkItem.Selected)
                        {
                            reqReport.Append("<th><b>" + chkItem.Text + "</b></th>");
                        }
                    }
                }
            
                reqReport.Append(" </tr>");
                int i = 0;
                foreach (DynamicDictionary d in candidateList1)
                {
                 
                       reqReport.Append("<tr>");
                    
                    foreach (var s in d.properties)
                    {
                        if (s.Key.ToString() == "Id")
                        {
                            reqReport.Append("     <td>" + "A" + Convert.ToString(s.Value) + "</td>");
                        }
                        else if (s.Key.ToString() == "UpdateDate" || s.Key.ToString() == "CreateDate")
                        {
                            reqReport.Append("     <td>" + Convert.ToDateTime(s.Value).ToShortDateString() + "</td>");
                        }
                        else 
                        {
                            reqReport.Append("     <td>" + Convert.ToString(s.Value) + "</td>");
                        }
                        
                    }
                    reqReport.Append("    </tr>");
                }
            }
            reqReport.Append("    <tr>");
            reqReport.Append("        <td align=left  width='300' height='75' >&nbsp;<img src='" + UrlConstants.ApplicationBaseUrl + "Images/logo-left-75px.png'  style= style='height:56px;width:56px;'/></td>");//0.1
            reqReport.Append("    </tr>");
            reqReport.Append(" </table>");             
            return reqReport.ToString();
        }

        private void BindList()
        {
            bool chkColumnListcount = false;
            foreach (ListItem items in chkColumnList.Items)
            {
                if (items.Selected)
                {
                    chkColumnListcount = true;
                    break;
                }
            }
            if (chkColumnListcount)
            {

                this.lsvRejectCandidateList.DataSourceID = "odsRejectCandidateList";
                this.lsvRejectCandidateList.DataBind();
                divCandidateList.Visible = true;
                
            }
            else
            {
                divCandidateList.Visible = false;
                MiscUtil.ShowMessage(lblMessage, "Please Select atleast One column", false);
            }
        }

        private void Clear()
        {
            divCandidateList.Visible = false;
            ClearSearchCriteria();
        }
        private void ClearSearchCriteria()
        {
            lblMessage.Text = string.Empty;
            wdcCandidatesAdded.ClearRange();
            txtCity.Text = string.Empty;
            ddlAddedByEmployee.SelectedIndex = 0;
            ddlCandidatesEducation.SelectedIndex = 0;
            ddlCandidatesGender.SelectedIndex = 0;
            ddlCandidatesMaritalStatus.SelectedIndex = 0;
            ddlCandidatesWorkPermit.SelectedIndex = 0;
            ddlAssignedManager.SelectedIndex = 0;
            uclCountryState.SelectedCountryId = 0;
            uclCountryState.SelectedStateId = 0;
            ddlWorlSchedule.SelectedIndex = 0;
            hdnRequisitionSelected.Value = "0";
            ddlRequisition.Items.Clear();
            int k = 0;
            System.Collections.ArrayList arr = Facade.GetAllJobPostingListByStatusId(k);
            JobPosting j = new JobPosting();
            j.Id = 0;
            j.JobTitle = "Any";
            arr.Insert(0, j);
            ddlRequisition.DataSource = arr;
            ddlRequisition.DataTextField = "JobTitle";
            ddlRequisition.DataValueField = "Id";
            ddlRequisition.DataBind();
        }

        private void PrepareView()
        {
            ArrayList lstEmployee = Facade.GetAllMemberNameByRoleName(ContextConstants.ROLE_EMPLOYEE);
            ddlAddedByEmployee.DataTextField  = "Name";
            ddlAddedByEmployee.DataValueField = "Id";
            ddlAddedByEmployee.DataSource = lstEmployee;
            ddlAddedByEmployee.DataBind();         
            ddlAssignedManager.DataBind();          
            ddlAddedByEmployee = (DropDownList)MiscUtil.RemoveScriptForDropDown(ddlAddedByEmployee);
            ddlAssignedManager = (DropDownList)MiscUtil.RemoveScriptForDropDown(ddlAssignedManager);
            ddlAddedByEmployee.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_ALL, "0"));
            ddlAssignedManager.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_ANY, "0"));
            MiscUtil.PopulateWorkSchedule(ddlWorlSchedule, Facade);
            MiscUtil.PopulateMaritalStatusType(ddlCandidatesMaritalStatus, Facade);
            ddlCandidatesMaritalStatus.Items.RemoveAt(0);
            ddlCandidatesMaritalStatus.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_ANY, "0"));

            MiscUtil.PopulateGenderType(ddlCandidatesGender, Facade);
            ddlCandidatesGender.Items.RemoveAt(0);
            ddlCandidatesGender.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_ANY, "0"));

            MiscUtil.PopulateWorkAuthorization(ddlCandidatesWorkPermit, Facade);
            ddlCandidatesWorkPermit.Items.RemoveAt(0);
            ddlCandidatesWorkPermit.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_ANY, "0"));

            MiscUtil.PopulateEducationQualification(ddlCandidatesEducation, Facade);
            ddlCandidatesEducation.Items.RemoveAt(0);
            ddlCandidatesEducation.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_ANY, "0"));

            PopulateRequisition(0);
        }
        private void ResetDataSourceParameters()
        {
            odsRejectCandidateList.SelectParameters["addedFrom"].DefaultValue = DateTime.MinValue.ToString();
            odsRejectCandidateList.SelectParameters["addedTo"].DefaultValue = DateTime.MinValue.ToString();
            odsRejectCandidateList.SelectParameters["addedBy"].DefaultValue = "0";
            odsRejectCandidateList.SelectParameters["addedBySource"].DefaultValue = "0";
            odsRejectCandidateList.SelectParameters["updatedFrom"].DefaultValue = DateTime.MinValue.ToString();
            odsRejectCandidateList.SelectParameters["updatedTo"].DefaultValue = DateTime.MinValue.ToString();
            odsRejectCandidateList.SelectParameters["updatedBy"].DefaultValue = "0";

            odsRejectCandidateList.SelectParameters["country"].DefaultValue = "0";
            odsRejectCandidateList.SelectParameters["state"].DefaultValue = "0";
            odsRejectCandidateList.SelectParameters["city"].DefaultValue = String.Empty;

            odsRejectCandidateList.SelectParameters["interviewFrom"].DefaultValue = DateTime.MinValue.ToString();
            odsRejectCandidateList.SelectParameters["interviewTo"].DefaultValue = DateTime.MinValue.ToString();
            odsRejectCandidateList.SelectParameters["interviewLevel"].DefaultValue = "0";
            odsRejectCandidateList.SelectParameters["interviewStatus"].DefaultValue = "0";
            odsRejectCandidateList.SelectParameters["industry"].DefaultValue = "0";
            odsRejectCandidateList.SelectParameters["functionalCategory"].DefaultValue = "0";
            odsRejectCandidateList.SelectParameters["workPermit"].DefaultValue = "0";
            odsRejectCandidateList.SelectParameters["WorkSchedule"].DefaultValue = "0";
            odsRejectCandidateList.SelectParameters["gender"].DefaultValue = "0";
            odsRejectCandidateList.SelectParameters["maritalStatus"].DefaultValue = "0";
            odsRejectCandidateList.SelectParameters["educationId"].DefaultValue = "0";
            odsRejectCandidateList.SelectParameters["assessmentId"].DefaultValue = "0";
            odsRejectCandidateList.SelectParameters["isBroadcastedResume"].DefaultValue = "false";
            odsRejectCandidateList.SelectParameters["isReferredCandidates"].DefaultValue = "false";
            odsRejectCandidateList.SelectParameters["hasJobAgent"].DefaultValue = "false";
            odsRejectCandidateList.SelectParameters["assignedManager"].DefaultValue = "0";
            odsRejectCandidateList.SelectParameters["JobPostingId"].DefaultValue = "0";
        }

        private void SetDataSourceParameters()
        {
            if (SortOrder.Text == string.Empty && SortColumn.Text == string.Empty)
            {
                SortOrder.Text = "asc";
                SortColumn.Text = "[C].[FirstName]";
            }
            odsRejectCandidateList.SelectParameters["SortOrder"].DefaultValue = SortOrder.Text.ToString();




            odsRejectCandidateList.SelectParameters["addedBy"].DefaultValue = ddlAddedByEmployee.SelectedValue;
            odsRejectCandidateList.SelectParameters["addedBySource"].DefaultValue = "0";
          
                odsRejectCandidateList.SelectParameters["interviewFrom"].DefaultValue = DateTime.MinValue.ToString();
           
            odsRejectCandidateList.SelectParameters["interviewTo"].DefaultValue = DateTime.MinValue.ToString();
            odsRejectCandidateList.SelectParameters["interviewLevel"].DefaultValue = "0";
            odsRejectCandidateList.SelectParameters["interviewStatus"].DefaultValue = "0";
            odsRejectCandidateList.SelectParameters["functionalCategory"].DefaultValue = "0";
            odsRejectCandidateList.SelectParameters["gender"].DefaultValue = ddlCandidatesGender.SelectedValue;
            odsRejectCandidateList.SelectParameters["educationId"].DefaultValue = ddlCandidatesEducation.SelectedValue;
            odsRejectCandidateList.SelectParameters["industry"].DefaultValue = "0";
            odsRejectCandidateList.SelectParameters["workPermit"].DefaultValue = ddlCandidatesWorkPermit.SelectedValue;

            odsRejectCandidateList.SelectParameters["WorkSchedule"].DefaultValue = ddlWorlSchedule .SelectedValue;


            odsRejectCandidateList.SelectParameters["maritalStatus"].DefaultValue = ddlCandidatesMaritalStatus.SelectedValue;
            odsRejectCandidateList.SelectParameters["assessmentId"].DefaultValue = "0";
            odsRejectCandidateList.SelectParameters["city"].DefaultValue =MiscUtil .RemoveScript ( txtCity.Text);
            odsRejectCandidateList.SelectParameters["assignedManager"].DefaultValue = ddlAssignedManager.SelectedValue;
            odsRejectCandidateList.SelectParameters["JobPostingId"].DefaultValue = ddlRequisition.SelectedValue.ToString();


        }

        private void HideListViewHeaderItem()
        {
            foreach (ListItem chkItem in chkColumnList.Items)
            {
                HtmlTableCell thHeaderTitle = (HtmlTableCell)lsvRejectCandidateList.FindControl("th" + chkItem.Value);
                if (thHeaderTitle != null)
                {
                    if (chkItem.Selected)
                    {
                        thHeaderTitle.Visible = true;

                    }
                    else
                    {
                        thHeaderTitle.Visible = false;
                    }
                }
            }
        }
        void PopulateRequisition(int StatusId)
        {
           
            hdnRequisitionSelected.Value = "0";
            ddlRequisition.Items.Clear();
            int k = 0;
            System.Collections.ArrayList arr = Facade.GetAllJobPostingListByStatusId(k);
            JobPosting j = new JobPosting();
            j.Id = 0;
            j.JobTitle = "Any";
            arr.Insert(0, j);
            ddlRequisition.DataSource = arr;
            ddlRequisition.DataTextField = "JobTitle";
            ddlRequisition.DataValueField = "Id";
            ddlRequisition.DataBind();
            ddlRequisition.SelectedIndex = 0;
        }

        private void HideListViewItem(ListViewItem itm)
        {
            foreach (ListItem chkItem in chkColumnList.Items)
            {
                HtmlTableCell thHeaderTitle = (HtmlTableCell)itm.FindControl("td" + chkItem.Value);
                if (thHeaderTitle != null)
                {
                    if (chkItem.Selected)
                    {
                        thHeaderTitle.Visible = true;

                    }
                    else
                    {
                        thHeaderTitle.Visible = false;
                    }
                }
            }
        }

        public void MessageAnswered(object sender, ConfirmationWindow.MsgBoxEventArgs e)
        {
            if (e.Answer == ConfirmationWindow.enmAnswer.OK)
            {
                repor("pdf");
            }

        }
        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            //try
            //{
            //    if (Facade.IsAvailableSessionId(Session.SessionID) != 1)
            //    {
            //        FormsAuthentication.SignOut();
            //        FormsAuthentication.RedirectToLoginPage();
            //        return;
            //    }
            //}
            //catch { }
         
            uclConfirm.MsgBoxAnswered += MessageAnswered;
            ddlAddedByEmployee.Attributes.Add("onmouseover", "this.title=this.options[this.selectedIndex].title");
            ddlAssignedManager.Attributes.Add("onmouseover", "this.title=this.options[this.selectedIndex].title");
            CookieName = "RejectCandidateReportUserColumnOption_" + base.CurrentMember.PrimaryEmail;
            CustomSiteMap CustomMap = new CustomSiteMap();
            CustomMap = Facade.GetAllCustomSiteMapByParentIdAndMemberPrivilege(358, CurrentMember.Id);
            if (CustomMap == null) _IsAccessToEmployee = false;
            else
            {
                IdForSitemap = CustomMap.Id;
                UrlForEmployee = "~/" + CustomMap.Url.ToString();
            }
                

            if (!IsPostBack)
            {
                PrepareView();
                divCandidateList.Visible = false;
                if (Request.Cookies[CookieName] != null)
                {
            
                    //string ColumnOptions = Request.Cookies[CookieName].Value;
					//5 Columns added
					string ColumnOptions = "Id#Name#PrimaryEmail#CurrentCity#CurrentStateName#PrimaryPhone#CellPhone#HomePhone#CurrentPosition#LastEmployer#NoticePeriod#Department#JobCode#FeedbackStatus#HiringManager#";
                    string[] Columns = ColumnOptions.Split('#');
                    foreach (ListItem item in chkColumnList.Items)
                    {
                        item.Selected = false;
                    }
                    foreach (string item in Columns)
                    {
                       if(chkColumnList.Items.FindByValue(item)!=null) chkColumnList.Items.FindByValue(item).Selected = true;
                    }
                }
            }
            if (!IsPostBack)
            {

                btnSearch_Click(sender, e);
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            hdnScrollPos.Value = "0";
            string ColumnOption = "";
            foreach (ListItem item in chkColumnList.Items)
            {
                if (item.Selected == true) ColumnOption += item.Value + "#";

            }
            System.Web . HttpCookie aCookie = new System.Web.HttpCookie(CookieName);
            aCookie.Value = ColumnOption.ToString();
            Response.Cookies.Add(aCookie);
            lsvRejectCandidateList.Items.Clear();
            SetDataSourceParameters();
            ASP.controls_pagercontrol_ascx PagerControls = (ASP.controls_pagercontrol_ascx)this.lsvRejectCandidateList.FindControl("pagerControl");
            if (PagerControls != null)
            {
                DataPager pager = (DataPager)PagerControls.FindControl("pager");
                if (pager != null)
                {
                    if (pager.Page.IsPostBack)
                        pager.SetPageProperties(0, pager.MaximumRows, true);
                }
            }
            divExportButton.Visible = false; 
            BindList();
            HideListViewHeaderItem();
           
            string pagesize = "";
            if ( txtSortColumn.Text =="")txtSortColumn.Text = "lnkBtnName";
           if( txtSortOrder.Text =="") txtSortOrder.Text = "ASC";
            PlaceUpDownArrow();
            pagesize = (Request.Cookies["RejectCandidateReportRowPerPage"] == null ? "" : Request.Cookies["RejectCandidateReportRowPerPage"].Value); ;

            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvRejectCandidateList.FindControl("pagerControl");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)PagerControl.FindControl("pager");
                if (pager != null)
                {
                    pager.PageSize = Convert.ToInt32(pagesize == "" ? "20" : pagesize);
                    if (pager.Page.IsPostBack)
                        pager.SetPageProperties(0, pager.MaximumRows, true);


                    
                }
            }
            System.Web.UI.HtmlControls.HtmlTableCell tdpager = (System.Web.UI.HtmlControls.HtmlTableCell)lsvRejectCandidateList.FindControl("tdpager");
            if (tdpager != null) tdpager.ColSpan = chkColumnList.Items.Cast<ListItem>().Where(x => x.Selected == true).Count();
            uclCountryState.SelectedCountryId = uclCountryState.SelectedCountryId;
            uclCountryState.SelectedStateId = uclCountryState.SelectedStateId;
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            Clear();
            divExportButton.Visible = false;
        }

        protected void btnExportToPDF_Click(object sender, EventArgs e)
        {
            uclConfirm.AddMessage("Note: Some columns may not appear in report if too many are selected.", ConfirmationWindow.enmMessageType.Attention, true, true, "");
           
        }
        public void repor(string rep)
        {
            int j = 0;
            int k = 0;
            foreach (ListItem items in chkColumnList.Items)
            {
                if (items.Selected)
                {
                    j = 1;

                }
                else
                    k = 1;
            }
            if ((j == 1 && k == 1) || (k == 0 && j == 1))
                GenerateRejectCandidateReport(rep);
            else
                MiscUtil.ShowMessage(lbMessage, "Please select columns before generating the report.", true);
        }

        protected void btnExportToExcel_Click(object sender, EventArgs e)
        {
            repor ("excel");
        }

        protected void btnExportToWord_Click(object sender, EventArgs e)
        {
            repor ("word");
        }

        protected void lsvRejectCandidateList_PreRender(object sender, EventArgs e)
        {
            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvRejectCandidateList.FindControl("pagerControl");
             if (PagerControl != null)
             {
                 DataPager pager = (DataPager)PagerControl.FindControl("pager");
                 if (pager != null)
                 {
                     DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                     if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
                 }
                 HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
                 if (hdnRowPerPageName != null) hdnRowPerPageName.Value = "RejectCandidateReportRowPerPage";
             }

             PlaceUpDownArrow();
             HideListViewHeaderItem();
             if (IsPostBack)
             {
                 if (lsvRejectCandidateList.Items.Count == 0)
                 {
                     lsvRejectCandidateList.DataSource = null;
                     lsvRejectCandidateList.DataBind();
                 }
             }
        }
        protected void lsvRejectCandidateList_ItemCommand(object sender, ListViewCommandEventArgs e)
          {
              try
              {
                  if (e.CommandName == "Sort")
                  {
                      LinkButton lnkbutton = (LinkButton)e.CommandSource;
                      if (txtSortColumn.Text == lnkbutton.ID)
                      {
                          if (txtSortOrder.Text == "ASC") txtSortOrder.Text = "DESC";
                          else txtSortOrder.Text = "ASC";
                      }
                      else
                      {
                          txtSortColumn.Text = lnkbutton.ID;
                          txtSortOrder.Text = "ASC";
                      }
                      if (SortColumn.Text == string.Empty || SortColumn.Text != e.CommandArgument.ToString())
                          SortOrder.Text = "asc";
                      else SortOrder.Text = SortOrder.Text == "asc" ? "desc" : "asc";
                      SortColumn.Text = e.CommandArgument.ToString();
                      odsRejectCandidateList.SelectParameters["SortOrder"].DefaultValue = SortOrder.Text.ToString();
                  }
              }
              catch
              {
              }
          }
        protected void lsvRejectCandidateList_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            lsvRejectCandidateList.Visible = true;
            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                RejectCandidate candidatInfo = ((ListViewDataItem)e.Item).DataItem as RejectCandidate;

                if (candidatInfo != null)
                {
                    divExportButton.Visible = true ; 
                    HyperLink lnkCandidateName = (HyperLink)e.Item.FindControl("lnkCandidateName");
                    Label lblCandidateID = (Label)e.Item.FindControl("lblCandidateID");
                    Label lblNickName = (Label)e.Item.FindControl("lblNickName");
                    Label lblPrimaryEmail = (Label)e.Item.FindControl("lblPrimaryEmail");
                    Label lblAlternateEmail = (Label)e.Item.FindControl("lblAlternateEmail");
                    Label lblPermanentAddress = (Label)e.Item.FindControl("lblPermanentAddress");
                    Label lblPermanentCity = (Label)e.Item.FindControl("lblPermanentCity");
                    Label lblPermanentState = (Label)e.Item.FindControl("lblPermanentState");
                    Label lblPermanentZip = (Label)e.Item.FindControl("lblPermanentZip");
                    Label lblPermanentCountry = (Label)e.Item.FindControl("lblPermanentCountry");
                    Label lblPermanentPhone = (Label)e.Item.FindControl("lblPermanentPhone");
                    Label lblPermanentMobile = (Label)e.Item.FindControl("lblPermanentMobile");
                    Label lblCurrentAddress = (Label)e.Item.FindControl("lblCurrentAddress");
                    Label lblCurrentCity = (Label)e.Item.FindControl("lblCurrentCity");
                    Label lblCurrentState = (Label)e.Item.FindControl("lblCurrentState");
                    Label lblCurrentZip = (Label)e.Item.FindControl("lblCurrentZip");
                    Label lblCurrentCountry = (Label)e.Item.FindControl("lblCurrentCountry");
                    Label lblPrimaryPhone = (Label)e.Item.FindControl("lblPrimaryPhone");
                    Label lblCellPhone = (Label)e.Item.FindControl("lblCellPhone");
                    Label lblHomePhone = (Label)e.Item.FindControl("lblHomePhone");
                    Label lblOfficePhone = (Label)e.Item.FindControl("lblOfficePhone");
                    Label lblCurrentPosition = (Label)e.Item.FindControl("lblCurrentPosition");
                    Label lblCurrentCompany = (Label)e.Item.FindControl("lblCurrentCompany");
                    Label lblDateOfBirth = (Label)e.Item.FindControl("lblDateOfBirth");
                    Label lblCityOfBirth = (Label)e.Item.FindControl("lblCityOfBirth");
                    Label lblCountryOfBirth = (Label)e.Item.FindControl("lblCountryOfBirth");
                    Label lblCitizenship = (Label)e.Item.FindControl("lblCitizenship");
                    Label lblGender = (Label)e.Item.FindControl("lblGender");
                    Label lblMaritalStatus = (Label)e.Item.FindControl("lblMaritalStatus");
                    Label lblYearsOfExperience = (Label)e.Item.FindControl("lblYearsOfExperience");
                    Label lblAvailability = (Label)e.Item.FindControl("lblAvailability");
                    Label lblWillingToTravel = (Label)e.Item.FindControl("lblWillingToTravel");
                   Label lblCurrentYearlyRate = (Label)e.Item.FindControl("lblCurrentYearlyRate");
                    Label lblExpectedYearlyRate = (Label)e.Item.FindControl("lblExpectedYearlyRate");
                    Label lblCreator = (Label)e.Item.FindControl("lblCreator");
                    Label lblCreateDate = (Label)e.Item.FindControl("lblCreateDate");
                    Label lblLastUpdator = (Label)e.Item.FindControl("lblLastUpdator");
                    Label lblLastUpdateDate = (Label)e.Item.FindControl("lblLastUpdateDate");
                    Label lblWorkSchedule = (Label)e.Item.FindControl("lblWorkSchedule");
                    Label lblWebsite = (Label)e.Item.FindControl("lblWebsite");
                    Label lblLinkedInProfile = (Label)e.Item.FindControl("lblLinkedInProfile");
                    Label lblPassportStatus = (Label)e.Item.FindControl("lblPassportStatus");
                    Label lblIDProof = (Label)e.Item.FindControl("lblIDProof");
                    Label lblHighestEducation = (Label)e.Item.FindControl("lblHighestEducation");
                    Label lblSource = (Label)e.Item.FindControl("lblSource");
                    Label lblSourceDescription = (Label)e.Item.FindControl("lblSourceDescription");
				    Label lblNoticePeriod = (Label)e.Item.FindControl("lblNoticePeriod");
                    Label lblDepartment = (Label)e.Item.FindControl("lblDepartment");
                    Label lblJobCode = (Label)e.Item.FindControl("lblJobCode");
                    Label lblFeedbackStatus = (Label)e.Item.FindControl("lblFeedbackStatus");
                    Label lblHiringManager = (Label)e.Item.FindControl("lblHiringManager");
                    Label lblRejectDetails = (Label)e.Item.FindControl("lblRejectDetails");
                    Label lblRejectedDate = (Label)e.Item.FindControl("lblRejectedDate");
                    Label lblJobTitle = (Label)e.Item.FindControl("lblJobTitle");

                    lblCandidateID.Text = "A" + candidatInfo.Id.ToString ();
                    string strCurrentAddress = candidatInfo.PermanentAddressLine1;
                    string strPermanentAddress = candidatInfo.CurrentAddressLine1;
                    string strFullName = MiscUtil.GetFirstAndLastName(candidatInfo.FirstName, candidatInfo.LastName);
                    if (strFullName.Trim() == "")
                    {
                        strFullName = "No Candidate Name";
                    }
                    if (_IsAccessToEmployee)
                    {
                        if (IdForSitemap > 0 && UrlForEmployee != string.Empty)
                            ControlHelper.SetHyperLink(lnkCandidateName, UrlForEmployee, string.Empty, strFullName, UrlConstants.PARAM_MEMBER_ID, StringHelper.Convert(candidatInfo.Id), UrlConstants.PARAM_SITEMAP_ID, IdForSitemap.ToString(),UrlConstants .PARAM_SITEMAP_PARENT_ID ,UrlConstants .Candidate .ATS_OVERVIEW_SITEMAP_PARENTID );
                    }
                    else lnkCandidateName.Text = strFullName;
                    lblNickName.Text = candidatInfo.NickName;
                    lblPrimaryEmail.Text = candidatInfo.PrimaryEmail;
                    lblAlternateEmail.Text = candidatInfo.AlternateEmail;
                    
                    if (!String.IsNullOrEmpty(strPermanentAddress) && !String.IsNullOrEmpty(candidatInfo.PermanentAddressLine2))
                    {
					strPermanentAddress = strPermanentAddress + "</br>" + candidatInfo.CurrentAddressLine2;
                    }
                    else if (String.IsNullOrEmpty(strPermanentAddress))
                    {
                        strPermanentAddress = candidatInfo.CurrentAddressLine2;
                    }

                    if (!String.IsNullOrEmpty(strCurrentAddress) && !String.IsNullOrEmpty(candidatInfo.CurrentAddressLine2))
                    {
                        strCurrentAddress = strCurrentAddress + "</br>" + candidatInfo.PermanentAddressLine2;;
                    }
                    else if (String.IsNullOrEmpty(strCurrentAddress))
                    {
                        strCurrentAddress = candidatInfo.PermanentAddressLine2;;
                    }

                    lblPermanentAddress.Text = strPermanentAddress;
                    lblPermanentCity.Text = candidatInfo.PermanentCity;
                    lblPermanentState.Text = candidatInfo.CurrentStateName;
                    lblPermanentZip.Text = candidatInfo.CurrentZip;
                    lblPermanentCountry.Text = candidatInfo.CurrentCountryName;
                    string strPermanentPhone = candidatInfo.PermanentPhone;
                    if (!String.IsNullOrEmpty(strPermanentPhone) && !String.IsNullOrEmpty(candidatInfo.PermanentPhoneExt))
                    {
                        strPermanentPhone = strPermanentPhone + " Ext. " + candidatInfo.PermanentPhoneExt;
                    }
                    lblPermanentPhone.Text = strPermanentPhone;
                    lblPermanentMobile.Text = candidatInfo.PermanentMobile;

                    lblCurrentAddress.Text = strCurrentAddress;
                    lblCurrentCity.Text = candidatInfo.CurrentCity;
                    lblCurrentState.Text = candidatInfo.PermanentStateName;
                    lblCurrentZip.Text = candidatInfo.PermanentZip;
                    lblCurrentCountry.Text = candidatInfo.PermanentCountryName;
                    string strPrimaryPhone = candidatInfo.PrimaryPhone;
                    if (!String.IsNullOrEmpty(strPrimaryPhone) && !String.IsNullOrEmpty(candidatInfo.PrimaryPhoneExtension))
                    {
                        strPrimaryPhone = strPrimaryPhone + " Ext. " + candidatInfo.PrimaryPhoneExtension;
                    }
                    lblPrimaryPhone.Text = strPrimaryPhone;
                    lblCellPhone.Text = candidatInfo.CellPhone;
                    lblHomePhone.Text = candidatInfo.HomePhone;
                    string strOfficePhone = candidatInfo.OfficePhone;
                    if (!String.IsNullOrEmpty(strOfficePhone) && !String.IsNullOrEmpty(candidatInfo.OfficePhoneExtension))
                    {
                        strOfficePhone = strOfficePhone + " Ext. " + candidatInfo.OfficePhoneExtension;
                    }
                    lblOfficePhone.Text = strOfficePhone;
                    lblCurrentPosition.Text = candidatInfo.CurrentPosition;
                    lblCurrentCompany.Text = candidatInfo.LastEmployer;
                    if (candidatInfo.DateOfBirth != null && candidatInfo.DateOfBirth != DateTime.MinValue)
                    {
                        lblDateOfBirth.Text = candidatInfo.DateOfBirth.ToShortDateString();
                    }
                    lblCityOfBirth.Text = candidatInfo.CityOfBirth;
                    lblCountryOfBirth.Text = candidatInfo.CitizenshipCountryName;
                    lblCitizenship.Text = candidatInfo.BirthCountryName;
                    lblGender.Text = candidatInfo.Gender;
                    lblMaritalStatus.Text = candidatInfo.MaritalStatus;
                    lblYearsOfExperience.Text = candidatInfo.TotalExperienceYears;
                    lblAvailability.Text = candidatInfo.AvailabilityText;
                    if (candidatInfo.AvailabilityText.IndexOf(" ") > 0 && candidatInfo.AvailableDate != null && candidatInfo.AvailableDate != DateTime.MinValue)
                    {
                        lblAvailability.Text = lblAvailability.Text + " " + candidatInfo.AvailableDate.ToShortDateString();
                    }
                    lblWillingToTravel.Text = candidatInfo.WillingToTravel ? "Yes" : "No";
                    
                    
                    lblWorkSchedule.Text = candidatInfo.WorkSchedule;
                    
                    
                    string strSalary = candidatInfo.CurrentYearlyRate.ToString();
                    if (!String.IsNullOrEmpty(strSalary) && !String.IsNullOrEmpty(candidatInfo.CurrentYearlyCurrency))
                    {
                        strSalary = strSalary + " " + candidatInfo.CurrentYearlyCurrency;
                    }
                    lblCurrentYearlyRate.Text = strSalary;
                    strSalary = candidatInfo.ExpectedYearlyRate.ToString();
                    if (!String.IsNullOrEmpty(strSalary) && !String.IsNullOrEmpty(candidatInfo.ExpectedYearlyCurrency))
                    {
                        strSalary = strSalary + " " + candidatInfo.ExpectedYearlyCurrency;
                    }
                    lblExpectedYearlyRate.Text = strSalary;
                    
                    lblCreator.Text = candidatInfo.CreatorName;
                    lblCreateDate.Text = candidatInfo.CreateDate.ToShortDateString();
                    lblLastUpdator.Text = candidatInfo.LastUpdatorName;
                    lblLastUpdateDate.Text = candidatInfo.UpdateDate.ToShortDateString();

                    lblWebsite.Text = candidatInfo.Website.ToString();
                    lblLinkedInProfile.Text = candidatInfo.LinkedInProfile;
                    lblPassportStatus.Text = candidatInfo.PassportStatus ? "Yes" : "No";
                    lblIDProof.Text = candidatInfo.IDCard;
                    lblHighestEducation.Text = candidatInfo.HighestDegree;
                    lblSource.Text = candidatInfo.Source;
                    lblSourceDescription.Text = candidatInfo.SourceDescription;

                    lblNoticePeriod.Text = candidatInfo.NoticePeriod;
                    lblDepartment.Text = candidatInfo.Department;
                    lblJobCode.Text = candidatInfo.JobCode;
                    lblFeedbackStatus.Text = candidatInfo.FeedbackStatus;
                    lblHiringManager.Text = candidatInfo.HiringManager;

                    if (candidatInfo.RejectedDate != null && candidatInfo.RejectedDate != DateTime.MinValue)
                    {
                        lblRejectedDate.Text = candidatInfo.RejectedDate.ToShortDateString();
                    }
                    lblRejectDetails.Text = candidatInfo.RejectDetails;
                    lblJobTitle.Text = candidatInfo.JobTitle;
               
                    HideListViewItem(e.Item);
                }
            }
        }

        protected void lnkBtnResult_Click(object sender, EventArgs e)
        {
            LinkButton lnkBtnSender = (LinkButton)sender;
            if (lnkBtnSender.Text != "0")
            {
                HideListViewHeaderItem();
                ResetDataSourceParameters();
                switch (lnkBtnSender.ID)
                {
                    case "lnkBtnResultByNew":
                        odsRejectCandidateList.SelectParameters["addedBy"].DefaultValue = ddlAddedByEmployee.SelectedValue;
                        odsRejectCandidateList.SelectParameters["addedBySource"].DefaultValue = "0";
                        break;
                    case "lnkBtnResultByInterviewe":
                      
                            odsRejectCandidateList.SelectParameters["interviewFrom"].DefaultValue = DateTime.MinValue.ToString();
                        
                        odsRejectCandidateList.SelectParameters["interviewTo"].DefaultValue = DateTime.MinValue.ToString();
                        odsRejectCandidateList.SelectParameters["interviewLevel"].DefaultValue = "0";
                        odsRejectCandidateList.SelectParameters["interviewStatus"].DefaultValue = "0";
                        break;
                    case "lnkBtnResultByFunctionalCategory":
                        odsRejectCandidateList.SelectParameters["functionalCategory"].DefaultValue = "0";
                        break;
                    case "lnkBtnResultByGender":
                        odsRejectCandidateList.SelectParameters["gender"].DefaultValue = ddlCandidatesGender.SelectedValue;
                        break;
                    case "lnkBtnResultByEducation":
                        odsRejectCandidateList.SelectParameters["educationId"].DefaultValue = ddlCandidatesEducation.SelectedValue;
                        break;
                    case "lnkResultByIndustry":
                        odsRejectCandidateList.SelectParameters["industry"].DefaultValue = "0";
                        break;
                    case "lnkBtnResultByWorkPermit":
                        odsRejectCandidateList.SelectParameters["workPermit"].DefaultValue = ddlCandidatesWorkPermit.SelectedValue;
                        break;
                    case "lnkBtnResultByMaritalStatus":
                        odsRejectCandidateList.SelectParameters["maritalStatus"].DefaultValue = ddlCandidatesMaritalStatus.SelectedValue;
                        break;
                    case "lnkBtnResultByTakenTests":
                        odsRejectCandidateList.SelectParameters["assessmentId"].DefaultValue = "0";
                        break;
                    case "lnkBtnResultByManager":
                        odsRejectCandidateList.SelectParameters["assignedManager"].DefaultValue = ddlAssignedManager.SelectedValue;
                        break;
                    case "lnkBtnResultByLocation":
                        odsRejectCandidateList.SelectParameters["city"].DefaultValue = txtCity.Text;
                        break;
                    default:
                        break;
                }
                BindList();
            }
        }

       

        #endregion

    }
}