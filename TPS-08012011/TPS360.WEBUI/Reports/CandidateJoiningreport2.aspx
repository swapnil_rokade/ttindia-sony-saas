﻿<%---------------------------------------------------------------------------------------------------------------------------------------------
    FileName: CandidateJoiningreport1.aspx
    Description: This is the page used to display the candidate joining reports in excel,word,Pdf formats.
    Created By: Kanchan Yeware
    Created On: 09-Aug-2016
    Modification Log:     
--%>

<%@ Page Language="C#" MasterPageFile="~/Default.master" AutoEventWireup="true" CodeFile="CandidateJoiningreport2.aspx.cs"
    Inherits="TPS360.Web.UI.Reports.CandidateJoiningreport2" Title="Candidate Joining Report - 2"
    EnableViewStateMac="false" %>

<%@ Register Src="~/Controls/DateRangePicker.ascx" TagName="DateRangePicker" TagPrefix="ucl" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Controls/PagerControl.ascx" TagName="Pager" TagPrefix="ucl" %>
<%@ Register Src="~/Controls/ConfirmationWindow.ascx" TagName="Confirm" TagPrefix="ucl" %>
<%@ Register Src="~/Controls/CountryStatePicker.ascx" TagName="CountryState" TagPrefix="ucl" %>
<asp:Content ID="Content2" ContentPlaceHolderID="cphHomeMasterTitle" runat="Server">
    Candidate Joining Report - 2
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphHomeMaster" runat="Server">

    <script type="text/javascript" language="javascript" src="../js/AjaxVariables.js"></script>

    <script type="text/javascript" language="javascript" src="../js/AjaxScript.js"></script>

    <script language="javascript" type="text/javascript">
        //To Keep the scroll bar in the Exact Place
        Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(PageLoadedReq);
        function PageLoadedReq(sender, args) {
            try {
                var hdnScroll = document.getElementById('ctl00_cphHomeMaster_hdnScrollPos');
                var bigDiv = document.getElementById('bigDiv');
                bigDiv.scrollLeft = hdnScroll.value;
            }
            catch (e) {
            }
        }
        function SetScrollPosition() {
            var hdnScroll = document.getElementById('ctl00_cphHomeMaster_hdnScrollPos');
            var bigDiv = document.getElementById('bigDiv');
            hdnScroll.value = bigDiv.scrollLeft;
        }

        function SelectUnselectColumnOption(chkColumnOption) {
            var chkBoxAll = $get('<%= chkColumns.ClientID%>');
            var chkBoxList = $get('<%= chkColumnList.ClientID%>');
            var chkBoxCount = chkBoxList.getElementsByTagName("input");
            if (chkBoxAll.checked) {
                for (var i = 0; i < chkBoxCount.length; i++) {
                    chkBoxCount[i].checked = true;
                }
            }
            if (!chkBoxAll.checked) {
                for (var i = 0; i < chkBoxCount.length; i++) {
                    chkBoxCount[i].checked = false;
                }
            }
        }
        function SelectUnselectAllColumnOption(chkColumnOptionHeader) {
            var checkedCount = 0;
            //debugger;
            var chkBoxAll = $get('<%= chkColumns.ClientID%>');
            var chkBoxList = $get('<%= chkColumnList.ClientID%>');
            var chkBoxCount = chkBoxList.getElementsByTagName("input");
            for (var i = 0; i < chkBoxCount.length; i++) {
                if (chkBoxCount[i].checked) {
                    checkedCount++;
                }
            }
            if (checkedCount == chkBoxCount.length) {
                chkBoxAll.checked = true;
            }
            else {
                chkBoxAll.checked = false;
            }
        }
    </script>

    <style>  
       .TableFormLeble
        {
            width: 25%;
        }      
        .Pager th a {        
            width:22px;
        }
        
        .Pager th a#PagerIDMain {        
            width:110px;
        }
      
        .Pager th {font-weight: normal !important;}
     </style>
    <ucl:Confirm ID="uclConfirm" runat="server" />
    <asp:UpdatePanel ID="pnlCandidatereports" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:HiddenField ID="hdnScrollPos" runat="server" />
            <asp:TextBox ID="txtSortColumn" runat="server" Visible="false"></asp:TextBox>
            <asp:TextBox ID="txtSortOrder" runat="server" Visible="false"></asp:TextBox>
            <asp:TextBox ID="SortColumn" runat="server" EnableViewState="true" Visible="false" />
            <asp:TextBox ID="SortOrder" runat="server" EnableViewState="true" Visible="false" />
            <asp:ObjectDataSource ID="odsCandidateList" runat="server" SelectMethod="GetCandidateJoiningReport2"
                TypeName="TPS360.Web.UI.CandidateJoiningDataSource" SelectCountMethod="GetListCount1"
                EnablePaging="true" SortParameterName="sortExpression">
                <SelectParameters>
                    <asp:ControlParameter ControlID="wdcCandidatesAdded" Name="addedFrom" PropertyName="StartDate" />
                    <asp:ControlParameter ControlID="wdcCandidatesAdded" Name="addedTo" PropertyName="EndDate" />
                    <asp:ControlParameter ControlID="txtSuperVisoryCode" Name="SuperVisoryCode" PropertyName="Text" />
                    <asp:Parameter Name="MemberId" />
                    <asp:Parameter Name="SortOrder" DefaultValue="asc" />
                    <asp:Parameter Name="sortExpression" />
                    <asp:Parameter Name="maximumRows" />
                    <asp:Parameter Name="startRowIndex" />
                </SelectParameters>
            </asp:ObjectDataSource>
            <asp:Panel ID="pnlSearchRegion" runat="server" DefaultButton="btnSearch">
                <div style="width: 100%;">
                    <div>
                        <div class="TableRow">
                            <div class="TabPanelHeader nomargin" style="border-style: none none none;">
                                Filter Options
                            </div>
                        </div>
                        <div class="TableRow" style="text-align: center">
                            <asp:Label ID="lblMessage" runat="server" EnableViewState="false"></asp:Label>
                        </div>
                        <div class="FormLeftColumn" style="width: 49%">
                            <div class="TableRow" style="white-space: nowrap">
                                <div class="TableFormLeble" style="width: 40%;">
                                    <asp:Label ID="lblCandidatesAdded" runat="server" EnableViewState="false" Text="Date"></asp:Label>
                                </div>
                                <div class="TableFormContent">
                                    <ucl:DateRangePicker ID="wdcCandidatesAdded" runat="server" />
                                </div>
                            </div>
                        </div>
                        <div class="TableRow" style="white-space: nowrap">
                            <div class="TableFormLeble" style="width: 19.5%;">
                                <asp:Label ID="lblSuperVisoryCode" runat="server" EnableViewState="false" Text="Supervisory Organization Code"> </asp:Label>
                            </div>
                            <div class="TableFormContent">
                                <asp:TextBox ID="txtSuperVisoryCode" runat="server" EnableViewState="false" CssClass="SearchTextbox"></asp:TextBox>
                            </div>
                        </div>
                        <div style="text-align: left">
                            <asp:Label ID="lbMessage" runat="server" EnableViewState="false"></asp:Label>
                        </div>
                        <div class="TableRow well well-small nomargin">
                            <asp:CollapsiblePanelExtender ID="cpnlColumnSelection" runat="server" TargetControlID="pnlColumnSelectionBody"
                                ExpandControlID="pnlColumnSelectionHeader" CollapseControlID="pnlColumnSelectionHeader"
                                Collapsed="true" ImageControlID="imgShowHideColumnSelection" CollapsedImage="~/Images/expand-plus.png"
                                ExpandedImage="~/Images/collapse-minus.png" SuppressPostBack="true">
                            </asp:CollapsiblePanelExtender>
                            <asp:Panel ID="pnlColumnSelectionHeader" runat="server">
                                <div class="" style="clear: both; cursor: pointer">
                                    <asp:Image ID="imgShowHideColumnSelection" ImageUrl="~/Images/expand-plus.png" runat="server" />
                                    <asp:Label ID="lblDisplayColumns" runat="server" Text="Included Columns" EnableViewState="false"></asp:Label>
                                </div>
                            </asp:Panel>
                            <asp:Panel ID="pnlColumnSelectionBody" runat="server" Style="overflow: hidden;" Height="0">
                                <div class="TableRow" style="text-align: left; padding-top: 5px">
                                    <asp:CheckBox ID="chkColumns" runat="server" CssClass="ColumnSelection" Text="Select All"
                                        onclick="javascript:SelectUnselectColumnOption(this);" />
                                    <asp:CheckBoxList ID="chkColumnList" runat="server" RepeatDirection="Horizontal"
                                        RepeatColumns="4" CssClass="ColumnSelection" onclick="javascript:SelectUnselectAllColumnOption('chkColumns')">
                                        <asp:ListItem Value="DOJ" Selected="True">DOJ</asp:ListItem>
                                        <asp:ListItem Value="DOC">DOC</asp:ListItem>
                                        <asp:ListItem Value="GID">GID</asp:ListItem>
                                        <asp:ListItem Value="Position">Position</asp:ListItem>
                                        <asp:ListItem Value="Name" Selected="True">Name</asp:ListItem>
                                        <asp:ListItem Value="FirstName">Local Given Name</asp:ListItem>
                                        <asp:ListItem Value="MiddleName">Local Middle Name</asp:ListItem>
                                        <asp:ListItem Value="LastName">Local Family Name </asp:ListItem>
                                        <asp:ListItem Value="mailID" Selected="True">Mail ID</asp:ListItem>
                                        <asp:ListItem Value="DOB">DOB</asp:ListItem>
                                        <asp:ListItem Value="MaritalStatus">Marital Status</asp:ListItem>
                                        <asp:ListItem Value="Gender">Gender</asp:ListItem>
                                        <asp:ListItem Value="NationalIDType">National ID Type</asp:ListItem>
                                        <asp:ListItem Value="NationalID">National ID</asp:ListItem>
                                        <asp:ListItem Value="PhoneNumber" Selected="True">Phone Number</asp:ListItem>
                                        <asp:ListItem Value="PhoneDevice">Home Number</asp:ListItem>
                                        <asp:ListItem Value="PhoneType">Office Number</asp:ListItem>
                                        <asp:ListItem Value="Address" Selected="True">Address</asp:ListItem>
                                        <asp:ListItem Value="VendorName">Supplier Information</asp:ListItem>
                                        <asp:ListItem Value="Location">Location</asp:ListItem>
                                        <asp:ListItem Value="ReasonForHire" Selected="True">Hire Reason</asp:ListItem>
                                        <asp:ListItem Value="EmployeeType">Employee Type</asp:ListItem>
                                        <asp:ListItem Value="SubtypeofCW">Sub-Type Of CW</asp:ListItem>
                                        <asp:ListItem Value="IndividualContributor">Individual Contributor/ Management</asp:ListItem>
                                        <asp:ListItem Value="JobFamily" Selected="True">Job Family</asp:ListItem>
                                        <asp:ListItem Value="JobProfiles">Job Profile Code</asp:ListItem>
                                        <asp:ListItem Value="JobProfileName">Job Profile Name</asp:ListItem>
                                        <asp:ListItem Value="SupervisoryOrgCode" Selected="True">Supervisory Organization Code</asp:ListItem>
                                        <asp:ListItem Value="SupervisoryOrgShortText">Supervisory Organization Short Text</asp:ListItem>
                                        <asp:ListItem Value="SupervisoryOrgDesc" Selected="True">Supervisory Organization Description</asp:ListItem>
                                        <asp:ListItem Value="CostCenter">Cost Center Code</asp:ListItem>
                                        <asp:ListItem Value="Designation">Designation</asp:ListItem>
                                        <asp:ListItem Value="GlobalGrade">Sony Global Grade</asp:ListItem>
                                        <asp:ListItem Value="Currency">Currency</asp:ListItem>
                                        <asp:ListItem Value="Frequency">Frequency(Monthly/Annual)</asp:ListItem>
                                        <asp:ListItem Value="CompensationPlan">Compensation Plan</asp:ListItem>
                                        <asp:ListItem Value="OfferHead1">Total Base Pay</asp:ListItem>
                                        <asp:ListItem Value="OfferHead2">Type 1 (Housing Allowance)</asp:ListItem>
                                        <asp:ListItem Value="OfferHead3">Type 2 (Conveyance Allowance)</asp:ListItem>
                                        <asp:ListItem Value="OfferHead4">Type 3 (Other Allowance)</asp:ListItem>
                                        <asp:ListItem Value="OfferHead5">Type 4 (Flexible Compensation Allowance)</asp:ListItem>
                                        <asp:ListItem Value="OfferHead6">Bonus Plan</asp:ListItem>
                                        <asp:ListItem Value="OfferHead7">Bonus Percentage</asp:ListItem>
                                        <asp:ListItem Value="ProjectName">Project Name</asp:ListItem>
                                        <asp:ListItem Value="Division">Division</asp:ListItem>
                                        <asp:ListItem Value="ReportMangGID">Reporting Manager GID</asp:ListItem>
                                        <asp:ListItem Value="ManagerName">Manager Name</asp:ListItem>
                                        <asp:ListItem Value="OrgUnitHead">Org Unit Head</asp:ListItem>
                                    </asp:CheckBoxList>
                                </div>
                            </asp:Panel>
                        </div>
                        <div class="TableRow" style="text-align: center; padding-top: 5px; padding-bottom: 5px;">
                            <asp:LinkButton ID="btnSearch" runat="server" Text="Search" AlternateText="Search"
                                EnableViewState="false" TabIndex="12" CssClass="btn btn-primary" ValidationGroup="Search"
                                OnClick="btnSearch_Click">
                            <i class="icon-search icon-white"></i> Search</asp:LinkButton>
                            <asp:Button ID="btnClear" runat="server" Text="Clear" AlternateText="Clear" CssClass="btn"
                                EnableViewState="false" CausesValidation="false" TabIndex="13" OnClick="btnClear_Click" />
                        </div>
                        <div class="TableRow" style="padding-bottom: 5px;" id="divExportButton" runat="server"
                            visible="false">
                            <div class="TableRow">
                                <div class="TabPanelHeader nomargin" style="margin-bottom: 5px; border-style: none none none;">
                                    Report Results</div>
                            </div>
                            <asp:ImageButton ID="btnExportToExcel" CssClass="btn" TabIndex="14" SkinID="sknExportToExcel"
                                runat="server" ToolTip="Export To Excel" />
                            <asp:ImageButton ID="btnExportToPDF" CssClass="btn" TabIndex="15" SkinID="sknExportToPDF"
                                runat="server" ToolTip="Export To PDF" />
                            <asp:ImageButton ID="btnExportToWord" CssClass="btn" TabIndex="16" SkinID="sknExportToWord"
                                runat="server" ToolTip="Export To Word" />
                        </div>
                        <div id="divCandidateList" runat="server">
                            <asp:Panel ID="pnlCandidateListBody" runat="server">
                                <div class="GridContainer" style="overflow: auto; overflow-y: hidden; width: 100%;
                                    text-align: left;" id="bigDiv" onscroll='SetScrollPosition()'>
                                    <asp:ListView ID="lsvCandidateList" runat="server" DataKeyNames="ID" OnItemDataBound="lsvCandidateList_ItemDataBound"
                                        OnItemCommand="lsvCandidateList_ItemCommand" OnPreRender="lsvCandidateList_PreRender">
                                        <LayoutTemplate>
                                            <table id="tlbTemplate" class="Grid ReportGrid" cellspacing="0" border="0">
                                                <tr id="trHeader" runat="server">
                                                    <th id="thDOJ" runat="server" style="min-width: 100px; white-space: nowrap" enableviewstate="false">
                                                        <asp:LinkButton ID="lnkDOJ" runat="server" TabIndex="1" ToolTip="Sort By Date Of Joining"
                                                            CommandName="Sort" CommandArgument="[CJ].[DateOfJoining]" Text="DOJ" />
                                                    </th>
                                                    <th runat="server" id="thDOC" style="min-width: 100px" enableviewstate="false">
                                                        DOC
                                                    </th>
                                                    <th runat="server" id="thGID" style="min-width: 100px" enableviewstate="false">
                                                        GID
                                                    </th>
                                                    <th runat="server" id="thPosition" style="min-width: 100px" enableviewstate="false">
                                                        Position
                                                    </th>
                                                    <th runat="server" id="thName" style="min-width: 100px">
                                                        <asp:LinkButton ID="lnkBtnName" runat="server" TabIndex="2" ToolTip="Sort By Name"
                                                            CommandName="Sort" CommandArgument="[C].[FirstName]" Text="Name" />
                                                    </th>
                                                    <th runat="server" id="thFirstName" style="min-width: 100px" enableviewstate="false">
                                                        <asp:LinkButton ID="lnkFirstName" runat="server" TabIndex="3" ToolTip="Sort By Local Given Name"
                                                            CommandName="Sort" CommandArgument="[C].[FirstName]" Text="Local Given Name" />
                                                    </th>
                                                    <th runat="server" id="thMiddleName" style="min-width: 100px" enableviewstate="false">
                                                        <asp:LinkButton ID="lnkMiddleName" runat="server" TabIndex="4" ToolTip="Sort By Local Middle Name"
                                                            CommandName="Sort" CommandArgument="[C].[MiddleName]" Text=" Local Middle Name" />
                                                    </th>
                                                    <th runat="server" id="thLastName" style="min-width: 100px" enableviewstate="false">
                                                        <asp:LinkButton ID="lnkLastName" runat="server" TabIndex="5" ToolTip="Sort By Local Family Name"
                                                            CommandName="Sort" CommandArgument="[C].[LastName]" Text="Local Family Name" />
                                                    </th>
                                                    <th runat="server" id="thmailID" style="min-width: 100px" enableviewstate="false">
                                                        Mail ID
                                                    </th>
                                                    <th runat="server" id="thDOB" style="min-width: 100px" enableviewstate="false">
                                                        <asp:LinkButton ID="lnkDateOfBirth" runat="server" TabIndex="6" ToolTip="Sort By DOB"
                                                            CommandName="Sort" CommandArgument="[C].[DateOfBirth]" Text="DOB" />
                                                    </th>
                                                    <th runat="server" id="thMaritalStatus" style="min-width: 100px" enableviewstate="false">
                                                        Marital Status
                                                    </th>
                                                    <th runat="server" id="thGender" style="min-width: 100px" enableviewstate="false">
                                                        Gender
                                                    </th>
                                                    <th runat="server" id="thNationalIDType" style="min-width: 100px" enableviewstate="false">
                                                        National ID Type
                                                    </th>
                                                    <th runat="server" id="thNationalID" style="min-width: 100px" enableviewstate="false">
                                                        National ID
                                                    </th>
                                                    <th runat="server" id="thPhoneNumber" style="min-width: 100px" enableviewstate="false">
                                                        Phone Number
                                                    </th>
                                                    <th runat="server" id="thPhoneDevice" style="min-width: 100px" enableviewstate="false">
                                                        Home Number
                                                    </th>
                                                    <th runat="server" id="thPhoneType" style="min-width: 100px" enableviewstate="false">
                                                        Office Number
                                                    </th>
                                                    <th runat="server" id="thAddress" style="min-width: 100px" enableviewstate="false">
                                                        Address
                                                    </th>
                                                    <th runat="server" id="thVendorName" style="min-width: 100px" enableviewstate="false">
                                                        Supplier Information
                                                    </th>
                                                    <th runat="server" id="thLocation" style="min-width: 100px" enableviewstate="false">
                                                        <asp:LinkButton ID="lnkLocation" runat="server" TabIndex="7" ToolTip="Sort By Location"
                                                            CommandName="Sort" CommandArgument="[J].[City]" Text="Location" />
                                                    </th>
                                                    <th runat="server" id="thReasonForHire" style="min-width: 100px" enableviewstate="false">
                                                        Hire Reason
                                                    </th>
                                                    <th runat="server" id="thEmployeeType" style="min-width: 100px" enableviewstate="false">
                                                        Employee Type
                                                    </th>
                                                    <th runat="server" id="thSubtypeofCW" style="min-width: 100px" enableviewstate="false">
                                                        Subtype of CW
                                                    </th>
                                                    <th runat="server" id="thIndividualContributor" style="min-width: 100px" enableviewstate="false">
                                                        Individual Contributor/ Management
                                                    </th>
                                                    <th runat="server" id="thJobFamily" style="min-width: 100px" enableviewstate="false">
                                                        Job Family
                                                    </th>
                                                    <th runat="server" id="thJobProfiles" style="min-width: 100px" enableviewstate="false">
                                                        Job Profile Code
                                                    </th>
                                                    <th runat="server" id="thJobProfileName" style="min-width: 100px" enableviewstate="false">
                                                        Job Profile Name
                                                    </th>
                                                    <th runat="server" id="thSupervisoryOrgCode" style="min-width: 100px" enableviewstate="false">
                                                        <asp:LinkButton ID="lnkSupervisoryOrgCode" runat="server" TabIndex="8" ToolTip="Sort By Supervisory Organization Code"
                                                            CommandName="Sort" CommandArgument="[Org].[OrgUnitCode]" Text="Supervisory Organization Code" />
                                                    </th>
                                                    <th runat="server" id="thSupervisoryOrgShortText" style="min-width: 100px" enableviewstate="false">
                                                        <asp:LinkButton ID="lnkSupervisoryOrgShortText" runat="server" TabIndex="9" ToolTip="Sort By Supervisory Organization Short Text"
                                                            CommandName="Sort" CommandArgument="[Org].[ReportingOrgStTxt]" Text="Supervisory Organization Short Text" />
                                                    </th>
                                                    <th runat="server" id="thSupervisoryOrgDesc" style="min-width: 100px" enableviewstate="false">
                                                        Supervisory Organization Description
                                                    </th>
                                                    <th runat="server" id="thCostCenter" style="min-width: 100px" enableviewstate="false">
                                                        Cost Centre Code
                                                    </th>
                                                    <th runat="server" id="thDesignation" style="min-width: 100px" enableviewstate="false">
                                                        Designation
                                                    </th>
                                                    <th runat="server" id="thGlobalGrade" style="min-width: 100px" enableviewstate="false">
                                                        Sony Global Grade
                                                    </th>
                                                    <th runat="server" id="thCurrency" style="min-width: 100px" enableviewstate="false">
                                                        Currency
                                                    </th>
                                                    <th runat="server" id="thFrequency" style="min-width: 100px" enableviewstate="false">
                                                        Frequency(Monthly/Annual)
                                                    </th>
                                                    <th runat="server" id="thCompensationPlan" style="min-width: 100px" enableviewstate="false">
                                                        Compensation Plan
                                                    </th>
                                                    <th runat="server" id="thOfferHead1" style="min-width: 100px" enableviewstate="false">
                                                        Total Base Pay
                                                    </th>
                                                    <th runat="server" id="thOfferHead2" style="min-width: 100px" enableviewstate="false">
                                                        Type 1 (Housing Allowance)
                                                    </th>
                                                    <th runat="server" id="thOfferHead3" style="min-width: 100px" enableviewstate="false">
                                                        Type 2 (Conveyance Allowance)
                                                    </th>
                                                    <th runat="server" id="thOfferHead4" style="min-width: 100px" enableviewstate="false">
                                                        Type 3 (Other Allowance)
                                                    </th>
                                                    <th runat="server" id="thOfferHead5" style="min-width: 100px" enableviewstate="false">
                                                        Type 4 (Flexible Compensation Allowance)
                                                    </th>
                                                    <th runat="server" id="thOfferHead6" style="min-width: 100px" enableviewstate="false">
                                                        Bonus Plan
                                                    </th>
                                                    <th runat="server" id="thOfferHead7" style="min-width: 100px" enableviewstate="false">
                                                        Bonus Percentage
                                                    </th>
                                                    <th runat="server" id="thProjectName" style="min-width: 100px" enableviewstate="false">
                                                        Project Name
                                                    </th>
                                                    <th runat="server" id="thDivision" style="min-width: 100px" enableviewstate="false">
                                                        Division
                                                    </th>
                                                    <th runat="server" id="thReportMangGID" style="min-width: 100px" enableviewstate="false">
                                                        Reporting Manager GID
                                                    </th>
                                                    <th runat="server" id="thManagerName" style="min-width: 100px" enableviewstate="false">
                                                        Manager Name
                                                    </th>
                                                    <th runat="server" id="thOrgUnitHead" style="min-width: 100px" enableviewstate="false">
                                                        <asp:Label runat="server" ID="lnkNotes" Text="Org unit Head" EnableViewState="false"></asp:Label>
                                                    </th>
                                                </tr>
                                                <tr id="itemPlaceholder" runat="server">
                                                </tr>
                                                <tr class="Pager">
                                                    <th id="tdpager" runat="server" colspan="9">
                                                        <ucl:Pager ID="pagerControl" runat="server" EnableViewState="true" />
                                                    </th>
                                                </tr>
                                            </table>
                                        </LayoutTemplate>
                                        <EmptyDataTemplate>
                                            <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                                                <tr>
                                                    <td>
                                                        No data was returned.
                                                    </td>
                                                </tr>
                                            </table>
                                        </EmptyDataTemplate>
                                        <ItemTemplate>
                                            <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                                                <td runat="server" id="tdDOJ" style="text-align: left;">
                                                    <asp:Label runat="server" ID="lblDOJ"></asp:Label>
                                                </td>
                                                <td runat="server" id="tdDOC" style="text-align: left;">
                                                    <asp:Label runat="server" ID="lblDOC"></asp:Label>
                                                </td>
                                                <td runat="server" id="tdGID">
                                                    <asp:Label runat="server" ID="lblGID"></asp:Label>
                                                </td>
                                                <td runat="server" id="tdPosition">
                                                    <asp:Label runat="server" ID="lblPosition"></asp:Label>
                                                </td>
                                                <td runat="server" id="tdName">
                                                    <asp:Label ID="lblName" runat="server"></asp:Label>
                                                </td>
                                                <td runat="server" id="tdFirstName">
                                                    <asp:Label ID="lblFirstName" runat="server"></asp:Label>
                                                </td>
                                                <td runat="server" id="tdMiddleName">
                                                    <asp:Label ID="lblMiddleName" runat="server"></asp:Label>
                                                </td>
                                                <td runat="server" id="tdLastName">
                                                    <asp:Label ID="lblLastName" runat="server"></asp:Label>
                                                </td>
                                                <td runat="server" id="tdmailID">
                                                    <asp:Label ID="lblmailID" runat="server"></asp:Label>
                                                </td>
                                                <td runat="server" id="tdDOB">
                                                    <asp:Label ID="lblDOB" runat="server"></asp:Label>
                                                </td>
                                                <td runat="server" id="tdMaritalStatus">
                                                    <asp:Label ID="lblMaritalStatus" runat="server"></asp:Label>
                                                </td>
                                                <td runat="server" id="tdGender">
                                                    <asp:Label ID="lblGender" runat="server"></asp:Label>
                                                </td>
                                                <td runat="server" id="tdNationalIDType">
                                                    <asp:Label ID="lblNationalIDType" runat="server"></asp:Label>
                                                </td>
                                                <td runat="server" id="tdNationalID">
                                                    <asp:Label ID="lblNationalID" runat="server"></asp:Label>
                                                </td>
                                                <td runat="server" id="tdPhoneNumber">
                                                    <asp:Label ID="lblPhoneNumber" runat="server"></asp:Label>
                                                </td>
                                                <td runat="server" id="tdPhoneDevice">
                                                    <asp:Label ID="lblPhoneDevice" runat="server"></asp:Label>
                                                </td>
                                                <td runat="server" id="tdPhoneType">
                                                    <asp:Label ID="lblPhoneType" runat="server"></asp:Label>
                                                </td>
                                                <td runat="server" id="tdAddress">
                                                    <asp:Label ID="lblAddress" runat="server"></asp:Label>
                                                </td>
                                                <td runat="server" id="tdVendorName">
                                                    <asp:Label ID="lblVendorName" runat="server"></asp:Label>
                                                </td>
                                                <td runat="server" id="tdLocation">
                                                    <asp:Label ID="lblLocation" runat="server"></asp:Label>
                                                </td>
                                                <td runat="server" id="tdReasonForHire">
                                                    <asp:Label ID="lblReasonForHire" runat="server"></asp:Label>
                                                </td>
                                                <td runat="server" id="tdEmployeeType">
                                                    <asp:Label ID="lblEmployeeType" runat="server"></asp:Label>
                                                </td>
                                                <td runat="server" id="tdSubtypeofCW">
                                                    <asp:Label ID="lblSubtypeofCW" runat="server"></asp:Label>
                                                </td>
                                                <td runat="server" id="tdIndividualContributor">
                                                    <asp:Label ID="lblIndividualContributor" runat="server"></asp:Label>
                                                </td>
                                                <td runat="server" id="tdJobFamily">
                                                    <asp:Label ID="lblJobFamily" runat="server"></asp:Label>
                                                </td>
                                                <td runat="server" id="tdJobProfiles">
                                                    <asp:Label ID="lblJobProfiles" runat="server"></asp:Label>
                                                </td>
                                                <td runat="server" id="tdJobProfileName">
                                                    <asp:Label ID="lblJobProfileName" runat="server"></asp:Label>
                                                </td>
                                                <td runat="server" id="tdSupervisoryOrgCode">
                                                    <asp:Label ID="lblSupervisoryOrgCode" runat="server"></asp:Label>
                                                </td>
                                                <td runat="server" id="tdSupervisoryOrgShortText">
                                                    <asp:Label ID="lblSupervisoryOrgShortText" runat="server"></asp:Label>
                                                </td>
                                                <td runat="server" id="tdSupervisoryOrgDesc">
                                                    <asp:Label ID="lblSupervisoryOrgDesc" runat="server"></asp:Label>
                                                </td>
                                                <td runat="server" id="tdCostCenter">
                                                    <asp:Label ID="lblCostCenter" runat="server"></asp:Label>
                                                </td>
                                                <td runat="server" id="tdDesignation">
                                                    <asp:Label ID="lblDesignation" runat="server"></asp:Label>
                                                </td>
                                                <td runat="server" id="tdGlobalGrade">
                                                    <asp:Label ID="lblGlobalGrade" runat="server"></asp:Label>
                                                </td>
                                                <td runat="server" id="tdCurrency">
                                                    <asp:Label ID="lblCurrency" runat="server"></asp:Label>
                                                </td>
                                                <td runat="server" id="tdFrequency">
                                                    <asp:Label ID="lblFrequency" runat="server"></asp:Label>
                                                </td>
                                                <td runat="server" id="tdCompensationPlan">
                                                    <asp:Label ID="lblCompensationPlan" runat="server"></asp:Label>
                                                </td>
                                                <td runat="server" id="tdOfferHead1" style="text-align:right !important;">
                                                    <asp:Label ID="lblOfferHead1" runat="server"></asp:Label>
                                                </td>
                                                <td runat="server" id="tdOfferHead2" style="text-align:right !important;">
                                                    <asp:Label ID="lblOfferHead2" runat="server"></asp:Label>
                                                </td>
                                                <td runat="server" id="tdOfferHead3" style="text-align:right !important;">
                                                    <asp:Label ID="lblOfferHead3" runat="server"></asp:Label>
                                                </td>
                                                <td runat="server" id="tdOfferHead4" style="text-align:right !important;">
                                                    <asp:Label ID="lblOfferHead4" runat="server"></asp:Label>
                                                </td>
                                                <td runat="server" id="tdOfferHead5" style="text-align:right !important;">
                                                    <asp:Label ID="lblOfferHead5" runat="server"></asp:Label>
                                                </td>
                                                <td runat="server" id="tdOfferHead6" style="text-align:right !important;">
                                                    <asp:Label ID="lblOfferHead6" runat="server"></asp:Label>
                                                </td>
                                                <td runat="server" id="tdOfferHead7" style="text-align:right !important;">
                                                    <asp:Label ID="lblOfferHead7" runat="server"></asp:Label>
                                                </td>
                                                <td runat="server" id="tdProjectName">
                                                    <asp:Label ID="lblProjectName" runat="server"></asp:Label>
                                                </td>
                                                <td runat="server" id="tdDivision">
                                                    <asp:Label ID="lblDivision" runat="server"></asp:Label>
                                                </td>
                                                <td runat="server" id="tdReportMangGID">
                                                    <asp:Label ID="lblReportMangGID" runat="server"></asp:Label>
                                                </td>
                                                <td runat="server" id="tdManagerName">
                                                    <asp:Label ID="lblManagerName" runat="server"></asp:Label>
                                                </td>
                                                <td runat="server" id="tdOrgUnitHead">
                                                    <asp:Label ID="lblOrgUnitHead" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:ListView>
                                </div>
                            </asp:Panel>
                        </div>
                    </div>
                </div>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnExportToPDF" />
            <asp:PostBackTrigger ControlID="btnExportToExcel" />
            <asp:PostBackTrigger ControlID="btnExportToWord" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
