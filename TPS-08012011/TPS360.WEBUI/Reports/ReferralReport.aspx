﻿<%@ Page EnableEventValidation="false" Language="C#" MasterPageFile="~/Default.master"
    AutoEventWireup="true" CodeFile="ReferralReport.aspx.cs" Inherits="TPS360.Web.UI.Reports.ReferralReport"
    Title="Employee Referral Report" EnableViewStateMac="false" %>

<%@ Register Src="~/Controls/DateRangePicker.ascx" TagName="DateRangePicker" TagPrefix="ucl" %>
<%@ Register Src="~/Controls/PagerControl.ascx" TagName="Pager" TagPrefix="ucl" %>
<%@ Register Src="~/Controls/SmallPagerControl.ascx" TagName="SmallPager" TagPrefix="ucl" %>
<asp:Content ID="Content2" ContentPlaceHolderID="cphHomeMasterTitle" runat="Server">
    Employee Referral Report
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphHomeMaster" runat="Server">

    <script type="text/javascript" src="https://www.google.com/jsapi"></script>

    <link href="../assets/css/chosen.css" rel="Stylesheet" />

    <script>

var XmlHttp;
var controlid;
function GetReportData()
{
$('#visualization').html('<div style=\' backgound-color:red; margin-top:35px; height : 400px; width:100%; vertical-align:center; text-align:center;\'><img src=../Images/Ajax-loader.gif style=\' margin-top:180px; \'/></div>');
    var ddlRequisition=document .getElementById ('<%= ddlRequisition.ClientID %>');
    var ddlRefferedby=document .getElementById ('<%= ddlRefferedby.ClientID %>');
    var hdnStartDate=document .getElementById ('<%= hdnStartDate.ClientID %>');
    var hdnEndDate=document .getElementById ('<%= hdnEndDate.ClientID %>');
    var requestUrl =  "../AJAXServers/GraphicalReport.aspx?RType=ER&JID=" + ddlRequisition .options[ddlRequisition .selectedIndex].value  + "&RID=" + ddlRefferedby .options[ddlRefferedby .selectedIndex].value + "&SD=" + hdnStartDate .value + "&ED=" + hdnEndDate .value ;
    var modal= document.getElementById(controlid);
    CreateXmlHttp();
	if(XmlHttp)
	{
	try{
		XmlHttp.onreadystatechange = HandleResponseExpanded;
		XmlHttp.open("GET", requestUrl,  true);
		XmlHttp.send(null);		
		}
		catch(e)
		{
		    
		}
	}
}

function CreateXmlHttp()
{
	try
	{
		XmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
	}
	catch(e)
	{
		try
		{
			XmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
		} 
		catch(oc)
		{
			XmlHttp = null;
		}
	}
	if(!XmlHttp && typeof XMLHttpRequest != "undefined") 
	{
		XmlHttp = new XMLHttpRequest();
	}
}

function HandleResponseExpanded()
{
	if(XmlHttp.readyState == 4)
	{
	try{
		if(XmlHttp.status == 200)
		{			
			var t1 = XmlHttp.responseText;
			drawVisualization(t1);
		}
		else
		{
			alert("There was a problem retrieving data from the server." );
		}
		}
		catch(e)
		{
		    alert(e.Message);
		}
	}
}



google.load('visualization', '1', {packages: ['corechart']});
//google.setOnLoadCallback(GetReportData);

Sys.Application.add_load(function() { 

if($('#<%=hdnIsPostback.ClientID %>').val()=="0")
{
    $('#<%=hdnIsPostback.ClientID %>').val("1");
    __doPostBack('<%= lblReferredby.ClientID %>','');
 
 }
 $('#visualization').css({opacity: 0});
GetReportData ();
});
function drawVisualization(t1) {


try{
   xmlDoc = $.parseXML( t1 );
    var $xml = $( xmlDoc );
    var noofrows=0;    
    $xml.find( "Row" ).each(function (){noofrows =noofrows +1;});

    var data2 = new google.visualization.DataTable();
    
    data2.addColumn({id: 'date', label: 'Date', type: 'date'});
    data2.addColumn( {id: 'numReferrals', label: '# of Referrals', type: 'number'});
    data2 .addRows(noofrows);
    var rowno=0;
    $xml.find( "Row" ).each(function (){
        data2 .setCell(rowno ,0 ,new Date ($(this).find('Year').text(),$(this).find('Month').text()-1,$(this).find('Day').text()) );
        data2 .setCell(rowno ,1 , parseInt ($(this).find('Count').text()) );
        rowno =rowno +1;
        });
        }
        catch (e)
        {
            alert (e.message);
        }
  
var formatter = new google.visualization.NumberFormat({fractionDigits: 0});
formatter.format(data2, 1);   
  
  // Create and draw the visualization.
  var ac = new google.visualization.AreaChart(document.getElementById('visualization'));
  ac.draw(data2, {
    title : 'Employee Referrals by Day',
    isStacked: false,
    width: 800,
    height: 450,
    vAxis: {},
    hAxis: {
      format: 'MMM d'
    },
    backgroundColor: 'transparent',
    titleTextStyle: {
                color: '#111',
                fontSize: 16
            },
    colors: ['#06C'],
     areaOpacity: 0.1,
            lineWidth: 2,
            pointSize: 6,
            legend: {
                position: 'none'
            },
  });
  
  $('#visualization').animate({opacity: 1},500)
}

    </script>

    <link href="../Style/TabStyle.css" rel="Stylesheet" type="text/css" />
    <asp:HiddenField ID="hdnScrollPos" runat="server" />
    <asp:HiddenField ID="hdnIsPostback" runat="server" Value="0" />
    <asp:TextBox ID="SortColumn" runat="server" EnableViewState="true" Visible="false" />
    <asp:TextBox ID="SortOrder" runat="server" EnableViewState="true" Visible="false" />
    <div class="well" style="clear: both; padding-top: 0px;">
        <div class="TabPanelHeader">
            Filter Options</div>
        <div style="text-align: center ; vertical-align : middle ">
            <asp:UpdatePanel ID="upFilter" runat="server">
                <ContentTemplate>
                    <asp:HiddenField ID="hdnStartDate" runat="server" />
                    <asp:HiddenField ID="hdnEndDate" runat="server" />
                    <ucl:DateRangePicker ID="dtReferralDate" runat="server" />
                    <asp:Label ID="lblReferredby" runat="server" Text="Referred by:"></asp:Label>
                    <div style="text-align: left; display: inline; margin-top : 10px">
                        <asp:DropDownList ID="ddlRefferedby" runat="server" CssClass="chzn-select" Width="150px" style=" margin-top : 30px;"
						AutoPostBack="true" OnSelectedIndexChanged="ddlRefferedby_SelectedIndexChanged">
                        </asp:DropDownList>
                    </div>
                    <asp:Label ID="lblRequisition" runat="server" Text="Requisition:"></asp:Label>
                    <div style="text-align: left; display: inline">
                        <asp:DropDownList ID="ddlRequisition" runat="server" CssClass="chzn-select" Width="150px">
                        </asp:DropDownList>
                    </div>
                    <asp:Button ID="btnApply" runat="server" Text="Apply" CssClass="btn btn-primary"
                        OnClick="btnApply_Click" />
                    <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btn" OnClick="btnReset_Click" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <div class="tabbable">
        <!-- Only required for left/right tabs -->
        <ul class="nav nav-tabs">
            <li class="active"><a href="#tab1" data-toggle="tab">Summary</a></li>
            <li><a href="#tab2" data-toggle="tab">Details</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="tab1">
                <div id="visualization" style="margin: 0 auto; margin-top: -35px; margin-bottom: -35px;
                    min-height: 400px; margin-left: 100px;">
                </div>
                <div class="FormLeftColumn">
                    <div class="TabPanelHeader">
                        By Referrer</div>
                    <asp:UpdatePanel ID="upSummaryByReferrer" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtByRefSortColumn" runat="server" Visible="false"></asp:TextBox>
                            <asp:TextBox ID="txtByRefSortOrder" runat="server" Visible="false"></asp:TextBox>
                            <asp:TextBox ID="ByRefSortColumn" runat="server" Visible="false" Text="[ERR].[RefererFirstName]"></asp:TextBox>
                            <asp:TextBox ID="ByRefSortOrder" runat="server" Visible="false" Text="ASC"></asp:TextBox>
                            <asp:ObjectDataSource ID="odsByRef" runat="server" SelectMethod="GetPaged_ForSummary"
                                TypeName="TPS360.Web.UI.EmployeeReferralDataSource" SelectCountMethod="GetListCount_ForSummary"
                                EnablePaging="True" SortParameterName="sortExpression">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="dtReferralDate" Name="StartDate" PropertyName="StartDate"
                                        Type="DateTime" />
                                    <asp:ControlParameter ControlID="dtReferralDate" Name="EndDate" PropertyName="EndDate"
                                        Type="DateTime" />
                                    <asp:ControlParameter ControlID="ddlRequisition" Name="JobPostingId" PropertyName="SelectedValue"
                                        Type="String" />
                                    <asp:ControlParameter ControlID="ddlRefferedby" Name="ReferrerID" PropertyName="SelectedValue"
                                        Type="String" />
                                    <asp:Parameter Name="Type" DefaultValue="" />
                                    <asp:ControlParameter ControlID="ByRefSortOrder" Name="SortOrder" Type="String" />
                                </SelectParameters>
                            </asp:ObjectDataSource>
                            <asp:ListView ID="lsvByReferrer" runat="server" DataKeyNames="Id" DataSourceID="odsByRef"
                                OnItemDataBound="lsvByReferrer_ItemDataBound" OnItemCommand="lsvByReferrer_ItemCommand"
                                OnPreRender="lsvByReferrer_PreRender">
                                <LayoutTemplate>
                                    <table id="tlbTemplate" runat="server" class="Grid" cellspacing="0" border="0">
                                        <tr>
                                            <th>
                                                <asp:LinkButton runat="server" ID="btnName" CommandName="Sort" CommandArgument="[ERR].[RefererFirstName]"
                                                    Text="Name" ToolTip="Sort By Name" />
                                            </th>
                                            <th style="width: 100px !important">
                                                <asp:LinkButton runat="server" ID="btnReferralsCount" CommandName="Sort" CommandArgument="[ER].[Count]"
                                                    Text="# of Referrals" ToolTip="Sort By # of Referrals" />
                                            </th>
                                        </tr>
                                        <tr id="itemPlaceholder" runat="server">
                                        </tr>
                                        <tr class="Pager">
                                            <td id="tdpager" runat="server" colspan="2">
                                                <ucl:SmallPager ID="pagerControl" runat="server" EnableViewState="true" />
                                            </td>
                                        </tr>
                                    </table>
                                </LayoutTemplate>
                                <EmptyDataTemplate>
                                    <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                                        <tr>
                                            <td>
                                                No data was returned.
                                            </td>
                                        </tr>
                                    </table>
                                </EmptyDataTemplate>
                                <ItemTemplate>
                                    <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                                        <td>
                                            <asp:Label ID="lblName" runat="server"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label runat="server" ID="lblcount" />
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:ListView>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="FormRightColumn">
                    <div class="TabPanelHeader">
                        By Requisition</div>
                    <asp:UpdatePanel ID="upByReq" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtByReqSortColumn" runat="server" Visible="false"></asp:TextBox>
                            <asp:TextBox ID="txtByReqSortOrder" runat="server" Visible="false"></asp:TextBox>
                            <asp:TextBox ID="ByReqSortColumn" runat="server" Visible="false" Text="[J].[JobTitle]"></asp:TextBox>
                            <asp:TextBox ID="ByReqSortOrder" runat="server" Visible="false" Text="ASC"></asp:TextBox>
                            <asp:ObjectDataSource ID="odsByReq" runat="server" SelectMethod="GetPaged_ForSummary"
                                TypeName="TPS360.Web.UI.EmployeeReferralDataSource" SelectCountMethod="GetListCount_ForSummary"
                                EnablePaging="True" SortParameterName="sortExpression">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="dtReferralDate" Name="StartDate" PropertyName="StartDate"
                                        Type="DateTime" />
                                    <asp:ControlParameter ControlID="dtReferralDate" Name="EndDate" PropertyName="EndDate"
                                        Type="DateTime" />
                                    <asp:ControlParameter ControlID="ddlRequisition" Name="JobPostingId" PropertyName="SelectedValue"
                                        Type="String" />
                                    <asp:ControlParameter ControlID="ddlRefferedby" Name="ReferrerID" PropertyName="SelectedValue"
                                        Type="String" />
                                    <asp:Parameter Name="Type" DefaultValue="ByRequisition" />
                                    <asp:ControlParameter ControlID="ByReqSortOrder" Name="SortOrder" Type="String" />
                                </SelectParameters>
                            </asp:ObjectDataSource>
                            <asp:ListView ID="lsvByRequisition" runat="server" DataKeyNames="Id" DataSourceID="odsByReq"
                                OnItemDataBound="lsvByRequisition_ItemDataBound" OnItemCommand="lsvByRequisition_ItemCommand"
                                OnPreRender="lsvByRequisition_PreRender">
                                <LayoutTemplate>
                                    <table id="tlbTemplate" runat="server" class="Grid" cellspacing="0" border="0">
                                        <tr>
                                            <th>
                                                <asp:LinkButton runat="server" ID="btnName" CommandName="Sort" CommandArgument="[J].[JobTitle]"
                                                    Text="Job Title" ToolTip="Sort By Job Title" />
                                            </th>
                                            <th style="width: 100px !important">
                                                <asp:LinkButton runat="server" ID="btnReferralsCount" CommandName="Sort" CommandArgument="[ER].[Count]"
                                                    Text="# of Referrals" ToolTip="Sort By # of Referrals" />
                                            </th>
                                        </tr>
                                        <tr id="itemPlaceholder" runat="server">
                                        </tr>
                                        <tr class="Pager">
                                            <td id="tdpager" runat="server" colspan="2">
                                                <ucl:SmallPager ID="pagerControl" runat="server" EnableViewState="true" />
                                            </td>
                                        </tr>
                                    </table>
                                </LayoutTemplate>
                                <EmptyDataTemplate>
                                    <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                                        <tr>
                                            <td>
                                                No data was returned.
                                            </td>
                                        </tr>
                                    </table>
                                </EmptyDataTemplate>
                                <ItemTemplate>
                                    <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                                        <td>
                                            <asp:HyperLink ID="lnkJobTitle" runat="server"></asp:HyperLink>
                                        </td>
                                        <td>
                                            <asp:Label runat="server" ID="lblcount" />
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:ListView>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
            <div class="tab-pane" id="tab2" style="width: 100%; overflow: auto;">
                <asp:UpdatePanel ID="upDetail" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtSortColumn" runat="server" Visible="false"></asp:TextBox>
                        <asp:TextBox ID="txtSortOrder" runat="server" Visible="false"></asp:TextBox>
                        <asp:ObjectDataSource ID="odsReferralDetial" runat="server" SelectMethod="GetPaged_ForDetail"
                            TypeName="TPS360.Web.UI.EmployeeReferralDataSource" SelectCountMethod="GetListCount_ForDetail"
                            EnablePaging="True" SortParameterName="sortExpression">
                            <SelectParameters>
                                <asp:ControlParameter ControlID="dtReferralDate" Name="StartDate" PropertyName="StartDate"
                                    Type="DateTime" />
                                <asp:ControlParameter ControlID="dtReferralDate" Name="EndDate" PropertyName="EndDate"
                                    Type="DateTime" />
                                <asp:ControlParameter ControlID="ddlRequisition" Name="JobPostingId" PropertyName="SelectedValue"
                                    Type="String" />
                                <asp:ControlParameter ControlID="ddlRefferedby" Name="ReferrerID" PropertyName="SelectedValue"
                                    Type="String" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                        <div class="TableRow" style="padding-bottom: 5px" id="divExportButtons" runat="server"
                            visible="true">
                            <div class="TabPanelHeader nomargin" style="margin-bottom: 5px; border-style: none none none;">
                                Report Results</div>
                            <asp:ImageButton ID="btnExportToExcel" CssClass="btn" SkinID="sknExportToExcel" runat="server"
                                ToolTip="Export To Excel" OnClick="btnExportToExcel_Click" />
                            <asp:ImageButton ID="btnExportToPDF" CssClass="btn" SkinID="sknExportToPDF" runat="server"
                                ToolTip="Export To PDF" OnClick="btnExportToPDF_Click" />
                            <asp:ImageButton ID="btnExportToWord" CssClass="btn" SkinID="sknExportToWord" runat="server"
                                ToolTip="Export To Word" OnClick="btnExportToWord_Click" />
                        </div>
                        <asp:ListView ID="lsvReferralDetail" runat="server" DataKeyNames="Id" DataSourceID="odsReferralDetial"
                            OnItemDataBound="lsvReferralDetail_ItemDataBound" OnItemCommand="lsvReferralDetail_ItemCommand"
                            OnPreRender="lsvReferralDetail_PreRender">
                            <LayoutTemplate>
                                <table id="tlbTemplate" runat="server" class="Grid" cellspacing="0" border="0">
                                    <tr>
                                        <th>
                                            <asp:LinkButton runat="server" ID="btnDate" CommandName="Sort" CommandArgument="[ER].[CreateDate]"
                                                Text="Date" ToolTip="Date" />
                                        </th>
                                        <th>
                                            <asp:LinkButton runat="server" ID="btnReferrer" CommandName="Sort" CommandArgument="[ERR].[RefererFirstName]"
                                                Text="Referrer" ToolTip="Referrer" />
                                        </th>
                                        <th>
                                            <asp:LinkButton runat="server" ID="btnReferrerEmail" CommandName="Sort" CommandArgument="[ERR].[RefererEmail]"
                                                Text="Referrer Email" ToolTip="Referrer Email" />
                                        </th>
                                        <th>
                                            <asp:LinkButton runat="server" ID="btnReferrerPS" CommandName="Sort" CommandArgument="[ERR].[EmployeeId]"
                                                Text="Referrer PS#" ToolTip="Referrer PS #" />
                                        </th>
                                        <th>
                                            <asp:LinkButton runat="server" ID="btnRequisition" CommandName="Sort" CommandArgument="[J].[JobTitle]"
                                                Text="Requisition" ToolTip="Requisition" />
                                        </th>
                                         <th>
                                            <asp:LinkButton runat="server" ID="lnkId" CommandName="Sort" CommandArgument="[C].[Id]"
                                                Text="Candidate ID #" ToolTip="Candidate ID #" />
                                                
                                        </th>
                                        <th>
                                            <asp:LinkButton runat="server" ID="btnCandidate" CommandName="Sort" CommandArgument="[C].[CandidateName]"
                                                Text="Candidate" ToolTip="Candidate" />
                                        </th>
                                    </tr>
                                    <tr id="itemPlaceholder" runat="server">
                                    </tr>
                                    <tr class="Pager">
                                        <td id="tdpager" runat="server" colspan="7">
                                            <ucl:Pager ID="pagerControl" runat="server" EnableViewState="true" />
                                        </td>
                                    </tr>
                                </table>
                            </LayoutTemplate>
                            <EmptyDataTemplate>
                                <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                                    <tr>
                                        <td>
                                            No data was returned.
                                        </td>
                                    </tr>
                                </table>
                            </EmptyDataTemplate>
                            <ItemTemplate>
                                <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                                    <td>
                                        <asp:Label ID="lblDate" runat="server"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label runat="server" ID="lblReferrer" />
                                    </td>
                                    <td>
                                        <asp:Label runat="server" ID="lblReferrerEmail" />
                                    </td>
                                    <td>
                                        <asp:Label runat="server" ID="lblReferrerPS" />
                                    </td>
                                    <td>
                                        <asp:HyperLink ID="hlkJobTitle" runat="server"></asp:HyperLink>
                                    </td>
                                    <td>
                                        <asp:Label runat="server" ID="lblCandidateID" />
                                    </td>
                                    <td>
                                        <asp:HyperLink ID="hlnkCandidate" runat="server" ></asp:HyperLink>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:ListView>
                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnExportToPDF" />
                        <asp:PostBackTrigger ControlID="btnExportToExcel" />
                        <asp:PostBackTrigger ControlID="btnExportToWord" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>

    <script src="../assets/js/chosen.jquery.js" type="text/javascript"></script>

    <script type="text/javascript"> 
   Sys.Application.add_load(function() { 
   $(".chzn-select").chosen();
  $(".chzn-select-deselect").chosen({allow_single_deselect:true});
  });
    </script>

</asp:Content>
