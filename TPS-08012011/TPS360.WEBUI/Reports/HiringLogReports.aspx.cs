﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using TPS360.Common.Helper;
using System.Text;
using TPS360.Web.UI.Helper;
using TPS360.Common.BusinessEntities;
using ExpertPdf.HtmlToPdf;
using System.Collections.Generic;
using System.Drawing;
using TPS360.Common.Shared;

namespace TPS360.Web.UI.Reports
{
    public partial class HiringLogReports : BasePage
    {
        #region Variables
        private bool isAccess = false;
        private static string UrlForCandidate = string.Empty;
        private static int SitemapIdForCandidate = 0;
        bool _IsAccessToCandidate = true;
        #endregion

        #region Properties
        int CandidateId
        {
            get
            {
                if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]))
                {
                    return Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]);

                }
                else
                {
                    return 0;
                }

            }
        }
       
        #endregion

        # region Page Event
        protected void Page_Load(object sender, EventArgs e)
        {
           
            //try
            //{
            //    if (Facade.IsAvailableSessionId(Session.SessionID) != 1)
            //    {
            //        FormsAuthentication.SignOut();
            //        FormsAuthentication.RedirectToLoginPage();
            //        return;
            //    }
            //}
            //catch { }
            //uclConfirm.MsgBoxAnswered += MessageAnswered;
            if (!IsPostBack)
            {
                PrepareView(); btnSearch_Click(sender, e);
            }
            
        }
        #endregion

        #region Methods
        private void PrepareView()
        {
            MiscUtil.PopulateMemberListByRole(ddlEmployee , ContextConstants.ROLE_EMPLOYEE, Facade);
            LoadJobPosting();
            PopulateClientDropdowns();
            LoadActionType();

            if (SiteSetting[DefaultSiteSetting.ApplicationEdition.ToString()].ToString() == UIConstants.INTERNAL_HIRING)
            {
                lblClient.Text = "BU";
            }
        }
        private void LoadActionType()
        {
            DropDownList list = new DropDownList();
            foreach (EventLogForRequisition r in Enum.GetValues(typeof(EventLogForRequisition)))
            {
                ListItem item = new ListItem(Enum.GetName(typeof(EventLogForRequisition), r), r.ToString());
                item.Text = TPS360.Common.EnumHelper.GetDescription(r).TrimStart();
                item.Value = item.Text;
                list.Items.Add(item);
            }
            foreach (EventLogForCandidate r in Enum.GetValues(typeof(EventLogForCandidate)))
            {
                ListItem item = new ListItem(Enum.GetName(typeof(EventLogForCandidate), r), r.ToString());
                item.Text = TPS360.Common.EnumHelper.GetDescription(r).Replace("for <candidate name>", "").Replace("to <candidate name>", "").Replace("<candidate name>", "").Replace("to <new status level>", "").TrimStart();
                item.Value = TPS360.Common.EnumHelper.GetDescription(r).Replace("to <new status level>", "");
                list.Items.Add(item);
            }
            var sort = list.Items.Cast<ListItem>().OrderBy(o => o.Text);
           
           foreach (ListItem item in sort)
           {
               ddlActionType.Items.Add(item);
           }
            ddlActionType.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));  
        }
        private void PopulateClientDropdowns()
        {
            ddlClient.Items.Clear();
            int companyStatus = (int)MiscUtil.GetCompanyStatusFromDefaultSiteSetting(SiteSetting);
            ddlClient.DataSource = Facade.GetAllClientsByStatus(companyStatus);
            ddlClient.DataTextField = "CompanyName";
            ddlClient.DataValueField = "Id";
            ddlClient.DataBind();
            if (SiteSetting[DefaultSiteSetting.AssociateRequisitionsWithEntitiesFromModule.ToString()].ToString() == UIConstants.AccountType_Department)
            {
                ddlClient = (DropDownList)MiscUtil.RemoveScriptForDropDown(ddlClient);
                ddlClient.Items.Insert(0, new ListItem("Select BU", "0"));
                lblClient.Text = "BU";
            }
            else if (SiteSetting[DefaultSiteSetting.AssociateRequisitionsWithEntitiesFromModule.ToString()].ToString() == UIConstants.AccountType_Client)
            {
                ddlClient = (DropDownList)MiscUtil.RemoveScriptForDropDown(ddlClient);
                ddlClient.Items.Insert(0, new ListItem("Select Account", "0"));
                lblClient.Text = "Account";
            }
            else if (SiteSetting[DefaultSiteSetting.AssociateRequisitionsWithEntitiesFromModule.ToString()].ToString() == UIConstants.AccountType_Vendor)
            {
                ddlClient = (DropDownList)MiscUtil.RemoveScriptForDropDown(ddlClient);
                ddlClient.Items.Insert(0, new ListItem("Select Vendor", "0"));
                lblClient.Text = "Vendor";
            }
            
        }
        private void LoadJobPosting()
        {
            ddlRequisition.DataSource = Facade.GetAllJobPostingByStatus (0);//.GetAllJobPostingByStatusAndManagerId(0,0);
            ddlRequisition.DataTextField = "JobTitle";
            ddlRequisition.DataValueField = "Id";
            ddlRequisition.DataBind();
            ddlRequisition = (DropDownList)MiscUtil.RemoveScriptForDropDown(ddlRequisition);
            ddlRequisition.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));
        }
    
        public void MessageAnswered(object sender, ConfirmationWindow.MsgBoxEventArgs e)
        {

            if (e.Answer == ConfirmationWindow.enmAnswer.OK)
            {
               // repor("pdf");
            }

        }

        private void BindList()
        {
            this.lsvHiringLog.DataSourceID = "odsHiringLog";
            this.divlsvHiringLog.Visible = true;
            this.lsvHiringLog.DataBind();
        }

        private void PlaceUpDownArrow()
        {
            try
            {
                LinkButton lnk = (LinkButton)lsvHiringLog.FindControl(hdnSortColumn.Text);
                HtmlTableCell im = (HtmlTableCell)lnk.Parent;
                im.Attributes.Add("class", (hdnSortOrder.Text  == "ASC" ? "Ascending" : "Descending"));

            }
            catch
            {
            }

        }

        private void ClearControls()
        {
            ddlRequisition.SelectedIndex = 0;
            ddlEmployee.SelectedIndex = 0;
            ddlClient.SelectedIndex = 0;
            ddlActionType.SelectedIndex = 0;
            //wcdStartDate.Value = wcdEndDate.Value = null;
            lsvHiringLog.Visible = false;
            dtPicker.ClearRange();
        }

        private void GenerateHiringLogReport(string format)
        {
            string exportFileName = "HiringLogReport-" + DateTime.Now.ToString("yyyy-MM-dd");
            if (string.Equals(format, "word"))
            {
                Response.AddHeader("content-disposition", "attachment;filename="+exportFileName +".doc");
                Response.ContentEncoding = System.Text.Encoding.ASCII;
                Response.ContentType = "application/msword";
                Response.Output.Write(GetHiringLogReportTable());
                Response.Flush();
                Response.End();
            }
            else if (string.Equals(format, "excel"))
            {
                Response.AddHeader("content-disposition", "attachment;filename="+exportFileName +".xls");
                Response.ContentEncoding = System.Text.Encoding.ASCII;
                Response.ContentType = "application/msexcel";
                Response.Output.Write(GetHiringLogReportTable());
                Response.Flush();
                Response.End();
            }
            else if (string.Equals(format, "pdf"))
            {
                PdfConverter pdfConverter = new PdfConverter();
                UnicodeEncoding unicodeEncoding = new UnicodeEncoding();
                pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
                pdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Normal;
                pdfConverter.PdfDocumentOptions.ShowHeader = false;
                pdfConverter.PdfDocumentOptions.ShowFooter = false;
                pdfConverter.PdfDocumentOptions.LeftMargin = 15;
                pdfConverter.PdfDocumentOptions.RightMargin = 5;
                pdfConverter.PdfDocumentOptions.TopMargin = 15;
                pdfConverter.PdfDocumentOptions.BottomMargin = 5;
                pdfConverter.PdfDocumentOptions.GenerateSelectablePdf = true;
                pdfConverter.PdfDocumentOptions.ShowHeader = false;
                pdfConverter.PdfHeaderOptions.DrawHeaderLine = false;
                pdfConverter.PdfFooterOptions.FooterTextColor = Color.Black;
                pdfConverter.PdfFooterOptions.DrawFooterLine = true;
                pdfConverter.PdfFooterOptions.PageNumberText = "Page";
                pdfConverter.PdfFooterOptions.ShowPageNumber = true;

                pdfConverter.LicenseKey = ContextConstants.EPHTMLTOPDFKEY;
                byte[] downloadBytes = pdfConverter.GetPdfBytesFromHtmlString(GetHiringLogReportTable());

                System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
                response.Clear();
                response.AddHeader("Content-Type", "binary/octet-stream");
                response.AddHeader("Content-Disposition", "attachment; filename="+exportFileName +".pdf; size=" + downloadBytes.Length.ToString());
                response.Flush();
                response.BinaryWrite(downloadBytes);
                response.Flush();
                response.End();
            }
        }

        private string GetHiringLogReportTable()
        {
            StringBuilder reqReport = new StringBuilder();
            EventLogDataSource LogDataSource = new EventLogDataSource();
            IList<EventLogForRequisitionAndCandidate> LogList = LogDataSource.GetPaged((ddlRequisition.SelectedIndex > 0 ? Convert.ToInt32(ddlRequisition.SelectedValue) : 0), (ddlClient.SelectedIndex > 0 ? Convert.ToInt32(ddlClient.SelectedValue) : 0), (ddlEmployee.SelectedIndex > 0 ? Convert.ToInt32(ddlEmployee.SelectedValue) : 0), dtPicker.StartDate.ToString(), dtPicker.EndDate.ToString(), (ddlActionType.SelectedIndex > 0 ? ddlActionType.SelectedValue : "0"), null, -1, -1);

            if (LogList != null)
            {
                string WebRoot = ConfigurationManager.AppSettings["WebRoot"].ToString();
                string LogoPath;
                if (string.IsNullOrEmpty(HttpRuntime.AppDomainAppVirtualPath))
                {
                    LogoPath = WebRoot;
                }
                else
                {
                    LogoPath = WebRoot + HttpRuntime.AppDomainAppVirtualPath;
                }

                reqReport.Append("<table style='font-family : calibriz;width:100%; border-collapse:collapse; border-spacing: 1px;' align='left' border = '1' bordercolor='#000000'  cellspacing='0' cellpadding='0'>");

                reqReport.Append("    <tr>");
                reqReport.Append("        <td align=left  width='300' height='75'>&nbsp;<img src='" + UrlConstants.ApplicationBaseUrl + "Images/logo-left-75px.png' style= style='height:56px;width:56px'/></td>");
                reqReport.Append("    </tr>");
                reqReport.Append(" <tr>");
                reqReport.Append("     <th>Date & Time</th>");
                reqReport.Append("     <th>User</th>");
                reqReport.Append("     <th>Requisition</th>");
                reqReport.Append("     <th>Action </th>");
                reqReport.Append(" </tr>");

                foreach (EventLogForRequisitionAndCandidate log in LogList)
                {
                    if (log != null)
                    {
                        reqReport.Append(" <tr>");
                        reqReport.Append("     <td>" + log .ActionDate .ToString () + "&nbsp;</td>");
                        reqReport.Append("     <td>" + log .UserName  + "&nbsp;</td>");
                        reqReport.Append("     <td>" + log .JobTitle  + "&nbsp;</td>");
                        reqReport.Append("     <td>" + log .ActionType  + "&nbsp;</td>");
                        reqReport.Append(" </tr>");
                    }
                }

                reqReport.Append("    <tr>");
                reqReport.Append("        <td align=left  width='300' height='75'><img src='" + UrlConstants.ApplicationBaseUrl + "Images/logo-left-75px.png' style= style='height:56px;width:56px;'/></td>");
                reqReport.Append("    </tr>");
              
                reqReport.Append(" </table>");
            }
            return reqReport.ToString();
        }
        #endregion
        #region ButtonEvents
        protected void btnClear_Click(object sender, EventArgs e)
        {
            ClearControls();
            divExportButtons.Visible = false;
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
           
            lsvHiringLog.Visible = true;
            hdnScrollPos.Value = "0";
            lsvHiringLog.Items.Clear();
            ASP.controls_pagercontrol_ascx PagerControls = (ASP.controls_pagercontrol_ascx)this.lsvHiringLog.FindControl("pagerControl");
            if (PagerControls != null)
            {
                DataPager pager = (DataPager)PagerControls.FindControl("pager");
                if (pager != null)
                {
                    if (pager.Page.IsPostBack)
                        pager.SetPageProperties(0, pager.MaximumRows, true);
                }
            }
            BindList();
            if (lsvHiringLog != null && lsvHiringLog.Items.Count > 0)
            {
                divExportButtons.Visible = true;
                lsvHiringLog.Visible = true;

                if (hdnSortColumn.Text == "") hdnSortColumn.Text = "btnDate";
                if (hdnSortOrder.Text == "") hdnSortOrder.Text = "DESC";
                PlaceUpDownArrow();
            }
            else divExportButtons.Visible = false;
            string pagesize = "";
            pagesize = (Request.Cookies["HiringLogReportRowPerPage"] == null ? "" : Request.Cookies["HiringLogReportRowPerPage"].Value); ;
            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvHiringLog.FindControl("pagerControl");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)PagerControl.FindControl("pager");
                if (pager != null)
                {
                    pager.PageSize = Convert.ToInt32(pagesize == "" ? "20" : pagesize);
                    if (pager.Page.IsPostBack)
                        pager.SetPageProperties(0, pager.MaximumRows, true);
                }
            }
        }

        protected void btnExportToPDF_Click(object sender, EventArgs e)
        {
            GenerateHiringLogReport("pdf");
        }
        protected void btnExportToExcel_Click(object sender, EventArgs e)
        {
            GenerateHiringLogReport("excel");
        }
        protected void btnExportToWord_Click(object sender, EventArgs e)
        {
            GenerateHiringLogReport("word");
        }
        #endregion

        #region Listview Events
        protected void lsvHiringLog_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            StringBuilder ColumnOption = new StringBuilder();
            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                EventLogForRequisitionAndCandidate  Log = ((ListViewDataItem)e.Item).DataItem as EventLogForRequisitionAndCandidate  ;

                if (Log != null)
                {
                    Label lblDate = (Label)e.Item.FindControl("lblDate");
                    Label lblUser = (Label)e.Item.FindControl("lblUser");
                    Label lblJobtitle = (Label)e.Item.FindControl("lblJobtitle");
                    Label lblActions = (Label)e.Item.FindControl("lblActions");
                    lblDate.Text = Log.ActionDate.ToString();
                    lblUser.Text = Log.UserName;
                    lblJobtitle.Text = Log.JobTitle;
                    lblActions.Text = Log.ActionType;
                }
            }
        }

        protected void lsvHiringLog_PreRender(object sender, EventArgs e)
        {
            divExportButtons.Visible = lsvHiringLog.Items.Count > 0;
            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvHiringLog.FindControl("pagerControl");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)PagerControl.FindControl("pager");
                if (pager != null)
                {
                    DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                    if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
                }
                HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
                if (hdnRowPerPageName != null) hdnRowPerPageName.Value = "ProductivityReportRowPerPage";
            }
            PlaceUpDownArrow();
            
            if (IsPostBack)
            {
                if (PagerControl == null)
                {
                    lsvHiringLog.Items.Clear();
                    lsvHiringLog.DataSource = null;
                    lsvHiringLog.DataBind();
                    divExportButtons.Visible = lsvHiringLog.Items.Count > 0;
                }
            }
        }

        protected void lsvHiringLog_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Sort")
                {
                    LinkButton lnkbutton = (LinkButton)e.CommandSource;
                    if (hdnSortColumn.Text == lnkbutton.ID)
                    {
                        if (hdnSortOrder.Text == "ASC") hdnSortOrder.Text = "DESC";
                        else hdnSortOrder.Text = "ASC";
                    }
                    else
                    {
                        hdnSortColumn.Text = lnkbutton.ID;
                        hdnSortOrder.Text = "ASC";
                    }
                }
            }
            catch
            { }
        }
         #endregion
    }
}
