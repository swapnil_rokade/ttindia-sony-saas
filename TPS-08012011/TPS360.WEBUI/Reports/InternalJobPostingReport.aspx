﻿<%@ Page EnableEventValidation="false" Language="C#" MasterPageFile="~/Default.master"
    AutoEventWireup="true" CodeFile="InternalJobPostingReport.aspx.cs" Inherits="TPS360.Web.UI.Reports.InternalJobPostingReport"
    Title="Internal Job Posting Report" EnableViewStateMac="false" %>


<%@ Register Src="~/Controls/InternalJobPostingReport.ascx" TagName="InternalReferralReport" TagPrefix="ucl" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphHomeMasterTitle" runat="Server">
    Internal Job Posting Report
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphHomeMaster" runat="Server">
<ucl:InternalReferralReport ID="ucnReferralReport" runat="server" IsTeamReport="false" IsMyReferralReport="false" />
    
</asp:Content>


