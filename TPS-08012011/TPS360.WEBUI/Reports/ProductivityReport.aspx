﻿<%@ Page EnableEventValidation="false" Language="C#" MasterPageFile="~/Default.master"
    AutoEventWireup="true" CodeFile="ProductivityReport.aspx.cs" Inherits="TPS360.Web.UI.Reports.ProductivityReport"
    MaintainScrollPositionOnPostback="true" Title="Productivity Report" EnableViewStateMac="false" %>

<%@ Register Src="~/Controls/CheckedDropDownList.ascx" TagName="CheckedDropDownList"
    TagPrefix="ucl" %>
<%@ Register Src="~/Controls/DateRangePicker.ascx" TagName="DateRangePicker" TagPrefix="ucl" %>
<%@ Register Src="~/Controls/PagerControl.ascx" TagName="Pager" TagPrefix="ucl" %>
<asp:Content ID="Content2" ContentPlaceHolderID="cphHomeMasterTitle" runat="Server">
    Productivity Report
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphHomeMaster" runat="Server">

    <script type="text/javascript" src="https://www.google.com/jsapi"></script>

    <script src="../js/EmployeeTeam.js" type="text/javascript"></script>

    <script>

var XmlHttp;
var controlid;
function GetReportData()
{

$('#visualization').html('<div style=\' backgound-color:red; margin-top:35px; height : 400px; width:100%; vertical-align:center; text-align:center;\'><img src=../Images/Ajax-loader.gif style=\' margin-top:180px; \'/></div>');
    var ddlUser=document .getElementById ('<%= hdnUserSelectedValue.ClientID %>');
     var ddlTeam=document .getElementById ('<%= ddlTeam.ClientID %>');
    var hdnStartDate=document .getElementById ('<%= hdnStartDate.ClientID %>');
    var hdnEndDate=document .getElementById ('<%= hdnEndDate.ClientID %>');
    var requestUrl =  "../AJAXServers/GraphicalReport.aspx?RType=PRByE&TID=" + (ddlTeam ==null ?"0" : ddlTeam.options[ddlTeam.selectedIndex].value)  + "&UID=" +  ddlUser.value + "&SD=" + hdnStartDate.value + "&ED=" + hdnEndDate.value ;
    var modal= document.getElementById(controlid);
    CreateXmlHttp();
	if(XmlHttp)
	{
	try{
		XmlHttp.onreadystatechange = HandleResponseExpanded;
		XmlHttp.open("GET", requestUrl,  true);
		XmlHttp.send(null);		
		}
		catch(e)
		{
		    
		}
	}
	
}

function CreateXmlHttp()
{
	try
	{
		XmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
	}
	catch(e)
	{
		try
		{
			XmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
		} 
		catch(oc)
		{
			XmlHttp = null;
		}
	}
	if(!XmlHttp && typeof XMLHttpRequest != "undefined") 
	{
		XmlHttp = new XMLHttpRequest();
	}
}

function HandleResponseExpanded()
{
	if(XmlHttp.readyState == 4)
	{
	try{
		if(XmlHttp.status == 200)
		{			
			var t1 = XmlHttp.responseText;
			drawVisualization(t1);
			drawUserProductivityByDay(t1);
		}
		else
		{
			alert("There was a problem retrieving data from the server." );
		}
		}
		catch(e)
		{
		    alert(e.Message);
		}
	}
}



google.load('visualization', '1', {packages: ['corechart']});
//google.setOnLoadCallback(GetReportData);

Sys.Application.add_load(function() { 
 $('#visualization').css({opacity: 0});
GetReportData ();
});
function drawVisualization(t1) {


try{
   xmlDoc = $.parseXML( t1 );
    var $xml = $( xmlDoc );
    var noofrows=0;    
    $xml.find( "Row" ).each(function (){noofrows =noofrows +1;});

    var data2 = new google.visualization.DataTable();
    
    data2.addColumn({id: 'user', label: 'User', type: 'string'});
    data2.addColumn( {id: 'numNewCandidates', label: '# of New Candidates', type: 'number'});
    data2.addColumn( {id: 'numRequisitionsPublished', label: '# of Requisitions', type: 'number'});
    data2.addColumn( {id: 'numCandidatesSourced', label: '# of Candidates Sourced', type: 'number'});
    data2.addColumn( {id: 'numCandidateSubmissions', label: '# of Candidate Submissions', type: 'number'});
    data2.addColumn( {id: 'numInterviews', label: '# of Interviews', type: 'number'});
    data2.addColumn( {id: 'numCandidateOffers', label: '# of Candidate Offers', type: 'number'});
    data2.addColumn( {id: 'numCandidateOffersRejected ', label: '# of Candidate Offers Decline ', type: 'number'});
    data2.addColumn( {id: 'numCandidatesJoined', label: '# of Candidates Joined', type: 'number'});
    if($('#<%=hdnAppType.ClientID %>').val()=='landt')data2.addColumn( {id: 'numPendingJoiners', label: '# of Pending Joiners', type: 'number'});
    
    data2 .addRows(noofrows);
    var rowno=0;
    $xml.find( "Row" ).each(function (){
        data2 .setCell(rowno ,0 ,$(this).find('User').text()) ;
        data2 .setCell(rowno ,1 , parseInt ($(this).find('NewCan').text()) );
        data2 .setCell(rowno ,2 , parseInt ($(this).find('ReqPub').text()) );
        data2 .setCell(rowno ,3 , parseInt ($(this).find('CanSor').text()) );
        data2 .setCell(rowno ,4 , parseInt ($(this).find('CanSub').text()) );
        data2 .setCell(rowno ,5 , parseInt ($(this).find('Inter').text()) );
        data2 .setCell(rowno ,6 , parseInt ($(this).find('Offers').text()) );
        data2 .setCell(rowno ,7 , parseInt ($(this).find('Rejected').text()) );
        data2 .setCell(rowno ,8 , parseInt ($(this).find('Joined').text()) );
  if($('#<%=hdnAppType.ClientID %>').val()=='landt')      data2 .setCell(rowno ,9 , parseInt ($(this).find('Pending').text()) );
        rowno =rowno +1;
        });
        }
        catch (e)
        {
            alert (e.message);
        }
  
var formatter = new google.visualization.NumberFormat({fractionDigits: 0});
formatter.format(data2, 1);   
  
  // Create and draw the visualization.
 var ac = new google.visualization.BarChart(document.getElementById('visualization'));
  ac.draw(data2, {
    title : 'User Productivity Summary',
    width: 800,
     
    height: data2.getNumberOfRows() >= 5 ? data2.getNumberOfRows() * 150 : 450,
    backgroundColor: 'transparent',
    titleTextStyle: {
                color: '#111',
                fontSize: 16
            },
  
     areaOpacity: 0.1,
            lineWidth: 2,
            pointSize: 6,
            legend: {
                position: 'none'
            },
      legend: {position: 'right', textStyle: {color: 'black', fontSize: 12}}
  });
  
  $('#visualization').animate({opacity: 1},500)
}


///////////////////

function drawUserProductivityByDay(t1) {


try{
   xmlDoc = $.parseXML( t1 );
    var $xml = $( xmlDoc );
    var noofrows=0;    
    $xml.find( "ByDate" ).each(function (){noofrows =noofrows +1;});

    var data2 = new google.visualization.DataTable();
    
    data2.addColumn({id: 'date', label: 'Date', type: 'date'});
    data2.addColumn( {id: 'numNewCandidates', label: '# of New Candidates', type: 'number'});
    data2.addColumn( {id: 'numRequisitionsPublished', label: '# of Requisitions', type: 'number'});
    data2.addColumn( {id: 'numCandidatesSourced', label: '# of Candidates Sourced', type: 'number'});
    data2.addColumn( {id: 'numCandidateSubmissions', label: '# of Candidate Submissions', type: 'number'});
    data2.addColumn( {id: 'numInterviews', label: '# of Interviews', type: 'number'});
    data2.addColumn( {id: 'numCandidateOffers', label: '# of Candidate Offers', type: 'number'});
    data2.addColumn( {id: 'numCandidateOffersRejected ', label: '# of Candidate Offers Decline', type: 'number'});
    data2.addColumn( {id: 'numCandidatesJoined', label: '# of Candidates Joined', type: 'number'});
    if($('#<%=hdnAppType.ClientID %>').val()=='landt')data2.addColumn( {id: 'numPendingJoiners', label: '# of Pending Joiners', type: 'number'});
    data2 .addRows(noofrows);
    var rowno=0;
    $xml.find( "ByDate" ).each(function (){
        data2 .setCell(rowno ,0 ,new Date ($(this).find('Year').text(),$(this).find('Month').text()-1,$(this).find('Day').text()) );
        data2 .setCell(rowno ,1 , parseInt ($(this).find('NewCan').text()) );
        data2 .setCell(rowno ,2 , parseInt ($(this).find('ReqPub').text()) );
        data2 .setCell(rowno ,3 , parseInt ($(this).find('CanSor').text()) );
        data2 .setCell(rowno ,4 , parseInt ($(this).find('CanSub').text()) );
        data2 .setCell(rowno ,5 , parseInt ($(this).find('Inter').text()) );
        data2 .setCell(rowno ,6 , parseInt ($(this).find('Offers').text()) );
        data2 .setCell(rowno ,7 , parseInt ($(this).find('Rejected').text()) );
        data2 .setCell(rowno ,8 , parseInt ($(this).find('Joined').text()) );
  if($('#<%=hdnAppType.ClientID %>').val()=='landt') data2 .setCell(rowno ,9 , parseInt ($(this).find('Pending').text()) );
        rowno =rowno +1;
        });
        }
        catch (e)
        {
            alert (e.message);
        }
  
var formatter = new google.visualization.NumberFormat({fractionDigits: 0});
formatter.format(data2, 1);   
  
 var ac = new google.visualization.LineChart(document.getElementById('ProductivityByDay'));
  ac.draw(data2, {
    title : 'Total User Productivity by Day',
    width: 800,
    height: 450,
    chartArea: {  width: "49%", height: "70%" },
    vAxis: {},
    hAxis: {
      format: 'MMM d'
    },
    backgroundColor: 'transparent',
    titleTextStyle: {
                color: '#111',
                fontSize: 16
            },
  
     areaOpacity: 0.1,
            lineWidth: 2,
            pointSize: 6,
            legend: {
                position: 'none'
            },
      legend: {position: 'right', textStyle: {color: 'black', fontSize: 12}}
  });
  
  $('#visualization').animate({opacity: 1},500)
}


    </script>

    <link href="../Style/TabStyle.css" rel="Stylesheet" type="text/css" />
    <asp:HiddenField ID="hdnAppType" runat ="server" />
    <asp:HiddenField ID="hdnScrollPos" runat="server" />
    <ucl:CheckedDropDownList ID="uclCheckedDropDown" runat="server" Visible="false" />
    <asp:TextBox ID="hdnSortColumn" runat="server" EnableViewState="true" Visible="false"
        Text="btnUser" />
    <asp:TextBox ID="hdnSortTeamColumn" runat="server" EnableTheming="true" Visible="false"
        Text="btnTeam" />
    <asp:TextBox ID="hdnProListSortColumn" runat="server" Visible="false" Text="[E].[EmployeeName]"></asp:TextBox>
    <asp:TextBox ID="hdnProListSortOrder" runat="server" Visible="false" Text="ASC"></asp:TextBox>
    <asp:TextBox ID="hdnProListSortColumnByTeam" runat="server" Visible="false" Text="[ET].[Title]"></asp:TextBox>
    <asp:TextBox ID="hdnProListSortOrderByTeam" runat="server" Visible="false" Text="ASC"></asp:TextBox>
    <asp:TextBox ID="hdnSortOrder" runat="server" EnableViewState="true" Visible="false"
        Text="ASC" />
    <asp:TextBox ID="hdnSortTeamOrder" runat="server" EnableViewState="true" Visible="false"
        Text="ASC" />
    <div class="well" style="clear: both; padding-top: 0px;">
        <div class="TabPanelHeader">
            Filter Options</div>
        <div style="text-align: center">
            <asp:UpdatePanel ID="upFilter" runat="server">
                <ContentTemplate>
                    <asp:HiddenField ID="hdnStartDate" runat="server" />
                    <asp:HiddenField ID="hdnEndDate" runat="server" />
                    <asp:ObjectDataSource ID="odsProductivityList" runat="server" SelectMethod="GetPagedEmployeeProductivityReportByEmployee"
                        TypeName="TPS360.Web.UI.EmployeeDataSource" SelectCountMethod="GetListCountEmployeeProductivityReportByEmployee"
                        EnablePaging="True" SortParameterName="sortExpression">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="dtReferralDate" Name="StartDate" PropertyName="StartDate"
                                Type="String" />
                            <asp:ControlParameter ControlID="dtReferralDate" Name="EndDate" PropertyName="EndDate"
                                Type="String" />
                            <asp:ControlParameter ControlID="ddlTeam" Name="TeamList" PropertyName="SelectedValue"
                                Type="String" />
                            <asp:ControlParameter ControlID="hdnUserSelectedValue" Name="User" PropertyName="Value"
                                Type="String" />
                            <asp:ControlParameter ControlID="hdnProListSortOrder" Name="SortOrder" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                    <asp:Label ID="lblDate" runat="server" Text="Date Range:"></asp:Label>
                    <ucl:DateRangePicker ID="dtReferralDate" runat="server" />
                    <div id="divTeam" runat="server" style =" display :inline">
                        <asp:Label ID="lblTeam" runat="server" Text="Team:"></asp:Label>
                        <asp:DropDownList ID="ddlTeam" runat="server" AutoPostBack="false" CssClass="CommonDropDownList">
                        </asp:DropDownList>
                    </div>
                    <asp:Label ID="lblUser" runat="server" Text="User:"></asp:Label>
                    <asp:DropDownList ID="ddlUser" runat="server" CssClass="CommonDropDownList">
                    </asp:DropDownList>
                    <asp:HiddenField ID="hdnUserSelectedValue" runat="server" Value="0"  />
                    <asp:Button ID="btnApply" runat="server" Text="Apply" CssClass="btn btn-primary"
                        OnClick="btnApply_Click" />
                    <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btn" OnClick="btnReset_Click" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <div class="tabbable">
        <ul class="nav nav-pills">
            <li class="active"><a href="#tab1" data-toggle="tab">Summary By User</a> </li>
            <li><a href="#tab2" data-toggle="tab">By Day</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="tab1">
                <div id="visualization" style="margin: 0 auto; margin-top: -35px; margin-bottom: -0px;
                    min-height: 400px; margin-left: 100PX;">
                </div>
            </div>
            <div class="tab-pane" id="tab2">
                <div id="ProductivityByDay" style="margin: 0 auto; margin-top: -35px; margin-bottom: -0px;
                    min-height: 400px; margin-left: 100PX;">
                </div>
            </div>
        </div>
    </div>
    <asp:UpdatePanel ID="upReport" runat="server">
        <ContentTemplate>
            <div style="overflow: inherit" id="bigDiv" onscroll='SetScrollPosition()'>
                <div style="padding-bottom: 40px">
                
                <div class="TableRow" style="padding-bottom: 5px" id="divExportButtons" runat="server"
                            visible="true">
                            <div class="TabPanelHeader nomargin" style="margin-bottom: 5px;border-style: none none none;">Report Results</div>
                            <div class="TabPanelHeader">
                              Summary by User 
                </div>
                
                <asp:ImageButton ID="btnExportToExcel" CssClass ="btn"  SkinID ="sknExportToExcel" runat ="server" ToolTip ="Export To Excel" OnClick ="btnExportToExcel_Click" />
                            <asp:ImageButton ID="btnExportToPDF" CssClass ="btn"  SkinID ="sknExportToPDF" runat ="server" ToolTip ="Export To PDF" OnClick ="btnExportToPDF_Click" />
                            <asp:ImageButton ID="btnExportToWord" CssClass ="btn"  SkinID ="sknExportToWord" runat ="server" ToolTip ="Export To Word" OnClick ="btnExportToWord_Click" />
                             </div>
                             
                            
              
                
                    <asp:ListView ID="lsvProductivityReport" runat="server" DataKeyNames="Id" EnableViewState="true"
                        DataSourceID="odsProductivityList" OnItemCommand="lsvProductivityReport_ItemCommand"
                        OnItemDataBound="lsvProductivityReport_ItemBound" OnPreRender="lsvProductivityReport_PreRender">
                        <LayoutTemplate>
                            <table id="tlbTemplate" runat="server" class="Grid" cellspacing="0" border="0">
                                <tr runat="server" id="trPrecise">
                                    <th>
                                        <asp:LinkButton ID="btnUser" runat="server" CommandName="Sort" ToolTip="Sort By User" CommandArgument="[E].[EmployeeName]"
                                            Text="User" TabIndex="2" />
                                    </th>
                                    <th>
                                        <asp:LinkButton ID="lnkNewCandidates" runat="server" CommandName="Sort" ToolTip="Sort By New Candidates Count" CommandArgument="NoOfNewCandidates"
                                            Text="New Candidates" TabIndex="3" EnableViewState="false" style="min-width: 130px" />
                                    </th>
                                    <th>
                                        <asp:LinkButton ID="lnkRequisitionPublished" runat="server" CommandName="Sort" ToolTip="Sort By Requisition Published Count"  CommandArgument="NoOfRequisitionPublished"
                                            Text="Requisition Published" TabIndex="4" Width="100%"/>
                                    </th>
                                    <th>
                                        <asp:LinkButton ID="btnSourced" runat="server" CommandName="Sort" ToolTip="Sort By Sourced Count" CommandArgument="NoOfCandidateSourced"
                                            Text="Sourced" TabIndex="5" />
                                    </th>
                                    <th>
                                        <asp:LinkButton ID="btnSort" runat="server" CommandName="Sort" ToolTip="Sort By Submissions Count" CommandArgument="NoOfCandidatesSubmissions"
                                            Text="Submissions" TabIndex="6" />
                                    </th>
                                    <th>
                                        <asp:LinkButton ID="lnkInterviews" runat="server" Text="Interviews" ToolTip="Sort By Interviews Count"
                                            CommandName="Sort" CommandArgument="NoOfInterviews"></asp:LinkButton>
                                    </th>
                                    <th>
                                        <asp:LinkButton ID="lnkoffers" runat="server" Text="Offers" ToolTip="Sort By Offers Count"
                                            CommandArgument="NoOfCandidatesOffers" CommandName="Sort"></asp:LinkButton>
                                    </th>
                                    <th>
                                        <asp:LinkButton ID="btnoffersrejected" runat="server" Text="Offers Decline" CommandName="Sort" ToolTip="Sort By Offers Decline Count"
                                            CommandArgument="NoOfCamdidateOffersRejected" TabIndex="7" style="min-width: 140px" />
                                    </th>
                                    <th >
                                        <asp:LinkButton ID="btnjoined" runat="server" Text="Joined" CommandName="Sort" ToolTip="Sort By Joined Count" CommandArgument="NoOfCandidateJoined"
                                            TabIndex="7" />
                                    </th>
                                    <th id="thPendingJoiners" runat ="server" >

                                     <asp:LinkButton ID="btnjoiners" runat="server" Text="Pending Joiners" CommandName="Sort" ToolTip="Sort By Pending Joiners Count" CommandArgument="PendingJoiners"

                                            TabIndex="8"  />
                                    </th>
                                </tr>
                                <tr>
                                    <td colspan="9" style="padding: 0px 0px 0px 0px; border-style: none none none none">
                                    </td>
                                </tr>
                                <tr id="itemPlaceholder" runat="server">
                                </tr>
                                <tr class="Pager">
                                    <td colspan="9"  runat="server" id="tdPager">
                                        <ucl:Pager ID="pagerControl" runat="server" EnableViewState="true" NoOfRowsCookie="ProductivityReportbyEmployeeRowPerPage" />
                                    </td>
                                </tr>
                            </table>
                        </LayoutTemplate>
                        <EmptyDataTemplate>
                            <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                                <tr>
                                    <td>
                                        No data was returned.
                                    </td>
                                </tr>
                            </table>
                        </EmptyDataTemplate>
                        <ItemTemplate>
                            <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                                <td>
                                    <asp:Label ID="lblUser" runat="server" Width="80px" />
                                </td>
                                <td>
                                    <asp:Label ID="lblNewCandidates" runat="server" />
                                </td>
                                <td>
                                    <asp:Label ID="lblRequisitionPublished" runat="server" />
                                </td>
                                <td>
                                    <asp:Label ID="lblSourced" runat="server" />
                                </td>
                                <td>
                                    <asp:Label ID="lblSubmissions" runat="server" />
                                </td>
                                <td>
                                    <asp:Label ID="lblInterviews" runat="server" />
                                </td>
                                <td>
                                    <asp:Label ID="lblOffers" runat="server" Style="min-height: 100px;" />
                                </td>
                                <td>
                                    <asp:Label ID="lblOfferrejected" runat="server" />
                                </td>
                                <td>
                                    <asp:Label ID="lblJoined" runat="server" />
                                </td>
                                <td  id="tdPendingJoiners"  runat ="server" >
                                    <asp:Label ID="lblPendingJoiners" runat="server"  />
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:ListView>
                    </div>
                    </asp:Panel>
                
                </div>
                <asp:ObjectDataSource ID="odsProductivityTeamList" runat="server" SelectMethod="GetPagedEmployeeProductivityReportByTeam"
                    TypeName="TPS360.Web.UI.EmployeeDataSource" SelectCountMethod="GetListCountEmployeeProductivityReportByTeam"
                    EnablePaging="True" SortParameterName="sortExpression">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="dtReferralDate" Name="StartDate" PropertyName="StartDate"
                            Type="String" />
                        <asp:ControlParameter ControlID="dtReferralDate" Name="EndDate" PropertyName="EndDate"
                            Type="String" />
                        <asp:ControlParameter ControlID="ddlTeam" Name="TeamList" PropertyName="SelectedValue"
                            Type="String" />
                        <asp:ControlParameter ControlID="hdnUserSelectedValue" Name="User" PropertyName="Value"
                            Type="String" />
                        <asp:ControlParameter ControlID="hdnProListSortOrderByTeam" Name="SortOrder" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                
                <div class="TableRow" style="padding-bottom: 5px" id="divTeamExportButtons" runat="server"
                            visible="true">
                            
                            <div class="TabPanelHeader">
                              Summary by Team 
                </div>
                             <asp:ImageButton ID="teamExportToExcel" CssClass ="btn"  SkinID ="sknExportToExcel" runat ="server" ToolTip ="Export To Excel" OnClick ="teamExportToExcel_Click " />
                            <asp:ImageButton ID="teamExportToPDF" CssClass ="btn"  SkinID ="sknExportToPDF" runat ="server" ToolTip ="Export To PDF" OnClick ="teamExportToPDF_Click " />
                            <asp:ImageButton ID="teamExportToWord" CssClass ="btn"  SkinID ="sknExportToWord" runat ="server" ToolTip ="Export To Word" OnClick ="teamExportToWord_Click " />
                            
                            
                        </div>
                
                <asp:ListView ID="lsvteamReport" DataSourceID="odsProductivityTeamList" runat="server"
                    EnableViewState="true" DataKeyNames="Id" OnItemDataBound="lsvteamReport_ItemBound"
                    OnItemCommand="lsvteamReport_ItemCommand" OnPreRender="lsvteamReport_PreRender">
                    <LayoutTemplate>
                        <table id="tlbTemplate" runat="server" class="Grid" cellspacing="0" border="0">
                            <tr id="trHeadLevel" runat="server">
                                <th>
                                    <asp:LinkButton ID="btnTeam" runat="server" CommandName="Sort" ToolTip="Sort By Team" CommandArgument="[ET].[Title]"
                                        Text="Team" TabIndex="2" />
                                </th>
                                <th >
                                    <asp:LinkButton ID="lnkNewCandidates" runat="server" CommandName="Sort" ToolTip="Sort By New Candidates Count" CommandArgument="NoOfNewCandidates"
                                        Text="New Candidates" TabIndex="3" EnableViewState="false" style="min-width: 130px"/>
                                </th>
                                <th>
                                    <asp:LinkButton ID="lnkRequisitionPublished" runat="server" CommandName="Sort" ToolTip="Sort By Requisition Published Count" CommandArgument="NoOfRequisitionPublished"
                                        Text="Requisition Published" TabIndex="4" />
                                </th>
                                <th>
                                    <asp:LinkButton ID="btnSourced" runat="server" CommandName="Sort" ToolTip="Sort By Sourced Count" CommandArgument="NoOfCandidateSourced"
                                        Text="Sourced" TabIndex="5" />
                                </th>
                                <th>
                                    <asp:LinkButton ID="btnSort" runat="server" CommandName="Sort" ToolTip="Sort By Submissions Count" CommandArgument="NoOfCandidatesSubmissions"
                                        Text="Submissions" TabIndex="6" Width="100%" />
                                </th>
                                <th>
                                    <asp:LinkButton ID="lnkInterviews" runat="server" Text="Interviews" ToolTip="Sort By Interviews Count"
                                        CommandName="Sort" CommandArgument="NoOfInterviews"></asp:LinkButton>
                                </th>
                                <th>
                                    <asp:LinkButton ID="lnkoffers" runat="server" Text="Offers" ToolTip="Sort By Offers Count"
                                        CommandArgument="NoOfCandidatesOffers" CommandName="Sort"></asp:LinkButton>
                                </th>
                                <th>
                                    <asp:LinkButton ID="btnoffersrejected" runat="server" Text="Offers Decline" ToolTip="Sort By Offers Decline Count" CommandName="Sort"
                                        CommandArgument="NoOfCamdidateOffersRejected" TabIndex="7" style="min-width: 130px" />
                                </th>
                                <th>
                                    <asp:LinkButton ID="btnjoined" runat="server" Text="Joined" CommandName="Sort" ToolTip="Sort By Joined Count" CommandArgument="NoOfCandidateJoined"
                                        TabIndex="7" Width="50%" />
                                </th>
                                <th id="thPendingJoiners" runat ="server" >

                                    <asp:LinkButton ID="btnjoinersteam" runat="server" Text="Pending Joiners" CommandName="Sort" ToolTip="Sort By Pending Joiners Count" CommandArgument="PendingJoiners"
                                        TabIndex="8" Width="50%" />
                                </th>
                            </tr>
                            <tr>
                                <td colspan="9" style="padding: 0px 0px 0px 0px; border-style: none none none none">
                                </td>
                            </tr>
                            <tr id="itemPlaceholder" runat="server">
                            </tr>
                            <tr class="Pager">
                                <td colspan="10" runat="server" id="tdPager">
                                    <ucl:Pager ID="pagerControl" runat="server" EnableViewState="true" NoOfRowsCookie="ProductivityReportbyTeamRowPerPage" />
                                </td>
                            </tr>
                        </table>
                    </LayoutTemplate>
                    <EmptyDataTemplate>
                        <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                            <tr>
                                <td>
                                    No data was returned.
                                </td>
                            </tr>
                        </table>
                    </EmptyDataTemplate>
                    <ItemTemplate>
                        <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                            <td>
                                <asp:Label ID="lblTeam" runat="server" Width="80px" />
                            </td>
                            <td>
                                <asp:Label ID="lblNewCandidates" runat="server" />
                            </td>
                            <td>
                                <asp:Label ID="lblRequisitionPublished" runat="server" />
                            </td>
                            <td>
                                <asp:Label ID="lblsourced" runat="server" />
                            </td>
                            <td>
                                <asp:Label ID="lblSubmissions" runat="server" />
                            </td>
                            <td>
                                <asp:Label ID="lblInterviews" runat="server" />
                            </td>
                            <td>
                                <asp:Label ID="lblOffers" runat="server" Style="min-height: 100px;" />
                            </td>
                            <td>
                                <asp:Label ID="lblofferrejected" runat="server" />
                            </td>
                            <td>
                                <asp:Label ID="lblJoined" runat="server" />
                            </td>
                            <td id="tdPendingJoiners" runat ="server" >
                                <asp:Label ID="lblPendingJoiners" runat="server"/>
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:ListView>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnExportToPDF" />
            <asp:PostBackTrigger ControlID="btnExportToExcel" />
            <asp:PostBackTrigger ControlID="btnExportToWord" />
        </Triggers>
         <Triggers>
            <asp:PostBackTrigger ControlID="teamExportToExcel" />
            <asp:PostBackTrigger ControlID="teamExportToPDF" />
            <asp:PostBackTrigger ControlID="teamExportToWord" />
        </Triggers>
        
    </asp:UpdatePanel>
</asp:Content>
