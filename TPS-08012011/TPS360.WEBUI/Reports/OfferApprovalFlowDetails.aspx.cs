﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using TPS360.Common.Helper;
using System.Text;
using TPS360.Web.UI.Helper;
using TPS360.Common.BusinessEntities;
using ExpertPdf.HtmlToPdf;
using System.Collections.Generic;
using System.Drawing;
using TPS360.Common.Shared;

namespace TPS360.Web.UI.Reports
{
    public partial class OfferApprovalFlowDetails : BasePage
    {
        #region Variables
        private bool isAccess = false;
        private static string UrlForCandidate = string.Empty;
        private static int SitemapIdForCandidate = 0;
        bool _IsAccessToCandidate = true;
        #endregion

        #region Properties
        int CandidateId
        {
            get
            {
                if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]))
                {
                    return Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]);

                }
                else
                {
                    return 0;
                }

            }
        }

        #endregion

        # region Page Event
        protected void Page_Load(object sender, EventArgs e)
        {
            ddlJobStatus.Attributes.Add("onChange", "return RequisitionStatusOnChange('" + ddlJobStatus.ClientID + "','" + ddlRequisition.ClientID + "')");
            ddlRequisition.Attributes.Add("onChange", "return RequisitionOnChange('" + ddlRequisition.ClientID + "','" + hdnRequisitionSelected.ClientID + "')");
            if (!IsPostBack)
            {
                PrepareView(); btnSearch_Click(sender, e);
            }
        }
        #endregion

        #region Methods
        private void PrepareView()
        {
            MiscUtil.PopulateMemberListByRole(ddlEmployee, ContextConstants.ROLE_EMPLOYEE, Facade);
            MiscUtil.PopulateMemberListByRole(ddlCandidate, ContextConstants.ROLE_CANDIDATE, Facade);
            MiscUtil.PopulateOfferApprovalStatusMaster(ddlStatusType, Facade);
            PopulateRequistionStatusDropdowns();
            PopulateRequisition(0);
        }
        private void LoadActionType()
        {
            DropDownList list = new DropDownList();
            foreach (EventLogForRequisition r in Enum.GetValues(typeof(EventLogForRequisition)))
            {
                ListItem item = new ListItem(Enum.GetName(typeof(EventLogForRequisition), r), r.ToString());
                item.Text = TPS360.Common.EnumHelper.GetDescription(r).TrimStart();
                item.Value = item.Text;
                list.Items.Add(item);
            }
            foreach (EventLogForCandidate r in Enum.GetValues(typeof(EventLogForCandidate)))
            {
                ListItem item = new ListItem(Enum.GetName(typeof(EventLogForCandidate), r), r.ToString());
                item.Text = TPS360.Common.EnumHelper.GetDescription(r).Replace("for <candidate name>", "").Replace("to <candidate name>", "").Replace("<candidate name>", "").Replace("to <new status level>", "").TrimStart();
                item.Value = TPS360.Common.EnumHelper.GetDescription(r).Replace("to <new status level>", "");
                list.Items.Add(item);
            }
            var sort = list.Items.Cast<ListItem>().OrderBy(o => o.Text);

            foreach (ListItem item in sort)
            {
                ddlStatusType.Items.Add(item);
            }
            ddlStatusType.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));
        }
        private void PopulateRequistionStatusDropdowns()
        {
            MiscUtil.PopulateRequistionStatus(ddlJobStatus, Facade);
            ddlJobStatus.Items.Insert(0, new ListItem("Any", "0"));
        }
        public void MessageAnswered(object sender, ConfirmationWindow.MsgBoxEventArgs e)
        {

            if (e.Answer == ConfirmationWindow.enmAnswer.OK)
            {
                // repor("pdf");
            }

        }

        private void BindList()
        {
            this.lsvOfferApprovalFlowDetails.DataSourceID = "odsOfferApprovalFlowDetails";
            this.divlsvOfferApprovalFlowDetails.Visible = true;
            this.lsvOfferApprovalFlowDetails.DataBind();
        }

        private void PlaceUpDownArrow()
        {
            try
            {
                LinkButton lnk = (LinkButton)lsvOfferApprovalFlowDetails.FindControl(hdnSortColumn.Text);
                HtmlTableCell im = (HtmlTableCell)lnk.Parent;
                im.Attributes.Add("class", (hdnSortOrder.Text == "ASC" ? "Ascending" : "Descending"));

            }
            catch
            {
            }

        }
        protected void ddlJobStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            MiscUtil.LoadAllControlState(Request, this);
        }
        void PopulateRequisition(int StatusId)
        {

            ddlRequisition.Items.Clear();
            System.Collections.ArrayList arr = Facade.GetAllJobPostingListByStatusId(StatusId);
            JobPosting j = new JobPosting();
            j.Id = 0;
            j.JobTitle = "Any";
            arr.Insert(0, j);
            ddlRequisition.DataSource = arr;
            ddlRequisition.DataTextField = "JobTitle";
            ddlRequisition.DataValueField = "Id";
            ddlRequisition.DataBind();
            ControlHelper.SelectListByValue(ddlRequisition, hdnRequisitionSelected.Value);
        }
        private void ClearControls()
        {
            ddlRequisition.SelectedIndex = 0;
            ddlEmployee.SelectedIndex = 0;
            ddlCandidate.SelectedIndex = 0;
            ddlJobStatus.SelectedIndex = 0;
            ddlStatusType.SelectedIndex = 0;
            //wcdStartDate.Value = wcdEndDate.Value = null;
            lsvOfferApprovalFlowDetails.Visible = false;
            dtPicker.ClearRange();
        }

        private void GenerateHiringLogReport(string format)
        {
            string exportFileName = "OfferApprovalFlowDetailsReport-" + DateTime.Now.ToString("yyyy-MM-dd");
            if (string.Equals(format, "word"))
            {
                Response.AddHeader("content-disposition", "attachment;filename=" + exportFileName + ".doc");
                Response.ContentEncoding = System.Text.Encoding.ASCII;
                Response.ContentType = "application/msword";
                Response.Output.Write(GetHiringLogReportTable());
                Response.Flush();
                Response.End();
            }
            else if (string.Equals(format, "excel"))
            {
                Response.AddHeader("content-disposition", "attachment;filename=" + exportFileName + ".xls");
                Response.ContentEncoding = System.Text.Encoding.ASCII;
                Response.ContentType = "application/msexcel";
                Response.Output.Write(GetHiringLogReportTable());
                Response.Flush();
                Response.End();
            }
            else if (string.Equals(format, "pdf"))
            {
                PdfConverter pdfConverter = new PdfConverter();
                UnicodeEncoding unicodeEncoding = new UnicodeEncoding();
                pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
                pdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Normal;
                pdfConverter.PdfDocumentOptions.ShowHeader = false;
                pdfConverter.PdfDocumentOptions.ShowFooter = false;
                pdfConverter.PdfDocumentOptions.LeftMargin = 15;
                pdfConverter.PdfDocumentOptions.RightMargin = 5;
                pdfConverter.PdfDocumentOptions.TopMargin = 15;
                pdfConverter.PdfDocumentOptions.BottomMargin = 5;
                pdfConverter.PdfDocumentOptions.GenerateSelectablePdf = true;
                pdfConverter.PdfDocumentOptions.ShowHeader = false;
                pdfConverter.PdfHeaderOptions.DrawHeaderLine = false;
                pdfConverter.PdfFooterOptions.FooterTextColor = Color.Black;
                pdfConverter.PdfFooterOptions.DrawFooterLine = true;
                pdfConverter.PdfFooterOptions.PageNumberText = "Page";
                pdfConverter.PdfFooterOptions.ShowPageNumber = true;

                pdfConverter.LicenseKey = ContextConstants.EPHTMLTOPDFKEY;
                byte[] downloadBytes = pdfConverter.GetPdfBytesFromHtmlString(GetHiringLogReportTable());

                System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
                response.Clear();
                response.AddHeader("Content-Type", "binary/octet-stream");
                response.AddHeader("Content-Disposition", "attachment; filename=" + exportFileName + ".pdf; size=" + downloadBytes.Length.ToString());
                response.Flush();
                response.BinaryWrite(downloadBytes);
                response.Flush();
                response.End();
            }
        }

        private string GetHiringLogReportTable()
        {
            StringBuilder reqReport = new StringBuilder();
            JobPostingDataSource ApprovalFlowDetailsDataSource = new JobPostingDataSource();
            IList<JobPosting> ApprovalFlowDetails = ApprovalFlowDetailsDataSource.GetPagedOfferApprovalFlowDetails((ddlRequisition.SelectedIndex > 0 ? Convert.ToInt32(ddlRequisition.SelectedValue) : 0), (ddlJobStatus.SelectedIndex > 0 ? Convert.ToInt32(ddlJobStatus.SelectedValue) : 0), (ddlEmployee.SelectedIndex > 0 ? Convert.ToInt32(ddlEmployee.SelectedValue) : 0), dtPicker.StartDate.ToString(), dtPicker.EndDate.ToString(), (ddlStatusType.SelectedIndex > 0 ? Convert.ToInt32(ddlStatusType.SelectedValue) : 0), (ddlCandidate.SelectedIndex > 0 ? Convert.ToInt32(ddlCandidate.SelectedValue) : 0), null, -1, -1);

            if (ApprovalFlowDetails != null)
            {
                string WebRoot = ConfigurationManager.AppSettings["WebRoot"].ToString();
                string LogoPath;
                if (string.IsNullOrEmpty(HttpRuntime.AppDomainAppVirtualPath))
                {
                    LogoPath = WebRoot;
                }
                else
                {
                    LogoPath = WebRoot + HttpRuntime.AppDomainAppVirtualPath;
                }

                reqReport.Append("<table style='font-family : calibriz;width:100%; border-collapse:collapse; border-spacing: 1px;' align='left' border = '1' bordercolor='#000000'  cellspacing='0' cellpadding='0'>");

                reqReport.Append("    <tr>");
                reqReport.Append("        <td align=left  width='300' height='75'>&nbsp;<img src='" + UrlConstants.ApplicationBaseUrl + "Images/logo-left-75px.png' style= style='height:56px;width:56px'/></td>");
                reqReport.Append("    </tr>");
                reqReport.Append(" <tr>");
                reqReport.Append("     <th>Date & Time</th>");
                reqReport.Append("     <th>Requisition</th>");
                reqReport.Append("     <th>Candidate</th>");
                reqReport.Append("     <th>User</th>");
                reqReport.Append("     <th>Status </th>");
                reqReport.Append("     <th>Comment</th>");
                reqReport.Append(" </tr>");

                foreach (JobPosting ApprovalFlow in ApprovalFlowDetails)
                {
                    if (ApprovalFlow != null)
                    {
                        reqReport.Append(" <tr>"); 
                        //reqReport.Append("     <td>" + ApprovalFlow.CreateDate.ToString() + "&nbsp;</td>");
                        reqReport.Append("     <td>" + ApprovalFlow.CreateDate.ToString("dd-MMM-yyyy hh:mm:ss") + "&nbsp;</td>");
                        reqReport.Append("     <td>" + ApprovalFlow.JobTitle + " [" + ApprovalFlow.JobPostingCode + "]" + "&nbsp;</td>");
                        reqReport.Append("     <td>" + ApprovalFlow.CandidateName + " [" + ApprovalFlow.MemberId + "]" + "&nbsp;</td>");
                        reqReport.Append("     <td>" + ApprovalFlow.Name + " [" + ApprovalFlow.GID + "]" + "&nbsp;</td>");
                        reqReport.Append("     <td>" + ApprovalFlow.Status + "&nbsp;</td>");
                        reqReport.Append("     <td>" + ApprovalFlow.Comment + "&nbsp;</td>");
                        reqReport.Append(" </tr>");
                    }
                }

                reqReport.Append("    <tr>");
                reqReport.Append("        <td align=left  width='300' height='75'><img src='" + UrlConstants.ApplicationBaseUrl + "Images/logo-left-75px.png' style= style='height:56px;width:56px;'/></td>");
                reqReport.Append("    </tr>");

                reqReport.Append(" </table>");
            }
            return reqReport.ToString();
        }
        #endregion
        #region ButtonEvents
        protected void btnClear_Click(object sender, EventArgs e)
        {
            ClearControls();
            divExportButtons.Visible = false;
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {

            lsvOfferApprovalFlowDetails.Visible = true;
            hdnScrollPos.Value = "0";
            lsvOfferApprovalFlowDetails.Items.Clear();
            ASP.controls_pagercontrol_ascx PagerControls = (ASP.controls_pagercontrol_ascx)this.lsvOfferApprovalFlowDetails.FindControl("pagerControl");
            if (PagerControls != null)
            {
                DataPager pager = (DataPager)PagerControls.FindControl("pager");
                if (pager != null)
                {
                    if (pager.Page.IsPostBack)
                        pager.SetPageProperties(0, pager.MaximumRows, true);
                }
            }
            BindList();
            if (lsvOfferApprovalFlowDetails != null && lsvOfferApprovalFlowDetails.Items.Count > 0)
            {
                divExportButtons.Visible = true;
                lsvOfferApprovalFlowDetails.Visible = true;

                if (hdnSortColumn.Text == "") hdnSortColumn.Text = "btnDate";
                if (hdnSortOrder.Text == "") hdnSortOrder.Text = "DESC";
                PlaceUpDownArrow();
            }
            else divExportButtons.Visible = false;
            string pagesize = "";
            pagesize = (Request.Cookies["HiringLogReportRowPerPage"] == null ? "" : Request.Cookies["HiringLogReportRowPerPage"].Value); ;
            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvOfferApprovalFlowDetails.FindControl("pagerControl");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)PagerControl.FindControl("pager");
                if (pager != null)
                {
                    pager.PageSize = Convert.ToInt32(pagesize == "" ? "20" : pagesize);
                    if (pager.Page.IsPostBack)
                        pager.SetPageProperties(0, pager.MaximumRows, true);
                }
            }
        }

        protected void btnExportToPDF_Click(object sender, EventArgs e)
        {
            GenerateHiringLogReport("pdf");
        }
        protected void btnExportToExcel_Click(object sender, EventArgs e)
        {
            GenerateHiringLogReport("excel");
        }
        protected void btnExportToWord_Click(object sender, EventArgs e)
        {
            GenerateHiringLogReport("word");
        }
        #endregion

        #region Listview Events
        protected void lsvOfferApprovalFlowDetails_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            StringBuilder ColumnOption = new StringBuilder();
            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                JobPosting  jobposting = ((ListViewDataItem)e.Item).DataItem as JobPosting;

                if (jobposting != null)
                {
                    Label lblDate = (Label)e.Item.FindControl("lblDate");
                    Label lblUser = (Label)e.Item.FindControl("lblUser");
                    Label lblCandidate = (Label)e.Item.FindControl("lblCandidate");
                    Label lblJobtitle = (Label)e.Item.FindControl("lblJobtitle");
                    Label lblComment = (Label)e.Item.FindControl("lblComment");
                    Label lblStatus = (Label)e.Item.FindControl("lblStatus");
                    //lblDate.Text = jobposting.CreateDate.ToString();
                    lblDate.Text = jobposting.CreateDate.ToString("dd-MMM-yyyy hh:mm:ss"); //DateTime.ParseExact(jobposting.CreateDate, "DD-MMM-YYYY", null);
                    lblUser.Text = jobposting.Name + " [" + jobposting.GID + "]";
                    lblCandidate.Text = jobposting.CandidateName  + " [" + jobposting.MemberId  + "]";
                    lblJobtitle.Text = jobposting.JobTitle + " [" + jobposting.JobPostingCode + "]";
                    lblStatus.Text = jobposting.Status;
                    lblComment.Text = jobposting.Comment ;
                }
            }
        }

        protected void lsvOfferApprovalFlowDetails_PreRender(object sender, EventArgs e)
        {
            divExportButtons.Visible = lsvOfferApprovalFlowDetails.Items.Count > 0;
            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvOfferApprovalFlowDetails.FindControl("pagerControl");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)PagerControl.FindControl("pager");
                if (pager != null)
                {
                    DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                    if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
                }
                HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
                if (hdnRowPerPageName != null) hdnRowPerPageName.Value = "ProductivityReportRowPerPage";
            }
            PlaceUpDownArrow();

            if (IsPostBack)
            {
                if (PagerControl == null)
                {
                    lsvOfferApprovalFlowDetails.Items.Clear();
                    lsvOfferApprovalFlowDetails.DataSource = null;
                    lsvOfferApprovalFlowDetails.DataBind();
                    divExportButtons.Visible = lsvOfferApprovalFlowDetails.Items.Count > 0;
                }
            }
        }

        protected void lsvOfferApprovalFlowDetails_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Sort")
                {
                    LinkButton lnkbutton = (LinkButton)e.CommandSource;
                    if (hdnSortColumn.Text == lnkbutton.ID)
                    {
                        if (hdnSortOrder.Text == "ASC") hdnSortOrder.Text = "DESC";
                        else hdnSortOrder.Text = "ASC";
                    }
                    else
                    {
                        hdnSortColumn.Text = lnkbutton.ID;
                        hdnSortOrder.Text = "ASC";
                    }
                }
            }
            catch
            { }
        }
        #endregion
    }
}
