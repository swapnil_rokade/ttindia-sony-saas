﻿<%@ Page Language="C#" MasterPageFile="~/Default.master" AutoEventWireup="true" CodeFile="RequisitionApprovalFlowDetails.aspx.cs"
    EnableEventValidation="false" Inherits="TPS360.Web.UI.Reports.RequisitionApprovalFlowDetails"
    Title="Requisition Approval Flow Details" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Controls/PagerControl.ascx" TagName="Pager" TagPrefix="ucl" %>
<%@ Register Src="~/Controls/ConfirmationWindow.ascx" TagName="Confirm" TagPrefix="ucl" %>
<%@ Register Src="~/Controls/DateRangePicker.ascx" TagName="DateRangePicker" TagPrefix="ucl" %>
<asp:Content ID="Content2" ContentPlaceHolderID="cphHomeMasterTitle" runat="Server">
    Requisition Approval Flow Details
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphHomeMaster" runat="Server">

    <script src="../Scripts/ToolTipscript.js"></script>
    <script src="../js/JobPostingByStatus.js"></script>
    <link type="text/css" href="../Style/ToolTip.css" rel="Stylesheet" />
    <script type="text/javascript" language="javascript" src="../js/AjaxVariables.js"></script>
    <script type="text/javascript" language="javascript" src="../js/AjaxScript.js"></script>
    <script src="../Scripts/jsUpdateProgress.js" type="text/javascript"></script>

    <script type="text/javascript" language="javascript">
        var ModalProgress = '<%= Modal.ClientID %>';
        Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(PageLoadedReq);
        function PageLoadedReq(sender, args) {
            try {
                var hdnScroll = document.getElementById('<%=hdnScrollPos.ClientID%>');
                var bigDiv = document.getElementById('ctl00_cphHomeMaster_divlsvInterview');
                bigDiv.scrollLeft = hdnScroll.value;
            }
            catch (e) {
            }
        }
        function SetScrollPosition() {
            var hdnScroll = document.getElementById('<%=hdnScrollPos.ClientID%>');
            var bigDiv = document.getElementById('ctl00_cphHomeMaster_divlsvInterview');
            hdnScroll.value = bigDiv.scrollLeft;
        }
                
    </script>

    <asp:TextBox ID="hdnSortColumn" runat="server" Visible="false" />
    <asp:TextBox ID="hdnSortOrder" runat="server" Visible="false" />
    <ucl:Confirm ID="uclConfirm" runat="server"></ucl:Confirm>
    <asp:UpdatePanel ID="pnlInterviewReports" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div style="text-align: left">
                <asp:Label ID="lbMessage" runat="server" EnableViewState="false"></asp:Label>
            </div>
            <asp:HiddenField ID="hdnScrollPos" runat="server" />
            <asp:HiddenField ID="hdnRequisitionSelected" runat="server" Value="0" />
            <asp:ObjectDataSource ID="odsRequisitionApprovalFlowDetails" runat="server" SelectMethod="GetPagedRequisitionApprovalFlowDetails" TypeName="TPS360.Web.UI.JobPostingDataSource"
                SelectCountMethod="GetListCountRequisitionApprovalFlowDetails" EnablePaging="True" SortParameterName="sortExpression">
                <SelectParameters>
                    <asp:ControlParameter ControlID="dtPicker" Name="StartDate" PropertyName="StartDate"
                        Type="String" />
                    <asp:ControlParameter ControlID="dtPicker" Name="EndDate" PropertyName="EndDate"
                        Type="String" />
                    <asp:ControlParameter ControlID="ddlRequisition" Name="JobPostingId" PropertyName="SelectedValue"
                        Type="String" />
                    <asp:ControlParameter ControlID="ddlJobStatus" Name="ReqStatusId" PropertyName="SelectedValue"
                        Type="String" />
                    <asp:ControlParameter ControlID="ddlEmployee" Name="CreatorId" PropertyName="SelectedValue"
                        Type="String" />
                    <asp:ControlParameter ControlID="ddlStatusType" Name="STATUSId" PropertyName="SelectedValue"
                        Type="String" />
                </SelectParameters>
            </asp:ObjectDataSource>
            <asp:Panel ID="pnlSearchRegion" runat="server" DefaultButton="btnSearch">
                <div style="width: 100%;">
                    <div>
                        <div class="TableRow">
                            <div class="TabPanelHeader nomargin" style="border-style: none none none;">
                                Filter Options</div>
                        </div>
                        <div class="TableRow" style="text-align: center">
                            <asp:Label ID="lblMessage" runat="server" EnableViewState="false"></asp:Label>
                        </div>
                        <asp:Panel ID="pnlSearchContent" runat="server" Visible="true" Style="overflow: hidden;">
                            <div class="FormLeftColumn" style="width: 48%">
                                <div class="TableRow">
                                    <div class="TableFormLeble" style="width: 40%;">
                                        <asp:Label ID="lblDate" runat="server" EnableViewState="false" Text="Date"></asp:Label>:
                                    </div>
                                    <div class="TableFormContent">
                                        <ucl:DateRangePicker ID="dtPicker" runat="server" />
                                    </div>
                                </div>
                                <div class="TableRow">
                                    <div class="TableFormLeble" style="width: 40%;">
                                        <asp:Label EnableViewState="false" ID="lblEmployee" runat="server" Text="User"></asp:Label>:
                                    </div>
                                    <div class="TableFormContent">
                                        <asp:DropDownList ID="ddlEmployee" CssClass="CommonDropDownList" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="TableRow">
                                    <div class="TableFormLeble" style="width: 40%;">
                                        <asp:Label EnableViewState="false" ID="lblStatusType" runat="server" Text="Status Type"></asp:Label>:
                                    </div>
                                    <div class="TableFormContent">
                                        <asp:DropDownList ID="ddlStatusType" CssClass="CommonDropDownList" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="FormRightColumn" style="width: 48%">
                                    <div class="TableRow">
                            <div class="TableFormLeble" style="width: 40%;">
                                <asp:Label EnableViewState="false" ID="lblJobStatusHeader" runat="server" Text="By Requisition Status"></asp:Label>:
                            </div>
                            <div class="TableFormContent">
                                <asp:DropDownList EnableViewState="true" ID="ddlJobStatus" runat="server" CssClass="CommonDropDownList"
                                    AutoPostBack="false" OnSelectedIndexChanged="ddlJobStatus_SelectedIndexChanged">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="TableRow">
                            <div class="TableFormLeble" style="width: 40%;">
                                <asp:Label EnableViewState="false" ID="lblByRequisition" runat="server" Text="By Requisition"></asp:Label>:
                            </div>
                            <div class="TableFormContent">
                                <asp:DropDownList EnableViewState="true" ID="ddlRequisition" runat="server" CssClass="CommonDropDownList"
                                    AutoPostBack="false">
                                </asp:DropDownList>
                            </div>
                        </div>
                                <%--<div class="TableRow">
                                    <div class="TableFormLeble" style="width: 40%;">
                                        <asp:Label EnableViewState="false" ID="lblRequisition" runat="server" Text="Requisition"></asp:Label>:
                                    </div>
                                    <div class="TableFormContent">
                                        <asp:DropDownList ID="ddlRequisition" runat="server" CssClass="CommonDropDownList">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="TableRow">
                                    <div class="TableFormLeble" style="width: 40%;">
                                        <asp:Label EnableViewState="true" ID="lblReqStatus" runat="server" Text="Requisition Status"></asp:Label>:
                                    </div>
                                    <div class="TableFormContent">
                                        <asp:DropDownList ID="ddlReqStatus" CssClass="CommonDropDownList" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                </div>--%>
                            </div>
                        </asp:Panel>
                        <div class="HeaderDevider">
                        </div>
                        <div class="TableRow" style="text-align: center; padding-top: 5px">
                            <asp:LinkButton ID="btnSearch" runat="server" Text="Search" AlternateText="Search"
                                CssClass="btn btn-primary" EnableViewState="false" OnClick="btnSearch_Click"><i class="icon-search icon-white"></i> Search</asp:LinkButton>
                            <asp:Button ID="btnClear" runat="server" Text="Clear" AlternateText="Clear" CssClass="btn"
                                EnableViewState="false" CausesValidation="false" OnClick="btnClear_Click" />
                        </div>
                        <div class="TableRow" style="padding-bottom: 5px" id="divExportButtons" runat="server"
                            visible="false">
                            <div class="TabPanelHeader nomargin" style="margin-bottom: 5px; border-style: none none none;">
                                Report Results</div>
                            <asp:ImageButton ID="btnExportToExcel" CssClass="btn" SkinID="sknExportToExcel" runat="server"
                                ToolTip="Export To Excel" OnClick="btnExportToExcel_Click" />
                            <asp:ImageButton ID="btnExportToPDF" CssClass="btn" SkinID="sknExportToPDF" runat="server"
                                ToolTip="Export To PDF" OnClick="btnExportToPDF_Click" />
                            <asp:ImageButton ID="btnExportToWord" CssClass="btn" SkinID="sknExportToWord" runat="server"
                                ToolTip="Export To Word" OnClick="btnExportToWord_Click" />

                        </div>
                    </div>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlGridRegion" runat="server">
                <div id="divlsvRequisitionApprovalFlowDetails" runat="server" class="GridContainer" style="overflow: auto;
                    overflow-y: hidden; width: 100%; text-align: left;" onscroll='SetScrollPosition()'>
                    <asp:ListView ID="lsvRequisitionApprovalFlowDetails" runat="server" DataKeyNames="Id" OnItemDataBound="lsvRequisitionApprovalFlowDetails_ItemDataBound"
                        OnItemCommand="lsvRequisitionApprovalFlowDetails_ItemCommand" OnPreRender="lsvRequisitionApprovalFlowDetails_PreRender">
                        <LayoutTemplate>
                            <table id="tlbTemplate" runat="server" class="Grid" cellspacing="0" border="0">
                                <tr>
                                    <th runat="server" id="thDate" style="width: 15%" enableviewstate="false">
                                        <asp:LinkButton runat="server" ID="btnDate" CommandName="Sort" CommandArgument="[RAC].[DateCreated]"
                                            Text="Date & Time" ToolTip="Sort By Date & Time" />
                                    </th>
                                     <th runat="server" id="thRequisition" style="width: 25%" enableviewstate="false">
                                        <asp:LinkButton runat="server" ID="btnRequisition" CommandName="Sort" CommandArgument="[J].[JobTitle]"
                                            Text="Requisition" ToolTip="Sort By Requisition" />
                                    </th>
                                    <th runat="server" id="thUser" style="width: 25%" enableviewstate="false">
                                        <asp:LinkButton runat="server" ID="btnUser" CommandName="Sort" CommandArgument="[M].[FirstName]"
                                            Text="User" ToolTip="Sort By User" />
                                    </th>                                   
                                    <th runat="server" id="thAction" style="width: 25%" enableviewstate="false">
                                        <asp:Label runat="server" ID="btnAction" CommandName="Sort" CommandArgument="[RASM].[STATUS]"
                                            Text="Status" ToolTip="Sort By Status" />
                                    </th>
                                    <th runat="server" id="thComment" style="width: 45%" enableviewstate="false">
                                        <asp:Label runat="server" ID="btnComment" CommandName="Sort" CommandArgument="[RAC].[COMMENT]"
                                            Text="Comments" ToolTip="Sort By COMMENT" />
                                    </th>
                                </tr>
                                <tr id="itemPlaceholder" runat="server">
                                </tr>
                                <tr class="Pager">
                                    <td id="tdpager" runat="server" colspan="6">
                                        <ucl:Pager ID="pagerControl" runat="server" EnableViewState="true" />
                                    </td>
                                </tr>
                            </table>
                        </LayoutTemplate>
                        <EmptyDataTemplate>
                            <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                                <tr>
                                    <td>
                                        No data was returned.
                                    </td>
                                </tr>
                            </table>
                        </EmptyDataTemplate>
                        <ItemTemplate>
                            <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                                <td runat="server" id="tdDate">
                                    <asp:Label ID="lblDate" runat="server"></asp:Label>
                                </td>
                                 <td runat="server" id="tdRequisition">
                                    <asp:Label runat="server" ID="lblJobtitle" />
                                </td>
                                <td runat="server" id="tdUser">
                                    <asp:Label runat="server" ID="lblUser" />
                                </td>
                                <td runat="server" id="tdStatus">
                                    <asp:Label runat="server" ID="lblStatus" />
                                </td>
                                <td runat="server" id="tdComment">
                                    <asp:Label runat="server" ID="lblComment" />
                                </td>
                                
                            </tr>
                        </ItemTemplate>
                    </asp:ListView>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlmodal" runat="server" Style="display: none">
                <asp:UpdatePanel runat="server" ID="UpdatePanel2" RenderMode="Inline" UpdateMode="Conditional">
                    <ContentTemplate>
                        <img src="../Images/AjaxLoading.gif" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </asp:Panel>
            <ajaxToolkit:ModalPopupExtender ID="Modal" runat="server" TargetControlID="pnlmodal"
                PopupControlID="pnlmodal" BackgroundCssClass="divModalBackground">
            </ajaxToolkit:ModalPopupExtender>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnExportToPDF" />
            <asp:PostBackTrigger ControlID="btnExportToExcel" />
            <asp:PostBackTrigger ControlID="btnExportToWord" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>

