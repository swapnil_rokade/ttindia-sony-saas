﻿<%---------------------------------------------------------------------------------------------------------------------------------------------
    FileName: CandidateJoiningreport1.aspx
    Description: This is the page used to display the candidate joining reports in excel,word,Pdf formats.
    Created By: Kanchan Yeware
    Created On: 09-Aug-2016
    Modification Log:     
--%>

<%@ Page Language="C#" MasterPageFile="~/Default.master" AutoEventWireup="true" CodeFile="CandidateJoiningreport1.aspx.cs"
    Inherits="TPS360.Web.UI.Reports.CandidateJoiningreport1" Title="Candidate joining Report - 1"
    EnableViewStateMac="false" %>

<%@ Register Src="~/Controls/DateRangePicker.ascx" TagName="DateRangePicker" TagPrefix="ucl" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Controls/PagerControl.ascx" TagName="Pager" TagPrefix="ucl" %>
<%@ Register Src="~/Controls/ConfirmationWindow.ascx" TagName="Confirm" TagPrefix="ucl" %>
<%@ Register Src="~/Controls/CountryStatePicker.ascx" TagName="CountryState" TagPrefix="ucl" %>
<asp:Content ID="Content2" ContentPlaceHolderID="cphHomeMasterTitle" runat="Server">
    Candidate Joining Report - 1
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphHomeMaster" runat="Server">

    <script type="text/javascript" language="javascript" src="../js/AjaxVariables.js"></script>

    <script type="text/javascript" language="javascript" src="../js/AjaxScript.js"></script>

   <script language="javascript" type="text/javascript">
       //To Keep the scroll bar in the Exact Place
       Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(PageLoadedReq);
       function PageLoadedReq(sender, args) {
           try {
               var hdnScroll = document.getElementById('ctl00_cphHomeMaster_hdnScrollPos');
               var bigDiv = document.getElementById('bigDiv');
               bigDiv.scrollLeft = hdnScroll.value;
           }
           catch (e) {
           }
       }
       function SetScrollPosition() {
           var hdnScroll = document.getElementById('ctl00_cphHomeMaster_hdnScrollPos');
           var bigDiv = document.getElementById('bigDiv');
           hdnScroll.value = bigDiv.scrollLeft;
       }



       function SelectUnselectColumnOption(chkColumnOption) {
           var chkBoxAll = $get('<%= chkColumns.ClientID%>');
           var chkBoxList = $get('<%= chkColumnList.ClientID%>');
           var chkBoxCount = chkBoxList.getElementsByTagName("input");
           if (chkBoxAll.checked) {
               for (var i = 0; i < chkBoxCount.length; i++) {
                   chkBoxCount[i].checked = true;
               }
           }
           if (!chkBoxAll.checked) {
               for (var i = 0; i < chkBoxCount.length; i++) {
                   chkBoxCount[i].checked = false;
               }
           }
       }
       function SelectUnselectAllColumnOption(chkColumnOptionHeader) {
           var checkedCount = 0;
           //debugger;
           var chkBoxAll = $get('<%= chkColumns.ClientID%>');
           var chkBoxList = $get('<%= chkColumnList.ClientID%>');
           var chkBoxCount = chkBoxList.getElementsByTagName("input");
           for (var i = 0; i < chkBoxCount.length; i++) {
               if (chkBoxCount[i].checked) {
                   checkedCount++;
               }
           }
           if (checkedCount == chkBoxCount.length) {
               chkBoxAll.checked = true;
           }
           else {
               chkBoxAll.checked = false;
           }
       }
    </script>
    <style>
        .TableFormLeble
        {
            width: 25%;
        }
    </style>
    <ucl:Confirm ID="uclConfirm" runat="server" />
    <asp:UpdatePanel ID="pnlCandidatereports" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:HiddenField ID="hdnScrollPos" runat="server" />
            <asp:TextBox ID="txtSortColumn" runat="server" Visible="false"></asp:TextBox>
            <asp:TextBox ID="txtSortOrder" runat="server" Visible="false"></asp:TextBox>
            <asp:TextBox ID="SortColumn" runat="server" EnableViewState="true" Visible="false" />
            <asp:TextBox ID="SortOrder" runat="server" EnableViewState="true" Visible="false" />
            <asp:ObjectDataSource ID="odsCandidateList" runat="server" SelectMethod="GetCandidateJoiningReport1"
                TypeName="TPS360.Web.UI.CandidateJoiningDataSource" SelectCountMethod="GetListCount"
                EnablePaging="True" SortParameterName="sortExpression">
                <SelectParameters>
                    <asp:ControlParameter ControlID="wdcCandidatesAdded" Name="addedFrom" PropertyName="StartDate" />
                    <asp:ControlParameter ControlID="wdcCandidatesAdded" Name="addedTo" PropertyName="EndDate" />
                    <asp:ControlParameter ControlID="txtCity" Name="SuperVisoryCode" PropertyName="Text" Type="String" />
                    <asp:Parameter Name="MemberId" />
                    <asp:Parameter Name="SortOrder" DefaultValue="asc" />
                    <asp:Parameter Name="sortExpression" />
                    <asp:Parameter Name="maximumRows" />
                    <asp:Parameter Name="startRowIndex" />
                </SelectParameters>
            </asp:ObjectDataSource>
            <asp:Panel ID="pnlSearchRegion" runat="server" DefaultButton="btnSearch">
                <div style="width: 100%;">
                    <div>
                        <div class="TableRow">
                            <div class="TabPanelHeader nomargin" style="border-style: none none none;">
                                Filter Options</div>
                        </div>
                        <div class="TableRow" style="text-align: center">
                            <asp:Label ID="lblMessage" runat="server" EnableViewState="false"></asp:Label>
                        </div>
                        <div class="FormLeftColumn" style="width: 49%">
                            <div class="TableRow" style="white-space: nowrap">
                                <div class="TableFormLeble" style="width: 40%;">
                                    <asp:Label EnableViewState="false" ID="lblCandidatesAdded" runat="server" Text="Date"></asp:Label>:
                                </div>
                                <div class="TableFormContent">
                                    <ucl:DateRangePicker ID="wdcCandidatesAdded" runat="server" />
                                </div>
                            </div>
                        </div>
                        <div class="TableRow" style="white-space: nowrap">
                            <div class="TableFormLeble" style="width: 19.5%;">                                
                               <asp:Label EnableViewState="false" ID="lblCity" runat="server" Text="Supervisory Organization Code"></asp:Label>:
                            </div>
                            <div class="TableFormContent">
                                <asp:TextBox EnableViewState="false" ID="txtCity" CssClass="SearchTextbox" TabIndex="3"
                                    runat="server"></asp:TextBox>
                            </div>
                        </div>
                        <ucl:CountryState ID="uclCountryState" runat="server" FirstOption="Any" CountryTabIndex="4"
                            StateTabIndex="5" />
                        <div style="text-align: left">
                            <asp:Label ID="lbMessage" runat="server" EnableViewState="false"></asp:Label>
                        </div>
                        <div class="TableRow well well-small nomargin">
                            <asp:CollapsiblePanelExtender ID="cpnlColumnSelection" runat="server" TargetControlID="pnlColumnSelectionBody"
                                ExpandControlID="pnlColumnSelectionHeader" CollapseControlID="pnlColumnSelectionHeader"
                                Collapsed="true" ImageControlID="imgShowHideColumnSelection" CollapsedImage="~/Images/expand-plus.png"
                                ExpandedImage="~/Images/collapse-minus.png" SuppressPostBack="true">
                            </asp:CollapsiblePanelExtender>
                            <asp:Panel ID="pnlColumnSelectionHeader" runat="server">
                                <div class="" style="clear: both; cursor: pointer">
                                    <asp:Image ID="imgShowHideColumnSelection" ImageUrl="~/Images/expand-plus.png" runat="server" />
                                    <asp:Label ID="lblDisplayColumns" runat="server" Text="Included Columns" EnableViewState="false"></asp:Label>
                                </div>
                            </asp:Panel>
                            <asp:Panel ID="pnlColumnSelectionBody" runat="server" Style="overflow: hidden;" Height="0">
                                <div class="TableRow" style="text-align: left; padding-top: 5px">
                                    <asp:CheckBox runat="server" Text="Select All" ID="chkColumns" CssClass="ColumnSelection"
                                        onclick="javascript:SelectUnselectColumnOption(this);" />
                                    <asp:CheckBoxList ID="chkColumnList" RepeatDirection="Horizontal" runat="server"
                                        CssClass="ColumnSelection" RepeatColumns="4" onclick="javascript:SelectUnselectAllColumnOption('chkColumns')">
                                        <%-- <asp:ListItem Value="Id" Selected="True">Candidate ID #</asp:ListItem>--%>
                                        <asp:ListItem Value="JobPostingCode" Selected="True">Requisition Code</asp:ListItem>
                                        <asp:ListItem Value="Name" Selected="True">Name</asp:ListItem>
                                        <asp:ListItem Value="SupervisoryOrgCode" Selected="True">Supervisory Organization Code</asp:ListItem>
                                        <asp:ListItem Value="SupervisoryOrgShortText">Supervisory Organization Short Text</asp:ListItem>
                                        <asp:ListItem Value="SupervisoryOrgDesc" Selected="True">Supervisory Organization Description</asp:ListItem>
                                        <asp:ListItem Value="ReasonForHire" Selected="True">Position Request Reason</asp:ListItem>
                                        <asp:ListItem Value="JobTitle" Selected="True">Job Posting Title</asp:ListItem>
                                        <asp:ListItem Value="NoOfOpenings" Selected="True">Number of Positions </asp:ListItem>
                                        <asp:ListItem Value="AvailabilityDate" Selected="True">Availability Date </asp:ListItem>
                                        <asp:ListItem Value="FinalHiredDate">Earliest Hire Date </asp:ListItem>
                                        <asp:ListItem Value="JobRestrictions" Selected="True">Job Restrictions</asp:ListItem>
                                        <asp:ListItem Value="JobFamily">Job Family </asp:ListItem>
                                        <asp:ListItem Value="JobProfiles">Job Profiles </asp:ListItem>
                                        <asp:ListItem Value="City">Location</asp:ListItem>
                                        <asp:ListItem Value="TimeType">Time Type </asp:ListItem>
                                        <asp:ListItem Value="WorkerType">Worker Type</asp:ListItem>
                                        <asp:ListItem Value="WorkerSubType">Worker Sub-Type</asp:ListItem>
                                        <asp:ListItem Value="Company" Selected="True">Company </asp:ListItem>
                                        <asp:ListItem Value="CostCenter" Selected="True">Cost Center </asp:ListItem>
                                        <asp:ListItem Value="Region" Selected="True">Region </asp:ListItem>
                                        <asp:ListItem Value="InternationalEmployeeStatus">International Employee Status</asp:ListItem>
                                        <asp:ListItem Value="PayGroup" Selected="True">Pay Group </asp:ListItem>
                                        <asp:ListItem Value="PayGroupGrade" Selected="True">Pay Group Grade </asp:ListItem>
                                        <asp:ListItem Value="MaximumFTE">Maximum FTE %</asp:ListItem>
                                        <asp:ListItem Value="Notes">Notes</asp:ListItem>
                                    </asp:CheckBoxList>
                                </div>
                            </asp:Panel>
                        </div>
                        <div class="TableRow" style="text-align: center; padding-top: 5px; padding-bottom: 5px;">
                            <asp:LinkButton ID="btnSearch" runat="server" Text="Search" AlternateText="Search"
                                TabIndex="12" CssClass="btn btn-primary" EnableViewState="false" OnClick="btnSearch_Click"
                                ValidationGroup="Search"><i class="icon-search icon-white"></i> Search</asp:LinkButton>
                            <asp:Button ID="btnClear" runat="server" Text="Clear" AlternateText="Clear" CssClass="btn"
                                EnableViewState="false" CausesValidation="false" TabIndex="13" OnClick="btnClear_Click" />
                        </div>
                        <div class="TableRow" style="padding-bottom: 5px;" id="divExportButton" runat="server"
                            visible="false">
                            <div class="TableRow">
                                <div class="TabPanelHeader nomargin" style="margin-bottom: 5px; border-style: none none none;">
                                    Report Results</div>
                            </div>
                            <asp:ImageButton ID="btnExportToExcel" CssClass="btn" TabIndex="14" SkinID="sknExportToExcel"
                                runat="server" ToolTip="Export To Excel" />
                            <asp:ImageButton ID="btnExportToPDF" CssClass="btn" TabIndex="15" SkinID="sknExportToPDF"
                                runat="server" ToolTip="Export To PDF" />
                            <asp:ImageButton ID="btnExportToWord" CssClass="btn" TabIndex="16" SkinID="sknExportToWord"
                                runat="server" ToolTip="Export To Word" />
                        </div>
                        <div id="divCandidateList" runat="server">
                            <asp:Panel ID="pnlCandidateListBody" runat="server">
                                <div class="GridContainer" style="overflow: auto; overflow-y: hidden; width: 100%;
                                    text-align: left;" id="bigDiv" onscroll='SetScrollPosition()'>
                                    <asp:ListView ID="lsvCandidateList" runat="server" DataKeyNames="ID" OnItemDataBound="lsvCandidateList_ItemDataBound"
                                        OnItemCommand="lsvCandidateList_ItemCommand" OnPreRender="lsvCandidateList_PreRender">
                                        <LayoutTemplate>
                                            <table id="tlbTemplate" class="Grid ReportGrid" cellspacing="0" border="0">
                                                <tr id="trHeader" runat="server">
                                                    <th runat="server" id="thJobPostingCode" style="min-width: 100px" enableviewstate="false">
                                                        Requisition Code
                                                    </th>
                                                    <th runat="server" id="thName" style="min-width: 100px">
                                                        <asp:LinkButton ID="lnkBtnName" runat="server" TabIndex="17" ToolTip="Sort By Name"
                                                            CommandName="Sort" CommandArgument="[C].[FirstName]" Text="Name" />
                                                    </th>
                                                    <th runat="server" id="thSupervisoryOrgCode" style="min-width: 100px" enableviewstate="false">
                                                        <asp:LinkButton ID="lnkSupervisoryOrgCode" runat="server" TabIndex="17" ToolTip="Sort By Supervisory Organization Code"
                                                            CommandName="Sort" CommandArgument="[C].[SupervisoryOrgCode]" Text="Supervisory Organization Code" />
                                                    </th>
                                                    <th runat="server" id="thSupervisoryOrgShortText" style="min-width: 100px" enableviewstate="false">
                                                        Supervisory Organization Short Text
                                                    </th>
                                                    <th runat="server" id="thSupervisoryOrgDesc" style="min-width: 100px" enableviewstate="false">
                                                        Supervisory Organization Description
                                                    </th>
                                                    <th runat="server" id="thReasonForHire" style="min-width: 100px" enableviewstate="false">
                                                        Position Request Reason
                                                    </th>
                                                    <th runat="server" id="thJobTitle" style="min-width: 100px" enableviewstate="false">
                                                        <asp:LinkButton ID="lnkJobTitle" runat="server" TabIndex="17" ToolTip="Sort By Job Posting Title"
                                                            CommandName="Sort" CommandArgument="[J].[JobTitle]" Text="Job Posting Title" />
                                                    </th>
                                                    <th runat="server" id="thNoOfOpenings" style="min-width: 100px" enableviewstate="false">
                                                        <asp:LinkButton ID="lnkNoOfOpenings" runat="server" TabIndex="17" ToolTip="Sort By Number Of Openings"
                                                            CommandName="Sort" CommandArgument="[J].[NoOfOpenings]" Text="Number Of Openings" />
                                                    </th>
                                                    <th runat="server" id="thAvailabilityDate" style="min-width: 100px" enableviewstate="false">
                                                        Availability Date
                                                    </th>
                                                    <th runat="server" id="thFinalHiredDate" style="min-width: 100px" enableviewstate="false">
                                                        Earliest Hire Date
                                                    </th>
                                                    <th runat="server" id="thJobRestrictions" style="min-width: 100px" enableviewstate="false">
                                                        Job Restrictions
                                                    </th>
                                                    <th runat="server" id="thJobFamily" style="min-width: 100px" enableviewstate="false">
                                                        Job Family
                                                    </th>
                                                    <th runat="server" id="thJobProfiles" style="min-width: 100px" enableviewstate="false">
                                                        Job Profiles
                                                    </th>
                                                    <th runat="server" id="thCity" style="min-width: 100px" enableviewstate="false">
                                                        <asp:LinkButton ID="lnkJobLocation" runat="server" TabIndex="17" ToolTip="Sort By Location"
                                                            CommandName="Sort" CommandArgument="[J].[City]" Text="Location" />
                                                    </th>
                                                    <th runat="server" id="thTimeType" style="min-width: 100px" enableviewstate="false">
                                                        Time Type
                                                    </th>
                                                    <th runat="server" id="thWorkerType" style="min-width: 100px" enableviewstate="false">
                                                        <asp:LinkButton ID="lnkWorkerType" runat="server" TabIndex="17" ToolTip="Sort By Worker Type"
                                                            CommandName="Sort" CommandArgument="[GW].[Name]" Text="Worker Type" />
                                                    </th>
                                                    <th runat="server" id="thWorkerSubType" style="min-width: 100px" enableviewstate="false">
                                                        Worker Sub-Type
                                                    </th>
                                                    <th runat="server" id="thCompany" style="min-width: 100px" enableviewstate="false">
                                                        Company
                                                    </th>
                                                    <th runat="server" id="thCostCenter" style="min-width: 100px" enableviewstate="false">
                                                        Cost Center
                                                    </th>
                                                    <th runat="server" id="thRegion" style="min-width: 100px" enableviewstate="false">
                                                        Region
                                                    </th>
                                                    <th runat="server" id="thInternationalEmployeeStatus" style="min-width: 100px" enableviewstate="false">
                                                        International Employee Status
                                                    </th>
                                                    <th runat="server" id="thPayGroup" style="min-width: 100px" enableviewstate="false">
                                                        Pay Group
                                                    </th>
                                                    <th runat="server" id="thPayGroupGrade" style="min-width: 100px" enableviewstate="false">
                                                        Pay Group Grade
                                                    </th>
                                                    <th runat="server" id="thMaximumFTE" style="min-width: 100px" enableviewstate="false">
                                                        Maximum FTE %
                                                    </th>
                                                    <th runat="server" id="thNotes" style="min-width: 100px" enableviewstate="false">
                                                        <asp:Label runat="server" ID="lnkNotes" Text="Notes" ToolTip="Sort By Notes" EnableViewState="false"></asp:Label>
                                                    </th>
                                                </tr>
                                                <tr id="itemPlaceholder" runat="server">
                                                </tr>
                                                <tr class="Pager" >
                                                    <td id="tdpager" runat="server" colspan="14">
                                                        <ucl:Pager ID="pagerControl" runat="server" EnableViewState="true" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </LayoutTemplate>
                                        <EmptyDataTemplate>
                                            <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                                                <tr>
                                                    <td>
                                                        No data was returned.
                                                    </td>
                                                </tr>
                                            </table>
                                        </EmptyDataTemplate>
                                        <ItemTemplate>
                                            <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                                                <td runat="server" id="tdJobPostingCode" style="text-align: left;">
                                                    <asp:Label runat="server" ID="lblJobPostingCode"></asp:Label>
                                                </td>
                                                <td runat="server" id="tdName" style="text-align: left;">
                                                    <asp:Label ID="lblCandidateName" runat="server"></asp:Label>
                                                </td>
                                                <td runat="server" id="tdSupervisoryOrgCode">
                                                    <asp:Label runat="server" ID="lblSupervisoryOrgCode"></asp:Label>
                                                </td>
                                                <td runat="server" id="tdSupervisoryOrgShortText">
                                                    <asp:Label runat="server" ID="lblSupervisoryOrgShortText"></asp:Label>
                                                </td>
                                                <td runat="server" id="tdSupervisoryOrgDesc">
                                                    <asp:Label runat="server" ID="lblSupervisoryOrgDesc"></asp:Label>
                                                </td>
                                                <td runat="server" id="tdReasonForHire">
                                                    <asp:Label runat="server" ID="lblReasonForHire"></asp:Label>
                                                </td>
                                                <td runat="server" id="tdJobTitle">
                                                    <asp:Label runat="server" ID="lblJobTitle"></asp:Label>
                                                </td>
                                                <td runat="server" id="tdNoOfOpenings">
                                                    <asp:Label runat="server" ID="lblNoOfOpenings"></asp:Label>
                                                </td>
                                                <td runat="server" id="tdAvailabilityDate">
                                                    <asp:Label runat="server" ID="lblAvailabilityDate"></asp:Label>
                                                </td>
                                                <td runat="server" id="tdFinalHiredDate">
                                                    <asp:Label runat="server" ID="lblFinalHiredDate"></asp:Label>
                                                </td>
                                                <td runat="server" id="tdJobRestrictions">
                                                    <asp:Label runat="server" ID="lblJobRestrictions"></asp:Label>
                                                </td>
                                                <td runat="server" id="tdJobFamily">
                                                    <asp:Label runat="server" ID="lblJobFamily"></asp:Label>
                                                </td>
                                                <td runat="server" id="tdJobProfiles">
                                                    <asp:Label runat="server" ID="lblJobProfiles"></asp:Label>
                                                </td>
                                                <td runat="server" id="tdcity">
                                                    <asp:Label runat="server" ID="lblCity"></asp:Label>
                                                </td>
                                                <td runat="server" id="tdTimeType">
                                                    <asp:Label runat="server" ID="lblTimeType"></asp:Label>
                                                </td>
                                                <td runat="server" id="tdWorkerType">
                                                    <asp:Label runat="server" ID="lblWorkerType"></asp:Label>
                                                </td>
                                                <td runat="server" id="tdWorkerSubType">
                                                    <asp:Label runat="server" ID="lblWorkerSubType"></asp:Label>
                                                </td>
                                                <td runat="server" id="tdCompany">
                                                    <asp:Label runat="server" ID="lblCompany"></asp:Label>
                                                </td>
                                                <td runat="server" id="tdCostCenter">
                                                    <asp:Label runat="server" ID="lblCostCenter"></asp:Label>
                                                </td>
                                                <td runat="server" id="tdRegion">
                                                    <asp:Label runat="server" ID="lblRegion"></asp:Label>
                                                </td>
                                                <td runat="server" id="tdInternationalEmployeeStatus">
                                                    <asp:Label runat="server" ID="lblInternationalEmployeeStatus"></asp:Label>
                                                </td>
                                                <td runat="server" id="tdPayGroup">
                                                    <asp:Label runat="server" ID="lblPayGroup"></asp:Label>
                                                </td>
                                                <td runat="server" id="tdPayGroupGrade">
                                                    <asp:Label runat="server" ID="lblPayGroupGrade"></asp:Label>
                                                </td>
                                                <td runat="server" id="tdMaximumFTE">
                                                    <asp:Label runat="server" ID="lblMaximumFTE"></asp:Label>
                                                </td>
                                                <td runat="server" id="tdNotes">
                                                    <asp:Label runat="server" ID="lblNotes"></asp:Label>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:ListView>
                                </div>
                            </asp:Panel>
                        </div>
                    </div>
                </div>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnExportToPDF" />
            <asp:PostBackTrigger ControlID="btnExportToExcel" />
            <asp:PostBackTrigger ControlID="btnExportToWord" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
