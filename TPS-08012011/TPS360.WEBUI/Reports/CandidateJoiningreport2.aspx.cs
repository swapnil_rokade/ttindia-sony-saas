﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName:       CandidateJoiningreport2.aspx.cs
    Description:    This is the objects source file
    Created By:  Kanchan Yeware   
    Created On:  09-Aug-2016  
    Modification Log:
*/
#region Namespaces
using System;
using System.Linq;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using TPS360.Common.BusinessEntities;
using TPS360.Web.UI.Helper;
#endregion

#region CandidateJoiningreport2
namespace TPS360.Web.UI.Reports
{
    public partial class CandidateJoiningreport2 : BasePage
    {
        #region Members
        private bool _IsAccessToEmployee = true;
        private static string UrlForEmployee = string.Empty;
        private static int IdForSitemap = 0;

        string _cookiename;
        string CookieName
        {
            get { return _cookiename; }
            set { _cookiename = value; }
        }
        #endregion

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            CookieName = "CandidateReportUserColumnOption_" + base.CurrentMember.PrimaryEmail;
            CustomSiteMap CustomMap = new CustomSiteMap();
            CustomMap = Facade.GetAllCustomSiteMapByParentIdAndMemberPrivilege(358, CurrentMember.Id);
            if (CustomMap == null) _IsAccessToEmployee = false;
            else
            {
                IdForSitemap = CustomMap.Id;
                UrlForEmployee = "~/" + CustomMap.Url.ToString();
            }

            if (!IsPostBack)
            {
                lsvCandidateList.DataBind();
                divCandidateList.Visible = false;
                if (Request.Cookies[CookieName] != null)
                {
                    string ColumnOptions = "DOJ#Name#mailID#PhoneNumber#Address#ReasonForHire#JobFamily#SupervisoryOrgCode#SupervisoryOrgDesc#";
                    string[] Columns = ColumnOptions.Split('#');
                    foreach (ListItem item in chkColumnList.Items)
                    {
                        item.Selected = false;
                    }
                    foreach (string item in Columns)
                    {
                        if (chkColumnList.Items.FindByValue(item) != null) chkColumnList.Items.FindByValue(item).Selected = true;
                    }
                }
            }
            if (!IsPostBack)
            {

                btnSearch_Click(sender, e);
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            hdnScrollPos.Value = "0";
            string ColumnOption = "";
            foreach (ListItem item in chkColumnList.Items)
            {
                if (item.Selected == true) ColumnOption += item.Value + "#";

            }
            System.Web.HttpCookie aCookie = new System.Web.HttpCookie(CookieName);
            aCookie.Value = ColumnOption.ToString();
            Response.Cookies.Add(aCookie);
            lsvCandidateList.Items.Clear();
            SetDataSourceParameters();
            ASP.controls_pagercontrol_ascx PagerControls = (ASP.controls_pagercontrol_ascx)this.lsvCandidateList.FindControl("pagerControl");
            if (PagerControls != null)
            {
                DataPager pager = (DataPager)PagerControls.FindControl("pager");
                if (pager != null)
                {
                    if (pager.Page.IsPostBack)
                        pager.SetPageProperties(0, pager.MaximumRows, true);
                }
            }
            divExportButton.Visible = false;
            BindList();
            HideListViewHeaderItem();

            string pagesize = "";
            if (txtSortColumn.Text == "") txtSortColumn.Text = "lnkBtnName";
            if (txtSortOrder.Text == "") txtSortOrder.Text = "ASC";
            PlaceUpDownArrow();
            pagesize = (Request.Cookies["CandidateReportRowPerPage"] == null ? "" : Request.Cookies["CandidateReportRowPerPage"].Value); ;

            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvCandidateList.FindControl("pagerControl");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)PagerControl.FindControl("pager");
                if (pager != null)
                {
                    pager.PageSize = Convert.ToInt32(pagesize == "" ? "20" : pagesize);
                    if (pager.Page.IsPostBack)
                        pager.SetPageProperties(0, pager.MaximumRows, true);
                }
            }
            System.Web.UI.HtmlControls.HtmlTableCell tdpager = (System.Web.UI.HtmlControls.HtmlTableCell)lsvCandidateList.FindControl("tdpager");
            if (tdpager != null)
            {
                tdpager.ColSpan = chkColumnList.Items.Cast<ListItem>().Where(x => x.Selected).Count();
            }
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            Clear();
            divExportButton.Visible = false;
        }

        protected void lsvCandidateList_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            lsvCandidateList.Visible = true;
            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                CandidateJoiningReport candidatInfo = ((ListViewDataItem)e.Item).DataItem as CandidateJoiningReport;

                if (candidatInfo != null)
                {
                    divExportButton.Visible = false;
                    Label lblDOJ = (Label)e.Item.FindControl("lblDOJ");
                    Label lblDOC = (Label)e.Item.FindControl("lblDOC");
                    Label lblGID = (Label)e.Item.FindControl("lblGID");
                    Label lblPosition = (Label)e.Item.FindControl("lblPosition");
                    Label lblCandidateName = (Label)e.Item.FindControl("lblName");
                    Label lblFirstName = (Label)e.Item.FindControl("lblFirstName");
                    Label lblMiddleName = (Label)e.Item.FindControl("lblMiddleName");
                    Label lblLastName = (Label)e.Item.FindControl("lblLastName");
                    Label lblPrimaryMail = (Label)e.Item.FindControl("lblmailID");
                    Label lblDateOfBirth = (Label)e.Item.FindControl("lblDOB");
                    Label lblMaritalStatus = (Label)e.Item.FindControl("lblMaritalStatus");
                    Label lblGender = (Label)e.Item.FindControl("lblGender");
                    Label lblNationalIDType = (Label)e.Item.FindControl("lblNationalIDType");
                    Label lblNationalID = (Label)e.Item.FindControl("lblNationalID");
                    Label lblPhoneNumber = (Label)e.Item.FindControl("lblPhoneNumber");
                    Label lblPhoneDevice = (Label)e.Item.FindControl("lblPhoneDevice");
                    Label lblPhoneType = (Label)e.Item.FindControl("lblPhoneType");
                    Label lblAddress = (Label)e.Item.FindControl("lblAddress");
                    Label lblVendorName = (Label)e.Item.FindControl("lblVendorName");
                    Label lblCity = (Label)e.Item.FindControl("lblLocation");
                    Label lblReasonForHire = (Label)e.Item.FindControl("lblReasonForHire");
                    Label lblEmployeeType = (Label)e.Item.FindControl("lblEmployeeType");
                    Label lblSubtypeofCW = (Label)e.Item.FindControl("lblSubtypeofCW");
                    Label lblIndividualContributor = (Label)e.Item.FindControl("lblIndividualContributor");
                    Label lblJobFamily = (Label)e.Item.FindControl("lblJobFamily");
                    Label lblJobProfiles = (Label)e.Item.FindControl("lblJobProfiles");
                    Label lblJobProfileName = (Label)e.Item.FindControl("lblJobProfileName");
                    Label lblSupervisoryOrgCode = (Label)e.Item.FindControl("lblSupervisoryOrgCode");
                    Label lblSupervisoryOrgShortText = (Label)e.Item.FindControl("lblSupervisoryOrgShortText");
                    Label lblSupervisoryOrgDesc = (Label)e.Item.FindControl("lblSupervisoryOrgDesc");
                    Label lblCostCenter = (Label)e.Item.FindControl("lblCostCenter");
                    Label lblDesignation = (Label)e.Item.FindControl("lblDesignation");
                    Label lblGlobalGrade = (Label)e.Item.FindControl("lblGlobalGrade");
                    Label lblCurrency = (Label)e.Item.FindControl("lblCurrency");
                    Label lblFreequency = (Label)e.Item.FindControl("lblFrequency");
                    Label lblCompensationPlan = (Label)e.Item.FindControl("lblCompensationPlan");
                    Label lblOfferHead1 = (Label)e.Item.FindControl("lblOfferHead1");
                    Label lblOfferHead2 = (Label)e.Item.FindControl("lblOfferHead2");
                    Label lblOfferHead3 = (Label)e.Item.FindControl("lblOfferHead3");
                    Label lblOfferHead4 = (Label)e.Item.FindControl("lblOfferHead4");
                    Label lblOfferHead5 = (Label)e.Item.FindControl("lblOfferHead5");
                    Label lblOfferHead6 = (Label)e.Item.FindControl("lblOfferHead6");
                    Label lblOfferHead7 = (Label)e.Item.FindControl("lblOfferHead7");
                    Label lblProjectName = (Label)e.Item.FindControl("lblProjectName");
                    Label lblDivision = (Label)e.Item.FindControl("lblDivision");
                    Label lblReportMangGID = (Label)e.Item.FindControl("lblReportMangGID");
                    Label lblManagerName = (Label)e.Item.FindControl("lblManagerName");
                    Label lblOrgUnitHead = (Label)e.Item.FindControl("lblOrgUnitHead");

                    if (candidatInfo.DOJ != null && candidatInfo.DOJ != DateTime.MinValue)
                    {
                        lblDOJ.Text = candidatInfo.DOJ.ToShortDateString();
                    }
                    lblDOC.Text = candidatInfo.DOC;
                    lblGID.Text = candidatInfo.GID;
                    lblPosition.Text = candidatInfo.Position;

                    string strFullName = MiscUtil.GetFirstAndLastName(candidatInfo.FirstName, candidatInfo.LastName);
                    lblCandidateName.Text = strFullName;

                    lblFirstName.Text = candidatInfo.FirstName;
                    lblMiddleName.Text = candidatInfo.MiddleName;
                    lblLastName.Text = candidatInfo.LastName;
                    lblPrimaryMail.Text = candidatInfo.PrimaryEmail;

                    if (candidatInfo.DateOfBirth != null && candidatInfo.DateOfBirth != DateTime.MinValue)
                    {
                        lblDateOfBirth.Text = candidatInfo.DateOfBirth.ToShortDateString();
                    }
                    lblMaritalStatus.Text = candidatInfo.MaritalStatus;
                    lblGender.Text = candidatInfo.Gender;
                    lblNationalIDType.Text = candidatInfo.NationalIDType;
                    lblNationalID.Text = candidatInfo.NationalID;
                    lblPhoneNumber.Text = candidatInfo.PhoneNumber;
                    lblPhoneDevice.Text = candidatInfo.PhoneDevice;
                    lblPhoneType.Text = candidatInfo.PhoneType;
                    lblAddress.Text = candidatInfo.Address;
                    lblVendorName.Text = candidatInfo.VendorName;
                    lblCity.Text = candidatInfo.City;
                    lblReasonForHire.Text = candidatInfo.ReasonForHire;
                    lblEmployeeType.Text = candidatInfo.EmployeeType;
                    lblSubtypeofCW.Text = candidatInfo.SubtypeofCW;
                    lblIndividualContributor.Text = candidatInfo.IndividualContributor;
                    lblJobFamily.Text = candidatInfo.JobFamily;
                    lblJobProfiles.Text = candidatInfo.JobProfile;
                    lblJobProfileName.Text = candidatInfo.jobProfileName;
                    lblSupervisoryOrgCode.Text = Convert.ToString(candidatInfo.SupervisoryOrgCode);
                    lblSupervisoryOrgShortText.Text = candidatInfo.SupervisoryOrgShortText;
                    lblSupervisoryOrgDesc.Text = candidatInfo.SupervisoryOrgDesc;
                    lblCostCenter.Text = candidatInfo.CostCenter;
                    lblDesignation.Text = candidatInfo.Designation;
                    lblGlobalGrade.Text = candidatInfo.GlobalGrade;
                    lblCurrency.Text = candidatInfo.Currency;
                    lblFreequency.Text = candidatInfo.Frequency;
                    lblCompensationPlan.Text = candidatInfo.CompensationPlan;
                    lblOfferHead1.Text = Convert.ToString(candidatInfo.OfferHead1);
                    lblOfferHead2.Text = Convert.ToString(candidatInfo.OfferHead2);
                    lblOfferHead3.Text = Convert.ToString(candidatInfo.OfferHead3);
                    lblOfferHead4.Text = Convert.ToString(candidatInfo.OfferHead4);
                    lblOfferHead5.Text = Convert.ToString(candidatInfo.OfferHead5);
                    lblOfferHead6.Text = Convert.ToString(candidatInfo.OfferHead6);
                    lblOfferHead7.Text = Convert.ToString(candidatInfo.OfferHead7);
                    lblProjectName.Text = candidatInfo.ProjectName;
                    lblDivision.Text = candidatInfo.Division;
                    lblReportMangGID.Text = candidatInfo.ReportMangGID;
                    lblManagerName.Text = candidatInfo.ManagerName;
                    lblOrgUnitHead.Text = candidatInfo.OrgUnitHead;

                    HideListViewItem(e.Item);
                }
            }
        }

        protected void lsvCandidateList_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Sort")
                {
                    LinkButton lnkbutton = (LinkButton)e.CommandSource;
                    if (txtSortColumn.Text == lnkbutton.ID)
                    {
                        if (txtSortOrder.Text == "ASC") txtSortOrder.Text = "DESC";
                        else txtSortOrder.Text = "ASC";
                    }
                    else
                    {
                        txtSortColumn.Text = lnkbutton.ID;
                        txtSortOrder.Text = "ASC";
                    }
                    if (SortColumn.Text == string.Empty || SortColumn.Text != e.CommandArgument.ToString())
                        SortOrder.Text = "asc";
                    else SortOrder.Text = SortOrder.Text == "asc" ? "desc" : "asc";
                    SortColumn.Text = e.CommandArgument.ToString();
                    odsCandidateList.SelectParameters["SortOrder"].DefaultValue = SortOrder.Text.ToString();
                }
            }
            catch { }
        }

        protected void lsvCandidateList_PreRender(object sender, EventArgs e)
        {
            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvCandidateList.FindControl("pagerControl");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)PagerControl.FindControl("pager");
                if (pager != null)
                {
                    DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                    if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
                }
                HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
                if (hdnRowPerPageName != null) hdnRowPerPageName.Value = "CandidateReportRowPerPage";
            }

            PlaceUpDownArrow();
            HideListViewHeaderItem();
            if (IsPostBack)
            {
                if (lsvCandidateList.Items.Count == 0)
                {
                    lsvCandidateList.DataSource = null;
                    lsvCandidateList.DataBind();
                }
            }
        }
        #endregion

        #region Methods
        private void SetDataSourceParameters()
        {
            if (SortOrder.Text == string.Empty && SortColumn.Text == string.Empty)
            {
                SortOrder.Text = "asc";
                SortColumn.Text = "[C].[FirstName]";
            }

            odsCandidateList.SelectParameters["MemberId"].DefaultValue = Convert.ToString(CurrentMember.Id);
            odsCandidateList.SelectParameters["SortOrder"].DefaultValue = SortOrder.Text.ToString();
            odsCandidateList.SelectParameters["sortExpression"].DefaultValue = SortOrder.Text.ToString();
            odsCandidateList.SelectParameters["maximumRows"].DefaultValue = "20";
            odsCandidateList.SelectParameters["startRowIndex"].DefaultValue = "0";
        }

        private void BindList()
        {
            bool chkColumnListcount = false;
            foreach (ListItem items in chkColumnList.Items)
            {
                if (items.Selected)
                {
                    chkColumnListcount = true;
                    break;
                }
            }
            if (chkColumnListcount)
            {
                this.lsvCandidateList.DataSourceID = "odsCandidateList";
                this.lsvCandidateList.DataBind();
                divCandidateList.Visible = true;

            }
            else
            {
                divCandidateList.Visible = false;
                MiscUtil.ShowMessage(lblMessage, "Please Select atleast One column", false);
            }
        }

        private void Clear()
        {
            divCandidateList.Visible = false;
            ClearSearchCriteria();
        }

        private void ClearSearchCriteria()
        {
            lblMessage.Text = string.Empty;
            wdcCandidatesAdded.ClearRange();
            txtSuperVisoryCode.Text = "";
        }

        private void HideListViewHeaderItem()
        {
            foreach (ListItem chkItem in chkColumnList.Items)
            {
                HtmlTableCell thHeaderTitle = (HtmlTableCell)lsvCandidateList.FindControl("th" + chkItem.Value);
                if (thHeaderTitle != null)
                {
                    if (chkItem.Selected)
                    {
                        thHeaderTitle.Visible = true;

                    }
                    else
                    {
                        thHeaderTitle.Visible = false;
                    }
                }
            }
        }

        private void PlaceUpDownArrow()
        {
            try
            {
                LinkButton lnk = (LinkButton)lsvCandidateList.FindControl(txtSortColumn.Text);
                HtmlTableCell im = (HtmlTableCell)lnk.Parent;
                im.EnableViewState = false;
                im.Attributes.Add("class", (txtSortOrder.Text == "ASC" ? "Ascending" : "Descending"));
            }
            catch { }
        }

        private void HideListViewItem(ListViewItem itm)
        {
            foreach (ListItem chkItem in chkColumnList.Items)
            {
                HtmlTableCell thHeaderTitle = (HtmlTableCell)itm.FindControl("td" + chkItem.Value);
                if (thHeaderTitle != null)
                {
                    if (chkItem.Selected)
                    {
                        thHeaderTitle.Visible = true;
                    }
                    else
                    {
                        thHeaderTitle.Visible = false;
                    }
                }
            }
        }
        #endregion
    }
}
#endregion
