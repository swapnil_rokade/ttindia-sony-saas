﻿<%@ Page EnableEventValidation="false" Language="C#" MasterPageFile="~/Default.master"
    AutoEventWireup="true" CodeFile="VendorSubmissionReport.aspx.cs" Inherits="TPS360.Web.UI.Reports.VendorSubmissionReport"
    Title="Vendor Submission Report" EnableViewStateMac="false" %>

<%@ Register Src="~/Controls/DateRangePicker.ascx" TagName="DateRangePicker" TagPrefix="ucl" %>
<%@ Register Src="~/Controls/PagerControl.ascx" TagName="Pager" TagPrefix="ucl" %>
<asp:Content ID="Content2" ContentPlaceHolderID="cphHomeMasterTitle" runat="Server">
    Vendor Submission Report
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphHomeMaster" runat="Server">
  <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript" language="javascript" src="../js/AjaxVariables.js"></script>
    <script type="text/javascript" language="javascript" src="../js/AjaxScript.js"></script>
<script >

var XmlHttp;
var controlid;
function GetReportData()
{
$('#visualization').html('<div style=\' backgound-color:red; margin-top:35px; height : 400px; width:100%; vertical-align:center; text-align:center;\'><img src=../Images/Ajax-loader.gif style=\' margin-top:180px; \'/></div>');
    var ddlRequisition=document .getElementById ('<%= ddlRequisition.ClientID %>');
    var ddlVendor=document .getElementById ('<%= ddlVendor.ClientID %>');
        var hdnSelectedContactID=document .getElementById ('<%= hdnSelectedContactID.ClientID %>');
        
    var hdnStartDate=document .getElementById ('<%= hdnStartDate.ClientID %>');
    var hdnEndDate=document .getElementById ('<%= hdnEndDate.ClientID %>');
    var requestUrl =  "../AJAXServers/GraphicalReport.aspx?RType=VS&JID=" + ddlRequisition .options[ddlRequisition .selectedIndex].value  + "&CCID=" + hdnSelectedContactID.value  + "&VID=" + ddlVendor .options[ddlVendor .selectedIndex].value + "&SD=" + hdnStartDate .value + "&ED=" + hdnEndDate .value ;
    var modal= document.getElementById(controlid);
    CreateXmlHttp();
	if(XmlHttp)
	{
	try{
		XmlHttp.onreadystatechange = HandleResponseExpanded;
		XmlHttp.open("GET", requestUrl,  true);
		XmlHttp.send(null);		
		}
		catch(e)
		{
		    
		}
	}
}

function CreateXmlHttp()
{
	try
	{
		XmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
	}
	catch(e)
	{
		try
		{
			XmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
		} 
		catch(oc)
		{
			XmlHttp = null;
		}
	}
	if(!XmlHttp && typeof XMLHttpRequest != "undefined") 
	{
		XmlHttp = new XMLHttpRequest();
	}
}

function HandleResponseExpanded()
{
	if(XmlHttp.readyState == 4)
	{
	try{
		if(XmlHttp.status == 200)
		{			
			var t1 = XmlHttp.responseText;
			drawVisualization(t1);
		}
		else
		{
			alert("There was a problem retrieving data from the server." );
		}
		}
		catch(e)
		{
		    alert(e.Message);
		}
	}
}



google.load('visualization', '1', {packages: ['corechart']});
//google.setOnLoadCallback(GetReportData);

Sys.Application.add_load(function() { 

if($('#<%=hdnIsPostback.ClientID %>').val()=="0")
{
    $('#<%=hdnIsPostback.ClientID %>').val("1");
    __doPostBack('<%= lblVendor.ClientID %>','');
 
 }
 
 
 $('#visualization').css({opacity: 0});
GetReportData ();
});
function drawVisualization(t1) {


try{
   xmlDoc = $.parseXML( t1 );
    var $xml = $( xmlDoc );
    var noofrows=0;    
    $xml.find( "Row" ).each(function (){noofrows =noofrows +1;});

    var data2 = new google.visualization.DataTable();
    
    data2.addColumn({id: 'date', label: 'Date', type: 'date'});
    data2.addColumn( {id: 'numReferrals', label: '# of Referrals', type: 'number'});
    data2 .addRows(noofrows);
    var rowno=0;
    $xml.find( "Row" ).each(function (){
        data2 .setCell(rowno ,0 ,new Date ($(this).find('Year').text(),$(this).find('Month').text()-1,$(this).find('Day').text()) );
        data2 .setCell(rowno ,1 , parseInt ($(this).find('Count').text()) );
        rowno =rowno +1;
        });
        }
        catch (e)
        {
            alert (e.message);
        }
  
var formatter = new google.visualization.NumberFormat({fractionDigits: 0});
formatter.format(data2, 1);   
  
  // Create and draw the visualization.
  var ac = new google.visualization.AreaChart(document.getElementById('visualization'));
  ac.draw(data2, {
    title : 'Vendor Portals Submission By Day',
    isStacked: false,
    width: 800,
    height: 450,
    vAxis: {},
    hAxis: {
      format: 'MMM d'
    },
    backgroundColor: 'transparent',
    titleTextStyle: {
                color: '#111',
                fontSize: 16
            },
    colors: ['#06C'],
     areaOpacity: 0.1,
            lineWidth: 2,
            pointSize: 6,
            legend: {
                position: 'none'
            },
  });
  
  $('#visualization').animate({opacity: 1},500)
}

</script>

    <link href="../Style/TabStyle.css" rel="Stylesheet" type="text/css" />
    <asp:HiddenField ID="hdnScrollPos" runat="server" />
<asp:HiddenField ID="hdnIsPostback" runat ="server" Value ="0" />
    <asp:TextBox ID="SortColumn" runat="server" EnableViewState="true" Visible="false" />
    <asp:TextBox ID="SortOrder" runat="server" EnableViewState="true" Visible="false" />
    <div class="well" style="clear: both; padding-top: 0px;">
        <div class="TabPanelHeader">
            Filter Options</div>
        <div style="text-align: center">
        <asp:UpdatePanel ID="upFilter" runat ="server" >
        <ContentTemplate >
        
            <asp:HiddenField ID="hdnStartDate" runat ="server" />
    <asp:HiddenField ID ="hdnEndDate" runat ="server" />
    
            <ucl:DateRangePicker ID="dtSubmissionDate" runat="server" />
            <asp:Label ID="lblVendor" runat="server" Text="Vendor:" ></asp:Label>
            <asp:DropDownList ID="ddlVendor" runat="server" CssClass ="CommonDropDownList" Width="110px" >
            </asp:DropDownList>
            
            <asp:Label ID="lblVendorContact" runat="server" Text="Vendor Contact:" ></asp:Label>
            <asp:DropDownList ID="ddlVendorContact" runat="server" CssClass ="CommonDropDownList" Width="110px" >
            </asp:DropDownList>
               <asp:HiddenField ID="hdnSelectedContactID" runat="server" Value ="0" />
            
            <asp:Label ID="lblRequisition" runat="server" Text="Requisition:"></asp:Label>
            <asp:DropDownList ID="ddlRequisition" runat="server"  CssClass ="CommonDropDownList" Width="110px">
            </asp:DropDownList>
            <asp:Button ID="btnApply" runat="server" Text="Apply" CssClass="btn btn-primary" OnClick ="btnApply_Click" />
            <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btn" OnClick ="btnReset_Click" />
            </ContentTemplate>
        </asp:UpdatePanel>
        </div>
    </div>
    
    <div class="tabbable">
        <!-- Only required for left/right tabs -->
        <ul class="nav nav-tabs">
            <li class="active"><a href="#tab1" data-toggle="tab">Summary</a></li>
            <li><a href="#tab2" data-toggle="tab">Details</a></li>
        </ul>
        
        
    
        <div class="tab-content">
            <div class="tab-pane active" id="tab1">
            <div id="visualization" style =" margin : 0 auto; margin-top : -35px; margin-bottom : -35px; min-height : 400px; margin-left : 100px;"></div>
                    <div class="FormLeftColumn">
                    <div class="TabPanelHeader">
            By Vendor Contact</div>
                         <asp:UpdatePanel ID="upSummaryByReferrer" runat ="server" >
                <ContentTemplate >
                    <asp:TextBox ID="txtByRefSortColumn" runat="server" Visible="false"></asp:TextBox>
                    <asp:TextBox ID="txtByRefSortOrder" runat="server" Visible="false"></asp:TextBox>
                <asp:ObjectDataSource ID="odsByRef" runat="server" SelectMethod="GetPaged_ForSummary"
                    TypeName="TPS360.Web.UI.CompanyConatctDataSource" SelectCountMethod="GetListCount_ForSummary"
                    EnablePaging="True" SortParameterName="sortExpression">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="dtSubmissionDate" Name="StartDate" PropertyName="StartDate"
                            Type="DateTime" />
                        <asp:ControlParameter ControlID="dtSubmissionDate" Name="EndDate" PropertyName="EndDate"
                            Type="DateTime" />
                        <asp:ControlParameter ControlID="ddlRequisition" Name="JobPostingId" PropertyName="SelectedValue"
                            Type="String" />
                        <asp:ControlParameter ControlID="ddlVendor" Name="VendorId" PropertyName="SelectedValue"
                            Type="String" />
                            <asp:ControlParameter ControlID="hdnSelectedContactID" Name="ContactId" PropertyName="Value" Type="String"
                             />
                        <asp:Parameter Name="Type" DefaultValue ="" />
                        
                    </SelectParameters>
                </asp:ObjectDataSource>
               
                 <asp:ListView ID="lsvByCompanyContact" runat="server" DataKeyNames="Id" DataSourceID ="odsByRef" OnItemDataBound="lsvByCompanyContact_ItemDataBound"
                        OnItemCommand="lsvByCompanyContact_ItemCommand" OnPreRender="lsvByCompanyContact_PreRender">
                        <LayoutTemplate>
                            <table id="tlbTemplate" runat="server" class="Grid" cellspacing="0" border="0">
                                <tr>
                                    <th>
                                        <asp:LinkButton runat="server" ID="btnName" CommandName="Sort" CommandArgument="[C].[CompanyName]"
                                            Text="Vendor" ToolTip="Sort By Vendor" />
                                    </th>
                                    <th>
                                        <asp:LinkButton runat="server" ID="lnbContactName" CommandName="Sort" CommandArgument="[M].[FirstName]"
                                            Text="Contact Name" ToolTip="Sort By Contact Name" />
                                    </th>
                                                                       
                                    
                                    <th style =" width : 100px !important">
                                        <asp:LinkButton runat="server" ID="btnReferralsCount" CommandName="Sort" CommandArgument="[VS].[Count]"
                                            Text="# of Referrals" ToolTip="Sort By # of Referrals" />
                                    </th>
                                                                    
                                   
                                </tr>
                                <tr id="itemPlaceholder" runat="server">
                                </tr>
                                <tr class="Pager">
                                    <td id="tdpager" runat="server" colspan="3">
                                        <ucl:Pager ID="pagerControl" runat="server" EnableViewState="true" />
                                    </td>
                                </tr>
                            </table>
                        </LayoutTemplate>
                        <EmptyDataTemplate>
                            <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                                <tr>
                                    <td>
                                        No data was returned.
                                    </td>
                                </tr>
                            </table>
                        </EmptyDataTemplate>
                        <ItemTemplate>
                            <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                                <td>
                                    <asp:Label ID="lblVendorName" runat="server"></asp:Label>
                                </td>
                                <td>
                                <asp:Label ID="lblcontactname" runat="server" />
                                </td>
                                
                                <td>
                                    <asp:Label runat="server" ID="lblcount" />
                                </td>
                                
                                
                             
                            </tr>
                        </ItemTemplate>
                    </asp:ListView>
                  
                  </ContentTemplate>
                </asp:UpdatePanel>
                    </div> 
                      <div class="FormRightColumn">
                      <div class="TabPanelHeader">
            By Requisition</div>
                         <asp:UpdatePanel ID="upByReq" runat ="server" >
                <ContentTemplate >
                    <asp:TextBox ID="txtByReqSortColumn" runat="server" Visible="false"></asp:TextBox>
                    <asp:TextBox ID="txtByReqSortOrder" runat="server" Visible="false"></asp:TextBox>
                <asp:ObjectDataSource ID="odsByReq" runat="server" SelectMethod="GetPaged_ForSummary"
                    TypeName="TPS360.Web.UI.CompanyConatctDataSource" SelectCountMethod="GetListCount_ForSummary"
                    EnablePaging="True" SortParameterName="sortExpression">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="dtSubmissionDate" Name="StartDate" PropertyName="StartDate"
                            Type="DateTime" />
                        <asp:ControlParameter ControlID="dtSubmissionDate" Name="EndDate" PropertyName="EndDate"
                            Type="DateTime" />
                        <asp:ControlParameter ControlID="ddlRequisition" Name="JobPostingId" PropertyName="SelectedValue"
                            Type="String" />
                        <asp:ControlParameter ControlID="ddlVendor" Name="VendorId" PropertyName="SelectedValue"
                            Type="String" />
                             <asp:ControlParameter ControlID="ddlVendorContact" Name="ContactId" PropertyName="SelectedValue"
                            Type="String" />
                        <asp:Parameter Name="Type" DefaultValue ="ByRequisition" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                
                 <asp:ListView ID="lsvByRequisition" runat="server" DataKeyNames="Id" DataSourceID ="odsByReq" OnItemDataBound="lsvByRequisition_ItemDataBound"
                        OnItemCommand="lsvByRequisition_ItemCommand" OnPreRender="lsvByRequisition_PreRender">
                        <LayoutTemplate>
                            <table id="tlbTemplate" runat="server" class="Grid" cellspacing="0" border="0">
                                <tr>
                                    <th>
                                        <asp:LinkButton runat="server" ID="btnName" CommandName="Sort" CommandArgument="[J].[JobTitle]"
                                            Text="Job Title" ToolTip="Sort By Name" />
                                    </th>
                                    <th style =" width : 100px !important">
                                        <asp:LinkButton runat="server" ID="btnReferralsCount" CommandName="Sort" CommandArgument="[VS].[Count]"
                                            Text="# of Referrals" ToolTip="Sort By # of Referrals" />
                                    </th>
                                                                      
                                </tr>
                                <tr id="itemPlaceholder" runat="server">
                                </tr>
                                <tr class="Pager">
                                    <td id="tdpager" runat="server" colspan="2">
                                        <ucl:Pager ID="pagerControl" runat="server" EnableViewState="true" />
                                    </td>
                                </tr>
                            </table>
                        </LayoutTemplate>
                        <EmptyDataTemplate>
                            <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                                <tr>
                                    <td>
                                        No data was returned.
                                    </td>
                                </tr>
                            </table>
                        </EmptyDataTemplate>
                        <ItemTemplate>
                            <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                                <td>
                                   <asp:HyperLink ID="lnkJobTitle" runat ="server" ></asp:HyperLink>
                                </td>
                                <td>
                                    <asp:Label runat="server" ID="lblcount" />
                                </td>
                                                           </tr>
                        </ItemTemplate>
                    </asp:ListView>
                  
                  </ContentTemplate>
                </asp:UpdatePanel>
                    </div> 
                
                
            </div>
            <div class="tab-pane" id="tab2" style="width: 100%; overflow: auto;">
            
                <asp:UpdatePanel ID="upDetail" runat ="server" >
                <ContentTemplate >
                <asp:TextBox ID="txtSortColumn" runat="server" Visible="false"></asp:TextBox>
    <asp:TextBox ID="txtSortOrder" runat="server" Visible="false"></asp:TextBox>
                  <asp:ObjectDataSource ID="odsVendorSubmissions" runat="server" SelectMethod="GetPaged_ForVendorSubmissionDetailForReport"
                TypeName="TPS360.Web.UI.CompanyConatctDataSource" SelectCountMethod="GetListCount_ForVendorSubmissionDetail"
                EnablePaging="True" SortParameterName="sortExpression" >
                <SelectParameters>
                    <asp:ControlParameter ControlID="dtSubmissionDate" Name="StartDate" PropertyName="StartDate"
                            Type="DateTime" />
                        <asp:ControlParameter ControlID="dtSubmissionDate" Name="EndDate" PropertyName="EndDate"
                            Type="DateTime" />
                        <asp:ControlParameter ControlID="ddlRequisition" Name="JobPostingId" PropertyName="SelectedValue"
                            Type="String" />
                        <asp:ControlParameter ControlID="ddlVendor" Name="VendorId" PropertyName="SelectedValue"
                            Type="String" />
                             <asp:ControlParameter ControlID="hdnSelectedContactID" Name="ContactId" PropertyName="Value"
                            Type="String" />
                        <asp:Parameter Name="Type" DefaultValue ="" />
                        <asp:Parameter Name ="CandidateId" DefaultValue ="0" />
                </SelectParameters>
            </asp:ObjectDataSource>
            
             <div class="TableRow" style="padding-bottom: 5px" id="divExportButtons" runat="server"
                            visible="true">
                            <div class="TabPanelHeader nomargin" style="margin-bottom: 5px;border-style: none none none;">Report Results</div>
                             <asp:ImageButton ID="btnExportToExcel" CssClass ="btn"  SkinID ="sknExportToExcel" runat ="server" ToolTip ="Export To Excel" OnClick ="btnExportToExcel_Click" />
                            <asp:ImageButton ID="btnExportToPDF" CssClass ="btn"  SkinID ="sknExportToPDF" runat ="server" ToolTip ="Export To PDF" OnClick ="btnExportToPDF_Click" />
                            <asp:ImageButton ID="btnExportToWord" CssClass ="btn"  SkinID ="sknExportToWord" runat ="server" ToolTip ="Export To Word" OnClick ="btnExportToWord_Click" />
                            
                            
                        </div>
            <asp:ListView ID="lsvVendorSubmissions" runat="server" DataKeyNames="Id" DataSourceID="odsVendorSubmissions"
                OnItemDataBound="lsvVendorSubmissions_ItemDataBound" EnableViewState="true" OnPreRender="lsvVendorSubmissions_PreRender"
                OnItemCommand="lsvVendorSubmissions_ItemCommand">
                <LayoutTemplate>
                    <table id="tlbTemplate" runat="server" class="Grid" cellspacing="0" border="0">
                        <tr>
                            <th>
                                <asp:LinkButton ID="lnkDateSubmitted" runat="server" ToolTip="Sort By Date Submitted" CommandName="Sort"
                                    CommandArgument="[MJC].[CreateDate]" Text="Date Submitted" />
                            </th>
                            <th>
                                <asp:LinkButton ID="lnkId" runat="server" ToolTip="Sort By Candidate ID #" CommandName="Sort"
                                    CommandArgument="[C].[ID]" Text="Candidate ID #" />
                            </th>
                            <th>
                                <asp:LinkButton ID="lnkCandidate" runat="server" ToolTip="Sort By Candidate Name" CommandName="Sort"
                                    CommandArgument="[C].[CandidateName]" Text="Candidate Name" />
                            </th>
                            <th>
                                <asp:LinkButton ID="lnkVendor" runat="server" ToolTip="Sort By Vendor"
                                    CommandName="Sort" CommandArgument="[Com].CompanyName" Text="Vendor" />
                            </th>
                            <th>
                                <asp:LinkButton ID="lnkSubmittedBy" runat="server" ToolTip="Sort By Submitted By" CommandName="Sort"
                                    CommandArgument="[M].[FirstName]" Text="Submitted By" />
                            </th>
                            <th>
                                <asp:LinkButton ID="lnkRequisition" runat="server" ToolTip="Sort By Job Title" CommandName="Sort"
                                    CommandArgument="[JP].JobTitle" Text="Job Title" />
                            </th>
                            <th>
                            <asp:LinkButton ID="lnkCurrentStatus" runat="server" ToolTip="Sort By Current Status" CommandName="Sort" CommandArgument="[DS].[HiringStatus]" Text="Current Status" />
                          
                            </th>
                        </tr>
                        <tr id="itemPlaceholder" runat="server">
                        </tr>
                        <tr class="Pager">
                            <td colspan="7" id="tdPager" runat="server">
                                <ucl:Pager ID="pagerControl" runat="server" />
                            </td>
                        </tr>
                    </table>
                </LayoutTemplate>
                <EmptyDataTemplate>
                    <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server"
                        style="width: 100%; margin: 0px 0px;">
                        <tr>
                            <td>
                                No Vendor Submissions.
                            </td>
                        </tr>
                    </table>
                </EmptyDataTemplate>
                <ItemTemplate>
                    <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                        <td>
                            <asp:Label ID="lblDateTime" runat="server"  />
                        </td>
                           <td>
                           <asp:Label ID="lblCandidateID" runat="server"  />
                        </td>
                        <td>
                           <asp:HyperLink ID="hlnkCandidateName" runat="server"  ></asp:HyperLink>
                        </td>
                        <td>
                            <asp:HyperLink ID="hnlkVendor" runat="server"   Enabled ="false"  ></asp:HyperLink>
                        </td>
                        <td>
                            <asp:HyperLink ID="hlnkContact" runat="server"  ></asp:HyperLink>
                        </td>
                        <td>
                           <asp:HyperLink ID="lnkJobTitle" runat="server"  ></asp:HyperLink>
                        </td>
                        <td>
                        <asp:Label ID="hlnkCurrentStatus" runat="server" ></asp:Label>
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:ListView>

                  
                  </ContentTemplate>
                  <Triggers>
            <asp:PostBackTrigger ControlID="btnExportToPDF" />
            <asp:PostBackTrigger ControlID="btnExportToExcel" />
            <asp:PostBackTrigger ControlID="btnExportToWord" />
        </Triggers>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
</asp:Content>
