﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: InterviewReports.aspx.cs
    Description: This is the InterviewReports
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.           Date               Author             Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1              12/May/2016         pravin khot        added new field-  InterviewFeedback.
------------------------------------------------------------------------------------------------------------------------------------------- 
*/


using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using TPS360.Common.Helper;
using System.Text;
using TPS360.Web.UI.Helper;
using TPS360.Common.BusinessEntities;
using ExpertPdf.HtmlToPdf;
using System.Collections.Generic;
using System.Drawing;
using TPS360.Common.Shared;
namespace TPS360.Web.UI.Reports
{
    public partial class InterviewReports : BasePage
    {
        #region Variables
        private bool isAccess = false;
        private static string UrlForCandidate = string.Empty;
        private static int SitemapIdForCandidate = 0;
        bool _IsAccessToCandidate = true;
        #endregion
        #region Properties
        int CandidateId
        {
            get
            {
                if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]))
                {
                    return Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]);

                }
                else
                {
                    return 0;
                }

            }
        }
        string _cookiename;
        string CookieName
        {
            get { return _cookiename; }
            set { _cookiename = value; }
        }
        #endregion
        #region Methods
        private void PlaceUpDownArrow()
        {
            try
            {
                LinkButton lnk = (LinkButton)lsvInterview.FindControl(hdnSortColumn.Text);
                HtmlTableCell im = (HtmlTableCell)lnk.Parent;
                im.Attributes.Add("class", (hdnSortOrder.Text == "ASC" ? "Ascending" : "Descending"));
                
            }
            catch
            {
            }

        }
        private void BindList()
        {
            bool checkedAtleastOne = false;
            foreach (ListItem chkItem in chkColumnList.Items)
            {
                if (chkItem.Selected == true)
                {
                    checkedAtleastOne = true;
                    break;
                }
            }
            this.lsvInterview.DataSourceID = "odsInterviewList";
            if (checkedAtleastOne)
            {
                this.divlsvInterview.Visible = true;
                this.lsvInterview.DataBind();
            }
            else
            {
                this.divlsvInterview.Visible = false;
                MiscUtil.ShowMessage(lblMessage, "Please Select atleast One column", false);
            }
        }
        private void ClearControls()
        {
            ddlInterviewType.SelectedIndex = 0;
    
            dtPicker.ClearRange();
            ddlStartStartTime.SelectedIndex = ddlStartEndTime.SelectedIndex = ddlEndStartTime.SelectedIndex = ddlEndEndTime.SelectedIndex = ddlRequisition.SelectedIndex =ddlClient.SelectedIndex =ddlClientInterviewers.SelectedIndex =ddlInternalInterviewers.SelectedIndex = 0;
            populateClientInterviers("0");
            ddlClientInterviewers.Enabled = false;
            hdnSelectedContactID.Value = "0";
            txtLocation.Text = string.Empty;
            lsvInterview.Visible = false;
        }
        private void HideUnHideListViewHeaderItem(string headerTitle, string chkItemName)
        {
            HtmlTableCell thHeaderTitle = (HtmlTableCell)lsvInterview.FindControl(headerTitle);

            if (thHeaderTitle != null)
            {
                if (!chkColumnList.Items.FindByValue(chkItemName).Selected)
                {
                    thHeaderTitle.Visible = false;
                }
                else
                {
                    thHeaderTitle.Visible = true;
                }
            }
        }
        private void RenameListHeader(string headerTitle, string LinkName)
        {

            HtmlTableCell thHeaderTitle = (HtmlTableCell)lsvInterview.FindControl(headerTitle);
            if (thHeaderTitle != null)
            {
                LinkButton link = (LinkButton)thHeaderTitle.FindControl(LinkName);
              if(link !=null)  link.Text = link.Text.Replace("Account", "Department");
              if (link != null) link.ToolTip  = link.ToolTip .Replace("Account", "Department");
            }
        }
        private void HideUnhideListViewHeader()
        {
            if (SiteSetting[DefaultSiteSetting.ApplicationEdition.ToString()].ToString() == UIConstants.INTERNAL_HIRING)
            {
                RenameListHeader("thClient", "btnClient");
                RenameListHeader("thClientInterviewer", "btnClientInterviewer");
            }
            HideUnHideListViewHeaderItem("thId", "Id");
            HideUnHideListViewHeaderItem("thDate", "Date");
            HideUnHideListViewHeaderItem("thStartTime", "StartTime");
            HideUnHideListViewHeaderItem("thEndTime", "EndTime");
            HideUnHideListViewHeaderItem("thCandidateName", "CandidateName");
            HideUnHideListViewHeaderItem("thJobTitle", "JobTitle");
            HideUnHideListViewHeaderItem("thReqCode", "ReqCode");
            HideUnHideListViewHeaderItem("thApptTitle", "ApptTitle");
            HideUnHideListViewHeaderItem("thLocation", "Location");
            HideUnHideListViewHeaderItem("thType", "Type");
            HideUnHideListViewHeaderItem("thClient", "BU");
            HideUnHideListViewHeaderItem("thClientInterviewer", "BUInterviewers");
            HideUnHideListViewHeaderItem("thInternalInterviewers", "Recruiters");
            HideUnHideListViewHeaderItem("thNotes", "Notes");
            HideUnHideListViewHeaderItem("thOtherInterviewers", "OtherInterviewers");
            HideUnHideListViewHeaderItem("thInterviewFeedback", "InterviewFeedback");//ADDED BY PRAVIN KHOT ON 12/May/2016

        }
        string[] ColumnValues(string val, string split)
        {
            string[] separator = new string[] { split };
            string[] splitvalues = val.Split(separator, StringSplitOptions.RemoveEmptyEntries);
            return splitvalues;
        }

        private void LoadTimeDropDowns()
        {
            int intStartTime = 12;
            string strMin = "00";
            string strAMPM = "AM";
            string strTime = "";
            int value = 0;
            ArrayList arrTime = new ArrayList();
            for (int i = 1; i <= 24; i++)
            {
                //if (i % 2 == 0)
                //{
                //    strMin = "30";
                //}
                //else
                //{
                //    strMin = "00";
                //}
                for (int j = 0; j < 60; j += 15)
                {
                    if (i == 1 && j==0)
                    {
                        value = 0;
                    }
                    else
                    {
                        value += 15;
                    }
                    ListItem item = new ListItem();
                    switch (j)
                    {
                        case 0:
                            strTime = intStartTime.ToString() + ":00 " + strAMPM;
                            item.Text = strTime;
                            item.Value = value.ToString();
                            arrTime.Add(item); //1.2
                            break;
                        case 15:
                            strTime = intStartTime.ToString() + ":15 " + strAMPM;
                            item.Text = strTime;
                            item.Value = value.ToString();
                            arrTime.Add(item); //1.2 //1.2
                            break;
                        case 30:
                            strTime = intStartTime.ToString() + ":30 " + strAMPM;
                            item.Text = strTime;
                            item.Value = value.ToString();
                            arrTime.Add(item); //1.2//1.2
                            break;
                        default:
                            strTime = intStartTime.ToString() + ":45 " + strAMPM;
                            item.Text = strTime;
                            item.Value = value.ToString();
                            arrTime.Add(item); //1.2 //1.2
                            break;
                    }
                    ddlStartStartTime.Items.Add(item);
                       ddlStartEndTime.Items.Add(item);
                       ddlEndStartTime.Items.Add(item);
                       ddlEndEndTime.Items.Add(item);
                }
                if (intStartTime == 12)
                {
                    intStartTime = 1;
                }
                else
                {
                    intStartTime++;
                }
                if (i == 12)
                {
                    strAMPM = "PM";
                }
            }
            //ddlStartStartTime.DataSource = ddlStartEndTime.DataSource = ddlEndStartTime.DataSource = ddlEndEndTime.DataSource = arrTime;
            //ddlStartStartTime.DataBind();
            //ddlStartEndTime.DataBind();
            //ddlEndStartTime.DataBind();
            //ddlEndEndTime.DataBind();
            ////ArrayList arrTime = new ArrayList();
            //for (int i = 1; i <= 48; i++)
            //{
            //    ListItem item = new ListItem();
            //    if (i % 2 == 0)
            //    {
            //        strMin = "30";
            //    }
            //    else
            //    {
            //        strMin = "00";
            //    }
            //    string strTime = intStartTime.ToString() + ":" + strMin + " " + strAMPM;
            //    //arrTime.Add(strTime); //1.2
            //    item.Text = strTime;
            //    item.Value = (((i - 1) / 2) * 60 + (i % 2 == 0 ? 30 : 0)).ToString();
            //    if (intStartTime == 12 && i % 2 == 0)
            //    {
            //        intStartTime = 1;
            //    }
            //    else if (i % 2 == 0)
            //    {
            //        intStartTime++;
            //    }
            //    if (i == 24)
            //    {
            //        strAMPM = "PM";
            //    }
            //    ddlStartStartTime.Items.Add(item);
            //    ddlStartEndTime.Items.Add(item);
            //    ddlEndStartTime.Items.Add(item);
            //    ddlEndEndTime.Items.Add(item);
            //}
            ddlStartStartTime.Items.Insert(0, new ListItem(" ", " "));
            ddlStartEndTime.Items.Insert(0, new ListItem(" ", " "));
            ddlEndStartTime.Items.Insert(0, new ListItem(" ", " "));
            ddlEndEndTime.Items.Insert(0, new ListItem(" ", " "));
            //ddlStartTime.DataSource = ddlEndTime.DataSource = arrTime; //1.2

            //ddlStartTime.DataBind();
            //ddlEndTime.DataBind();
            //GetCurrentTime();
        }
        //private void LoadTimeDropDowns()
        //{
        //    int intStartTime = 12;
        //    string strMin = "00";
        //    string strAMPM = "AM";
        //    ArrayList arrTime = new ArrayList();
        //    for (int i = 1; i <= 48; i++)
        //    {
        //        if (i % 2 == 0)
        //        {
        //            strMin = "30";
        //        }
        //        else
        //        {
        //            strMin = "00";
        //        }
        //        string strTime = intStartTime.ToString() + ":" + strMin + " " + strAMPM;
        //        arrTime.Add(strTime); 
        //        if (intStartTime == 12 && i % 2 == 0)
        //        {
        //            intStartTime = 1;
        //        }
        //        else if (i % 2 == 0)
        //        {
        //            intStartTime++;
        //        }
        //        if (i == 24)
        //        {
        //            strAMPM = "PM";
        //        }
        //    }
         
        //    ddlStartStartTime.DataSource = ddlStartEndTime.DataSource = ddlEndStartTime.DataSource = ddlEndEndTime.DataSource = arrTime;
        //    ddlStartStartTime.DataBind();
        //    ddlStartEndTime.DataBind();
        //    ddlEndStartTime.DataBind();
        //    ddlEndEndTime.DataBind();
        //    ddlStartStartTime.Items.Insert(0, new ListItem(" ", "0"));
        //    ddlStartEndTime.Items.Insert(0, new ListItem(" ", "0"));
        //    ddlEndStartTime.Items.Insert(0, new ListItem(" ", "0"));
        //    ddlEndEndTime.Items.Insert(0, new ListItem(" ", "0"));
        //}
        private void LoadJobPosting()
        {
            ddlRequisition.DataSource = Facade.GetAllJobPostingByInterview();
            ddlRequisition.DataTextField = "JobTitle";
            ddlRequisition.DataValueField = "Id";
            ddlRequisition.DataBind();
            ddlRequisition = (DropDownList)MiscUtil.RemoveScriptForDropDown(ddlRequisition);
            ddlRequisition.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));
        }
        private void populateClientInterviers(string CompanyId)
        {

            try
            {
                if (Convert.ToString(ddlClient.SelectedItem.Text) != "Select OU") 
                {
                    string divisionValue = Convert.ToString(ddlClient.SelectedValue);

                    ddlClientInterviewers.DataSource = Facade.GetDivisions(divisionValue, 1);
                    ddlClientInterviewers.DataTextField = "HeadName";
                    ddlClientInterviewers.DataValueField = "Id";
                    ddlClientInterviewers.DataBind();
                    ddlClientInterviewers = (DropDownList)MiscUtil.RemoveScriptForDropDown(ddlClientInterviewers);
                    if (ddlClientInterviewers.Items.Count > 0)
                        ddlClientInterviewers.Enabled = true;
                    else
                        ddlClientInterviewers.Enabled = false;
                }
            }
            catch
            {
                int i = 0;
                ddlClientInterviewers.SelectedValue = i.ToString();
            }

            //if (ddlClient.SelectedIndex > 0)
            //{
            //    ddlClientInterviewers.DataSource = Facade.GetAllCompanyContactsByCompanyId(CompanyId);
            //    ddlClientInterviewers.DataTextField = "FirstName";
            //    ddlClientInterviewers.DataValueField = "Id";
            //    ddlClientInterviewers.DataBind();
            //    ddlClientInterviewers = (DropDownList)MiscUtil.RemoveScriptForDropDown(ddlClientInterviewers);
            //    if (ddlClientInterviewers.Items.Count > 0)
            //        ddlClientInterviewers.Enabled = true;
            //    else
            //        ddlClientInterviewers.Enabled = false;
            //}
            //else
            //    ddlClientInterviewers.Enabled = false;

            //ddlClientInterviewers.Items.Insert(0, new ListItem("Please Select", "0"));
        }
        private void PopulateClientDropdowns()
        {
            ddlClient.Items.Clear();

            //int companyStatus = (int)MiscUtil.GetCompanyStatusFromDefaultSiteSetting(SiteSetting);
            //ddlClient.DataSource = Facade.GetAllClientsByStatus(companyStatus);
            ddlClient.DataSource = Facade.GetDivisionsByMemberId(string.Empty, 0, 0);
            //ddlClient.DataTextField = "CompanyName";
            //ddlClient.DataValueField = "Id";
            ddlClient.DataTextField = "OrgID";       //0.7
            ddlClient.DataValueField = "OrgUnitCode";
            ddlClient.DataBind();
         

            if (SiteSetting[DefaultSiteSetting.AssociateRequisitionsWithEntitiesFromModule.ToString()].ToString() == UIConstants.AccountType_Department)
            {
                ddlClient = (DropDownList)MiscUtil.RemoveScriptForDropDown(ddlClient);
                ddlClient.Items.Insert(0, new ListItem("Select OU", "0"));
                lblClient.Text = "OU";
                lblClientInterviewers.Text = "OU Interviewers";
            }
            else if (SiteSetting[DefaultSiteSetting.AssociateRequisitionsWithEntitiesFromModule.ToString()].ToString() == UIConstants.AccountType_Client)
            {
                ddlClient = (DropDownList)MiscUtil.RemoveScriptForDropDown(ddlClient);
                ddlClient.Items.Insert(0, new ListItem("Select Account", "0"));
                lblClient.Text = "Account";
                lblClientInterviewers.Text = "Account Interviewers";
            }
            else if (SiteSetting[DefaultSiteSetting.AssociateRequisitionsWithEntitiesFromModule.ToString()].ToString() == UIConstants.AccountType_Vendor)
            {
                ddlClient = (DropDownList)MiscUtil.RemoveScriptForDropDown(ddlClient);
                ddlClient.Items.Insert(0, new ListItem("Select Vendor", "0"));
                lblClient.Text = "Vendor";
                lblClientInterviewers.Text = "Vendor Interviewers";
            }

            
        }
       
        private void PrepareView()
        {
            MiscUtil.PopulateInterviewType(ddlInterviewType, Facade);
            MiscUtil.PopulateMemberListByRole(ddlInternalInterviewers , ContextConstants.ROLE_EMPLOYEE, Facade);
            LoadTimeDropDowns();
            LoadJobPosting();
            PopulateClientDropdowns();
           
            ddlClientInterviewers.Items.Insert(0, new ListItem("Please Select", "0"));

         
        }
        private void GenerateRequisitionReport(string format)
        {
            if (string.Equals(format, "word"))
            {
                Response.AddHeader("content-disposition", "attachment;filename=InterviewReport-" + DateTime.Now.ToString("yyyy-MM-dd") + ".doc");
                Response.ContentEncoding = System.Text.Encoding.ASCII;
                Response.ContentType = "application/msword";
                Response.Output.Write(GetRequisitionReportTable());
                Response.Flush();
                Response.End();
            }
            else if (string.Equals(format, "excel"))
            {
                Response.AddHeader("content-disposition", "attachment;filename=InterviewReport-" + DateTime.Now.ToString("yyyy-MM-dd") + ".xls");
                Response.ContentEncoding = System.Text.Encoding.ASCII;
                Response.ContentType = "application/msexcel";
                Response.Output.Write(GetRequisitionReportTable());
                Response.Flush();
                Response.End();
            }
            else if (string.Equals(format, "pdf"))
            {
                PdfConverter pdfConverter = new PdfConverter();
                UnicodeEncoding unicodeEncoding = new UnicodeEncoding();
                pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
                pdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Normal;
                pdfConverter.PdfDocumentOptions.ShowHeader = false;
                pdfConverter.PdfDocumentOptions.ShowFooter = false;
                pdfConverter.PdfDocumentOptions.LeftMargin = 15;
                pdfConverter.PdfDocumentOptions.RightMargin = 5;
                pdfConverter.PdfDocumentOptions.TopMargin = 15;
                pdfConverter.PdfDocumentOptions.BottomMargin = 5;
                pdfConverter.PdfDocumentOptions.GenerateSelectablePdf = true;
                pdfConverter.PdfDocumentOptions.ShowHeader = false;
                pdfConverter.PdfHeaderOptions.DrawHeaderLine = false;
                pdfConverter.PdfFooterOptions.FooterTextColor = Color.Black;
                pdfConverter.PdfFooterOptions.DrawFooterLine = true;
                pdfConverter.PdfFooterOptions.PageNumberText = "Page";
                pdfConverter.PdfFooterOptions.ShowPageNumber = true;

                pdfConverter.LicenseKey = ContextConstants.EPHTMLTOPDFKEY;
                byte[] downloadBytes = pdfConverter.GetPdfBytesFromHtmlString(GetRequisitionReportTable());

                System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
                response.Clear();
                response.AddHeader("Content-Type", "binary/octet-stream");
                response.AddHeader("Content-Disposition", "attachment; filename=InterviewReport-" + DateTime.Now.ToString("yyyy-MM-dd") + ".pdf; size=" + downloadBytes.Length.ToString());
                response.Flush();
                response.BinaryWrite(downloadBytes);
                response.Flush();
                response.End();
            }
        }

        private string GetRequisitionReportTable()
        {
            StringBuilder reqReport = new StringBuilder();
            MemberInterviewDataSource  InterviewDataSource = new MemberInterviewDataSource ();
            IList<MemberInterview> InterviewList = InterviewDataSource.GetPaged((ddlInterviewType.SelectedIndex > 0 ? ddlInterviewType.SelectedValue : "0"), dtPicker .StartDate .ToString (), dtPicker .EndDate .ToString (),
                (ddlStartStartTime.SelectedIndex > 0 ? ddlStartStartTime.SelectedValue : string.Empty), (ddlStartEndTime.SelectedIndex > 0 ? ddlStartEndTime.SelectedValue : string.Empty), (ddlEndStartTime.SelectedIndex > 0 ? ddlEndStartTime.SelectedValue : string.Empty), (ddlEndEndTime.SelectedIndex > 0 ? ddlEndEndTime.SelectedValue : string.Empty),
                (ddlRequisition.SelectedIndex > 0 ? ddlRequisition.SelectedValue : "0"), (ddlClient.SelectedIndex > 0 ? ddlClient.SelectedValue : "0"), (hdnSelectedContactID.Value != "0" && hdnSelectedContactID.Value != "" ? hdnSelectedContactID.Value  : "0"), (ddlInternalInterviewers.SelectedIndex > 0 ? ddlInternalInterviewers.SelectedValue : "0"), (txtLocation.Text != null ? txtLocation.Text : string.Empty), null, -1, -1);

            if (InterviewList != null)
            {
                string WebRoot = ConfigurationManager.AppSettings["WebRoot"].ToString();
                string LogoPath;
                if (string.IsNullOrEmpty(HttpRuntime.AppDomainAppVirtualPath))
                {
                    LogoPath = WebRoot;
                }
                else
                {
                    LogoPath = WebRoot + HttpRuntime.AppDomainAppVirtualPath;
                }

                reqReport.Append("<table style='font-family : calibriz;width:100%; border-collapse:collapse; border-spacing: 1px;' align='left' border = '1' bordercolor='#000000'  cellspacing='0' cellpadding='0'>");

                reqReport.Append("    <tr>");
                reqReport.Append("        <td align=left  width='300' height='75'>&nbsp;<img src='" + UrlConstants.ApplicationBaseUrl + "Images/logo-left-75px.png' style= style='height:56px;width:56px'/></td>");
                reqReport.Append("    </tr>");
                reqReport.Append(" <tr>");
                if (chkColumnList.Items.FindByValue("Date").Selected)
                {
                    reqReport.Append("     <th>Date</th>");
                }
                if (chkColumnList.Items.FindByValue("StartTime").Selected)
                {
                    reqReport.Append("     <th>Start Time</th>");
                }

                if (chkColumnList.Items.FindByValue("EndTime").Selected)
                {
                    reqReport.Append("     <th>End Time</th>");
                }
                if (chkColumnList.Items.FindByValue("Id").Selected)
                {
                    reqReport.Append("     <th>Candidate ID #</th>");
                }
                if (chkColumnList.Items.FindByValue("CandidateName").Selected)
                {
                    reqReport.Append("     <th>Candidate Name</th>");
                }
                if (chkColumnList.Items.FindByValue("JobTitle").Selected)
                {
                    reqReport.Append("     <th>Job Title</th>");
                }

                if (chkColumnList.Items.FindByValue("ReqCode").Selected)
                {
                    reqReport.Append("     <th>Req. Code</th>");
                }

                if (chkColumnList.Items.FindByValue("ApptTitle").Selected)
                {
                    reqReport.Append("     <th>Appt. Title</th>");

                }

                if (chkColumnList.Items.FindByValue("Location").Selected)
                {
                    reqReport.Append("     <th>Location</th>");
                }

                if (chkColumnList.Items.FindByValue("Type").Selected)
                {
                    reqReport.Append("     <th>Type</th>");
                }

                if (chkColumnList.Items.FindByValue("BU").Selected)
                {
                    if (SiteSetting[DefaultSiteSetting.ApplicationEdition.ToString()].ToString() == UIConstants.INTERNAL_HIRING)
                    {
                        reqReport.Append("     <th>OU</th>");
                    }
                    else
                        reqReport.Append("     <th>Account</th>");
                }
                if (chkColumnList.Items.FindByValue("BUInterviewers").Selected)
                {
                    if (SiteSetting[DefaultSiteSetting.ApplicationEdition.ToString()].ToString() == UIConstants.INTERNAL_HIRING)
                        reqReport.Append("     <th>OU Interviewers </th>");
                    else
                    reqReport.Append("     <th>Account Interviewers </th>");
                }
                if (chkColumnList.Items.FindByValue("Recruiters").Selected)
                {
                    reqReport.Append("     <th>Recruiters</th>");
                }
                if (chkColumnList.Items.FindByValue("Notes").Selected)
                {
                    reqReport.Append("     <th>Notes</th>");
                }
                if (chkColumnList.Items.FindByValue("OtherInterviewers").Selected)
                {
                    reqReport.Append("     <th>OtherInterviewers</th>");
                }
                //******************Added by pravin khot on 12/May/2016**********
                if (chkColumnList.Items.FindByValue("InterviewFeedback").Selected)
                {
                    reqReport.Append("     <th>InterviewFeedback</th>");
                }
                //**********************END**************************

                reqReport.Append(" </tr>");

                foreach (MemberInterview  interview in InterviewList )
                {
                    if (interview != null)
                    {
                        reqReport.Append(" <tr>");
                        if (chkColumnList.Items.FindByValue("Date").Selected)
                        {
                            if (interview.AllDayEvent)
                                reqReport.Append("     <td>" + interview.StartDateTime.ToShortDateString() + " All Day" + "&nbsp;</td>");
                            else
                                reqReport.Append("     <td>" + interview.StartDateTime.ToShortDateString() + "&nbsp;</td>");
                        }
                        if (chkColumnList.Items.FindByValue("StartTime").Selected)
                        {
                            if (interview.AllDayEvent)
                                reqReport.Append("     <td>" + "&nbsp;</td>");
                            else 
                                reqReport.Append("     <td>" + interview.StartDateTime.ToString ("hh:mm tt") + "&nbsp;</td>");
                        }
                        if (chkColumnList.Items.FindByValue("EndTime").Selected)
                        {
                            if (interview.AllDayEvent)
                                reqReport.Append("     <td>" + "&nbsp;</td>");
                            else 
                                reqReport.Append("     <td>" + getTime (interview .StartDateTime ,interview .Duration ).ToString ("hh:mm tt") + "&nbsp;</td>");
                        }
                        if (chkColumnList.Items.FindByValue("Id").Selected)
                        {
                            reqReport.Append("     <td>" + "A" + interview.MemberId .ToString () + "&nbsp;</td>");
                        }

                        if (chkColumnList.Items.FindByValue("CandidateName").Selected)
                        {
                            reqReport.Append("     <td>" + interview.FirstName + "  " + interview .LastName  + "&nbsp;</td>");
                        }
                        if (chkColumnList.Items.FindByValue("JobTitle").Selected)
                        {
                            reqReport.Append("     <td>" + interview.JobTitle  + "&nbsp;</td>");
                        }

                        if (chkColumnList.Items.FindByValue("ReqCode").Selected)
                        {
                            reqReport.Append("     <td>" + interview.JobPostingCode  + "&nbsp;</td>");
                        }

                        if (chkColumnList.Items.FindByValue("ApptTitle").Selected)
                        {
                            reqReport.Append("     <td>" + interview.Title  + "&nbsp;</td>");

                        }

                        if (chkColumnList.Items.FindByValue("Location").Selected)
                        {
                            reqReport.Append("     <td>" + interview.Location  + "&nbsp;</td>");
                        }

                        if (chkColumnList.Items.FindByValue("Type").Selected)
                        {
                                reqReport.Append("     <td>" + interview .InterviewTypeName  + "&nbsp;</td>");
                        }

                        if (chkColumnList.Items.FindByValue("BU").Selected)
                        {
                            reqReport.Append("     <td>" + interview .CompanyName  + "&nbsp;</td>");
                        }

                        if (chkColumnList.Items.FindByValue("BUInterviewers").Selected)
                        {
                            if(interview .ClientInterviewerName !=string .Empty )
                                reqReport.Append("     <td>" + interview .ClientInterviewerName .Trim () + "&nbsp;</td>");
                            else
                                reqReport.Append("     <td>" + string.Empty + "&nbsp;</td>");
                        }
                        if (chkColumnList.Items.FindByValue("Recruiters").Selected)
                        {
                            if(interview .InterviewerName !=string .Empty )
                                reqReport.Append("     <td>" + interview .InterviewerName.Trim () + "&nbsp;</td>");
                            else
                                reqReport.Append("     <td>" + string.Empty + "&nbsp;</td>");
                        }
                        if (chkColumnList.Items.FindByValue("Notes").Selected)
                        {
                                reqReport.Append("     <td>" + interview .Remark  + "&nbsp;</td>");
                        }
                        if (chkColumnList.Items.FindByValue("OtherInterviewers").Selected)
                        {
                            if (interview.OtherInterviewers != string.Empty)
                                reqReport.Append("     <td>" + interview.OtherInterviewers + "&nbsp;</td>");
                            else
                                reqReport.Append("     <td>" + string.Empty + "&nbsp;</td>");
                        }
                        //****************Code added by pravin khot on 12/May/2016*******
                        if (chkColumnList.Items.FindByValue("InterviewFeedback").Selected)
                        {
                            reqReport.Append("     <td>" + interview.Feedback + "&nbsp;</td>");
                        }
                        //******************************END*************************

                        reqReport.Append(" </tr>");
                    }
                }

                reqReport.Append("    <tr>");
                reqReport.Append("        <td align=left  width='300' height='75'><img src='" + UrlConstants.ApplicationBaseUrl + "Images/logo-left-75px.png' style= style='height:56px;width:56px;'/></td>");
                reqReport.Append("    </tr>");
               
                reqReport.Append(" </table>");
            }
            return reqReport.ToString();
        }
        private DateTime getTime(DateTime date, int duration)
        {
            if (duration > 0)
            {
                duration = duration / 60;
            }
            return date.AddMinutes(Convert.ToDouble(duration));
        }
        public void MessageAnswered(object sender, ConfirmationWindow.MsgBoxEventArgs e)
        {

            if (e.Answer == ConfirmationWindow.enmAnswer.OK)
            {
                repor("pdf");
            }

        }
        public void repor(string rep)
        {
            int j = 0;
            int k = 0;
            foreach (ListItem items in chkColumnList.Items)
            {
                if (items.Selected)
                {
                    j = 1;

                }
                else
                    k = 1;
            }
            if ((j == 1 && k == 1) || (k == 0 && j == 1))
                GenerateRequisitionReport(rep);
            else
            {
                MiscUtil.ShowMessage(lbMessage, "Please select columns before generating the report.", true);
            }
        }
        #endregion
        # region Page Event
        protected void Page_Load(object sender, EventArgs e)
        {

            //try
            //{
            //    if (Facade.IsAvailableSessionId(Session.SessionID) != 1)
            //    {
            //        FormsAuthentication.SignOut();
            //        FormsAuthentication.RedirectToLoginPage();
            //        return;
            //    }
            //}
            //catch { }
            ddlClient.Attributes.Add("onChange", "return Company_OnChange('" + ddlClient.ClientID + "','" + ddlClientInterviewers.ClientID + "','" + hdnSelectedContactID.ClientID + "')");
            ddlClientInterviewers.Attributes.Add("OnChange", "return Contact_OnChange('" + ddlClientInterviewers.ClientID + "','" + hdnSelectedContactID.ClientID + "')");
            ddlClient.Attributes.Add("onmouseover", "this.title=this.options[this.selectedIndex].title");
            ddlClientInterviewers.Attributes.Add("onmouseover", "this.title=this.options[this.selectedIndex].title");
            ddlInternalInterviewers.Attributes.Add("onmouseover", "this.title=this.options[this.selectedIndex].title");
            ddlRequisition.Attributes.Add("onmouseover", "this.title=this.options[this.selectedIndex].title");
            uclConfirm.MsgBoxAnswered += MessageAnswered;
            ArrayList AList = Facade.GetAllMemberPrivilegeIdsByMemberId(CurrentMember.Id);
            if (AList.Contains(403))
                isAccess = true;
            else
            {
                isAccess = false;
                CustomSiteMap CustomMap = new CustomSiteMap();
                CustomMap = Facade.GetAllCustomSiteMapByParentIdAndMemberPrivilege(358, CurrentMember.Id);
                if (CustomMap == null) _IsAccessToCandidate = false;
                else
                {
                    SitemapIdForCandidate = CustomMap.Id;
                    UrlForCandidate = "~/" + CustomMap.Url.ToString();
                }
            }
            if (!IsPostBack)
            {
                PrepareView();
                if (Request.Cookies[CookieName] != null)
                {
                    //string selectedColumnValue = Request.Cookies[CookieName].Value;
                    string selectedColumnValue = "";
                    //string[] Columns = ColumnValues(selectedColumnValue, "#");

                    foreach (ListItem item in chkColumnList.Items)
                    {
                        item.Selected = false;
                        selectedColumnValue = selectedColumnValue + item.Value + "#";
                    }
                    string[] Columns = ColumnValues(selectedColumnValue, "#");
                    foreach (string item in Columns)
                    {
                        chkColumnList.Items.FindByValue(item).Selected = true;
                    }
                }
            }
            if (!IsPostBack)
            {

                btnSearch_Click(sender, e);
            }
        }
        #endregion
        #region Button Events
        protected void btnClear_Click(object sender, EventArgs e)
        {
            ClearControls();
            divExportButtons.Visible = false;
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            lsvInterview.Visible = true;
            hdnScrollPos.Value = "0";
            lsvInterview.Items.Clear();
            ASP.controls_pagercontrol_ascx PagerControls = (ASP.controls_pagercontrol_ascx)this.lsvInterview.FindControl("pagerControl");
            if (PagerControls != null)
            {
                DataPager pager = (DataPager)PagerControls.FindControl("pager");
                if (pager != null)
                {
                    if (pager.Page.IsPostBack)
                        pager.SetPageProperties(0, pager.MaximumRows, true);
                }
            }
            BindList();
            if (lsvInterview != null && lsvInterview.Items.Count > 0)
            {
                divExportButtons.Visible = true;
                HideUnhideListViewHeader();
                lsvInterview.Visible = true;

                if (hdnSortColumn.Text == "") hdnSortColumn.Text = "btnDate";
                if (hdnSortOrder.Text == "") hdnSortOrder.Text = "DESC";
                PlaceUpDownArrow();
            }
            else divExportButtons.Visible = false;
            string pagesize = "";
            pagesize = (Request.Cookies["InterviewReportRowPerPage"] == null ? "" : Request.Cookies["InterviewReportRowPerPage"].Value); ;
            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvInterview.FindControl("pagerControl");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)PagerControl.FindControl("pager");
                if (pager != null)
                {
                    pager.PageSize = Convert.ToInt32(pagesize == "" ? "20" : pagesize);
                    if (pager.Page.IsPostBack)
                        pager.SetPageProperties(0, pager.MaximumRows, true);
                }
            }

            System.Web.UI.HtmlControls.HtmlTableCell tdpager = (System.Web.UI.HtmlControls.HtmlTableCell)lsvInterview.FindControl("tdpager");
            int count = 0;
            foreach (ListItem item in chkColumnList.Items)
            {
                if (item.Selected)
                    count = count + 1;
            }
            if (tdpager != null) tdpager.ColSpan = count;

            populateClientInterviers(Convert.ToString(ddlClient.SelectedValue));
            ControlHelper .SelectListByValue (ddlClientInterviewers ,hdnSelectedContactID .Value );
        }
        protected void btnExportToPDF_Click(object sender, EventArgs e)
        {
            repor("pdf");
        }
        protected void btnExportToExcel_Click(object sender, EventArgs e)
        {
            repor("excel");
        }
        protected void btnExportToWord_Click(object sender, EventArgs e)
        {
            repor("word");
        }
        #endregion
        #region Listview Events
        protected void lsvInterview_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            StringBuilder ColumnOption = new StringBuilder();
            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {

                MemberInterview  interview = ((ListViewDataItem)e.Item).DataItem as MemberInterview ;

                if (interview != null)
                {

                    HyperLink lnkJobTitle = (HyperLink)e.Item.FindControl("lnkJobTitle");
                    HtmlTableCell tdJobTitle = (HtmlTableCell)e.Item.FindControl("tdJobTitle");

                    if (chkColumnList.Items.FindByValue("JobTitle").Selected)
                    {                        
                        lnkJobTitle.Attributes.Add("onclick", "EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/JobDetail.aspx?JID=" + interview.JobPostingId + "&FromPage=JobDetail" + "','700px','570px'); return false;");
                        lnkJobTitle.Text = interview.JobTitle;
                        lnkJobTitle.Visible = true;
                        tdJobTitle.Visible = true;
                        ColumnOption.Append("JobTitle").Append("#");
                    }
                    else
                    {
                        tdJobTitle.Visible = false;
                        lnkJobTitle.Visible = false;
                    }


                    Label lblReqCode = (Label)e.Item.FindControl("lblReqCode");

                    HtmlTableCell tdReqCode = (HtmlTableCell)e.Item.FindControl("tdReqCode");

                    if (chkColumnList.Items.FindByValue("ReqCode").Selected)
                    {
                        lblReqCode.Text = interview.JobPostingCode;
                        tdReqCode.Visible = true;
                        lblReqCode.Visible = true;
                        ColumnOption.Append("ReqCode").Append("#");
                    }
                    else
                    {
                        tdReqCode.Visible = false;
                        lblReqCode.Visible = false;
                    }
                    if (interview.AllDayEvent)
                    {
                        Label lblDate = (Label)e.Item.FindControl("lblDate");

                        HtmlTableCell tdDate = (HtmlTableCell)e.Item.FindControl("tdDate");

                        if (chkColumnList.Items.FindByValue("Date").Selected)
                        {
                            lblDate.Text = interview.StartDateTime.ToShortDateString() +" All Day";
                            tdDate.Visible = true;
                            lblDate.Visible = true;
                            ColumnOption.Append("Date").Append("#");
                        }
                        else
                        {
                            lblDate.Visible = false;
                            tdDate.Visible = false;
                        }

                        if (!chkColumnList.Items.FindByValue("StartTime").Selected)
                        {
                            Label lblStartTime = (Label)e.Item.FindControl("lblStartTime");

                            HtmlTableCell tdStartTime = (HtmlTableCell)e.Item.FindControl("tdStartTime");

                            lblStartTime.Visible = tdStartTime.Visible = false;
                        }

                        if (!chkColumnList.Items.FindByValue("EndTime").Selected)
                        {
                            Label lblEndTime = (Label)e.Item.FindControl("lblEndTime");

                            HtmlTableCell tdEndTime = (HtmlTableCell)e.Item.FindControl("tdEndTime");

                            lblEndTime.Visible = tdEndTime.Visible = false;
                        }

                    }
                    else
                    {
                        Label lblDate = (Label)e.Item.FindControl("lblDate");

                        HtmlTableCell tdDate = (HtmlTableCell)e.Item.FindControl("tdDate");

                        if (chkColumnList.Items.FindByValue("Date").Selected)
                        {
                            lblDate.Text = interview.StartDateTime.ToShortDateString();
                            tdDate.Visible = true;
                            lblDate.Visible = true;
                            ColumnOption.Append("Date").Append("#");
                        }
                        else
                        {
                            lblDate.Visible = false;
                            tdDate.Visible = false;
                        }

                        Label lblStartTime = (Label)e.Item.FindControl("lblStartTime");

                        HtmlTableCell tdStartTime = (HtmlTableCell)e.Item.FindControl("tdStartTime");

                        if (chkColumnList.Items.FindByValue("StartTime").Selected)
                        {
                            lblStartTime.Text = interview.StartDateTime.ToString("hh:mm tt");
                            tdStartTime.Visible = true;
                            lblStartTime.Visible = true;
                            ColumnOption.Append("StartTime").Append("#");
                        }
                        else
                        {
                            lblStartTime.Visible = false;
                            tdStartTime.Visible = false;
                        }

                        Label lblEndTime = (Label)e.Item.FindControl("lblEndTime");

                        HtmlTableCell tdEndTime = (HtmlTableCell)e.Item.FindControl("tdEndTime");

                        if (chkColumnList.Items.FindByValue("EndTime").Selected)
                        {
                            lblEndTime.Text = getTime(interview.StartDateTime, interview.Duration).ToString("hh:mm tt");
                            tdEndTime.Visible = true;
                            lblEndTime.Visible = true;
                            ColumnOption.Append("EndTime").Append("#");
                        }
                        else
                        {
                            lblEndTime.Visible = false;
                            tdEndTime.Visible = false;
                        }
                    }
                    HyperLink lnkCandidateName = (HyperLink)e.Item.FindControl("lnkCandidateName");
                    HtmlTableCell tdCandidateName = (HtmlTableCell)e.Item.FindControl("tdCandidateName");
                    string _strFullName = interview.FirstName + " " + interview.LastName;
                    if (_strFullName.Trim() == "")
                        _strFullName = "No Candidate Name";
                    if (chkColumnList.Items.FindByValue("CandidateName").Selected)
                    {
                        if (isAccess )
                        {
                            ControlHelper.SetHyperLink(lnkCandidateName, UrlConstants.ATS.ATS_INTERNALINTERVIEW_SCHEDULE, string.Empty, _strFullName, UrlConstants.PARAM_MEMBER_ID, StringHelper.Convert(interview.MemberId), UrlConstants.PARAM_SITEMAP_ID, "403",UrlConstants .PARAM_SITEMAP_PARENT_ID ,UrlConstants .Candidate .ATS_OVERVIEW_SITEMAP_PARENTID );
                        }
                        else
                        {
                            if(_IsAccessToCandidate )
                                ControlHelper.SetHyperLink(lnkCandidateName, UrlForCandidate, string.Empty, _strFullName, UrlConstants.PARAM_MEMBER_ID, StringHelper.Convert(interview.MemberId), UrlConstants.PARAM_SITEMAP_ID, SitemapIdForCandidate.ToString());
                            else
                                lnkCandidateName.Text = _strFullName;// interview.FirstName + " " + interview.LastName;
                        }
                        tdCandidateName.Visible = true;
                        lnkCandidateName.Visible = true;
                        ColumnOption.Append("CandidateName").Append("#");
                    }
                    else
                    {
                        lnkCandidateName.Visible = false;
                        tdCandidateName.Visible = false;
                    }

                    if (chkColumnList.Items.FindByValue("Id").Selected)
                    {
                        HtmlTableCell tdId = (HtmlTableCell)e.Item.FindControl("tdId");
                        tdId.Visible = true;
                        Label lblCandidateID = (Label)e.Item.FindControl("lblCandidateID");
                        lblCandidateID.Text = "A" + interview.MemberId.ToString();
                    }
                    else
                    {
                        HtmlTableCell tdId = (HtmlTableCell)e.Item.FindControl("tdId");
                        tdId.Visible = false;
                    }
                    Label lblApptTitle = (Label)e.Item.FindControl("lblApptTitle");

                    HtmlTableCell tdApptTitle = (HtmlTableCell)e.Item.FindControl("tdApptTitle");

                    if (chkColumnList.Items.FindByValue("ApptTitle").Selected)
                    {
                        if (interview.Title.Length > 15)
                        {
                            lblApptTitle.Text = interview.Title.Substring(0, 13) + "...";
                            lblApptTitle.ToolTip = interview.Title;
                        }
                        else 
                        lblApptTitle.Text = interview.Title;
                        tdApptTitle.Visible = true;
                        lblApptTitle.Visible = true;
                        ColumnOption.Append("ApptTitle").Append("#");
                    }
                    else
                    {
                        lblApptTitle.Visible = false;
                        tdApptTitle.Visible = false;
                    }

                    Label lblLocation = (Label)e.Item.FindControl("lblLocation");

                    HtmlTableCell tdLocation = (HtmlTableCell)e.Item.FindControl("tdLocation");

                    if (chkColumnList.Items.FindByValue("Location").Selected)
                    {
                        if (interview.Location.Length > 15)
                        {
                            lblLocation.Text = interview.Location.Substring(0, 13) + "...";
                            lblLocation.ToolTip = interview.Location;
                        }
                        else 
                            lblLocation.Text = interview.Location;
                        tdLocation.Visible = true;
                        lblLocation.Visible = true;
                        ColumnOption.Append("Location").Append("#");
                    }
                    else
                    {
                        lblLocation.Visible = false;
                        tdLocation.Visible = false;
                    }

                    Label lblType = (Label)e.Item.FindControl("lblType");

                    HtmlTableCell tdType = (HtmlTableCell)e.Item.FindControl("tdType");

                    if (chkColumnList.Items.FindByValue("Type").Selected)
                    {
                        lblType.Text = interview.InterviewTypeName;
                        tdType.Visible = true;
                        lblType.Visible = true;
                        ColumnOption.Append("Type").Append("#");
                    }
                    else
                    {
                        tdType.Visible = false;
                        lblType.Visible = false;
                    }
                    Label  lblClient = (Label )e.Item.FindControl("lblClient");
                    HtmlTableCell tdClient = (HtmlTableCell)e.Item.FindControl("tdClient");
                    if (chkColumnList.Items.FindByValue("BU").Selected)
                    {
                        lblClient.Text = interview.CompanyName;
                        tdClient.Visible = true;
                        lblClient.Visible = true;
                        ColumnOption.Append("Client").Append("#");
                    }
                    else
                    {
                        lblClient.Visible = false;
                        tdClient.Visible = false;
                    }


                    Label lblClientInterviewers = (Label)e.Item.FindControl("lblClientInterviewers");

                    HtmlTableCell tdClientInterviewers = (HtmlTableCell)e.Item.FindControl("tdClientInterviewers");

                    if (chkColumnList.Items.FindByValue("BUInterviewers").Selected)
                    {
                        lblClientInterviewers.Text = interview.ClientInterviewerName.Trim();
                        tdClientInterviewers.Visible = true;
                        lblClientInterviewers.Visible = true;
                        ColumnOption.Append("ClientInterviewers").Append("#");
                    }
                    else
                    {
                        lblClientInterviewers.Visible = false;
                        tdClientInterviewers.Visible = false;
                    }
                    Label  lblInternalInterviewers = (Label )e.Item.FindControl("lblInternalInterviewers");
                    HtmlTableCell tdInternalInterviewers = (HtmlTableCell)e.Item.FindControl("tdInternalInterviewers");

                    if (chkColumnList.Items.FindByValue("Recruiters").Selected)
                    {
                        lblInternalInterviewers.Text = interview.InterviewerName.Trim();
                        tdInternalInterviewers.Visible = true;
                        lblInternalInterviewers.Visible = true;
                        ColumnOption.Append("Recruiters").Append("#");
                    }
                    else
                    {
                        lblInternalInterviewers.Visible = false;
                        tdInternalInterviewers.Visible = false;
                    }

                    Label lblNotes = (Label)e.Item.FindControl("lblNotes");

                    HtmlTableCell tdNotes = (HtmlTableCell)e.Item.FindControl("tdNotes");

                    if (chkColumnList.Items.FindByValue("Notes").Selected)
                    {
                        if (interview.Remark.Length > 25)
                        {
                            lblNotes.Text = interview.Remark.Substring(0, 24) + "...";
                            lblNotes.ToolTip = interview.Remark;
                        }
                        else
                            lblNotes.Text = interview.Remark;
                        tdNotes.Visible = true;
                        lblNotes.Visible = true;
                        ColumnOption.Append("Notes").Append("#");
                    }
                    else
                    {
                        lblNotes.Visible = false;
                        tdNotes.Visible = false;
                    }

                    //*************Added by pravin khot on 12/May/2016*******
                    Label lblInterviewFeedback = (Label)e.Item.FindControl("lblInterviewFeedback");

                    HtmlTableCell tdInterviewFeedback = (HtmlTableCell)e.Item.FindControl("tdInterviewFeedback");

                    if (chkColumnList.Items.FindByValue("InterviewFeedback").Selected)
                    {
                        if (interview.Feedback.Length > 25)
                        {
                            lblInterviewFeedback.Text = interview.Feedback.Substring(0, 24) + "...";
                            lblInterviewFeedback.ToolTip = interview.Feedback;
                        }
                        else
                            lblInterviewFeedback.Text = interview.Feedback;
                        tdInterviewFeedback.Visible = true;
                        lblInterviewFeedback.Visible = true;
                        ColumnOption.Append("InterviewFeedback").Append("#");
                    }
                    else
                    {
                        lblInterviewFeedback.Visible = false;
                        tdInterviewFeedback.Visible = false;
                    }

                    //***********************END****************************
                    if (Request.Cookies[CookieName] != null)
                    {
                        Response.Cookies[CookieName].Value = ColumnOption.ToString();
                    }

                   
                    else
                    {
                        HttpCookie aCookie = new HttpCookie(CookieName);
                        aCookie.Expires = DateTime.Now.AddDays(1);
                        aCookie.Value = ColumnOption.ToString();
                        Response.Cookies.Add(aCookie);
                    }

                    Label lblOtherInterviewers = (Label)e.Item.FindControl("lblOtherInterviewers");
                    HtmlTableCell tdOtherInterviewers = (HtmlTableCell)e.Item.FindControl("tdOtherInterviewers");

                    if (chkColumnList.Items.FindByValue("OtherInterviewers").Selected)
                    {
                        lblOtherInterviewers.Text = interview.OtherInterviewers.Trim().TrimStart(',');
                        tdOtherInterviewers.Visible = true;
                        lblOtherInterviewers.Visible = true;
                        ColumnOption.Append("OtherInterviewers").Append("#");
                    }
                    else
                    {
                        lblOtherInterviewers.Visible = false;
                        tdOtherInterviewers.Visible = false;

                    }

                   
                }

            }
        }
        protected void lsvInterview_PreRender(object sender, EventArgs e)
        {
            divExportButtons.Visible = lsvInterview.Items.Count > 0;
            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvInterview.FindControl("pagerControl");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)PagerControl.FindControl("pager");
                if (pager != null)
                {
                    DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                    if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
                }
                HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
                if (hdnRowPerPageName != null) hdnRowPerPageName.Value = "InterViewReportRowPerPage";
            }
            PlaceUpDownArrow();
            HideUnhideListViewHeader();
            if (IsPostBack)
            {
                if (PagerControl == null)
                {
                    lsvInterview.Items.Clear();
                    lsvInterview.DataSource = null;
                    lsvInterview.DataBind();
                    divExportButtons.Visible = lsvInterview.Items.Count > 0;
                }
            }
        }
        protected void lsvInterview_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Sort")
                {
                    LinkButton lnkbutton = (LinkButton)e.CommandSource;
                    if (hdnSortColumn.Text == lnkbutton.ID)
                    {
                        if (hdnSortOrder.Text == "ASC") hdnSortOrder.Text = "DESC";
                        else hdnSortOrder.Text = "ASC";
                    }
                    else
                    {
                        hdnSortColumn.Text = lnkbutton.ID;
                        hdnSortOrder.Text = "ASC";
                    }
                }
            }
            catch
            { }
        }
        #endregion
    }
}
