﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName:       CandidateJoiningreport1.aspx.cs
    Description:    This is the objects source file
    Created By:  Kanchan Yeware   
    Created On:  09-Aug-2016  
    Modification Log:
*/
#region Namespaces
using System;
using TPS360.Common.BusinessEntities;
using TPS360.Web.UI.Helper;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Linq;
#endregion

#region CandidateJoiningreport1
namespace TPS360.Web.UI.Reports
{
    public partial class CandidateJoiningreport1 : BasePage
    {
        #region Members
        private bool _IsAccessToEmployee = true;
        private static string UrlForEmployee = string.Empty;
        private static int IdForSitemap = 0;

        string _cookiename;
        string CookieName
        {
            get { return _cookiename; }
            set { _cookiename = value; }
        }

        #endregion

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            CookieName = "CandidateReportUserColumnOption_" + base.CurrentMember.PrimaryEmail;
            CustomSiteMap CustomMap = new CustomSiteMap();
            CustomMap = Facade.GetAllCustomSiteMapByParentIdAndMemberPrivilege(358, CurrentMember.Id);
            if (CustomMap == null) _IsAccessToEmployee = false;
            else
            {
                IdForSitemap = CustomMap.Id;
                UrlForEmployee = "~/" + CustomMap.Url.ToString();
            }

            if (!IsPostBack)
            {
                lsvCandidateList.DataBind();
                divCandidateList.Visible = false;
                if (Request.Cookies[CookieName] != null)
                {
                    string ColumnOptions = "JobPostingCode#Name#SupervisoryOrgCode#SupervisoryOrgDesc#ReasonForHire#JobTitle#NoOfOpenings#AvailabilityDate#JobRestrictions#Company#CostCenter#Region#PayGroup#PayGroupGrade#";
                    string[] Columns = ColumnOptions.Split('#');
                    foreach (ListItem item in chkColumnList.Items)
                    {
                        item.Selected = false;
                    }
                    foreach (string item in Columns)
                    {
                        if (chkColumnList.Items.FindByValue(item) != null) chkColumnList.Items.FindByValue(item).Selected = true;
                    }
                }
            }
            if (!IsPostBack)
            {


            }
            btnSearch_Click(sender, e);
        }

        protected void lsvCandidateList_PreRender(object sender, EventArgs e)
        {
            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvCandidateList.FindControl("pagerControl");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)PagerControl.FindControl("pager");
                if (pager != null)
                {
                    DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                    if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
                }
                HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
                if (hdnRowPerPageName != null) hdnRowPerPageName.Value = "CandidateReportRowPerPage";
            }

            PlaceUpDownArrow();
            HideListViewHeaderItem();
            if (IsPostBack)
            {
                if (lsvCandidateList.Items.Count == 0)
                {
                    lsvCandidateList.DataSource = null;
                    lsvCandidateList.DataBind();
                }
            }
        }

        protected void lsvCandidateList_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            lsvCandidateList.Visible = true;
            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                CandidateJoiningReport candidatInfo = ((ListViewDataItem)e.Item).DataItem as CandidateJoiningReport;

                if (candidatInfo != null)
                {
                    divExportButton.Visible = false;
                    Label lblJobPostingCode = (Label)e.Item.FindControl("lblJobPostingCode");
                    Label lblCandidateName = (Label)e.Item.FindControl("lblCandidateName");
                    Label lblSupervisoryOrgCode = (Label)e.Item.FindControl("lblSupervisoryOrgCode");
                    Label lblSupervisoryOrgShortText = (Label)e.Item.FindControl("lblSupervisoryOrgShortText");
                    Label lblSupervisoryOrgDesc = (Label)e.Item.FindControl("lblSupervisoryOrgDesc");
                    Label lblReasonForHire = (Label)e.Item.FindControl("lblReasonForHire");
                    Label lblJobTitle = (Label)e.Item.FindControl("lblJobTitle");
                    Label lblNoOfOpenings = (Label)e.Item.FindControl("lblNoOfOpenings");
                    Label lblAvailabilityDate = (Label)e.Item.FindControl("lblAvailabilityDate");
                    Label lblFinalHiredDate = (Label)e.Item.FindControl("lblFinalHiredDate");
                    Label lblJobRestrictions = (Label)e.Item.FindControl("lblJobRestrictions");
                    Label lblJobFamily = (Label)e.Item.FindControl("lblJobFamily");
                    Label lblJobProfiles = (Label)e.Item.FindControl("lblJobProfiles");
                    Label lblCity = (Label)e.Item.FindControl("lblCity");
                    Label lblTimeType = (Label)e.Item.FindControl("lblTimeType");
                    Label lblWorkerType = (Label)e.Item.FindControl("lblWorkerType");
                    Label lblWorkerSubType = (Label)e.Item.FindControl("lblWorkerSubType");
                    Label lblCompany = (Label)e.Item.FindControl("lblCompany");
                    Label lblCostCenter = (Label)e.Item.FindControl("lblCostCenter");
                    Label lblRegion = (Label)e.Item.FindControl("lblRegion");
                    Label lblInternationalEmployeeStatus = (Label)e.Item.FindControl("lblInternationalEmployeeStatus");
                    Label lblPayGroup = (Label)e.Item.FindControl("lblPayGroup");
                    Label lblPayGroupGrade = (Label)e.Item.FindControl("lblPayGroupGrade");
                    Label lblMaximumFTE = (Label)e.Item.FindControl("lblMaximumFTE");
                    Label lblNotes = (Label)e.Item.FindControl("lblNotes");

                    lblJobPostingCode.Text = candidatInfo.JobPostingCode;

                    string strFullName = MiscUtil.GetFirstAndLastName(candidatInfo.FirstName, candidatInfo.LastName);
                    lblCandidateName.Text = strFullName;

                    lblSupervisoryOrgCode.Text = Convert.ToString(candidatInfo.SupervisoryOrgCode);
                    lblSupervisoryOrgShortText.Text = candidatInfo.SupervisoryOrgShortText;
                    lblSupervisoryOrgDesc.Text = candidatInfo.SupervisoryOrgDesc;
                    lblReasonForHire.Text = candidatInfo.ReasonForHire;
                    lblJobTitle.Text = candidatInfo.JobTitle;
                    lblNoOfOpenings.Text = Convert.ToString(candidatInfo.NoOfOpenings);

                    HtmlTableCell tdTimeToFill = (HtmlTableCell)e.Item.FindControl("tdAvailabilityDate");
                    if (chkColumnList.Items.FindByValue("AvailabilityDate").Selected)
                    {
                        lblAvailabilityDate.Text = TimeToFill(candidatInfo);
                        tdTimeToFill.Visible = true;
                        lblAvailabilityDate.Visible = true;
                    }

                    HtmlTableCell tdFinalHiredDate = (HtmlTableCell)e.Item.FindControl("tdFinalHiredDate");
                    if (chkColumnList.Items.FindByValue("FinalHiredDate").Selected)
                    {
                        lblFinalHiredDate.Text = (MiscUtil.IsValidDate(candidatInfo.FinalHiredDate) ? candidatInfo.FinalHiredDate.ToShortDateString() : string.Empty);
                        tdFinalHiredDate.Visible = true;
                        lblFinalHiredDate.Visible = true;
                    }

                    lblJobRestrictions.Text = candidatInfo.JobRestrictions;
                    lblJobFamily.Text = candidatInfo.JobFamily;
                    lblJobProfiles.Text = candidatInfo.JobProfile;
                    lblCity.Text = candidatInfo.City;
                    lblTimeType.Text = candidatInfo.TimeType;
                    lblWorkerType.Text = candidatInfo.WorkerType;
                    lblWorkerSubType.Text = candidatInfo.WorkerSubType;
                    lblCompany.Text = candidatInfo.Company;
                    lblCostCenter.Text = candidatInfo.CostCenter;
                    lblRegion.Text = candidatInfo.Region;
                    lblInternationalEmployeeStatus.Text = candidatInfo.InternationalEmployeeStatus;
                    lblPayGroup.Text = candidatInfo.PayGroup;
                    lblPayGroupGrade.Text = candidatInfo.PayGroupGrade;
                    lblMaximumFTE.Text = Convert.ToString(candidatInfo.MaximumFTE);
                    lblNotes.Text = candidatInfo.Notes;

                    HideListViewItem(e.Item);
                }
            }
        }

        protected void lsvCandidateList_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Sort")
                {
                    LinkButton lnkbutton = (LinkButton)e.CommandSource;
                    if (txtSortColumn.Text == lnkbutton.ID)
                    {
                        if (txtSortOrder.Text == "ASC") txtSortOrder.Text = "DESC";
                        else txtSortOrder.Text = "ASC";
                    }
                    else
                    {
                        txtSortColumn.Text = lnkbutton.ID;
                        txtSortOrder.Text = "ASC";
                    }
                    if (SortColumn.Text == string.Empty || SortColumn.Text != e.CommandArgument.ToString())
                        SortOrder.Text = "asc";
                    else SortOrder.Text = SortOrder.Text == "asc" ? "desc" : "asc";
                    SortColumn.Text = e.CommandArgument.ToString();
                    odsCandidateList.SelectParameters["SortOrder"].DefaultValue = SortOrder.Text.ToString();
                }
            }
            catch
            {
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            hdnScrollPos.Value = "0";
            string ColumnOption = "";
            foreach (ListItem item in chkColumnList.Items)
            {
                if (item.Selected == true) ColumnOption += item.Value + "#";

            }
            System.Web.HttpCookie aCookie = new System.Web.HttpCookie(CookieName);
            aCookie.Value = ColumnOption.ToString();
            Response.Cookies.Add(aCookie);
            lsvCandidateList.Items.Clear();
            SetDataSourceParameters();
            ASP.controls_pagercontrol_ascx PagerControls = (ASP.controls_pagercontrol_ascx)this.lsvCandidateList.FindControl("pagerControl");
            if (PagerControls != null)
            {
                DataPager pager = (DataPager)PagerControls.FindControl("pager");
                if (pager != null)
                {
                    if (pager.Page.IsPostBack)
                        pager.SetPageProperties(0, pager.MaximumRows, true);
                }
            }
            divExportButton.Visible = false;
            BindList();
            HideListViewHeaderItem();

            string pagesize = "";
            if (txtSortColumn.Text == "") txtSortColumn.Text = "lnkBtnName";
            if (txtSortOrder.Text == "") txtSortOrder.Text = "ASC";
            PlaceUpDownArrow();
            pagesize = (Request.Cookies["CandidateReportRowPerPage"] == null ? "" : Request.Cookies["CandidateReportRowPerPage"].Value); ;

            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvCandidateList.FindControl("pagerControl");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)PagerControl.FindControl("pager");
                if (pager != null)
                {
                    pager.PageSize = Convert.ToInt32(pagesize == "" ? "20" : pagesize);
                    if (pager.Page.IsPostBack)
                        pager.SetPageProperties(0, pager.MaximumRows, true);
                }
            }
            System.Web.UI.HtmlControls.HtmlTableCell tdpager = (System.Web.UI.HtmlControls.HtmlTableCell)lsvCandidateList.FindControl("tdpager");
            if (tdpager != null)
            {
                tdpager.ColSpan = chkColumnList.Items.Cast<ListItem>().Where(x => x.Selected).Count();
            }
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            Clear();
            divExportButton.Visible = false;
        }
        #endregion

        #region Methods
        private void HideListViewHeaderItem()
        {
            foreach (ListItem chkItem in chkColumnList.Items)
            {
                HtmlTableCell thHeaderTitle = (HtmlTableCell)lsvCandidateList.FindControl("th" + chkItem.Value);
                if (thHeaderTitle != null)
                {
                    if (chkItem.Selected)
                    {
                        thHeaderTitle.Visible = true;

                    }
                    else
                    {
                        thHeaderTitle.Visible = false;
                    }
                }
            }
        }

        private void PlaceUpDownArrow()
        {
            try
            {
                LinkButton lnk = (LinkButton)lsvCandidateList.FindControl(txtSortColumn.Text);
                HtmlTableCell im = (HtmlTableCell)lnk.Parent;
                im.EnableViewState = false;
                im.Attributes.Add("class", (txtSortOrder.Text == "ASC" ? "Ascending" : "Descending"));
            }
            catch
            {
            }

        }

        private void HideListViewItem(ListViewItem itm)
        {
            foreach (ListItem chkItem in chkColumnList.Items)
            {
                HtmlTableCell thHeaderTitle = (HtmlTableCell)itm.FindControl("td" + chkItem.Value);
                if (thHeaderTitle != null)
                {
                    if (chkItem.Selected)
                    {
                        thHeaderTitle.Visible = true;
                    }
                    else
                    {
                        thHeaderTitle.Visible = false;
                    }
                }
            }
        }

        private void Clear()
        {
            divCandidateList.Visible = false;
            ClearSearchCriteria();
        }

        private void ClearSearchCriteria()
        {
            lblMessage.Text = string.Empty;
            wdcCandidatesAdded.ClearRange();
            txtCity.Text = "";
        }

        private void SetDataSourceParameters()
        {
            if (SortOrder.Text == string.Empty && SortColumn.Text == string.Empty)
            {
                SortOrder.Text = "asc";
                SortColumn.Text = "[C].[FirstName]";
            }

            odsCandidateList.SelectParameters["MemberId"].DefaultValue = Convert.ToString(CurrentMember.Id);
            odsCandidateList.SelectParameters["SortOrder"].DefaultValue = SortOrder.Text.ToString();
            odsCandidateList.SelectParameters["sortExpression"].DefaultValue = SortOrder.Text.ToString();
            odsCandidateList.SelectParameters["maximumRows"].DefaultValue = "20";
            odsCandidateList.SelectParameters["startRowIndex"].DefaultValue = "0";
        }

        private void BindList()
        {
            bool chkColumnListcount = false;
            foreach (ListItem items in chkColumnList.Items)
            {
                if (items.Selected)
                {
                    chkColumnListcount = true;
                    break;
                }
            }
            if (chkColumnListcount)
            {
                this.lsvCandidateList.DataSourceID = "odsCandidateList";
                this.lsvCandidateList.DataBind();
                divCandidateList.Visible = true;

            }
            else
            {
                divCandidateList.Visible = false;
                MiscUtil.ShowMessage(lblMessage, "Please Select atleast One column", false);
            }
        }

        string TimeToFill(CandidateJoiningReport candidatInfo)
        {
            if (((candidatInfo.FinalHiredDate == DateTime.MinValue) || (candidatInfo.FinalHiredDate == null)) || ((candidatInfo.ActivationDate == DateTime.MinValue) || (candidatInfo.ActivationDate == null)))
                return "";
            else
                return MiscUtil.FindDateDiffFrmEndtoStart(candidatInfo.FinalHiredDate, candidatInfo.OpenDate);
        }
        #endregion
    }
}
#endregion
