﻿<%---------------------------------------------------------------------------------------------------------------------------------------------
    FileName: InternalReferral.aspx.cs
    Description: This page is to access Internal Portal of IJP
    Created By: Kanchan Yeware
    Created On: 20-Sept-2016
    Modification Log:   
------------------------------------------------------------------------------------------------------------------------------------------- 
--%>
<%@ Page Language="C#" MasterPageFile="~/CandidatePortal/Portal.master" 
AutoEventWireup="true" CodeFile="InternalReferral.aspx.cs" EnableEventValidation="false" 
Inherits="TPS360.Web.UI.InternalReferral" Title="Internal Referral Portal" %>

<%@ Register Src="~/Controls/RegistrationInternal.ascx" TagName="regI" TagPrefix="uc1" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphHomeMasterTitle" runat="Server">
<asp:Label ID="titleContainer"  runat ="server" Text ="Refer Internal Candidate for job opening" ></asp:Label>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphHomeMaster" runat="Server">
    <div style="padding: 18px;">
        <asp:UpdatePanel ID="pnlRegistrationInformation" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div id="divRegInternal" runat="server">              
                    <uc1:regI ID="rgInternal" runat="server" />
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>

