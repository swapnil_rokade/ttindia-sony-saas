﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: InternalReferral.cs
    Description: This page is to access Internal Portal of IJP
    Created By: Kanchan Yeware
    Created On: 20-Sept-2016
    Modification Log:   
------------------------------------------------------------------------------------------------------------------------------------------- 
*/
#region Namespaces
using System;
using TPS360.Common.Helper;
using TPS360.Web.UI.Helper;
using System.Web;
#endregion
#region InternalReferral
namespace TPS360.Web.UI
{
    public partial class InternalReferral : System.Web.UI.Page
    {
        #region Veriables
        private WebHelper _helper;
        protected WebHelper Helper
        {
            get
            {
                if (_helper == null)
                {
                    _helper = new WebHelper(this);
                }

                return _helper;
            }
        }
        #endregion

        #region Properties
        private int JobId
        {
            get
            {
                if (!TPS360.Common.Helper.StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_JOB_ID]))
                {
                    return Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_JOB_ID]);
                }
                else return 0;

            }
        }
        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            rgInternal.Role = ContextConstants.ROLE_CANDIDATE;
            if (!IsPostBack)
            {
                if (JobId > 0)
                {
                    TPS360.Common.BusinessEntities.JobPosting job = new TPS360.Common.BusinessEntities.JobPosting();
                    TPS360.BusinessFacade.IFacade facade = new TPS360.BusinessFacade.Facade();
                    job = facade.GetJobPostingById(JobId);
                    if (job != null)
                        titleContainer.Text = titleContainer.Text;  // + " - " + job.JobTitle -- Change by Kanchan Yeware -- 20-Sept-2016
                }
            }

        }

        #endregion
    }
}
#endregion
