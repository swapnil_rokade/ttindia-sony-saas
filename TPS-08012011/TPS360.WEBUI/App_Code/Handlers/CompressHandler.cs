﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using TPS360.Common.BusinessEntities;

public abstract class CompressHandler : IHttpHandler
{
    public abstract void ProcessRequest(HttpContext context);

    public static Stream Compress(HttpContext context, HttpResponse response, Stream stream)
    {
        var encoding = context.Request.Headers["Accept-Encoding"];

        if (encoding.IsNotNullOrEmpty())
        {
            var gzip = "gzip";
            var deflate = "deflate";
            var header = "Content-Encoding";

            if (encoding.ContainsIgnoreCase(gzip))
            {
                response.AddHeader(header, gzip);
                stream = new System.IO.Compression.GZipStream(stream, System.IO.Compression.CompressionMode.Compress);
            }
            else if (encoding.ContainsIgnoreCase(deflate))
            {
                response.AddHeader(header, deflate);
                stream = new System.IO.Compression.DeflateStream(stream, System.IO.Compression.CompressionMode.Compress);
            }
        }

        return stream;
    }


    public virtual bool IsReusable
    {
        get
        {
            return true;
        }
    }
}