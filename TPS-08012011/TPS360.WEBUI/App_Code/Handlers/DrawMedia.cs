﻿using System;
using System.Web;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TPS360.Common.BusinessEntities;
using System.Text.RegularExpressions;
using TPS360.Web.UI;
using System.Drawing;
using System.Drawing.Imaging;
using TPS360.Common.Helper;

public class DrawMedia : BaseHandler
{
    public override void ProcessRequest(HttpContext context)
    {
        try
        {
            Size proposedSize;
            bool showThumb = bool.Parse(Helper.Url.SecureUrl[UIConstants.SHOW_THUMB]);

            string imgUrl = Helper.Url.SecureUrl[UrlConstants.PARAM_IMG_FILE];

            byte[] binaryContent = null;

            ImageFormat format = GetFormat(Path.GetExtension(imgUrl));

            using (Bitmap bmp = new Bitmap(imgUrl))
            {
                if (showThumb)
                {
                    proposedSize = MiscUtil.CalculateThumbSize(bmp.Size);
                }
                else
                {
                    proposedSize = bmp.Size;
                }

                using (Bitmap thumbnail = new Bitmap(bmp, proposedSize))
                {
                    using (MemoryStream msProposed = new MemoryStream())
                    {
                        thumbnail.Save(msProposed, format);

                        binaryContent = new byte[msProposed.Length];
                        msProposed.Seek(0, SeekOrigin.Begin);
                        msProposed.Read(binaryContent, 0, binaryContent.Length);
                    }
                }
            }

            if (binaryContent != null)
            {
                context.Response.Clear();
                context.Response.ContentType = GetContentType(format);
                context.Response.OutputStream.Write(binaryContent, 0, binaryContent.Length);
                context.Response.End();
            }
        }
        catch (Exception ex)
        {

        }
    }

    private static ImageFormat GetFormat(string fileExtension)
    {
        fileExtension = StringHelper.ToLower(fileExtension).Substring(1);

        ImageFormat format;

        if (StringHelper.IsEqual(fileExtension, "bmp"))
        {
            format = System.Drawing.Imaging.ImageFormat.Bmp;
        }
        else if (StringHelper.IsEqual(fileExtension, "emf"))
        {
            format = System.Drawing.Imaging.ImageFormat.Emf;
        }
        else if (StringHelper.IsEqual(fileExtension, "exif"))
        {
            format = System.Drawing.Imaging.ImageFormat.Exif;
        }
        else if (StringHelper.IsEqual(fileExtension, "gif"))
        {
            format = System.Drawing.Imaging.ImageFormat.Gif;
        }
        else if (StringHelper.IsEqual(fileExtension, "ico"))
        {
            format = System.Drawing.Imaging.ImageFormat.Icon;
        }
        else if (StringHelper.IsEqual(fileExtension, "png"))
        {
            format = System.Drawing.Imaging.ImageFormat.Png;
        }
        else if (StringHelper.IsEqual(fileExtension, "tif"))
        {
            format = System.Drawing.Imaging.ImageFormat.Tiff;
        }
        else if (StringHelper.IsEqual(fileExtension, "wmf"))
        {
            format = System.Drawing.Imaging.ImageFormat.Wmf;
        }
        else
        {
            format = System.Drawing.Imaging.ImageFormat.Jpeg;
        }

        return format;
    }

    private static string GetContentType(ImageFormat format)
    {
        if (format == ImageFormat.Gif)
        {
            return "image/jpeg";
        }
        else if (format == ImageFormat.Gif)
        {
            return "image/gif";
        }
        else if (format == ImageFormat.Bmp)
        {
            return "image/bmp";
        }
        else if (format == ImageFormat.Png)
        {
            return "image/x-png";
        }
        else if (format == ImageFormat.Tiff)
        {
            return "image/tiff";
        }
        else if (format == ImageFormat.Emf)
        {
            return "image/x-emf";
        }
        else if (format == ImageFormat.Wmf)
        {
            return "image/x-wmf";
        }
        else if (format == ImageFormat.Icon)
        {
            return "image/ico";
        }
        else
        {
            return "image/jpeg";
        }
    }
}
