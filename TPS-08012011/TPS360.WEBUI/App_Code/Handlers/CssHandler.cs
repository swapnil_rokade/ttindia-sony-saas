﻿using System;
using System.Web;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TPS360.Common.BusinessEntities;
using System.Text.RegularExpressions;
using TPS360.Web.UI;

public class CssHandler : CompressHandler
{
    public const string ContentType = "text/css";
    //private static readonly Regex ex = new Regex(@"(url\('(?<path>(\w|\.|:|/)*)'\)*)|(src=""(?<path>(\w|\.|:|/)*)"")", RegexOptions.Compiled | RegexOptions.Multiline);
    private static readonly Regex ex = new Regex(@"url\((\""|\')?(?<path>(.*))?(\""|\')?\)");

    public override void ProcessRequest(HttpContext context)
    {
        // Collect the query parameters
        var theme = context.Request.QueryString["t"];
        var fileSet = context.Request.QueryString["fileSet"];
        var directory = context.Request.QueryString["d"];
        var file = context.Request.QueryString["f"];
       

        // Identify the response as CSS
        var response = context.Response;
        response.ContentType = ContentType;

        // Wrap the response in a compression stream
        var output = response.OutputStream;
        //output = Compress(context, response, output);

        // Process any query parameters
        ProcessByTheme(context, theme, output);

        string files = System.Configuration.ConfigurationManager.AppSettings[fileSet];

        if (files != null)
        {
            string[] fileNames = files.Split(',');

            if ((fileNames != null) && (fileNames.Length > 0))
            {
                //Write each files in the response
                for (int i = 0; i < fileNames.Length; i++)
                {
                    //context.Response.Write(System.IO.File.ReadAllText(context.Server.MapPath(fileNames[i])));
                    ProcessByFile(context, fileNames[i], output);
                }
            }
        }

        ProcessByFile(context, file, output);
        //ProcessByDirectory(context, directory, output);

        //Cache our work on the client
        
        context.Response.Cache.SetCacheability(HttpCacheability.Public);
        context.Response.Cache.SetExpires(DateTime.Now.AddDays(30));
        context.Response.Cache.SetMaxAge(TimeSpan.FromDays(30));
        context.Response.Cache.AppendCacheExtension("must-revalidate, proxy-revalidate");

    }

    private static void ProcessByTheme(HttpContext context, string theme, Stream output)
    {
        if (theme.IsNotNullOrEmpty())
        {
            var path = context.Server.MapPath("~/App_Themes/{0}".Fill(theme));
            CombineFromDirectory(context, output, path);
        }
    }

    private static void ProcessByFile(HttpContext context, string file, Stream output)
    {
        if (file.IsNotNullOrEmpty())
        {
            try
            {
                var path = context.Server.MapPath(file);
                Combine(context, output, MapPathReverse(path));
            }
            finally
            { 
            }
        }
    }

    private static void ProcessByDirectory(HttpContext context, string directory, Stream output)
    {
        if (directory.IsNotNullOrEmpty())
        {
            directory = context.Server.MapPath(directory);
            CombineFromDirectory(context, output, directory);
        }
    }

    public static string MapPathReverse(string path)
    {
        var context = HttpContext.Current;

        if (context != null)
        {
            if (context.Request.PhysicalApplicationPath != null)
            {
                var root = context.Request.PhysicalApplicationPath.TrimEnd('\\');
                var relative = path.Replace(root, String.Empty);
                var clean = relative.Replace('\\', '/');
                return clean.Insert(0, "~");
            }
        }

        return String.Empty;
    }

    private static void CombineFromDirectory(HttpContext context, Stream output, string directory)
    {
        var directories = Directory.GetDirectories(directory, "*", SearchOption.AllDirectories).ToList();
        directories.Add(directory);

        for (var p = 0; p < directories.Count; p++)
        {
            directories[p] = MapPathReverse(directories[p]);
        }

        Combine(context, output, directories);
    }

    private static void Combine(HttpContext context, Stream output, string relativePath)
    {
        Combine(context, output, new[] { relativePath });
    }

    private static void Combine(HttpContext context, Stream output, params string[] relativePaths)
    {
        Combine(context, output, relativePaths.ToList());
    }

    private static void Combine(HttpContext context, Stream output, IEnumerable<string> relativePaths)
    {
        using (var sw = new StreamWriter(output))
        {
            // HttpRuntime is faster than HttpContext.Current
            var cache = HttpRuntime.Cache;

            foreach (var relativePath in relativePaths)
            {
                // Check the cache for this relative path first
                if (cache[relativePath] == null)
                {
                    var sb = new StringBuilder();
                    var path = context.Server.MapPath(relativePath);
                    var files = new List<string>();
                    var pathAsFile = Path.GetFileName(path);

                    if (!pathAsFile.Contains('.'))
                    {
                        files.AddRange(Directory.GetFiles(path));
                    }
                    else
                    {
                        // Already a file
                        files.Add(path);
                    }

                    foreach (var file in files)
                    {
                        FileInfo fileInfo = new FileInfo(file);
                        if (fileInfo.Extension.ToLower() == ".css")
                        {
                            // Read the file and minify it
                            var minified = File.ReadAllText(file).CssMinify();

                            // Write the file to a temporary builder
                            sb.Append(minified);
                        }
                    }

                    // Combine the string content

                    var cssContent = sb.ToString();
                    var theme = context.Request.QueryString["t"];

                    cssContent = ex.Replace(cssContent, new MatchEvaluator(delegate(Match m)
                    {
                        string imgPath = m.Groups["path"].Value;

                        if (!imgPath.StartsWith("http://"))
                        {
                            if (relativePath.ContainsIgnoreCase("App_Themes/{0}".Fill(theme)))
                            {
                                return "url('" + MiscUtil.AppendSuffixAndPrefix(imgPath, UrlConstants.CssSuffix, UrlConstants.ApplicationBaseUrl + "App_Themes/{0}/".Fill(theme)) + "')";
                            }
                            else
                            {
                                imgPath = imgPath.Replace("../", string.Empty);
                                return "url('" + MiscUtil.AppendSuffixAndPrefix(imgPath, UrlConstants.CssSuffix, UrlConstants.CssPrefix) + "')";
                            }
                        }
                        else
                        {
                            return "url('" + imgPath + "')";
                        }
                    }));

                    // Create a cache dependency based on the files we just combined
                    var dependency = new System.Web.Caching.CacheDependency(files.ToArray());

                    // Cache the content by path name, so we don't combine it twice
                    cache.Insert(relativePath, cssContent, dependency);
                    sw.Write(cssContent);
                }

                else
                {
                    // It's already cached, so serve it up
                    var existing = cache[relativePath].ToString();
                    sw.Write(existing);
                }
            }
        }
    }
}
