﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.Security;
using System.Web.Services.Protocols;
using System.Web.UI.WebControls;
using TPS360.BusinessFacade;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using System.Web.Services;
using TPS360.Web.UI;
using System.Data;
using System.ComponentModel;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]

public class ResumeParserService : WebServiceBase
{

    #region Contractor

    public ResumeParserService()
    {

    }

    #endregion

    #region Web Method

    #region Activity

    [WebMethod]
    public List<Activity> Activity_GetAllActivityByResourceID(int resourceID)
    {
        return (List<Activity>)Facade.GetAllActivityByResourceId(resourceID);
    }

    [WebMethod]
    public Int32 Activity_Create(bool allDayEvent, string activityDescription, Int32 duration, string location, DateTime startDateTimeUtc, string subject, bool enableReminder, Int32 reminderInterval, Int32 showTimeAs, Int32 importance, Int32 status, Int32 recurrenceID, string resourceName)
    {
        Activity activity = new Activity();
        activity.AllDayEvent = allDayEvent;
        activity.ActivityDescription = activityDescription;
        activity.Duration = duration;
        activity.Location = location;
        activity.StartDateTimeUtc = startDateTimeUtc;
        activity.Subject = subject;
        activity.EnableReminder = enableReminder;
        activity.ReminderInterval = reminderInterval;
        activity.ShowTimeAs = showTimeAs;
        activity.Importance = importance;
        activity.Status = status;
        activity.RecurrenceID = recurrenceID;
        activity.ResourceName = resourceName;

        return Facade.AddActivity(activity).Id;
    }

    [WebMethod]
    public bool ActivityResource_Create(Int32 activityId, Int32 resourceId)
    {
        ActivityResource activityResource = new ActivityResource();
        activityResource.ActivityID = activityId;
        activityResource.ResourceID = resourceId;

        Facade.AddActivityResource(activityResource);
        return true;
    }

    #endregion

    #region Authentication and MemberShip

    [WebMethod]
    public bool IsServiceAvailable()
    {
        return true;
    }

    [WebMethod]
    public string IsAuthenticated(string strUserName, string strPassWord)
    {
        string memberInformation = string.Empty;
        bool userIsValid = Membership.ValidateUser(strUserName, strPassWord);

        if (userIsValid)
        {
            MembershipUser user = Membership.GetUser(strUserName, true);
            string role = (Roles.GetRolesForUser(strUserName)).GetValue(0).ToString();

            if (role != string.Empty)
            {
                if (Roles.IsUserInRole(strUserName, ContextConstants.ROLE_ADMIN) || Roles.IsUserInRole(strUserName, ContextConstants.ROLE_EMPLOYEE))
                {
                    Member member = Facade.GetMemberByUserId((Guid)user.ProviderUserKey);
                    memberInformation = member.Id.ToString() + "," + member.FirstName + " " + member.LastName + "," + member.PrimaryEmail + "," + role;
                }
                else
                {
                    memberInformation = "Role not Authorized";
                }
            }
            else
            {
                memberInformation = "Role not Assign";
            }
        }
        else
        {
            memberInformation = "User Not Exits";
        }
        return memberInformation;
    }

    [WebMethod]
    public Guid CreateMemberShipUser(string _email, string _password, string _role)
    {
        Guid UserId = Guid.Empty;
        MembershipUser memberShipUser = Membership.CreateUser(_email, _password, _email);
        if (!string.IsNullOrEmpty(memberShipUser.UserName))
        {
            Roles.AddUserToRole(_email, _role);
            UserId = (Guid)memberShipUser.ProviderUserKey;
        }
        return UserId;
    }

    [WebMethod]
    public Guid GetMemberShipUser(string _email)
    {
        Guid userId = Guid.Empty;
        //GuidConverter gc = new GuidConverter();
        MembershipUser memberShipUser = Membership.GetUser(_email, true);
        //userId = (Guid)gc.ConvertFrom(memberShipUser.ProviderUserKey);
        if (memberShipUser != null)
        {
            userId = (Guid)memberShipUser.ProviderUserKey;
        }
        return userId;
    }

    #endregion

    #region Company

    [WebMethod]
    public Int32 GetCompanyIdByName(string companyName)
    {
        return Facade.GetCompanyIdByName(companyName);
    }

    [WebMethod]
    public int Company_Create(string companyName,string address1,string address2,string city,string stateName,string zip,string countryName,string webAddress,string companyInformation,string officePhone,string fax,string primaryEmail,string secondaryEmail, Int32 creatorId , CompanyStatus companyStatus, bool isSelfRegistration)
    {
        Company _company = new Company();
        _company.CompanyName = companyName;
        _company.Address1 = address1;
        _company.Address2 = address2;
        _company.City = city;
        _company.StateId = GetStateIdByStateName(stateName);
        _company.Zip = zip;
        _company.CountryId = GetCountryIdByCountryName(countryName);
        _company.WebAddress = webAddress;
        _company.CompanyLogo = string.Empty;
        _company.CompanyInformation = companyInformation;
        _company.IndustryType = 0;
        _company.CompanySize = 0;
        _company.OwnerType = 0;
        _company.TickerSymbol = string.Empty;
        _company.OfficePhone = officePhone;
        _company.OfficePhoneExtension = string.Empty;
        _company.TollFreePhone = string.Empty;
        _company.TollFreePhoneExtension = string.Empty;
        _company.FaxNumber = fax;
        _company.PrimaryEmail = primaryEmail;
        _company.SecondaryEmail = secondaryEmail;
        _company.EINNumber = string.Empty;
        _company.AnnualRevenue = string.Empty;
        _company.NumberOfEmployee = 0;
        _company.CompanyType = string.Empty;
        _company.IsEndClient = false;
        _company.IsTier1Client = false;
        _company.IsTier2Client = false;
        _company.IsRemoved = false;
        _company.CreatorId = creatorId;
        _company.IsVolumeHireClient = false;

        return Facade.AddCompany(_company, companyStatus, isSelfRegistration).Id;
    }

    [WebMethod]
    public bool CompanyContact_Create(int companyId,string title,string email,string firstName,string lastName,string address1,string address2,string city,string stateName,string countryName,string zip,string officePhone,string mobile,string directNumber,string fax)
    {
        try
        {
            CompanyContact _companyContact = new CompanyContact();
            _companyContact.CompanyId = companyId;
            _companyContact.IsPrimaryContact = true;
            _companyContact.Title = title;
            _companyContact.NoBulkEmail = true;
            _companyContact.Email = email;
            _companyContact.FirstName = firstName;
            _companyContact.LastName = lastName;
            _companyContact.Address1 = address1;
            _companyContact.Address2 = address2;
            _companyContact.City = city;
            _companyContact.StateId = GetStateIdByStateName(stateName);
            _companyContact.CountryId = GetCountryIdByCountryName(countryName);
            _companyContact.ZipCode = zip;
            _companyContact.OfficePhone = officePhone;
            _companyContact.OfficePhoneExtension = string.Empty;
            _companyContact.MobilePhone = mobile;
            _companyContact.DirectNumber = directNumber;
            _companyContact.DirectNumberExtension = directNumber;
            _companyContact.ContactRemarks = string.Empty;
            _companyContact.Fax = fax;
            _companyContact.IsOwner = false;
            _companyContact.OwnershipPercentage = 0.0M;
            _companyContact.ResumeFile = string.Empty;
            _companyContact.EthnicGroupLookupId = 0;
            _companyContact.DivisionLookupId = 0;

            Facade.AddCompanyContact(_companyContact);
            return true;
        }
        catch
        {
            return false;
        }
    }

    [WebMethod]
    public bool CompanyManager_Create(int companyId,int memberId)
    {
        try
        {
            CompanyAssignedManager _companyAssignedManager = new CompanyAssignedManager();
            _companyAssignedManager.CompanyId = companyId;
            _companyAssignedManager.MemberId = memberId;

            Facade.AddCompanyAssignedManager(_companyAssignedManager);
            return true;
        }
        catch
        {
            return false;
        }
    }

    #endregion

    #region Country and State

    [WebMethod]
    public List<Country> GetAllCountry()
    {
        return (List<Country>)Facade.GetAllCountry();
    }

    [WebMethod]
    public List<State> GetAllStateByCountryId(Int32 countryId)
    {
        return (List<State>)Facade.GetAllStateByCountryId(countryId);
    }

    [WebMethod]
    public List<State> GetAllStateData()
    {
        List<State> stateLookupList = (List<State>)Facade.GetAllState();
        return stateLookupList;
    }

    [WebMethod]
    public Int32 GetCountryIdByCountryName(string countryName)
    {
        try
        {
            return Facade.GetCountryIdByCountryName(countryName);
        }
        catch
        {
            return 0;
        }
    }

    [WebMethod]
    public Int32 GetStateIdByStateName(string stateName)
    {
        try
        {
            return Facade.GetStateIdByStateName(stateName);
        }
        catch
        {
            return 0;
        }
    }

    #endregion

    #region GenericLookup

    [WebMethod]
    public List<GenericLookup> GetLookupData(int lookupType)
    {
        List<GenericLookup> genericLookupList = (List<GenericLookup>)Facade.GetAllGenericLookupByLookupType((LookupType)lookupType);
        return genericLookupList;
    }

    [WebMethod]
    public List<GenericLookup> GetLookupDataByLookupIdAndName(int lookupType, string lookupName)
    {
        List<GenericLookup> genericLookupList = (List<GenericLookup>)Facade.GetAllGenericLookupByLookupTypeAndName((LookupType)lookupType, lookupName);
        return genericLookupList;
    }

    #endregion

    #region HTTP Document Upload

    [WebMethod]
    public bool UploadResume(int memberId, string strFileName, string strFileType, byte[] byteResume)
    {
        try
        {
            string strFilePath = GetMemberDocumentPathForResumeParser(memberId, strFileName, strFileType, false);
            using (FileStream fsOut = new FileStream(strFilePath, FileMode.Create, FileAccess.ReadWrite))
            {
                fsOut.Write(byteResume, 0, byteResume.Length);
                return true;
            }
        }
        catch
        {
            return false;
        }
    }

    [WebMethod]
    public bool UploadEmailAttachment(int memberEmailId, string strFileName, byte[] byteResume)
    {
        try
        {
            string strFilePath = GetMemberDocumentPathForEmailAttachmen(memberEmailId, strFileName, false);
            using (FileStream fsOut = new FileStream(strFilePath, FileMode.Create, FileAccess.ReadWrite))
            {
                fsOut.Write(byteResume, 0, byteResume.Length);
                return true;
            }
        }
        catch
        {
            return false;
        }
    }

    #endregion

    #region JobPosting

    [WebMethod]
    public DataTable GetAllJobPostingListByStatus(int status)
    {
        return Facade.GetAllJobPostingListByStatus(status);
    }

    [WebMethod]
    public Int32 JobPostingCreate(JobPosting jobPosting)
    {
        Int32 jobPostingId = 0;
        try
        {
            jobPostingId = Facade.AddJobPosting(jobPosting).Id;
        }
        catch
        {

        }
        return jobPostingId;
    }

    //[WebMethod]
    //public void JobPostingSkillCreate(string[] _jobPostingSkillSet, Int32 _JobPostingId, Int32 _experienceReq)
    //{
    //    if (_JobPostingId > 0)
    //    {
    //        foreach (string jobSkill in _jobPostingSkillSet)
    //        {
    //            Int32 skillId = 0;
    //            skillId = GetSkillIdByName(jobSkill);
    //            if (skillId > 0)
    //            {
    //                Int32 jobPostingSkillSetId = 0;
    //                JobPostingSkillSet jobPostingSkillSet = new JobPostingSkillSet();
    //                if (jobPostingSkillSet != null)
    //                {
    //                    jobPostingSkillSet = Facade.GetJobPostingSkillSetByJobPostingIdAndSkillId(_JobPostingId, skillId);
    //                    if (jobPostingSkillSet != null)
    //                    {
    //                        jobPostingSkillSetId = jobPostingSkillSet.Id;
    //                    }
    //                    else
    //                    {
    //                        jobPostingSkillSetId = 0;
    //                    }
    //                }
    //                else
    //                {
    //                    jobPostingSkillSetId = 0;
    //                }
    //                if (jobPostingSkillSetId <= 0)
    //                {
    //                    JobPostingSkillSet jobPostingSkillSetNew = new JobPostingSkillSet();
    //                    jobPostingSkillSetNew.JobPostingId = _JobPostingId;
    //                    jobPostingSkillSetNew.SkillId = skillId;
    //                    jobPostingSkillSetNew.ProficiencyLookupId = 0;
    //                    jobPostingSkillSetNew.NoOfYearsExp = _experienceReq;
    //                    jobPostingSkillSetNew.AssessmentRequired = false;

    //                    Facade.AddJobPostingSkillSet(jobPostingSkillSetNew);
    //                }
    //            }
    //        }
    //    }
    //}

    #endregion

    #region Member

    [WebMethod]
    public int Member_Create(Member _member)
    {
        return Facade.AddMember(_member).Id;
    }

    [WebMethod]
    public bool Member_Update(Member _member)
    {
        try
        {
            //_member.Id = Facade.GetMemberByUserId(_member.UserId).Id;
            Facade.UpdateMember(_member);
            return true;
        }
        catch
        {
            return false;
        }
    }

    [WebMethod]
    public Member Member_GetByUserId(Guid _userId)
    {
        Member member = new Member();
        if (member != null)
        {
            member = Facade.GetMemberByUserId(_userId);
        }
        return member;
    }

    #endregion

    #region MemberDetail

    [WebMethod]
    public bool MemberDetail_Create(MemberDetail _memberDetail)
    {
        try
        {
            Facade.AddMemberDetail(_memberDetail);
            return true;
        }
        catch
        {
            return false;
        }
    }

    [WebMethod]
    public bool MemberDetail_Update(MemberDetail _memberDetail)
    {
        try
        {
            _memberDetail.Id = Facade.GetMemberDetailByMemberId(_memberDetail.MemberId).Id;
            Facade.UpdateMemberDetail(_memberDetail);
            return true;
        }
        catch
        {
            return false;
        }
    }

    #endregion

    #region MemberExtendedInformation

    [WebMethod]
    public bool MemberExtendedInformation_Create(MemberExtendedInformation _memberExtendedInformation)
    {
        try
        {
            Facade.AddMemberExtendedInformation(_memberExtendedInformation);
            return true;
        }
        catch
        {
            return false;
        }
    }

    [WebMethod]
    public bool MemberExtendedInformation_Update(MemberExtendedInformation _memberExtendedInformation)
    {
        try
        {
            _memberExtendedInformation.Id = Facade.GetMemberExtendedInformationByMemberId(_memberExtendedInformation.MemberId).Id;
            Facade.UpdateMemberExtendedInformation(_memberExtendedInformation);
            return true;
        }
        catch
        {
            return false;
        }
    }

    #endregion

    #region MemberDocument

    [WebMethod]
    public bool MemberDocument_Create(MemberDocument _memberDocument)
    {
        try
        {
            Facade.AddMemberDocument(_memberDocument);
            return true;
        }
        catch
        {
            return false;
        }
    }

    [WebMethod]
    public bool MemberDocument_Update(MemberDocument _memberDocument)
    {
        try
        {
            _memberDocument.Id = Facade.GetMemberDocumentByMemberIdAndFileName(_memberDocument.MemberId, _memberDocument.FileName).Id;
            Facade.UpdateMemberDocument(_memberDocument);
            return true;
        }
        catch
        {
            return false;
        }
    }

    #endregion

    #region MemberObjectiveAndSummary

    [WebMethod]
    public bool MemberObjectiveAndSummary_Create(MemberObjectiveAndSummary _memberObjectiveAndSummary)
    {
        try
        {
            Facade.AddMemberObjectiveAndSummary(_memberObjectiveAndSummary);
            return true;
        }
        catch
        {
            return false;
        }
    }

    [WebMethod]
    public bool MemberObjectiveAndSummary_Update(MemberObjectiveAndSummary _memberObjectiveAndSummary)
    {
        try
        {
            _memberObjectiveAndSummary.Id = Facade.GetMemberObjectiveAndSummaryByMemberId(_memberObjectiveAndSummary.MemberId).Id;
            Facade.UpdateMemberObjectiveAndSummary(_memberObjectiveAndSummary);
            return true;
        }
        catch
        {
            return false;
        }
    }

    #endregion

    #region Member Manager

    [WebMethod]
    public void MemberManager_Create(Int32 _memberId, Int32 _managerId)
    {
        if (_memberId > 0)
        {
            if (string.IsNullOrEmpty(Facade.GetMemberManagerById(_memberId).Id.ToString()))
            {
                MemberManager memberManager = new MemberManager();
                memberManager.MemberId = _memberId;
                memberManager.ManagerId = _managerId;
                memberManager.IsPrimaryManager = true;
                memberManager.IsRemoved = false;
                memberManager.CreatorId = _managerId;
                memberManager.UpdatorId = _managerId;
                Facade.AddMemberManager(memberManager);
            }
        }
    }

    #endregion

    #region MemberJobCart and MemberJobCartDetail

    [WebMethod]
    public Int32 MemberJobCart_Create(Int32 _memberId, Int32 _jobPostingId, Int32 _creatorId)
    {
        MemberJobCart memberJobCart = new MemberJobCart();
        memberJobCart.ApplyDate = DateTime.Now;
        memberJobCart.IsPrivate = false;
        memberJobCart.IsInternal = false;
        memberJobCart.MemberId = _memberId;
        memberJobCart.JobPostingId = _jobPostingId;
        memberJobCart.CreatorId = _creatorId;
        memberJobCart.IsRemoved = false;

        return Facade.AddMemberJobCart(memberJobCart).Id;
    }

    [WebMethod]
    public void MemberJobCartDetail_Create(Int32 _memberId, Int32 _jobPostingId, Int32 _creatorId)
    {
        MemberJobCartDetail memberJobCartDetail = new MemberJobCartDetail();
        memberJobCartDetail.AddedDate = DateTime.Now;
           HiringMatrixLevels hiringMatrixLevel= Facade.HiringMatrixLevel_GetInitialLevel();
           int initialLevel = 0;
           if (hiringMatrixLevel != null) initialLevel = hiringMatrixLevel.Id;

        memberJobCartDetail.SelectionStepLookupId = initialLevel ;
        memberJobCartDetail.AddedBy = _memberId;
        memberJobCartDetail.IsRemoved = false;
        memberJobCartDetail.MemberJobCartId = _jobPostingId;
        memberJobCartDetail.CreatorId = _creatorId;

        Facade.AddMemberJobCartDetail(memberJobCartDetail);
    }

    #endregion

    #region MemberEmail EmailDetail and EmailAttachment

    [WebMethod]
    public Int32 MemberEmail_Create(Int32 _senderId, string _senderEmail, Int32 _receiverId, string _receiverEmail, string _subject, string _emailBody, Int32 _creatorId)
    {
        Int32 memberEmailId = 0;
        MemberEmail memberEmail = new MemberEmail();
        memberEmail.SenderId = _senderId;
        memberEmail.SenderEmail = _senderEmail;
        memberEmail.ReceiverId = _receiverId;
        memberEmail.ReceiverEmail = _receiverEmail;
        memberEmail.Subject = _subject;
        memberEmail.EmailBody = _emailBody;
        memberEmail.CreatorId = _creatorId;
        memberEmailId = Facade.AddMemberEmail(memberEmail).Id;
        return memberEmailId;
    }

    [WebMethod]
    public Int32 MemberEmailDetail_Create(Int32 _memberId, string _emailAddress, Int32 _addressTypeId, Int32 _memberEmailId)
    {
        Int32 memberEmailDetailId = 0;
        MemberEmailDetail memberEmailDetail = new MemberEmailDetail();
        memberEmailDetail.MemberId = _memberId;
        memberEmailDetail.EmailAddress = _emailAddress;
        memberEmailDetail.AddressTypeId = _addressTypeId;
        memberEmailDetail.SendingTypeId = Convert.ToInt32(EmailSendingType.Single);
        memberEmailDetail.MemberEmailId = _memberEmailId;
        memberEmailDetailId = Facade.AddMemberEmailDetail(memberEmailDetail).Id;
        return memberEmailDetailId;
    }

    [WebMethod]
    public Int32 MemberEmailAttachment_Create(string _attachmentFile, Int32 _attachmentSize, Int32 _memberEmailId)
    {
        Int32 memberEmailAttachmentId = 0;
        MemberEmailAttachment memberEmailAttachment = new MemberEmailAttachment();
        memberEmailAttachment.AttachmentFile = _attachmentFile;
        memberEmailAttachment.AttachmentSize = _attachmentSize;
        memberEmailAttachment.MemberEmailId = _memberEmailId;
        memberEmailAttachmentId = Facade.AddMemberEmailAttachment(memberEmailAttachment).Id;
        return memberEmailAttachmentId;
    }

    [WebMethod]
    public Int32 GetMemberEmailSenderByMemberIdSubjectDate(Int32 _memberId, string _subject, DateTime _createDate)
    {
        Int32 senderId = 0;
        senderId = Facade.GetMemberEmailSenderByMemberIdSubjectDate(_memberId, _subject, _createDate).Id;
        return senderId;
    }

    [WebMethod]
    public Int32 GetMemberEmailReceiverByMemberIdSubjectDate(Int32 _memberId, string _subject, DateTime _createDate)
    {
        Int32 receiverId = 0;
        receiverId = Facade.GetMemberEmailReceiverByMemberIdSubjectDate(_memberId, _subject, _createDate).Id;
        return receiverId;
    }

    #endregion

    #region MemberSkill

    [WebMethod]
    public void MemberSkillsCreate(string[] _memberSkillSet, Int32 _memberId, Int32 creatorId)
    {
        if (_memberId > 0)
        {
            foreach (string memberSkill in _memberSkillSet)
            {
                Int32 skillId = 0;
                skillId = GetSkillIdByName(memberSkill.Trim());
                if (skillId > 0)
                {
                    Int32 skillMapId = 0;
                    MemberSkillMap memberSkillmap = new MemberSkillMap();
                    if (memberSkillmap != null)
                    {
                        memberSkillmap = Facade.GetMemberSkillMapByMemberIdAndSkillId(_memberId, skillId);
                        if (memberSkillmap != null)
                        {
                            skillMapId = memberSkillmap.Id;
                        }
                        else
                        {
                            skillMapId = 0;
                        }
                    }
                    else
                    {
                        skillMapId = 0;
                    }
                    //skillMapId = Facade.GetMemberSkillMapByMemberIdAndSkillId(skillId, _memberId).Id;
                    if (skillMapId <= 0)
                    {
                        MemberSkillMap memberSkillMap = new MemberSkillMap();
                        memberSkillMap.ProficiencyLookupId = 0;
                        memberSkillMap.YearsOfExperience = 2;
                        memberSkillMap.Comment = string.Empty;
                        memberSkillMap.MemberId = _memberId;
                        memberSkillMap.SkillId = skillId;
                        memberSkillMap.CreatorId = creatorId;
                        memberSkillMap.LastUsed = Convert.ToDateTime("01/01/1754");
                        memberSkillMap.IsRemoved = false;

                        Facade.AddMemberSkillMap(memberSkillMap);
                    }
                }
            }
        }
    }

    [WebMethod]
    public List<Skill> GetAllSkill()
    {
        return (List<Skill>)Facade.GetAllSkill();
    }

    [WebMethod]
    public void MemberSkillsCreateBySkillIds(string _memberSkillIds, Int32 _memberId, Int32 creatorId)
    {
        if (_memberId > 0)
        {
            string[] _memberSkillSetIds = _memberSkillIds.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string memberSkillId in _memberSkillSetIds)
            {
                Int32 skillId = Int32.Parse(memberSkillId);
                if (skillId > 0)
                {
                    MemberSkillMap memberSkillMap = Facade.GetMemberSkillMapByMemberIdAndSkillId(_memberId, skillId);
                    if (memberSkillMap == null)
                    {
                        memberSkillMap = new MemberSkillMap();
                        memberSkillMap.ProficiencyLookupId = 0;
                        memberSkillMap.YearsOfExperience = 2;
                        memberSkillMap.Comment = string.Empty;
                        memberSkillMap.MemberId = _memberId;
                        memberSkillMap.SkillId = skillId;
                        memberSkillMap.CreatorId = creatorId;
                        memberSkillMap.LastUsed = DateTime.Today;
                        memberSkillMap.IsRemoved = false;

                        Facade.AddMemberSkillMap(memberSkillMap);
                    }
                }
            }
        }
    }

    #endregion

    #region Resource

    [WebMethod]
    public bool Resource_Create(Resource _resource)
    {
        try
        {
            Facade.AddResource(_resource);
            return true;
        }
        catch
        {
            return false;
        }
    }

    [WebMethod]
    public Resource Resource_GetByResourceName(string resourceName)
    {
        return Facade.GetResourceByResourceName(resourceName);
    }

    #endregion

    #region Parser

    [WebMethod]
    public int SaveMember(Guid userId,int memberId, string primaryEmail, string password, string role, string firstName, string middleName, string lastName, string currentAddress1, string currentAddress2, string currentCity, int currentStateId,
        string currentZip, int currentCountryId, string alternativeEmail, string primaryPhone, string cellPhone, string officePhone, int resumeSource,
        int resumeSharing, DateTime dateofbirth, bool relocation, string currentPosition, string currentEmployer, string yearsOfExperience,
        decimal currentYearlyRate, decimal currentHourlyRate, int workAuthorization, int internalRating,bool willingToTravel,bool securityClearance,int gender,int maritalStatus, 
        int jobDuration, int creatorId, bool isAssignToMySelf, int jobId)
    {
        bool isNew = false;
        //int memberId = 0;
        Member member = new Member();
        MemberDetail memberDetail = new MemberDetail();
        MemberExtendedInformation memberExtendedInformation = new MemberExtendedInformation();
        if (userId != Guid.Empty)
        {
            member = Facade.GetMemberByUserId(userId);
            memberId = member.Id;
            memberDetail = Facade.GetMemberDetailByMemberId(memberId);
            memberExtendedInformation = Facade.GetMemberExtendedInformationByMemberId(memberId);
        }
        else if (memberId>0)
        {
            member = Facade.GetMemberById(memberId);
            memberId = member.Id;
            memberDetail = Facade.GetMemberDetailByMemberId(memberId);
            memberExtendedInformation = Facade.GetMemberExtendedInformationByMemberId(memberId);
        }
        else
        {
            userId = CreateMemberShipUser(primaryEmail, password, role);
        }
        member.FirstName = CheckEmpty(member.FirstName,firstName);
        member.MiddleName = CheckEmpty(member.MiddleName, middleName);
        member.LastName = CheckEmpty(member.LastName, lastName);
        member.DateOfBirth = CheckEmpty(member.DateOfBirth, dateofbirth);
        member.PermanentAddressLine1 = CheckEmpty(member.PermanentAddressLine1, currentAddress1);
        member.PermanentAddressLine2 = CheckEmpty(member.PermanentAddressLine2, currentAddress2);
        member.PermanentCity = CheckEmpty(member.PermanentCity, currentCity);
        member.PermanentCountryId = CheckEmpty(member.PermanentCountryId, currentCountryId);
        member.PermanentStateId = CheckEmpty(member.PermanentStateId, currentStateId);
        member.PermanentZip = CheckEmpty(member.PermanentZip, currentZip);
        member.PermanentPhone = CheckEmpty(member.PermanentPhone, primaryPhone);
        member.PrimaryEmail = CheckEmpty(member.PrimaryEmail, primaryEmail);
        member.AlternateEmail = CheckEmpty(member.AlternateEmail, alternativeEmail);
        member.PrimaryPhone = CheckEmpty(member.PrimaryPhone, primaryPhone);
        member.CellPhone = CheckEmpty(member.CellPhone, cellPhone);
        member.ResumeSource = resumeSource;
        member.ResumeSharing = resumeSharing;
        member.Status = (int)MemberStatus.Active;
        member.UserId = userId;
        isNew = member.IsNew;
        if (member.IsNew)
        {
            member.CreatorId = creatorId;
            member = Facade.AddMember(member);
            memberId = member.Id;
        }
        else
        {
            member.UpdatorId = creatorId;
            member = Facade.UpdateMember(member);
            memberId = member.Id;
        }
        if (member != null && member.Id > 0)
        {
            if (memberDetail.IsNew)
            {
                memberDetail.IsCurrentSameAsPrimaryAddress = true;
            }
            memberDetail.CurrentAddressLine1 = CheckEmpty(memberDetail.CurrentAddressLine1, currentAddress1);
            memberDetail.CurrentAddressLine2 = CheckEmpty(memberDetail.CurrentAddressLine2, currentAddress2);
            memberDetail.CurrentCity = CheckEmpty(memberDetail.CurrentCity, currentCity);
            memberDetail.CurrentStateId = CheckEmpty(memberDetail.CurrentStateId, currentStateId);
            memberDetail.CurrentZip = CheckEmpty(memberDetail.CurrentZip, currentZip);
            memberDetail.CurrentCountryId = CheckEmpty(memberDetail.CurrentCountryId, currentCountryId);
            memberDetail.HomePhone = CheckEmpty(memberDetail.HomePhone, primaryPhone);
            memberDetail.OfficePhone = CheckEmpty(memberDetail.OfficePhone, officePhone);
            memberDetail.OfficePhoneExtension = CheckEmpty(memberDetail.OfficePhoneExtension, string.Empty);
            memberDetail.Fax = CheckEmpty(memberDetail.Fax, string.Empty);
            memberDetail.CityOfBirth = CheckEmpty(memberDetail.CityOfBirth, string.Empty);
            memberDetail.ProvinceOfBirth = CheckEmpty(memberDetail.ProvinceOfBirth, string.Empty);
            memberDetail.StateIdOfBirth = CheckEmpty(memberDetail.StateIdOfBirth, string.Empty);
            memberDetail.CountryIdOfBirth = CheckEmpty(memberDetail.CountryIdOfBirth, string.Empty);
            memberDetail.CountryIdOfCitizenship = CheckEmpty(memberDetail.CountryIdOfCitizenship, string.Empty);
            memberDetail.SSNPAN = CheckEmpty(memberDetail.SSNPAN, string.Empty);
            memberDetail.BirthMark = CheckEmpty(memberDetail.BirthMark, string.Empty);
            memberDetail.GenderLookupId = CheckEmpty(memberDetail.GenderLookupId, gender);
            memberDetail.EthnicGroupLookupId = CheckEmpty(memberDetail.EthnicGroupLookupId, 0);
            memberDetail.Photo = CheckEmpty(memberDetail.Photo, string.Empty);
            memberDetail.BloodGroupLookupId = CheckEmpty(memberDetail.BloodGroupLookupId, 0);
            memberDetail.Height = CheckEmpty(memberDetail.Height, string.Empty);
            memberDetail.Weight = CheckEmpty(memberDetail.Weight, string.Empty);
            memberDetail.EmergencyContactPerson = CheckEmpty(memberDetail.EmergencyContactPerson, string.Empty);
            memberDetail.EmergencyContactNumber = CheckEmpty(memberDetail.EmergencyContactNumber, string.Empty);
            memberDetail.EmergencyContactRelation = CheckEmpty(memberDetail.EmergencyContactRelation, string.Empty);
            memberDetail.FatherName = CheckEmpty(memberDetail.FatherName, string.Empty);
            memberDetail.MotherName = CheckEmpty(memberDetail.MotherName, string.Empty);
            memberDetail.MaritalStatusLookupId = CheckEmpty(memberDetail.MaritalStatusLookupId, maritalStatus);
            memberDetail.SpouseName = CheckEmpty(memberDetail.SpouseName, string.Empty);
            memberDetail.AnniversaryDate = CheckEmpty(memberDetail.AnniversaryDate, DateTime.MinValue);
            memberDetail.NumberOfChildren = CheckEmpty(memberDetail.NumberOfChildren, 0);
            memberDetail.DisabilityInformation = CheckEmpty(memberDetail.DisabilityInformation, string.Empty);
            memberDetail.IsRemoved = false;
            memberDetail.MemberId = memberId;
            memberDetail.CreatorId = creatorId;
            if (memberDetail.IsNew)
            {
                memberDetail.CreatorId = creatorId;
                memberDetail = Facade.AddMemberDetail(memberDetail);
            }
            else
            {
                memberDetail.UpdatorId = creatorId;
                memberDetail = Facade.UpdateMemberDetail(memberDetail);
            }


            memberExtendedInformation.Relocation = relocation;
            memberExtendedInformation.CurrentPosition = CheckEmpty(memberExtendedInformation.CurrentPosition, currentPosition);
            memberExtendedInformation.AdditionalJobTitle = CheckEmpty(memberExtendedInformation.AdditionalJobTitle, string.Empty);
            memberExtendedInformation.LastEmployer = CheckEmpty(memberExtendedInformation.LastEmployer, currentEmployer);
            memberExtendedInformation.LastEmployerTypeLookupId = CheckEmpty(memberExtendedInformation.LastEmployerTypeLookupId, 0);
            memberExtendedInformation.HighestEducationLookupId = CheckEmpty(memberExtendedInformation.HighestEducationLookupId, 0);
            memberExtendedInformation.TotalExperienceYears = CheckEmpty(memberExtendedInformation.TotalExperienceYears, yearsOfExperience);
            memberExtendedInformation.Availability = CheckEmpty(memberExtendedInformation.Availability, 315);
            if (memberExtendedInformation.Availability == 315)
            {
                memberExtendedInformation.AvailableDate = DateTime.MinValue;
            }
            else
            {
                memberExtendedInformation.AvailableDate = CheckEmpty(memberExtendedInformation.AvailableDate, DateTime.MinValue);
            }
            memberExtendedInformation.CurrentYearlyRate = CheckEmpty(memberExtendedInformation.CurrentYearlyRate, currentYearlyRate);
            memberExtendedInformation.CurrentYearlyCurrencyLookupId = CheckEmpty(memberExtendedInformation.CurrentYearlyCurrencyLookupId, 0);
            memberExtendedInformation.CurrentMonthlyRate = CheckEmpty(memberExtendedInformation.CurrentMonthlyRate, 0);
            memberExtendedInformation.CurrentMonthlyCurrencyLookupId = CheckEmpty(memberExtendedInformation.CurrentMonthlyCurrencyLookupId, 0);
            memberExtendedInformation.CurrentHourlyRate = CheckEmpty(memberExtendedInformation.CurrentHourlyRate, currentHourlyRate);
            memberExtendedInformation.CurrentHourlyCurrencyLookupId = CheckEmpty(memberExtendedInformation.CurrentHourlyCurrencyLookupId, 0);
            memberExtendedInformation.ExpectedYearlyRate = CheckEmpty(memberExtendedInformation.ExpectedYearlyRate, 0);
            memberExtendedInformation.ExpectedYearlyCurrencyLookupId = CheckEmpty(memberExtendedInformation.ExpectedYearlyCurrencyLookupId, 0);
            memberExtendedInformation.ExpectedMonthlyRate = CheckEmpty(memberExtendedInformation.ExpectedMonthlyRate, 0);
            memberExtendedInformation.ExpectedMonthlyCurrencyLookupId = CheckEmpty(memberExtendedInformation.ExpectedMonthlyCurrencyLookupId, 0);
            memberExtendedInformation.ExpectedHourlyRate = CheckEmpty(memberExtendedInformation.ExpectedHourlyRate, 0);
            memberExtendedInformation.ExpectedHourlyCurrencyLookupId = CheckEmpty(memberExtendedInformation.ExpectedHourlyCurrencyLookupId, 0);
            memberExtendedInformation.WillingToTravel = willingToTravel;
            if (memberExtendedInformation.IsNew)
            {
                memberExtendedInformation.PassportStatus = false;
            }
            memberExtendedInformation.SalaryTypeLookupId = CheckEmpty(memberExtendedInformation.SalaryTypeLookupId, 0);
            memberExtendedInformation.JobTypeLookupId = CheckEmpty(memberExtendedInformation.JobTypeLookupId, jobDuration);
            memberExtendedInformation.SecurityClearance = securityClearance;
            memberExtendedInformation.WorkAuthorizationLookupId = CheckEmpty(memberExtendedInformation.WorkAuthorizationLookupId, workAuthorization);
            memberExtendedInformation.InternalRatingLookupId = CheckEmpty(memberExtendedInformation.InternalRatingLookupId, internalRating);
            memberExtendedInformation.PreferredLocation = CheckEmpty(memberExtendedInformation.PreferredLocation, string.Empty);
            memberExtendedInformation.IndustryExperience = CheckEmpty(memberExtendedInformation.IndustryExperience, string.Empty);
            memberExtendedInformation.IndustryTypeLookupId = CheckEmpty(memberExtendedInformation.IndustryTypeLookupId, 0);
            memberExtendedInformation.Remarks = CheckEmpty(memberExtendedInformation.Remarks, string.Empty);
            memberExtendedInformation.IsRemoved = false;
            memberExtendedInformation.MemberId = memberId;

            if (memberExtendedInformation.IsNew)
            {
                memberExtendedInformation.CreatorId = creatorId;
                memberExtendedInformation = Facade.AddMemberExtendedInformation(memberExtendedInformation);
            }
            else
            {
                memberExtendedInformation.UpdatorId = creatorId;
                memberExtendedInformation = Facade.UpdateMemberExtendedInformation(memberExtendedInformation);
            }
            if (isAssignToMySelf)
            {
                MemberManager memberManager = new MemberManager();
                memberManager.IsPrimaryManager = isNew;
                memberManager.ManagerId = creatorId;
                memberManager.MemberId = memberId;
                memberManager.CreatorId = creatorId;
                memberManager.UpdatorId = creatorId;
                Facade.AddMemberManager(memberManager);
            }

            if (jobId > 0)
            {
                   HiringMatrixLevels hiringMatrixLevel= Facade.HiringMatrixLevel_GetInitialLevel();
                    int initialLevel = 0;
                    if (hiringMatrixLevel != null) initialLevel = hiringMatrixLevel.Id;
                MiscUtil.MoveApplicantToHiringLevel(jobId, memberId, creatorId, initialLevel , Facade);
            }
            return member.Id;
        }
        else
        {
            return 0;
        }
    }

    [WebMethod]
    public int SaveObjectiveSummaryAndWordResume(int memberId, string applicantFullName, string objective, string summary, string copyPasteResume, int creatorId)
    {
        MemberObjectiveAndSummary memberObjective = Facade.GetMemberObjectiveAndSummaryByMemberId(memberId);
        if (memberObjective == null)
        {
            memberObjective = new MemberObjectiveAndSummary();
        }
        memberObjective.CopyPasteResume = CheckEmpty(memberObjective.CopyPasteResume, copyPasteResume.Replace("\n", "</br>"));
        memberObjective.RawCopyPasteResume = CheckEmpty(memberObjective.RawCopyPasteResume, copyPasteResume);
        memberObjective.Objective = CheckEmpty(memberObjective.Objective, objective.Replace("\n", "</br>"));
        memberObjective.Summary = CheckEmpty(memberObjective.Summary, summary.Replace("\n", "</br>"));
        memberObjective.MemberId = memberId;
        memberObjective.CreatorId = creatorId;
        if (memberObjective.IsNew)
        {
            memberObjective = Facade.AddMemberObjectiveAndSummary(memberObjective);
        }
        else
        {
            memberObjective = Facade.UpdateMemberObjectiveAndSummary(memberObjective);
        }

        IList<MemberDocument> memberDocumentList = Facade.GetAllMemberDocumentByTypeAndMemberId(ContextConstants.MEMBER_DOCUMENT_TYPE_WORDRESUME, memberId);
        if (memberDocumentList != null && memberDocumentList.Count == 0)
        {
            string fileName = applicantFullName + ".doc";
            string strFilePath = GetMemberDocumentPathForResumeParser(memberId, fileName, "Word Resume", false);
            CreateFile(strFilePath, copyPasteResume);
            MemberDocument memberDocument = new MemberDocument();
            memberDocument.Title = "Resume in Word Document";
            memberDocument.FileName = fileName;
            memberDocument.Description = string.Empty;
            memberDocument.FileTypeLookupId = 55;
            memberDocument.IsRemoved = false;
            memberDocument.MemberId = memberId;
            memberDocument.CreatorId = creatorId;
            Facade.AddMemberDocument(memberDocument);
        }
        return memberObjective.Id;
    }

    [WebMethod]
    public int SaveObjectiveSummary(int memberId, string objective, string summary, string copyPasteResume, int creatorId)
    {
        MemberObjectiveAndSummary memberObjective = Facade.GetMemberObjectiveAndSummaryByMemberId(memberId);
        if (memberObjective == null)
        {
            memberObjective = new MemberObjectiveAndSummary();
        }
        memberObjective.CopyPasteResume = CheckEmpty(memberObjective.CopyPasteResume, copyPasteResume.Replace("\n", "</br>"));
        memberObjective.RawCopyPasteResume = CheckEmpty(memberObjective.RawCopyPasteResume, copyPasteResume);
        memberObjective.Objective = CheckEmpty(memberObjective.Objective, objective.Replace("\n", "</br>"));
        memberObjective.Summary = CheckEmpty(memberObjective.Summary, summary.Replace("\n", "</br>"));
        memberObjective.MemberId = memberId;
        memberObjective.CreatorId = creatorId;
        if (memberObjective.IsNew)
        {
            memberObjective = Facade.AddMemberObjectiveAndSummary(memberObjective);
        }
        else
        {
            memberObjective = Facade.UpdateMemberObjectiveAndSummary(memberObjective);
        }

        return memberObjective.Id;
    }

    [WebMethod]
    public int SaveWordResume(int memberId, string applicantFullName, byte[] byteResume,int creatorId)
    {
        try
        {
            IList<MemberDocument> memberDocumentList = Facade.GetAllMemberDocumentByTypeAndMemberId(ContextConstants.MEMBER_DOCUMENT_TYPE_WORDRESUME, memberId);
            if (memberDocumentList == null || memberDocumentList.Count == 0)
            {
                string fileName = applicantFullName + ".doc";
                string strFilePath = GetMemberDocumentPathForResumeParser(memberId, fileName, "Word Resume", false);
                using (FileStream fsOut = new FileStream(strFilePath, FileMode.Create, FileAccess.ReadWrite))
                {
                    fsOut.Write(byteResume, 0, byteResume.Length);
                }

                MemberDocument memberDocument = new MemberDocument();
                memberDocument.Title = "Resume in Word Document";
                memberDocument.FileName = fileName;
                memberDocument.Description = string.Empty;
                memberDocument.FileTypeLookupId = 55;
                memberDocument.IsRemoved = false;
                memberDocument.MemberId = memberId;
                memberDocument.CreatorId = creatorId;
                memberDocument = Facade.AddMemberDocument(memberDocument);
                return memberDocument.Id;
            }
            else
            {
                return 0;
            }
        }
        catch
        {
            return 0;
        }
    }
    #endregion

    #region WebDomain

    [WebMethod]
    public DataTable GetAllWebParserDomainNames()
    {
        return Facade.GetAllWebParserDomainName();
    }

    [WebMethod]
    public List<WebParserDomain> GetAllWebParserDomain()
    {
        return (List<WebParserDomain>)Facade.GetAllWebParserDomain();
    }

    [WebMethod]
    public WebParserDomain GetWebParserDomainById(int Id)
    {
        return Facade.GetWebParserDomainById(Id);
    }

    [WebMethod]
    public bool AddWebParserDomain(WebParserDomain webParserDomain)
    {
        webParserDomain = Facade.AddWebParserDomain(webParserDomain);
        if (webParserDomain.Id > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    [WebMethod]
    public bool UpdateWebParserDomain(WebParserDomain webParserDomain)
    {
        webParserDomain = Facade.UpdateWebParserDomain(webParserDomain);
        if (webParserDomain.Id > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    [WebMethod]
    public bool DeleteWebParserDomainById(int Id)
    {
        return Facade.DeleteWebParserDomainById(Id);
    }

    #endregion

    #endregion

    #region Method

    public static string GetMemberDocumentPathForResumeParser(int memberId, string strFileName, string strDocumenType, bool isVirtual)
    {
        string strFilePath = String.Empty;
        if (isVirtual)
        {
            strFilePath = "Resources/Member";
        }
        else
        {
            strFilePath = HttpContext.Current.Server.MapPath("Resources/Member");

        }
        if (!isVirtual)
        {
            strFilePath = strFilePath + "\\" + memberId.ToString();
            if (!Directory.Exists(strFilePath))
            {
                Directory.CreateDirectory(strFilePath);
            }
        }
        else
        {
            strFilePath = strFilePath + "/" + memberId.ToString();
        }


        if (!isVirtual)
        {
            strFilePath = strFilePath + "\\" + strDocumenType;
            if (!Directory.Exists(strFilePath))
            {
                Directory.CreateDirectory(strFilePath);
            }
        }
        else
        {
            strFilePath = strFilePath + "/" + strDocumenType;
        }

        if (!isVirtual)
        {
            strFilePath = strFilePath + "\\" + strFileName;
            return strFilePath;

        }
        else
        {
            strFilePath = strFilePath + "/" + strFileName;
            string strPhysicalPath = HttpContext.Current.Server.MapPath(strFilePath);
            if (File.Exists(strPhysicalPath))
            {
                return strFilePath;
            }
            else
            {
                return strFileName;
            }
        }
    }

    public static string GetMemberDocumentPathForEmailAttachmen(int memberEmailId, string strFileName, bool isVirtual)
    {
        string strFilePath = String.Empty;
        if (isVirtual)
        {
            strFilePath = "Resources/EmailDocuments";
        }
        else
        {
            strFilePath = HttpContext.Current.Server.MapPath("Resources/EmailDocuments");

        }
        if (!isVirtual)
        {
            strFilePath = strFilePath + "\\" + memberEmailId.ToString();
            if (!Directory.Exists(strFilePath))
            {
                Directory.CreateDirectory(strFilePath);
            }
        }
        else
        {
            strFilePath = strFilePath + "/" + memberEmailId.ToString();
        }

        if (!isVirtual)
        {
            strFilePath = strFilePath + "\\" + strFileName;
            return strFilePath;

        }
        else
        {
            strFilePath = strFilePath + "/" + strFileName;
            string strPhysicalPath = HttpContext.Current.Server.MapPath(strFilePath);
            if (File.Exists(strPhysicalPath))
            {
                return strFilePath;
            }
            else
            {
                return strFileName;
            }
        }
    }

    private Int32 GetSkillIdByName(string skillName)
    {
        #region MyRegion
        //Int32 skillId = 0;
        //List<Skill> allSkills = GetAllSkill();
        //if (allSkills != null)
        //{
        //    foreach (Skill skill in allSkills)
        //    {
        //        if (skill.Name.Contains(skillName))
        //        {
        //            skillId = skill.Id;
        //            break;
        //        }
        //    }
        //} 
        #endregion
        return Facade.GetSkillIdBySkillName(skillName);
    }

    private void CreateFile(string filePathWithFileName, string data)
    {
        using (StreamWriter sw = new StreamWriter(filePathWithFileName))
        {
            sw.Write(data);
        }
    }

    private string CheckEmpty(string existingValue,string newValue)
    {
        if (string.IsNullOrEmpty(existingValue))
        {
            return newValue;
        }
        else if (!string.IsNullOrEmpty(newValue))
        {
            return newValue;
        }
        else
        {
            return existingValue;
        }
    }

    private int CheckEmpty(int existingValue, int newValue)
    {
        if (existingValue==0)
        {
            return newValue;
        }
        else if (newValue!=0)
        {
            return newValue;
        }
        else
        {
            return existingValue;
        }
    }
    private decimal CheckEmpty(decimal existingValue, decimal newValue)
    {
        if (existingValue == 0)
        {
            return newValue;
        }
        else if (newValue != 0)
        {
            return newValue;
        }
        else
        {
            return existingValue;
        }
    }

    private DateTime CheckEmpty(DateTime existingValue, DateTime newValue)
    {
        if (existingValue==DateTime.MinValue)
        {
            return newValue;
        }
        else if (newValue!=DateTime.MinValue)
        {
            return newValue;
        }
        else
        {
            return existingValue;
        }
    }
    #endregion

}

