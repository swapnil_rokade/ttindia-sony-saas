﻿//#region Modificiation Log
///*----------------------------------------------------------------------------------------------
// * Version  Date            Modified By         Description
// * --------------------------------------------------------------------------------------------------------
// *  0.1     12-Feb-2009      Anand Dixit         Removed unnecessary comments are wrote modification log
// *  0.2     06--Aug-2008     Anand Dixit         Added ConvertTo method to convert resume to any given format.
// * --------------------------------------------------------------------------------------------------------
// */
//#endregion
//namespace Parser
//{
//    using System;
//    using System.Collections.Generic;
//    using System.Linq;
//    using System.Text;
//    using System.IO;
//    using System.Reflection;
//    //using DocumentConversion;
//    using Sovren;
//    public class DocumentConverter
//    {
//        /// <summary>
//        /// Convert the resume to  plain text 
//        /// </summary>
//        /// <param name="resumeLocation">Physical or URL pointing to the file</param>
//        /// <returns>converted resume as string </returns>

//        //public static ParserLibrary .DocumentConverter converter = new ParserLibrary.DocumentConverter();
//        static DocumentConverter()
//        {
//            //converter.EnabledModules = DocumentConversion.DocumentConverter.Modules.All;
//        }
//        public byte[] ConvertTo(string doc, OutputTypes output)
//        {
//            //DocumentConversion.DocumentConverter converter = new DocumentConversion.DocumentConverter();
//            //converter.EnabledModules = DocumentConversion.DocumentConverter.Modules.All;
//            byte[] docAsBytes = Sovren.RrLib1.LoadFileIntoByteArray(doc);
//            byte[] datam;
//            using (Stream stream = typeof(ParserLibrary . DocumentConverter).Assembly
//                .GetManifestResourceStream("TalentrackrParser.Resources.DocumentConversion.dll"))
//            {
//                datam = ReadFully(stream);
//            }
//            Assembly asm = Assembly.Load(datam);
//            ConstructorInfo ctor = null;
//            Type type = asm.GetType("DocumentConversion.DocumentConverter");
//            foreach (ConstructorInfo into in type.GetConstructors())
//            {
//                ctor = into;
//            }
//            object instance = ctor.Invoke(new object[0]);


//            try
//            {
//                if (type != null && instance != null)
//                {
//                    //string[] results = converter.DoConversion(docAsBytes, outType);
//                    //converter.Dispose();
//                    MethodInfo method = type.GetMethod("DoConversion", new Type[] { typeof(byte[]), typeof(string) });
//                    object res = method.Invoke(instance, new object[] { docAsBytes, output.ToString () });
//                    string[] results = (string[])res;
//                    if (results != null && results.Length > 1)
//                    {
//                        byte[] data = System.Text.ASCIIEncoding.ASCII.GetBytes(results[1].ToCharArray());
//                        return data;
//                    }
//                }
//            }
//            catch (Exception ex)
//            {

//            }

//            return null;
//        }
//        public static string GetResumeText(string resumeLocation)
//        {
//            byte[] data;
//            byte[] docAsBytes = Sovren.RrLib1.LoadFileIntoByteArray(resumeLocation);
//            using (Stream stream = typeof(ParserLibrary . DocumentConverter).Assembly
//               .GetManifestResourceStream("TalentrackrParser.Resources.DocumentConversion.dll"))
//            {
//                data = ReadFully(stream);
//            }
//            Assembly asm = Assembly.Load(data);
//            ConstructorInfo ctor = null;
//            Type type = asm.GetType("DocumentConversion.DocumentConverter");
//            foreach (ConstructorInfo into in type.GetConstructors())
//            {
//                ctor = into;
//            }
//            object instance = ctor.Invoke(new object[0]);
//            try
//            {
//                //string[] results = converter.DoConversion(docAsBytes, "PLAIN_TEXT");
//                MethodInfo method = type.GetMethod("DoConversion", new Type[] { typeof(byte[]), typeof(string) });
//                object res = method.Invoke(instance, new object[] { docAsBytes, "PLAIN_TEXT" });
//                string[] results = (string[])res;
//                if (results != null && results.Length > 1)
//                {
//                    return results[1].Trim();
//                }

//            }
//            catch
//            {
//                return null;
//            }
//            return null;
//        }
//        static byte[] ReadFully(Stream stream)
//        {
//            byte[] buffer = new byte[8192];
//            using (MemoryStream ms = new MemoryStream())
//            {
//                int bytesRead;
//                while ((bytesRead = stream.Read(buffer, 0, buffer.Length)) > 0)
//                {
//                    ms.Write(buffer, 0, bytesRead);
//                }
//                return ms.ToArray();
//            }
//        }
//        public  enum OutputTypes
//        {
//            PlainText = 0,
//            NormalizedText = 1,
//            HtmlFormatted = 2,
//            HtmlPlain = 3,
//            Rtf = 4,
//            WordXml = 5,
//        }

//    }

//}
#region Modificiation Log
/*----------------------------------------------------------------------------------------------
 * Version  Date            Modified By         Description
 * --------------------------------------------------------------------------------------------------------
 *  0.1     12-Feb-2009      Anand Dixit         Removed unnecessary comments are wrote modification log
 *  0.2     06--Aug-2008     Anand Dixit         Added ConvertTo method to convert resume to any given format.
 * --------------------------------------------------------------------------------------------------------
 */
#endregion
namespace Parser
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using DocumentConversion;
    using Sovren;

    public class DocumentConverter
    {
        /// <summary>
        /// Convert the resume to  plain text 
        /// </summary>
        /// <param name="resumeLocation">Physical or URL pointing to the file</param>
        /// <returns>converted resume as string </returns>

        public static DocumentConversion.DocumentConverter converter = new DocumentConversion.DocumentConverter();
        public delegate byte[] _ConvertTo(string doc, DocumentConversion.DocumentConverter.OutputTypes outType);
        static DocumentConverter()
        {
            converter.EnabledModules = DocumentConversion.DocumentConverter.Modules.All;
        }
        public static string GetResumeText(string resumeLocation)
        {
            byte[] docAsBytes = Sovren.RrLib1.LoadFileIntoByteArray(resumeLocation);
            try
            {
                string[] results = converter.DoConversion(docAsBytes, "PLAIN_TEXT");
                if (results != null && results.Length > 1)
                {
                    return results[1].Trim();
                }

            }
            catch
            {
                return null;
            }
            return null;
        }
        public byte[] ConvertTo(string doc, OutputTypes outType)
        {
            DocumentConversion.DocumentConverter converter = new DocumentConversion.DocumentConverter();
            converter.EnabledModules = DocumentConversion.DocumentConverter.Modules.All;
            byte[] docAsBytes = Sovren.RrLib1.LoadFileIntoByteArray(doc);
            try
            {
                if (converter != null)
                {
                    string[] results = converter.DoConversion(docAsBytes, outType.ToString());
                    converter.Dispose();
                    if (results != null && results.Length > 1)
                    {
                        byte[] data = System.Text.ASCIIEncoding.ASCII.GetBytes(results[1].ToCharArray());
                        return data;
                    }
                }
            }
            catch (Exception ex)
            {

            }
            if (converter != null)
            {
                converter.Dispose();
            }
            return null;
        }
        public enum OutputTypes
        {
            PlainText = 0,
            NormalizedText = 1,
            HtmlFormatted = 2,
            HtmlPlain = 3,
            Rtf = 4,
            WordXml = 5,
        }


    }

}