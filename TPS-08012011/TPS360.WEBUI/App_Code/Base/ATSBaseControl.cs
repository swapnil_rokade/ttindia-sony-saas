using TPS360.Common.BusinessEntities;

namespace TPS360.Web.UI
{
    public class ATSBaseControl : BaseControl
    {

        #region Member Variables
        private object m_Data = null;
        public object Data
        {
            get { return m_Data; }
            set { m_Data = value; }
        }
        #endregion

        #region Properties

        public int CurrentJobPostingId
        {
            get
            {
                return ((ATSBasePage)this.Page).CurrentJobPostingId;
            }
        }
        public JobPosting _jobPosting;
        public JobPosting CurrentJobPosting
        {
            get
            {
                if(this.Page is ATSBasePage )
                    return ((ATSBasePage)this.Page).CurrentJobPosting;
                return _jobPosting ;
            }
            set
            {
                if(this.Page is ATSBasePage )  ((ATSBasePage)this.Page).CurrentJobPosting = value;
                _jobPosting = value;
            }
        }

        #endregion

        #region Methods

        #endregion
    }
}