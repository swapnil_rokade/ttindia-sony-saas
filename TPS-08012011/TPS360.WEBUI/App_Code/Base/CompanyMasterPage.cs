using System;
using System.Diagnostics;
using System.Web.Security;

using TPS360.BusinessFacade;
using TPS360.Web.UI.Helper;
using TPS360.Common.BusinessEntities;

namespace TPS360.Web.UI
{
    public abstract class CompanyMasterPage : BaseMasterPage
    {
        #region Member Variables

        #endregion

        #region Properties

        public int CurrentCompanyId
        {
            get
            {
                return ((CompanyBasePage)this.Page).CurrentCompanyId;

            }
        }

        public Company CurrentCompany
        {
            get
            {
                return ((CompanyBasePage)this.Page).CurrentCompany;

            }
            set
            {
                ((CompanyBasePage)this.Page).CurrentCompany = value;
            }
        }

        #endregion

        #region Methods

        public abstract void RenderCurrentCompany();

        #endregion

        #region Events

        #endregion
    }
}