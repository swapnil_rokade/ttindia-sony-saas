﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

/// <summary>
/// Summary description for CandidateBaseControl
/// </summary>
/// 
namespace TPS360.Web.UI
{
    public class CandidateBaseControl : BaseControl
    {
        #region Member Variables

        #endregion

        #region Properties

        public int CurrentCandidateId
        {
            get
            {
                return ((CandidateBasePage)this.Page).CurrentCandidateId;
            }
        }

        public Member CurrentJobPosting
        {
            get
            {
                return ((CandidateBasePage)this.Page).CurrentCandidate;

            }
        }
        //suraj
        public CandidateStatus CurrentCandidateStatus
        {
            get
            {
                string ParentID = Helper.Url.SecureUrl[UrlConstants.PARAM_SITEMAP_PARENT_ID];
                if (ParentID == "12")
                    return CandidateStatus.Candidates;
                else if (ParentID == "690")
                    return CandidateStatus.InternalCandidates;
                else return CandidateStatus.Candidates;
            }
        }
        //suraj
        #endregion

        #region Methods

        #endregion
    }
}
