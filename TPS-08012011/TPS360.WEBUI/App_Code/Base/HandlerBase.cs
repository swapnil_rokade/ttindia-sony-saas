using System.Diagnostics;
using System.Web;
using TPS360.BusinessFacade;
using TPS360.Web.UI.Helper;

namespace TPS360.Web.UI
{
    public abstract class BaseHandler : IHttpHandler
    {
        public abstract void ProcessRequest(HttpContext context);
        private WebHelper _helper;
        private IFacade _facade;

        public virtual bool IsReusable
        {
            get
            {
                return true;
            }
        }

        protected WebHelper Helper
        {
            [DebuggerStepThrough()]
            get
            {
                if (_helper == null)
                {
                    _helper = new WebHelper(HttpContext.Current);
                }

                return _helper;
            }
        }

        protected IFacade Facade
        {
            [DebuggerStepThrough()]
            get
            {
                if (_facade == null)
                {
                    _facade = HttpContext.Current.Items[ContextConstants.Facade] as IFacade;
                }

                return _facade;
            }
        }
    }
}