/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName : WidgetInstanceDataAccess.cs
    Description: 
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1            May-05-2009           N.Srilakshmi         Defect ID: 10168; Added new method GetStateByHostId
-------------------------------------------------------------------------------------------------------------------------------------------       
*/

using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections.Generic;

public class WidgetInstance
{
    private int _id;
    private int _pageId;
    private int _widgetId;
    private int _orderNo;
    private int _columnNo;    
    private bool _expanded;    
    private string _title;
    private string _state;
    //private bool _isFirstLoad;
    private Widget _widget;
    private UserPage _userPage;

    public int Id
    {
        get
        {
            return _id;
        }
        set
        {
            _id = value;
        }
    }

    public int PageId
    {
        get
        {
            return _pageId;
        }
        set
        {
            _pageId = value;
        }
    }

    public int WidgetId
    {
        get
        {
            return _widgetId;
        }
        set
        {
            _widgetId = value;
        }
    }

    public int ColumnNo
    {
        get
        {
            return _columnNo;
        }
        set
        {
            _columnNo = value;
        }
    }

    public int OrderNo
    {
        get
        {
            return _orderNo;
        }
        set
        {
            _orderNo = value;
        }
    }

    public bool Expanded
    {
        get
        {
            return _expanded;
        }
        set
        {
            _expanded = value;
        }
    }

    public string Title
    {
        get
        {
            return _title;
        }
        set
        {
            _title = value;
        }
    }

    public string State
    {
        get
        {
            return _state;
        }
        set
        {
            _state = value;
        }
    }

    public bool IsFirstLoad
    {
        get
        {
            return _expanded;
        }
        set
        {
            _expanded = value;
        }
    }

    public Widget Widget
    {
        get
        {
            if (_widget == null)
            {
                _widget = new Widget();
            }

            return _widget;
        }
        set
        {
            _widget = value;
        }
    }

    public UserPage UserPage
    {
        get
        {
            if (_userPage == null)
            {
                _userPage = new UserPage();
            }

            return _userPage;
        }
        set
        {
            _userPage = value;
        }
    }
}

public partial class WidgetInstanceDataAccess
{
    public bool AddWidgetInstance(WidgetInstance widgetInstance)
    {
        const string SQL = "INSERT INTO [WidgetInstance]([PageId], [WidgetId], [ColumnNo], [OrderNo], [Expanded], [Title], [State], [IsFirstLoad])               " +
                            "VALUES (@p1, @p2, @p3, @p4, @p5, @p6, @p7, @p8)";

        using (IDbConnection cnn = CreateConnection())
        {
            using (IDbCommand cmd = cnn.CreateCommand())
            {
                cmd.CommandText = SQL;
                cmd.Parameters.Add(new SqlParameter("@p1", widgetInstance.PageId));
                cmd.Parameters.Add(new SqlParameter("@p2", widgetInstance.WidgetId));
                cmd.Parameters.Add(new SqlParameter("@p3", widgetInstance.ColumnNo));
                cmd.Parameters.Add(new SqlParameter("@p4", widgetInstance.OrderNo));
                cmd.Parameters.Add(new SqlParameter("@p5", widgetInstance.Expanded));
                cmd.Parameters.Add(new SqlParameter("@p6", widgetInstance.Title));
                cmd.Parameters.Add(new SqlParameter("@p7", widgetInstance.State));
                cmd.Parameters.Add(new SqlParameter("@p8", widgetInstance.IsFirstLoad));

                if (cmd.ExecuteNonQuery() > -1)
                {
                    return true;
                }
            }
        }

        return false;
    }

    public List<WidgetInstance> GetAllWidgetInstanceByPageId(int pageId)
    {
        List<WidgetInstance> list = new List<WidgetInstance>();

        using (IDbConnection cnn = CreateConnection())
        {
            using (IDbCommand cmd = cnn.CreateCommand())
            {
                cmd.CommandText = "SELECT [Id], [PageId], [WidgetId], [ColumnNo], [OrderNo], [Expanded], [Title], [State], [IsFirstLoad] FROM [WidgetInstance] WHERE [WidgetInstance].[PageId] = @pageId  order by  [ColumnNo] asc , [OrderNo] asc ";
                cmd.Parameters.Add(new SqlParameter("@pageId", pageId));

                using (IDataReader rdr = cmd.ExecuteReader())
                {
                    while (rdr.Read())
                    {
                        WidgetInstance c = new WidgetInstance();

                        list.Add(BuildWidgetInstance(rdr));
                    }
                }
            }
        }

        if (list.Count == 0)
        {
            return null;
        }

        return list;
    }


    public WidgetInstance GetLastAddedWidgetInstanceByPageId(int pageId)
    {
        WidgetInstance list = new WidgetInstance();

        using (IDbConnection cnn = CreateConnection())
        {
            using (IDbCommand cmd = cnn.CreateCommand())
            {
                cmd.CommandText = "SELECT [Id], [PageId], [WidgetId], [ColumnNo], [OrderNo], [Expanded], [Title], [State], [IsFirstLoad] FROM [WidgetInstance] WHERE [Id]= (select max(ID) from [WidgetInstance] where [WidgetInstance].[PageId] = @pageId )  ";
                cmd.Parameters.Add(new SqlParameter("@pageId", pageId));

                using (IDataReader rdr = cmd.ExecuteReader())
                {
                    while (rdr.Read())
                    {
                        WidgetInstance c = new WidgetInstance();

                        return  BuildWidgetInstance(rdr);
                    }
                }
            }
        }

       
            return null;
       
    }


    public List<WidgetInstance> GetAllSingleColumnWidgetByPageId(int pageId)
    {
        const string SQL = "        SELECT  [WI].[Id],                                       " +
                           "                [WI].[PageId],                                     " +
                           "                [WI].[WidgetId],                                      " +
                           "                [WI].[ColumnNo],                  " +
                           "                [WI].[OrderNo],                                      " +
                           "                [WI].[Expanded],                   " +
                           "                [WI].[Title],                   " +
                           "                [WI].[State],                                 " +
                           "                [WI].[IsFirstLoad]                                       " +
                           "         FROM                                                          " +
                           "                [WidgetInstance] AS [WI]                                      " +
                           "         INNER JOIN                                               " +
                           "                [Widget] AS [W]                                    " +
                           "         ON     [W].[Id] = [WI].[WidgetId] AND [W].[Colspan] = 1 AND [W].[IsActive] = 1                    " +
                           "         WHERE                                                         " +
                           "                [WI].[PageId] = @pageId                                 ";

        List<WidgetInstance> list = new List<WidgetInstance>();

        using (IDbConnection cnn = CreateConnection())
        {
            using (IDbCommand cmd = cnn.CreateCommand())
            {
                cmd.CommandText = SQL;
                cmd.Parameters.Add(new SqlParameter("@pageId", pageId));

                using (IDataReader rdr = cmd.ExecuteReader())
                {
                    while (rdr.Read())
                    {
                        WidgetInstance c = new WidgetInstance();

                        list.Add(BuildWidgetInstance(rdr));
                    }
                }
            }
        }

        if (list.Count == 0)
        {
            return null;
        }

        return list;
    }

    public bool CheckIfAlreadyExists(int pageId, int widgetId)
    {
        using (IDbConnection cnn = CreateConnection())
        {
            using (IDbCommand cmd = cnn.CreateCommand())
            {
                cmd.CommandText = "SELECT Count([Id]) FROM [WidgetInstance] WHERE [WidgetInstance].[PageId] = @PageId AND [WidgetInstance].[WidgetId] = @WidgetId";
                cmd.Parameters.Add(new SqlParameter("@PageId", pageId));
                cmd.Parameters.Add(new SqlParameter("@WidgetId", widgetId));

                using (IDataReader rdr = cmd.ExecuteReader())
                {
                    if (rdr.Read())
                    {
                        int count = rdr.IsDBNull(0) ? int.MinValue : rdr.GetInt32(0);

                        return (count > 0) ? true : false;
                    }
                }
            }
        }

        return false;
    }
    public bool GetWidgetAccessByRoleId(int CustomRoleId,int WidgetId)
    {
        using (IDbConnection cnn = CreateConnection())
        {
            using (IDbCommand cmd = cnn.CreateCommand())
            {
                cmd.CommandText = "SELECT Count([Id]) FROM [WidgetAccess] WHERE [WidgetAccess].[CustomRoleId]=@CustomRoleId AND [WidgetAccess].[WidgetId]=@WidgetId";
                cmd.Parameters.Add(new SqlParameter("@CustomRoleId", CustomRoleId));
                cmd.Parameters.Add(new SqlParameter("@WidgetId", WidgetId));
                using (IDataReader rdr = cmd.ExecuteReader())
                {
                    if (rdr.Read())
                    {
                        int count = rdr.IsDBNull(0) ? int.MinValue : rdr.GetInt32(0);

                        return (count > 0) ? true : false;
                    }
                }
            }
        }
        return false;
    }
    public bool UpdateWidgetInstanceState(string state, int wiId)
    {
        const string SQL = "UPDATE [WidgetInstance]              " +
                            "SET       [State] = @p1  " +
                            "WHERE                          " +
                            "       [Id] = @p2";

        using (IDbConnection cnn = CreateConnection())
        {
            using (IDbCommand cmd = cnn.CreateCommand())
            {
                cmd.CommandText = SQL;
                cmd.Parameters.Add(new SqlParameter("@p1", state));
                cmd.Parameters.Add(new SqlParameter("@p2", wiId));

                if (cmd.ExecuteNonQuery() > -1)
                {
                    return true;
                }
            }
        }
        return false;
    }

    public int GetStateByHostId(int HostId)
    {
        using (IDbConnection cnn = CreateConnection())
        {
            using (IDbCommand cmd = cnn.CreateCommand())
            {
                cmd.CommandText = "SELECT State FROM [WidgetInstance] WHERE [Id] = @Id";
                cmd.Parameters.Add(new SqlParameter("@Id", HostId));
                
                using (IDataReader rdr = cmd.ExecuteReader())
                {
                    if (rdr.Read())
                    {
                       
                            string strValue = rdr.GetValue(0).ToString();
                            if (strValue != null && strValue !="")
                            {
                                int count = Convert.ToInt32(strValue);
                                return (count > 0) ? count : 0;
                            }
                    }
                }
            }
        }

        return 0;
    }

    public bool UpdateWidgetInstanceExpanded(bool status, int wiId)
    {
        const string SQL = "UPDATE [WidgetInstance]              " +
                            "SET       [Expanded] = @p1  " +
                            "WHERE                          " +
                            "       [Id] = @p2";

        using (IDbConnection cnn = CreateConnection())
        {
            using (IDbCommand cmd = cnn.CreateCommand())
            {
                cmd.CommandText = SQL;
                cmd.Parameters.Add(new SqlParameter("@p1", status));
                cmd.Parameters.Add(new SqlParameter("@p2", wiId));

                if (cmd.ExecuteNonQuery() > -1)
                {
                    return true;
                }
            }
        }
        return false;

    }

    public bool UpdateWidgetInstancePosition(int pageId)
    {
        List<WidgetInstance> wiList = GetAllSingleColumnWidgetByPageId(pageId);

        const string SQL = "UPDATE [WidgetInstance]              " +
                            "SET   [ColumnNo] = @p1,  " +
                            "      [OrderNo] = @p2  " +
                            "WHERE                          " +
                            "       [Id] = @p3";

        using (IDbConnection cnn = CreateConnection())
        {
            

            if (wiList != null && wiList.Count > 0)
            {
                int row = 0;
                int col = 0;
                int widgetPerColumn = (int)Math.Ceiling((float)wiList.Count / 2.0);

                foreach (WidgetInstance wi in wiList)
                {
                    if (wi.Widget.Colspan == 1)
                    {
                        using (IDbCommand cmd = cnn.CreateCommand())
                        {
                            cmd.CommandText = SQL;
                            cmd.Parameters.Add(new SqlParameter("@p1", col));
                            cmd.Parameters.Add(new SqlParameter("@p2", row));
                            cmd.Parameters.Add(new SqlParameter("@p3", wi.Id));
                            cmd.ExecuteNonQuery();
                            row++;

                            if (row >= widgetPerColumn)
                            {
                                row = 0;
                                col++;
                            }
                        }
                    }
                    
                }
                return true;
            }
        }
        return false;
    }

    public bool DeleteWidgetInstance(int instanceId)
    {
        const string SQL = "DELETE [WidgetInstance]       " +
                            "WHERE                   " +
                            "       [Id] = @P1";

        using (IDbConnection cnn = CreateConnection())
        {
            using (IDbCommand cmd = cnn.CreateCommand())
            {
                cmd.CommandText = SQL;
                cmd.Parameters.Add(new SqlParameter("@p1", instanceId));

                if (cmd.ExecuteNonQuery() > -1)
                {
                    return true;
                }
            }
        }

        return false;
    }

    public bool DeleteWidgetInstanceByPageId(int pageId)
    {
        const string SQL = "DELETE [WidgetInstance]       " +
                            "WHERE                   " +
                            "       [PageId] = @P1";

        using (IDbConnection cnn = CreateConnection())
        {
            using (IDbCommand cmd = cnn.CreateCommand())
            {
                cmd.CommandText = SQL;
                cmd.Parameters.Add(new SqlParameter("@p1", pageId));

                if (cmd.ExecuteNonQuery() > -1)
                {
                    return true;
                }
            }
        }

        return false;
    }

    private static WidgetInstance BuildWidgetInstance(IDataReader reader)
    {
        WidgetInstance widgetInstance = new WidgetInstance();

        widgetInstance.Id = reader.IsDBNull(0) ? int.MinValue : reader.GetInt32(0);
        widgetInstance.PageId = reader.IsDBNull(1) ? int.MinValue : reader.GetInt32(1);
        widgetInstance.WidgetId = reader.IsDBNull(2) ? int.MinValue : reader.GetInt32(2);
        widgetInstance.ColumnNo = reader.IsDBNull(3) ? int.MinValue : reader.GetInt32(3);
        widgetInstance.OrderNo = reader.IsDBNull(4) ? int.MinValue : reader.GetInt32(4);
        widgetInstance.Expanded = reader.IsDBNull(5) ? false : reader.GetBoolean(5);
        widgetInstance.Title = reader.IsDBNull(6) ? string.Empty : reader.GetString(6);
        widgetInstance.State = reader.IsDBNull(7) ? string.Empty : reader.GetString(7);
        widgetInstance.IsFirstLoad = reader.IsDBNull(8) ? false : reader.GetBoolean(8);

        UserPageDataAccess upda = new UserPageDataAccess();
        widgetInstance.UserPage = upda.GetUserPage(widgetInstance.PageId);

        WidgetDataAccess wda = new WidgetDataAccess();
        widgetInstance.Widget = wda.GetWidget(widgetInstance.WidgetId);

        return widgetInstance;
    }

    private static IDbConnection CreateConnection()
    {
        IDbConnection cnn = new System.Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings["TPS360CS"].ConnectionString);
        cnn.Open();

        return cnn;
    }
    public void UpdateWidgetPosition(int instanceId, int ColumnNo, int RowNo)
    {
        const string SQL = "Update [WidgetInstance]  set ColumnNo=@ColumnNo  , OrderNo=@RowNo" + " WHERE                   " +
                          "       [Id] = @P1";

        using (IDbConnection cnn = CreateConnection())
        {
            using (IDbCommand cmd = cnn.CreateCommand())
            {
                cmd.CommandText = SQL;
                cmd.Parameters.Add(new SqlParameter("@p1", instanceId));
                cmd.Parameters.Add(new SqlParameter("@ColumnNo", ColumnNo));
                cmd.Parameters.Add(new SqlParameter("@RowNo", RowNo));
                try
                {
                    if (cmd.ExecuteNonQuery() > -1)
                    {
                        // return true;
                    }
                }
                catch
                {
                }
            }
        }

        //return false;
    }
}
