using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections.Generic;

public class Widget
{
    private int _id;
    private string _name;
    private string _url;    
    private string _description;    
    private bool _isDefault;
    private string _icon;    
    private int _orderNo;
    private string _defaultState;
    private int _colSpan;
    
    public int Id
    {
        get
        {
            return _id;
        }
        set
        {
            _id = value;
        }
    }

    public string Name
    {
        get
        {
            return _name;
        }
        set
        {
            _name = value;
        }
    }

    public string Url
    {
        get
        {
            return _url;
        }
        set
        {
            _url = value;
        }
    }    

    public string Description
    {
        get
        {
            return _description;
        }
        set
        {
            _description = value;
        }
    }

    public int OrderNo
    {
        get
        {
            return _orderNo;
        }
        set
        {
            _orderNo = value;
        }
    }

    public bool IsDefault
    {
        get
        {
            return _isDefault;
        }
        set
        {
            _isDefault = value;
        }
    }

    public string Icon
    {
        get
        {
            return _icon;
        }
        set
        {
            _icon = value;
        }
    }

    public string DefaultState
    {
        get
        {
            return _defaultState;
        }
        set
        {
            _defaultState = value;
        }
    }

    public int Colspan
    {
        get
        {
            return _colSpan;
        }
        set
        {
            _colSpan = value;
        }
    }
}

public partial class WidgetDataAccess
{
    public bool AddWidget(Widget widget)
    {
        const string SQL = "INSERT INTO [Widget]([Name], [Url], [Description], [IsDefault], [Icon], [OrderNo], [DefaultState], [Colspan], [CreateDate], [UpdateDate])               " +
                            "VALUES (@p1, @p2, @p3, @p4, @p5, @p6, p7, p8, p9, p10)";
        
        using (IDbConnection cnn = CreateConnection())
        {
            using (IDbCommand cmd = cnn.CreateCommand())
            {
                cmd.CommandText = SQL;
                cmd.Parameters.Add(new SqlParameter("@p1", widget.Name));
                cmd.Parameters.Add(new SqlParameter("@p2", widget.Url));
                cmd.Parameters.Add(new SqlParameter("@p3", widget.Description));
                cmd.Parameters.Add(new SqlParameter("@p4", widget.IsDefault));
                cmd.Parameters.Add(new SqlParameter("@p5", widget.Icon));
                cmd.Parameters.Add(new SqlParameter("@p6", widget.OrderNo));
                cmd.Parameters.Add(new SqlParameter("@p7", widget.DefaultState));
                cmd.Parameters.Add(new SqlParameter("@p8", widget.Colspan));
                cmd.Parameters.Add(new SqlParameter("@p9", DateTime.Now));
                cmd.Parameters.Add(new SqlParameter("@p10", DateTime.Now));
                
                if (cmd.ExecuteNonQuery() > -1)
                {
                    return true;
                }
            }
        }

        return false;
    }

    public Widget GetWidget(int Id)
    {
        using (IDbConnection cnn = CreateConnection())
        {
            using (IDbCommand cmd = cnn.CreateCommand())
            {
                cmd.CommandText = "SELECT [Id], [Name], [Url], [Description], [IsDefault], [Icon], [OrderNo], [DefaultState], [Colspan] FROM [Widget] WHERE [Widget].[Id] = @Id";
                cmd.Parameters.Add(new SqlParameter("@Id", Id));

                using (IDataReader rdr = cmd.ExecuteReader())
                {
                    if (rdr.Read())
                    {
                        return BuildWidget(rdr);
                    }
                }
            }
        }

        return null;
    }

    public List<Widget> GetAllWidget()
    {
        List<Widget> list = new List<Widget>();

        using (IDbConnection cnn = CreateConnection())
        {
            using (IDbCommand cmd = cnn.CreateCommand())
            {
                cmd.CommandText = "SELECT [Id], [Name], [Url], [Description], [IsDefault], [Icon], [OrderNo], [DefaultState], [Colspan] FROM [Widget] where [IsActive]=1";
                
                using (IDataReader rdr = cmd.ExecuteReader())
                {
                    while (rdr.Read())
                    {
                        UserPage c = new UserPage();

                        list.Add(BuildWidget(rdr));
                    }
                }
            }
        }

        if (list.Count == 0)
        {
            return null;
        }

        return list;
    }

    public List<Widget> GetAllWidget(bool IsDefault)
    {
        List<Widget> list = new List<Widget>();

        using (IDbConnection cnn = CreateConnection())
        {
            using (IDbCommand cmd = cnn.CreateCommand())
            {
                cmd.CommandText = "SELECT [Id], [Name], [Url], [Description], [IsDefault], [Icon], [OrderNo], [DefaultState], [Colspan] FROM [Widget] WHERE [Widget].[IsDefault] = @IsDefault And [Widget].[SourceType]='TPS' order by [Widget].[OrderNo] desc";
                cmd.Parameters.Add(new SqlParameter("@IsDefault", IsDefault));

                using (IDataReader rdr = cmd.ExecuteReader())
                {
                    while (rdr.Read())
                    {
                        UserPage c = new UserPage();

                        list.Add(BuildWidget(rdr));
                    }
                }
            }
        }

        if (list.Count == 0)
        {
            return null;
        }

        return list;
    }

    public List<Widget> GetAllWidgetByRole(int roleId)
    {
        List<Widget> list = new List<Widget>();

        const string SQL = "        SELECT DISTINCT  [W].[Id],                                       " +
                           "                [W].[Name],                                     " +
                           "                [W].[Url],                                     " +
                           "                [W].[Description],                                     " +
                           "                [W].[IsDefault],                                      " +
                           "                [W].[Icon],                  " +
                           "                [W].[OrderNo],                                      " +
                           "                [W].[DefaultState],                   " +
                           "                [W].[Colspan]                   " +
                           "         FROM                                                          " +
                           "                [Widget] AS [W]                                      " +
                           "         INNER JOIN                                               " +
                           "                [WidgetAccess] AS [WA]                                    " +
                           "         ON     [W].[Id] = [WA].[WidgetId] AND [WA].[CustomRoleId] = @CustomRoleId AND [W].[IsActive]=1     order by [W].[Name]            " 
                           ;

        using (IDbConnection cnn = CreateConnection())
        {
            using (IDbCommand cmd = cnn.CreateCommand())
            {
                cmd.CommandText = SQL;
                cmd.Parameters.Add(new SqlParameter("@CustomRoleId", roleId));

                using (IDataReader rdr = cmd.ExecuteReader())
                {
                    while (rdr.Read())
                    {
                        list.Add(BuildWidget(rdr));
                    }
                }
            }
        }

        if (list.Count == 0)
        {
            return null;
        }

        return list;
    }

    private static Widget BuildWidget(IDataReader reader)
    {
        Widget widget = new Widget();

        widget.Id = reader.IsDBNull(0) ? int.MinValue : reader.GetInt32(0);        
        widget.Name = reader.IsDBNull(1) ? string.Empty : reader.GetString(1);
        widget.Url = reader.IsDBNull(2) ? string.Empty : reader.GetString(2);
        widget.Description = reader.IsDBNull(3) ? string.Empty : reader.GetString(3);
        widget.IsDefault = reader.IsDBNull(4) ? false : reader.GetBoolean(4);
        widget.Icon = reader.IsDBNull(5) ? string.Empty : reader.GetString(5);
        widget.OrderNo = reader.IsDBNull(6) ? int.MinValue : reader.GetInt32(6);
        widget.DefaultState = reader.IsDBNull(7) ? string.Empty : reader.GetString(7);
        widget.Colspan = reader.IsDBNull(8) ? int.MinValue : reader.GetInt32(8);

        return widget;
    }

    private static IDbConnection CreateConnection()
    {
        IDbConnection cnn = new System.Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings["TPS360CS"].ConnectionString);
        cnn.Open();

        return cnn;
    }
}