using System.Collections.Generic;

public class UserPageSetup
{
    public UserPageSetup()
    {
    }

    private List<UserPage> _Pages;

    public List<UserPage> Pages
    {
        get { return _Pages; }
        set { _Pages = value; }
    }

    public UserPage CurrentPage
    {
        get
        {
            UserPage currentPage = null;
            foreach (UserPage userPage in Pages)
            {
                if (userPage.Id == _UserSetting.CurrentPageId)
                {
                    currentPage = userPage;
                    break;
                }
            }
            return currentPage;
        }
    }

    private UserPageSetting _UserSetting;

    public UserPageSetting UserSetting
    {
        get { return _UserSetting; }
        set { _UserSetting = value; }
    }


    private List<WidgetInstance> _WidgetInstances;

    public List<WidgetInstance> WidgetInstances
    {
        get { return _WidgetInstances; }
        set { _WidgetInstances = value; }
    }

    public UserPageSetup(int userId)
    {
        UserPageDataAccess upda = new UserPageDataAccess();
        Pages = upda.GetAllUserPagesByUserId(userId);

        UserPageSettingDataAccess upsda = new UserPageSettingDataAccess();
        UserSetting = upsda.GetUserPageSetting(userId);

        WidgetInstanceDataAccess wida = new WidgetInstanceDataAccess();
        WidgetInstances = wida.GetAllWidgetInstanceByPageId(CurrentPage.Id);
    }
}