using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;

public class WidgetAccess
{
    private int _id;
    private string _role;
    private int _widgetId;

    public int Id
    {
        get
        {
            return _id;
        }
        set
        {
            _id = value;
        }
    }


    public string Role
    {
        get
        {
            return _role;
        }
        set
        {
            _role = value;
        }
    }

    public int WidgetId
    {
        get
        {
            return _widgetId;
        }
        set
        {
            _widgetId = value;
        }
    }
}

public partial class WidgetAccessDataAccess
{
    public bool AddWidgetAccess(int roleId, int widgetId)
    {
        const string SQL = "INSERT INTO [WidgetAccess]([WidgetId], [CustomRoleId])               " +
                            "VALUES (@p1, @p2)";

        using (IDbConnection cnn = CreateConnection())
        {
            using (IDbCommand cmd = cnn.CreateCommand())
            {
                cmd.CommandText = SQL;
                cmd.Parameters.Add(new SqlParameter("@p1", widgetId));
                cmd.Parameters.Add(new SqlParameter("@p2", roleId));
                
                if (cmd.ExecuteNonQuery() > -1)
                {
                    return true;
                }
            }
        }

        return false;
    }

    public List<WidgetAccess> GetWidgetByRole(int roleId)
    {
        const string SQL = "        SELECT  [WidgetAccess].[Id],                                       " +
                           "                [WidgetAccess].[WidgetId],                                     " +
                           "                [WidgetAccess].[CustomRoleId]                                     " +
                           "         FROM                                                          " +
                           "                [WidgetAccess]                                      " +
                           "         WHERE                                                         " +
                           "                [WidgetAccess].[CustomRoleId] = @p1                                 ";

        List<WidgetAccess> widgetAccesslist = new List<WidgetAccess>();

        using (IDbConnection cnn = CreateConnection())
        {
            using (IDbCommand cmd = cnn.CreateCommand())
            {
                cmd.CommandText = SQL;
                cmd.Parameters.Add(new SqlParameter("@p1", roleId));

                using (IDataReader rdr = cmd.ExecuteReader())
                {
                    while (rdr.Read())
                    {
                        WidgetAccess p = BuildWidgetAccess(rdr);

                        widgetAccesslist.Add(p);
                    }
                }
            }
        }

        if (widgetAccesslist.Count == 0)
        {
            return null;
        }

        return widgetAccesslist;
    }

    public ArrayList GetWidgetAccessIdsByRole(string roleId)
    {
        ArrayList widgetAccessIds;
        const string SQL = @"SELECT  [WidgetAccess].[Id]," +
                           " [WidgetAccess].[WidgetId]," +
                           " [WidgetAccess].[CustomRoleId]" +
                           " FROM" +
                           " [WidgetAccess]" +
                           " WHERE" +
                           " [WidgetAccess].[CustomRoleId] =@Role";

        using (IDbConnection cnn = CreateConnection())
        {
            using (IDbCommand cmd = cnn.CreateCommand())
            {
                cmd.CommandText = SQL;
                cmd.Parameters.Add(new SqlParameter("@Role", roleId));

                using (IDataReader rdr = cmd.ExecuteReader())
                {
                    widgetAccessIds = new ArrayList();

                    while (rdr.Read())
                    {
                        widgetAccessIds.Add(rdr.IsDBNull(1) ? int.MinValue : rdr.GetInt32(1));
                    }
                }
            }
        }

        return widgetAccessIds;
    }

    public bool DeleteWidgetAccess(int roleId)
    {
        const string SQL = "DELETE [WidgetAccess]       " +
                            "WHERE                   " +
                            "       [CustomRoleId] = @P1";

        using (IDbConnection cnn = CreateConnection())
        {
            using (IDbCommand cmd = cnn.CreateCommand())
            {
                cmd.CommandText = SQL;
                cmd.Parameters.Add(new SqlParameter("@p1", roleId));

                if (cmd.ExecuteNonQuery() > -1)
                {
                    return true;
                }
            }
        }

        return false;
    }

    public bool DeleteWidgetAccessFromRole(int roleId, int widgetId)
    {
        const string SQL = "DELETE [WidgetAccess]       " +
                            "WHERE                   " +
                            "       [CustomRoleId] = @P1 AND [WidgetId] = @P2";

        using (IDbConnection cnn = CreateConnection())
        {
            using (IDbCommand cmd = cnn.CreateCommand())
            {
                cmd.CommandText = SQL;
                cmd.Parameters.Add(new SqlParameter("@p1", roleId));
                cmd.Parameters.Add(new SqlParameter("@p2", widgetId));

                if (cmd.ExecuteNonQuery() > -1)
                {
                    return true;
                }
            }
        }

        return false;
    }

    private static WidgetAccess BuildWidgetAccess(IDataReader reader)
    {
        WidgetAccess widgetAccess = new WidgetAccess();

        widgetAccess.Id = reader.IsDBNull(0) ? int.MinValue : reader.GetInt32(0);
        widgetAccess.WidgetId = reader.IsDBNull(1) ? int.MinValue : reader.GetInt32(1);
        widgetAccess.Role = reader.IsDBNull(2) ? string.Empty: reader.GetString(2);

        return widgetAccess;
    }

    private static IDbConnection CreateConnection()
    {
        IDbConnection cnn = new System.Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings["TPS360CS"].ConnectionString);
        cnn.Open();

        return cnn;
    }
}