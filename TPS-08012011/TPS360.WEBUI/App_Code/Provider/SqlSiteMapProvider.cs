﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration.Provider;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Security.Permissions;
using System.Web;
using System.Web.Configuration;

using TPS360.Web.UI.Helper;
using System.Web.Caching;

namespace TPS360.Providers
{
    [SqlClientPermission(SecurityAction.Demand, Unrestricted = true)]
    public class SqlSiteMapProvider : StaticSiteMapProvider
    {
        private const string _errmsg1 = "Missing node ID";
        private const string _errmsg2 = "Duplicate node ID";
        private const string _errmsg3 = "Missing parent ID";
        private const string _errmsg4 = "Invalid parent ID";
        private const string _errmsg5 = "Empty or missing connectionStringName";
        private const string _errmsg6 = "Missing connection string";
        private const string _errmsg7 = "Empty connection string";
        private const string _cacheDependencyName = "__SiteMapCacheDependency";

        private static string _connectionString;
        private string _defaultUrl;

        private int _indexId;
        private int _indexTitle;
        private int _indexDesc;
        private int _indexUrl;
        private int _indexImageUrl;
        private int _indexRoles;
        private int _indexParent;
        private int _indexIsDefault;
        private int _indexHasCount;
        private  SiteMapNode _root;

        public override void Initialize(string name, NameValueCollection config)
        {
            if (config == null)
            {
                throw new ArgumentNullException("config");
            }

            if (string.IsNullOrEmpty(name))
            {
                name = "EmployeeSqlSiteMapProvider";
            }

            if (string.IsNullOrEmpty(config["description"]))
            {
                config.Remove("description");
                config.Add("description", "SQL SiteMap Provider");
            }

            base.Initialize(name, config);

            string connect = config["connectionStringName"];

            if (string.IsNullOrEmpty(connect))
            {
                throw new ProviderException(_errmsg5);
            }

            config.Remove("connectionStringName");

            if (WebConfigurationManager.ConnectionStrings[connect] == null)
            {
                throw new ProviderException(_errmsg6);
            }

            _connectionString = WebConfigurationManager.ConnectionStrings[connect].ConnectionString;

            if (string.IsNullOrEmpty(_connectionString))
            {
                throw new ProviderException(_errmsg7);
            }

            if (config["defaultUrl"] != null)
            {
                _defaultUrl = config["defaultUrl"];
                config.Remove("defaultUrl");
            }

            // Initialize SQL cache dependency info
            string dependency = config["sqlCacheDependency"];

            if (!String.IsNullOrEmpty(dependency))
            {
                if (String.Equals(dependency, "CommandNotification", StringComparison.InvariantCultureIgnoreCase))
                {
                    //SqlDependency.Start(_connectionString);                    
                }

                config.Remove("sqlCacheDependency");
            }

            config.Remove("sqlCacheDependency");

            if (config["securityTrimmingEnabled"] != null)
            {
                config.Remove("securityTrimmingEnabled");
            }

            if (config.Count > 0)
            {
                string attr = config.GetKey(0);

                if (!string.IsNullOrEmpty(attr))
                {
                    throw new ProviderException("Unrecognized attribute: " + attr);
                }
            }
        }

        public override SiteMapNode BuildSiteMap()
        {
            lock (this)
            {
                if (_root != null)
                {
                    return _root;
                }

                using (SqlConnection cnn = CreateConnection())
                {
                    using (SqlCommand cmd = cnn.CreateCommand())
                    {
                        cmd.CommandText = "CustomSiteMap_GetAll";
                        cmd.CommandType = CommandType.StoredProcedure;

                        //SqlCacheDependency dependency = new SqlCacheDependency(cmd);

                        using (IDataReader reader = cmd.ExecuteReader())
                        {
                            _indexId = reader.GetOrdinal("Id");
                            _indexTitle = reader.GetOrdinal("Title");
                            _indexDesc = reader.GetOrdinal("Description");
                            _indexUrl = reader.GetOrdinal("Url");
                            _indexImageUrl = reader.GetOrdinal("ImageUrl");
                            _indexRoles = reader.GetOrdinal("Roles");
                            _indexParent = reader.GetOrdinal("ParentId");
                            _indexIsDefault = reader.GetOrdinal("IsDefault");
                            _indexHasCount = reader.GetOrdinal("HasCount");
                            Dictionary<string, SiteMapNode> cache = new Dictionary<string, SiteMapNode>(128);

                            while (reader.Read())
                            {
                                SiteMapNode node = CreateSiteMapNodeFromDataReader(reader);

                                if (_root == null)
                                {
                                    _root = node;
                                }

                                if (reader.IsDBNull(_indexParent))
                                {
                                    try
                                    {
                                        AddNode(node, null);
                                    }
                                    catch
                                    {
                                    }
                                }
                                else
                                {
                                    string parentId = reader.GetInt32(_indexParent).ToString(CultureInfo.CurrentCulture);
                                    
                                        SiteMapNode parent = cache[parentId];
                                        try
                                        {
                                            AddNode(node, parent);
                                        }
                                        catch
                                        {
                                        }
                                }

                                cache.Add(node.Key, node);
                            }
                        }

                        //if (dependency != null)
                        //{
                           // HttpRuntime.Cache.Insert(_cacheDependencyName, new object(), dependency,
                                //Cache.NoAbsoluteExpiration, Cache.NoSlidingExpiration, CacheItemPriority.NotRemovable,
                               // new CacheItemRemovedCallback(OnSiteMapChanged));
                        //}
                    }
                }

                return _root;
            }
        }

        protected override SiteMapNode GetRootNodeCore()
        {
            BuildSiteMap();

            return _root;
        }

        public  SiteMapNode CreateSiteMapNodeFromDataReader(IDataReader reader)
        {
            if (reader.IsDBNull(_indexId))
            {
                throw new ProviderException(_errmsg1);
            }

            int id = reader.GetInt32(_indexId);

            string title = reader.IsDBNull(_indexTitle) ? null : reader.GetString(_indexTitle).Trim();
            string description = reader.IsDBNull(_indexDesc) ? null : reader.GetString(_indexDesc).Trim();
            string url = reader.IsDBNull(_indexUrl) ? null : reader.GetString(_indexUrl).Trim();
            string imageUrl = reader.IsDBNull(_indexImageUrl) ? null : reader.GetString(_indexImageUrl).Trim();
            int parentId = reader.IsDBNull(_indexParent) ? int.MinValue : reader.GetInt32(_indexParent);
            string roles = reader.IsDBNull(_indexRoles) ? null : reader.GetString(_indexRoles).Trim();
            bool isDefault = reader.IsDBNull(_indexIsDefault) ? false : reader.GetBoolean(_indexIsDefault);
            bool HasCount = reader.IsDBNull(_indexHasCount) ? false : reader.GetBoolean(_indexHasCount);
            NameValueCollection attributes = new NameValueCollection();
            attributes.Add("Id", id.ToString(CultureInfo.CurrentCulture));

            if (!string.IsNullOrEmpty(url))
            {
                attributes.Add("OriginalUrl", url);

                if (url.Contains("?"))
                {
                    url += "&sitemapid=" + id.ToString(CultureInfo.CurrentCulture) + "&sitemapparentid=" + parentId.ToString(CultureInfo.CurrentCulture);
                }
                else
                {
                    url += "?sitemapid=" + id.ToString(CultureInfo.CurrentCulture) + "&sitemapparentid=" + parentId.ToString(CultureInfo.CurrentCulture);
                }
            }

            //url = (new SecureUrl(url)).ToString();

            attributes.Add("ImageUrl", imageUrl);
            attributes.Add("IsDefault", isDefault.ToString(CultureInfo.CurrentCulture));
            attributes.Add("HasCount", HasCount.ToString(CultureInfo.CurrentCulture));
            string[] roleList = null;

            if (!string.IsNullOrEmpty(roles))
            {
                roleList = roles.Split(new char[] { ',', ';', '|' });
            }

            SiteMapNode node = new SiteMapNode(this, id.ToString(CultureInfo.CurrentCulture), url, title, description, roleList, attributes, null, null);
            return node;
        }
        
        private static SqlConnection CreateConnection()
        {
            SqlConnection cnn = new System.Data.SqlClient.SqlConnection(_connectionString);
            cnn.Open();

            return cnn;
        }

        void OnSiteMapChanged(string key, object item, CacheItemRemovedReason reason)
        {
            lock (this)
            {
                if (key == _cacheDependencyName && reason == CacheItemRemovedReason.DependencyChanged)
                {
                    // Refresh the site map
                    Clear();
                    _root = null;
                }
            }
        }

        public void ResetSiteMap()
        {
            Clear();
            _root = null;
        }
    }
    public class SqlSiteMapProviders : StaticSiteMapProvider
    {
        private const string _errmsg1 = "Missing node ID";
        private const string _errmsg2 = "Duplicate node ID";
        private const string _errmsg3 = "Missing parent ID";
        private const string _errmsg4 = "Invalid parent ID";
        private const string _errmsg5 = "Empty or missing connectionStringName";
        private const string _errmsg6 = "Missing connection string";
        private const string _errmsg7 = "Empty connection string";
        private const string _cacheDependencyName = "__SiteMapCacheDependency";

        private static string _connectionString;
        private string _defaultUrl;

        private int _indexId;
        private int _indexTitle;
        private int _indexDesc;
        private int _indexUrl;
        private int _indexImageUrl;
        private int _indexRoles;
        private int _indexParent;
        private int _indexIsDefault;
        private int _indexHasCount;
        private SiteMapNode _root;

        public override void Initialize(string name, NameValueCollection config)
        {
            if (config == null)
            {
                throw new ArgumentNullException("config");
            }

            if (string.IsNullOrEmpty(name))
            {
                name = "EmployeeSqlSiteMapProvider";
            }

            if (string.IsNullOrEmpty(config["description"]))
            {
                config.Remove("description");
                config.Add("description", "SQL SiteMap Provider");
            }

            base.Initialize(name, config);

            string connect = config["connectionStringName"];

            if (string.IsNullOrEmpty(connect))
            {
                throw new ProviderException(_errmsg5);
            }

            config.Remove("connectionStringName");

            if (WebConfigurationManager.ConnectionStrings[connect] == null)
            {
                throw new ProviderException(_errmsg6);
            }

            _connectionString = WebConfigurationManager.ConnectionStrings[connect].ConnectionString;

            if (string.IsNullOrEmpty(_connectionString))
            {
                throw new ProviderException(_errmsg7);
            }

            if (config["defaultUrl"] != null)
            {
                _defaultUrl = config["defaultUrl"];
                config.Remove("defaultUrl");
            }

            // Initialize SQL cache dependency info
            string dependency = config["sqlCacheDependency"];

            if (!String.IsNullOrEmpty(dependency))
            {
                if (String.Equals(dependency, "CommandNotification", StringComparison.InvariantCultureIgnoreCase))
                {
                    //SqlDependency.Start(_connectionString);                    
                }

                config.Remove("sqlCacheDependency");
            }

            config.Remove("sqlCacheDependency");

            if (config["securityTrimmingEnabled"] != null)
            {
                config.Remove("securityTrimmingEnabled");
            }

            if (config.Count > 0)
            {
                string attr = config.GetKey(0);

                if (!string.IsNullOrEmpty(attr))
                {
                    throw new ProviderException("Unrecognized attribute: " + attr);
                }
            }
        }

        public override SiteMapNode BuildSiteMap()
        {
            lock (this)
            {
                if (_root != null)
                {
                    return _root;
                }

                using (SqlConnection cnn = CreateConnection())
                {
                    using (SqlCommand cmd = cnn.CreateCommand())
                    {
                        cmd.CommandText = "CustomSiteMap_GetAll";
                        cmd.CommandType = CommandType.StoredProcedure;

                        //SqlCacheDependency dependency = new SqlCacheDependency(cmd);

                        using (IDataReader reader = cmd.ExecuteReader())
                        {
                            _indexId = reader.GetOrdinal("Id");
                            _indexTitle = reader.GetOrdinal("Title");
                            _indexDesc = reader.GetOrdinal("Description");
                            _indexUrl = reader.GetOrdinal("Url");
                            _indexImageUrl = reader.GetOrdinal("ImageUrl");
                            _indexRoles = reader.GetOrdinal("Roles");
                            _indexParent = reader.GetOrdinal("ParentId");
                            _indexIsDefault = reader.GetOrdinal("IsDefault");
                            _indexHasCount = reader.GetOrdinal("HasCount");
                            Dictionary<string, SiteMapNode> cache = new Dictionary<string, SiteMapNode>(128);

                            while (reader.Read())
                            {
                                SiteMapNode node = CreateSiteMapNodeFromDataReader(reader);

                                if (_root == null)
                                {
                                    _root = node;
                                }

                                if (reader.IsDBNull(_indexParent))
                                {
                                    try
                                    {
                                        AddNode(node, null);
                                    }
                                    catch
                                    {
                                    }
                                }
                                else
                                {
                                    string parentId = reader.GetInt32(_indexParent).ToString(CultureInfo.CurrentCulture);

                                    SiteMapNode parent = cache[parentId];
                                    try
                                    {
                                        AddNode(node, parent);
                                    }
                                    catch
                                    {
                                    }
                                }

                                cache.Add(node.Key, node);
                            }
                        }

                        //if (dependency != null)
                        //{
                        // HttpRuntime.Cache.Insert(_cacheDependencyName, new object(), dependency,
                        //Cache.NoAbsoluteExpiration, Cache.NoSlidingExpiration, CacheItemPriority.NotRemovable,
                        // new CacheItemRemovedCallback(OnSiteMapChanged));
                        //}
                    }
                }

                return _root;
            }
        }

        protected override SiteMapNode GetRootNodeCore()
        {
            BuildSiteMap();

            return _root;
        }

        public SiteMapNode CreateSiteMapNodeFromDataReader(IDataReader reader)
        {
            if (reader.IsDBNull(_indexId))
            {
                throw new ProviderException(_errmsg1);
            }

            int id = reader.GetInt32(_indexId);

            string title = reader.IsDBNull(_indexTitle) ? null : reader.GetString(_indexTitle).Trim();
            string description = reader.IsDBNull(_indexDesc) ? null : reader.GetString(_indexDesc).Trim();
            string url = reader.IsDBNull(_indexUrl) ? null : reader.GetString(_indexUrl).Trim();
            string imageUrl = reader.IsDBNull(_indexImageUrl) ? null : reader.GetString(_indexImageUrl).Trim();
            int parentId = reader.IsDBNull(_indexParent) ? int.MinValue : reader.GetInt32(_indexParent);
            string roles = reader.IsDBNull(_indexRoles) ? null : reader.GetString(_indexRoles).Trim();
            bool isDefault = reader.IsDBNull(_indexIsDefault) ? false : reader.GetBoolean(_indexIsDefault);
            bool hasCount = reader.IsDBNull(_indexHasCount) ? false : reader.GetBoolean(_indexHasCount);
            NameValueCollection attributes = new NameValueCollection();
            attributes.Add("Id", id.ToString(CultureInfo.CurrentCulture));

            if (!string.IsNullOrEmpty(url))
            {
                attributes.Add("OriginalUrl", url);

                if (url.Contains("?"))
                {
                    url += "&sitemapid=" + id.ToString(CultureInfo.CurrentCulture) + "&sitemapparentid=" + parentId.ToString(CultureInfo.CurrentCulture);
                }
                else
                {
                    url += "?sitemapid=" + id.ToString(CultureInfo.CurrentCulture) + "&sitemapparentid=" + parentId.ToString(CultureInfo.CurrentCulture);
                }
            }

            //url = (new SecureUrl(url)).ToString();

            attributes.Add("ImageUrl", imageUrl);
            attributes.Add("IsDefault", isDefault.ToString(CultureInfo.CurrentCulture));
            attributes.Add("HasCount",  hasCount.ToString(CultureInfo.CurrentCulture));

            string[] roleList = null;

            if (!string.IsNullOrEmpty(roles))
            {
                roleList = roles.Split(new char[] { ',', ';', '|' });
            }

            SiteMapNode node = new SiteMapNode(this, id.ToString(CultureInfo.CurrentCulture), url, title, description, roleList, attributes, null, null);
            return node;
        }

        private static SqlConnection CreateConnection()
        {
            SqlConnection cnn = new System.Data.SqlClient.SqlConnection(_connectionString);
            cnn.Open();

            return cnn;
        }

        void OnSiteMapChanged(string key, object item, CacheItemRemovedReason reason)
        {
            lock (this)
            {
                if (key == _cacheDependencyName && reason == CacheItemRemovedReason.DependencyChanged)
                {
                    // Refresh the site map
                    Clear();
                    _root = null;
                }
            }
        }

        public void ResetSiteMap()
        {
            Clear();
            _root = null;
        }
    }
}