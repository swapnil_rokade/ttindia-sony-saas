﻿using System;
using System.Diagnostics;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using TPS360.BusinessFacade;
using TPS360.Common.BusinessEntities;
using TPS360.Web.UI.Helper;
using System.Collections;
using System.IO;

namespace TPS360.Web.UI
{
    public class TemplateUserControl : UserControl   
    {
       
        #region Member Variables
        private IFacade _facade;

        private object m_Data = null;
        public object Data
        {
            get { return m_Data; }
            set { m_Data = value; }
        }

        private object m_DataValue = null;
        public object DataValue
        {
            get { return m_DataValue; }
            set { m_DataValue = value; }
        }

        private object m_JobPostingID = null;
        public object JobPostingID
        {
            get { return m_JobPostingID; }
            set { m_JobPostingID = value; }
        }

        private object m_StatusId = null;
        public object StatusId
        {
            get { return m_StatusId; }
            set { m_StatusId = value; }
        }

        private object m_NoteUpdatorName = null;
        public object NoteUpdatorName
        {
            get { return m_NoteUpdatorName; }
            set { m_NoteUpdatorName = value; }
        }
        private object m_NoteUpdateTime = null;
        public object NoteUpdateTime
        {
            get { return m_NoteUpdateTime; }
            set { m_NoteUpdateTime = value; }
        }
        #endregion

        #region Properties
        private int  m_CurrentJobPostingID =0;
        public int CurrentJobPostingId
        {
            set 
            {
                m_CurrentJobPostingID = value;
            }
            get { return m_CurrentJobPostingID; }
        }

        protected IFacade Facade
        {
            get
            {
                if (_facade == null)
                {
                    _facade = Context.Items[ContextConstants.Facade] as IFacade;
                }

                return _facade;
            }
        }
        protected Member CurrentMember
        {
            //[DebuggerStepThrough()]
            get
            {
                return Context.Items[ContextConstants.MEMBER] as Member;
            }
        }
        protected Hashtable SiteSetting
        {
            [DebuggerStepThrough()]
            get
            {
                return Context.Items[ContextConstants.SITESETTING] as Hashtable;
            }
        }


        private string m_StartupScript = string.Empty;

        public string StartupScript
        {
            get { return m_StartupScript; }
            set { m_StartupScript = value; }
        }

        private string m_CustomStyleSheet = string.Empty;

        public string CustomStyleSheet
        {
            get { return m_CustomStyleSheet; }
            set { m_CustomStyleSheet = value; }
        }
        #endregion

        #region Methods

        #endregion

       
    }
}