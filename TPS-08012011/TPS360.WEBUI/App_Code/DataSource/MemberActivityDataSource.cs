using System;
using System.Collections.Generic;
using System.ComponentModel;

using TPS360.BusinessFacade;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Utility;

namespace TPS360.Web.UI
{
    [DataObject(true)]
    public class MemberActivityDataSource : ObjectDataSourceBase
    {
        PagedResponse<MemberActivity> pageResponse = null;

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCount(int creatorId, DateTime createDate, int activityOn, int activityOnObjectId)
        {
            return pageResponse.TotalRow;
        }

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<MemberActivity> GetPaged(int creatorId,DateTime createDate, int activityOn, int activityOnObjectId, string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
                if (creatorId > 0)
                {
                    base.AppendParameter(pageRequest, "creatorId", creatorId.ToString());
                }
                if (activityOn > 0)
                {
                    base.AppendParameter(pageRequest, "activityOn", activityOn.ToString());
                }

                if (activityOnObjectId > 0)
                {
                    base.AppendParameter(pageRequest, "activityOnObjectId", activityOnObjectId.ToString());
                }

                if (createDate != null && createDate!=DateTime.MinValue)
                {
                    base.AppendParameter(pageRequest, "createDate", createDate.ToString("MM/dd/yyyy"));
                }
                 
                pageResponse = Facade.GetPagedMemberActivity(pageRequest);
                return pageResponse.Response as List<MemberActivity>;
            }
        }
    }
}