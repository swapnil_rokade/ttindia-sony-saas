﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.ComponentModel;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using System.Collections.Generic;


/// <summary>
/// Summary description for RequisitionNotesDataSource
/// </summary>

namespace TPS360.Web.UI
{
 [DataObject(true)]
    public class RequisitionNotesDataSource : ObjectDataSourceBase
{
    public RequisitionNotesDataSource()
    {}

    PagedResponse<RequisitionNotesEntry> pageResponse = null;
        
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCount(string JobPostingId)
        {
            return pageResponse.TotalRow;
        }

        
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<RequisitionNotesEntry> GetPaged(string JobPostingId,string sortExpression ,int startRowIndex, int maximumRows)
        {
            //using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
                pageRequest.Conditions.Add("JobPostingId", JobPostingId.ToString());
                pageResponse = Facade.GetAllReqNotesByJobPostingId(pageRequest);//JobPostingId);
                return pageResponse.Response as List<RequisitionNotesEntry>;
                //return List < RequisitionNotesEntry > reqNote = Facade.GetAllReqNotesByJobPostingId(JobPostingId);
            }
        }
        
    }
}

