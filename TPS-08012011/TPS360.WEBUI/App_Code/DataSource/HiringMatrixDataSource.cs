using System;
using System.Collections.Generic;
using System.ComponentModel;

using TPS360.BusinessFacade;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Utility;

namespace TPS360.Web.UI
{
    [DataObject(true)]
    public class HiringMatrixDataSource : ObjectDataSourceBase
    {
        PagedResponse<HiringMatrix> pageResponse = null;

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCount()
        {
            return pageResponse.TotalRow;
        }

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCount(string JobPostingId, string SelectionStepLookupId)
        {
            return pageResponse.TotalRow;
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<HiringMatrix> GetPaged(string JobPostingId, string SelectionStepLookupId, string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);

                pageRequest.Conditions.Add("JobPostingId", JobPostingId.ToString());
                if(SelectionStepLookupId !="0")
                pageRequest.Conditions.Add("SelectionStepLookupId", SelectionStepLookupId.ToString());

                pageResponse = Facade.GetMinPagedHiringMatrix (pageRequest);
                return pageResponse.Response as List<HiringMatrix>;
            }
        }

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<HiringMatrix> GetPagedAll(string JobPostingId, string SelectionStepLookupId, string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {

                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);

                pageRequest.Conditions.Add("JobPostingId", JobPostingId.ToString());
                if (SelectionStepLookupId != "0")
                    pageRequest.Conditions.Add("SelectionStepLookupId", SelectionStepLookupId.ToString());

                pageResponse = Facade.GetMinPagedHiringMatrix(pageRequest);
                return pageResponse.Response as List<HiringMatrix>;
            }
        }
    }
}