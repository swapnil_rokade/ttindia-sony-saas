/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: D:\Canarys\Project\TPS360\Working\Source\Solution\TPS360.WebUI\App_Code\DataSource\SkillDataSource.cs
    Description: his is used for member skill data access
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1            Feb-4-2009           Gopala Swamy           Defect ID:9090; Added Overloaded method for "GetSkillsByMemberId()"
 --------------------------------------------------------------------------------------------------------------------------------------------        
*/

using System;
using System.Collections.Generic;
using System.ComponentModel;

using TPS360.BusinessFacade;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Utility;

namespace TPS360.Web.UI
{
    [DataObject(true)]
    public class SkillDataSource : ObjectDataSourceBase
    {
        PagedResponse<Skill> pageResponse = null;

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCount()
        {
            return pageResponse.TotalRow;
        }

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<Skill> GetPaged(string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
                pageResponse = Facade.GetPagedSkill(pageRequest);
                return pageResponse.Response as List<Skill>;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)] //0.1 starts here 
        public IList<MemberSkill> GetSkillsByMemberId(int MemberId, string sortExpression)
        {
            using (new PerformanceBenchmark())
            {
             
                IList<MemberSkill> MemberSkills= Facade.GetAllMemberSkillByMemberId(MemberId,sortExpression);
                return MemberSkills;
            }
        }   //0.1 end here 
    }
}