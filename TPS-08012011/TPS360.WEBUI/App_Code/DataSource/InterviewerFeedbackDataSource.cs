/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName:       InterviewerFeedback.cs
    Description:    This is the objects source file
    Created By:     Prasanth Kumar G
    Created On:     20/Oct/2015
    Modification Log:
    --------------------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ---------------------------------------------------------------------------------------------------------------------------------------------
      0.1               24/Dec/2015         PRAVIN KHOT           ADD REPORT GET DATA FUNCTIONS
---------------------------------------------------------------------------------------------------------------------------------------------------        
*/

using System;
using System.Collections.Generic;
using System.ComponentModel;

using TPS360.BusinessFacade;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Utility;

namespace TPS360.Web.UI
{
    [DataObject(true)]
    public class InterviewerFeedbackDataSource : ObjectDataSourceBase
    {

        PagedResponse<InterviewResponse> pageResponse = null;
        PagedResponse<InterviewFeedback> pageResponse1 = null;

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public IList<InterviewFeedback> InterviewFeedbackGetPaged(int InterviewId, string EmailId, string SortOrder, string SortColumn, string sortExpression, int startRowIndex, int maximumRows)
        {


            using (new PerformanceBenchmark())
            {

                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);

                if (InterviewId > 0 && EmailId != "")
                {
                    pageRequest.Conditions.Add("InterviewId", InterviewId.ToString());
                    pageRequest.Conditions.Add("EmailId", EmailId.ToString());
                }



                pageResponse1 = Facade.InterviewerFeedback_GetPaged(pageRequest);
                return pageResponse1.Response as List<InterviewFeedback>;
            }

        }


        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int InterviewFeedbackGetListCount(int InterviewId, string EmailId, string SortOrder, string SortColumn)
        {
            return pageResponse1.TotalRow;
        }

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public IList<InterviewResponse> InterviewResponseGetPaged(int InterviewId, string InterviewerEmail, int AssessmentId,  string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
 
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);

                if (InterviewId > 0 && InterviewerEmail != "")
                {
                    pageRequest.Conditions.Add("InterviewId", InterviewId.ToString());
                    pageRequest.Conditions.Add("InterviewerEmail", InterviewerEmail.ToString());
                    pageRequest.Conditions.Add("AssessmentId", AssessmentId.ToString());
                }
               


                pageResponse = Facade.InterviewResponse_GetPaged(pageRequest);
                return pageResponse.Response as List<InterviewResponse>;
            }


            //IList<InterviewResponse> InterviewResponse = Facade.InterviewResponse_GetByInterviewId_Email(128, "gpkprashanth83@gmail.com");
            //return InterviewResponse;
        }


        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int InterviewResponseGetListCount(int InterviewId, string InterviewerEmail, int AssessmentId)
        {
            return pageResponse.TotalRow;
        }

        //*********************************add by pravin khot on 24/Dec/2015*************************************

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCount(int Cid)
        {
            return pageResponse1.TotalRow;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCount(string InterviewerName, string StartDate, string EndDate, string Cid, string CName)
        {
            return pageResponse1.TotalRow;
        }

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<InterviewFeedback> InterviewFeedbackReportGetPaged(string InterviewerName, string StartDate, string EndDate,
            string Cid, string CName, string sortExpression, int startRowIndex, int maximumRows)
        {
            DateTime t = DateTime.Now;
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
                //if (InterviewerName == "Please Select" || InterviewerName==string.Empty )
                //{
                //    InterviewerName = "";
                //}
                pageRequest.Conditions.Add("InterviewType", InterviewerName.ToString());

                pageRequest.Conditions.Add("StartDate", StartDate != null ? Convert.ToDateTime(StartDate.ToString()).ToString() : DateTime.MinValue.ToString());
                pageRequest.Conditions.Add("EndDate", EndDate != null ? Convert.ToDateTime(EndDate.ToString()).ToString() : DateTime.MinValue.ToString());

                pageRequest.Conditions.Add("Cid", Cid != null ? MiscUtil.RemoveScript(Cid.ToString()) : string.Empty);
                pageRequest.Conditions.Add("CName", CName != null ? MiscUtil.RemoveScript(CName.ToString()) : string.Empty);
                pageResponse1 = Facade.GetPagedInterviewFeedbackReport(pageRequest);
                return pageResponse1.Response as List<InterviewFeedback>;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<InterviewFeedback> InterviewFeedbackReportPrint(string InterviewId, string mail, string sortExpression, int startRowIndex, int maximumRows)
        {
            DateTime t = DateTime.Now;
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);

                pageRequest.Conditions.Add("Cid", InterviewId != null ? MiscUtil.RemoveScript(InterviewId.ToString()) : string.Empty);
                pageRequest.Conditions.Add("CName", mail != null ? MiscUtil.RemoveScript(mail.ToString()) : string.Empty);
                pageResponse1 = Facade.GetPagedInterviewFeedbackReportPrint(pageRequest);
                return pageResponse1.Response as List<InterviewFeedback>;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<InterviewFeedback> InterviewAssesReportPrint(string InterviewId, string mail, string sortExpression, int startRowIndex, int maximumRows)
        {
            DateTime t = DateTime.Now;
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);

                pageRequest.Conditions.Add("Cid", InterviewId != null ? MiscUtil.RemoveScript(InterviewId.ToString()) : string.Empty);
                pageRequest.Conditions.Add("CName", mail != null ? MiscUtil.RemoveScript(mail.ToString()) : string.Empty);
                pageResponse1 = Facade.GetPagedInterviewAssesReportPrint(pageRequest);
                return pageResponse1.Response as List<InterviewFeedback>;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<InterviewFeedback> InterviewFeedbackReportGetPaged(int CandidateId, string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
                if (CandidateId > 0)
                {
                    pageRequest.Conditions.Add("CandidateId", CandidateId.ToString());
                }
                // pageResponse = Facade.GetPagedMemberInterview(pageRequest);
                return pageResponse.Response as List<InterviewFeedback>;
            }
        }
        //*****************************************************************************************








        //public IList<QuestionBank> GetAllByCompanyID(int companyId, string sortExpression)
        //{
        //    IList<QuestionBank> QuestionBanks = Facade.GetAllQuestionBankByCompanyId(companyId, sortExpression);

        //    return QuestionBanks;

        //}
        //public IList<QuestionBank> GetAllByProjectID(int projectId, string sortExpression)//0.1
        //{
            

        //    IList<QuestionBank> QuestionBanks = Facade.GetAllQuestionBankByProjectId(projectId, sortExpression);//0.1 

        //    return QuestionBanks;
        //}

        public string  AppendSortParameter(string sortExpression)
        {
            string strOutput = "";
            if (!string.IsNullOrEmpty(sortExpression))
            {
                string[] part = sortExpression.Split(' ');

                if (part != null && part.Length > 0)
                {
                    strOutput = part[0];

                    if (part.Length > 1 && part[1] != null)
                    {
                        strOutput = part[1];
                    }
                    else
                    {
                        strOutput = UIConstants.SORT_ORDER_ASC;
                    }
                }
            }
            return strOutput;
        }

    }
}