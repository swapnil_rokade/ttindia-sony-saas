using System;
using System.Collections.Generic;
using System.ComponentModel;

using TPS360.BusinessFacade;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Utility;

namespace TPS360.Web.UI
{
    [DataObject(true)]
    public class CompanyDataSource : ObjectDataSourceBase
    {
        PagedResponse<Company> pageResponse = null;

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCount(string companyName, string industryType, string employeeId,  string country, string state, string city, string fromCreateDate, string toCreateDate, string fromUpdateDate, string toUpdateDate, string companyContact,string companystatus,string SortOrder)
        {
            return pageResponse.TotalRow;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCount(string companyName, string endClientType, string industryType, string employeeId, string companyStatus, string campaignId, string country, string state, string city, string fromCreateDate, string toCreateDate, string fromUpdateDate, string toUpdateDate, string companyContact)
        {
            return pageResponse.TotalRow;
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<Company> GetPaged(string companyName, string endClientType, string industryType, string employeeId, string companyStatus, string campaignId, string country, string state, string city, string fromCreateDate, string toCreateDate, string fromUpdateDate, string toUpdateDate, string companyContact, string sortExpression, int startRowIndex, int maximumRows)
        {
            return this.GetPaged(null, companyName, endClientType, industryType, employeeId, companyStatus, campaignId, country, state, city, fromCreateDate, toCreateDate, fromUpdateDate, toUpdateDate, null, companyContact, null, sortExpression, startRowIndex, maximumRows);
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<Company> GetPaged(string companyName, string industryType, string employeeId, string country, string state, string city, string fromCreateDate, string toCreateDate, string fromUpdateDate, string toUpdateDate, string companyContact,string companystatus,string SortOrder, string sortExpression, int startRowIndex, int maximumRows)
        {
            return this.GetPaged(null,MiscUtil .RemoveScript ( companyName), null, industryType, employeeId,companystatus ,null, country, state,MiscUtil .RemoveScript ( city), fromCreateDate, toCreateDate, fromUpdateDate, toUpdateDate, null,MiscUtil .RemoveScript ( companyContact), null,SortOrder , sortExpression, startRowIndex, maximumRows);
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCount(string IsPending, string pendingStatus)
        {
            return pageResponse.TotalRow;
        }

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<Company> GetPaged(string IsPending, string pendingStatus,  string sortExpression, int startRowIndex, int maximumRows)
        {
            return this.GetPaged(null, null, null, null, null, null, null, null, null, null, null, null, null, null, IsPending, null, pendingStatus, sortExpression, startRowIndex, maximumRows);
        }

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetCompanyCampaignListCount(string employeeId, string campaignId)
        {
            return pageResponse.TotalRow;
        }

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<Company> GetPagedCompanyCampaign(string employeeId, string campaignId, string sortExpression, int startRowIndex, int maximumRows)
        {
            return this.GetPaged(null, null, null, null, employeeId, null, campaignId, null, null, null, null, null, null, null, null, null, null, sortExpression, startRowIndex, maximumRows);
        }

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCountForDashboard(string companyName, string employeeId, string companyStatus, string country, string state, string city, string companyContact)
        {
            return pageResponse.TotalRow;
        }

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<Company> GetPagedForDashboard(string companyName, string employeeId, string companyStatus, string country, string state, string city, string companyContact, string sortExpression, int startRowIndex, int maximumRows)
        {
            return this.GetPaged(null, companyName, null, null, employeeId, companyStatus, null, country, state, city, null, null, null, null, null, companyContact, null, sortExpression, startRowIndex, maximumRows);
        }

        public List<Company> GetPaged(string companyId, string companyName, string endClientType, string industryType, string employeeId, string companyStatus, string campaignId, string country, string state, string city, string fromCreateDate, string toCreateDate, string fromUpdateDate, string toUpdateDate, string IsPending, string companyContact, string pendingStatus, string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {

                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);

                base.AppendParameter(pageRequest, "companyid", companyId);
                base.AppendParameter(pageRequest, "companyname", companyName);
                base.AppendParameter(pageRequest, "companycontact", companyContact);

                if (!string.IsNullOrEmpty(endClientType) && !string.Equals(endClientType, UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT))
                {
                    if (string.Equals(endClientType, "EndClient"))
                    {
                        base.AppendParameter(pageRequest, "endclient", "1");
                    }
                    else if (string.Equals(endClientType, "Tier1Client"))
                    {
                        base.AppendParameter(pageRequest, "tier1client", "1");
                    }
                    else if (string.Equals(endClientType, "Tier2Client"))
                    {
                        base.AppendParameter(pageRequest, "tier2client", "1");
                    }
                }

                if (!string.IsNullOrEmpty(industryType) && !string.Equals(industryType, UIConstants.DROP_DOWNL_ITEM_ALL))
                {
                    base.AppendParameter(pageRequest, "industrytype", industryType);
                }

                if (!string.IsNullOrEmpty(employeeId) && !string.Equals(employeeId, UIConstants.DROP_DOWNL_ITEM_ALL))
                {
                    base.AppendParameter(pageRequest, "employeeid", employeeId);
                }

                if (!string.IsNullOrEmpty(country) && !string.Equals(country, UIConstants.DROP_DOWNL_ITEM_ALL))
                {
                    base.AppendParameter(pageRequest, "country", country);
                }

                if (!string.IsNullOrEmpty(state) && !string.Equals(state, UIConstants.DROP_DOWNL_ITEM_ALL))
                {
                    base.AppendParameter(pageRequest, "state", state);
                }

                if (!string.IsNullOrEmpty(companyStatus))
                {
                    base.AppendParameter(pageRequest, "companystatus", companyStatus);
                }

                base.AppendParameter(pageRequest, "pendingstatus", pendingStatus);

                if (!string.IsNullOrEmpty(campaignId) && !string.Equals(campaignId, UIConstants.DROP_DOWNL_ITEM_ALL))
                {
                    base.AppendParameter(pageRequest, "campaignid", campaignId);
                }

                if (!string.IsNullOrEmpty(IsPending))
                {
                    base.AppendParameter(pageRequest, "ispending", "1");
                }

                base.AppendParameter(pageRequest, "city", city);
                base.AppendCommonParameter(pageRequest, fromCreateDate, toCreateDate, fromUpdateDate, toUpdateDate);

                pageResponse = Facade.GetPagedCompany(pageRequest);
                return pageResponse.Response as List<Company>;
            }
        }

        public List<Company> GetPaged(string companyId, string companyName, string endClientType, string industryType, string employeeId, string companyStatus, string campaignId, string country, string state, string city, string fromCreateDate, string toCreateDate, string fromUpdateDate, string toUpdateDate, string IsPending, string companyContact, string pendingStatus, string SortOrder, string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {

                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
                pageRequest.SortOrder = SortOrder;
                base.AppendParameter(pageRequest, "companyid", companyId);
                base.AppendParameter(pageRequest, "companyname", companyName);
                base.AppendParameter(pageRequest, "companycontact", companyContact);

                if (!string.IsNullOrEmpty(endClientType) && !string.Equals(endClientType, UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT))
                {
                    if (string.Equals(endClientType, "EndClient"))
                    {
                        base.AppendParameter(pageRequest, "endclient", "1");
                    }
                    else if (string.Equals(endClientType, "Tier1Client"))
                    {
                        base.AppendParameter(pageRequest, "tier1client", "1");
                    }
                    else if (string.Equals(endClientType, "Tier2Client"))
                    {
                        base.AppendParameter(pageRequest, "tier2client", "1");
                    }
                }

                if (!string.IsNullOrEmpty(industryType) && !string.Equals(industryType, UIConstants.DROP_DOWNL_ITEM_ALL))
                {
                    base.AppendParameter(pageRequest, "industrytype", industryType);
                }

                if (!string.IsNullOrEmpty(employeeId) && !string.Equals(employeeId, UIConstants.DROP_DOWNL_ITEM_ALL))
                {
                    base.AppendParameter(pageRequest, "employeeid", employeeId);
                }

                if (!string.IsNullOrEmpty(country) && !string .Equals (country ,"0"))
                {
                    base.AppendParameter(pageRequest, "country", country);
                }

                if (!string.IsNullOrEmpty(state)  && state.Trim () !="0")
                {
                    base.AppendParameter(pageRequest, "state", state);
                }

                if (!string.IsNullOrEmpty(companyStatus))
                {
                    base.AppendParameter(pageRequest, "companystatus", companyStatus);
                }

                base.AppendParameter(pageRequest, "pendingstatus", pendingStatus);

                if (!string.IsNullOrEmpty(campaignId) && !string.Equals(campaignId, UIConstants.DROP_DOWNL_ITEM_ALL))
                {
                    base.AppendParameter(pageRequest, "campaignid", campaignId);
                }

                if (!string.IsNullOrEmpty(IsPending))
                {
                    base.AppendParameter(pageRequest, "ispending", "1");
                }

                base.AppendParameter(pageRequest, "city", city);
                base.AppendCommonParameter(pageRequest, fromCreateDate, toCreateDate, fromUpdateDate, toUpdateDate);

                pageResponse = Facade.GetPagedCompany(pageRequest);
                return pageResponse.Response as List<Company>;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCounts(string quickSearchKeyWord)
        {
            return pageResponse.TotalRow;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCounts(string quickSearchKeyWord,string CreatorId)
        {
            return pageResponse.TotalRow;
        }

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCounts(string quickSearchKeyWord, string CreatorId, string SortOrder)
        {
            return pageResponse.TotalRow;
        }

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<Company> GetPagedForQuickSearch(string quickSearchKeyWord, string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
                if (!string.IsNullOrEmpty(quickSearchKeyWord))
                {
                    pageRequest.Conditions.Add("quickSearchKeyWord", quickSearchKeyWord);
                }
                pageResponse = Facade.GetPagedCompany(pageRequest);
                return pageResponse.Response as List<Company>;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<Company> GetPagedForQuickSearch(string quickSearchKeyWord,string CreatorId, string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
                if (!string.IsNullOrEmpty(quickSearchKeyWord))
                {
                    pageRequest.Conditions.Add("quickSearchKeyWord", quickSearchKeyWord);
                }
                if (!string.IsNullOrEmpty(CreatorId))
                {
                    pageRequest.Conditions.Add("CreatorId", CreatorId);
                }
                pageResponse = Facade.GetPagedCompany(pageRequest);
                return pageResponse.Response as List<Company>;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<Company> GetPagedForQuickSearch(string quickSearchKeyWord, string CreatorId,string SortOrder, string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
                pageRequest.SortOrder = SortOrder ;
                if (!string.IsNullOrEmpty(quickSearchKeyWord))
                {
                    pageRequest.Conditions.Add("quickSearchKeyWord", quickSearchKeyWord);
                }
                if (!string.IsNullOrEmpty(CreatorId))
                {
                    pageRequest.Conditions.Add("CreatorId", CreatorId);
                }
                pageResponse = Facade.GetPagedCompany(pageRequest);
                return pageResponse.Response as List<Company>;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public List<CompanyContact> GetCompanyContact(string companyId)
        {
            return Facade.GetAllCompanyContactByComapnyId(Convert.ToInt32(companyId)) as List<CompanyContact>;
        }

        
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public IList<CompanyAssignedManager> GetAllCompanyAssignedManagerByCompanyId(int companyId, string sortExpression)
        {
            using (new PerformanceBenchmark())
            {
                if (companyId > 0)
                    return Facade.GetAllCompanyAssignedManagerByCompanyId(companyId, sortExpression);
                else
                    return null;
            }
        }
    }
}