  /*Modification Log:
    -----------------------------------------------------------------------------------------------------------    Ver.No.             Date                Author              MOdification
    ----------------------------------------------------------------------------------------------------------- *  0.1              26-Feb-2009          Sandeesh              Defect Id : 9671 - Added  new method to                                                                          implement Sorting
---------------------------------------------------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;

using TPS360.BusinessFacade;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Utility;

namespace TPS360.Web.UI
{
    [DataObject(true)]
    public class MemberJobCartDataSource : ObjectDataSourceBase
    {
        PagedResponse<MemberJobCart> pageResponse = null;

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCount(int memberId)
        {
            return pageResponse.TotalRow;
        }

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<MemberJobCart> GetPaged(int memberId, string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
                if (memberId != 0)
                {
                    pageRequest.Conditions.Add("MemberId", memberId.ToString());
                }

                pageResponse = Facade.GetPagedMemberJobCart(pageRequest);
                return pageResponse.Response as List<MemberJobCart>;
            }
        }

		//Candidate Status in Employee Referral Portal.
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<MemberJobCart> GetPagedForEmployeeReferal(int memberId, string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
                if (memberId != 0)
                {
                    pageRequest.Conditions.Add("MemberId", memberId.ToString());
                }

                pageResponse = Facade.GetPagedMemberJobCartForEmployeeReferal(pageRequest);
                return pageResponse.Response as List<MemberJobCart>;
            }
        }
        //0.1 start


        //Submitted candidate score
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public IList<MemberJobCart> GetPagedForSubmittedCandidateInterviewScore(int memberId, int selectionStep,string sortExpression, int startRowIndex, int maximumRows)
        {
            if (memberId != 0)
            {
               IList<MemberJobCart> memberJobCart = Facade.GetAllSubmittedCandidateInterviewScore(memberId, (int)MemberSelectionStep.LevelIIIInterview, sortExpression);
                return memberJobCart;
            }
            else
            {
                IList<MemberJobCart> memberJobCart=null;
                return memberJobCart ;
            }

                     
        }
         //0.1 end



    }
}