﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

using TPS360.BusinessFacade;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Utility;

namespace TPS360.Web.UI
{
    [DataObject(true)]
    public class OfferDataSource : ObjectDataSourceBase
    {
        //PagedResponse<JobPosting> pageResponse = null;

        PagedResponse<MemberHiringDetails> pageResponse = null;
        PagedResponse<Submission> pageResponse1 = null;

        //Vishal Tripathy start
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetOfferedListCount(int menberId)
        {
            return pageResponse.TotalRow;
        }



        public int GetOfferedHRListCount(int memberId)
        {
            return pageResponse.TotalRow;

        }
        //Vishal Tripathy Ends

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<Submission> GetPagedForDashboard(bool DashBoardSubmission, string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);

                if (DashBoardSubmission)
                {
                    pageRequest.Conditions.Add("DashBoardSubmission", DashBoardSubmission.ToString());
                }
                pageResponse1 = Facade.GetPagedDashBoardSubmission(pageRequest);
                return pageResponse1.Response as List<Submission>;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<Submission> GetPagedForDashboard(bool DashBoardSubmission, int MemberId, string sortExpression, int startRowIndex, int maximumRows)
        {

            RequisitionBasePage rPage = new RequisitionBasePage();
            TPS360.Web.UI.BaseControl b = new BaseControl();

            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);

                if (DashBoardSubmission)
                {
                    pageRequest.Conditions.Add("DashBoardSubmission", DashBoardSubmission.ToString());
                }
                if (MemberId > 0)
                {
                    pageRequest.Conditions.Add("MemberId", MemberId.ToString());
                }
                pageResponse1 = Facade.GetPagedDashBoardSubmission(pageRequest);
                return pageResponse1.Response as List<Submission>;
            }
        }
        //Requisition Search
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCountForVendorPortal(string JobTitle, string ReqCode, string City, string StateID, string CountryID, string VendorId)
        {
            return pageResponse.TotalRow;
        }
        //*************************************************
        //Vishal Tripathy start
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<MemberHiringDetails> GetPagedOfferedList(int memberId, string approvalType, string SortColumn, string sortOrder, string sortExpression, string startRowIndex, string maximumRows, string status)
        {
            using (new PerformanceBenchmark())
            {

                //PagedRequest pageRequest = base.PreparePagedRequest("asc", 0, 10);
                PagedRequest pageRequest = base.PreparePagedRequest(sortOrder, Convert.ToInt32(startRowIndex), Convert.ToInt32(maximumRows));
                if (memberId > 0)
                {
                    pageRequest.Conditions.Add("MemberId", memberId.ToString());

                }

                pageResponse = Facade.GetPagedMemberHiringDetails(pageRequest, memberId, approvalType, SortColumn, status);

                return pageResponse.Response as List<MemberHiringDetails>;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<MemberHiringDetails> GetPagedHROfferedList(int memberId, string approvalType, string SortColumn, string sortOrder, string sortExpression, string startRowIndex, string maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortOrder, Convert.ToInt32(startRowIndex), Convert.ToInt32(maximumRows));
                if (memberId > 0)
                {
                    pageRequest.Conditions.Add("MemberId", memberId.ToString());
                }
                pageResponse = Facade.GetPagedMemberHRApprovalDetails(pageRequest, memberId, approvalType, SortColumn);

                return pageResponse.Response as List<MemberHiringDetails>;
            }
        }
        public List<MemberHiringDetails> GetPagedDivisionHeadApprovalList(int memberId, string approvalType, string SortColumn, string sortOrder, string sortExpression, string startRowIndex, string maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortOrder, Convert.ToInt32(startRowIndex), Convert.ToInt32(maximumRows));
                if (memberId > 0)
                {
                    pageRequest.Conditions.Add("MemberId", memberId.ToString());
                }
                pageResponse = Facade.GetPagedDivisionHeadApprovalList(pageRequest, memberId, approvalType, SortColumn);

                return pageResponse.Response as List<MemberHiringDetails>;
            }
        }





        //*************************************************
        //Vishal Tripathy end

      
    }
}
