﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using TPS360.BusinessFacade;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Utility;

/// <summary>
/// Summary description for MemberHiringDetails
/// </summary>
namespace TPS360.Web.UI
{
    [DataObject(true)]
    public class MemberPendingJoinersDataSource:ObjectDataSourceBase
    {
        PagedResponse<MemberPendingJoiners> pageResponse = null;

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCount(int MemberId)
        {
            return pageResponse.TotalRow;
        }

        public List<MemberPendingJoiners> GetPaged(int MemberId, string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
                if (MemberId > 0)
                {
                    pageRequest.Conditions.Add("MemberId", MemberId.ToString());
                }
                pageResponse = Facade.MemberPendingJoiners_GetPaged (pageRequest);
                return pageResponse.Response as List<MemberPendingJoiners>;
            }
        }
    }
}
