﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName:       CandidateJoiningDataSource.cs
    Description:    This is the objects source file
    Created By:  Kanchan Yeware   
    Created On:  09-Aug-2016  
    Modification Log:
*/

using System;
using System.Collections.Generic;
using System.ComponentModel;

using TPS360.BusinessFacade;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Utility;

/// <summary>
/// Summary description for CandidateJoiningDataSource
/// </summary>
namespace TPS360.Web.UI
{
    [DataObject(true)]
    public class CandidateJoiningDataSource : ObjectDataSourceBase
    {
        PagedResponse<CandidateJoiningReport> pageResponse = null;
        PagedResponse<DynamicDictionary> pageResponse1 = null;
        PagedResponse<CandidateSourcingInfo> pageResponse2 = null;   

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<CandidateJoiningReport> GetCandidateJoiningReport1(DateTime addedFrom, DateTime addedTo, string SuperVisoryCode, int MemberId, string SortOrder, string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
                pageRequest.SortOrder = SortOrder;
                if (MemberId > 0)
                {
                    pageRequest.Conditions.Add("MemberId", MemberId.ToString());
                }
                pageResponse = Facade.GetCandidateJoiningReport1(pageRequest, addedFrom, addedTo, SuperVisoryCode);
                return pageResponse.Response as List<CandidateJoiningReport>;
            }
        }

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCount(DateTime addedFrom, DateTime addedTo, string SuperVisoryCode, int MemberId, string SortOrder, string sortExpression, int startRowIndex, int maximumRows)
        {
            return pageResponse.TotalRow;
        }

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<CandidateJoiningReport> GetCandidateJoiningReport2(DateTime addedFrom, DateTime addedTo, string SuperVisoryCode, int MemberId, string SortOrder, string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
                pageRequest.SortOrder = SortOrder;
                if (MemberId > 0)
                {
                    pageRequest.Conditions.Add("MemberId", MemberId.ToString());
                }
                pageResponse = Facade.GetCandidateJoiningReport2(pageRequest, addedFrom, addedTo, SuperVisoryCode);
                return pageResponse.Response as List<CandidateJoiningReport>;
            }
        }

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCount1(DateTime addedFrom, DateTime addedTo, string SuperVisoryCode, int MemberId, string SortOrder, string sortExpression, int startRowIndex, int maximumRows)
        {
            return pageResponse.TotalRow;
        }

        public CandidateJoiningDataSource()
        {           
        }
    }
}
