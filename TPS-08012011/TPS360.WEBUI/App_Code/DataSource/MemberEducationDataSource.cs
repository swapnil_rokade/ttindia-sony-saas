using System;
using System.Collections.Generic;
using System.ComponentModel;

using TPS360.BusinessFacade;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Utility;

namespace TPS360.Web.UI
{
    [DataObject(true)]
    public class MemberEducationDataSource : ObjectDataSourceBase
    {
        PagedResponse<MemberEducation> pageResponse = null;

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCount()
        {
            return pageResponse.TotalRow;
        }

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCount(int memberId)
        {
            return pageResponse.TotalRow;
        }

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<MemberEducation> GetPaged(string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
                pageResponse = Facade.GetPagedMemberEducation(pageRequest);
                return pageResponse.Response as List<MemberEducation>;
            }
        }


        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<MemberEducation> GetPagedByMemberId(int memberId, string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
                if (memberId != 0)
                {
                    pageRequest.Conditions.Add("MemberId", memberId.ToString());
                }
              
                pageResponse = Facade.GetPagedMemberEducationByMemberId(pageRequest);
                return pageResponse.Response as List<MemberEducation>;
            }
        }
    }
}