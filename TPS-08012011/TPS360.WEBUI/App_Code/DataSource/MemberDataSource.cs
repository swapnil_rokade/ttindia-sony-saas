using System;
using System.Collections.Generic;
using System.ComponentModel;

using TPS360.BusinessFacade;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Utility;

namespace TPS360.Web.UI
{
    [DataObject(true)]
    public class MemberDataSource : ObjectDataSourceBase
    {
        PagedResponse<Member> pageResponse = null;
        PagedResponse<Candidate > pageResponses = null;
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCount()
        {
            return pageResponse.TotalRow;
        }

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCount(int creatorId, string roleName)
        {
            return pageResponse.TotalRow;
        }

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<Member> GetPaged(string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
                pageResponse = Facade.GetPagedMember(pageRequest);
                return pageResponse.Response as List<Member>;
            }
        }

        //Get Candidate By MemberGroupId

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCount(string memberGroupId,string SortOrder)
        {
            return pageResponses.TotalRow;
        }

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<Candidate> GetPagedByMemberGroupId(string memberGroupId,string SortOrder, string sortExpression, int startRowIndex, int maximumRows)
        {
            
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
                pageRequest.SortOrder = SortOrder;
                if (memberGroupId != "")
                {
                    pageRequest.Conditions.Add("MemberGroupId", memberGroupId.ToString());
                }

                pageResponses = Facade.GetPagedMemberByMemberGroupId(pageRequest);
                return pageResponses.Response as List<Candidate>;
            }
        }

        //end candidate

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<Member> GetPagedByRoleNameAndCreatorId(int creatorId, string roleName,string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
                if (creatorId != 0)
                {
                    pageRequest.Conditions.Add("CreatorId", creatorId.ToString());
                }

                if (!String.IsNullOrEmpty(roleName))
                {
                    pageRequest.Conditions.Add("RoleName", roleName);
                }
                
                pageResponse = Facade.GetPagedMemberByCreatorIdAndRoleName(pageRequest);
                return pageResponse.Response as List<Member>;
            }
        }
    }
}