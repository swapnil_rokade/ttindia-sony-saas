using System;
using System.Collections.Generic;
using System.ComponentModel;

using TPS360.BusinessFacade;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Utility;

namespace TPS360.Web.UI
{
    [DataObject(true)]
    public class MemberInterviewDataSource : ObjectDataSourceBase
    {
        PagedResponse<MemberInterview> pageResponse = null;

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCount(int CandidateId)
        {
            return pageResponse.TotalRow;
        }

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCountForDashboard(int MemberId)
        {
            return pageResponse.TotalRow;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCount(string InterviewType, string StartDate, string EndDate, string StartStartTime,
            string StartEndTime, string EndStartTime, string EndEndTime, string Requisition, string Client, string ClientInterviewers, string InternalInterviewers, string Location)
        {
            return pageResponse.TotalRow;
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<MemberInterview> GetPaged(string InterviewType, string StartDate, string EndDate, string StartStartTime,
            string StartEndTime, string EndStartTime, string EndEndTime, string Requisition, string Client, string ClientInterviewers, string InternalInterviewers, string Location, string sortExpression, int startRowIndex, int maximumRows)
        {
            DateTime t = DateTime.Now;
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);

                pageRequest.Conditions.Add("InterviewType", InterviewType.ToString ());
                pageRequest.Conditions.Add("StartDate", StartDate != null ? Convert.ToDateTime(StartDate.ToString()).ToString() : DateTime.MinValue.ToString());
                pageRequest.Conditions.Add("EndDate", EndDate != null ? Convert.ToDateTime(EndDate.ToString()).ToString() : DateTime.MinValue.ToString());
                pageRequest.Conditions.Add("StartStartTime", StartStartTime!=null ? StartStartTime .ToString () : string .Empty );
                pageRequest.Conditions.Add("StartEndTime", StartEndTime != null ? StartEndTime.ToString() : string.Empty);

                pageRequest.Conditions.Add("EndStartTime", EndStartTime != null ? EndStartTime.ToString() : string.Empty);
                pageRequest.Conditions.Add("EndEndTime", EndEndTime != null ? EndEndTime.ToString() : string.Empty);
                pageRequest.Conditions.Add("Requisition", Requisition.ToString());

                pageRequest.Conditions.Add("Client", Client.ToString());
                pageRequest.Conditions.Add("ClientInterviewers", ClientInterviewers.ToString());
                pageRequest.Conditions.Add("InternalInterviewers", InternalInterviewers.ToString());
                pageRequest.Conditions.Add("Location", Location!=null ?MiscUtil .RemoveScript ( Location.ToString()) : string .Empty );
                pageResponse = Facade.GetPagedInterviewReport(pageRequest);
                return pageResponse.Response as List<MemberInterview>;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<MemberInterview> GetPaged(int CandidateId, string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
                if (CandidateId > 0)
                {
                    pageRequest.Conditions.Add("CandidateId", CandidateId.ToString());
                }
                pageResponse = Facade.GetPagedMemberInterview(pageRequest);
                return pageResponse.Response as List<MemberInterview>;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="memberId">Participant</param>
        /// <param name="status">Interview status</param>
        /// <param name="managerId">Interviewer</param>
        /// <param name="memberType">Inteview scheduled for candidate or consultant</param>
        /// <param name="sortExpression"></param>
        /// <param name="startRowIndex"></param>
        /// <param name="maximumRows"></param>
        /// <returns></returns>
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<MemberInterview> GetPagedForDashboard(int MemberId,string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);

                if (MemberId > 0)
                {
                    pageRequest.Conditions.Add("MemberId", MemberId.ToString());
                }
                else if (MemberId == -1)
                    pageRequest.Conditions.Add("MemberId", string.Empty);

                pageResponse = Facade.GetPagedMemberInterviewForDashboard(pageRequest);
                return pageResponse.Response as List<MemberInterview>;
               
                
            }
        }
    }
}