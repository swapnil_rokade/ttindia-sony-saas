using System;
using System.Collections.Generic;
using System.ComponentModel;

using TPS360.BusinessFacade;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Utility;

namespace TPS360.Web.UI
{
    [DataObject(true)]
    public class CustomRoleDataSource : ObjectDataSourceBase
    {
        PagedResponse<CustomRole> pageResponse = null;

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCount()
        {
            return pageResponse.TotalRow;
        }

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<CustomRole> GetPaged(string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
                pageResponse = Facade.GetPagedCustomRole(pageRequest);
                return pageResponse.Response as List<CustomRole>;
            }
        }
    }
}