﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: BasicInfoEditor.ascx.cs
    Description:
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.         Date                Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
     0.1           23/feb/2016         pravin khot         Code added by pravin khot on 23/Feb/2016 [Using Hide workflow and publish section for specific roles]
 * ------------------------------------------------------------------------------------------------------------------------------------------- 
*/

using System;
using System.Web;
using System.Web.UI.WebControls;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using System.Collections.Generic;
using System.Collections;
using TPS360.BusinessFacade;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using TPS360.Web.UI.Helper;
using System.Linq;
using System.Web.UI;


namespace TPS360.Web.UI.Requisition
{
    public partial class BasicInfoEditor : RequisitionBasePage
    {

        protected void btnPublish_Click(object sender, EventArgs e)
        {

            //string script = "alert(\"Hello!\");";
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "ServerControlScript", script, true);
        
            JobPosting jobPosting = null;
            if (CurrentJobPostingId > 0) jobPosting = CurrentJobPosting;
            else jobPosting = new JobPosting();
            jobPosting = reqEditor.BuildJobPosting(jobPosting);
            jobPosting = reqDescription.BuildJobDescription(jobPosting);
            jobPosting = reqWorkflow.BuildJobPosting(jobPosting);
            jobPosting = reqPublish.BuildJobPostingPreviewPublish(jobPosting);
            
            //********************************************************************
            //Rupesh Kadam Start
            //if (btnPublish.Text == "Publish Now")
            if (btnPublish.Text == "Submit")
            //Rupesh Kadam End
            //********************************************************************            
            {
                IList<GenericLookup> RequisitionStatusList = Facade.GetAllGenericLookupByLookupTypeAndName(LookupType.RequisitionStatusValues, JobStatus.Approval.ToString());//JobStatus.Open.ToString());
                if (RequisitionStatusList != null)
                    jobPosting.JobStatus = RequisitionStatusList[0].Id;
                jobPosting.ActivationDate = DateTime.Now;

            }

            JobPosting newJobPosting = new JobPosting();
              //Added by pravin khot on 19/Jan/2016**********
            string AdminEmailId =string.Empty ;
            int AdminMemberid =0;
            SiteSetting siteSetting = Facade.GetSiteSettingBySettingType((int)SettingType.SiteSetting);
            if (siteSetting != null)
            {
                Hashtable siteSettingTable = ObjectEncrypter.Decrypt<Hashtable>(siteSetting.SettingConfiguration);
                AdminEmailId = siteSettingTable[DefaultSiteSetting.AdminEmail.ToString()].ToString();
                AdminMemberid = Facade.GetMemberIdByEmail(AdminEmailId);
                jobPosting.AdminMemberid = AdminMemberid;
            }
            //************END********************
            if (jobPosting.Id == 0)
            {
                newJobPosting = Facade.AddJobPosting(jobPosting);
            }
            else
            {
                //*******************Suraj Adsule******Start********For********20160908*********//
                string roleName = Facade.GetCustomRoleNameByMemberId(CurrentMember.Id);
                if (roleName.ToLower() == "hr manager")
                {
                    newJobPosting = Facade.UpdateJobPostingForIJPApproval(jobPosting);
                    newJobPosting = Facade.UpdateJobPosting_ResourceDate(jobPosting);
                } //*******************Suraj Adsule******Start********For********20160914*********//
                else if (roleName.ToLower() == "recruitment manager")
                {
                    newJobPosting = Facade.UpdateJobPosting_RecruiterManager(jobPosting);
                    newJobPosting = Facade.UpdateJobPosting_ResourceDate(jobPosting);
                }
                //*********************Suraj Adsule******End********For ********20160914********//
                else
                {
                    //*********************Suraj Adsule******End********For ********20160908********//
                    newJobPosting = Facade.UpdateJobPosting(jobPosting);
                }
            }
            divReqTitle.InnerText = newJobPosting.JobTitle + " - " + newJobPosting.JobPostingCode;
            //reqEditor.SaveJobPostingDocument(newJobPosting.Id);
            int BUmanagerId = 0;
            try
            {
                BUmanagerId = Facade.GetMemberIdByCompanyContact(newJobPosting.ClientContactId);
            }
            catch (Exception ex)
            {

            }

            reqWorkflow.SaveRecruiters(newJobPosting.Id, BUmanagerId);
            reqPublish.SaveAgent(newJobPosting);
            if (btnPublish.Text == "Publish Now")
            {
                MiscUtil.EventLogForRequisition(EventLogForRequisition.RequisitionPublished, newJobPosting.Id, CurrentMember.Id, Facade);
                reqPublish.PublishRequisition(newJobPosting,1);

                string javaScript = string.Format("test(\"{0}\")", "MyRequisitionList.aspx");

                ScriptManager.RegisterStartupScript(upReq, typeof(string), "alertscipt", javaScript, true);
            }
            else
            {
                //MiscUtil.ShowMessage(lblMessage, "Successfully saved changes for Requisition.", false);
                reqPublish.PublishRequisition(newJobPosting, 0);
                string javaScript = string.Format("test(\"{0}\")", "MyRequisitionList.aspx");

                ScriptManager.RegisterStartupScript(upReq, typeof(string), "alertscipt", javaScript, true);
            }
            

            //string message = "Requisition saved successfully.Will Redirect to My Requisition Page.";
            //System.Text.StringBuilder sb = new System.Text.StringBuilder();
            //sb.Append("<script type = 'text/javascript'>");
            //sb.Append("window.onload=function(){");
            //sb.Append("alert('");
            //sb.Append(message);
            //sb.Append("')};");
            //sb.Append("</script>");
            //ClientScript.RegisterClientScriptBlock(this.GetType(), "alert", sb.ToString());
          
            //ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Insert is successfull')", true);

            //string javaScript = string.Format("test(\"{0}\")", "MyRequisitionList.aspx");

          //  ScriptManager.RegisterStartupScript(upReq, typeof(string), "alertscipt", javaScript, true);

          
           //Response.Redirect(UrlConstants.Requisition.JOB_POSTING_MY_REQUISITION_LIST_PAGE);
          
          
            
            
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {                
                //********************************************************************
                //Rupesh Kadam Start
                HF_Disable.Value = DisableJobPosting;
                //Rupesh Kadam End
                //********************************************************************
                if (CurrentJobPostingId > 0)
                {
                    // SecureUrl url = UrlHelper.BuildSecureUrl(UrlConstants.ApplicationBaseUrl + UrlConstants.Requisition.JOB_POSTING_INTERNAL_PREVIEW_PAGE.Substring(2, UrlConstants.Requisition.JOB_POSTING_INTERNAL_PREVIEW_PAGE.Length - 2).Replace("//", "/"), string.Empty, UrlConstants.PARAM_JOB_ID, StringHelper.Convert(base.CurrentJobPostingId));
                    // btnPreview.OnClientClick = "window.open('" + url.ToString() + "'); return false;";
                    divReqTitle.InnerText = CurrentJobPosting.JobTitle + " - " + CurrentJobPosting.JobPostingCode;
                }
                if (CurrentJobPostingId > 0)
                {
                    IList<GenericLookup> RequisitionStatusList = Facade.GetAllGenericLookupByLookupTypeAndName(LookupType.RequisitionStatusValues, JobStatus.Draft.ToString());
                    if (RequisitionStatusList != null)
                        if (CurrentJobPosting.JobStatus != RequisitionStatusList[0].Id)
                        {
                            btnPublish.Text = "Save Changes";
                            btnSaveAsDraft.Visible = false;
                        }
                }
            }
            //**********Code added/Modify by pravin khot on 23/Feb/2016 [Using Hide workflow and publish section for specific roles]**************
            IList<CustomRole> roleList = Facade.GetCustomRoleRequisition(0); //Using Hide workflow
            string roleName = Facade.GetCustomRoleNameByMemberId(CurrentMember.Id);

            workflownavtab.Visible = false;
            previewnavtab.Visible = false;
            divworkflowarea.Visible = false;
            divpreviewarea.Visible = false;
            reqWorkflow.Visible = false;
            reqPublish.Visible = false;

            if (roleList != null)
            {
                foreach (CustomRole role in roleList)
                {
                    if (role.Name == roleName)
                    {
                        workflownavtab.Visible = true;
                        //previewnavtab.Visible = false;
                        divworkflowarea.Visible = true;
                        //divpreviewarea.Visible = false;
                        reqWorkflow.Visible = true;
                       // reqPublish.Visible = false ;
                        break;
                    }
                    else
                    {
                     
                    }
                }
            }

            IList<CustomRole> roleList1 = Facade.GetCustomRoleRequisition(1); //Using Hide workflow
            if (roleList1 != null)
            {
                foreach (CustomRole role1 in roleList1)
                {
                    if (role1.Name == roleName)
                    {
                        //workflownavtab.Visible = false;
                        previewnavtab.Visible = true;
                        //divworkflowarea.Visible = false;
                        divpreviewarea.Visible = true;
                        //reqWorkflow.Visible = false;
                        reqPublish.Visible = true;
                        break;
                    }
                    else
                    {
                      
                    }
                }
            }
            //***************************End****************************************
            //*******************Suraj Adsule******Start********For********20160901*********//
            IList<CustomRole> roleList3 = Facade.GetCustomRoleRequisition(3); //Using Hide workflow
            string roleName3 = Facade.GetCustomRoleNameByMemberId(CurrentMember.Id);
            if (roleList3 != null)
            {
                foreach (CustomRole role1 in roleList3)
                {
                    if (role1.Id == CurrentMember.Id || roleName.ToLower() == "admin")
                    {
                        previewnavtab.Visible = true;
                        divpreviewarea.Visible = true;
                        reqPublish.Visible = true;
                        break;
                    }
                    else { }
                }
            }
            //*******************Suraj Adsule******End********For********20160901*********//


           // IList<CustomRole> roleLists = Facade.GetCustomRoleRequisition(3); //Using Hide workflow
            if (roleList3 != null)
            {
                foreach (CustomRole role1 in roleList3)
                {
                    if (role1.Id  == CurrentMember.Id )
                    {
                        descriptionnavtab.Visible = false;
                        divDescriptionarea.Visible = false;
                        reqDescription.Visible = false;
                        break;
                    }
                    else
                    {

                    }
                }
            }
           
        }
        protected void btnSaveasdraft_Click(object sender, EventArgs e)
        {
            JobPosting jobPosting = null;
            if (CurrentJobPostingId > 0) jobPosting = CurrentJobPosting;
            else jobPosting = new JobPosting();
            jobPosting = reqEditor.BuildJobPosting(jobPosting);
            jobPosting = reqDescription.BuildJobDescription(jobPosting);
            jobPosting = reqWorkflow.BuildJobPosting(jobPosting);
            jobPosting = reqPublish.BuildJobPostingPreviewPublish(jobPosting);
            reqPublish.ShowSearchAgent();
            IList<GenericLookup> RequisitionStatusList = Facade.GetAllGenericLookupByLookupTypeAndName(LookupType.RequisitionStatusValues, JobStatus.Draft.ToString());
            if (RequisitionStatusList != null)
                jobPosting.JobStatus = RequisitionStatusList[0].Id;

            JobPosting newJobPosting = new JobPosting();
            if (jobPosting.Id == 0)
                newJobPosting = Facade.AddJobPosting(jobPosting);
            else newJobPosting = Facade.UpdateJobPosting(jobPosting);
            divReqTitle.InnerText = newJobPosting.JobTitle + " - " + newJobPosting.JobPostingCode;
            //reqEditor.SaveJobPostingDocument(newJobPosting.Id);
            reqWorkflow.SaveRecruiters(newJobPosting.Id, newJobPosting.ClientContactId);
            reqPublish.SaveAgent(newJobPosting);
            reqPublish.ShowSearchAgent(newJobPosting.Id);
            ViewState["JobPostingId"] = newJobPosting.Id;
            btnPreview.Visible = true;
            MiscUtil.EventLogForRequisition(EventLogForRequisition.RequisitionCreated, newJobPosting.Id, CurrentMember.Id, Facade);
            MiscUtil.ShowMessage(lblMessage, "Successfully saved Requisition as draft.", false);
            // SecureUrl url = UrlHelper.BuildSecureUrl(UrlConstants.ApplicationBaseUrl + UrlConstants.Requisition.JOB_POSTING_INTERNAL_PREVIEW_PAGE.Substring(2, UrlConstants.Requisition.JOB_POSTING_INTERNAL_PREVIEW_PAGE.Length - 2).Replace("//", "/"), string.Empty, UrlConstants.PARAM_JOB_ID, StringHelper.Convert(newJobPosting .Id ));
            // btnPreview.OnClientClick = "window.open('" + url.ToString() + "'); return false;";
        }
        protected void btnPreview_Click(object sender, EventArgs e)
        {
            JobPosting jobPosting = null;
            if (CurrentJobPostingId > 0) jobPosting = CurrentJobPosting;
            else jobPosting = new JobPosting();
            jobPosting = reqEditor.BuildJobPosting(jobPosting);
            jobPosting = reqDescription.BuildJobDescription(jobPosting);
            jobPosting = reqWorkflow.BuildJobPosting(jobPosting);
            jobPosting = reqPublish.BuildJobPostingPreviewPublish(jobPosting);
            //uclJobDetail.Document = reqEditor.getAtachedDocumentList(0);
            uclJobDetail.AssignedRecuiters = reqWorkflow.getHiringTeam();
            
            uclJobDetail.DraftJobPosting = jobPosting;
            ModalPopupExtender.Enabled = true;
            ModalPopupExtender.Show();
        }
    }
}
