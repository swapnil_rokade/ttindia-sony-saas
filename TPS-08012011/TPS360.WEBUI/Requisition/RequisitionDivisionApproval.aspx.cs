﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using TPS360.Web.UI;
using TPS360.BusinessFacade;
using System.Collections.Generic;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

public partial class Requisition_RequisitionDivisionApproval : RequisitionBasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (CurrentMember != null && CurrentMember.Id > 0)
        {
            string CurrentMemberRole = Facade.GetCustomRoleNameByMemberId(CurrentMember.Id);
            ObjectDataSource odsReqisitionDivisionApprovalList = (ObjectDataSource)this.uclRequisition.FindControl("odsReqisitionDivisionApprovalList");
            if (CurrentMemberRole == ContextConstants.ROLE_DEPARTMENT_CONTACT)
            {
                odsReqisitionDivisionApprovalList.SelectParameters["memberId"].DefaultValue = CurrentMember.Id.ToString();
            }
            else
            {

                odsReqisitionDivisionApprovalList.SelectParameters["memberId"].DefaultValue = CurrentMember.Id.ToString();
            }
        }
        else
        {
            Response.Redirect("..//Login.aspx?SeOut=SessionOut", true);
        }
    }
}
