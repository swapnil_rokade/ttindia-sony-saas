﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using TPS360.Web.UI;
using TPS360.BusinessFacade;
using System.Collections.Generic;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
public partial class Requisition_ShowOffer : RequisitionBasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (CurrentMember != null && CurrentMember.Id > 0)
        {
            int approvalType = 6;
            string CurrentMemberRole = Facade.GetCustomRoleNameByMemberId(CurrentMember.Id);

            ObjectDataSource odsRequisitionList = (ObjectDataSource)this.Offer1.FindControl("odsOfferList");
            odsRequisitionList.SelectParameters["memberId"].DefaultValue = CurrentMember.Id.ToString();
            odsRequisitionList.SelectParameters["approvalType"].DefaultValue = approvalType.ToString();
            if (CurrentMemberRole == ContextConstants.ROLE_RECRUITER)
            {
                odsRequisitionList.SelectParameters["memberId"].DefaultValue = CurrentMember.Id.ToString();
                odsRequisitionList.SelectParameters["status"].DefaultValue = "Recruiter";

            }
            else
            {
                odsRequisitionList.SelectParameters["memberId"].DefaultValue = CurrentMember.Id.ToString();
                odsRequisitionList.SelectParameters["status"].DefaultValue = "RecruiterManager";

            }
        }
        else
        {
            Response.Redirect("..//Login.aspx?SeOut=SessionOut", true);
        }

    }
}
