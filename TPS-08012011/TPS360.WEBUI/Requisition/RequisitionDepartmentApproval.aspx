﻿<%@ Page Language="C#" MasterPageFile="~/Dashboard.master" AutoEventWireup="true"
    Title="Department Head Approval" CodeFile="RequisitionDepartmentApproval.aspx.cs" Inherits="Requisition_DepartmentApproval" %>

<%@ Register Src="~/Controls/RequisitionDepartmentApproval.ascx" TagName="Requisition" TagPrefix="ucl" %>
<asp:Content ID="cntBasicInfoEditor" ContentPlaceHolderID="cphHomeMaster" runat="server">
<style>
    h2.CommonTitle {display: none;}
</style>
    <div id="BodyMiddle" style="width: 96.9%;">
        <div class="section">
            <div class="section-header">
                <h3>
                    Requisition Department Head Approval
                </h3>
            </div>
            <div class="section-body" style="color: #333333;">
                <asp:Panel ID="pnlmodal" runat="server">
                    <asp:UpdatePanel ID="pnlRequisition" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <ucl:Requisition ID="uclRequisition" runat="server" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </asp:Panel>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHomeMasterTitle" runat="server">
</asp:Content>
