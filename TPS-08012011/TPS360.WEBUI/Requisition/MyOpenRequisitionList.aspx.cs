﻿
/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: MyOpenRequisitionList.aspx.cs
    Description: This is the page which is used to display the MyOpenRequisation list 
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    --------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    0.1             Nov-27-2008          Gopala Swamy          Defect id: 9336;Added one line to get the Code of user using ternary operator and modified functional prototype
 *  0.2             May-15-2009          Sandeesh              Defect id:10440 :Changes made to get the Requisition status from database instead of reading from the  Enum
    0.3             Feb-25-2009          Rajendra A.S          Defect id:12135;Added code in ItemCommand event. 
    0.4             Apr-27-2010          Ganapati Bhat         Defect id:12725;Added Code in lsvJobPosting_ItemDataBound()
 *  0.5             May-12-2010          Ganapati Bhat         Defect id:12133;Added Code in lsvJobPosting_ItemDataBound()
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------       
--%>*/
using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Web.UI.Helper; 

namespace TPS360.Web.UI.Requisition
{
    public partial class MyOpenRequisitionList : RequisitionBasePage
    {
        #region Member Variables


        #endregion

        #region Methods

        private void BindList()
        {
            this.lsvJobPosting.DataBind();
        }

        private void LoadAssignedManagersControl()
        {
            int jobId = 0;
            for (int count = 0; count < lsvJobPosting.Items.Count; count++)
            {
                Int32.TryParse(lsvJobPosting.DataKeys[count].Value.ToString(), out jobId);
                if (jobId > 0)
                {
                    ListViewDataItem item = ((ListViewDataItem)lsvJobPosting.Items[count]);
                    Label lblAssignedManager = (Label)item.FindControl("lblAssignedManager");
                    System.EventHandler clickEvent = new EventHandler(this.lnkBtnUnAssignManager_Click);
                    //GetJobPostingAssignedManagers(jobId, lblAssignedManager, clickEvent, count);       //0.5
                }
            }
        }

        public void GetJobPostingAssignedManagers(int jobId, Control ctrlAssignedManagers, System.EventHandler clickEvent, int rowNo)
        {
            if (jobId > 0)
            {
                IList<JobPostingHiringTeam> jobPostingHiringTeamList = Facade.GetAllJobPostingHiringTeamByJobPostingId(jobId);
                if (jobPostingHiringTeamList != null)
                {
                    foreach (JobPostingHiringTeam jobPostingHiringTeam in jobPostingHiringTeamList)
                    {
                        if (jobPostingHiringTeam != null)
                        {
                            if (jobPostingHiringTeam.MemberId > 0)
                            {
                                Member member = Facade.GetMemberById(jobPostingHiringTeam.MemberId);
                                if (member != null)
                                {
                                    Label lblManagerName = new Label();
                                    lblManagerName.Text = member.FirstName + " " + member.LastName + " ";
                                    lblManagerName.ID = "lblAssignedManager," + jobId + "," + member.Id.ToString();
                                    ctrlAssignedManagers.Controls.Add(lblManagerName);

                                    LinkButton lnkButton = new LinkButton();
                                    if (member.Id == base.CurrentMember.Id)
                                    {
                                        lnkButton.Text = "Self Un-Assign</br>";
                                    }
                                    else
                                    {
                                        lnkButton.Text = "Un-Assign</br>";
                                    }
                                    lnkButton.ID = "lnkAssignedManager," + jobId + "," + rowNo + "," + member.Id.ToString();
                                    lnkButton.CommandArgument = jobPostingHiringTeam.Id.ToString();
                                    lnkButton.Click += clickEvent;
                                    ctrlAssignedManagers.Controls.Add(lnkButton);
                                }
                            }
                        }
                    }
                }
            }
        }

        #endregion

        #region Events

        #region Page Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string message = Helper.Url.SecureUrl[UrlConstants.PARAM_MSG];

                ControlHelper.SetHyperLink(lnkAddJobPosting, UrlConstants.Requisition.JOB_POSTING_EDITOR_PAGE, string.Empty, "[ Add Requisition ]");

                if (!StringHelper.IsBlank(message))
                {
                    MiscUtil.ShowMessage(lblMessage, message, false);
                }
            }
            else
            {
                LoadAssignedManagersControl();
            }
        }

        protected void lnkBtnUnAssignManager_Click(object sender, EventArgs e)
        {
            LinkButton lnkBtnUnassignManager = (LinkButton)sender;
            string[] values = lnkBtnUnassignManager.ID.Split(',');
            int jobId = 0;
            Int32.TryParse(values[1], out jobId);

            int rowNo = 0;
            Int32.TryParse(values[2], out rowNo);

            int jobPostingHiringTeamId = 0;
            if (!string.IsNullOrEmpty(lnkBtnUnassignManager.CommandArgument))
            {
                Int32.TryParse(lnkBtnUnassignManager.CommandArgument, out jobPostingHiringTeamId);
                if (jobPostingHiringTeamId > 0)
                {
                    Facade.DeleteJobPostingHiringTeamById(jobPostingHiringTeamId);

                    Label lblAssignedManager = (Label)lsvJobPosting.Items[rowNo].FindControl("lblAssignedManager");
                    lblAssignedManager.Text = string.Empty;

                    System.EventHandler clickEvent = new EventHandler(this.lnkBtnUnAssignManager_Click);
                    GetJobPostingAssignedManagers(jobId, lblAssignedManager, clickEvent, rowNo);

                    MiscUtil.ShowMessage(lblMessage, "Successfully Un-Assigned Manager...", false);
                }
            }
        }

        #endregion

        #region ListView Events

        protected void lsvJobPosting_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            bool _isAdmin = base.IsUserAdmin; 
            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                JobPosting jobPosting = ((ListViewDataItem)e.Item).DataItem as JobPosting;

                if (jobPosting != null)
                {
                    Label lblPostedDate = (Label)e.Item.FindControl("lblPostedDate");
                    Label lblClientName = (Label)e.Item.FindControl("lblClientName");
                    HyperLink lnkJobTitle = (HyperLink)e.Item.FindControl("lnkJobTitle");
                    Label lblCity = (Label)e.Item.FindControl("lblCity");
                    Label lblRevenue = (Label)e.Item.FindControl("lblRevenue");
                    Label lblJobStatus = (Label)e.Item.FindControl("lblJobStatus");
                    Label lblJobType = (Label)e.Item.FindControl("lblJobType");                    
                    Label lblAssignedManager = (Label)e.Item.FindControl("lblAssignedManager");

                    ImageButton btnEdit = (ImageButton)e.Item.FindControl("btnEdit");
                    ImageButton btnCreateNewFromExisting = (ImageButton)e.Item.FindControl("btnCreateNewFromExisting");
                    ImageButton btnDelete = (ImageButton)e.Item.FindControl("btnDelete");

                    lblPostedDate.Text = jobPosting.PostedDate.ToShortDateString();

                    lblClientName.Text = MiscUtil.GetClientNameByJobId(jobPosting.Id, Facade);

                    ControlHelper.SetHyperLink(lnkJobTitle, UrlConstants.Requisition.JOB_POSTING_INTERNAL_PREVIEW_PAGE, string.Empty, jobPosting.JobTitle, UrlConstants.PARAM_JOB_ID, StringHelper.Convert(jobPosting.Id));
                    lblCity.Text = jobPosting.City + ((!string.IsNullOrEmpty(jobPosting.City) && jobPosting.StateId > 0) ? ", " : string.Empty) +  MiscUtil.GetStateNameById(jobPosting.StateId,Facade);
                    lblRevenue.Text = jobPosting.ExpectedRevenue.ToString();
                    if (jobPosting.JobDurationLookupId !=string .Empty )
                    {
                        //lblJobType.Text = MiscUtil.GetLookupNameById(jobPosting.JobDurationLookupId, Facade);
                        lblJobType.Text = MiscUtil.SplitValues(jobPosting.JobDurationLookupId, ',', Facade);
                    }
                    //lblJobStatus.Text = Enum.GetName(typeof(JobStatus), jobPosting.JobStatus);
                    //0.4 Starts
                    GenericLookup _RequisitionStatusLookup = Facade.GetGenericLookupById(jobPosting.JobStatus);
                    if (_RequisitionStatusLookup != null)
                    {
                        lblJobStatus.Text = _RequisitionStatusLookup.Name;
                    }
                    //0.4 Ends 

                    int rowNo = ((ListViewDataItem)e.Item).DataItemIndex;
                    System.EventHandler clickEvent = new EventHandler(this.lnkBtnUnAssignManager_Click);
                    //GetJobPostingAssignedManagers(jobPosting.Id, lblAssignedManager, clickEvent, rowNo);
                    //0.5 Starts
                    Member MemberCreated = Facade.GetMemberById(jobPosting.CreatorId);             
                    if (MemberCreated != null)
                    {
                        lblAssignedManager.Text = MemberCreated.FirstName + " " + MemberCreated.LastName;  
                    }
                    //0.5 Ends

                    btnEdit.CommandArgument = btnDelete.CommandArgument = btnCreateNewFromExisting.CommandArgument = StringHelper.Convert(jobPosting.Id);
                    btnDelete.Visible = _isAdmin;
                    btnDelete.OnClientClick = "return ConfirmDelete('jobPosting')";
                }
            }
        }

        protected void lsvJobPosting_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            int id;

            int.TryParse(e.CommandArgument.ToString(), out id);

            if (id > 0)
            {
                if (string.Equals(e.CommandName, "EditItem"))
                {
                    Helper.Url.Redirect(UrlConstants.Requisition.JOB_POSTING_EDITOR_PAGE, string.Empty, UrlConstants.PARAM_JOB_ID, StringHelper.Convert(id));
                }
                else if (string.Equals(e.CommandName, "CreateNewFromExisting"))
                {
                    try
                    {
                        int intReqCount = Facade.GetRequisitionCountByMemberId(base.CurrentMember.Id);
                        string strMemberJobCount = "";

                        if (strMemberJobCount.Length < 4)
                        {
                            strMemberJobCount = (intReqCount + 1).ToString();
                            strMemberJobCount = "000" + strMemberJobCount;
                            strMemberJobCount = strMemberJobCount.Substring((strMemberJobCount.Length - 4), 4);
                        }
                        //jobPosting.JobPostingCode = (base.CurrentMember.LastName.IsNullOrEmpty() ? (Convert.ToString(base.CurrentMember.FirstName.Substring(0, 2))) : ((Convert.ToString(base.CurrentMember.FirstName.Substring(0, 1)) + Convert.ToString(base.CurrentMember.LastName.Substring(0, 1))))).ToUpper() + Convert.ToString(jobPosting.Id);  //0.2 added here
                        string jobPostingCode = (base.CurrentMember.LastName.IsNullOrEmpty() ? (Convert.ToString(base.CurrentMember.FirstName.Substring(0, 2))) : ((Convert.ToString(base.CurrentMember.FirstName.Substring(0, 1)) + Convert.ToString(base.CurrentMember.LastName.Substring(0, 1))))).ToUpper() + strMemberJobCount;

                        JobPosting jobPosting = Facade.CreateJobPostingFromExistingJob(id, jobPostingCode, base.CurrentMember.Id);//0.1

                        //0.3 starts
                        JobPostingHiringTeam JPHT = new JobPostingHiringTeam();
                        JPHT.MemberId = base.CurrentMember.Id;
                        JPHT.JobPostingId = jobPosting.Id;
                        JPHT.UpdateDate = System.DateTime.Now;
                        JPHT.UpdatorId = base.CurrentMember.Id;
                        JPHT.CreateDate = System.DateTime.Now;
                        JPHT.EmployeeType = "Internal";
                        JPHT.CreatorId = base.CurrentMember.Id;
                        Facade.AddJobPostingHiringTeam(JPHT);
                        //0.3 ends

                        Facade.UpdateMemberRequisitionCount(base.CurrentMember.Id, intReqCount + 1);
                        if (jobPosting != null)
                        {
                            Helper.Url.Redirect(UrlConstants.Requisition.JOB_POSTING_WORKFLOW_PAGE, string.Empty, UrlConstants.PARAM_JOB_ID, StringHelper.Convert(jobPosting.Id));
                            //BindList();
                            //MiscUtil.ShowMessage(lblMessage, "New job posting has been successfully created from this job.", false);
                        }
                    }
                    catch (ArgumentException ex)
                    {
                        MiscUtil.ShowMessage(lblMessage, ex.Message, true);
                    }
                }
                else if (string.Equals(e.CommandName, "DeleteItem"))
                {
                    try
                    {
                        if (Facade.DeleteJobPostingById(id, base.CurrentMember.Id))
                        {
                            BindList();
                            MiscUtil.ShowMessage(lblMessage, "Job posting has been successfully deleted.", false);
                        }
                    }
                    catch (ArgumentException ex)
                    {
                        MiscUtil.ShowMessage(lblMessage, ex.Message, true);
                    }
                }
            }
        }

        #endregion

        #region Pager Events

        protected void PagerCommand(object sender, DataPagerCommandEventArgs e)
        {
            DataPager pager = e.Item.Pager;
            int newSartRowIndex;

            switch (e.CommandName)
            {
                case "Next":
                    {
                        newSartRowIndex = pager.StartRowIndex + pager.MaximumRows > pager.TotalRowCount ? pager.StartRowIndex : pager.StartRowIndex + pager.MaximumRows;
                        break;
                    }
                case "Previous":
                    {
                        newSartRowIndex = pager.StartRowIndex - pager.MaximumRows < 0 ? pager.StartRowIndex : pager.StartRowIndex - pager.MaximumRows;
                        break;
                    }
                case "Last":
                    {
                        newSartRowIndex = (pager.TotalRowCount / pager.MaximumRows) * pager.MaximumRows;
                        break;
                    }
                case "First":
                    {
                        newSartRowIndex = 0;
                        break;
                    }
                default:
                    {
                        newSartRowIndex = 0;
                        break;
                    }
            }

            e.NewMaximumRows = e.Item.Pager.MaximumRows;
            e.NewStartRowIndex = newSartRowIndex;
        }

        protected void CurrentPageChanged(object sender, EventArgs e)
        {
            TextBox txtCurrentPage = sender as TextBox;

            DataPager pager = this.lsvJobPosting.FindControl("pager") as DataPager;
            int startRowIndex = (int.Parse(txtCurrentPage.Text) - 1) * pager.MaximumRows;
            pager.SetPageProperties(startRowIndex, pager.MaximumRows, true);
        }

        #endregion

        #region ObjectDataSource Event

        protected void odsJobPostingList_Load(object sender, EventArgs e)
        {
            odsJobPostingList.SelectParameters["memberId"].DefaultValue = base.CurrentMember.Id.ToString();
        }

        

      //0.2 Start

        protected void odsJobPostingList_Init(object sender, EventArgs e)
        {
            int JobStatusid = 0;
            IList<GenericLookup> RequisitionStatusList = Facade.GetAllGenericLookupByLookupTypeAndName(LookupType.RequisitionStatus, JobStatus.Open.ToString());
            if (RequisitionStatusList != null)
            {
                JobStatusid = RequisitionStatusList[0].Id;
            }
            odsJobPostingList.SelectParameters["JobStatus"].DefaultValue = JobStatusid.ToString();
        }

        //0.2 End
        #endregion

        #endregion
    }
}
