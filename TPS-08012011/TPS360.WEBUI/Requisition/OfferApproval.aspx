﻿<%@ Page Language="C#" MasterPageFile="~/Dashboard.master" AutoEventWireup="true"
    CodeFile="OfferApproval.aspx.cs" Inherits="Requisition_OfferApproval" Title="Offer Approval"%>

<%@ Register Src="~/Controls/OfferApproval.ascx" TagName="Offer" TagPrefix="ucl" %>
<asp:Content ID="cntBasicInfoEditor" ContentPlaceHolderID="cphHomeMaster" runat="server">
<style>
    h2.CommonTitle {display: none;}
</style>
    <div id="BodyMiddle" style="width: 96.9%;">
        <div class="section">
            <div class="section-header">
                <h3>
                    Offer Approval
                </h3>
            </div>
            <div class="section-body" style="color: #333333;">
                <asp:Panel ID="pnlmodal" runat="server">
                    <asp:UpdatePanel ID="pnlOfferApproval" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <ucl:Offer ID="Offer1" runat="server" />                           
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </asp:Panel>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHomeMasterTitle" runat="server">
</asp:Content>
