﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using TPS360.Web.UI;
using TPS360.BusinessFacade;
using System.Collections.Generic;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

public partial class Requisition_CandidateHiringStatusUpdate : RequisitionBasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (CurrentMember != null && CurrentMember.Id > 0)
        {
            string CurrentMemberRole = Facade.GetCustomRoleNameByMemberId(CurrentMember.Id);
        }
        else
        {
            Response.Redirect("..//Login.aspx?SeOut=SessionOut", true);
        }
    }

    [System.Web.Services.WebMethod]
    public static object[] ChangeRequisitionStatus(object data)
    {
        IFacade Facade = new Facade();
        Dictionary<string, object> param =
              (Dictionary<string, object>)data;
        try
        {
            JobPosting jobPosting = Facade.GetJobPostingById(Convert.ToInt32(param["RequisitionId"]));
            jobPosting.JobStatus = Convert.ToInt32(param["StatusId"]);
            jobPosting.UpdatorId = Convert.ToInt32(param["UpdatorId"]);

            ApplicationWorkflow applicationWorkflow = Facade.GetApplicationWorkflowByTitle(WorkFlowTitle.RequisitionManagementShouldSelectedApprovedUsersAbilityToChangeStats.ToString());
            if (applicationWorkflow != null && applicationWorkflow.RequireApproval)
            {
                IList<ApplicationWorkflowMap> applicationWorkflowMapList = Facade.GetAllApplicationWorkflowMapByApplicationWorkflowId(applicationWorkflow.Id);
                if (applicationWorkflowMapList != null && applicationWorkflowMapList.Count > 0)
                {
                    foreach (ApplicationWorkflowMap applicationWorkflowMap in applicationWorkflowMapList)
                    {
                        if (applicationWorkflowMap.MemberId == Convert.ToInt32(param["UpdatorId"]))
                        {
                            Facade.UpdateJobPosting(jobPosting);
                            MiscUtil.EventLogForRequisition(EventLogForRequisition.RequisitionStatusChange, jobPosting.Id, Convert.ToInt32(param["UpdatorId"]), Facade);
                            break;
                        }
                    }
                }
            }
            else
            {
                Facade.UpdateJobPosting(jobPosting);
                MiscUtil.EventLogForRequisition(EventLogForRequisition.RequisitionStatusChange, jobPosting.Id, Convert.ToInt32(param["UpdatorId"]), Facade);
                string[] result = { "Changed", "" };
                return result;
            }
        }
        catch (ArgumentException ax)
        {

        }
        string[] result1 = { "Changed", "" };
        return result1;
    }
}
