﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using TPS360.Web.UI;
using TPS360.BusinessFacade;
using System.Collections.Generic;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

public partial class Requisition_DivisionHeadHRApproval : RequisitionBasePage
{
    // Vishal Tripathy Dated for 24th August 2016.
    // When Division Head Approves.Rejects and go to recruiter/Recruiter Manager
    #region Page Load Events
    protected void Page_Load(object sender, EventArgs e)
    {
        int approvalType = 4;
        ObjectDataSource odsRequisitionList = (ObjectDataSource)this.DivisionHROffer.FindControl("odsOfferList");
        odsRequisitionList.SelectParameters["memberId"].DefaultValue = CurrentMember.Id.ToString();
        odsRequisitionList.SelectParameters["approvalType"].DefaultValue = approvalType.ToString();


    }
    #endregion

}
