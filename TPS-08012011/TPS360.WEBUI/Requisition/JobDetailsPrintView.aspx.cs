﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace TPS360.Web.UI
{
    public partial class JobDetailsPrintView :BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ImageButton imgClose = (ImageButton)uclPrintJob.FindControl("imgClose");
            imgClose.Visible = false ;
        }
    }

}