﻿<%@ Page Language="C#" MasterPageFile="~/Dashboard.master" AutoEventWireup="true"
    CodeFile="ShowOffer.aspx.cs" Inherits="Requisition_ShowOffer" Title="Show Offer To Recruiter" %>

<%@ Register Src="~/Controls/ShowOffer.ascx" TagName="Offer" TagPrefix="ucl" %>
<asp:Content ID="cntBasicInfoEditor" ContentPlaceHolderID="cphHomeMaster" runat="server">
<style>
    h2.CommonTitle {display: none;}
</style>
    <div id="BodyMiddle" style="width: 96.9%;">
        <div class="section">
            <div class="section-header">
                <h3>
                    Show Offer
                </h3>
            </div>
            <div class="section-body" style="color: #333333;">
                <asp:Panel ID="pnlmodal" runat="server">
                    <asp:UpdatePanel ID="pnlOfferApproval" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <ucl:Offer ID="Offer1" runat="server" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </asp:Panel>
            </div>
        </div>
    </div>
</asp:Content>
