﻿
 <%--
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: CandidateHiringStatusUpdate.aspx
    Description: This page is used to display the Candidate Hiring Status Update
    Created By: Kanchan Yeware
    Created On: 7-8-2016
    Modification Log:    
-------------------------------------------------------------------------------------------------------------------------------------------       

--%>
<%@ Page Language="C#" MasterPageFile="~/Default.master" AutoEventWireup="true" CodeFile="CandidateHiringStatusUpdate.aspx.cs" 
Inherits="Requisition_CandidateHiringStatusUpdate" Title="Candidate Hire Status" %>
<%@ Register Src ="~/Controls/CandidateHireStatusUpdate.ascx" TagName ="CandidateHire" TagPrefix ="ucl"%>

<asp:Content ID="cphHiringStatusUpdateTitle" ContentPlaceHolderID="head" Runat="Server">  
</asp:Content>
<asp:Content ID="cphHead" ContentPlaceHolderID="cphHomeMasterTitle" Runat="Server">
Candidate Status Update - Hiring Manager
</asp:Content>
<asp:Content ID="cphMainBody" ContentPlaceHolderID="cphHomeMaster" Runat="Server">
    <ucl:CandidateHire ID="uclCandidateHire" runat="server" />
</asp:Content>

