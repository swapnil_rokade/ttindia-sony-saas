﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: CompanyDocument.aspx.cs
    Description: This is the aspx.cs page used to upload the company documents.
    Created By: 
    Created On:
    Modification Log:
    --------------------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                 Author                 Modification
    ---------------------------------------------------------------------------------------------------------------------------------------------
    0.1              Jan-27-2009        Kalyani Pallagani       Defect ID: 8835; Display alert message for manadatory fields(doument title and name)
    0.2              Feb-17-2009        Kalyani Pallagani       Defect ID: 9933; Added code in SaveFiles() and Page_load()
    0.3              Mar-30-2009        N.Srilakshmi            Defect ID: 10065,10066; Added code in SaveFiles(),deletefile(),Uploadfiles()
    0.4              Apr-28-2009        A.S.Rajendra Prasad     Defect ID: 10372;Added code in UploadClicked(),Uploadfile(),SourceRowCommand().
    0.5              May-15-2009        Jagadish                Defect ID: 10475; Change made in method 'SaveFiles()'.
    0.6              June-22-2009       Gopala Swamy J          Defect ID: 10740; Put a line to ask for confrimation to delete the document.
---------------------------------------------------------------------------------------------------------------------------------------------------        
*/

using System;

using TPS360.Common.BusinessEntities;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Collections;
using System.Web.UI.WebControls;
using TPS360.Common.Helper;
using TPS360.Web.UI.Helper;

namespace TPS360.Web.UI.Admin
{
    public partial class CompanyDocumentPage : CompanyBasePage
    {
        #region Member Variables

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Page.Title = base.CurrentCompany.CompanyName + " - " + "Documents";
                hdnPageTitle.Value = Page.Title;
            }
            Page.Title = hdnPageTitle.Value;  
        }

        #endregion
    }
}