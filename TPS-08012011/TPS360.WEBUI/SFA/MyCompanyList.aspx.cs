﻿using System;
using TPS360.Common.Shared;

namespace TPS360.Web.UI.Admin
{
    public partial class MyCompanyList : CompanyBasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string currentSiteMapId = Helper.Url.SecureUrl[UrlConstants.PARAM_SITEMAP_PARENT_ID].ToString();
            if (currentSiteMapId == "11")
                cltSearchClient.Status = CompanyStatus.Client;
            else if (currentSiteMapId == "638") cltSearchClient.Status = CompanyStatus.Vendor;
            else cltSearchClient.Status = CompanyStatus.Department;
            if (!IsPostBack)
            {
                cltSearchClient.SearchType = SearchType.MySearch;
                
            }
            string Title = "";
            switch (CurrentCompanyStatus)
            {
                case CompanyStatus.Client:
                    Title = "My Account List";
                    break;
                case CompanyStatus.Department:
                    Title = "My BU List";
                    break;
                case CompanyStatus.Vendor:
                    Title = "My Vendor List";
                    break;
            }

            lblCompanyHeader.Text = this.Page.Title = Title;
        }
    }
}