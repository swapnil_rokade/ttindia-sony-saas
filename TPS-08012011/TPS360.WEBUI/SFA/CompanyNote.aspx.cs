﻿using System;

namespace TPS360.Web.UI.SFA
{

    public partial class SFA_CompanyNote : CompanyBasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Page.Title =MiscUtil .RemoveScript ( base.CurrentCompany.CompanyName,string .Empty ) + " - " + "Notes";
                hdnPageTitle.Value = Page.Title;
            }
            Page.Title = hdnPageTitle.Value; 
        }
    }
}
