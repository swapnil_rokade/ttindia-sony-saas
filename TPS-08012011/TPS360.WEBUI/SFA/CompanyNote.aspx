﻿<%@ Page Language="C#" MasterPageFile="~/Masters/CompanyProfile.master" AutoEventWireup="true"
    CodeFile="CompanyNote.aspx.cs" Inherits="TPS360.Web.UI.SFA.SFA_CompanyNote" Title="Company Note" %>

<%@ Register Src="~/Controls/CompanyNote.ascx" TagName="CompanyNote" TagPrefix="uc1" %>
<asp:Content ID="cntCreateNotes" ContentPlaceHolderID="cphCompanyMaster" runat="Server">

    <script src="../Scripts/fixWebkit.js" type="text/javascript"></script>

    <asp:HiddenField ID="hdnPageTitle" runat="server" />
    <uc1:CompanyNote ID="CompanyNote1" runat="server" />
</asp:Content>
