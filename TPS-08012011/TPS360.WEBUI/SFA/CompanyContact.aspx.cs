﻿using System;

namespace TPS360.Web.UI.Admin
{
    public partial class CompanyContactPage : CompanyBasePage
    {
        #region Methods
        
        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
          
                string currentSiteMapId = Helper.Url.SecureUrl[UrlConstants.PARAM_SITEMAP_PARENT_ID].ToString();
                if (currentSiteMapId == "11")
                {
                    cltCompanyContact._role = ContextConstants.ROLE_COMPANY_CONTACT ;
                }
                else if (currentSiteMapId == "638")
                {
                    cltCompanyContact._role = ContextConstants.ROLE_VENDOR ;
                }
                else
                {
                    cltCompanyContact._role = ContextConstants.ROLE_DEPARTMENT_CONTACT;
                }

                if (!IsPostBack)
                {

                    Page.Title = base.CurrentCompany.CompanyName + " - " + "Contacts";
                    hdnPageTitle.Value = Page.Title;
                }
            Page.Title = hdnPageTitle.Value;  
        }            

        #endregion
    }
}