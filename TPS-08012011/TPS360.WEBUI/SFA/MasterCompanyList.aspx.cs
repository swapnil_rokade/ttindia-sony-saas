﻿using System;
using TPS360.Common.Shared;

namespace TPS360.Web.UI.Admin
{
    public partial class MasterCompanyList : CompanyBasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string currentSiteMapId = Helper.Url.SecureUrl[UrlConstants.PARAM_SITEMAP_PARENT_ID].ToString();
            if (currentSiteMapId == "11")
                cltSearchCompany.Status = CompanyStatus.Client;
            else if (currentSiteMapId == "638") cltSearchCompany.Status = CompanyStatus.Vendor;
            else cltSearchCompany.Status = CompanyStatus.Department;


            if (!IsPostBack)
            {
                cltSearchCompany.SearchType = SearchType.MasterSearch;
           
            }
            switch (CurrentCompanyStatus)
            {
                case CompanyStatus.Client:
                    Title = "Master Account List";
                    break;
                case CompanyStatus.Department:
                    Title = "Master BU List";
                    break;
                case CompanyStatus.Vendor:
                    Title = "Master Vendor List";
                    break;
            }

            lblCompanyHeader.Text = this.Page.Title = Title;
        }

       
    }
}