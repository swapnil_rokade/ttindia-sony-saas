﻿<%@ Page Language="C#" MasterPageFile="~/Masters/CompanyProfile.master" AutoEventWireup="true"
    CodeFile="CompanyAssignManager.aspx.cs" Inherits="TPS360.Web.UI.SFA_CompanyAssignManager"
    Title="Assign Manager" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCompanyMaster" runat="Server">
    <asp:HiddenField ID="hdnPageTitle" runat="server" />

    <script src="../Scripts/fixWebkit.js" type="text/javascript"></script>
    

    <asp:UpdatePanel ID="updPanelCompanyTeam" runat="server">
        <ContentTemplate>
            <asp:HiddenField ID="hfPrimaryManagerId" runat="server" Value="0" />
            <div>
                <asp:Label ID="lblMessage" runat="server" EnableViewState="false"></asp:Label>
            </div>
            <div class="TabPanelHeader">
                    Assign Manager
                </div>
            <div class="TableRow"  id="dvAddManager"  runat="server" >
                    <div class="TableFormContent" style =" margin-left : 35%">
                        <asp:DropDownList ID="lstEmployee" runat="server" Width="200px" ValidationGroup="CompanyTeam" CssClass ="CommonDropDownList" >
                        </asp:DropDownList>
                        <asp:Button ID="btnAdd" CssClass="CommonButton" runat="server" Text="Add" OnClick="btnAdd_Click"
                            ValidationGroup="CompanyTeam" />
                    </div>
            </div>
            <div class="TableFormValidatorContent">
                <asp:RequiredFieldValidator ID="rfvEmployee" runat="server" ControlToValidate="lstEmployee"
                    ErrorMessage="Please Select an Employee name." EnableViewState="False" Display="Dynamic"
                    ValidationGroup="CompanyTeam"></asp:RequiredFieldValidator>
            </div>
            <br />
            <br />
            <div  class="TabPanelHeader">
                    List of Assigned Managers
            </div>
            <div class="GridContainer">
                <asp:ObjectDataSource ID="odsCompanyAssignedManager" runat="server" SelectMethod="GetAllCompanyAssignedManagerByCompanyId"
                    TypeName="TPS360.Web.UI.CompanyDataSource" SortParameterName="sortExpression">
                    <SelectParameters>
                        <asp:Parameter Name="companyId" DefaultValue="0" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <div>
                    <asp:ListView ID="lsvCompanyAssignedManager" runat="server" DataKeyNames="Id" OnItemDataBound="lsvCompanyAssignedManager_ItemDataBound"
                        OnItemCommand="lsvCompanyAssignedManager_ItemCommand" DataSourceID="odsCompanyAssignedManager"
                        OnPreRender="lsvCompanyAssignedManager_PreRender">
                        <LayoutTemplate>
                            <table id="tlbTemplate" runat="server" class="Grid" cellspacing="0" border="0">
                                <tr>
                                    <th style="width: 35%; white-space: nowrap;">
                                        Manager
                                    </th>
                                    <th style="width: 15%; text-align: center; white-space: nowrap;">
                                        Date Assigned
                                    </th>
                                    <th style="width: 20%; text-align: center; white-space: nowrap;">
                                        Primary Manager
                                    </th>
                                    <th id="thUnAssign" style="text-align: center; white-space: nowrap; width: 10%;">
                                        Un-Assign Manager
                                    </th>
                                </tr>
                                <tr id="itemPlaceholder" runat="server">
                                </tr>
                            </table>
                        </LayoutTemplate>
                        <EmptyDataTemplate>
                            <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                                <tr>
                                    <td style="color: Red">
                                        No manager assigned.
                                    </td>
                                </tr>
                            </table>
                        </EmptyDataTemplate>
                        <ItemTemplate>
                            <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                                <td style="text-align: left;">
                                    <asp:Label ID="lblManagerName" runat="server" />
                                </td>
                                <td style="text-align: center;">
                                    <asp:Label ID="lblAssignedDate" runat="server" />
                                </td>
                                <td style="white-space: nowrap; text-align: left;">
                                    <asp:Label ID="lblPrimaryManager" runat="server" />
                                </td>
                                <td id="tdUnAssign" style="text-align: center;">
                                    <asp:ImageButton ID="btnDelete" SkinID="sknDeleteButton" runat="server" CommandName="DeleteItem">
                                    </asp:ImageButton>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:ListView>
                </div>
            </div>
            <br />
            <div class="TableRow" style="text-align: center">
                <asp:Button ID="btnUpdate" CssClass="CommonButton" runat="server" Text="Update" OnClick="btnUpdate_Click" /></div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
