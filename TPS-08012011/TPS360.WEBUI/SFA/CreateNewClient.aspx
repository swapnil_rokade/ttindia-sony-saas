﻿<%@ Page Language="C#" MasterPageFile="~/Default.master" AutoEventWireup="true" CodeFile="CreateNewClient.aspx.cs"
    Inherits="SFA_CreateNewClient" Title="Create New Client"  EnableEventValidation ="false" %>
<%@ Register Src ="~/Controls/Company.ascx" TagName ="Company" TagPrefix ="ucl" %>
<asp:Content ID="cntAdditionalInformationHeader" ContentPlaceHolderID="cphHomeMasterTitle" runat="Server">
    <asp:Label ID="lblAccountHeader" runat ="server" Text ="Create New Account"></asp:Label>
</asp:Content>
<asp:Content ID="cntAdditionalInformation" ContentPlaceHolderID="cphHomeMaster" runat="Server">
  <asp:UpdatePanel ID="upCompany" runat ="server" >
  <ContentTemplate >
    <ucl:Company ID="uclCompany" runat ="server" />
  </ContentTemplate>
  </asp:UpdatePanel>
  
   <%-- 
    <div style="width: 100%; text-align: left;">
        <asp:Panel ID="pnlAddClient" runat="server" DefaultButton="btnSave">
            <div style="height: 50px">
                <asp:Label ID="lblMessage" runat="server" Text="" EnableViewState="false"></asp:Label>
            </div>
            <div class="TableRow">
                <div class="TableFormLeble">
                    <asp:Label ID="lblCompanyName" runat="server" Text="Account Name"></asp:Label>:
                </div>
                <div class="TableFormContent">
                    <asp:TextBox ID="txtCompanyName" runat="server" CssClass="CommonTextBox"></asp:TextBox>
                </div>
            </div>
            <div class="TableRow">
                <div class="TableFormValidatorContent" style="margin-left: 42%">
                    <asp:RequiredFieldValidator ID="rfvCompanyName" runat="server" ControlToValidate="txtCompanyName"
                        ErrorMessage="Please enter the Account name." EnableViewState="False" ValidationGroup="company"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div style="height: 50px">
            </div>
            <div class="TableRow" style="text-align: center">
                <asp:Button ID="btnSave" CssClass="btn btn-primary" runat="server" Text="Save" ValidationGroup="company"
                    OnClick="btnSave_Click" />
            </div>
        </asp:Panel>
    </div>
    --%>
</asp:Content>
