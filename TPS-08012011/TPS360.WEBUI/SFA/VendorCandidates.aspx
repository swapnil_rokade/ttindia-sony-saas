﻿<%@ Page Language="C#" MasterPageFile="~/Masters/CompanyProfile.master" AutoEventWireup="true"
    CodeFile="CompanyDocument.aspx.cs" Inherits="TPS360.Web.UI.Admin.CompanyDocumentPage"
    Title="Company Documents" %>

<%@ Register Src="~/Controls/VendorCandidates.ascx" TagName="VendorCandidates" TagPrefix="uc1" %>
<asp:Content ID="cntCompanyEditor" ContentPlaceHolderID="cphCompanyMaster" runat="Server">
    <asp:HiddenField ID="hdnPageTitle" runat="server" />
    <uc1:VendorCandidates ID="uclVendorCandidates" runat ="server" />
</asp:Content>
