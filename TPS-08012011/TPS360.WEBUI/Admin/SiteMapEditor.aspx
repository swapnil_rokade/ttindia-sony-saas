﻿<%@ Page Language="C#" MasterPageFile="~/Default.master" AutoEventWireup="true" CodeFile="SiteMapEditor.aspx.cs"
    Inherits="TPS360.Web.UI.Admin.SiteMapEditor" Title="SiteMap Editor" %>
<%@ Register assembly="Infragistics35.Web.v12.2, Version=12.2.20122.1007, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" namespace="Infragistics.Web.UI.LayoutControls" tagprefix="ig" %>
    
<%@ Register Src="../Controls/SiteMap.ascx" TagName="SiteMap" TagPrefix="uc1" %>
<%@ Register Src="../Controls/SiteMapList.ascx" TagName="SiteMapList" TagPrefix="uc2" %>
<asp:Content ID="cntSitMapEditorHeader" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="cntSitMapEditorTitle" ContentPlaceHolderID="cphHomeMasterTitle"
    runat="Server">
    Menu Builder
</asp:Content>
<asp:Content ID="cntSitMapEditor" ContentPlaceHolderID="cphHomeMaster" runat="Server">
    <%--<div class="GridContainer" style="width: 100%;">
        <igtab:UltraWebTab ID="UltraWebTab1" runat="server" EnableAppStyling="True" Width="100%">
            <DefaultTabStyle Height="16px" Font-Size="8pt">
                <Padding Top="2px"></Padding>
            </DefaultTabStyle>
            <RoundedImage LeftSideWidth="7" RightSideWidth="6" ShiftOfImages="2" SelectedImage="~/Images/ig_tab_winXPs1.gif"
                NormalImage="~/Images/ig_tab_winXPs3.gif" HoverImage="~/Images/ig_tab_winXPs2.gif"
                FillStyle="LeftMergedWithCenter"></RoundedImage>
            <SelectedTabStyle>
                <Padding Bottom="2px"></Padding>
            </SelectedTabStyle>
            <Tabs>
                <igtab:Tab Text="Site Map Editor" Key="editorTab">
                    <ContentTemplate>
                        <div class="TabContentHolder">
                            <uc1:SiteMap ID="ctlSiteMap" runat="server" />
                        </div>
                    </ContentTemplate>
                </igtab:Tab>
                
                <igtab:Tab Text="Site Map List" Key="listTab">
                    <ContentTemplate>
                        <div class="TabContentHolder">
                            <uc2:SiteMapList ID="ctlSiteMapList" runat="server" />
                        </div>
                    </ContentTemplate>
                </igtab:Tab>                          
            </Tabs>
        </igtab:UltraWebTab>
    </div>--%>
   <div>
    <ig:WebTab ID="UltraWebTab1" runat="server" Width="100%">
    <Tabs>
    <ig:ContentTabItem runat="server" Text="Site Map Editor" Key="editorTab">
    <Template>
    <div class="TabContentHolder">
    <uc1:SiteMap ID="ctlSiteMap" runat="server" />
    </div>
    </Template>
    </ig:ContentTabItem>
    <ig:ContentTabItem runat="server" Text="Site Map List" runat="server">
    <Template>
    <divclass="TabContentHolder">
    <uc2:SiteMapList ID="ctlSiteMapList" runat="server" />
    </div>
    </Template>
    </ig:ContentTabItem>
    </Tabs>
    </ig:WebTab>
    </div>
</asp:Content>