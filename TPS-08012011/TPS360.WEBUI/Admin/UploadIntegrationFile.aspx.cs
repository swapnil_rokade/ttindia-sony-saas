﻿using System;
using System.Web.UI.WebControls;
using System.Collections;
using System.Collections.Generic;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Web.UI.Helper;
using System.Web.Configuration;
using System.Data.OleDb;
using System.IO;
using System.Data;
using System.Web.Security;
using System.Linq;
namespace TPS360.Web.UI.Admin
{
    public partial class UploadIntegrationFile : AdminBasePage
    {
        String strTextFilePath = String.Empty;
        String strxlsFilePath = String.Empty;
        String strFilePath = String.Empty;
        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
           //string url = UrlConstants.Teamplate.RESOURCE_DIR + UrlConstants.Teamplate.CANDIDATE_IMPORT_TEAMPLATE_FILENAME;

            //lnkDownload.NavigateUrl = url;
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            bool file = true;          
            try
            {
                if (fuEmployeeData.HasFile || fuOrgonizationStructure.HasFile)
                {
                    strTextFilePath = System.Web.Hosting.HostingEnvironment.MapPath("~/Resources/UploadIntegrationFile/EmployeeData.txt");
                    strxlsFilePath = System.Web.Hosting.HostingEnvironment.MapPath("~/Resources/UploadIntegrationFile/Org.xls");

                    string strFilePath = System.Web.Hosting.HostingEnvironment.MapPath("~/Resources/UploadIntegrationFile/");

                    if (!Directory.Exists(strFilePath))
                    {
                        Directory.CreateDirectory(strFilePath);
                    }

                    if (fuEmployeeData.HasFile)
                    {                      
                        fuEmployeeData.SaveAs(strTextFilePath);
                    }

                    if (fuOrgonizationStructure.HasFile)
                    {                      
                        fuOrgonizationStructure.SaveAs(strxlsFilePath);
                    }

                    MiscUtil.ShowMessage(lblMessage, "File Uploaded Successfully.", false);                  
                }
                else
                {
                    MiscUtil.ShowMessage(lblMessage, "Please select atleast one file.", true);
                }
            }
            catch (Exception ex)
            {
                if (file == false)
                    MiscUtil.ShowMessage(lblMessage, "Please select appropriate file", true);
            }
            finally
            {

            }
        }    
        #endregion
    }

}