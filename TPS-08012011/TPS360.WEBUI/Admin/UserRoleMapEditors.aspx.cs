﻿/* 
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: UserRoleMapEditors.ascx.cs
    Description: assign the system role to user role.
    Created By: pravin khot
    Created On: 25/Feb/2016
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------- 
 */
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Web.UI.Helper;
using System.Web.UI;
using System.Collections;
using System.Text.RegularExpressions;
using System.Text;
namespace TPS360.Web.UI
{

    public partial class UserRoleMapEditors : BasePage
    {
        private bool Isdelete = false;
        private static int _buMapid = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                MiscUtil.PopulateCustomRole(ddlUser, Facade);
                ddlSystemRole.SelectedIndex = -1;
                ddlUser.SelectedIndex = -1;
            }
            ddlUser.Enabled = true;
        }
        #region Method

        public IList<int> getAssignedBUfromList()
        {
            IList<int> BUIds = new List<int>();

            //foreach (ListViewDataItem item in lsvBUList.Items)
            //{
            //    HiddenField hdnBUID = (HiddenField)item.FindControl("hdnBUID");
            //    if (hdnBUID.Value != "")
            //    {
            //        if (!BUIds.Contains(Convert.ToInt32(hdnBUID.Value)))
            //            BUIds.Add(Convert.ToInt32(hdnBUID.Value));
            //    }
            //}
            return BUIds;
        }

        public UserRoleMapEditor BuildBUMapping()
        {
            UserRoleMapEditor UserRoleMap = new UserRoleMapEditor();
            if (Convert.ToInt32(ddlSystemRole.SelectedValue) <= 0)
            {
                MiscUtil.ShowMessage(lblMessage, "Please select System Role", true);
            }
            else
            {

                UserRoleMap.RoleId = Convert.ToInt32(ddlUser.SelectedValue);
                UserRoleMap.SystemRole = ddlSystemRole.SelectedItem.Text.Trim();
            }
            return UserRoleMap;
        }

        //public void AddBUtoIndustryGroup(ListBox list)
        //{
        //    bool iserror = false;
        //    int countSelected = 0;
        //    int countAdded = 0;
        //    int BUId = 0;

        //    IList<int> AddedBU = getAssignedBUfromList();
        //    IList<Company> company = new List<Company>();

        //    string message = string.Empty;

        //    {
        //        for (int i = 0; i <= list.Items.Count - 1; i++)
        //        {
        //            Int32.TryParse(list.Items[i].Value, out BUId);
        //            if (BUId > 0)
        //            {
        //                if (list.Items[i].Selected || AddedBU.Contains(BUId))
        //                {
        //                    if (list.Items[i].Selected) countSelected++;
        //                    Company com = new Company();

        //                    com.Id = BUId;
        //                    com.CompanyName = list.Items[i].Text;
        //                    company.Add(com);
        //                    if (!AddedBU.Contains(BUId)) countAdded++;
        //                }
        //            }
        //        }
        //        lsvBUList.DataSource = company;
        //        try
        //        {
        //            lsvBUList.DataBind();
        //        }
        //        catch (Exception ex) { }


        //        if (countSelected > 0)
        //        {
        //            if (countAdded > 0)
        //            {
        //                int countNotAdded = countSelected - countAdded;
        //                if (countNotAdded == 0)
        //                {
        //                    message = "Successfully added all the selected BUs.";
        //                }
        //                else
        //                {
        //                    message = "Successfully added " + countAdded + " of " + countSelected + " selected BUs. The remaining BUs are already in the list.</b></font>";
        //                }
        //            }
        //            else
        //            {
        //                message = "The selected BU(s) are already in the list";
        //                iserror = true;
        //            }
        //        }
        //        else
        //        {
        //            message = "Please select at least one BU to add.";
        //            iserror = true;
        //        }

        //    }

        //    MiscUtil.ShowMessage(lblMessage, message, iserror);
        //}

        private void PrepareEditView()
        {
            UserRoleMapEditor UserRoleMap = null;
            try
            {
                UserRoleMap = Facade.UserRoleMapEditor_GetAllByBUMappingID(Convert.ToInt32(hdnSelectedIDtoEdit.Value));
            }
            catch { }

            if (UserRoleMap != null)
            {
                //ControlHelper.SelectListByValue(ddlUser, buMapping.UserName.ToString());
                ControlHelper.SelectListByValue(ddlUser, UserRoleMap.Id.ToString());
                ddlSystemRole.SelectedItem.Text  = Convert.ToString(UserRoleMap.SystemRole);
                ddlUser.Enabled = false;
            }
        }

        private void PlaceUpDownArrow()
        {
            try
            {
                LinkButton lnk = (LinkButton)lsvUserRoleMapEditor.FindControl(hdnSortColumn.Value);
                System.Web.UI.HtmlControls.HtmlTableCell im = (System.Web.UI.HtmlControls.HtmlTableCell)lnk.Parent;
                im.EnableViewState = false;
                im.Attributes.Add("class", (hdnSortOrder.Value == "ASC" ? "Ascending" : "Descending"));
            }
            catch
            {
            }

        }
        private void clear()
        {
            //ddlSystemRole.SelectedItem.Text = "Please Select";
            ddlUser.SelectedIndex = 0;
            ddlSystemRole.SelectedIndex = -1;
            hdnSelectedIDtoEdit.Value = "";
        }
        #endregion
      
        protected void btnSave_Click(object sender, EventArgs e)
        {

            UserRoleMapEditor UserRoleMap = new UserRoleMapEditor();
            if (ddlUser.SelectedIndex == 0)
            {
                MiscUtil.ShowMessage(lblMessage, "Please select User Role", true);
                ddlUser.Focus();
            }
            else if (Convert.ToInt32(ddlSystemRole.SelectedValue) <= 0)
            {
                MiscUtil.ShowMessage(lblMessage, "Please select System Role", true);
                ddlSystemRole.Focus();
            }
           
            else
            {
                UserRoleMap.RoleId = Convert.ToInt32(ddlUser.SelectedValue);
                UserRoleMap.SystemRole = ddlSystemRole.SelectedItem.Text.Trim();
                if (UserRoleMap.RoleId != 0 && UserRoleMap.SystemRole != "")
                {
                    if (hdnSelectedIDtoEdit.Value != "")
                    {
                        UserRoleMap.Id = Convert.ToInt32(hdnSelectedIDtoEdit.Value);
                        UserRoleMap.UpdatorId = base.CurrentMember.Id;
                        Facade.UpdateUserRoleMapping(UserRoleMap);
                        hdnSelectedIDtoEdit.Value = "";
                        MiscUtil.ShowMessage(lblMessage, "Successfully updated User Role mapping.", false);
                        ddlUser.Enabled = true ;
                    }
                    else
                    {
                        int ExistsRoleId = Facade.UserRoleMap_Id(UserRoleMap.RoleId);
                        if (ExistsRoleId > 0)
                        {
                            MiscUtil.ShowMessage(lblMessage, "This User Role allready Assign System Role.", true);
                        }
                        else
                        {
                            UserRoleMap.CreatorId = base.CurrentMember.Id;
                            Facade.AddUserRoleMapping(UserRoleMap);
                            MiscUtil.ShowMessage(lblMessage, "Successfully added User Role mapping.", false);
                        }
                    }

                    lsvUserRoleMapEditor.DataBind();
                    clear();
                }
                else
                {
                    MiscUtil.ShowMessage(lblMessage, "Please select both Role and System Role.", true);
                    ddlSystemRole.SelectedIndex = -1;
                    ddlUser.SelectedIndex = -1;
                }
                
            }
       
        }
        protected void lsvBUList_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                Company company = ((ListViewDataItem)e.Item).DataItem as Company;
                if (company != null)
                {
                    HiddenField hdnBUID = (HiddenField)e.Item.FindControl("hdnBUID");

                    Label lblBUName = (Label)e.Item.FindControl("lblBUName");

                    ImageButton btnDelete = (ImageButton)e.Item.FindControl("btnDelete");

                    if (company.Id > 0)
                    {
                        hdnBUID.Value = company.Id.ToString();

                        lblBUName.Text = company.CompanyName;

                        //Isdelete = true;

                    }
                }
            }
        }

        protected void lsvUserRoleMapEditor_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                UserRoleMapEditor UserRoleMap = ((ListViewDataItem)e.Item).DataItem as UserRoleMapEditor;

                Label lblRole = (Label)e.Item.FindControl("lblRole");
                Label lblSystemRole = (Label)e.Item.FindControl("lblSystemRole");

                lblRole.Text = UserRoleMap.Role;

                lblSystemRole.Text = UserRoleMap.SystemRole;

                //if (lblBU.Text.Trim().EndsWith(","))
                //{
                //    lblBU.Text = lblBU.Text.Trim().Substring(0, (lblBU.Text.Trim().Length - 1));
                //}
                ImageButton btnEdit = (ImageButton)e.Item.FindControl("btnEdit");
                ImageButton btnDelete = (ImageButton)e.Item.FindControl("btnDeleteList");
                btnDelete.OnClientClick = "return ConfirmDelete('User Role Mapping');";

                btnEdit.CommandArgument = btnDelete.CommandArgument = StringHelper.Convert(UserRoleMap.Id);

                if (Page.Request.Url.AbsoluteUri.ToString().Contains("Overview"))
                {
                    System.Web.UI.HtmlControls.HtmlTableCell table = (System.Web.UI.HtmlControls.HtmlTableCell)lsvUserRoleMapEditor.FindControl("thAction");
                    System.Web.UI.HtmlControls.HtmlTableCell tabledata = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdAction");
                    table.Visible = false;
                    tabledata.Visible = false;

                }
            }
        }

        protected void lsvUserRoleMapEditor_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Sort")
                {
                    LinkButton lnkbutton = (LinkButton)e.CommandSource;
                    if (hdnSortColumn.Value == lnkbutton.ID)
                    {
                        if (hdnSortOrder.Value == "ASC") hdnSortOrder.Value = "DESC";
                        else hdnSortOrder.Value = "ASC";
                    }
                    else
                    {
                        hdnSortColumn.Value = lnkbutton.ID;
                        hdnSortOrder.Value = "ASC";
                    }
                    if (hdnSortColumnName.Value == string.Empty || hdnSortColumnName.Value != e.CommandArgument.ToString())
                    {
                        hdnSortColumnOrder.Value = "asc";

                    }
                    else
                    {
                        hdnSortColumnOrder.Value = hdnSortColumnOrder.Value.ToLower() == "asc" ? "desc" : "asc";

                    }
                    hdnSortColumnName.Value = e.CommandArgument.ToString();

                }
            }
            catch
            {
            }
            int id;

            int.TryParse(e.CommandArgument.ToString(), out id);

            if (id > 0)
            {
                if (string.Equals(e.CommandName, "EditItem"))
                {
                    _buMapid = id;
                    hdnSelectedIDtoEdit.Value = id.ToString();
                    PrepareEditView();
                    ddlUser.Focus();
                    //Page.ClientScript.RegisterStartupScript(typeof(Page), "MoveTop", "<script>self.moveTo(0,-300); </script>");
                }
                else if (string.Equals(e.CommandName, "DeleteItem"))
                {
                    try
                    {
                        if (Facade.DeleteUserRoleMappingByid(id))
                        {
                            lsvUserRoleMapEditor.DataBind();

                            MiscUtil.ShowMessage(lblMessage, "Successfully deleted BU mapping.", false);
                            clear();
                        }
                    }
                    catch (ArgumentException ex)
                    {
                        MiscUtil.ShowMessage(lblMessage, ex.Message, true);
                    }
                }
            }

        }
        protected void lsvUserRoleMapEditor_PreRender(object sender, EventArgs e)
        {
            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvUserRoleMapEditor.FindControl("pagerControl");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)PagerControl.FindControl("Pager");
                if (pager != null)
                {
                    DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                    if (ddlrowPerPage != null)
                        ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
                }
                HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
                if (hdnRowPerPageName != null) hdnRowPerPageName.Value = "TeamsRowPerPage";
            }

            PlaceUpDownArrow();
            if (lsvUserRoleMapEditor.Items.Count == 0)
            {
                lsvUserRoleMapEditor.DataSource = null;
                lsvUserRoleMapEditor.DataBind();
            }
        }
    }
}