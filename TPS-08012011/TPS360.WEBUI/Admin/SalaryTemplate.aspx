﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Dashboard.master"
    CodeFile="SalaryTemplate.aspx.cs" Inherits="SalaryTemplate_SalaryTemplate" Title="Salary Template" %>

<%@ Register Src="~/Controls/SalaryTemplate.ascx" TagName="SalaryTemplate" TagPrefix="ucl" %>
<asp:Content ID="cntBasicInfoEditor" ContentPlaceHolderID="cphHomeMaster" runat="server">
    <ucl:SalaryTemplate ID="salaryTemplate" runat="server" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphHomeMasterTitle" runat="server">
</asp:Content>

