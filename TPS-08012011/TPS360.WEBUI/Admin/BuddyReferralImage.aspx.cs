﻿using System;
using System.Web.UI.WebControls;
using System.Collections;
using System.Collections.Generic;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Web.UI.Helper;
using System.Web.Configuration;
using System.Data.OleDb;
using System.IO;
using System.Data;
using System.Web.Security;
using System.Linq;
namespace TPS360.Web.UI.Admin
{
    public partial class BuddyReferralImage : AdminBasePage
    {
        OleDbConnection oldbConn;
        string strFilePath = string.Empty;
        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            //string url = UrlConstants.Teamplate.RESOURCE_DIR + UrlConstants.Teamplate.CANDIDATE_IMPORT_TEAMPLATE_FILENAME;

            //lnkDownload.NavigateUrl = url;
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            bool file = true;
            try
            {
                if (fuBuddyImage.HasFile)
                {
                    bool Size = ValidateFileDimensions();
                    if (Size)
                    {
                        strFilePath = System.Web.Hosting.HostingEnvironment.MapPath("~/Images/BuddyReferral-for-TTR.PNG");

                        fuBuddyImage.SaveAs(strFilePath);

                        MiscUtil.ShowMessage(lblMessage, "Image Uploaded Successfully.", false);
                    }
                    else
                    {
                        MiscUtil.ShowMessage(lblMessage, "Please select correct file size. (Dimensions: 1305 x 216)", false);
                    }

                }
                else
                {
                    MiscUtil.ShowMessage(lblMessage, "Please select file.", false);
                }
            }
            catch (Exception ex)
            {
                if (file == false)
                    MiscUtil.ShowMessage(lblMessage, "Please select appropriate file", false);
            }
            finally
            {

            }

        }

        public bool ValidateFileDimensions()
        {
            using (System.Drawing.Image myImage =
                   System.Drawing.Image.FromStream(fuBuddyImage.PostedFile.InputStream))
            {
                if (myImage.Height == 216 && myImage.Width == 1305)
                    return true;
                else
                    return false;
            }
        }

        #endregion
    }

}