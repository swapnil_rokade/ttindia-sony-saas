﻿<%@ Page Language="C#" MasterPageFile="~/Default.master" AutoEventWireup="true" CodeFile="CustomRoleEditor.aspx.cs"
    Inherits="TPS360.Web.UI.Admin.CustomRoleEditor" Title="Custom Role Editor" %>
<%@ Register assembly="Infragistics35.Web.v12.2, Version=12.2.20122.1007, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" namespace="Infragistics.Web.UI.LayoutControls" tagprefix="ig" %>
<%@ Register Src="../Controls/CustomRoleEditor.ascx" TagName="CustomRoleEditor" TagPrefix="uc1" %>
<%@ Register Src="../Controls/CustomRoleList.ascx" TagName="CustomRoleList" TagPrefix="uc2" %>

<asp:Content ID="cntSkillEditorHeader" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="cntSkillEditorTitle" ContentPlaceHolderID="cphHomeMasterTitle" Runat="Server">
Role Builder
</asp:Content>
<asp:Content ID="cntSkillEditor" ContentPlaceHolderID="cphHomeMaster" runat="Server">
    <%--<div class="GridContainer" style="width: 100%;">
        <igtab:UltraWebTab ID="UltraWebTab1" runat="server" EnableAppStyling="True" Width="100%">
            <DefaultTabStyle Height="16px" Font-Size="8pt">
                <Padding Top="2px"></Padding>
            </DefaultTabStyle>
            <RoundedImage LeftSideWidth="7" RightSideWidth="6" ShiftOfImages="2" SelectedImage="~/Images/ig_tab_winXPs1.gif"
                NormalImage="~/Images/ig_tab_winXPs3.gif" HoverImage="~/Images/ig_tab_winXPs2.gif"
                FillStyle="LeftMergedWithCenter"></RoundedImage>
            <SelectedTabStyle>
                <Padding Bottom="2px"></Padding>
            </SelectedTabStyle>
            <Tabs>
                <igtab:Tab Text="Custom Role Editor" Key="editorTab">
                    <ContentTemplate>
                        <div class="TabContentHolder">
                            <uc1:CustomRoleEditor ID="ctrlCustomRoleEditor" runat="server" />
                        </div>
                    </ContentTemplate>
                </igtab:Tab>
                
                <igtab:Tab Text="Custom Role List" Key="listTab">
                    <ContentTemplate>
                        <div class="TabContentHolder">
                       
                        </div>
                    </ContentTemplate>
                </igtab:Tab>                          
            </Tabs>
        </igtab:UltraWebTab>
    </div>--%>
    <div>
    
    <ig:WebTab ID="UltraWebTab1" runat="server" Width="100%" PostBackOptions-EnableDynamicUpdatePanels="true"  
    >
    <Tabs>
    <ig:ContentTabItem VisibleIndex="1" runat="server" Text="Custom Role Editor" Key="editorTab" >
    <Template>
    <div class="TabContentHolder">
    <uc1:CustomRoleEditor ID="ctrlCustomRoleEditor" runat="server" />
    </div>
    </Template>
    </ig:ContentTabItem>
    <ig:ContentTabItem VisibleIndex="2" runat="server" Text="Custom Role List" >
    <Template>
    <divclass="TabContentHolder">
        <uc2:CustomRoleList ID="ctrlCustomRoleList" runat="server" />
    </div>
    </Template>
    </ig:ContentTabItem>
    </Tabs>
    </ig:WebTab>
    </div>
</asp:Content>