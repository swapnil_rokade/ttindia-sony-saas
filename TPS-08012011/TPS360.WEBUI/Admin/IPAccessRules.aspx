﻿<%@ Page Language="C#" MasterPageFile="~/Default.master" AutoEventWireup="true" CodeFile="IPAccessRules.aspx.cs"
    Inherits="TPS360.Web.UI.Admin.IPAccessRules" Title="IP Access Rules" EnableEventValidation ="false"  %>
    <%@ Register Src ="~/Controls/IPAccessRules.ascx" TagName ="Rules" TagPrefix ="ucl" %>
<asp:Content ID="cntIPAccessRulesHeader" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="cntIPAccessRulesTitle" ContentPlaceHolderID="cphHomeMasterTitle"
    runat="Server">
    IP Access Rules
</asp:Content>
<asp:Content ID="cntIPAccessRules" ContentPlaceHolderID="cphHomeMaster" runat="Server">
    <div class="GridContainer" style="width: 100%;">
    <asp:UpdatePanel ID="upRules" runat ="server" >
    <ContentTemplate >
    
            <ucl:Rules id="uclIPAccessRules" runat="server"></ucl:Rules>        
            </ContentTemplate></asp:UpdatePanel>
    </div>
</asp:Content>