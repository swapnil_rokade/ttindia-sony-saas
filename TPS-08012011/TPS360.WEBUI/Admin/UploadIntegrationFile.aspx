﻿<%@ Page Language="C#" MasterPageFile="~/Default.master" AutoEventWireup="true" CodeFile="UploadIntegrationFile.aspx.cs"
    Inherits="TPS360.Web.UI.Admin.UploadIntegrationFile" Title="Upload Integration File"
    EnableEventValidation="false" %>
    
<asp:Content ID="cntUploadIntegrationFileTitle" ContentPlaceHolderID="cphHomeMasterTitle"
    runat="Server">
    Upload Integration File
</asp:Content>
<asp:Content ID="cntCandidateImport" ContentPlaceHolderID="cphHomeMaster" runat="Server">
<script type="text/javascript">
    function ValidateTextFileUpload(Source, args) {
        var fuData = document.getElementById('<%= fuEmployeeData.ClientID %>');
        var FileUploadPath = fuData.value;
        if (FileUploadPath == '') {
            // There is no file selected
            args.IsValid = false;
        }
        else {
            //var Extension = FileUploadPath.substring(FileUploadPath.lastIndexOf('.') + 1).toLowerCasevar
            string.includes(substring);
            if (FileUploadPath.toLowerCase().includes("employeedata.txt")) {      
//            if (FileUploadPath.toLowerCase() == "employeedata.txt") {              
                args.IsValid = true; // Valid file type
        }
        else {
                args.IsValid = false; // Not valid file type
        }
    }
}
function ValidateExcelFileUpload(Source, args) {
    var fuData = document.getElementById('<%= fuOrgonizationStructure.ClientID %>');
    var FileUploadPath = fuData.value;
 
    if (FileUploadPath == '') {
        // There is no file selected
        args.IsValid = false;
    }
    else {
        if(FileUploadPath.toLowerCase().indexOf("org.xls") >= 0) {
          args.IsValid = true; // Valid file type
        }
//        if (FileUploadPath.toLowerCase() == "org.xls") {
//            args.IsValid = true; // Valid file type
//        }
        else {
            args.IsValid = false; // Not valid file type
        }
    }
}

</script>
<asp:UpdatePanel ID="pnlBulkImport" runat="server">
<ContentTemplate>
<div class="TableRow" style="padding-top: 2em">
        <div class="TableFormLeble">
            Upload Employee Data:</div>
        <div class="TableFormContent">
            <asp:UpdatePanel ID="upDescriptionupload" runat="server">
                <ContentTemplate>
                <div style="text-align: left">
                    <asp:Label ID="lblMessage" runat="server"></asp:Label>
                </div>
                    <asp:FileUpload ID="fuEmployeeData" runat="server" CssClass="CommonButton" Width="206px" TabIndex="36" />
                     <asp:CustomValidator ID="cvEmployeeData" runat="server" ValidationGroup="UploadDocumentDescriotion" ControlToValidate="fuEmployeeData" ClientValidationFunction="ValidateTextFileUpload"
                        ErrorMessage="Please select valid File.(Name should be employeedata.txt)"></asp:CustomValidator>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnExport" />
                </Triggers> 
            </asp:UpdatePanel>
        </div>        
        
        <%--*********************--%>
          <div class="TableFormLeble">
            Upload Orgonization Structure:</div>
           <div class="TableFormContent">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                <div style="text-align: left">
                    <asp:Label ID="Label1" runat="server"></asp:Label>
                </div>
                    <asp:FileUpload ID="fuOrgonizationStructure" runat="server" CssClass="CommonButton" Width="206px" TabIndex="36" />

                     <asp:CustomValidator ID="cvOrgonizationStructure" runat="server" ValidationGroup="UploadDocumentDescriotion" ControlToValidate="fuOrgonizationStructure" ClientValidationFunction="ValidateExcelFileUpload"
                        ErrorMessage="Please select valid File.(Name should be org.xls)"></asp:CustomValidator>
          
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnExport" />
                </Triggers> 
            </asp:UpdatePanel>
        </div>
        <%--**********************--%>
    </div>
    
      <div class="TableRow">
            <div class="TableFormLeble">
            </div>
            <div class="TableFormContent">
                <asp:Button ID="btnExport" Text="Upload" runat="server" CssClass="CommonButton" OnClick="btnExport_Click"
                ValidationGroup="UploadDocumentDescriotion" TabIndex="37" />
            </div>
        </div>

  
</ContentTemplate>

</asp:UpdatePanel>

</asp:Content>
