﻿using System;
using System.Web.UI.WebControls;

using TPS360.Common.Helper;

using TPS360.Web.UI.Helper;
using TPS360.Common.Shared;

namespace TPS360.Web.UI.Admin
{
    public partial class LookUpEditor : AdminBasePage
    {
        #region Member Variables

        #endregion

        #region Methods

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_TAB]))
                {
                    string tabKey = Convert.ToString(Helper.Url.SecureUrl[UrlConstants.PARAM_TAB]);

                    if (tabKey != "")
                    {
                    }
                }
            }
        }

        #endregion
    }
}