﻿<%-- 
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: UserRoleMapEditorPage.ascx
    Description: assign the system role to user role.
    Created By: pravin khot
    Created On: 25/Feb/2016
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date             Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------       
--%>

<%@ Page EnableEventValidation="false" Language="C#" MasterPageFile="~/Default.master"
    AutoEventWireup="true" CodeFile="UserRoleMapEditors.aspx.cs" Inherits="TPS360.Web.UI.UserRoleMapEditors"
    Title="User Role Map Editor"  EnableViewStateMac ="false" %>
<%@ Register Src="~/Controls/PagerControl.ascx" TagName="Pager" TagPrefix="ucl" %>
<asp:Content ID="cntCandidateReportHeader" ContentPlaceHolderID="cphHomeMasterTitle" runat="Server">
User Role Map Editor
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphHomeMaster" runat="Server">
  <script language="javascript" type="text/javascript">
        Sys.Application.add_load(function() {

        $('.TableRow').show();
         });
        function RemoveRow(row)
        {
          $(row).parent().parent().remove();
        }
    </script>
 
 <asp:UpdatePanel ID="UPBuMapList" runat="server">
 <ContentTemplate>
            <asp:HiddenField ID="hdnSortColumnName" runat="server" Value="[ET].[Title]" />
            <asp:HiddenField ID="hdnSortColumnOrder" runat="server" Value="ASC" />
            <asp:HiddenField ID="hdnSortColumn" runat="server" Value="btnTitle" />
            <asp:HiddenField ID="hdnSortOrder" runat="server" Value="ASC" />
            <asp:HiddenField ID="hdnSelectedIDtoEdit" runat="server" />
            
            <div id="divBUMapping" runat="server" style="width: 100%">
                <div class="TableRow" style="text-align: center">
                    <asp:Label EnableViewState="false" ID="lblMessage" runat="server"></asp:Label>
                    <asp:HiddenField ID="hfBUId" runat="server" Value="0" />
                </div>
                
                <div class="TabPanelHeader">
                    Add Role/System Role Map
                </div>
                <div class="TableRow">
                        <div class="TableFormLeble">
                        <asp:Label EnableViewState="false" ID="lblUserName" runat="server" Text="Role"></asp:Label>:
                    </div>
                    <div class="TableFormContent">
                        <div id="divTeamTitle" runat="server">
                        </div>
                        <asp:DropDownList ID="ddlUser" runat="server" CssClass="CommonDropDownList" ></asp:DropDownList>
                        <span class="RequiredField">*</span>
                    </div>
                </div>
                 <div class="TableRow">
                     
                    <div class="TableFormValidatorContent" style="margin-left: 42%">
                        <asp:CompareValidator ID="rfvUser" runat="server" ControlToValidate="ddlUser"
                            ErrorMessage="Please select User." EnableViewState="False" Display="Dynamic"
                            ValueToCompare="0" Operator="GreaterThan"
                            ValidationGroup="BUUserMappinggroup1"></asp:CompareValidator>
                     </div>
                 </div>
                 
                 
                  <div class="TableRow" style="">
                    <div class="TableFormLeble">
                        <asp:Label EnableViewState="false" ID="lblBUList" runat="server" Text="System Role"></asp:Label>:
                    </div>
                    <div class="TableFormContent">
                         <asp:DropDownList ID="ddlSystemRole" runat="server" CssClass="CommonDropDownList" >
                           <asp:ListItem Value="0">Please Select</asp:ListItem>
                             <asp:ListItem Value="1">Recruiter</asp:ListItem>
                              <asp:ListItem Value="2">Candidate</asp:ListItem>
                               <asp:ListItem Value="3">Hiring Manager</asp:ListItem>
                                <asp:ListItem Value="4">Vendor</asp:ListItem>
                                 <asp:ListItem Value="5">BU Contact</asp:ListItem>
                         </asp:DropDownList>
                        <span class="RequiredField" style="height: 110px">*</span>
                       <%-- <asp:Button ID="btnAddTeamMembers" CssClass="CommonButton" runat="server" Text="Add"
                            OnClick="btnAddBU_Click" TabIndex="4" EnableViewState="true" ValidationGroup="BUUserMappinggroup1"/>--%>
                    </div>
                </div>
            
                 <%-- <div class="TableRow" style="">
                    <div class="TableFormLeble">
                        <asp:Label EnableViewState="false" ID="lblBUList" runat="server" Text="Associated BU's"></asp:Label>:
                    </div>
                    <div class="TableFormContent">
                        <asp:ListBox ID="lstBUList" runat="server" SelectionMode="multiple" CssClass="CommonDropDownList"
                            Width="250px" Height="110px" TabIndex="3"></asp:ListBox>
                        <span class="RequiredField" style="height: 110px">*</span>
                        <asp:Button ID="btnAddTeamMembers" CssClass="CommonButton" runat="server" Text="Add"
                            OnClick="btnAddBU_Click" TabIndex="4" EnableViewState="true" ValidationGroup="BUUserMappinggroup1"/>
                    </div>
                </div>--%>
                <div class="TableRow">
                    <div class="TableFormValidatorContent" style="margin-left: 42%">
                        <asp:RequiredFieldValidator ID="rfvBUList" runat="server" ControlToValidate="ddlSystemRole"
                            ErrorMessage="Please select BU." EnableViewState="False" Display="Dynamic"
                            ValidationGroup="BUUserMappinggroup1"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <br />
                <div class="TableRow" style="width: 100%;">
                    <div class="TableFormLeble">
                    </div>
                   <%-- <asp:UpdatePanel ID="upPanel" UpdateMode="Conditional" runat="server">
                        <ContentTemplate>
                        <div style="vertical-align: text-top; float: left; width: 300px;">
                            <asp:ListView ID="lsvBUList" runat="server" DataKeyNames="Id" OnItemDataBound="lsvBUList_ItemDataBound"
                                    OnPreRender="lsvBUList_PreRender" >
                                    <LayoutTemplate>
                                        <table id="tlbTemplate" runat="server" class="Grid" cellspacing="0" border="0" width="100%">
                                            <tr>
                                                <th style="white-space: nowrap;">
                                                    BU
                                                </th>
                                                <th id="thUnAssign" style="text-align: center; white-space: nowrap; width: 55px;">
                                                    Remove
                                                </th>
                                            </tr>
                                            <tr id="itemPlaceholder" runat="server">
                                            </tr>
                                        </table>
                                    </LayoutTemplate>
                                    <ItemTemplate>
                                    <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                                            <td style="text-align: left;">
                                                <asp:Label ID="lblBUName" runat="server" />
                                                <asp:HiddenField ID="hdnBUID" runat="server" />
                                            </td>
                                            <td id="tdUnAssign" style="text-align: center;">
                                                <asp:ImageButton ID="btnDelete" SkinID="sknDeleteButton" runat="server" CommandName="DeleteItem"
                                                    OnClientClick="RemoveRow(this); return;"></asp:ImageButton>
                                            </td>
                                    </tr>
                                    </ItemTemplate>
                            </asp:ListView>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>--%>
                    <br />
                    </div>
                    <br />
                    <br />
                <div class="TableRow" style="text-align: center">
                    <asp:Button ID="btnSave" CssClass="CommonButton" runat="server" Text="Save" ValidationGroup="BUUserMappinggroup1"
                        OnClick="btnSave_Click" TabIndex="5"  />
                </div>
                <div class="TabPanelHeader">
                    List of Mappings
                </div>
                    </div>
                    <div>
                    <div class="GridContainer" style="text-align: left;">
                    <asp:ObjectDataSource ID="odsUserRoleMapEditor" runat="server" SelectMethod="GetPagedUserRoleMapEditor" SelectCountMethod="GetCountUserRoleMapEditor"
                    TypeName="TPS360.Web.UI.EmployeeDataSource" EnablePaging="true" SortParameterName="sortExpression">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="hdnSortColumnOrder" Name="sortOrder" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                    <asp:UpdatePanel ID="upUserRoleMapEditor" runat="server">
                        <ContentTemplate>
                            <asp:ListView ID="lsvUserRoleMapEditor" runat="server" DataSourceID="odsUserRoleMapEditor" DataKeyNames="Id" OnItemDataBound="lsvUserRoleMapEditor_ItemDataBound"
                            OnPreRender="lsvUserRoleMapEditor_PreRender" OnItemCommand="lsvUserRoleMapEditor_ItemCommand" EnableViewState="true">
                                <LayoutTemplate>
                                    <table id="tlbBUMapping" class="Grid" cellspacing="0" border="0" style="width: 100%">
                                       <tr id="Tr1" style="width: 100%" enableviewstate="true" runat="server">
                                        <th>
                                        <asp:LinkButton ID="btnUser" runat="server" ToolTip="Sort By User" CommandName="Sort"
                                            CommandArgument="[CR].[NAME]" Text="Role" TabIndex="7" />
                                        </th>
                                        <th>
                                           <asp:LinkButton ID="btnSysrole" runat="server" ToolTip="Sort By User" CommandName="Sort"
                                            CommandArgument="[UR].[Role]" Text="System Role" TabIndex="8" />                                            
                                        </th>
                                        <th style="width: 50px !important" runat="server" id="thAction">
                                            Action
                                        </th>
                                     </tr>
                                     <tr id="itemPlaceholder" runat="server">
                                     </tr>
                                    <tr class="Pager">
                                        <td colspan="3" runat="server" id="tdPager" >
                                            <ucl:Pager ID="pagerControl" runat="server" EnableViewState="true"   />
                                        </td>
                                    </tr>
                                    </table>
                                </LayoutTemplate>
                                <EmptyDataTemplate>
                                   <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                                        <tr>
                                            <td>
                                                No Mapping data available.
                                            </td>
                                        </tr>
                                    </table>
                                </EmptyDataTemplate>
                                <ItemTemplate>
                                    <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2== 0 ? "row" : "altrow" %>'>
                                        <td>
                                            <asp:Label ID="lblRole" runat="server" />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblSystemRole" runat="server" />
                                        </td>
                                        <td style="" runat="server" id="tdAction">
                                            <asp:ImageButton ID="btnEdit" SkinID="sknEditButton" CausesValidation="false" runat="server"
                                            CommandName="EditItem" TabIndex="8"></asp:ImageButton>
                                            <asp:ImageButton ID="btnDeleteList" Visible="true" SkinID="sknDeleteButton" runat="server"
                                            CommandName="DeleteItem" TabIndex="9" ></asp:ImageButton>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:ListView>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    </div></div>
 </ContentTemplate>
 </asp:UpdatePanel>
</asp:Content>