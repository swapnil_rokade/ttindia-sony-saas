﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Web.UI.Helper;
using TPS360.Web.UI;
using System.Collections;

public partial class AllUpcomingInterviews : TPS360.Web.UI.BaseControl, IWidget
{
    #region Member Variables
    private bool isAccess = false;
    private bool isAccessToCompany = false;
    #endregion
    #region Properties

    private IWidgetHost _Host;

    public IWidgetHost Host
    {
        get { return _Host; }
        set { _Host = value; }
    }

    private string _userId;
    public string UserId
    {
        get { return _userId; }
        set { _userId = value; }
    }

    #endregion

    #region Events

    private void setParameters()
    {
        odsInterviewSchedule.SelectParameters.Clear();
        odsInterviewSchedule.SelectParameters.Add("MemberId", "-1");
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        (this as IWidget).HideSettings();

        ArrayList AList = Facade.GetAllMemberPrivilegeIdsByMemberId(CurrentMember.Id);
        if (AList.Contains(361))
            isAccess = true;
        else
            isAccess = false;
        if (AList.Contains(400))
            isAccessToCompany = true;
        else
            isAccessToCompany = false;
        setParameters();
        if (!IsPostBack)
        {
            GetWidgetData();
            txtSortColumn.Text = "btnDateTime";
            txtSortOrder.Text = "DESC";
            PlaceUpDownArrow();
        }
        string pagesize = "";
        pagesize = (Request.Cookies["DashboardAllUpComingInterviewsRowPerPage"] == null ? "" : Request.Cookies["DashboardAllUpComingInterviewsRowPerPage"].Value); ;
        ASP.controls_smallpagercontrol_ascx PagerControl = (ASP.controls_smallpagercontrol_ascx)this.lsvInterviewSchedule.FindControl("pagerControl");
        if (PagerControl != null)
        {
            DataPager pager = (DataPager)PagerControl.FindControl("pager");
            if (pager != null) pager.PageSize = Convert.ToInt32(pagesize == "" ? "10" : pagesize);
        }
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
    }

    private DateTime getTime(DateTime date, int duration)
    {
        if (duration > 0)
        {
            duration = duration / 60;
        }
        return date.AddMinutes(Convert.ToDouble(duration));
    }

    private void PlaceUpDownArrow()
    {
        try
        {
            LinkButton lnk = (LinkButton)lsvInterviewSchedule.FindControl(txtSortColumn.Text);
            System.Web.UI.HtmlControls.HtmlTableCell im = (System.Web.UI.HtmlControls.HtmlTableCell)lnk.Parent;
            im.Attributes.Add("class", ( txtSortOrder.Text == "ASC" ? "Ascending" : "Descending"));
                
        }
        catch
        {
        }

    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        (this as IWidget).HideSettings();
    }

    #region ListView Events

    protected void lsvInterviewSchedule_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
        {
            MemberInterview memberInterview = ((ListViewDataItem)e.Item).DataItem as MemberInterview;
            string _strFullName = memberInterview.FirstName + " " + memberInterview.LastName;
            if (_strFullName.Trim() == "")
                _strFullName = "No Candidate Name";
            if (memberInterview != null)
            {
                Label lblDateTime = (Label)e.Item.FindControl("lblDateTime");
                HyperLink lnkJobTitle = (HyperLink)e.Item.FindControl("lnkRequisition");
                HyperLink lnkCandidateName = (HyperLink)e.Item.FindControl("lnkCandidateName");
                Label lblInternalInterviewers = (Label)e.Item.FindControl("lblInternalInterviewers");
                /*
                Label lblLocation = (Label)e.Item.FindControl("lblLocation");
                Label lblInterviewType = (Label)e.Item.FindControl("lblInterviewType");
                HyperLink lnkClient = (HyperLink)e.Item.FindControl("lnkClient");
                Label lblClientInterviewers = (Label)e.Item.FindControl("lblClientInterviewers");
                Label lblInternalInterviewers = (Label)e.Item.FindControl("lblInternalInterviewers");
                Label lblNotes = (Label)e.Item.FindControl("lblNotes");
                 * */
                if (memberInterview.AllDayEvent)
                {
                    lblDateTime.Text = memberInterview.StartDateTime.ToShortDateString() + " All Day";
                }
                else
                {
                    DateTime date = new DateTime();
                    date = getTime(memberInterview.StartDateTime, memberInterview.Duration);
                    if (date.Hour > 12 || memberInterview.StartDateTime.Hour > 12)
                        lblDateTime.Text = memberInterview.StartDateTime.ToShortDateString() + " " + memberInterview.StartDateTime.ToString("hh:mm tt");//+ " - " + getTime(memberInterview.StartDateTime, memberInterview.Duration).ToString("hh:mm tt") + "<br/>";
                    else
                        lblDateTime.Text = memberInterview.StartDateTime.ToShortDateString() + " " + memberInterview.StartDateTime.ToString("hh:mm");// +" - " + getTime(memberInterview.StartDateTime, memberInterview.Duration).ToString("hh:mm tt") + "<br/>";
                }
                /*
                lblLocation.Text = memberInterview.Location;
                lblInterviewType.Text = memberInterview.InterviewTypeName;

                if (memberInterview.CompanyId > 0)
                {
                    if (isAccessToCompany)
                        ControlHelper.SetHyperLink(lnkClient, UrlConstants.SFA.COMPANY_OVERVIEW_PAGE, string.Empty, memberInterview.CompanyName, UrlConstants.PARAM_COMPANY_ID, memberInterview.CompanyId.ToString(), UrlConstants.PARAM_SITEMAP_ID, "400");
                    else
                    {
                        lnkClient.Enabled = false;
                        lnkClient.Text = memberInterview.CompanyName;
                    }
                }
                lblClientInterviewers.Text = memberInterview.ClientInterviewerName;
                lblInternalInterviewers.Text = memberInterview.InterviewerName;
                if (memberInterview.Remark != string.Empty)
                {
                    if (memberInterview.Remark.Length > 40)
                    {
                        lblNotes.Text = memberInterview.Remark.Substring(0, 40) + "...";
                        lblNotes.ToolTip = memberInterview.Remark;
                    }
                    else
                        lblNotes.Text = memberInterview.Remark;
                }
                 * */
                lblInternalInterviewers.Text = memberInterview.InterviewerName;
                if (isAccess)
                    ControlHelper.SetHyperLink(lnkCandidateName, UrlConstants.Candidate.OVERVIEW, string.Empty, _strFullName, UrlConstants.PARAM_MEMBER_ID, memberInterview.MemberId.ToString(), UrlConstants.PARAM_SITEMAP_ID, "361");
                else
                {
                    lnkCandidateName.Enabled = false;
                    lnkCandidateName.Text = _strFullName;// memberInterview.FirstName + " " + memberInterview.LastName;
                }
                if (memberInterview.JobPostingId > 0)
                {
                    lnkJobTitle.Attributes.Add("onclick", "EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/JobDetail.aspx?JID=" + memberInterview.JobPostingId + "&FromPage=JobDetail" + "','700px','570px'); return false;");
                    lnkJobTitle.Text = memberInterview.JobTitle;
                    
                }
            }
        }
    }

    protected void lsvInterviewSchedule_PreRender(object sender, EventArgs e)
    {
        lsvInterviewSchedule.DataBind();
        ASP.controls_smallpagercontrol_ascx PagerControl = (ASP.controls_smallpagercontrol_ascx)this.lsvInterviewSchedule.FindControl("pagerControl");
        if (PagerControl != null)
        {
            DataPager pager = (DataPager)PagerControl.FindControl("pager");
            if (pager != null)
            {
                if (pager.Controls.Count >= 1)
                {
                    DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                    if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
                }
            }
            HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
            if (hdnRowPerPageName != null) hdnRowPerPageName.Value = "DashboardAllUpComingInterviewsRowPerPage";
        }
        PlaceUpDownArrow();
        if (lsvInterviewSchedule.Controls.Count == 0)
        {
            lsvInterviewSchedule.DataSource = null;
            lsvInterviewSchedule.DataBind();
        }
    }

    protected void lsvInterviewSchedule_ItemCommand(object sender, ListViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Sort")
            {
                LinkButton lnkbutton = (LinkButton)e.CommandSource;
                if (txtSortColumn.Text == lnkbutton.ID)
                {
                    if (txtSortOrder.Text == "ASC") txtSortOrder.Text = "DESC";
                    else txtSortOrder.Text = "ASC";
                }
                else
                {
                    txtSortColumn.Text = lnkbutton.ID;
                    txtSortOrder.Text = "ASC";
                }

            }
        }
        catch
        {
        }
    }

    #endregion

    #endregion

    #region Methods

    public void GetWidgetData()
    {
        try
        {
            odsInterviewSchedule.SelectParameters["MemberId"].DefaultValue = "-1";
            this.lsvInterviewSchedule.DataBind();
        }
        catch
        {
        }
    }

    #endregion

    #region Implementation

    void IWidget.Init(IWidgetHost host)
    {
        this.Host = host;
    }

    void IWidget.ShowSettings()
    {
        pnlSettings.Visible = true;
    }
    void IWidget.HideSettings()
    {
        pnlSettings.Visible = false;
    }
    void IWidget.Minimized()
    {
    }
    void IWidget.Maximized()
    {
        if (lsvInterviewSchedule != null)
        {
            if (lsvInterviewSchedule.Items.Count == 0)
            {
                lsvInterviewSchedule.DataSource = null;
                lsvInterviewSchedule.DataBind();
            }
        }
    }
    void IWidget.Closed()
    {
    }

    #endregion
}
