﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Web.UI.Helper;
using TPS360.Web.UI;
using System.Collections;

public partial class CurrentSessions : TPS360.Web.UI.BaseControl, IWidget
{
    #region Member Variables
    private bool _IsAccessToEmployee = true;
    private  string UrlForEmployee = string.Empty;
    private  int IdForSitemap = 0;
    #endregion

    #region Properties

    private IWidgetHost _Host;

    public IWidgetHost Host
    {
        get { return _Host; }
        set { _Host = value; }
    }

    private string _userId;
    public string UserId
    {
        get { return _userId; }
        set { _userId = value; }
    }

    #endregion
    #region Page Events
    private void setParameters()
    {
        odsCurrentSessions.SelectParameters.Clear();
        odsCurrentSessions.SelectParameters.Add("SortOrder", txtSortOrder.Text);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        setParameters();
      
      
        CustomSiteMap CustomMap = new CustomSiteMap();
        CustomMap = Facade.GetAllCustomSiteMapByParentIdAndMemberPrivilege(360, CurrentMember.Id);
        if (CustomMap == null) _IsAccessToEmployee = false;
        else
        {
            IdForSitemap = CustomMap.Id;
            UrlForEmployee = "~/" + CustomMap.Url.ToString();
        }
        
        (this as IWidget).HideSettings();
      
        
        if (!IsPostBack)
        {
            GetWidgetData();
            txtSortColumn.Text = "btnEmployeeName";
            txtSortOrder.Text = "ASC";
            PlaceUpDownArrow();
        }
        string pagesize = "";
        pagesize = (Request.Cookies["DashboardCurrentSessionsRowPerPage"] == null ? "" : Request.Cookies["DashboardCurrentSessionsRowPerPage"].Value); ;
        ASP.controls_smallpagercontrol_ascx PagerControl = (ASP.controls_smallpagercontrol_ascx)this.lsvCurrentSessions.FindControl("pagerControl");
        if (PagerControl != null)
        {
            DataPager pager = (DataPager)PagerControl.FindControl("pager");
            if (pager != null) pager.PageSize = Convert.ToInt32(pagesize == "" ? "10" : pagesize); 
        }
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
    }
    #endregion

    #region Listview Events
    protected void lsvCurrentSessions_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
        {
            TPS360 .Common .BusinessEntities . CurrentSessions _currentSessions = ((ListViewDataItem)e.Item).DataItem as TPS360 .Common .BusinessEntities .CurrentSessions;

            if (_currentSessions != null)
            {
                Label lblSessionType = (Label)e.Item.FindControl("lblSessionType");
                Label lblLoggedIn = (Label)e.Item.FindControl("lblLoggedIn");
                HyperLink lnkEmployeeName = (HyperLink)e.Item.FindControl("lnkEmployeeName");
                if (_currentSessions.ApplicationName == "TPS")
                    lblSessionType.Text = "Web";
                else if (_currentSessions.ApplicationName == "RP")
                    lblSessionType.Text = "Resume Parser";
                else lblSessionType.Text = "OL AddIn";
                lblLoggedIn.Text = _currentSessions.LoggedIn.ToShortDateString() + " " + _currentSessions.LoggedIn.ToString("HH:mm");

                if (_IsAccessToEmployee)
                {
                    if (IdForSitemap > 0 && UrlForEmployee != string.Empty)
                        ControlHelper.SetHyperLink(lnkEmployeeName, UrlForEmployee, string.Empty, _currentSessions.EmployeeName, UrlConstants.PARAM_MEMBER_ID, StringHelper.Convert(_currentSessions.Id ), UrlConstants.PARAM_SITEMAP_ID, IdForSitemap.ToString());
                }
                else lnkEmployeeName.Text = _currentSessions.EmployeeName;


                //lnkEmployeeName.Text = _currentSessions.EmployeeName;
            }


        }
    }

    protected void lsvCurrentSessions_PreRender(object sender, EventArgs e)
    {
        lsvCurrentSessions.DataBind();
      
        ASP.controls_smallpagercontrol_ascx PagerControl = (ASP.controls_smallpagercontrol_ascx)this.lsvCurrentSessions.FindControl("pagerControl");
        if (PagerControl != null)
        {
            DataPager pager = (DataPager)PagerControl.FindControl("pager");
            if (pager != null)
            {
                if (pager.Controls.Count >= 1)
                {
                    DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                    if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
                }
            }
            HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
            if (hdnRowPerPageName != null) hdnRowPerPageName.Value = "DashboardCurrentSessionsRowPerPage";
        }
        PlaceUpDownArrow();
        if (lsvCurrentSessions != null)
        {
            if (lsvCurrentSessions.Items.Count == 0)
            {
                lsvCurrentSessions.DataSource = null;
                lsvCurrentSessions.DataBind();
            }
        }
        
    }

    protected void lsvCurrentSessions_ItemCommand(object sender, ListViewCommandEventArgs e)
    {
        try
        {
           
            if (e.CommandName == "Sort")
            {
                LinkButton lnkbutton = (LinkButton)e.CommandSource;
                if (txtSortColumn.Text == lnkbutton.ID)
                {
                    if (txtSortOrder.Text == "ASC") txtSortOrder.Text = "DESC";
                    else txtSortOrder.Text = "ASC";
                }
                else
                {
                    txtSortColumn.Text = lnkbutton.ID;
                    txtSortOrder.Text = "ASC";
                }

            }

            odsCurrentSessions.SelectParameters["SortOrder"].DefaultValue = txtSortOrder.Text; 
        }
        catch
        {
        }
    }
    #endregion
    #region Methods
    public void GetWidgetData()
    {
        try
        {
          odsCurrentSessions.SelectParameters["SortOrder"].DefaultValue = txtSortOrder.Text ;
            this.lsvCurrentSessions.DataBind();
        }
        catch
        {
        }
    }

    private void PlaceUpDownArrow()
    {
        try
        {
            LinkButton lnk = (LinkButton) lsvCurrentSessions.FindControl(txtSortColumn.Text);
            System.Web.UI.HtmlControls.HtmlTableCell th = (System.Web.UI.HtmlControls.HtmlTableCell)lnk.Parent;
            th.Attributes.Add("class", ( txtSortOrder .Text   == "ASC" ? "Ascending" : "Descending"));
        }
        catch
        {
        }

    }
    #endregion
    #region Implementation

    void IWidget.Init(IWidgetHost host)
    {
        this.Host = host;
    }

    void IWidget.ShowSettings()
    {
        pnlSettings.Visible = true;
    }
    void IWidget.HideSettings()
    {
        pnlSettings.Visible = false;
    }
    void IWidget.Minimized()
    {
    }
    void IWidget.Maximized()
    {

        if (lsvCurrentSessions != null)
        {
            if (lsvCurrentSessions.Items.Count == 0)
            {
                lsvCurrentSessions.DataSource = null;
                lsvCurrentSessions.DataBind();
            }
        }
    }
    void IWidget.Closed()
    {
    }

    #endregion
}
