﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WidgetPanels.ascx.cs"
    Inherits="WidgetPanels" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="WidgetContainer.ascx" TagName="WidgetContainer" TagPrefix="widget" %>
<table width="98%" cellspacing="10" align="left" class="table_fixed" height="100%">
    <tbody>
        <tr>
            <td style="width: 100%; vertical-align: top;" colspan="2">
                <asp:UpdatePanel ID="ScheduleUpdatePanel" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Panel ID="SchedulePanel" runat="server" columnNo="4" class="widget_Scheduleview">
                            <div id="DropCue7" class="widget_dropzone">
                            </div>
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td style="width: 100%; vertical-align: top;" colspan="2">
                <asp:UpdatePanel ID="CenterUpdatePanel" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Panel ID="CenterPanel" runat="server" columnNo="3" class="widget_fullview">
                            <div id="DropCue5" class="widget_dropzone">
                            </div>
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td style="width: 50%; vertical-align: top">
                <asp:UpdatePanel ID="LeftUpdatePanel" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Panel ID="LeftPanel" runat="server" class="widget_holder" columnNo="0">
                            <div id="DropCue1" class="widget_dropzone">
                            </div>
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td style="width: 50%; vertical-align: top;">
                <asp:UpdatePanel ID="RightUpdatePanel" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Panel ID="RightPanel" runat="server" class="widget_holder" columnNo="2">
                            <div id="DropCue3" class="widget_dropzone">
                            </div>
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </tbody>
</table>
