<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RequisitionList.ascx.cs"
    Inherits="RequisitionList" %>
<%@ Register Src="~/Controls/SmallPagerControl.ascx" TagName="Pager" TagPrefix="ucl" %>
<asp:Panel ID="pnlSettings" runat="server">
</asp:Panel>
<asp:TextBox ID="SortColumn" runat="server" EnableViewState="true" Visible="false" />
<asp:TextBox ID="SortOrder" runat="server" EnableViewState="true" Visible="false" />
<asp:TextBox ID="txtSortColumn1" runat="server" Visible="false"></asp:TextBox>
<asp:TextBox ID="txtSortOrder1" runat="server" Visible="false"></asp:TextBox>
<asp:Panel ID="widgetBody" runat="server">
    <%--<asp:UpdatePanel ID="widgetBody" runat="server" EnableViewState ="true" >
<ContentTemplate >--%>
    <div class="CommonDashboardContent">
        <div style="overflow: auto;">
            <asp:ObjectDataSource ID="odsMyRequisitionList" runat="server" SelectMethod="GetPaged"
                TypeName="TPS360.Web.UI.JobPostingDataSource" SelectCountMethod="GetListCount"
                EnablePaging="True" SortParameterName="sortExpression">
                <SelectParameters>
                    <asp:Parameter Name="memberId" DefaultValue="0" />
                    <asp:Parameter Name="IsTemplate" DefaultValue="False" Type="Boolean" />
                    <asp:Parameter Name="IsDashBoard" DefaultValue="True" Type="Boolean" />
                    <asp:Parameter Name="SortOrder" DefaultValue="asc" />
                </SelectParameters>
            </asp:ObjectDataSource>
            <asp:ListView ID="lsvRequisition" runat="server" DataSourceID="odsMyRequisitionList"
                DataKeyNames="Id" EnableViewState="true" OnItemDataBound="lsvRequisition_ItemDataBound"
                OnItemCommand="lsvRequisition_ItemCommand" OnPreRender="lsvRequisition_PreRender">
                <LayoutTemplate>
                    <table id="tlbTemplate" runat="server" class="Grid" cellspacing="0" border="0">
                        <tr id="trJobTitle" runat="server">
                        
                            <th runat="server" id="thCode" visible="true"  align="center" style="min-width: 30px; white-space: nowrap; width:110px" enableviewstate="false">
                                <asp:LinkButton ID="btnCode" runat="server" align="center" ToolTip="Sort By Req Code" CommandName="Sort"
                                    CommandArgument="[J].[JobPostingCode]" Text="Req Code" />
                            </th>
                            <th style="min-width: 80px; white-space: nowrap;width:210px" enableviewstate="false">
                                <asp:LinkButton ID="btnJobTitle" runat="server" ToolTip="Sort By Req Title" CommandName="Sort"
                                    CommandArgument="[J].[JobTitle]" Text="Req Title" />
                            </th>
                         
                             <th id="thClientJobID" runat="server" visible="false" style="min-width: 60px; white-space: nowrap;" enableviewstate="false">
                                <asp:LinkButton ID="btnClientJobID" runat="server" ToolTip="Sort By Client Job ID" CommandName="Sort"
                                    CommandArgument="[J].[JobPostingCode]" Text="Client Job ID" />
                            </th>
                            
                          
                          <%--  <th style="min-width: 120px; white-space: nowrap;" enableviewstate="false">
                                <asp:LinkButton ID="btnDatePublished" runat="server" ToolTip="Sort By Date Published"
                                    CommandName="Sort" CommandArgument="[J].[PostedDate]" Text="Date Published   " />
                            </th>--%>
                            <th style="min-width: 40px; text-align: center; white-space: nowrap ; width:110px" runat="server"  enableviewstate="false">
                                <asp:LinkButton ID="btnTotalCandidates" runat="server" ToolTip="Sort By Total Candidates"
                                    CommandName="Sort" CommandArgument="[1]" Text="Total Candidates" />
                            </th>
                            
                           <%--  <th style="width: 40px !important">
                                    <asp:LinkButton ID="lnkTeam" runat="server" Text="Team" ToolTip="Sort By Team" CommandArgument="[JMC].[ManagersCount]"
                                        CommandName="Sort"></asp:LinkButton>
                                </th>--%>
                                
                              <%--Changes end By Vishal Tripathy--%>
                              <%--  <th style="white-space: nowrap; width:112px;" runat="server" id="thAssigningManager">
                                    Assigning<br /> Manager
                                </th>--%>
                                  <%--******************************************************************--%>
                                 <%--******************************************************************--%>
                                <%--Rupesh Kadam Start--%>
                                
                             <%--  <th style="min-width: 75px; white-space: nowrap; min-width: 110px" enableviewstate="false">
                                <asp:LinkButton ID="btnCurrentStatus" runat="server" ToolTip="Sort By Current Status"
                                    CommandName="Sort" CommandArgument="[GL].[Name]" Text="Current Status" />
                            </th>--%>
                                 <th style="white-space: nowrap; width:150px" runat="server" id="th2">
                                     Last Approver
                                </th>
                                <th style="white-space: nowrap; width:150px" runat="server" id="th1">
                                    Stage
                                </th>
                                
                                <th style="white-space: nowrap; width:150px" runat="server" id="th3">
                                    Submitted By
                                </th>
                                
                                <th  style="min-width: 40px; text-align: center; white-space: nowrap ; width:110px" runat="server">
                                <%--Changes start by Vishal Tripathy--%>
                                    <asp:LinkButton ID="btnPostedDate" runat="server" CommandName="Sort" ToolTip="Sort By Post Date"
                                        CommandArgument="[J].[PostedDate]" Text="Submitted On" TabIndex="2" />
                                    <%--    Changes end by Vishal Tripathy--%>
                                </th>
                                <%--Rupesh Kadam End--%>
                                <%--******************************************************************--%>
                        </tr>
                        <tr id="itemPlaceholder" runat="server">
                        </tr>
                        <tr class="Pager">
                            <td colspan="7">
                                <ucl:Pager ID="pagerControl" runat="server" />
                            </td>
                        </tr>
                    </table>
                </LayoutTemplate>
                <EmptyDataTemplate>
                    <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server"
                        style="width: 100%; margin: 0px 0px;">
                        <tr>
                            <td>
                                No requisitions available.
                            </td>
                        </tr>
                    </table>
                </EmptyDataTemplate>
                <ItemTemplate>
                    <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                            <%--Changes start by vishal tripathy--%>
                            <td runat="server" id="tdJobCode" >
                                 <asp:Label ID="lblJobCode" runat="server" />
                            </td>
                            <%--Changes end by vishal tripathy--%>
                        <td>
                           <%-- <asp:HyperLink ID="lblJobTitle" runat="server" ></asp:HyperLink>--%>
                            <asp:Label ID="lblJobTitle" runat="server" />
                        </td>
                       
                        <td runat="server" id="tdClientJobID"  visible="false">
                            <asp:Label ID="lblClientJobID" runat="server" />
                        </td>
                        
                      
                      <%--  <td>
                            <asp:Label ID="lblJobPostedDate" runat="server" />
                        </td>--%>
                        <td  style="text-align: center; white-space: nowrap; width: 25px !important; ">
                          <%--  <asp:HyperLink ID="hlnkNoOfCandidates" runat="server" ></asp:HyperLink>--%>
                            <asp:Label ID="hlnkNoOfCandidates" align="center" runat="server" />
                        </td>
                        
                        <%-- <td>
                              <%--  <asp:LinkButton ID="lnkTeamCount" runat="server" CommandName="Managers"></asp:LinkButton>
                                 <asp:Label ID="lnkTeamCount" runat="server" />
                            </td>--%>
                        <%--  <td runat="server" id="tdAssigningManager">
                                <asp:Label ID="lblAssignedManager" runat="server" />
                            </td>--%>
                           <%--******************************************************************--%>
                                <%--Rupesh Kadam Start--%>
                        <%--   <td>
                            <asp:Label ID="lblJobstatus" runat="server" />
                           </td>--%>
                                <td runat="server" id="tdApprovingManger">
                                <asp:Label ID="lblApprovingManager" runat="server" />
                            </td>                            
                          
                            <td runat="server" id="tdApprovalStatus">
                                <asp:Label ID="lblApprovalStatus" runat="server" />
                            </td>
                            
                              <td runat="server" id="td1" style="width:100px">
                                <asp:Label ID="lblPostedBy" runat="server" />
                            </td>
                            <td runat="server" id="tdpostte" style="width:100px;text-align: center;">
                                <asp:Label ID="lblPostedDate" runat="server"></asp:Label>
                            </td>
                                <%--Rupesh Kadam End--%>
                            <%--******************************************************************--%>      
                    </tr>
                </ItemTemplate>
            </asp:ListView>
        </div>
    </div>
</asp:Panel>
<%--</ContentTemplate>
</asp:UpdatePanel> --%>