<%--
    ------------------------------------------------------------------------------------------------------------------------------
    FileName: UpcommingInterviews.ascx
    Description: 
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date             Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1             Dec-25-2008          Anand Dixit         Defect id: 9157 
    ------------------------------------------------------------------------------------------------------------------------------

--%>
<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UpcomingInterviews.ascx.cs"
    Inherits="UpcomingInterviews" %>
<%@ Register Src="~/Controls/SmallPagerControl.ascx" TagName="SmlPager" TagPrefix="ucl" %>
<%@ Register Src="~/Controls/PagerControl.ascx" TagName="Pager" TagPrefix="ucl" %>

        <asp:Panel ID="pnlSettings" runat="server">
        </asp:Panel>
        <asp:TextBox ID="txtSortColumn" runat="server" Visible="false"></asp:TextBox>
        <asp:TextBox ID="txtSortOrder" runat="server" Visible="false"></asp:TextBox>
        <asp:Panel ID="widgetBody" runat="server" EnableViewState="true">
         <asp:UpdatePanel ID="upUpcommingInterviews" runat ="server" >
         <ContentTemplate >
         
                <asp:ObjectDataSource ID="odsInterviewSchedule" runat="server" SelectMethod="GetPagedForDashboard"
                    TypeName="TPS360.Web.UI.MemberInterviewDataSource" SelectCountMethod="GetListCountForDashboard"
                    EnablePaging="True" SortParameterName="sortExpression">
                    <SelectParameters>
                        <asp:Parameter Name="MemberId" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:ListView ID="lsvInterviewSchedule" runat="server" DataKeyNames="Id" DataSourceID="odsInterviewSchedule"
                    OnItemDataBound="lsvInterviewSchedule_ItemDataBound" EnableViewState="true "
                    OnPreRender="lsvInterviewSchedule_PreRender" OnItemCommand="lsvInterviewSchedule_ItemCommand">
                    <LayoutTemplate>
                        <table id="tlbTemplate" runat="server" class="Grid" cellspacing="0" border="0">
                            <tr>
                                <th>
                                    <asp:LinkButton ID="btnDateTime" runat="server" ToolTip="Sort By Date & Time" CommandName="Sort"
                                        CommandArgument="[I].[StartDateTime]" Text="Date & Time" />
                                </th>
                                <th>
                                    <asp:LinkButton ID="btnRequisition" runat="server" ToolTip="Sort By Requisition"
                                        CommandName="Sort" CommandArgument="[JP].[JobTitle]" Text="Requisition" />
                                </th>
                                <th>
                                    <asp:LinkButton ID="btnName" runat="server" ToolTip="Sort By Candidate Name" CommandName="Sort"
                                        CommandArgument="[M].[FirstName]" Text="Candidate Name" />
                                </th>
                                <%--   <th style="width: 25%;">
                                    <asp:LinkButton ID="btnType" runat="server" ToolTip="Sort By Interview Type" CommandName="Sort"
                                        CommandArgument="[GL].[Name]" Text="Interview Type" />
                                </th>
                                <th style="width: 25%;">
                                    <asp:LinkButton ID="btnLocation" runat="server" ToolTip="Sort By Location" CommandName="Sort"
                                        CommandArgument="[I].[Location]" Text="Location" />
                                </th>
                                <th style="width: 25%;">
                                    <asp:LinkButton ID="btnClient" runat="server" ToolTip="Sort By Client" CommandName="Sort"
                                        CommandArgument="[C].[CompanyName]" Text="Client" />
                                </th>
                                <th style="width: 25%;">
                                    <asp:LinkButton ID="btnClientInterviewers" runat="server" ToolTip="Sort By Client Interviewers" CommandName="Sort"
                                        CommandArgument="[GIVN].[InterviewerName]" Text="Client Interviewers" />
                                </th>
                                <th style="width: 25%;">
                                    <asp:LinkButton ID="btnInternalInterviewers" runat="server" ToolTip="Sort By Internal Interviewers" CommandName="Sort"
                                        CommandArgument="[GIV].[InterviewerName]" Text="Internal Interviewers" />
                                </th>
                                <th style="width: 35%;">
                                   
                                        <asp:Label runat ="server" ID ="btnNotes" Text="Notes" ToolTip="Notes"></asp:Label>
                                </th>--%>
                            </tr>
                            <tr id="itemPlaceholder" runat="server">
                            </tr>
                             <tr class="Pager" id="trPager" runat="server" visible="false">
                                <td colspan="3"  runat="server" id="tdPager">
                                    <ucl:Pager ID="pagerControl" runat="server" EnableViewState="true" />
                                </td>
                            </tr>
                            <tr class="Pager" id="trsmlPager" runat="server" visible="false">
                                <td colspan="3"  runat="server" id="td1">
                                    <ucl:smlPager ID="smlPagerControl" runat="server" EnableViewState="true" />
                                </td>
                            </tr>
                          <%--  <tr class="Pager">
                                <td colspan="3">
                                    <ucl:Pager ID="pagerControl" runat="server" EnableViewState="true" />
                                </td>
                            </tr>--%>
                        </table>
                    </LayoutTemplate>
                    <EmptyDataTemplate>
                        <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server"
                            style="width: 100%; margin: 0px 0px;">
                            <tr>
                                <td>
                                    No upcoming interviews.
                                </td>
                            </tr>
                        </table>
                    </EmptyDataTemplate>
                    <ItemTemplate>
                        <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                            <td>
                                <asp:Label ID="lblDateTime" runat="server" Width="110px" />
                            </td>
                            <td>
                                <asp:HyperLink ID="lnkRequisition" runat="server"  Width="100px"></asp:HyperLink>
                            </td>
                            <td style="white-space: normal;">
                                <asp:HyperLink ID="lnkCandidateName"  runat="server" Width="110px"></asp:HyperLink>
                            </td>
                            <%-- <td>
                                <asp:Label ID="lblInterviewType" runat="server" />
                            </td>
                            <td>
                                <asp:Label ID="lblLocation" runat="server" />
                            </td>
                            <td>
                                <asp:HyperLink ID ="lnkClient" runat ="server" Target ="_blank" ></asp:HyperLink>
                            </td>
                            <td>
                                <asp:Label ID="lblClientInterviewers" runat="server" />
                            </td>
                             <td>
                                <asp:Label ID="lblInternalInterviewers" runat="server" />
                            </td>
                             <td>
                                <asp:Label ID="lblNotes" runat="server" />
                            </td>--%>
                        </tr>
                    </ItemTemplate>
                </asp:ListView>
            </ContentTemplate>
         </asp:UpdatePanel>
        </asp:Panel>

