using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

public partial class WidgetContainer : System.Web.UI.UserControl, IWidgetHost
{
    public event Action<WidgetInstance> Deleted;
    private WidgetInstance _WidgetInstance;

    public WidgetInstance WidgetInstance
    {
        get { return _WidgetInstance; }
        set { _WidgetInstance = value; }
    }

    public bool SettingsOpen
    {
        get
        {
            object val = ViewState[this.ClientID + "_SettingsOpen"];
            return (bool)val;
        }
        set
        { 
            ViewState[this.ClientID + "_SettingsOpen"] = value; 
        }
    }

    private Widget _Widget;
    public Widget WidgetDef 
    {
        get { return _Widget; }
        set { _Widget = value; }
    }

    private string _userId;
    public string UserId
    {
        get { return _userId; }
        set { _userId = value; }
    }

    private bool _IsFirstLoad;

    public bool IsFirstLoad
    {
        get { return _IsFirstLoad; }
        set { _IsFirstLoad = value; }
    }

    private IWidget _WidgetRef;

    protected void Page_Load(object sender, EventArgs e)
    {
        WidgetTitle.Text = this.WidgetInstance.Title;
        this.SetExpandCollapseButtons();
        this.CloseWidget.OnClientClick = "DeleteWarning.show( function() { DeleteWidget('" + this.WidgetInstance.Id + "')}, Function.emptyFunction ); return false; ";
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        var widget = LoadControl(this.WidgetInstance.Widget.Url);
        widget.ID = "Widget" + this.WidgetInstance.Id.ToString();
        
        WidgetBodyPanel.Controls.Add(widget);
        this._WidgetRef = widget as IWidget;
        this._WidgetRef.Init(this);
    }

    private void SetExpandCollapseButtons()
    {
        if (!this.WidgetInstance.Expanded)
        {
            ExpandWidget.Visible = true;
            CollapseWidget.Visible = false;
            pnlBody.Visible = false;
            WidgetBodyPanel.Visible = false;
        }
        else
        {
            ExpandWidget.Visible = false;
            CollapseWidget.Visible = true;
            pnlBody.Visible = true;
            WidgetBodyPanel.Visible = true;
        }
    }

    protected void EditWidget_Click(object sender, EventArgs e)
    {
        if (this.SettingsOpen)
        {
            this.SettingsOpen = false;
            this._WidgetRef.HideSettings();
            EditWidget.Visible = true;
            CancelEditWidget.Visible = false;
        }
        else
        {
            this.SettingsOpen = true;
            this._WidgetRef.ShowSettings();
            (this as IWidgetHost).Maximize();
            EditWidget.Visible = false;
            CancelEditWidget.Visible = true;
        }

        WidgetBodyUpdatePanel.Update();
    }

    protected void CollapseWidget_Click(object sender, EventArgs e)
    {
        (this as IWidgetHost).Minimize();
    }

    protected void ExpandWidget_Click(object sender, EventArgs e)
    {
        (this as IWidgetHost).Maximize();
    }

    protected void CloseWidget_Click(object sender, EventArgs e)
    {
        this._WidgetRef.Closed();
        (this as IWidgetHost).Close();
    }

    protected void SaveWidgetTitle_Click(object sender, EventArgs e)
    {
        
    }

    protected void WidgetTitle_Click(object sender, EventArgs e)
    {
        WidgetTitleTextBox.Text = this.WidgetInstance.Title;
        WidgetTitleTextBox.Visible = true;
        SaveWidgetTitle.Visible = true;
        WidgetTitle.Visible = false;
    }

    protected void CancelEditWidget_Click(object sender, EventArgs e)
    {

    }

    public override void RenderControl(HtmlTextWriter writer)
    {
        writer.AddAttribute("InstanceId", this.WidgetInstance.Id.ToString());
        base.RenderControl(writer);
    }

    #region Implemented Methods

    int IWidgetHost.ID
    {
        get
        {
            return this.WidgetInstance.Id;
        }
    }

    void IWidgetHost.Maximize()
    {
        //update expand field of widget instance table
        new WidgetInstanceDataAccess().UpdateWidgetInstanceExpanded(true, this.WidgetInstance.Id);
        WidgetInstance.Expanded = true;
        this.SetExpandCollapseButtons();
        this._WidgetRef.Maximized();

        WidgetBodyUpdatePanel.Update();
        WidgetHeaderUpdatePanel.Update();
    }

    void IWidgetHost.Minimize()
    {
        //update expand field of widget instance table
        new WidgetInstanceDataAccess().UpdateWidgetInstanceExpanded(false, this.WidgetInstance.Id);
        WidgetInstance.Expanded = false;
        this.SetExpandCollapseButtons();
        this._WidgetRef.Minimized();

        WidgetBodyUpdatePanel.Update();
        WidgetHeaderUpdatePanel.Update();
    }

    void IWidgetHost.SaveState(string state)
    {
        new WidgetInstanceDataAccess().UpdateWidgetInstanceState(state, this.WidgetInstance.Id);

        // Invalidate cache because widget's state is stored in cache
        //Cache.Remove(UserId);
    }

    void IWidgetHost.Close()
    {
        Deleted(this.WidgetInstance);
    }

    string IWidgetHost.GetState()
    {
        return this.WidgetInstance.State;
    }

    bool IWidgetHost.IsFirstLoad
    {
        get
        {
            return this.IsFirstLoad;
        }
    }

    #endregion
}

