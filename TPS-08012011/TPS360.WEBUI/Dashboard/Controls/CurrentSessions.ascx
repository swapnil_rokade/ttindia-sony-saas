﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CurrentSessions.ascx.cs"
    Inherits="CurrentSessions" %>
<%@ Register Src="~/Controls/SmallPagerControl.ascx" TagName="Pagers" TagPrefix="ucll" %>
<asp:Panel ID="pnlSettings" runat="server">
</asp:Panel>
<asp:TextBox ID="txtSortColumn" runat="server" Visible="false" />
<asp:TextBox ID="txtSortOrder" runat="server" Visible="false" />
<asp:Panel ID="widgetBody" runat="server">
    <asp:UpdatePanel ID="upcurrentSession" runat="server">
        <ContentTemplate>
            <asp:ObjectDataSource ID="odsCurrentSessions" runat="server" SelectMethod="GetPaged"
                TypeName="TPS360.Web.UI.CurrentSessionsDataSource" SelectCountMethod="GetListCount"
                EnablePaging="True" SortParameterName="sortExpression" EnableViewState="true">
                <SelectParameters>
                    <asp:Parameter Name="SortOrder" DefaultValue="" />
                </SelectParameters>
            </asp:ObjectDataSource>
            <asp:ListView ID="lsvCurrentSessions" runat="server" DataKeyNames="Id" DataSourceID="odsCurrentSessions"
                OnItemDataBound="lsvCurrentSessions_ItemDataBound" EnableViewState="true" OnPreRender="lsvCurrentSessions_PreRender"
                OnItemCommand="lsvCurrentSessions_ItemCommand">
                <LayoutTemplate>
                    <table id="tlbTemplate" runat="server" class="Grid" cellspacing="0" border="0">
                        <tr>
                            <th style="width: 20%;" enableviewstate="false">
                                <asp:LinkButton ID="btnEmployeeName" runat="server" ToolTip="Sort By Employee Name"
                                    CommandName="Sort" CommandArgument="[M].[FirstName]" Text="Employee" />
                            </th>
                            <th style="width: 20%;" enableviewstate="false">
                                <asp:LinkButton ID="btnSessionType" runat="server" ToolTip="Sort By Session Type"
                                    CommandName="Sort" CommandArgument="[AAD].[AppName]" Text="Session Type" />
                            </th>
                            <th style="width: 20%;" enableviewstate="false">
                                <asp:LinkButton ID="btnLoggedIn" runat="server" ToolTip="Sort By Logged In" CommandName="Sort"
                                    CommandArgument="[AAD].[CreatedDate]" Text="Logged In" />
                            </th>
                        </tr>
                        <tr id="itemPlaceholder" runat="server">
                        </tr>
                        <tr class="Pager">
                            <td colspan="3">
                                <ucll:Pagers ID="pagerControl" runat="server" />
                            </td>
                        </tr>
                    </table>
                </LayoutTemplate>
                <EmptyDataTemplate>
                    <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server"
                        style="width: 100%; margin: 0px 0px;">
                        <tr>
                            <td>
                                No Sessions.
                            </td>
                        </tr>
                    </table>
                </EmptyDataTemplate>
                <ItemTemplate>
                    <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                        <td>
                            <asp:HyperLink ID="lnkEmployeeName" runat="server" ></asp:HyperLink>
                        </td>
                        <td>
                            <asp:Label ID="lblSessionType" runat="server"  />
                        </td>
                        <td>
                            <asp:Label ID="lblLoggedIn" runat="server" />
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:ListView>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Panel>
