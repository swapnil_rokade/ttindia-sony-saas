﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Web.UI.Helper;
using TPS360.Web.UI;
using System.Collections;

public partial class AllVendorSubmissions : TPS360.Web.UI.BaseControl, IWidget
{
    #region Member Variables
    private bool _IsAccessToCandidate = true;
    private static string UrlForCandidate = string.Empty;
    private static int SitemapIdForCandidate = 0;

    public bool HideDatePicker
    {
        set
        {
            dtSubmissionDate.Visible = false;

            imgSearch.Visible = false;
            dtSubmissionDate.ClearRange();
        }
    }
    public int ContactId
    {
        set
        {
            odsVendorSubmissions.SelectParameters["ContactId"].DefaultValue = value .ToString ();
        }
    }

    #endregion

    #region Properties

    private IWidgetHost _Host;

    public IWidgetHost Host
    {
        get { return _Host; }
        set { _Host = value; }
    }

    private string _userId;
    public string UserId
    {
        get { return _userId; }
        set { _userId = value; }
    }

    #endregion
    #region Page Events
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {

            dtSubmissionDate.setDateRange(DateTime.Now.AddDays(-6), DateTime.Now);
        }
      }

    protected void lsvVendorSubmissions_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
        {

            VendorSubmissions vendorSubmission = ((ListViewDataItem)e.Item).DataItem as VendorSubmissions;
            Label lblDateTime = (Label)e.Item.FindControl("lblDateTime");
            HyperLink hlnkCandidateName = (HyperLink)e.Item.FindControl("hlnkCandidateName");
            HyperLink lnkJobTitle = (HyperLink)e.Item.FindControl("lnkJobTitle");
            HyperLink hnlkVendor = (HyperLink)e.Item.FindControl("hnlkVendor");
            LinkButton hlnkContact = (LinkButton)e.Item.FindControl("hlnkContact");
            Label lblStatus = (Label)e.Item.FindControl("lblStatus");
            lblDateTime.Text = vendorSubmission.SubmittedDate.ToShortDateString();
            hlnkCandidateName.Text = vendorSubmission.CandidateName;
            ControlHelper.SetHyperLink(hlnkCandidateName, UrlConstants.Candidate.OVERVIEW, string.Empty, vendorSubmission.CandidateName, UrlConstants.PARAM_MEMBER_ID, vendorSubmission.CandidateId.ToString(), UrlConstants.PARAM_SITEMAP_ID, UrlConstants.Candidate.VENDOR_CANDIDATE_OVERVIEW_SITEMAP_ID, UrlConstants.PARAM_SITEMAP_PARENT_ID, "653");
            lnkJobTitle.Attributes.Add("onclick", "EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/JobDetail.aspx?JID=" +  vendorSubmission .JobPostingID  + "&FromPage=JobDetail" + "','700px','570px'); return false;");
            lnkJobTitle.Text = vendorSubmission.JobTitle;
            hnlkVendor.Text = vendorSubmission.VendorName;
            hlnkContact.Text = vendorSubmission.ContactName;
            lblStatus.Text = vendorSubmission.CurrentStatus;
            SecureUrl Modalurl = UrlHelper.BuildSecureUrl("../Modals/CompanyContact.aspx", string.Empty, UrlConstants.PARAM_CONTACT_ID, vendorSubmission.Id.ToString());
            hlnkContact.OnClientClick = "javascript:EditModal('" + Modalurl.ToString() + "','675px','450px'); return false;";

        }
    }
    private void PlaceUpDownArrow()
    {
        try
        {
            LinkButton lnk = (LinkButton)lsvVendorSubmissions.FindControl(txtSortColumn.Text);
            System.Web.UI.HtmlControls.HtmlTableCell im = (System.Web.UI.HtmlControls.HtmlTableCell)lnk.Parent;
            im.EnableViewState = false;
            im.Attributes.Add("class", (txtSortOrder.Text == "ASC" ? "Ascending" : "Descending"));
        }
        catch
        {
        }

    }
    protected void lsvVendorSubmissions_PreRender(object sender, EventArgs e)
    {
        lsvVendorSubmissions.DataBind();
        ASP.controls_smallpagercontrol_ascx PagerControl = (ASP.controls_smallpagercontrol_ascx)this.lsvVendorSubmissions.FindControl("pagerControl");
        if (PagerControl != null)
        {
            DataPager pager = (DataPager)PagerControl.FindControl("pager");
            if (pager != null)
            {
                if (pager.Controls.Count >= 1)
                {
                    DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                    if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
                }
            }
            HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
            if (hdnRowPerPageName != null) hdnRowPerPageName.Value = "DashboardRecentChangesRowPerPage";
        }
        PlaceUpDownArrow();
        if (lsvVendorSubmissions.Controls.Count == 0)
        {
            lsvVendorSubmissions.DataSource = null;
            lsvVendorSubmissions.DataBind();
        }
    }

    protected void lsvVendorSubmissions_ItemCommand(object sender, ListViewCommandEventArgs e)
    {
        try
        {
           
            if (e.CommandName == "Sort")
            {
                LinkButton lnkbutton = (LinkButton)e.CommandSource;
                if (txtSortColumn.Text == lnkbutton.ID)
                {
                    if (txtSortOrder.Text == "ASC") txtSortOrder.Text = "DESC";
                    else txtSortOrder.Text = "ASC";
                }
                else
                {
                    txtSortColumn.Text = lnkbutton.ID;
                    txtSortOrder.Text = "ASC";
                }

            }
        }
        catch
        {
        }
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
    }
    #endregion

    #region Implementation

    void IWidget.Init(IWidgetHost host)
    {
        this.Host = host;
    }

    void IWidget.ShowSettings()
    {
        pnlSettings.Visible = true;
    }
    void IWidget.HideSettings()
    {
        pnlSettings.Visible = false;
    }
    void IWidget.Minimized()
    {
    }
    void IWidget.Maximized()
    {
       
      
    }
    void IWidget.Closed()
    {
    }

    #endregion
}
