﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Web.UI.Helper;
using TPS360.Web.UI;
using System.Collections;

public partial class EmployeeReferralnotification:TPS360.Web.UI.BaseControl, IWidget
{
    #region Member Variables
    bool _IsAccessToCandidate = true;
    private string UrlForCandidate = string.Empty;
    private int SitemapIdForCandidate = 0;
    #endregion

    #region Properties

    private IWidgetHost _Host;

    public IWidgetHost Host
    {
        get { return _Host; }
        set { _Host = value; }
    }

    private string _userId;
    public string UserId
    {
        get { return _userId; }
        set { _userId = value; }
    }

    #endregion
    #region Page Events
    protected void Page_Load(object sender, EventArgs e)
    {
        CustomSiteMap CustomMap = new CustomSiteMap();
        CustomMap = Facade.GetAllCustomSiteMapByParentIdAndMemberPrivilege(358, CurrentMember.Id);
        if (CustomMap == null) _IsAccessToCandidate = false;
        else
        {
            SitemapIdForCandidate = CustomMap.Id;
            UrlForCandidate = "~/" + CustomMap.Url.ToString();
        }
        if (!IsPostBack)
        {

            dtRange.setDateRange(DateTime.Now.AddDays(-6), DateTime.Now);
            hdnSortColumn.Text = "btnDate";
            hdnSortOrder.Text = "Desc";
        }
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
    }
    #endregion

    #region Implementation

    void IWidget.Init(IWidgetHost host)
    {
        this.Host = host;
    }

    void IWidget.ShowSettings()
    {
        pnlSettings.Visible = true;
    }
    void IWidget.HideSettings()
    {
        pnlSettings.Visible = false;
    }
    void IWidget.Minimized()
    {
    }
    void IWidget.Maximized()
    {
       
      
    }
    void IWidget.Closed()
    {
    }

    #endregion

    protected void lsvReferralDetail_ItemDataBound(object sender, ListViewItemEventArgs e)
    {

        if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
        {
            EmployeeReferral referral = ((ListViewDataItem)e.Item).DataItem as EmployeeReferral;

            if (referral != null)
            {
                Label lblDate = (Label)e.Item.FindControl("lblDate");
                Label lblReferrer = (Label)e.Item.FindControl("lblReferrer");
                HyperLink hlnkJobTitle = (HyperLink)e.Item.FindControl("hlnkJobTitle");
                HyperLink hlnkCandidate = (HyperLink)e.Item.FindControl("hlnkCandidate");
                Label lblStatus = (Label)e.Item.FindControl("lblStatus");
                lblDate.Text = referral.CreateDate.ToShortDateString();
                lblReferrer.Text = referral.RefererFirstName + " " + referral.RefererLastName + "(" + referral.RefererEmail + ")"; 
                hlnkJobTitle.Text = referral.JobTitle;
                hlnkJobTitle.Attributes.Add("onclick", "EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/JobDetail.aspx?JID=" + referral.JobpostingId + "&FromPage=JobDetail" + "','700px','570px'); return false;");
                hlnkCandidate.Text = referral.CandidateName;
             
                if (_IsAccessToCandidate)
                {
                    if (SitemapIdForCandidate > 0 && UrlForCandidate != string.Empty)
                        ControlHelper.SetHyperLink(hlnkCandidate, UrlForCandidate, string.Empty, referral.CandidateName, UrlConstants.PARAM_MEMBER_ID, StringHelper.Convert(referral.MemberId), UrlConstants.PARAM_SITEMAP_ID, SitemapIdForCandidate.ToString());
                }

                lblStatus.Text = Facade.GetCandidateStatusByMemberIdAndJobPostingId(referral.MemberId, Convert.ToInt32(referral.JobpostingId));
            }
        }
    }

    protected void lsvReferralDetail_PreRender(object sender, EventArgs e)
    {
        lsvReferralDetail.DataBind();
        ASP.controls_smallpagercontrol_ascx PagerControl = (ASP.controls_smallpagercontrol_ascx)this.lsvReferralDetail .FindControl("pagerControl");
        if (PagerControl != null)
        {
            DataPager pager = (DataPager)PagerControl.FindControl("pager");
            if (pager != null)
            {
                DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
            }
            HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
            if (hdnRowPerPageName != null) hdnRowPerPageName.Value = "DashboardReferralRowPerPage";
        }
        PlaceUpDownArrow();

        if (lsvReferralDetail != null)
        {
            if (lsvReferralDetail.Items.Count == 0)
            {
                lsvReferralDetail.DataSource = null;
                lsvReferralDetail.DataBind();
            }
        }
    }
    private void PlaceUpDownArrow()
    {
        try
        {

            LinkButton lnk = (LinkButton)lsvReferralDetail.FindControl(hdnSortColumn.Text );
            System.Web.UI.HtmlControls.HtmlTableCell im = (System.Web.UI.HtmlControls.HtmlTableCell)lnk.Parent;
            im.EnableViewState = false;
            im.Attributes.Add("class", ( hdnSortOrder.Text == "ASC" ? "Ascending" : "Descending"));

        }
        catch
        {
        }

    }
      
    protected void lsvReferralDetail_ItemCommand(object sender, ListViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Sort")
            {
                LinkButton lnkbutton = (LinkButton)e.CommandSource;
                if (hdnSortColumn.Text == lnkbutton.ID)
                {
                    if (hdnSortOrder.Text == "ASC") hdnSortOrder.Text = "DESC";
                    else hdnSortOrder.Text = "ASC";
                }
                else
                {
                    hdnSortColumn.Text = lnkbutton.ID;
                    hdnSortOrder.Text = "ASC";
                }
            }
        }
        catch
        { }
    }
}
