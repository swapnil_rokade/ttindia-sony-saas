﻿<%@ Page Language="C#" MasterPageFile="~/Public.master" AutoEventWireup="true" CodeFile="TermsofUse.aspx.cs"
    Inherits="TPS360.Web.UI.TermsofUse" Title="Talentrackr - Terms of Use" %>

<asp:Content ID="cDefault" ContentPlaceHolderID="cphHomeMaster" runat="Server">
    <div class="CommonPageBox">
        <div class="CommonPageBoxHeader">
            Talentrackr - Terms of Use
        </div>
        <div class="CommonPageBoxContent">
            IMPORTANT: PLEASE READ THIS AGREEMENT CAREFULLY BEFORE INSTALLING THIS SOFTWARE.&nbsp;
            THIS AGREEMENT STATES THE TERMS AND CONDITIONS UPON WHICH TALENTRACKR OFFERS TO
            LICENSE THE Talentrackr ONLINE SERVICE.&nbsp; BY SELECTING THE &quot;I ACCEPT&quot; BUTTON
            BELOW, INSTALLING, OR OTHERWISE USING THIS SOFTWARE, YOU ACKNOWLEDGE THAT YOU HAVE
            READ THIS AGREEMENT, AND THAT YOU AGREE TO BE BOUND BY ITS TERMS AND CONDITIONS.&nbsp;
            IF YOU DO NOT AGREE TO ALL OF THE TERMS AND CONDITIONS OF THIS AGREEMENT, YOU MAY
            NOT INSTALL THIS SOFTWARE NOR USE THE SERVICE AND IT IS YOUR RESPONSIBILITY TO EXIT
            THIS INSTALLATION PROGRAM WITHOUT INSTALLING THE SOFTWARE BY SELECTING THE &quot;NO&quot;
            BUTTON BELOW, AND, IF INSTALLED, TO DELETE THE SOFTWARE FROM YOUR COMPUTER<br>
            <br />
            This Agreement is effective as of the original date of acceptance of this Agreement
            or installation of the Software (as defined below), whichever occurs first.&nbsp;
            The parties are Talentrackr as the licensor (collectively herein referred to as
            &quot;Talentrackr&quot;), and you as an individual or entity.<br>
            <br />
            <strong>TERMS AND CONDITIONS OF USE</strong><br />
            The Talentrackr Software Service (the &quot;Service&quot;), including the Talentrackr proprietary
            software and databases as well as software developed by third parties and distributed
            under license by Talentrackr that will enable you to use the Service (collectively the
            &quot;Software&quot;), is provided and owned by Talentrackr.&nbsp; You acknowledge and
            understand that you will use the Service in accordance with the terms and conditions
            of this Agreement.&nbsp; Your use of the Service, including any content and software
            contained therein, signifies your understanding and agreement that such use is entirely
            at your own risk.&nbsp; Talentrackr does not guarantee reliability, accuracy, or completeness
            of the Software or Service.
            <br>
            <br />
            The Software that will enable you to use the Service may be downloaded from the
            Talentrackr Web Site, another authorized web site or from a CD-ROM.&nbsp; You acknowledge
            that you made no payment for the Software, regardless of the method of delivery,
            and if payments were made, they were made for other equipment, products or services
            and not for the Software.&nbsp; To ensure the proper functionality and security
            of the Service always make sure you are using a current and authorized copy of the
            Software.
            <br>
            <br />
            Talentrackr may, at its sole discretion, limit, deny, or create different levels of use
            for different users, or cancel some or all of the functionality of the Software
            or the Service at any time, without prior notice.
            <br>
            <br />
            You agree that your use of the Services will not be in violation of any law and
            will not interfere unreasonably with others' use of the service.<br>
            <br>
            <strong>TERMS OF USE FOR SERVICES</strong><br />
            You agree that the terms of use for the Services are expressly incorporated herein
            by reference, and any breach of the terms of use for the Services shall be deemed
            a breach of this License Agreement.
            <br>
            <br />
            <strong>LICENSE</strong><br />
            For so long as you have a valid user account (&quot;USER ACCOUNT&quot;) with Talentrackr
            and are not in breach of this Agreement and/or the terms of use for the Services,
            Talentrackr hereby grants to you a non-exclusive, non-transferable license to use the
            Software with one personal computer.&nbsp;
            <br />
            You shall comply with all laws, regulations, orders or other restrictions on the
            export from the U.S. of the Software or of information related thereto which may
            be imposed from time to time by the U.S. Government.&nbsp; You shall not: (a) use
            the Software as a network-based Software, (b) relocate, transfer, assign, or sublicense
            the Software; (c) reproduce or make backup copies of any portion of the Software;
            (d) modify, adapt, translate, rent, lease, loan or distribute the Software or create
            any derivative works of the Software; (e) attempt to reverse engineer, decompile
            or otherwise determine the Software algorithms embedded within the Software;&nbsp;
            (f) use the Software for any purposes other than in connection with testing and
            reviewing the Talentrackr Software, input test transactions, or access or permit the
            access of the functionality of the Software functions; or (g) provide others with
            the Software or Service or any information available on, derived or extracted from
            the Service or any part of the foregoing.
            <br />
            You shall not remove, modify, destroy or obscure any proprietary, trademark or copyright
            markings or confidentiality legends (of Talentrackr or its suppliers) within or displayed
            by the Software or any automatically generated electronic files.<br>
            <br />
            You acknowledge and agree that all right, title and interest in and to the Software
            are and shall remain with Talentrackr or its suppliers.&nbsp; This Agreement does not
            convey to you any interest in or to the Software, but only a limited right of use,
            revocable in accordance with the terms of this Agreement.<br>
            <br />
            <strong>WARRANTIES AND DISCLAIMERS</strong><br />
            THE SERVICE IS PROVIDED &quot;AS IS&quot; AND WITHOUT WARRANTIES OF ANY KIND EITHER
            EXPRESS, IMPLIED OR STATUTORY OR ARISING FROM CUSTOM OR TRADE, INCLUDING, BUT NOT
            LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
            PURPOSE AND NON-INFRINGEMENT.&nbsp; TALENTRACKR ACCEPTS NO RESPONSIBILITY FOR ERRORS
            OR TECHNICAL DIFFICULTIES WITH THE FUNCTIONALITY OF THE SOFTWARE, INCLUDING THAT
            YOUR SERVICE WILL BE UNINTERRUPTED OR ERROR-FREE, OR THAT DEFECTS WILL BE CORRECTED.&nbsp;
            TALENTRACKR WILL NOT AND DOES NOT GUARANTEE, REPRESENT OR WARRANT THAT THE SERVICE OR
            SOFTWARE WILL BE FREE OF INFECTION OR VIRUSES, WORMS, TROJAN HORSES OR OTHER CODE
            OR DEFECTS THAT MANIFEST CONTAMINATING OR DESTRUCTIVE PROPERTIES.&nbsp; APPLICABLE
            LAW MAY NOT ALLOW THE EXCLUSION OF IMPLIED WARRANTIES, SO THE ABOVE EXCLUSION MAY
            NOT APPLY TO YOU.&nbsp; YOU ARE RESPONSIBLE FOR IMPLEMENTING SUFFICIENT FIREWALLS,
            PROTECTIONS, PROCEDURES AND CHECKPOINTS TO SATISFY YOUR PARTICULAR REQUIREMENTS
            FOR PROTECTION OF YOUR SYSTEM.<br />
            THE SERVICE, LIKE ALL SOFTWARE RELATED PRODUCTS, MAY BE SUBJECT TO KNOWN OR UNKNOWN
            ANOMALIES, WHICH MAY AFFECT ITS ABILITY TO PERFORM. &nbsp;YOU UNDERSTAND AND AGREE
            THAT TALENTRACKR IS NOT RESPONSIBLE OR LIABLE FOR ANY SUCH ANOMALIES.<br>
            <br />
            <strong>LIMITATION OF LIABILITY AND INDEMNIFICATION</strong><br />
            TALENTRACKR IS NOT LIABLE FOR ANY DAMAGES, INCLUDING LOST PROFITS, LOST SAVINGS, OR SPECIAL,
            INCIDENTAL, CONSEQUENTIAL DAMAGES, INDIRECT, OR PUNITIVE DAMAGES (INCLUDING BUT
            NOT LIMITED TO DAMAGES FOR BREACH OF CONTRACT OR WARRANTY OR FOR NEGLIGENCE OR STRICT
            LIABILITY) ARISING OUT OF OR IN CONNECTION WITH THIS AGREEMENT OR THE USE OR INABILITY
            TO USE THE SERVICE EVEN IF TALENTRACKR HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES,
            OR FOR ANY CLAIM BY ANY OTHER PARTY.&nbsp; YOU AGREE TO INDEMNIFY, DEFEND AND HOLD
            TALENTRACKR AND ITS AFFILIATES, AND THEIR RESPECTIVE OFFICERS, DIRECTORS, EMPLOYEES,
            AGENTS, INFORMATION PROVIDERS AND LICENSORS (COLLECTIVELY, THE &quot;INDEMNIFIED
            PARTIES&quot;) HARMLESS FROM AND AGAINST ANY AND ALL CLAIMS, LIABILITY, LOSSES,
            COSTS AND EXPENSES (INCLUDING ATTORNEYS' FEES) ARISING IN ANY WAY FROM YOUR USE
            OF THE SERVICE OR THE PLACEMENT OR TRANSMISSION OF ANY MESSAGE, INFORMATION, SOFTWARE
            OR OTHER MATERIALS THROUGH THE SERVICE BY YOU OR USERS AUTHORIZED BY YOU OR RELATED
            TO ANY BREACH OF THESE TERMS AND CONDITIONS BY YOU OR ANY USER AUTHORIZED BY YOU.&nbsp;
            YOUR SOLE REMEDY IS TO TERMINATE USE OF THE SERVICE.&nbsp; TALENTRACKR RESERVES THE RIGHT,
            AT ITS OWN EXPENSE, TO ASSUME THE EXCLUSIVE DEFENSE AND CONTROL OF ANY MATTER OTHERWISE
            SUBJECT TO INDEMNIFICATION BY YOU, AND IN SUCH CASE, YOU AGREE TO COOPERATE WITH
            US IN DEFENSE OF SUCH CLAIM.<br />
            In no event shall Talentrackrs' liability arising out of or in connection with this Agreement
            exceed $1.00.&nbsp; Some states do not allow the limitation or exclusion of liability
            for incidentals or consequential damages, so the above limitation or exclusion may
            not apply to you.<br>
            <br />
            <strong>TERM AND TERMINATION</strong><br />
            Either party may terminate this Agreement at any time on the same terms as set forth
            in the applicable terms of use for the Services.&nbsp; In addition to other rights
            that Talentrackr may have as provided herein, Talentrackr reserves the right to terminate
            the Agreement at any time if you do not adhere to these Terms and Conditions.&nbsp;
            In the event you terminate this Agreement you will be responsible for paying the
            remaining balance of any Service Fee.&nbsp;
            <br>
            <br>
            <strong>ASSIGNMENT</strong><br />
            You shall not transfer, assign, sublicense nor pledge in any manner whatsoever,
            any of your rights or obligations under this Agreement.&nbsp; Talentrackr may transfer,
            assign sublicense or pledge in any manner whatsoever, any of its rights and obligations
            under this agreement to a subsidiary, affiliate, or successor thereof or to any
            third party whatsoever, without notifying you or receiving your consent.<br>
            <br />
            <strong>JURISDICTION AND VENUE</strong><br />
            Any legal action arising from or out of this Agreement shall be governed by the
            laws of the State of New York without regard to its conflict of law principles.&nbsp;
            The sole jurisdiction and venue for any litigation arising from use of the Service
            shall be an appropriate federal or state court located in New York.&nbsp; The United
            Nations' Convention on Contracts for the International Sale of Goods is expressly
            disclaimed.<br>
            <br />
            <strong>INTELLECTUAL PROPERTY RIGHTS </strong>
            <br />
            You hereby acknowledge and agree that Talentrackr or its licensors own and retain all
            rights, title, and interest in and to the Service and Software, regardless of the
            form or media in or on which the original or other copies may subsequently exist
            including, without limitation, all copyrights, trademarks, patents and trade secret
            rights inherent therein or appurtenant thereto.&nbsp; Except as otherwise specifically
            provided in these terms and conditions, you may not download or save a copy of the
            Service or Software or any portion thereof, for any purpose.&nbsp; You may, however,
            print a copy of individual screens appearing as part of the Service and Software
            solely for your personal use or records.&nbsp; This Agreement shall not constitute
            a sale of the Software or Services and no title or proprietary rights to the Software
            are transferred to you hereby.
            <br />
            You also acknowledge that the Software is a unique, confidential and valuable asset
            and trade secret of Talentrackr or its licensors, and Talentrackr or its licensors shall have
            the right to obtain all equitable and legal redress which may be available to it
            for the breach or threatened breach of this Agreement including, without limitation,
            injunctive relief.<br>
            <br />
            <strong>US GOVERNMENT RESTRICTED RIGHTS</strong><br />
            The Software and any documentation thereof are provided with RESTRICTED RIGHTS.&nbsp;
            Use, duplication, or disclosure by the Government is subject to restrictions as
            set forth in subparagraph (c)(1)(ii) of the Rights in Technical Data and Computer
            Software clause at DFARS 252,227-7013 or subparagraphs (c)(1) and (2) of the Commercial
            Computer Software-Restricted Rights at 48 CFR 52.227-19, as applicable.&nbsp;
            <br>
            <br />
            <strong>CUSTOMER CONTACT</strong><br />
            Any questions concerning this Agreement should be directed to support@Talentrackr.com.<br>
            <br />
            <strong>GENERAL</strong><br />
            For U.S. Government end users, the Software is a &quot;commercial item&quot;, consisting
            of &quot;commercial computer software&quot; and &quot;commercial computer software
            documentation&quot;, and is provided to the U.S. Government only as a commercial
            end item.
            <br />
            Talentrackr reserves the right to discontinue service upon notification to its customers.
            <br>
            <br />
            Talentrackr's suppliers are third party beneficiaries of this Agreement.
            <br>
            <br />
            If any court of competent jurisdiction finds any provision of this Agreement void
            or unenforceable, then the validity of remaining provisions of this Agreement shall
            not be affected.<br>
            <br />
            You acknowledge having read this Agreement and understanding it, and agree to be
            bound by its terms and conditions.&nbsp; You further agree that this Agreement is
            the entire agreement between Talentrackr and you with respect to the Software license
            and supersedes all prior agreements (whether written or oral) and other communications
            between Talentrackr and you with respect to such license.&nbsp; It may be changed only
            in a writing executed by an authorized Talentrackr representative.
        </div>
    </div><br />
</asp:Content>
