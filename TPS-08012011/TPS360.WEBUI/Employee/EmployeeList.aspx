﻿<%@ Page Language="C#" MasterPageFile="~/Default.master" AutoEventWireup="true" CodeFile="EmployeeList.aspx.cs"
    Inherits="TPS360.Web.UI.EmployeeList" Title="Master User List" %>

<%@ Register Src="~/Controls/EmployeeList.ascx" TagName="EmployeeList" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="cntMemberEmployeeListHeader" ContentPlaceHolderID="cphHomeMasterTitle"
    runat="Server">
Master User List
</asp:Content>
<asp:Content ID="cntEmployeeListBody" ContentPlaceHolderID="cphHomeMaster"
    runat="Server">
    <div style="text-align: left; width: 100%;">
        <asp:HiddenField ID="hfMemberId" runat="server" Value="0"/>
        <uc1:EmployeeList ID="ucntrlEmployeeList" IsMylist="false" IsTier="false" IsMapping="false" HeaderTitle="List of Employees" runat="server" />
    </div>
</asp:Content>