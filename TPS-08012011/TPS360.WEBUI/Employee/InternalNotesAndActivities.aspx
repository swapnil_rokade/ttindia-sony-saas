﻿<%@ Page Language="C#" MasterPageFile="~/Masters/EmployeeProfile.master" AutoEventWireup="true"
    CodeFile="InternalNotesAndActivities.aspx.cs" Inherits="TPS360.Web.UI.EmployeeInternalNotesAndActivities"
    Title="Employee Notes And Activities" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<%@ Register Src="~/Controls/NotesAndActivitiesEditor.ascx" TagName="NotsActivities"
    TagPrefix="uc1" %>
<asp:Content ID="cntEmployeeInternalNotesActivities" ContentPlaceHolderID="cphEmployeeMaster"
    runat="Server">

    <script src="../Scripts/jsUpdateProgress.js" type="text/javascript"></script>

    <script language="javascript" type="text/javascript">

    </script>

    <asp:UpdatePanel ID="pnlNotes" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <uc1:NotsActivities ID="ucntrlNotsActivities" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
