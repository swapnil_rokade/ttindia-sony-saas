﻿<%@ Page Language="C#" MasterPageFile ="~/Masters/MemberPortal.master" AutoEventWireup="true" CodeFile="AllEmailHistory.aspx.cs" Inherits="TPS360.Web.UI.EmployeeEmailHistory" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src ="~/Controls/AllEmailList.ascx" TagName ="EmailAll" TagPrefix ="uc1" %>
<asp:Content ID="cntAllEmailHistoryHeader" ContentPlaceHolderID="head"
    runat="Server">
</asp:Content>
<asp:Content ID="cntAllEmailHistory" ContentPlaceHolderID="cphHomeMaster"
    runat="Server">
    <script src ="../Scripts/jsUpdateProgress.js" type ="text/javascript" ></script>
<script language="javascript" type="text/javascript">
var ModalProgress ='<%= Modal.ClientID %>';

</script>
       <div class="TabPanelHeader" >
        All Emails
    </div>  
        <asp:updatepanel ID="pnlAllEmailListTab" runat="server" UpdateMode="Conditional">
        <contenttemplate>
         <uc1:EmailAll ID ="ucAllEmailHistory" runat ="server"  />
        </contenttemplate>
        </asp:updatepanel>
              
    <asp:Panel ID="pnlmodal" runat="server" style="display: none">
                    <asp:UpdatePanel runat="server" ID="UpdatePanel2" RenderMode="Inline" UpdateMode="Conditional">
                        <ContentTemplate>
                        <img src="../Images/AjaxLoading.gif" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
            </asp:Panel>
            <ajaxToolkit:ModalPopupExtender ID ="Modal" runat ="server" TargetControlID ="pnlmodal" PopupControlID ="pnlmodal"
             BackgroundCssClass ="divModalBackground" ></ajaxToolkit:ModalPopupExtender>
</asp:Content>
