﻿<%@ Page Language="C#" MasterPageFile="~/Masters/MemberPortal.master" AutoEventWireup="true"
    CodeFile="ChangeSessionTimeout.aspx.cs" Inherits="TPS360.Web.UI.ChangeSessionTimeout"
    Title="Change Session TimeOut" %>

<asp:Content ID="cntSessiontimeOut" ContentPlaceHolderID="cphHomeMaster" runat="Server">

    <script language="javascript" type="text/javascript">
function CheckBoxCheck()
{

    var chkbox=document.getElementById('<%=chkSessionTime.ClientID %>');
    var drpdown=document.getElementById('<%=drpTimeoutValueList.ClientID %>');
    drpdown.disabled=!chkbox.checked;
    
}
    </script>

    <div class="TabPanelHeader">
        Session Timeout
    </div>
    <asp:Label ID="lblMessage" runat="server" EnableViewState="false"></asp:Label>
    <div style="text-align: left; width: 100%; padding-top: 50px;">
        <div class="TableRow">
            <div class="TableFormLeble" style="padding-left: 30px">
                <asp:CheckBox ID="chkSessionTime" runat="server" Text="Enable Session Timeout" Font-Bold="true"
                    onclick="javascript:CheckBoxCheck()"></asp:CheckBox>
            </div>
            <div class="TableFormContent">
            </div>
        </div>
        <div class="TableRow">
            <div class="TableFormLeble">
                <asp:Label EnableViewState="false" ID="lblsessiontimeout" runat="server" Text="Log out after"></asp:Label>:
            </div>
            <div class="TableFormContent">
                <asp:DropDownList ID="drpTimeoutValueList" runat="server" CssClass="CommonDropDown"
                    Width="100px">
                </asp:DropDownList>
                minutes
            </div>
        </div>
    </div>
    <br />
    <div class="TableRow">
        <div class="TableFormLeble">
        </div>
        <div class="TableFormContent">
            <asp:Button ID="btnSave" Text="Save" runat="server" CssClass="CommonButton" OnClick="btnSave_Click" />
        </div>
    </div>
</asp:Content>
