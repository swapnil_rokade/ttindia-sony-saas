﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: Overview.aspx.cs
    Description:  
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    --------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 *  0.1             May-15-2009           Sandeesh             Defect id:10440 :Changes made to get the Requisition status from database instead of getting from the Enum
 *  0.2             Apr-15-2010           Ganapati Bhat        Defect id:12540 :Changes made in lsvApplicantList_ItemDataBound()
 *  0.3             Apr-21-2010          Basavaraj A           Defect Id: 12603 , Changes are made to display the "description" of the Requisition.
 *  0.4             Apr-21-2010          Basavaraj A           Defect Id:12574 , Changes have been made to display applicant 'remarks'
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------       

--%>*/
using System;
using System.Collections;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using TPS360.Common.BusinessEntities;
using TPS360.Web.UI.Helper;
using TPS360.Common;
using TPS360.Common.Shared;
using TPS360.Common.Helper;
using System.Web.Security;
namespace TPS360.Web.UI
{
    public partial class EmployeeOverview : EmployeeBasePage
    {
        #region Member Variables
        private bool _IsAccessToEmployee = true;
        private static  string UrlForEmployee = string.Empty;
        private static  int IdForSitemap = 0;
        private int memberId = 0;

        #endregion

        #region Properties

        #endregion

        #region Events

        #region Page Events

        protected void Page_Load(object sender, EventArgs e)
        {
            CustomSiteMap CustomMap = new CustomSiteMap();
            CustomMap = Facade.GetAllCustomSiteMapByParentIdAndMemberPrivilege(358, CurrentMember.Id);
            if (CustomMap == null) _IsAccessToEmployee = false;
            else
            {
                IdForSitemap = CustomMap.Id;
                UrlForEmployee = "~/" + CustomMap.Url.ToString();
            }
            if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]))
            {
                memberId = Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]);
            }
            if (memberId > 0)
            {
                string name = MiscUtil.GetMemberNameById(memberId, Facade);
                this.Page.Title =MiscUtil .RemoveScript ( name) + " - " + "Overview";
            }
            //Requisition
            string pagesize1 = "", pagesize2 = "", pagesize3 = "", pagesize4="";
            pagesize1 = (Request.Cookies["EmpOverviewRequisitionRowPerPage"] == null ? "" : Request.Cookies["EmpOverviewRequisitionRowPerPage"].Value);
            ASP.controls_pagercontrol_ascx PagerControl_Requistion = (ASP.controls_pagercontrol_ascx)this.lsvRequisition.FindControl("pagerControl_Requisition");
            if (PagerControl_Requistion != null)
            {
                DataPager pager = (DataPager)PagerControl_Requistion.FindControl("pager");
                if (pager != null) pager.PageSize = Convert.ToInt32(pagesize1 == "" ? "20" : pagesize1);
            }
            //Candidate
            pagesize2 = (Request.Cookies["EmpOverviewCandidateRowPerPage"] == null ? "" : Request.Cookies["EmpOverviewCandidateRowPerPage"].Value);
            ASP.controls_pagercontrol_ascx PagerControl_Candiadate = (ASP.controls_pagercontrol_ascx)this.lsvApplicantList.FindControl("pagerControl_Candidate");
            if (PagerControl_Candiadate != null)
            {
                DataPager pager = (DataPager)PagerControl_Candiadate.FindControl("pager");
                if (pager != null) pager.PageSize = Convert.ToInt32(pagesize2 == "" ? "20" : pagesize2);
            }

            //Manager
            pagesize3 = (Request.Cookies["EmpOverviewManagerRowPerPage"] == null ? "" : Request.Cookies["EmpOverviewManagerRowPerPage"].Value);
            ASP.controls_pagercontrol_ascx PagerControl_Manager = (ASP.controls_pagercontrol_ascx)this.lsvAssignedManager.FindControl("pagerControl_Manager");
            if (PagerControl_Manager != null)
            {
                DataPager pager = (DataPager)PagerControl_Manager.FindControl("pager");
                if (pager != null) pager.PageSize = Convert.ToInt32(pagesize3 == "" ? "20" : pagesize3);
            }
            //Document
            pagesize4 = (Request.Cookies["EmpOverviewDocumentRowPerPage"] == null ? "" : Request.Cookies["EmpOverviewDocumentRowPerPage"].Value);
            ASP.controls_pagercontrol_ascx  PagerControl_Document = (ASP.controls_pagercontrol_ascx )this.lsvDocuments.FindControl("pagerControl_Document");
            if (PagerControl_Document != null)
            {
                DataPager pager = (DataPager)PagerControl_Document.FindControl("pager");
                if (pager != null) pager.PageSize = Convert.ToInt32(pagesize4 == "" ? "20" : pagesize4);
            }
        }

        #endregion

        #region ListView Events

        protected void lsvRequisition_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            
            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                JobPosting jobPosting = ((ListViewDataItem)e.Item).DataItem as JobPosting;

                if (jobPosting != null)
                {
                    HyperLink lblJobTitle = (HyperLink)e.Item.FindControl("lblJobTitle");
                    Label lblJobCode = (Label)e.Item.FindControl("lblJobCode");
                    Label lblJobstatus = (Label)e.Item.FindControl("lblJobstatus");
                    Label lblJobPostedDate = (Label)e.Item.FindControl("lblJobPostedDate");
                    
                    lblJobTitle.Attributes.Add("onclick", "EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/JobDetail.aspx?JID=" + jobPosting.Id + "&FromPage=JobDetail" + "','700px','570px'); return false;");
                    lblJobTitle.Text = jobPosting.JobTitle;
                    lblJobCode.Text = jobPosting.JobPostingCode;
                    lblJobPostedDate.Text = jobPosting.PostedDate.ToShortDateString();
                    GenericLookup _RequisitionStatusLookup = Facade.GetGenericLookupById(jobPosting.JobStatus);
                    if (_RequisitionStatusLookup != null)
                    {
                        lblJobstatus.Text = _RequisitionStatusLookup.Name;
                    }
                }
               
            }
        }

        protected void lsvApplicantList_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
           
            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                Candidate candidate = ((ListViewDataItem)e.Item).DataItem as Candidate;

                if (candidate != null)
                {
                    HyperLink lblApplicantName = (HyperLink)e.Item.FindControl("lblApplicantName");
                    Label lblApplicantPosition = (Label)e.Item.FindControl("lblApplicantPosition");
                    Label lblApplicantLocation = (Label)e.Item.FindControl("lblApplicantLocation");

                    string strFullName = MiscUtil.GetMemberNameById(candidate.Id, Facade);
                    if (strFullName.Trim() == "")
                        strFullName = "No Candidate Name";
                    if (_IsAccessToEmployee)
                    {
                        if (IdForSitemap > 0 && UrlForEmployee != string.Empty)
                            ControlHelper.SetHyperLink(lblApplicantName, UrlForEmployee, string.Empty, strFullName, UrlConstants.PARAM_MEMBER_ID, StringHelper.Convert(candidate .Id ), UrlConstants.PARAM_SITEMAP_ID, IdForSitemap.ToString());
                    }
                    else lblApplicantName.Text = strFullName;

                    lblApplicantPosition.Text = candidate.CurrentPosition;
                    string locaiton = candidate.CurrentCity;
                    string state = string .Empty ;
                    if (candidate.CurrentStateId > 0) state = MiscUtil.GetStateNameById(candidate.CurrentStateId,Facade );
                    if (state != string .Empty ) locaiton += (locaiton.Trim() != string.Empty ? "" : ", ") + state;
                    lblApplicantLocation.Text = locaiton;

                
                }
             
            }
        }

        protected void lsvAssignedManager_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                try
                {
                    Member member = ((ListViewDataItem)e.Item).DataItem as Member;
                    Label lblAssignedManagerName = (Label)e.Item.FindControl("lblAssignedManagerName");
                    Label lblAssignedManagerAccessRole = (Label)e.Item.FindControl("lblAssignedManagerAccessRole");
                    lblAssignedManagerName.Text = MiscUtil.GetFullName(member.FirstName, member.MiddleName, member.LastName);
                    lblAssignedManagerAccessRole.Text = Facade.GetCustomRoleNameByMemberId(member.Id);
                }
                catch
                {
                }
            }
        }
        protected void lsvDocuments_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                MemberDocument memberDocument = ((ListViewDataItem)e.Item).DataItem as MemberDocument;
                Label lblDocTitle = (Label)e.Item.FindControl("lblDocTitle");
                Label lblDocType = (Label)e.Item.FindControl("lblDocType");
                HyperLink hlnkDocFileName = (HyperLink)e.Item.FindControl("hlnkDocFileName");
                lblDocTitle.Text  = memberDocument.Title;
                GenericLookup lookup = null;
                if (memberDocument.FileTypeLookupId > 0)
                    lookup = Facade.GetGenericLookupById(memberDocument.FileTypeLookupId);
                if (lookup != null)
                    lblDocType.Text = lookup.Name;
                hlnkDocFileName.Text = memberDocument.FileName;
                string strFilePath = UrlConstants.ApplicationBaseUrl + @"Resources/Member/" + memberId.ToString() + "/" + lblDocType.Text + @"/" + memberDocument.FileName; //MiscUtil.GetMemberDocumentPath(this.Page, MemberID, memberdocument .FileName , lblType .Text , false);
                hlnkDocFileName.NavigateUrl = strFilePath;
            }
        }

        protected void odsDocumentList_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters["MemberID"] = memberId.ToString();
        }

        protected void odsMyRequisitionList_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters["memberId"] = memberId.ToString();
        }

        protected void odsCandidateList_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters["memberId"] = memberId.ToString();
        }
          protected void OdsassignedManager_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters["memberId"] = memberId.ToString();
        }

          protected void odsMyRequisitionList_Selected(object sender, ObjectDataSourceStatusEventArgs e)
          {
              string value = string.Empty;
              if (e.ReturnValue != null) value = e.ReturnValue.ToString();
              else value = "0";
             // Label lblRequisitioncount = (Label)this.Master.FindControl("ctlEmployeLP").FindControl("lblRequisitions");
            //  lblRequisitioncount.Text = value;
              //lblTotalNumberOfRequisition.Text = value;

          }
          protected void odsCandidateList_Selected(object sender, ObjectDataSourceStatusEventArgs e)
          {
              string value = string.Empty;
              if (e.ReturnValue != null) value = e.ReturnValue.ToString();
              else value = "0";
              //Label lblCandidatecount = (Label)this.Master.FindControl("ctlEmployeLP").FindControl("lblCandidates");
              //lblCandidatecount.Text = value;
              //lblTotalNumberofApplicant.Text = value;
          }
          protected void OdsManager_Selected(object sender, ObjectDataSourceStatusEventArgs e)
          {
              string value = string.Empty;
              if (e.ReturnValue != null) value = e.ReturnValue.ToString();
              else value = "0";
             // lblTotalNumberOfAssignedManager.Text = value;
          }
          protected void odsDocuments_Selected(object sender, ObjectDataSourceStatusEventArgs e)
          {
              string value = string.Empty;
              if (e.ReturnValue != null) value = e.ReturnValue.ToString();
              else value = "0";
              //lblTotalNumberOfDocuments.Text = value;
          }
          
          protected void lsvRequisition_PreRender(object sender, EventArgs e)
          {
              ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvRequisition.FindControl("pagerControl_Requisition");
              if (PagerControl != null)
              {
                  DataPager pager = (DataPager)PagerControl.FindControl("pager");
                  if (pager != null)
                  {
                      DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                      if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
                  }
                  HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
                  if (hdnRowPerPageName != null) hdnRowPerPageName.Value = "EmpOverviewRequisitionRowPerPage";
              }
              if (lsvRequisition.Items.Count == 0)
              {
                  lsvRequisition.DataSource = null;
                  lsvRequisition.DataBind();
              }
          }
          protected void lsvApplicantList_PreRender(object sender, EventArgs e)
          {
              ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvApplicantList.FindControl("pagerControl_Candidate");
              if (PagerControl != null)
              {
                  DataPager pager = (DataPager)PagerControl.FindControl("pager");
                  if (pager != null)
                  {
                      DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                      if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
                  }
                  HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
                  if (hdnRowPerPageName != null) hdnRowPerPageName.Value = "EmpOverviewCandidateRowPerPage";
              }

              if (lsvApplicantList.Items.Count == 0)
              {
                  lsvApplicantList.DataSource = null;
                  lsvApplicantList.DataBind();
              }
          }
          protected void lsvAssignedManager_PreRender(object sender, EventArgs e)
          {
              ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvAssignedManager.FindControl("pagerControl_Manager");
              if (PagerControl != null)
              {
                  DataPager pager = (DataPager)PagerControl.FindControl("pager");
                  if (pager != null)
                  {
                      DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                      if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
                  }
                  HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
                  if (hdnRowPerPageName != null) hdnRowPerPageName.Value = "EmpOverviewManagerRowPerPage";
              }
              if (lsvAssignedManager.Items.Count == 0)
              {
                  lsvAssignedManager.DataSource = null;
                  lsvAssignedManager.DataBind();
              }
          }
          protected void lsvDocuments_PreRender(object sender, EventArgs e)
          {
              ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvDocuments.FindControl("pagerControl_Documents");
              if (PagerControl != null)
              {
                  DataPager pager = (DataPager)PagerControl.FindControl("pager");
                  if (pager != null)
                  {
                      DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                      if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());

                  }
                  HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
                  if (hdnRowPerPageName != null) hdnRowPerPageName.Value = "EmpOverviewDocumentRowPerPage";

              }
              if (lsvDocuments.Items.Count == 0)
              {
                  lsvDocuments.DataSource = null;
                  lsvDocuments.DataBind();
              }
          }
        #endregion

      

        #endregion
    }
}
