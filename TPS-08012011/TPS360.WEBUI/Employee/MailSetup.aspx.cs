﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName:MailSetup.aspx.cs
    Description: 
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1             Dec-7-2009          Sandeesh         Defect id: 11591 ; Changes made to save blank SMTP credentials .
 *  0.2             30/May/2016         Prasanth         Enabled SMTP Settings
-------------------------------------------------------------------------------------------------------------------------------------------       

>*/
using TPS360.Web.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Web.UI.WebControls;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Helper;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Net.Sockets;
using System.Diagnostics;
using System.IO;
using System.Net.Security;
using AjaxControlToolkit;



namespace TPS360.Web.UI
{
    public partial class EmployeeMailSetup : EmployeeBasePage 
    {
        #region Private variable

        private static int _memberId = 0;
        MemberExtendedInformation _memberExtendedInfo;

        #endregion

        #region Properties

        private MemberExtendedInformation CurrentMemberExInfo
        {
            get
            {
                if (_memberExtendedInfo == null)
                {

                    if (_memberId > 0)
                    {
                        _memberExtendedInfo = Facade.GetMemberExtendedInformationByMemberId(_memberId);
                    }
                    else
                    {
                        _memberExtendedInfo = Facade.GetMemberExtendedInformationByMemberId(base.CurrentMember.Id);
                    }
                }

                return _memberExtendedInfo;
            }
        }
        #endregion

        #region Methods
        private byte[] GetSiteSettingTable()
        {

            Hashtable MailSettingTable = new Hashtable();
            MailSettingTable.Add(DefaultSiteSetting.SMTP.ToString(), txtSMTP.Text);
            MailSettingTable.Add(DefaultSiteSetting.SMTPUser.ToString(), txtSMTPUser.Text);
            if (txtSMTPwd.Text != "******")
                MailSettingTable.Add(DefaultSiteSetting.SMTPassword.ToString(), txtSMTPwd.Text);
            else
            {
                MemberExtendedInformation MailSetting = Facade.GetMemberExtendedInformationByMemberId(base.CurrentMember.Id);

                if (MailSetting.MailSetting != null)
                {
                    Hashtable MailSettingTables = ObjectEncrypter.Decrypt<Hashtable>(MailSetting.MailSetting);
                    if (MailSettingTables[DefaultSiteSetting.SMTPassword.ToString()] != null)
                    {
                        MailSettingTable.Add(DefaultSiteSetting.SMTPassword.ToString(), MailSettingTables[DefaultSiteSetting.SMTPassword.ToString()].ToString());
                    }
                }
            }
            MailSettingTable.Add(DefaultSiteSetting.SMTPort.ToString(), txtSMTPort.Text);
            MailSettingTable.Add(DefaultSiteSetting.SMTPEnableSSL.ToString(), chkSMTPSslEnable.Checked);
            return ObjectEncrypter.Encrypt<Hashtable>(MailSettingTable);
        }

        private void PopulateSiteSetting()
        {
            MemberExtendedInformation MailSetting = Facade.GetMemberExtendedInformationByMemberId(base.CurrentMember.Id);
            if (MailSetting != null)
            {
                if (MailSetting.MailSetting != null)
                {

                    Hashtable MailSettingTable = ObjectEncrypter.Decrypt<Hashtable>(MailSetting.MailSetting);

                    txtSMTP.Text = MailSettingTable[DefaultSiteSetting.SMTP.ToString()].ToString();

                    if (MailSettingTable[DefaultSiteSetting.SMTPUser.ToString()] != null)
                        txtSMTPUser.Text = MailSettingTable[DefaultSiteSetting.SMTPUser.ToString()].ToString();

                    if (MailSettingTable[DefaultSiteSetting.SMTPassword.ToString()] != null || MailSettingTable[DefaultSiteSetting.SMTPassword.ToString()] != string.Empty)
                    {
                        if (txtSMTPwd.Text != "******")
                        {
                            txtSMTPwd.Text = MailSettingTable[DefaultSiteSetting.SMTPassword.ToString()].ToString();
                            if (txtSMTPwd.Text != string.Empty)
                                txtSMTPwd.Text = "******";
                        }
                    }

                    if (MailSettingTable[DefaultSiteSetting.SMTPort.ToString()] != null)
                        txtSMTPort.Text = MailSettingTable[DefaultSiteSetting.SMTPort.ToString()].ToString();

                    txtSMTPwd.Attributes.Add("Value", txtSMTPwd.Text);

                    if (Convert.ToBoolean(MailSettingTable[DefaultSiteSetting.SMTPEnableSSL.ToString()]))
                        chkSMTPSslEnable.Checked = true;


                }
            }
        }

        public bool ValidatePort(string strSMTP, int intPort)
        {
            TcpClient tcpSMTP;
            tcpSMTP = new TcpClient();

            try
            {
                tcpSMTP.Connect(strSMTP, Convert.ToInt32(intPort));
                return true;
            }
            catch (SocketException ex)
            {
                return (false);
            }
        }

        private bool VerifyCredentials(string strSMTP, string strUser, string strPwd, int intPort, bool boolsslEnable, out bool IsAuthDisabled)
        {
            bool IsVerified = true;

            IsAuthDisabled = false;

            TcpClient tcpSMTP;

            const string SMTP_PING_SUCCESS = "220";
            const string SMTP_EHLO_SUCCESS = "250";

            tcpSMTP = new TcpClient();

            tcpSMTP.Connect(strSMTP, Convert.ToInt32(intPort));
            if (tcpSMTP.Connected)
            {
                System.Net.NetworkCredential credentials = new System.Net.NetworkCredential(strUser, strPwd);
            }
            return IsVerified;

        }

        private MemberExtendedInformation BuildMemberExInfo()
        {
            MemberExtendedInformation memberExInfo = CurrentMemberExInfo;

            memberExInfo.MailSetting = GetSiteSettingTable();

            if (_memberId > 0)
            {
                memberExInfo.MemberId = _memberId;
            }
            memberExInfo.CreatorId = base.CurrentMember.Id;
            memberExInfo.UpdatorId = base.CurrentMember.Id;

            return memberExInfo;
        } 

        #endregion

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            //Code commented by Prasanth on 30/May/2016
            //txtSMTP.Enabled = false;
            //txtSMTPort.Enabled = false;
            //chkSMTPSslEnable.Enabled = false;
            //**************END*********************
            if (!Page.IsPostBack)
            {
                PopulateSiteSetting();
                //Accordion accordion = (Accordion)this.Page.Master.FindControl(UIConstants.LEFTMENUCONTROL_ID).FindControl("accordion");
               // accordion.SelectedIndex = 3;
            }
            
        }

        protected void btnSaveSetting_Click(object sender, EventArgs e)
        {
            MemberExtendedInformation memberExInfo = BuildMemberExInfo();
            bool IsAuthDisabled = false;
           
            int IntSMTPPort = 0;

            if (txtSMTPort.Text.Trim().IsNotNullOrEmpty())
            {
                IntSMTPPort = Convert.ToInt32(txtSMTPort.Text);
            }
           
            if (txtSMTP.Text.Trim().IsNotNullOrEmpty())
            {
               // bool flag = ValidatePort(txtSMTP.Text.Trim(), IntSMTPPort); //Commenetd by pravin khot (only save no validate as per requirements) on 5/Jan/2016
                bool flag = true;
                if (flag == false)
                {
                    MiscUtil.ShowMessage(lblMessage, "Please enter valid SMTP and Port number.", true);
                    return;
                }
                //Commenetd by pravin khot (only save no validate as per requirements) on 5/Jan/2016
                //if (!(VerifyCredentials(txtSMTP.Text.Trim(), txtSMTPUser.Text, txtSMTPwd.Text, IntSMTPPort, chkSMTPSslEnable.Checked, out IsAuthDisabled)))
                //{
                //    if (!IsAuthDisabled)
                //    {
                //        MiscUtil.ShowMessage(lblMessage, "Please set valid SMTP User Id and Password", true);
                //        return;
                //    }
                //}
                //***************END******************//(only save no validate as per requirements) on 5/Jan/2016
            }
            if (memberExInfo != null)
            {
                Facade.UpdateMemberExtendedInformation(memberExInfo);
                if (!IsAuthDisabled)
                    MiscUtil.ShowMessage(lblMessage, "SMTP Details saved successfully.", false);
                else
                    MiscUtil.ShowMessage(lblMessage, "SMTP Details saved successfully, SMTP credentials could not be validated since, SMTP Authentication is not enabled on SMTP server.", false);
            }
            PopulateSiteSetting();
        }

        protected  void btnTest_Click(object sender, EventArgs args)
        {
            string pwd = txtSMTPwd.Text;

            if (Send() == "1")
            {
                divTestMessage.InnerHtml = "<div class=\"alert alert-success\">SMTP details verified</div>";
            }
            else
            {
                divTestMessage.InnerHtml = "<div class=\"alert alert-error\">Unable to connect with SMTP details</div>";
            }

            txtSMTPwd.Attributes.Add("value", pwd);
          txtSMTPwd.Text =pwd ;
        }
        public string Send() 
        {
            try
            {

                MailMessage mailMessage = new MailMessage();
                SmtpClient sendMail = new SmtpClient();
                MailAddress addressFrom = new MailAddress(txtSMTPUser .Text );
                mailMessage.From = addressFrom;

                //***************Code modify by pravin khot on 21/June/2016************
                //mailMessage.To.Add("talentrackr@gmail.com");
                //mailMessage.Subject = "Test Mail From" + txtSMTPUser.Text;
                mailMessage.To.Add(addressFrom);
                mailMessage.Subject = "Test Mail From Talentrackr for " + txtSMTPUser.Text;
                mailMessage.Body = "This is an e-mail message sent automatically by Talentrackr while testing the settings for your account.";
                //**************************END*****************************
 
                SmtpClient mailSender = new SmtpClient();
                string strSMTP = string.Empty;
                string strUser = string.Empty;
                string strPwd = string.Empty;
                string strssl = string.Empty;
                int intPort = 25;
                //strSMTP = txtSMTP.Text;
                strUser = txtSMTPUser.Text;
                TPS360.BusinessFacade.Facade f1 = new TPS360.BusinessFacade.Facade();
                SiteSetting siteSetting = f1.GetSiteSettingBySettingType((int)SettingType.SiteSetting);
                Hashtable siteSettingTable = ObjectEncrypter.Decrypt<Hashtable>(siteSetting.SettingConfiguration);
                strSMTP = siteSettingTable[DefaultSiteSetting.SMTP.ToString()].ToString();

                if (txtSMTPwd.Text != "******")
                      strPwd = txtSMTPwd.Text;
                else
                {
                    MemberExtendedInformation MailSetting = Facade.GetMemberExtendedInformationByMemberId(base.CurrentMember.Id);

                    if (MailSetting.MailSetting != null)
                    {
                        Hashtable MailSettingTables = ObjectEncrypter.Decrypt<Hashtable>(MailSetting.MailSetting);
                        if (MailSettingTables[DefaultSiteSetting.SMTPassword.ToString()] != null)
                        {
                            strPwd = MailSettingTables[DefaultSiteSetting.SMTPassword.ToString()].ToString();
                        }
                    }
                }
                strssl = siteSettingTable[DefaultSiteSetting.SMTPEnableSSL.ToString()].ToString();
                //strssl = chkSMTPSslEnable.Checked.ToString();

                //intPort = Convert.ToInt32(txtSMTPort .Text );
                intPort = Convert.ToInt32(siteSettingTable[DefaultSiteSetting.SMTPort.ToString()].ToString());
                mailSender = new SmtpClient(strSMTP, intPort);
                NetworkCredential SMTPUserInfo = new NetworkCredential(strUser, strPwd);

                mailSender.UseDefaultCredentials = false;
                mailSender.Credentials = SMTPUserInfo;
                mailSender.EnableSsl = strssl == "True" ? true : false;
                mailSender.Send(mailMessage);
                return "1";
            }
            catch (Exception ex)
            {
                BasePage.writeLog(ex);
                return ex.Data.ToString();
            }
          
        }
        #endregion
    }
}