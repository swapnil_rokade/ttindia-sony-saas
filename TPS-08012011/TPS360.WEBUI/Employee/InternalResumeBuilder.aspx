﻿<%@ Page Language="C#" MasterPageFile="~/Masters/EmployeeProfile.master" AutoEventWireup="true"
    CodeFile="InternalResumeBuilder.aspx.cs" Inherits="TPS360.Web.UI.EmployeeInternalResumeBuilder"
    Async="true" EnableEventValidation="false" ValidateRequest="false" Title="Employee Resume Builder" %>


<%@ Register Src="~/Controls/BasicInfoEditor.ascx" TagName="BasicInfo" TagPrefix="uc1" %>
<asp:Content ID="cntEmployeeResumeBuilder" ContentPlaceHolderID="cphEmployeeMaster"
    runat="Server">
    <div class="TabPanelHeader">Profile</div>
    <asp:UpdatePanel ID="pnlBasicInfoTab" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <uc1:BasicInfo ID="ucntrlBasicInfo" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
