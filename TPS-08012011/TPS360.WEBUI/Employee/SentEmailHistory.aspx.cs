﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using AjaxControlToolkit;
namespace TPS360.Web.UI
{
    public partial class EmployeeSentEmailHistory : EmployeeBasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
            ucSentEmailHistory.EmailFromType = EmailFromType.EmailHistory;
            Page.Title = CurrentMember.FirstName + " " + CurrentMember.LastName + " - Sent Emails";
        }
    }
}
