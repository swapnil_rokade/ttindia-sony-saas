﻿<%@ Page Language="C#" MasterPageFile="~/Default.master" AutoEventWireup="true" CodeFile="RegistrationInfoEditor.aspx.cs"
    Inherits="TPS360.Web.UI.EmployeeRegistrationInfoEditor" Title="New User Registration" EnableEventValidation ="false" 
    Async="true" %>


<%@ Register Src="~/Controls/RegistrationInternal.ascx" TagName="regI" TagPrefix="uc1" %>
<asp:Content ID="cntRegistrationInformationHeader" ContentPlaceHolderID="cphHomeMasterTitle" runat="Server">
    New User Registration
</asp:Content>
<asp:Content ID="cntRegistrationInformation" ContentPlaceHolderID="cphHomeMaster"
    runat="Server">
    <div style="text-align: left;">
        <asp:UpdatePanel ID="pnlRegistrationInformation" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div id="divRegInternal" runat="server">
                    <uc1:regI ID="uctlInternal" runat="server" />
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
