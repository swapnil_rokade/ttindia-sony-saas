﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName:MyEmployeeList.aspx.cs
    Description: To display all the employees belongs to particular user
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date          Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1            Apr-14-2009        Gopala Swamy J        Defect ID: 10302;assigning status value to 0 
  
 * -------------------------------------------------------------------------------------------------------------------------------------------        
*/

using System;
using System.Collections.Generic;
using TPS360.Common.Helper;
using System.Web.Security;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Web.UI.Helper;
using TPS360.BusinessFacade;



namespace TPS360.Web.UI.Employee
{
    public partial class MyEmployeeList : EmployeeBasePage
    {
        #region Member Variables
        private int _memberId = 0;
        #endregion

        #region Methods

        private void GetMemberId()
        {
            //From Member Profile
            if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]))
            {
                hfMemberId.Value = Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID];

            }
            //From Member Login
            else
            {
                hfMemberId.Value = base.CurrentMember.Id.ToString();
            }
            _memberId = Int32.Parse(hfMemberId.Value);
        }

        #endregion

        #region Events

        #region Page Events

        protected void Page_Load(object sender, EventArgs e)
        {
            _memberId = Int32.Parse(hfMemberId.Value);
            if (!IsPostBack)
            {
                GetMemberId();
            }
            ucntrlEmployeeList.ManagerId = _memberId;
            ucntrlEmployeeList.Status = 0;
        }

        #endregion
        [System.Web.Services.WebMethod]
        public static object[] EmployeeAccessStatusChange(object data)
        {
            Dictionary<string, object> param = (Dictionary<string, object>)data;
            IFacade Facade = new Facade();
            int StatusSeclected = Convert.ToInt32(param["StatusId"]);
            int UpdatorId = Convert.ToInt32(param["UpdatorId"]);
            int MemberId = Convert.ToInt32(param["MemberId"]);

            Member member = Facade.GetMemberById(MemberId);

            if (StatusSeclected == 1)
            {
                MembershipUser thisuser = Membership.GetUser(member.UserId);
                if (thisuser.UnlockUser())
                {
                    member.Status = (int)AccessStatus.Enabled;
                    member.UpdatorId = UpdatorId;
                    Facade.UpdateMemberStatus(member);
                }
            }
            else if (StatusSeclected == 2)
            {
                if (Facade.LockUser(member.UserId))
                {
                    member.Status = (int)AccessStatus.Blocked;
                    member.UpdatorId = UpdatorId;
                    Facade.UpdateMemberStatus(member);
                }
            }

            return null;
        }
        #endregion
    }
}