﻿using System;
using TPS360.Common.Helper;
using System.Web.UI.WebControls;
namespace TPS360.Web.UI
{
    public partial class CandidateInternalRating : CandidateBasePage
    {
        #region Member Variables

        int _memberId = 0;

        #endregion

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            string name = string.Empty;
            if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]))
            {
                _memberId = Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]);
            }
            if (_memberId > 0)
                name = MiscUtil.GetMemberNameById(_memberId, Facade);

            Label lbModalTitle = (Label)this.Master.FindControl("lbModalTitle");
            if (lbModalTitle != null) lbModalTitle.Text = name + " - " + "Internal Rating";

            //divHeaderRating.InnerText = "Candidate Internal Rating";
            //this.Page.Title = name + " - " + "Internal Rating";
        }
        #endregion
    }
}
