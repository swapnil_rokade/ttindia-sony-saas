﻿<%@ Page Language="C#" MasterPageFile="~/Masters/CandidateProfile.master" AutoEventWireup="true" CodeFile="InternalCandidateHotLists.aspx.cs"
    Inherits="TPS360.Web.UI.InternalCandidateHotLists" Title="Candidate Hot Lists" %>

<%@ Register Src="~/Controls/MemberHotList.ascx" TagName="CandidateHotlist" TagPrefix="uc1" %>
<asp:Content ID="cntCandidateHotList" ContentPlaceHolderID="cphCandidateMaster" runat="Server">
  <asp:HiddenField ID="hdnPageTitle" runat ="server" />

                        <div style="width: 100%;">
                            <div style="text-align: left; width: 100%;">
                                <asp:UpdatePanel ID="pnlCandidateHotList" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>                    
                                        <uc1:CandidateHotlist id="ucntrlCandidateHotList"  runat="server" />
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                
</asp:Content>
