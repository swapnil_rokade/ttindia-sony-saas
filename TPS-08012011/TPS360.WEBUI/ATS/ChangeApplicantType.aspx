﻿<%@ Page Language="C#" MasterPageFile="~/Modals/Modal.master" AutoEventWireup="true" CodeFile="ChangeApplicantType.aspx.cs" 
Inherits="TPS360.Web.UI.CandidateList" Title="Change Candidate Type"%>
<%@ Register Src="~/Controls/ChangeApplicantType.ascx" TagName="ApplicantType" TagPrefix="uc1" %>
<asp:Content ID="cntCandidateListBody" ContentPlaceHolderID="cphModalMaster" runat="Server">
    <div style="width: 475px; height: 230px; max-height: 230px; overflow: auto; display : table-cell ; vertical-align :middle " align="center" >
        <asp:HiddenField ID="hfMemberId" runat="server" Value="0" />
        <uc1:ApplicantType ID="ucntApplicantType" runat="server" />
    </div>
</asp:Content>
