﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: InternalResumeBuilder.aspx.cs
    Description: This is the user control page used for resume builder.
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.           Date               Author             Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1               Mar-04-2009        Shivanand          Defect #10053; usercontrol "ucntrlJobTitle" is removed.
    0.2               July-01-2009       Shivanand          Defect #10464; AsyncOptions are provided for Tabs.
 *  0.3               Apr-07-2010        Ganapati Bhat      Enhancement #12139; Removed tab "Skill Set"
------------------------------------------------------------------------------------------------------------------------------------------- 
*/

using System;

using TPS360.Common.Helper;
using System.Drawing;
namespace TPS360.Web.UI
{
    public partial class CandidateInternalResumeBuilder : CandidateBasePage
    {
        #region Veriables

        private int Tab
        {
            get
            {
                if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_TAB]))
                {
                    return  Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_TAB]);
                }
                return 0;
            }
        }
        private int CurrentCandidateID
        {
            get
            {
                if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]))
                {
                    return Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]);
                }
                return 0;

            }
        }
        #endregion

        #region Methods

        private void CommonLoad()
        {
            //uwtResumeBuilder.SelectedTabIndex = Tab;
            uclBasicInfo.MemberRole = ContextConstants.ROLE_CANDIDATE;
            uclAdditionInfo.MemberRole = ContextConstants.ROLE_CANDIDATE; ;
            uclObjuectiveandSummary.MemberRole = ContextConstants.ROLE_CANDIDATE;
            uclSkills.MemberRole = ContextConstants.ROLE_CANDIDATE;
            uclEducation.MemberRole = ContextConstants.ROLE_CANDIDATE;
            uclExperience.MemberRole = ContextConstants.ROLE_CANDIDATE;
            uclCertification.MemberRole = ContextConstants.ROLE_CANDIDATE; ;
            uclResume.MemberRole = ContextConstants.ROLE_CANDIDATE;
        }
        
        #endregion

        #region Events
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CommonLoad();
                //uwtResumeBuilder.AsyncOptions.EnableProgressIndicator = true;    
            }
            
        }
     
        //protected void uwtResumeBuilder_TabClick(object sender, Infragistics.WebUI.UltraWebTab.WebTabEvent e)
        //{ 
        //    string page = "";
        //    switch (e.Tab.Key)
        //    {
        //        case "0":
        //            page = UrlConstants.ATS.ATS_RESUMEBUILDER;
        //            break;
        //        case "1":
        //            page = UrlConstants.ATS.ATS_ADDITIONALINFOEDITOR;
        //            break;
        //        case "2":
        //            page = UrlConstants.ATS.ATS_OBJECTIVEANDSUMMARYEDITOR;
        //            break;
        //        case "3":
        //            page = UrlConstants.ATS.ATS_SKILLEDITOR;
        //            break;
        //        case "4":
        //            page = UrlConstants.ATS.ATS_EXPERIENCEEDITOR;
        //            break;
        //        case "5":
        //            page = UrlConstants.ATS.ATS_EDUCATIONEDITOR;
        //            break;
        //        case "6":
        //            page = UrlConstants.ATS.ATS_CERTIFICATIONEDITOR;
        //            break;
        //        case "7":
        //            page = UrlConstants.ATS.ATS_COPYPASTERESUMEEDITOR;
        //            break;

        //    }

        //    Helper.Url.Redirect(page, string.Empty, UrlConstants.PARAM_TAB, e.Tab.Key, UrlConstants.PARAM_MEMBER_ID, CurrentCandidateID.ToString());

        //}
        #endregion
}
}