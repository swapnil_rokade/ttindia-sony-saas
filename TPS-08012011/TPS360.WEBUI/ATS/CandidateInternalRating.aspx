﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Modals/Modal.master" CodeFile="CandidateInternalRating.aspx.cs" Inherits="TPS360.Web.UI.CandidateInternalRating"
Title="Applicant Internal Ratings" %>
    
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
    
<%@ Register Src="~/Controls/RemarksAndInternalRatingEditor.ascx" TagName="RemarksInternalRating"
    TagPrefix="uc1" %>
<asp:Content ID="cntCandidateInternalRatings" ContentPlaceHolderID="cphModalMaster"
    runat="Server">
    <script src="../Scripts/jsUpdateProgress.js" type="text/javascript"></script>
<script type ="text/javascript" >
var ModalProgress ='<%= Modal.ClientID %>';
</script>
   <div  style =" width : 475px; height : 230px; max-height : 230px ; overflow :auto ; display : table-cell ; vertical-align : middle  " align="center" >
                               <asp:UpdatePanel ID="pnlRemarksratings" runat="server" UpdateMode="Conditional">
                                     <ContentTemplate>
                                        <uc1:RemarksInternalRating ID="ucntrlRemarksInternalRating" runat="server" />
                                    </ContentTemplate>  
                               </asp:UpdatePanel> 
                            
    <asp:Panel ID="pnlmodal" runat="server" style="display: none">
                    <asp:UpdatePanel runat="server" ID="UpdatePanel2" RenderMode="Inline" UpdateMode="Conditional">
                        <ContentTemplate>
                        <img src="../Images/AjaxLoading.gif" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
            </asp:Panel>
            <ajaxToolkit:ModalPopupExtender ID ="Modal" runat ="server" TargetControlID ="pnlmodal" PopupControlID ="pnlmodal"
             BackgroundCssClass ="divModalBackground" ></ajaxToolkit:ModalPopupExtender>
             </div>
</asp:Content>
