﻿<%-- 
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: TPS360Overview.aspx
    Description: This is the page which is used to display overview information of Candidates.
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1             Mar-25-2009             Jagadish            Defect id: 9608; Replaced the script form Content "cntTPS360OverviewHeader" to "cntTPS360Overview".
    0.2             Aug-31-2009             Ranjit Kumar.I      DefectID:#11423;Disabled Trace in Page Level Script
    0.3             Nov-19-2009             Sandeesh            Enhancement#11814 :Enabled Async property for this page 
-------------------------------------------------------------------------------------------------------------------------------------------       
--%>

<%@ Page Language="C#" MasterPageFile="~/Masters/CandidateProfile.master" AutoEventWireup="true"  Async="true"
    CodeFile="TPS360Overview.aspx.cs" Inherits="TPS360.Web.UI.CandidateOverview"
    Title="Applicant Overview" Trace="false" EnableEventValidation="false" %>
                      <%@ Register Src ="~/Controls/CandidateOverviewHeader.ascx" TagName ="OverviewHeader" TagPrefix ="ucl" %>
<%@ Register Src="~/Controls/TPS360Overview.ascx" TagPrefix="uc1" TagName="CandidateOverView" %>
<asp:Content ID="cntTPS360Overview" ContentPlaceHolderID="cphCandidateMaster" runat="Server">
<asp:HiddenField ID="hdnPageTitle" runat ="server" />
   
   
    <ucl:OverviewHeader  ID="uclOverviewHeader" runat="server"></ucl:OverviewHeader>
    <uc1:CandidateOverView ID="uclCandidateOverView" runat="server" />  
      <%-- 
      
    <div class="MainBox" style="width: 99%;">
            <table style ="border-color :#949494; border-style :solid ; border-width :1px; width :99%; padding: 3px 3px 3px 15px;"><tr><td>
                 <asp:ListView ID="lsvFileName" runat="server" DataKeyNames="Id" OnItemDataBound="lsvFileName_ItemDataBound">
                <LayoutTemplate>
                    <div style="width: 100%; float: left;">
                        <asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
                    </div>
                </LayoutTemplate>
                <EmptyDataTemplate>
                    <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server" >
                        <tr>
                            <td>
                                No Documents.
                            </td>
                        </tr>
                    </table>
                </EmptyDataTemplate>
                <ItemTemplate>
                    <div style="width: 25%; float: left; position: relative; ">
                        <asp:Label ID="lblFileName" runat="server" style=" white-space : normal "></asp:Label>
                    </div>
                </ItemTemplate>
            </asp:ListView>
      </td></tr></table>
     <div class="HeaderDevider">
            </div>
        
        <div style="clear: both; border: 1px solid #949494; padding: 10px 10px 10px 10px;
            width: 98%;">
            
        </div>
        <br />
</div>

--%>
</asp:Content>
