﻿<%-- -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: InternalResumeBuilder.aspx
    Description: This is the page which is used to present tab controls
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date           Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1             Oct-16-2008        Gopala Swamy         Defect id: 8994; Renamed in tabcontrol from "Job tittle & Category" to "Job tittles"
    0.2             Mar-04-2009        Shivanand            Defect #10053; Removed tab "Job Titles".
    0.3             May-26-2009         Sandeesh            Defect id : 10464 Changes made to load the tabs instantaneously
    0.4             July-01-2009       Shivanand            Defect #10464; AsyncMode is enabled for Tabs.
    0.5             Jul-24-2009           Veda              Defect id: 11072 ; Contineous Spinning occurs in the browser tab.
    0.6             Jul-30-2009           Veda              Defect id: 11132; 
    0.7             Aug-31-2009         Ranjit Kumar.I      Defect id:#11421;Desabled Trace in Page Level Script
    0.8             Apr-07-2010        Ganapati Bhat        Enhancement #12139; Removed tab "Skill Set"
-------------------------------------------------------------------------------------------------------------------------------------------       

--%>


<%@ Page Language="C#" MasterPageFile="~/Masters/CandidateProfile.master" AutoEventWireup="true" CodeFile="InternalResumeBuilder.aspx.cs" Inherits="TPS360.Web.UI.CandidateInternalResumeBuilder"  EnableEventValidation="false" ValidateRequest="false" Title="Candidate Resume Builder" %>


<%@ Register Src="~/Controls/BasicInfoEditor.ascx" TagName="BasicInfo" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/AdditionalInfoEditor.ascx" TagName="AdditionalInfo" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/ObjectiveSummaryEditor.ascx" TagName="ObjectiveSummary" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/MemberSkillsEditor.ascx" TagName="Skills" TagPrefix="uc1" %>    <%-- 0.2 --%>
<%@ Register Src="~/Controls/MemberExperienceEditor.ascx" TagName="Experience" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/MemberEducationEditor.ascx" TagName="Education" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/MemberCertificationEditor.ascx" TagName="Cerfication" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/MemberCopyPasteResume.ascx" TagName="CopyPasteResume" TagPrefix="uc1" %>
<asp:Content ID="cntCandidateResumeBuilder" ContentPlaceHolderID="cphCandidateMaster"
    runat="Server">
    
    <div class="tabbable"> <!-- Only required for left/right tabs -->
    <div class="TabPanelHeader">Profile Builder</div>
        <ul class="nav nav-tabs">
            <li class="active"><a href="#tab1" data-toggle="tab">Basic Info</a></li>
            <li><a href="#tab2" data-toggle="tab">Additional Info</a></li>
            <li><a href="#tab3" data-toggle="tab">Objective & Summary</a></li>
            <li><a href="#tab4" data-toggle="tab">Skills</a></li>
            <li><a href="#tab5" data-toggle="tab">Experience</a></li>
            <li><a href="#tab6" data-toggle="tab">Education</a></li>
            <li><a href="#tab7" data-toggle="tab">Resumes</a></li>
    </ul>
    <div class="tab-content" style ="overflow:none !important">
         <div class="tab-pane active" id="tab1">
               <uc1:BasicInfo ID="uclBasicInfo" runat ="server" />
         </div>
         <div class="tab-pane" id="tab2" style =" width : 100%;">
            <uc1:AdditionalInfo ID="uclAdditionInfo" runat ="server" />
         </div>
         <div class="tab-pane" id="tab3">
            <uc1:ObjectiveSummary ID="uclObjuectiveandSummary" runat ="server" />
         </div>
         <div class="tab-pane" id="tab4">
            <uc1:Skills runat ="server" ID ="uclSkills" />
         </div>
         <div class="tab-pane" id="tab5">
            <uc1:Experience ID ="uclExperience" runat ="server" />
         </div>
         <div class="tab-pane" id="tab6">
         <uc1:Education ID ="uclEducation" runat ="server" />
    <hr class="divider" style ="  margin-top : 10px; margin-bottom : 10px; "></hr>
            <uc1:Cerfication ID="uclCertification" runat ="server" />
         </div> 
         <div class="tab-pane" id="tab7">
            <uc1:CopyPasteResume ID="uclResume" runat ="server" />
         </div>
    </div>
    </div>
</asp:Content>
        