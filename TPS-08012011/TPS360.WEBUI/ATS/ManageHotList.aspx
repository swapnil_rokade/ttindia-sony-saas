﻿<%@ Page Language="C#" MasterPageFile="~/Default.master" AutoEventWireup="true" Async="true"
    CodeFile="ManageHotList.aspx.cs" Inherits="TPS360.Web.UI.ManageHotList" Title="Manage Hot List"
    EnableEventValidation="false" %>

<%@ Register Src="~/Controls/ManageHotList.ascx" TagName="ManageHotList" TagPrefix="uc1" %>
<asp:Content ID="cntManageHotListHeader" ContentPlaceHolderID="cphHomeMasterTitle"
    runat="Server">
    Manage Hot List -
    <asp:Label ID="lblHotListName" runat="server"></asp:Label>
</asp:Content>
<asp:Content ID="cntManageHotListBody" ContentPlaceHolderID="cphHomeMaster" runat="Server">
    <asp:UpdatePanel ID="pnlEducationTab" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <uc1:ManageHotList ID="ucntrlManageHotList" IsMyList="true" HeaderTitle="My Candidates List"
                runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
