﻿<%------------------------------------------------------------------------------------------------------------------------------------
    FileName: PreciseSearch.aspx
    Description: This page is used for precise search in ATS.
    Created By: 
    Created On:
    Modification Log:
    -----------------------------------------------------------------------------------------------------------------------------------
    Ver.No.        Date           Author            Modification
    -----------------------------------------------------------------------------------------------------------------------------------
    0.1            12-Nov-2008    Shivanand         Defect #8670; Tab "Consultants Database" is removed.
    0.2            Nov-18-2009    Sandeesh         Enhancement#11814 :AM Notification - When a recruiter adds a candidate to a req the app will send a notification email to the Account Manager

---------------------------------------------------------------------------------------------------------------------------------------

--%>

<%@ Page Language="C#" MasterPageFile="~/Default.master" AutoEventWireup="true" Async="true"
    CodeFile="PreciseSearch.aspx.cs" Inherits="TPS360.Web.UI.Candidate.PreciseSearch"
    Title="Precise Search" EnableEventValidation="false" %>
<%@ Register Src="~/Controls/PreciseSearch.ascx" TagName="PreciseSearch" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script type="text/javascript">

        function CheckUnCheckRowLevelCheckboxes(cntrlHeaderCheckbox, cntrlGridView, posCheckbox) {
            //debugger;
            if (cntrlHeaderCheckbox.checked == true) {
                CheckAllGridViewRowCheckBoxes(cntrlGridView, posCheckbox);
            }
            else {
                UnCheckAllGridViewRowCheckBoxes(cntrlGridView, posCheckbox);
            }

        }

        function CheckUnCheckAllRowLevelCheckboxes(cntrlHeaderCheckbox, cntrlGridView) {
            //debugger;
            if (cntrlHeaderCheckbox.checked == true) {
                CheckAllGridViewRowCheckBoxes(cntrlGridView, 0);
            }
            else {
                UnCheckAllGridViewRowCheckBoxes(cntrlGridView, 0);
            }

        }
        function CheckAllGridViewRowCheckBoxes(cntrlGridView, posCheckbox) {
            //debugger;
            var rowLength = cntrlGridView.rows.length;
            for (var i = 1; i < rowLength; i++) {
                var myrow = cntrlGridView.rows[i];
                var mycel = myrow.getElementsByTagName("td")[posCheckbox];
                var chkBox = mycel.childNodes[2];
                if (chkBox != null) {
                    if (chkBox.checked == null) {
                        chkBox = chkBox.childNodes[2];
                    }
                    if (chkBox != null) {
                        if (chkBox.checked != null && chkBox.checked != true) {
                            chkBox.checked = true;
                        }
                    }
                }
            }
        }

        function UnCheckAllGridViewRowCheckBoxes(cntrlGridView, posCheckbox) {
            var rowLength = cntrlGridView.rows.length;
            for (var i = 1; i < rowLength; i++) {
                var myrow = cntrlGridView.rows[i];
                var mycel = myrow.getElementsByTagName("td")[posCheckbox];
                var chkBox = mycel.childNodes[2];
                if (chkBox != null) {
                    if (chkBox.checked == null) {
                        chkBox = chkBox.childNodes[2];
                    }
                    if (chkBox != null) {
                        if (chkBox.checked != null && chkBox.checked == true) {
                            chkBox.checked = false;
                        }
                    }
                }
            }
        }

        function CheckUnCheckGridViewHeaderCheckbox(dgrName, chkboxName, posRowChkbox) {
            //debugger; 
            var chkBoxAll = document.getElementById(chkboxName);
            var tbl = document.getElementById(dgrName).getElementsByTagName("tbody")[0];
            var checked = 0;
            var rowNum = 0;
            var rowLength = tbl.rows.length;
            for (var i = 1; i < rowLength; i++) {
                var myrow = tbl.rows[i];
                var mycel = myrow.getElementsByTagName("td")[posRowChkbox];
                var chkBox = mycel.childNodes[2];
                if (chkBox != null) {
                    if (chkBox.checked == null) {
                        chkBox = chkBox.childNodes[2];
                    }
                    if (chkBox != null) {
                        if (chkBox.checked != null) {
                            rowNum++;
                        }
                        if (chkBox.checked != null && chkBox.checked == true) {
                            checked++;
                        }
                    }
                }
            }
            //debugger; 
            if (checked == rowNum) {
                chkBoxAll.checked = true;
            }
            else {
                chkBoxAll.checked = false;
            }

        }
    </script>

</asp:Content>
<asp:Content ID="cntPreciseSearchHeader" ContentPlaceHolderID="cphHomeMasterTitle"
    runat="Server">
    Candidate Precise Search
</asp:Content>
<asp:Content ID="cntPreciseSearchContent" ContentPlaceHolderID="cphHomeMaster" runat="Server">

    <script src="../Scripts/jsUpdateProgress.js" type="text/javascript"></script>

    <script type="text/javascript">
        var ModalProgress = '<%= Modal.ClientID %>';

        Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(PageLoadedReq);

        function PageLoadedReq(sender, args) {
            try {

                var div = document.getElementById('divListViewExample');
                //if(div!=null)div.style.width="90%";// screen.width-75 + "px" ;

            }
            catch (e) {
            }
        }
    </script>

    <div align="center" id="divPrecieseSearch">
        <%--<igtab:UltraWebTab ID="UltraWebTab1" runat="server" EnableAppStyling="True" SelectedTab="0"
            Width="100%">
            <DefaultTabStyle Height="16px" Font-Size="8pt">
                <Padding Top="2px"></Padding>
            </DefaultTabStyle>
            <RoundedImage LeftSideWidth="7" RightSideWidth="6" ShiftOfImages="2" SelectedImage="../Images/ig_tab_winXPs1.gif"
                NormalImage="~/Images/ig_tab_winXPs3.gif" HoverImage="~/Images/ig_tab_winXPs2.gif"
                FillStyle="LeftMergedWithCenter"></RoundedImage>
            <SelectedTabStyle>
                <Padding Bottom="2px"></Padding>
            </SelectedTabStyle>
            <Tabs>
                <igtab:Tab Text="Candidate Database Search">
                    <ContentTemplate>
                        <uc1:PreciseSearch IsMyList="false" ID="ctrlEntireInternalDatabase" runat="server" />
                    </ContentTemplate>
                </igtab:Tab>
                <igtab:Tab Text="My Database" Visible="false">
                    <ContentTemplate>
                        <uc1:PreciseSearch IsMyList="true" ID="ctrlPreciseSearch" runat="server" />
                    </ContentTemplate>
                </igtab:Tab>
            </Tabs>
        </igtab:UltraWebTab>--%>
        <contenttemplate>
            <uc1:PreciseSearch IsMyList="false" ID="ctrlEntireInternalDatabase" runat="server" />
        </contenttemplate>
    </div>
    <asp:Panel ID="pnlmodal" runat="server" Style="display: none">
        <asp:UpdatePanel runat="server" ID="UpdatePanel2" RenderMode="Inline" UpdateMode="Conditional">
            <ContentTemplate>
                <img src="../Images/AjaxLoading.gif" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
    <ajaxToolkit:ModalPopupExtender ID="Modal" runat="server" TargetControlID="pnlmodal"
        PopupControlID="pnlmodal" BackgroundCssClass="divModalBackground">
    </ajaxToolkit:ModalPopupExtender>
</asp:Content>
