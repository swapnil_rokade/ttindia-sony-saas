﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Common.BusinessEntities;
using TPS360.Web.UI.Helper;
namespace TPS360.Web.UI
{
    public partial class UnSubscribe : BasePage
    {

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                string memberID = Request["Id"];
               //memberID = "8396";
                if (memberID != null && memberID.Trim() != string.Empty)
                {
                    int memId = Int32.Parse(memberID);
                    Facade.UnSubscribeMe (memId ); 
                }
            }
        }
     
    }
}
