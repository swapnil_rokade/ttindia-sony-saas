﻿<%@ Page Language="C#" MasterPageFile="~/Masters/CandidateProfile.master" AutoEventWireup="true" CodeFile="InternalNotesAndActivities.aspx.cs"
    Inherits="TPS360.Web.UI.CandidateInternalNotesAndActivities" Title="Applicant Notes And Activities" %>
    
<%@ Register Src="~/Controls/NotesAndActivitiesEditor.ascx" TagName="NotsActivities"
    TagPrefix="uc1" %>  
    
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

    
<asp:Content ID="cntCandidateInternalNotesActivities" ContentPlaceHolderID="cphCandidateMaster"
    runat="Server">
    <script src="../Scripts/jsUpdateProgress.js" type="text/javascript"></script>
<script type ="text/javascript" >
var ModalProgress ='<%= Modal.ClientID %>';
</script>
   
                        <div style="width: 99%;">
                            <div>
                              <asp:UpdatePanel ID="pnlNotes" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <uc1:NotsActivities ID="ucntrlNotsActivities" runat="server" />
                                    </ContentTemplate>                               
                               </asp:UpdatePanel>
                            </div>
                        </div>
                   
    <asp:Panel ID="pnlmodal" runat="server" style="display: none">
                    <asp:UpdatePanel runat="server" ID="UpdatePanel2" RenderMode="Inline" UpdateMode="Conditional">
                        <ContentTemplate>
                        <img src="../Images/AjaxLoading.gif" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
            </asp:Panel>
            <ajaxToolkit:ModalPopupExtender ID ="Modal" runat ="server" TargetControlID ="pnlmodal" PopupControlID ="pnlmodal"
             BackgroundCssClass ="divModalBackground" ></ajaxToolkit:ModalPopupExtender>
</asp:Content>
