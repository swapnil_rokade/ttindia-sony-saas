﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using TPS360.Web.UI;
using TPS360.Common.BusinessEntities;
using System.Collections.Generic;



public partial class Modals_ViewOffer : RequisitionBasePage
{
    double totalctc = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {

            if (Request.QueryString["MID"] != null && Request.QueryString["ID"] != null)
            {
                string Id = Request.QueryString["MID"].ToString();
                int ApprovalType = 1;
                int MemberId = Convert.ToInt32(Request.QueryString["ID"]);
                GetHiringDetails(MemberId, Id);
                DisplayOfferedSalaryTemplate(MemberId);
            }
        }
        Label lbModalTitle = (Label)this.Page.Master.FindControl("lbModalTitle");
        if (lbModalTitle != null)
            lbModalTitle.Text = "View Offer Details";
    }



    public void DisplayOfferedSalaryTemplate(int memberid)
    {
        DataTable dt = new DataTable();
        DataRow dr = null;
        int itemCount = 0;
        List<MemberHiringDetails> alList = Facade.GetMemberHiringDetailsShowRecords(memberid);


        dt.Columns.Add(new DataColumn("Heads", typeof(string)));
        dt.Columns.Add(new DataColumn("Value", typeof(string)));
        

        if (alList != null && alList.Count > 0)
        {
            hideHeader.Visible = true;
            for (int i = 0; i < alList.Count; i++)
            {
                dr = dt.NewRow();
                dr["Heads"] = alList[i].Header.ToString();
                dr["Value"] = alList[i].Value.ToString();
                dt.Rows.Add(dr);
            }
        }
        else
            hideHeader.Visible = false;

        dr = dt.NewRow();
        dr["Heads"] = "Total CTC";
        //dr["Value"] = Convert.ToDouble(lblSal.Text);
        dr["Value"] = totalctc.ToString();// System.Math.Round(Convert.ToDouble(lblSal.Text), 2); 
        dt.Rows.Add(dr);

        ViewState["CurrentTable"] = dt;

        grvSalaryTemplate.DataSource = dt;
        grvSalaryTemplate.DataBind();

        //for (int i = 0; i < offeredTemplate.Count; i++)
        //{
        //    dr = dt.NewRow();
        //    dr["Heads"] = offeredTemplate[i].Heads.ToString();
        //    dt.Rows.Add(dr);
        //}

        //ViewState["CurrentTable"] = dt;

        //grvSalaryTemplate.DataSource = dt;
        //grvSalaryTemplate.DataBind();

        //foreach (GridViewRow row in grvSalaryTemplate.Rows)
        //{
        //    SetFormulaTextBoxValues(Convert.ToDouble(offeredTemplate[itemCount].Value), row);
        //    itemCount++;
        //}
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
            e.Row.Cells[1].HorizontalAlign = HorizontalAlign.Right;
    }

    void GetHiringDetails(int MemberId, string memberloginId)
    {
        ArrayList alList = Facade.GetPagedMemberHiringDetailsShowValue(MemberId, Convert.ToInt32(memberloginId));

        if (alList.Count > 0)
        {
            DateTime dateOffer = Convert.ToDateTime(alList[0]);
            DateTime dateJoin = Convert.ToDateTime(alList[2]);
            lblDateOfOffer.Text = Convert.ToString(dateOffer.ToShortDateString());
            lblDemandType.Text = Convert.ToString(alList[1]) == "" ? "Contract" : "Permanent";
            lblDemandType.Text = Convert.ToString(alList[1]);
            lblDateOfJoining.Text = Convert.ToString(dateJoin.ToShortDateString());
            lblJoiningLocation.Text = Convert.ToString(alList[3]);
            lblPosition.Text = Convert.ToString(alList[4]);
            lblReportingHead.Text = Convert.ToString(alList[5]);
            lblGrade.Text = Convert.ToString(alList[6]);
            totalctc = Convert.ToDouble(alList[7]);
            lblSal.Text = Convert.ToString(alList[7]);
            lblCurrency.Text = "INR";
            lblPayrateCurrency.Text = "(Lacs)";

            MemberHiringDetails MHD = Facade.GetJobDescriptionByMHDId(MemberId);
            if (MHD.JobDescription == string.Empty)
            {
                divRefInfo.Visible = false;
            }
            else
            {
                divRefInfo.Visible = true ;
                txtRefInformation.Text = MHD.RawDescription;
            }
        
            JobPosting js = Facade.GetJobPostingById(Convert.ToInt32(alList[8]));
            if (js != null)
            {
                txtRequisitionName.Text = js.JobTitle + " [" + js.JobPostingCode + "]";
                txtProjectName.Text = js.ProjectName;               
            }
            CandidateOverviewDetails overviewdetail = Facade.GetCandidateOverviewDetails(Convert.ToInt32(alList[9]));
            if (overviewdetail != null)
            {
                lblsourcedescription.Text = overviewdetail.ResumeSourceName.Replace("<br/>", " - ");
            }

            if (lblDemandType.Text == "Contract")
            {
                hideGrade.Visible = false; 
                lblCTC.Text = "Amount";
                ddlSalary.Visible = false;
                lblResourceDate.Visible = true;
                lblResourceOnTextValue.Visible = true;
                lblResourceOnTextValue.Text = "From " + Convert.ToString(js.ResourceStartDate.ToShortDateString()) + " To " + Convert.ToString(js.ResourceEndDate.ToShortDateString());
            }
            else
            {
                lblResourceDate.Visible = false;
                lblResourceOnTextValue.Visible = false;
                hideGrade.Visible = true ; 
            }
        }

        //Facade.GetPagedMemberHiringDetails(
        //Facade.UpdateOfferApprovalStatus(Convert.ToInt32(HFJID.Value), Convert.ToInt32(HFID.Value), HFStatus.Value, txtComment.Text);
    }
}
