﻿<%@ Page Language="C#" MasterPageFile="~/Modals/Modal.master" AutoEventWireup="true" CodeFile="DisplayRequisitionInVendorPortal.aspx.cs" Inherits="TPS360.Web.UI.DisplayRequisitionInVendorPortal"  Title="Untitled Page" %>

<asp:Content ID="cntReferralLinkBuilder" ContentPlaceHolderID="cphModalMaster" runat="Server">   
 <asp:UpdatePanel ID="upRequsitionApproval" runat="server">

<ContentTemplate>   
  <script type="text/javascript" language="javascript">
 Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(PageLoadedReq); 
function PageLoadedReq(sender,args)
{
   ShowAndHideVendor();
}

  function ShowAndHideVendor()
       {
        
               var chkDisplayVendor=document .getElementById ('<%=chkVendorPortal.ClientID %>');
               var divVendor = document.getElementById('<%=divVendor.ClientID %>');
               var divbtnSubmit = document.getElementById('<%=divbtnSubmit.ClientID %>');
               if(chkDisplayVendor != null && divVendor != null)
               {
                   if(chkDisplayVendor.checked)
                   {
                       divVendor.style.display = "";
//                       divbtnSubmit.style.display = "";
                   }
                   else
                   {
                       divVendor.style.display = "none";
//                       divbtnSubmit.style.display = "none";
                   }
               }
              
       }
 </script>   
<asp:HiddenField ID="hdnmessage" runat="server" />
  <div  style =" width : 655px; height : 290px; " >
    <div class ="TableRow">
        <div style="text-align:center;  padding-left: 20%; padding-top:20px;">
        <div id ="alertwarning" runat ="server" class ="alert alert-warning"   enableviewstate="true" style =" width : 70%;  ">
           <b><asp:CheckBox ID="chkVendorPortal" runat="server" Text="Display Requisition in Vendor Portal"  onclick="ShowAndHideVendor()" /></b>
        </div>
        </div>   
        
          <div id="divVendor" runat="server" >
       <div class="TableRow">
           <div class="TableFormLeble">
               <strong>Publish to Selected Vendors:</strong>
           </div>
           <div style="border: solid 1px #7f7f7f; border-collapse: collapse; color: #333333; width: 42%; height: 100px; overflow: auto">
               <asp:CheckBoxList ID="chkVendorList" runat="server" AutoPostBack="false" abIndex="5">
               </asp:CheckBoxList>
           </div>
       </div>
       </div>
       <br />
         
       
    </div>
      <div id="divbtnSubmit" runat="server" >
        <div class="TableRow" style="text-align: center;padding-top:20px;">
            <asp:Button ID="btnSubmit"   CssClass="btn btn-primary" runat="server" Text="Submit"  OnClientClick="check();" onclick="btnSubmit_Click" />
         </div>
        </div>
    </div>
    
 </ContentTemplate>
</asp:UpdatePanel>
<script type="text/javascript" language="javascript">
    function check() {

        var hdnmessage = document.getElementById('<%= hdnmessage.ClientID %>');
        var chkDisplayVendor = document.getElementById('<%=chkVendorPortal.ClientID %>');

        if (chkDisplayVendor.checked) {
            if (confirm("Are you sure that you want to Enable Vendor Portal ?")) {                
                hdnmessage.value = "Yes";
            } else {            
                hdnmessage.value = "No";
            }
            return true;
        }
        else {
            if (confirm("Are you sure that you want to Disable Vendor Portal ?")) {
                hdnmessage.value = "Yes";
            } else {
                hdnmessage.value = "No";
            }
            return true;
        }
    }
    
</script>
</asp:Content>
        