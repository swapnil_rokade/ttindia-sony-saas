﻿<%@ Page Language="C#" MasterPageFile="~/Modals/Modal.master" AutoEventWireup="true" CodeFile="RequsitionStatusChange.aspx.cs" Inherits="TPS360.Web.UI.Modals_RequsitionStatusChange" Title="Untitled Page" %>
<%@ Register Assembly="Infragistics35.Web.v12.2, Version=12.2.20122.1007, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.Web.UI.EditorControls" TagPrefix="ig" %>
<%@ Register Assembly="Infragistics35.Web.v12.2, Version=12.2.20122.1007, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.Web.UI.LayoutControls" TagPrefix="ig" %>
 <%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="cntRequsitionStatusChange" ContentPlaceHolderID="cphModalMaster" Runat="Server">
 <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
   <asp:HiddenField ID="hdnmessage" runat="server" />
<asp:UpdatePanel ID="upRequsitionStatusChange" runat="server">
<ContentTemplate>
<div id="DivReqDate" runat="server">
      
         <div class="TableRow">
                        <div class="TableFormLeble">
                            <%--Added For LandT--%>
                            <asp:Label ID="lblStartDate" runat="server" Text="Req. Open Date"></asp:Label>:
                        </div>
                        <div class="TableFormContent" style="white-space: nowrap">
                            <div>
                                <div style="float: left" id="divDate" runat="server">
                                  <ig:WebDatePicker ID="wdcStartDate" DropDownCalendarID="ddcStartDate" runat="server" TabIndex="9">
                                    </ig:WebDatePicker>
                                    <ig:WebMonthCalendar ID="ddcStartDate" HideOtherMonthDays="true" AllowNull="true"
                                        runat="server">
                                    </ig:WebMonthCalendar>
                                </div>
                                <span class="RequiredField">*</span>
                                <%--for LandT--%>
                                <div id="divASAP" runat="server" style="display: none">
                                    <div align="left" style="float: left">
                                        <asp:CheckBox ID="chkStartDate" runat="server" Text="ASAP" AutoPostBack="true" OnCheckedChanged="chkStartDate_CheckedChanged"
                                            TabIndex="5" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="TableRow">
                        <div class="TableFormValidatorContent" style="margin-left: 42%">
                            <asp:CompareValidator ID="cmpStartDate" runat="server" Operator="GreaterThanEqual"
                                Type="Date" ControlToValidate="wdcStartDate" ErrorMessage="Start date should not be before the current date"
                                Display="Dynamic" />
                            <asp:RequiredFieldValidator ID="rfvStartDate" runat="server" ControlToValidate="wdcStartDate"
                                SetFocusOnError="true" ErrorMessage="Please select date of requisition." Display="Dynamic"
                                ValidationGroup="ValGrpJobTitle"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <%--Added for LandT--%>
                    <div class="TableRow">
                        <div class="TableFormValidatorContent" style="margin-left: 42%">
                           <%-- <asp:CompareValidator ID="cmpOpenDate" runat="server" Operator="GreaterThanEqual"
                                Type="Date" ControlToValidate="wdcOpenDate" ErrorMessage="Open date should not be before the current date"
                                Display="Dynamic" ValidationGroup="ValGrpJobTitle" />--%>
                           <%-- <asp:CompareValidator ID="cmbopenwithExpectedFullfillment" runat="server" Operator="LessThanEqual"
                                Type="Date" ControlToValidate="wdcOpenDate" ErrorMessage="Open date should not be after the expected fullfillment date"
                                Display="Dynamic" ControlToCompare="wdcClosingDate" ValidationGroup="ValGrpJobTitle" />
                            <asp:RequiredFieldValidator ID="rfVOpendDate" runat="server" ControlToValidate="wdcOpenDate"
                                SetFocusOnError="true" ErrorMessage="Please select open date." Display="Dynamic"
                                ValidationGroup="ValGrpJobTitle"></asp:RequiredFieldValidator>--%>
                        </div>
                    </div>
                    <div class="TableRow">
                        <div class="TableFormLeble">
                            <%--for LandT--%>
                            <asp:Label ID="lblClosingDate" runat="server" Text="Req. Expected Fulfillment Date"></asp:Label>:
                        </div>
                        <div class="TableFormContent" style="white-space: nowrap">
                            <div>
                                <div style="float: left" id="divExpFulfillmentDate" runat="server">
                                  <ig:WebDatePicker ID="wdcClosingDate" DropDownCalendarID="ddcClosingDate" runat="server" TabIndex="10">
                                    </ig:WebDatePicker>
                                    <ig:WebMonthCalendar ID="ddcClosingDate" AllowNull="true" HideOtherMonthDays="true"
                                        runat="server">
                                    </ig:WebMonthCalendar>
                                </div>
                                <span class="RequiredField">*</span>
                            </div>
                        </div>
                    </div>
                    <div class="TableRow">
                        <div class="TableFormValidatorContent" style="margin-left: 42%">
                            <asp:RequiredFieldValidator ID="rfvClosingDate" runat="server" ControlToValidate="wdcClosingDate"
                                SetFocusOnError="true" ErrorMessage="Please Select Expected Fulfillment Date."
                                Display="Dynamic" ValidationGroup="ValGrpJobTitle"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="TableRow">
                        <div class="TableFormValidatorContent" style="margin-left: 42%">
                            <asp:CompareValidator ID="cmpDate" ControlToCompare="wdcStartDate" ControlToValidate="wdcClosingDate"
                                Type="Date" Operator="GreaterThanEqual" ErrorMessage="Expected Fulfillment Date Should not before Date of Requisition."
                                ValidationGroup="ValGrpJobTitle" Display="Dynamic" runat="server"></asp:CompareValidator>
                        </div>
                    </div>

</div>



<div style="width: 675px; height: 500px;">
<div class="TableRow" style="text-align: left;" id="divApply" runat="server">
<%--<asp:Label ID="lblApplied" runat="server" Text="Requsition Approval" EnableViewState="true" CssClass=" btn btn-success"></asp:Label>--%>
</div>
<div class="TableRow" style="text-align: left;" id="divMsg" runat="server">
<asp:Label ID="lblMessage" runat="server" Text="Comments" EnableViewState="true" CssClass="TabPanelHeader"></asp:Label>
</div>

<div class="TableRow" style="text-align: left; color:red; font-size:small" id="divonHold"  runat="server">
        <asp:Label ID="lblonHold" runat="server" Text="" style="text-align: left; color:red; font-size:small" ></asp:Label>
</div>

<div class="TableRow" style="text-align: left;" id="div3" runat="server"></div>
<div class="TableRow" style="text-align: left;" id="div1" runat="server">
<%--<textarea cols="20" rows="5" id="txtComment" style="width: 450px"></textarea>--%>

    <asp:TextBox ID="txtComment" runat="server" TextMode="MultiLine" Width="600px" 
        Height="150px"></asp:TextBox><br />
    <asp:RequiredFieldValidator ID="RVComments" runat="server" 
        ErrorMessage="Please enter comment." ControlToValidate="txtComment"></asp:RequiredFieldValidator>
</div>
<div class="TableRow" style="text-align: left;" id="div4" runat="server"></div>
<br />
<div class="TableRow" style="text-align: left;" id="div2" runat="server">
<asp:Button ID="btnOk" runat="server" CssClass="btn btn-primary" Text="Ok" 
        Width="80px" onclick="btnOk_Click" OnClientClick="check();"/> &nbsp;&nbsp;&nbsp;&nbsp;
<asp:Button ID="btnCancel" runat="server" CssClass="btn btn-primary" Text="Cancel" 
        Width="80px" onclick="btnCancel_Click" OnClientClick="javascript:parent.CloseModal();"/>
    <asp:HiddenField ID="HFStatus" runat="server" />
    <asp:HiddenField ID="HFJID" runat="server" />
    <asp:HiddenField ID="HFID" runat="server" />
    
</div>
</ContentTemplate>
</asp:UpdatePanel>

<script type="text/javascript" language="javascript">
    function check() {
        var v = document.getElementById("<%=RVComments.ClientID%>");
        var hdnmessage = document.getElementById('<%= hdnmessage.ClientID %>');
        var HFStatus = document.getElementById('<%= HFStatus.ClientID %>');
        var message = "";
        if (HFStatus.value == "Cancel") {
            message = "All Candidates added to the Hiring Matrix will be removed ?"
        }
        else if (HFStatus.value == "Close") {
            message = "Are you sure that you want to close ?"
        }
        else if (HFStatus.value == "Remove On-Hold") {
        message = "Are you sure that you want to Re-Open Requisition for Hiring ?"
        }
        else {
            message = "Requisition will be On-Hold and Candidates cannot be added to the Requisition or Processed in Hiring Matrix."
        }
        ValidatorValidate(v);
        if (v.isvalid) {
            if (confirm(message)) {
                hdnmessage.value = "Yes";
            } else {
            hdnmessage.value = "No";
            }
//           parent.CloseModal();
            return true;
        }
        else {
            hdnmessage.value = "No";
            return false;
        }
    }
//    function test(url) {
//        alert(document.location.href.toString());
//        alert(url);

//        document.location.href = url;
//        
//    }
</script>
</asp:Content>


