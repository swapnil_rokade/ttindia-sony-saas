﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName:Default.master.cs
    Description:This is the master page for displaying Links to "My Account","Logout","Home"
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date              Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1             Jan-13-2009          Gopala Swamy          Enhancement  id:8675; Made visibility for "My Account" and provided Navigation Path to Basic Info Page
    0.2             Feb-5-2009           Nagarathna.V.B        Defect id:9867  for Consultant Provided Navigation path to basic info page.
    0.3             Feb-26-2009          Shivanand             Defect #9354; Variable strWelcomePageName is declared as static.
                                                                             In the method Page_Load(), the logic to assign properties to hyperlink "Home" is placed outside the Postback.
    0.4             Feb-26-2009         Nagarathna.V.B         Defect id:9739 assigned path to home hyperlink button.
-------------------------------------------------------------------------------------------------------------------------------------------       
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Web.UI.Helper;
using System.Web.Security;
using System.Web.UI.HtmlControls;

namespace TPS360.Web.UI
{
    public partial class ModalMaster : BaseMasterPage
    {
           

        

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
        }

      
        #endregion
    }
}