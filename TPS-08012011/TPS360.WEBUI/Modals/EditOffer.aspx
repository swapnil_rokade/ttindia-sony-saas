﻿<%@ Page Language="C#" MasterPageFile="~/Modals/Modal.master" AutoEventWireup="true"
    CodeFile="EditOffer.aspx.cs" Inherits="Modals_EditOffer" Title="Untitled Page" %>

<asp:Content ID="cntOfferApproval" ContentPlaceHolderID="cphModalMaster" runat="Server">
    <div style="padding: 10px 10px 10px 10px; height: 1185px; width: 900px;">
        <asp:Label ID="lblMessage" runat="server" EnableViewState="false"></asp:Label>
        <div >
            <h2>
                View Offer Details</h2>
        </div>
        <div>
        </div>
        <div 
            <asp:Panel ID="pnlCandidateJobDetail" runat="server">
                <asp:UpdatePanel ID="uppane" runat="server">
                    <ContentTemplate>
                        <asp:HiddenField ID="hdnBulkAction" runat="server" Value="" />
                        <asp:HiddenField ID="hfMemberId" runat="server" />
                        <asp:HiddenField ID="hfJobPostingId" runat="server" />
                        <asp:HiddenField ID="hfMemberHiringDetailsId" runat="server" Value="0" />
                        <asp:HiddenField ID="hfStatusId" runat="server" />
                        <asp:HiddenField ID="hfCurrentMemberId" runat="server" />
                        <asp:HiddenField ID="hdnSelectedMBand" runat="server" Value="0" />
                        <asp:HiddenField ID="hdnSelectedBand" runat="server" Value="0" />
                        <br />
                        <br />
                        <div class='noGradeSalTemp' style="text-align: center; font-family: Tahoma; font-size: 10pt;
                            color: #FF0000; padding: 12px;">
                            <asp:Label ID="Label1" runat="server"></asp:Label>
                        </div>
                        <div class="TableRow">
                            <div class="FormLeftColumn">
                                <div class="TableRow">
                                    <div class="TableFormLeble">
                                        Date of Offer:</div>
                                    <div class="TableFormContent">
                                        <asp:Label ID="lblDateOfOffer" runat="server"></asp:Label>
                                    </div>
                                </div>
                                <div class="TableRow">
                                    <div class="TableFormLeble">
                                        Expected Date of Joining:
                                    </div>
                                    <div class="TableFormContent">
                                        <asp:Label ID="lblDateOfJoining" runat="server"></asp:Label>
                                    </div>
                                </div>
                                <div class="TableRow">
                                    <div class="TableFormLeble">
                                        Offered Position:
                                    </div>
                                    <div class="TableFormContent">
                                        <asp:Label ID="lblPosition" runat="server" TabIndex="1"></asp:Label>
                                    </div>
                                </div>
                                <div class="TableRow">
                                    <div class="TableFormLeble">
                                        Reporting Manager:
                                    </div>
                                    <div class="TableFormContent">
                                        <asp:Label ID="lblReportingHead" runat="server" TabIndex="1"></asp:Label>
                                    </div>
                                </div>
                            </div>
                            <div class="FormRightColumn">
                                <div class="TableRow" align="left">
                                    <div class="TableFormLeble">
                                        Demand Type:
                                    </div>
                                    <div class="TableFormContent">
                                        <asp:Label ID="lblDemandType" runat="server" CssClass="CommonTextBox demandType"
                                            TabIndex="3" />
                                    </div>
                                    <div class="TableRow">
                                        <div class="TableFormLeble">
                                            Joining Location:
                                        </div>
                                        <div class="TableFormContent">
                                            <asp:Label ID="lblJoiningLocation" runat="server" CssClass="CommonTextBox Location"
                                                TabIndex="4" />
                                        </div>
                                        <div class="TableFormValidatorContent" style="margin-left: 42%">
                                        </div>
                                    </div>
                                    <div id='hideGrade' runat="server">
                                        <div class="TableRow">
                                            <div class="TableFormLeble">
                                                Grade:
                                            </div>
                                            <div class="TableFormContent">
                                                <asp:Label ID="lblGrade" runat="server"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="TableRow">
                                        <div class="TableFormLeble">
                                            CTC:
                                        </div>
                                        <div class="TableFormContent">
                                            <%-- //************************Code modify by pravin khot on 22/Feb/2016 ******add property -[rel="ValidDigit"]**********--%>
                                            <asp:Label ID="lblSal" runat="server" CssClass="CommonTextBox" rel="ValidDigit" TabIndex="2"
                                                Width="40px" onchange="fnSalaryAnnexure()"></asp:Label>
                                            <%--                      //***************************************END****************************************************  --%>
                                            <asp:DropDownList ID="ddlSalary" runat="server" CssClass="CommonDropDownList" Width="60px"
                                                TabIndex="3" Enabled="false">
                                                <asp:ListItem Value="4" Selected="True">Yearly</asp:ListItem>
                                                <asp:ListItem Value="3">Monthly</asp:ListItem>
                                                <asp:ListItem Value="2">Daily</asp:ListItem>
                                                <asp:ListItem Value="1">Hourly</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:DropDownList ID="ddlSalaryCurrency" runat="server" Width="60px" Enabled="false"
                                                CssClass="CommonDropDownList" TabIndex="4">
                                            </asp:DropDownList>
                                            <asp:Label ID="lblPayrateCurrency" runat="server"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br />
                            <br />
                            <br />
                            <div id='IsPermanent' runat="server" class='clsPermanent'>
                                <div class="TableRow" id="hideHeader" runat="server">
                                    <div class="TabPanelHeader">
                                        Salary Components:
                                    </div>
                                </div>
                                <div style="width: 90%; margin: 0 auto;">
                                    <asp:GridView ID="grvSalaryTemplate" runat="server" AutoGenerateColumns="False" OnRowDataBound="grvSalaryTemplate_RowDataBound">
                                        <Columns>
                                            <asp:BoundField DataField="Heads" HeaderText="Heads" />
                                            <asp:TemplateField HeaderText="Value">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtFormulaValue" CssClass="txtFormulaValue" runat="server" onkeypress="return onlyNumbers(this);"></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                            <div id="Div1" class="TableRow toggleBtn" align="center" style="padding: 20px;" runat="server">
                                <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click"  CssClass="CommonButton"
                                    ValidationGroup="ValOfferDetails" />
                                <%--<asp:Button ID="btnRemove" runat="server" Text="Remove Offer Details" 
                                    CssClass="CommonButton" />--%>
                            </div>
                        </div>
                        <!-- code modify by pravin khot on 8/March/2016 (rename label using Salary Components)------------------------>
                        <div>
                            <br />
                            <br />
                            <br />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </asp:Panel>
        </div>
    </div>
    <script type="text/javascript">
       
        //Restrict the user to key-in chrectors and other special charectors
        function onlyNumbers(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode

            if (charCode == 46) {
                var inputValue = $(this).val()
                if (inputValue.indexOf('.') < 1) {
                    return true;
                }
                return false;
            }
            if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }


 function mute()
 {
    alert("Record Updated Successfully.");

    return false;
}

</script>
</asp:Content>



