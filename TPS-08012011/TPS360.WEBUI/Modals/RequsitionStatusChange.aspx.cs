﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using TPS360.Common.BusinessEntities;
using System.Collections;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Web.UI.Helper;
using System.Text.RegularExpressions;
using System.Text;
using TPS360.Web.UI;
using ExpertPdf.HtmlToPdf;
using System.Drawing;
using System.IO;

namespace TPS360.Web.UI
{
    public partial class Modals_RequsitionStatusChange : RequisitionBasePage  //System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                lblonHold.Text = "";
                if (!Page.IsPostBack)
                {
                    HFJID.Value = Request.QueryString["JID"].ToString();
                    HFStatus.Value = Request.QueryString["STATUS"].ToString();
                    HFID.Value = Request.QueryString["ID"].ToString();
                }
                Label lbModalTitle = (Label)this.Page.Master.FindControl("lbModalTitle");
                if (lbModalTitle != null) lbModalTitle.Text = "Requsition Status Change- [" + HFStatus.Value + "]";

                if (HFStatus.Value == "Remove On-Hold")
                {
                    cmpStartDate.ValueToCompare = DateTime.Now.ToShortDateString();
                    lblonHold.Text = "Requisition will be Re-Opened for Hiring, please update the new Open Date and Expected fulfilment date.";
                    JobPosting jobPosting = Facade.GetJobPostingById(Convert.ToInt32(HFJID.Value));
                    DivReqDate.Visible = true;

                    if (!Page.IsPostBack)
                    {
                        if (!string.IsNullOrEmpty(Convert.ToString(jobPosting.StartDate)))
                        {
                            wdcStartDate.Value = jobPosting.StartDate;
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(jobPosting.FinalHiredDate)))
                        {
                            wdcClosingDate.Value = jobPosting.FinalHiredDate;
                        }
                    }
                }
                else
                {
                    DivReqDate.Visible = false;
                }
            }
            catch
            {
            }
       }     
        protected void chkStartDate_CheckedChanged(object sender, EventArgs e)
        {
        }
        protected void btnOk_Click(object sender, EventArgs e)
        {
            string confirmValue = hdnmessage.Value;

            if (confirmValue == "Yes")
            {
                string AdminEmailId = string.Empty;
                int AdminMemberid = 0;
                SiteSetting siteSetting = Facade.GetSiteSettingBySettingType((int)SettingType.SiteSetting);
                if (siteSetting != null)
                {
                    Hashtable siteSettingTable = ObjectEncrypter.Decrypt<Hashtable>(siteSetting.SettingConfiguration);
                    AdminEmailId = siteSettingTable[DefaultSiteSetting.AdminEmail.ToString()].ToString();
                    AdminMemberid = Facade.GetMemberIdByEmail(AdminEmailId);
                    Facade.UpdateRequsitionStatusWithComments(Convert.ToInt32(HFID.Value.ToString()), Convert.ToInt32(HFJID.Value.ToString()), HFStatus.Value.ToString(), txtComment.Text, "", GetFullRootUrl(), AdminMemberid);
                }

                if (HFStatus.Value == "Remove On-Hold")
                {
                    JobPosting jobPosting = Facade.GetJobPostingById(Convert.ToInt32(HFJID.Value.ToString()));
                    jobPosting.StartDate = wdcStartDate.Text.Trim() == "" ? "" : Convert.ToDateTime(wdcStartDate.Value).ToString("MM/dd/yyyy");

                    if (!string.IsNullOrEmpty(wdcClosingDate.Text) && wdcClosingDate.Text != "Null")
                    {
                        jobPosting.FinalHiredDate = DateTime.Parse(wdcClosingDate.Text);
                    }
                    else
                        jobPosting.FinalHiredDate = DateTime.MinValue;

                    jobPosting.UpdatorId = CurrentMember.Id;
                    Facade.UpdateJobPosting_OnlyDate(jobPosting);
                }
                if (HFStatus.Value == "Cancel")
                {
                    int candidatecount = 0;
                    candidatecount = Facade.GetCountOfHiringMatrixByStatusAndJobPostingId(Convert.ToInt32(HFJID.Value.ToString()));
                    if (candidatecount > 0)
                    {
                        string candidateId = Facade.GetCandidateIdOfHiringMatrixByStatusAndJobPostingId(Convert.ToInt32(HFJID.Value.ToString()));
                        RemoveCandidateFromHiringMatrix(candidateId.ToString());
                    }
                }
                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "CloseModelWindow", "parent.CloseModal(); parent.Reload(); window.location.href='" + Server.MapPath("~/Requisition/MasterRequisitionList.aspx") + "';", true);
            }
        }      
        private string GetFullRootUrl()
        {
            System.Web.HttpRequest request = System.Web.HttpContext.Current.Request;
            return request.Url.Scheme + "://" + request.Url.Authority + request.ApplicationPath + "/Login.aspx";
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
        }
        public void RemoveCandidateFromHiringMatrix(string candidateids)
        {
            string[] Ids = candidateids.TrimEnd(',').Split(new string[] { "," }, StringSplitOptions.None);
            int jobPostingid = CurrentJobPostingId;
            string MemberCurrentHiringMatrixLevel = string.Empty;
            //HiringMatrixLevels hmlevel = Facade.HiringMatrixLevel_GetInitialLevel();
            foreach (string id in Ids)
            {
                MemberCurrentHiringMatrixLevel = Facade.GetCandidateStatusByMemberIdAndJobPostingId(Convert.ToInt32(id), jobPostingid);
                //if (hmlevel.Name != MemberCurrentHiringMatrixLevel)
                if (MemberCurrentHiringMatrixLevel != null || MemberCurrentHiringMatrixLevel != "")
                { 
                    MemberJobCart jobCart = Facade.GetMemberJobCartByMemberIdAndJobPostringId(Convert.ToInt32(id), jobPostingid);
                    MemberHiringDetails hiringDetail = Facade.GetMemberHiringDetailsByMemberIdAndJobPostingID(Convert.ToInt32(id), jobPostingid);
                    MemberJoiningDetail joiningDetail = Facade.GetMemberJoiningDetailsByMemberIdAndJobPostingID(Convert.ToInt32(id), jobPostingid);
                    IList<Interview> interviews = Facade.GetInterviewByMemberIdAndJobPostingID(Convert.ToInt32(id), jobPostingid);
                    if (jobCart != null)
                    {
                        Facade.DeleteMemberJobCartById(jobCart.Id);
                    }
                    if (hiringDetail != null)
                    {
                        Facade.DeleteMemberHiringDetailsByID(hiringDetail.Id);
                    }
                    if (joiningDetail != null)
                    {
                        Facade.DeleteMemberJoiningDetailsByID(joiningDetail.Id);
                    }
                    MiscUtil.EventLogForCandidateStatus(EventLogForCandidate.RemoveCandidateFromHiringMatrix, jobPostingid, id, CurrentMember.Id, "", Facade);
                }
            }
        }
    }
}
