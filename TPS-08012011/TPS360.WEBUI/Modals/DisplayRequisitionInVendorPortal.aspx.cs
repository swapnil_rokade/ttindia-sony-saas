﻿using System;
using TPS360.Common.Helper;
using System.Drawing;
using System.Web.UI.WebControls;
using TPS360.Common.BusinessEntities;
using TPS360.Web.UI.Helper;
using TPS360.Common.Shared;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Threading;
using System.Net;
using System.Net.Mail;
using System.IO;
using System.Text;
using System.Web.UI;
namespace TPS360.Web.UI
{
    public partial class DisplayRequisitionInVendorPortal : RequisitionBasePage
    {
        protected void Page_Load(object sender, EventArgs args)
        {
            if (!IsPostBack)
            {
                Label lbModalTitle = (Label)this.Master.FindControl("lbModalTitle");
                if (lbModalTitle != null) lbModalTitle.Text = CurrentJobPosting.JobTitle + " [ " + CurrentJobPosting.JobPostingCode + " ] " + "Display Requisition in Vendor Portal";

                PrepareView();
                if (CurrentJobPosting.DisplayRequisitionInVendorPortal)
                {
                    chkVendorPortal.Checked = true;
                }

                if (CurrentJobPosting.VendorList != null && CurrentJobPosting.VendorList != "")
                {
                    foreach (ListItem list in chkVendorList.Items)
                    {
                        string[] word = CurrentJobPosting.VendorList.Split(',');
                        foreach (string s in word)
                            if (list.Value == s)
                                list.Selected = true;
                    }
                }
            }            
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            string confirmValue = hdnmessage.Value;
            if (confirmValue == "Yes")
            {
                int EnableStatus = 0;
                StringBuilder vendorList = new StringBuilder();
                try
                {
                    if (chkVendorPortal.Checked == true)
                    {

                        EnableStatus = 1;
                        foreach (ListItem list in chkVendorList.Items)
                        {
                            if (list.Selected)
                            {
                                vendorList.Append(list.Value);
                                vendorList.Append(",");
                            }
                        }
                    }
                    Facade.AddJobPostingVendorDetail(CurrentJobPosting.Id, base.CurrentMember.Id, EnableStatus, Convert.ToString(vendorList.ToString().TrimEnd(',')));
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "CloseModelWindow", "parent.CloseModal(); parent.Reload(); window.location.href='" + Server.MapPath("~/Requisition/MasterRequisitionList.aspx") + "';", true);
                }
                catch (ArgumentException ex)
                {
                    //MiscUtil.ShowMessage(lblMessage, ex.Message, true);
                }
            }
       }
        public void PrepareView()
        {
            MiscUtil.PopulateCompanyByCompanyStatus(chkVendorList, CompanyStatus.Vendor, Facade);
            chkVendorList.Items.RemoveAt(0);
        }
        public JobPosting BuildJobPostingPreviewPublish(JobPosting jobPosting)
        {
         StringBuilder vendorList = new StringBuilder();
            foreach (ListItem list in chkVendorList.Items)
            {

                if (list.Selected)
                {
                    vendorList.Append(list.Value);
                    vendorList.Append(",");
                }
            }
            if (vendorList.ToString() != "")
                jobPosting.VendorList = vendorList.ToString().TrimEnd(',');
                jobPosting.DisplayRequisitionInVendorPortal = chkVendorPortal.Checked;
                jobPosting.IsJobActive = true;

                jobPosting.WorkflowApproved = true;
              
                return jobPosting;
        
        }
    }
}
