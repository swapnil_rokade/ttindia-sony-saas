﻿<%--/*******************
Start
Suraj Adsule
For Candidate Joining Form
20160804*********/--%>
<%@ Page Language="C#" MasterPageFile="~/Modals/Modal.master" AutoEventWireup="true" CodeFile="CandidateJoiningForm.aspx.cs" Inherits="Modals_CandidateJoiningForm" Title="Untitled Page" %>

<%@ Register Src="~/Controls/CandidateJoiningJobDetails.ascx" TagName="JobInformation"
    TagPrefix="ucl" %>
<asp:Content ID="cntBasicInfoEditor" ContentPlaceHolderID="cphModalMaster" Runat="Server">
    <div style="width: 750px;overflow: auto; max-height: 1050px;" id="divCandidateJoiningDetail" runat="server">
        <ucl:JobInformation ID="jobDetails" runat="server" />
    </div>
</asp:Content>

