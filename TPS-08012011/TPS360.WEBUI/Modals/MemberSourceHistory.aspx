﻿<%-- -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: MemberSourceHistory.aspx
    Description: This is the page which is used to present tab controls
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date           Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
	
	
	
	
	
-----------------------------------------------------------------------------------------------------------------------------------------       

--%>

<%@ Page Language="C#" MasterPageFile="~/Modals/Modal.master" AutoEventWireup="true"
    CodeFile="MemberSourceHistory.aspx.cs" Inherits="TPS360.Web.UI.MemberSourceHistory" EnableViewState="true" EnableEventValidation ="false"  %>

<%@ Register Src="~/Controls/MemberSourceHistory.ascx" TagName="Hire" TagPrefix="ucl" %>
<asp:Content ID="cntCandidateResumeBuilder" ContentPlaceHolderID="cphModalMaster"
    runat="Server">
    <div class="TableRow">
        <div style="  overflow: auto; width :  670px; height : 420px;  " id="divContentHiringdetails" runat="server" EnableViewState ="true">
            <ucl:Hire ID="uclHire" runat="server" />
        </div>
    </div>
  
</asp:Content>
