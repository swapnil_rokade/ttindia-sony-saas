﻿/*******************
Start
Suraj Adsule
For Candidate Joining Form
20160804*********/

using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Modals_CandidateJoiningForm : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Label lbModalTitle = (Label)this.Page.Master.FindControl("lbModalTitle");
        if (lbModalTitle != null) lbModalTitle.Text = "Candidate Joining Detail";
        if (!IsPostBack)
        {

        }
    }
}
