﻿<%@ Page Language="C#" MasterPageFile="~/Modals/Modal.master" AutoEventWireup="true"
    CodeFile="ViewOffer.aspx.cs" Inherits="Modals_ViewOffer" Title="Untitled Page" %>

<asp:Content ID="cntOfferApproval" ContentPlaceHolderID="cphModalMaster" runat="Server">
<script type="text/javascript">
    $(document).ready(function() {
    
    
    
    var data = document.getElementById('*lblDemandType').innerHTML;
 if (data == "Contract") {
     document.getElementById("*MainDiv").style.width = "800px";
     document.getElementById("*MainDiv").style.height = "400px";
        }
    });


</script>
<style>
    table#ctl00_cphModalMaster_grvSalaryTemplate tr td:nth-child(2) {text-align: right;}
</style>

    <div id="MainDiv" style="padding: 10px 10px 10px 10px; height: 530px; width: 770px;overflow:scroll;" >
        <asp:Label ID="lblMessage" runat="server" EnableViewState="false"></asp:Label>   
        <div>
        </div>
        <div style="padding: 0px 0px 0px 0px;">
            <asp:Panel ID="pnlCandidateJobDetail" runat="server">
                <asp:UpdatePanel ID="uppane" runat="server">
                    <ContentTemplate>
                        <asp:HiddenField ID="hdnBulkAction" runat="server" Value="" />
                        <asp:HiddenField ID="hfMemberId" runat="server" />
                        <asp:HiddenField ID="hfJobPostingId" runat="server" />
                        <asp:HiddenField ID="hfMemberHiringDetailsId" runat="server" Value="0" />
                        <asp:HiddenField ID="hfStatusId" runat="server" />
                        <asp:HiddenField ID="hfCurrentMemberId" runat="server" />
                        <asp:HiddenField ID="hdnSelectedMBand" runat="server" Value="0" />
                        <asp:HiddenField ID="hdnSelectedBand" runat="server" Value="0" />
                        <%--<br />
                        <br />--%>
                     <%--   <div class='noGradeSalTemp' style="text-align: center; font-family: Tahoma; font-size: 10pt;
                            color: #FF0000; padding: 12px;">
                            <asp:Label ID="Label1" runat="server"></asp:Label>
                        </div>--%>
                        <div class="TableRow">
                            <div class="FormLeftColumn" style="width:46% !important;">
                                <div style="display: none">
                                <div class="TableRow">
                                    <div class="TableFormLeble">
                                        Date of Offer:</div>
                                    <div class="TableFormContent">
                                        <asp:Label ID="lblDateOfOffer" runat="server" style="font-size: 13px;"></asp:Label>
                                    </div>
                                  </div>
                                 </div>
                                <div class="TableRow">
                                    <div class="TableFormLeble">
                                        Expected Date of Joining:
                                    </div>
                                    <div class="TableFormContent">
                                        <asp:Label ID="lblDateOfJoining" runat="server" style="font-size: 13px;"></asp:Label>
                                    </div>
                                </div>
                                <div class="TableRow">
                                    <div class="TableFormLeble">
                                        Designation :
                                    </div>
                                    <div class="TableFormContent">
                                        <asp:Label ID="lblPosition" runat="server" TabIndex="1" style="font-size: 13px;"></asp:Label>
                                    </div>
                                </div>
                                <div class="TableRow">
                                    <div class="TableFormLeble">
                                        Reporting Manager:
                                    </div>
                                    <div class="TableFormContent">
                                        <asp:Label ID="lblReportingHead" runat="server" TabIndex="1" style="font-size: 13px;"></asp:Label>
                                    </div>
                                </div>
                                
                 <div class="TableRow">
                    <div class="TableFormLeble">
                          Requisition Name :
                    </div>
                    <div class="TableFormContent">
                          <asp:Label ID="txtRequisitionName" runat="server" TabIndex="1" style="font-size: 13px;"></asp:Label>
                       <%-- <asp:TextBox ID="txtRequisitionName" runat="server" CssClass="CommonTextBox" TabIndex="1"></asp:TextBox>--%>
                    </div>
                </div>
                
                
                 <div class="TableRow">
                    <div class="TableFormLeble">
                         Project Name :
                    </div>
                    <div class="TableFormContent">
                      <asp:Label ID="txtProjectName" runat="server" TabIndex="1" style="font-size: 13px;"></asp:Label>
                       <%-- <asp:TextBox ID="txtProjectName" runat="server" CssClass="CommonTextBox" TabIndex="1"></asp:TextBox>--%>
                    </div>
                </div>        
                
                  <div class="TableRow">
                        <div class="TableFormLeble">                          
                            <asp:Label ID="lblResourceDate" runat="server" Text="Resource Date:"></asp:Label>
                        </div>                       
                        <div class="TableFormContent" style="vertical-align:bottom">                           
                            <asp:Label ID="lblResourceOnTextValue" runat="server"></asp:Label>                         
                        </div>                       
                    </div>  
                   
                            </div>
                            <div class="FormRightColumn" style="width:54% !important;">
                                <div class="TableRow" align="left">
                                    <div class="TableFormLeble">
                                        Demand Type:
                                    </div>
                                    <div class="TableFormContent">
                                        <asp:Label ID="lblDemandType" runat="server" CssClass="CommonTextBox demandType"
                                            TabIndex="3" style="font-size: 13px; width: 90px !important; display:inline-block;"/>
                                    </div>
                                    <div class="TableRow">
                                        <div class="TableFormLeble">
                                            Location:
                                        </div>
                                        <div class="TableFormContent">
                                            <asp:Label ID="lblJoiningLocation" runat="server" CssClass="CommonTextBox Location"
                                                TabIndex="4" style="font-size: 13px; width: 90px !important; display: inline-block;" />
                                        </div>
                                        <div class="TableFormValidatorContent" style="margin-left: 42%">
                                        </div>
                                    </div>
                                    <div id='hideGrade' runat="server">
                                        <div class="TableRow">
                                            <div class="TableFormLeble">
                                                Grade:
                                            </div>
                                            <div class="TableFormContent">
                                                <asp:Label ID="lblGrade" runat="server" style="font-size: 13px;"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="TableRow">
                                        <div class="TableFormLeble">
                                           <asp:Label ID="lblCTC" Text="CTC:" runat="server"></asp:Label>    
                                        </div>
                                        <div class="TableFormContent">
                                            <%-- //************************Code modify by pravin khot on 22/Feb/2016 ******add property -[rel="ValidDigit"]**********--%>
                                            <asp:Label ID="lblSal" runat="server" CssClass="CommonTextBox" rel="ValidDigit" TabIndex="2"
                                                Width="90px" onchange="fnSalaryAnnexure()" style="font-size: 13px; text-align:right;" ></asp:Label>
                                            <%--                      //***************************************END****************************************************  --%>
                                            <asp:DropDownList ID="ddlSalary" runat="server" CssClass="CommonDropDownList" Width="60px"
                                                TabIndex="3" Enabled="false">
                                                <asp:ListItem Value="4" Selected="True">Yearly</asp:ListItem>
                                                <asp:ListItem Value="3">Monthly</asp:ListItem>
                                                <asp:ListItem Value="2">Daily</asp:ListItem>
                                                <asp:ListItem Value="1">Hourly</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:Label ID ="lblCurrency" runat="server" CssClass="CommonTextBox" style="font-size: 13px; width: 20px !important; display: inline-block;">
                                            
                                            </asp:Label>
                                           
                                            <asp:Label ID="lblPayrateCurrency" runat="server" style="font-size: 13px;"></asp:Label>
                                        </div>
                                    </div>
                                
                    <div class="TableRow">
                        <div class="TableFormLeble">
                            Source:
                        </div>
                        <div class="TableFormContent">
                           <asp:TextBox ID="lblsourcedescription" runat="server" TextMode="MultiLine" ReadOnly="true" CssClass="CommonTextBox" TabIndex="1"></asp:TextBox>
                           <%-- <asp:Label ID="lblsourcedescription" runat="server" class="TableFormContent"></asp:Label>  --%>                         
                        </div>
                    </div>
                   </div>            
                 </div>
                    
               <div id='divRefInfo' runat="server">     
                  <div  class ="TableRow" align="left" >Reference Information:</div>
                    <div class="TableFormContent"  >
                       <asp:TextBox ID="txtRefInformation" runat="server" style ="width:95.4%"  TextMode="MultiLine" ReadOnly="true" CssClass="CommonTextBox" TabIndex="1"></asp:TextBox>
                  </div>
               </div>
                     
                           <%-- <br />
                            <br />
                            <br />--%>
                            <div id='IsPermanent' runat="server" class='clsPermanent'>
                                <div class="TableRow" id="hideHeader" runat="server">
                                    <div class="TabPanelHeader">
                                        Salary Components:
                                    </div>
                                </div>
                                <div style="width: 50%; margin: 0 auto;">
                                    <asp:GridView ID="grvSalaryTemplate" runat="server" AutoGenerateColumns="False" OnRowDataBound="GridView1_RowDataBound">
                                        <Columns>
                                            <asp:BoundField DataField="Heads" HeaderText="Heads" />
                                            <asp:BoundField DataField="Value" HeaderText="Value"/>                                            
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                        <!-- code modify by pravin khot on 8/March/2016 (rename label using Salary Components)------------------------>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
