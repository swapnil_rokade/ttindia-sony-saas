﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using TPS360.Web.UI;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Web.UI.Helper;
using TPS360.Common.BusinessEntities;
// Vishal Tripathy Start Date 17th August 2016.
//Description: Modal for when recruiter submmites the offer then it goes to recruitermanager.Or when Hr Head rejects the data.

public partial class Modals_OfferApproval1 : RequisitionBasePage
{
    #region Page Load Events
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            HFJID.Value = Request.QueryString["MID"].ToString(); //memberHiringDetails.Id 
            HFStatus.Value = Request.QueryString["STATUS"].ToString();
            HFID.Value = Request.QueryString["ID"].ToString();//CurrentMember.Id 
        }
        Label lbModalTitle = (Label)this.Page.Master.FindControl("lbModalTitle");
        if (lbModalTitle != null)
            lbModalTitle.Text = "Recruitment Manager";
    }
    #endregion

    #region Events
    protected void btnOk_Click(object sender, EventArgs e)
    {
       
        Facade.UpdateOfferApprovalStatus(Convert.ToInt32(HFJID.Value), Convert.ToInt32(HFID.Value), HFStatus.Value, txtComment.Text);
        //Added by pravin khot on 19/Jan/2016**********
        string AdminEmailId = string.Empty;
        int AdminMemberid = 0;
        SiteSetting siteSetting = Facade.GetSiteSettingBySettingType((int)SettingType.SiteSetting);
        if (siteSetting != null)
        {
            Hashtable siteSettingTable = ObjectEncrypter.Decrypt<Hashtable>(siteSetting.SettingConfiguration);
            AdminEmailId = siteSettingTable[DefaultSiteSetting.AdminEmail.ToString()].ToString();
            AdminMemberid = Facade.GetMemberIdByEmail(AdminEmailId);
        }
        //**********************END*************************
        if (HFStatus.Value.ToString() == "1")
        {
            Facade.SendApprovalEmail(Convert.ToInt32(HFJID.Value.ToString()), Convert.ToInt32(HFID.Value.ToString()), HFStatus.Value.ToString(), txtComment.Text, 2, AdminMemberid);
        }
        else
        {
            Facade.SendApprovalEmail(Convert.ToInt32(HFJID.Value.ToString()), Convert.ToInt32(HFID.Value.ToString()), HFStatus.Value.ToString(), txtComment.Text, 1, AdminMemberid);
            //Facade.SendApprovalEmail(Convert.ToInt32(HFID.Value.ToString()), Convert.ToInt32(HFJID.Value.ToString()), HFStatus.Value.ToString(), txtComment.Text, 5);
        }
        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "CloseModelWindow", "parent.CloseModal(); parent.Reload(); window.location.href='" + Server.MapPath("~/Requisition/OfferApproval.aspx") + "';", true);
    }
    #endregion
}
