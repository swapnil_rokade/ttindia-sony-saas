﻿<%@ Page Language="C#" MasterPageFile="~/Modals/Modal.master" AutoEventWireup="true" CodeFile="HRApproval1.aspx.cs" Inherits="Modals_HRApproval1" Title="Untitled Page" %>

<asp:Content ID="cntOfferApproval" ContentPlaceHolderID="cphModalMaster" runat="Server">
    <asp:UpdatePanel ID="upOfferApproval" runat="server">
        <ContentTemplate>
            <div style="width: 675px; height: 500px;">
                <div class="TableRow" style="text-align: left;" id="divApply" runat="server">
                    <%--<asp:Label ID="lblApplied" runat="server" Text="Requsition Approval" EnableViewState="true" CssClass=" btn btn-success"></asp:Label>--%>
                </div>
                <div class="TableRow" style="text-align: left;" id="divMsg" runat="server">
                    <asp:Label ID="lblMessage" runat="server" Text="Comments" EnableViewState="true"
                        CssClass="TabPanelHeader"></asp:Label>
                </div>
                <div class="TableRow" style="text-align: left;" id="div3" runat="server">
                </div>
                <div class="TableRow" style="text-align: left;" id="div1" runat="server">
                    <%--<textarea cols="20" rows="5" id="txtComment" style="width: 450px"></textarea>--%>
                    <asp:TextBox ID="txtComment" runat="server" TextMode="MultiLine" Width="600px" Height="150px"></asp:TextBox><br />
                    <asp:RequiredFieldValidator ID="RVComments" runat="server" ErrorMessage="Please enter comment."
                        ControlToValidate="txtComment"></asp:RequiredFieldValidator>
                </div>
                <div class="TableRow" style="text-align: left;" id="div4" runat="server">
                </div>
                <br />
                <div class="TableRow" style="text-align: left;" id="div2" runat="server">
                    <asp:Button ID="btnOk" runat="server" CssClass="btn btn-primary" Text="Ok" Width="80px"
                        OnClientClick="check();" onclick="btnOk_Click"  />
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Button ID="btnCancel" runat="server" CssClass="btn btn-primary" Text="Cancel"
                        Width="80px" OnClientClick="javascript:parent.CloseModal();" />
                    <asp:HiddenField ID="HFStatus" runat="server" />
                    <asp:HiddenField ID="HFJID" runat="server" />
                    <asp:HiddenField ID="HFID" runat="server" />
                </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    <script type="text/javascript" language="javascript">
    function check() {
        var v = document.getElementById("<%=RVComments.ClientID%>");
        ValidatorValidate(v);
        if (v.isvalid) {
            //parent.CloseModal();
            return true;
        }
        else
            return false;
    }
    </script>

</asp:Content>


