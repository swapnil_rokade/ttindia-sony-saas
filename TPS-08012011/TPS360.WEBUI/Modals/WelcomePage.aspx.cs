﻿
using System;

using TPS360.Common.Helper;
using System.Drawing;
using System.Web.UI.WebControls ;
using System.Net.Mail;
using System.Net ;
using System.Configuration;
using TPS360.Common.BusinessEntities;
using System.Collections;
using TPS360.Common.Shared;
using System.Web.Security;
namespace TPS360.Web.UI
{
    public partial class WelcomePage : EmployeeBasePage  
    {
        protected void Page_Load(object sender, EventArgs args)
        {
            if (!IsPostBack)
            {
                System.Web.UI.HtmlControls.HtmlAnchor imgClose = (System.Web.UI.HtmlControls.HtmlAnchor)this.Page.Master.FindControl("imgClose");
                imgClose.Visible =false ;
                drpTimeoutValueList.Enabled = false;
                Label lbModalTitle = (Label)this.Page.Master.FindControl("lbModalTitle");
                if (lbModalTitle != null) lbModalTitle.Text = "Welcome To Talentrackr, " + CurrentMember .FirstName +"!";
                string DomainName = GettingCommonValues.GetDomainName();//Request.ServerVariables["HTTP_HOST"];
                
                SiteSetting sitesetting = Facade.GetSiteSettingBySettingType((int)SettingType.SiteSetting);
                if (sitesetting != null)
                {
                    Hashtable config = ObjectEncrypter.Decrypt<Hashtable>(sitesetting.SettingConfiguration);
                    if (config[DefaultSiteSetting.ResumeParserDownloadLink.ToString()] != null)
                        lnkDownloadParser.Attributes.Add("href", config[DefaultSiteSetting.ResumeParserDownloadLink.ToString()].ToString());

                    txtSMTPServer.Text=config[DefaultSiteSetting.SMTP.ToString()].ToString();
                    txtPort.Text=config[DefaultSiteSetting.SMTPort.ToString()].ToString();
                    chkEnableSSL.Checked = Convert.ToBoolean(config[DefaultSiteSetting.SMTPEnableSSL.ToString()].ToString());
                    txtSMTPServer.Enabled = false;
                    txtPort.Enabled = false;
                    chkEnableSSL.Enabled = false;
                }
         
                


                }
            }

        public void ChangePassword()
        {
            if (IsValid)
            {
                try
                {
                    if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]) || CurrentMember !=null)
                    {

                        int memberId = 0;
                        if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]) )
                            memberId = Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]);
                        else memberId = CurrentMember.Id;

                        Member member = Facade.GetMemberById(memberId);

                        if (member != null && member.Id > 0)
                        {
                            MembershipUser user = Membership.GetUser(member.UserId);

                            if (user != null && user.UserName != "" && !user.IsLockedOut)
                            {
                                string memberName = user.UserName;
                                string strOldPassword = user.GetPassword();

                                if (txtNewPassword.Text.Trim() == txtConfirmPassword.Text.Trim())
                                {
                                    user.ChangePassword(strOldPassword, txtNewPassword.Text.Trim());
                                    Membership.UpdateUser(user);
                                }
                            }
                        }
                    }
                }
                catch (ArgumentException ex)
                {
                    MiscUtil.ShowMessage(lblMessage, ex.Message, true);
                }
            }
        }
        protected void btnSaveAndStart_Click(object sender, EventArgs args)
        {
            if(txtNewPassword .Text .Trim ()!=string .Empty )    ChangePassword();
            SaveSessionTimeOut();
            SaveEmailSetup();
            Facade.Employee_EmployeeFirstLoginCreate(CurrentMember.Id);
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "Close", "<script>parent.window.location.reload(false);parent.CloseModal();</script>");
        }
        private MemberExtendedInformation BuildMemberExInfo()
        {

            MemberExtendedInformation memberExInfo = Facade.GetMemberExtendedInformationByMemberId(CurrentMember.Id);
            memberExInfo.MailSetting = GetSiteSettingTable();
            memberExInfo.UpdatorId = base.CurrentMember.Id;
            return memberExInfo;
        }
        private byte[] GetSiteSettingTable()
        {

            Hashtable MailSettingTable = new Hashtable();
            MailSettingTable.Add(DefaultSiteSetting.SMTP.ToString(), txtSMTPServer.Text);
            MailSettingTable.Add(DefaultSiteSetting.SMTPUser.ToString(), txtUserEmail.Text);
            MailSettingTable.Add(DefaultSiteSetting.SMTPassword.ToString(), txtPassword.Text);
            MailSettingTable.Add(DefaultSiteSetting.SMTPort.ToString(), txtPort.Text);
            MailSettingTable.Add(DefaultSiteSetting.SMTPEnableSSL.ToString(), chkEnableSSL.Checked);
            return ObjectEncrypter.Encrypt<Hashtable>(MailSettingTable);
        }

        private void SaveEmailSetup()
        {
            MemberExtendedInformation memberExInfo = BuildMemberExInfo();
            if (memberExInfo != null)
            {
                Facade.UpdateMemberExtendedInformation(memberExInfo);
            }
        }
        private void SaveSessionTimeOut()
        {
            string DomainName = GettingCommonValues.GetDomainName();
            int val = 0;
            if (chkSessionTime.Checked)
                val = Convert.ToInt32(drpTimeoutValueList.Text);

            int IsInserted = Facade.InsertSessionTimeout(CurrentMember.PrimaryEmail, val, Session.SessionID, "TPS", DomainName);
            if (IsInserted > 0)
            {
              //  MiscUtil.ShowMessage(lblMessage, "Session Logout Time Successfully Changed", false);
            }
        }

        protected void btnTextSMTPSettings_Click(object sender, EventArgs args)
        {
            if (Send() == "1")
            {
                divTestMessage.InnerHtml = "<div class=\"alert alert-success\">SMTP details verified</div>";
            }
            else
            {
                divTestMessage.InnerHtml = "<div class=\"alert alert-error\">Unable to connect with SMTP details</div>";
            }
            txtNewPassword.Attributes.Add("value", txtNewPassword.Text.Trim());
            txtConfirmPassword.Attributes.Add("value", txtConfirmPassword.Text.Trim());
            txtPassword.Attributes.Add("value", txtPassword.Text.Trim());
        }

        public string  Send() 
        {
            try
            {

                MailMessage mailMessage = new MailMessage();
                SmtpClient sendMail = new SmtpClient();
                MailAddress addressFrom = new MailAddress(txtUserEmail.Text);
                mailMessage.From = addressFrom;
                mailMessage.To.Add("talentrackr@gmail.com");
                mailMessage.Subject = "Test Mail From" + txtUserEmail.Text;

                SmtpClient mailSender = new SmtpClient();
                string strSMTP = string.Empty;
                string strUser = string.Empty;
                string strPwd = string.Empty;
                string strssl = string.Empty;
                int intPort = 25;
                strSMTP = txtSMTPServer.Text;
                strUser = txtUserEmail.Text;
                strPwd = txtPassword.Text;
                strssl = chkEnableSSL.Checked.ToString();
                intPort = Convert.ToInt32(txtPort.Text);
                mailSender = new SmtpClient(strSMTP, intPort);
                NetworkCredential SMTPUserInfo = new NetworkCredential(strUser, strPwd);

                mailSender.UseDefaultCredentials = false;
                mailSender.Credentials = SMTPUserInfo;
                mailSender.EnableSsl = strssl == "True" ? true : false;
                mailSender.Send(mailMessage);
                return "1";
            }
            catch(Exception ex)
            {
                return ex.Data.ToString ();
            }
        }

    }
}
