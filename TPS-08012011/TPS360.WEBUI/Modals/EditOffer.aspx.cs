﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using TPS360.Web.UI;
using TPS360.Common.BusinessEntities;
using System.Collections.Generic;


public partial class Modals_EditOffer : RequisitionBasePage
{

    protected void Page_Load(object sender, EventArgs e)
    {

        if (!Page.IsPostBack)
        {

            if (Request.QueryString["MID"] != null && Request.QueryString["ID"] != null)
            {
                string Id = Request.QueryString["MID"].ToString();
                int ApprovalType = 1;
                int MemberId = Convert.ToInt32(Request.QueryString["ID"]);
                GetHiringDetails(MemberId, Convert.ToInt32(Id));
                
                DisplayOfferedSalaryTemplate(MemberId);
            }
        }

    }
    protected void grvSalaryTemplate_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //DataRowView drv = e.Row.DataItem as DataRowView;
        int i = 0;
        if (ViewState["DataRecords"] != null)
        {
            DataTable dtRecords = ViewState["DataRecords"] as DataTable;
            if (ViewState["value"] != null)
            {
                i = Convert.ToInt32(ViewState["value"]);

            }
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                TextBox txtValue = (TextBox)e.Row.FindControl("txtFormulaValue");
                txtValue.Text = dtRecords.Rows[i]["Value"].ToString();
                i++;
                ViewState["value"] = i;

            }

        }

    }

    public void DisplayOfferedSalaryTemplate(int memberid)
    {
        DataTable dt = new DataTable();
        DataRow dr = null;
        int itemCount = 0;


        List<MemberHiringDetails> alList = Facade.GetMemberHiringDetailsShowRecords(memberid);

        //dt.Columns.Add(new DataColumn("Id", typeof(string)));
        dt.Columns.Add(new DataColumn("Heads", typeof(string)));
        dt.Columns.Add(new DataColumn("Value", typeof(string)));
        dt.Columns.Add(new DataColumn("Id", typeof(string)));
        if (alList != null && alList.Count > 0)
        {
            hideHeader.Visible = true;
            for (int i = 0; i < alList.Count; i++)
            {
                dr = dt.NewRow();
                //dr["Id"] = alList[i].offertemplateId.ToString();
                dr["Heads"] = alList[i].Header.ToString();
                dr["Value"] = alList[i].Value.ToString();
                dr["Id"] = alList[i].offertemplateId.ToString();
                dt.Rows.Add(dr);
            }


        }
        else
            hideHeader.Visible = false;


        ViewState["DataRecords"] = dt;
       
        grvSalaryTemplate.DataSource = dt;
        grvSalaryTemplate.DataBind();
        //grvSalaryTemplate.Columns[0].Visible = false;

        //for (int i = 0; i < offeredTemplate.Count; i++)
        //{
        //    dr = dt.NewRow();
        //    dr["Heads"] = offeredTemplate[i].Heads.ToString();
        //    dt.Rows.Add(dr);
        //}

        //ViewState["CurrentTable"] = dt;

        //grvSalaryTemplate.DataSource = dt;
        //grvSalaryTemplate.DataBind();

        //foreach (GridViewRow row in grvSalaryTemplate.Rows)
        //{
        //    SetFormulaTextBoxValues(Convert.ToDouble(offeredTemplate[itemCount].Value), row);
        //    itemCount++;
        //}
    }



    public void SaveCalculatedSalary(int memberid)
    {
        int rowCount = 1;
        //MemberHiringDetails memberHiringDetails = Facade.GetMemberHiringDetailsByMemberIdAndJobPostingID(Convert.ToInt32(hfMemberId.Value), JobPostingId);
        //int memberHiringId = Convert.ToInt32(memberHiringDetails.Id);


        DataTable dt = ViewState["DataRecords"] as DataTable;
        int count=0;
        foreach (GridViewRow row in grvSalaryTemplate.Rows)
        {
            try
            {
               
                
                    //string gradeValue = ddlSalGrade.SelectedItem.Text.ToString();
                    int MemberId = Convert.ToInt32(Request.QueryString["ID"]);
                    string headValue = row.Cells[0].Text.ToString();
                    TextBox formula = row.Cells[1].FindControl("txtFormulaValue") as TextBox;
                    double formulaValue = Convert.ToDouble(formula.Text);
                    string  Id = dt.Rows[count]["Id"].ToString();
                   

                    count++;

                    Facade.UpdateSalaryTemplate(formulaValue,Convert.ToInt32(Id));

                    //string demandType = txtJobTitle.Text.ToString();

                    //Facade.AddOfferedSalaryTemplate(JobPostingId, memberHiringId, gradeValue, demandType, headValue, formulaValue);
              

            }
            catch (Exception ex)
            {
                throw ex;
            }
            rowCount++;
        }
    }

    void GetHiringDetails(int MemberId, int memberLoginId)
    {


        ArrayList alList = Facade.GetPagedMemberHiringDetailsShowValue(MemberId, memberLoginId);

        if (alList.Count > 0)
        {
            lblDateOfOffer.Text = Convert.ToString(alList[0]);
            lblDemandType.Text = Convert.ToString(alList[1]);
            lblDateOfJoining.Text = Convert.ToString(alList[2]);
            lblJoiningLocation.Text = Convert.ToString(alList[3]);
            lblPosition.Text = Convert.ToString(alList[4]);
            lblGrade.Text = Convert.ToString(alList[5]);
            lblSal.Text = Convert.ToString(alList[6]);
            //lblSal.Text = Convert.ToString(alList[5]);

            //lblCandidateNameValue.Text = Convert.ToString(alList[0]);
            //lblJobTitleValue.Text = Convert.ToString(alList[1]);
            //lblGradeValue.Text = Convert.ToString(alList[2]);
            //lblOfferCreatedValue.Text = Convert.ToString(alList[3]);
            //lblOverALLCTCValue.Text = Convert.ToString(alList[4]);


        }

        //Facade.GetPagedMemberHiringDetails(
        //Facade.UpdateOfferApprovalStatus(Convert.ToInt32(HFJID.Value), Convert.ToInt32(HFID.Value), HFStatus.Value, txtComment.Text);
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        //Page.ClientScript.RegisterClientScriptBlock(Type.GetType("System.String"), "ALERT", "alert('welcome');", true);




        if (Request.QueryString["ID"] != null)
        {
            SaveCalculatedSalary(Convert.ToInt32(Request.QueryString["ID"]));
            ScriptManager.RegisterStartupScript(this, this.GetType(), "test", "javascript:mute();", true);

            //SaveCalculatedSalary();
            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "CloseModelWindow", "parent.CloseModal(); parent.Reload(); window.location.href='" + Server.MapPath("~/Requisition/ShowOffer.aspx") + "';", true);


        }

            
    }
}
