﻿
using System;
using System.Web.UI.WebControls;
using TPS360.Common.Helper;
using System.Drawing;
namespace TPS360.Web.UI
{
    public partial class Interviewfeedback : RequisitionBasePage 
    {
        public string modelTitle
        {
            get;
            set;
        }
        protected void Page_Load(object sender, EventArgs args)
        {
            Label lbModalTitle = (Label)this.Page.Master.FindControl("lbModalTitle");

            lbModalTitle.Text = ucntrlInterviewFeedback.InterviewTitle + " - " + "Feedback";
        }  
    }
}
