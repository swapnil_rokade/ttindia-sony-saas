﻿<%-- -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: InternalResumeBuilder.aspx
    Description: This is the page which is used to present tab controls
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date           Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1             Oct-16-2008        Gopala Swamy         Defect id: 8994; Renamed in tabcontrol from "Job tittle & Category" to "Job tittles"
    0.2             Mar-04-2009        Shivanand            Defect #10053; Removed tab "Job Titles".
    0.3             May-26-2009         Sandeesh            Defect id : 10464 Changes made to load the tabs instantaneously
    0.4             July-01-2009       Shivanand            Defect #10464; AsyncMode is enabled for Tabs.
    0.5             Jul-24-2009           Veda              Defect id: 11072 ; Contineous Spinning occurs in the browser tab.
    0.6             Jul-30-2009           Veda              Defect id: 11132; 
    0.7             Aug-31-2009         Ranjit Kumar.I      Defect id:#11421;Desabled Trace in Page Level Script
    0.8             Apr-07-2010        Ganapati Bhat        Enhancement #12139; Removed tab "Skill Set"
-------------------------------------------------------------------------------------------------------------------------------------------       

--%>


<%@ Page Language="C#" MasterPageFile="~/Modals/Modal.master" AutoEventWireup="true" CodeFile="ReferralLink.aspx.cs" Inherits="TPS360.Web.UI.ReferralLink"  EnableViewState ="true"  %>
<asp:Content ID="cntReferralLinkBuilder" ContentPlaceHolderID="cphModalMaster"
    runat="Server">
    <script >
    
    Sys.Application.add_load(function() { 
    $('.URLTEXT').focus();
     $('.URLTEXT').select();
        $('.URLTEXT').click(function (){
           $(this).select();
        });
    });

    </script>
    <style>
    .URLTEXT
    {
    	font-weight :bold ;
    }
    </style>
     <link href="../Style/Common.css" rel ="Stylesheet" type ="text/css" />

     <div  style =" width : 655px; height : 290px; " >
    <div class ="TableRow">
    <div style="text-align:center;  padding-left: 20%; padding-top:20px;">
        <div id ="alertwarning" runat ="server" class ="alert alert-warning"   enableviewstate="true" style =" width : 60%;  ">
        Press CTRL-C to Copy

        </div>
        </div>
        <div class="TableFormContent"style="text-align: center; ">
            <asp:TextBox EnableViewState="false" ID="txtReferralLink" runat="server" CssClass="CommonTextBox URLTEXT" autocomplete="off" TextMode="MultiLine"  Rows ="3"   Width="80%" ></asp:TextBox>
            
            
        </div>
        
        <div class="TableRow" style="text-align: center;padding-top:20px;">
            <asp:Button ID="btnSubmit" CssClass="btn btn-primary" runat="server" Text="Send by Email"   />
        </div>
    </div>
    </div>
</asp:Content>
        