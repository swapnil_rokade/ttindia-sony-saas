﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Common.BusinessEntities;
using TPS360.Web.UI.Helper;
using TPS360.Common;
using System.Web.Configuration;
using System.Net.NetworkInformation;
using System.Text;
namespace TPS360.Web.UI
{
    public partial class LoginReset : BasePage
    {
        static string UsrID = "";
        string varchrDomainName = GettingCommonValues.GetDomainName();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                System.Web.UI.HtmlControls.HtmlGenericControl fordiv = (System.Web.UI.HtmlControls.HtmlGenericControl)this.Master.FindControl("SettingMenuHolder");
               fordiv.Visible=false;

                if (Request.Cookies["LoginStatus"] != null && Request.Cookies["LoginStatus"].Value != "")
                {
                   Response.Cookies["LoginStatus"].Value = "";
                }
                else
                {
                    TPS360.Web.UI.Helper.SecureUrl urlLogin = TPS360.Web.UI.Helper.UrlHelper.BuildSecureUrl(UrlConstants.LOGIN_PAGE, "");
                    Helper.Url.Redirect(urlLogin.ToString());
                }
                UsrID = Helper.Url.SecureUrl["UserID"];
            }

        }
        
        protected void btnReset_Click(object sender, EventArgs e)
        {
            if (base.CurrentUser != null)
            {
                string baseUserName = base.CurrentUser.UserName.Trim();
//                string varchrDomainName = Request.ServerVariables["HTTP_HOST"];
                if (baseUserName != "")
                {
                    Facade.EntryUserAccessAppReset(baseUserName, "TPS", varchrDomainName);

                    int allowedusers = 0;
                    TPS360SectionHandler licenseSection = WebConfigurationManager.GetSection("tps360license") as TPS360SectionHandler; ;
                    if (licenseSection != null)
                    {
                        try
                        {
                            allowedusers = Convert.ToInt32(licenseSection.Details["NoofUsersAllowed"].Value);
                        }
                        catch { }
                    }

                    int i = Facade.EntryofUserAccessApp(baseUserName, Session.SessionID, "TPS", varchrDomainName, Request.Browser.Browser, allowedusers, Request.UserHostName);
                    Facade.UpdateAspStateTempSessions(baseUserName, Session.SessionID, "TPS", varchrDomainName);
                    SecureUrl urlLogin;
                     if (i == -7)
                    {
                        urlLogin = UrlHelper.BuildSecureUrl(UrlConstants.LOGIN_PAGE, "");
                        Response.Cookies["LoginStatus"].Value = "Only one session is allowed per browser.";// "Maximum number of users reached";
                        Helper.Url.Redirect(urlLogin.ToString());
                        return;
                    }

                     else if (i == -3)
                    {
                        urlLogin = UrlHelper.BuildSecureUrl(UrlConstants.LOGIN_PAGE, "");
                        Response.Cookies["LoginStatus"].Value = "Number of concurrent users allowed by license has been met. <br> Please try again after another user logs off.";// "Maximum number of users reached";
                        Helper.Url.Redirect(urlLogin.ToString());
                    }
                    else if (i == -2)
                    {

                        urlLogin = UrlHelper.BuildSecureUrl(UrlConstants.LOGIN_RESET, "", "UserID", baseUserName);
                        Response.Cookies["LoginStatus"].Value = "User already logged in";
                        Helper.Url.Redirect(urlLogin.ToString());
                    }
                    else if (i == 0)
                    {
                        FormsAuthentication.SetAuthCookie(baseUserName, true);
                        if ((Request.Browser.Cookies))
                        {
                            //Response.Cookies["PBLOGIN"].Expires = DateTime.Now.AddDays(30);
                            //Response.Cookies["PBLOGIN"]["UNAME"] = base.CurrentUser.UserName;
                            //Response.Cookies["PBLOGIN"]["UPASS"] = "";
                        }


                // Delete the cookies
                        else if (Request.Cookies["PBLOGIN"] != null)
                        {
                            HttpCookie aCookie;
                            string cookieName;

                            cookieName = Request.Cookies["PBLOGIN"].Name;
                            aCookie = new HttpCookie(cookieName);
                            aCookie.Expires = DateTime.Now.AddDays(-1);
                            Response.Cookies.Add(aCookie);

                        }
                        Session["Loggedin"] = "Yes";
                        Response.Redirect(UrlConstants.Dashboard.DEFAULT_PAGE);
                    }
                }
            }
            else
            {
                if (Request.Cookies["PBLOGIN"] != null)
                {
                    HttpCookie aCookie;
                    string cookieName;

                    cookieName = Request.Cookies["PBLOGIN"].Name;
                    aCookie = new HttpCookie(cookieName);
                    aCookie.Expires = DateTime.Now.AddDays(-1);
                    Response.Cookies.Add(aCookie);

                }
                TPS360.Web.UI.Helper.SecureUrl urlLogin = TPS360.Web.UI.Helper.UrlHelper.BuildSecureUrl(UrlConstants.LOGIN_PAGE, "");
                Helper.Url.Redirect(urlLogin.ToString());
            }
        }
        protected void btnUser_Click(object sender, EventArgs e)
        {
            if (Request.Cookies["PBLOGIN"] != null)
            {
                HttpCookie aCookie;
                string cookieName;

                cookieName = Request.Cookies["PBLOGIN"].Name;
                aCookie = new HttpCookie(cookieName);
                aCookie.Expires = DateTime.Now.AddDays(-1);
                Response.Cookies.Add(aCookie);

            }


            TPS360.Web.UI.Helper.SecureUrl urlLogin = TPS360.Web.UI.Helper.UrlHelper.BuildSecureUrl(UrlConstants.LOGIN_PAGE, "");
            Helper.Url.Redirect(urlLogin.ToString());
        }
    }
}
