﻿<%@ Page Language="C#" MasterPageFile="~/Public.master" AutoEventWireup="true" CodeFile="Support.aspx.cs"
    Inherits="TPS360.Web.UI.Support" Title="Talentrackr - Support" %>

<asp:Content ID="cDefault" ContentPlaceHolderID="cphHomeMaster" runat="Server">
    <div class="CommonPageBox">
        <div class="CommonPageBoxHeader">
            Talentrackr - Support
        </div>
        <div class="CommonPageBoxContent">
            Send all support inquiries to <a href="mailto:support@talentrackr.com" title="support@talentrackr.com">support@talentrackr.com</a>
        </div>
    </div>
    <br />
</asp:Content>
