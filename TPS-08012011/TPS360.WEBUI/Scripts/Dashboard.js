/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName:Dashboard.js
    Description: 
    Created By:
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.         Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1             Jul-31-2009         Nagarathna.V.B       Defect id: 10851; Commented textarea in"DeleteWarning".
   -------------------------------------------------------------------------------------------------------------------------------------------
*/

function DeleteWidget(instanceId)
{
WidgetService.DeleteWidgetInstance(instanceId);
var theDiv=document.getElementById("ctl00_cphHomeMaster_WidgetPanelsLayout_WidgetContainer"+instanceId+"_Widget");
OnDeletePageComplete();
theDiv.style.display="none";
window.location.reload();
}

function DeletePage(pageId)
{
UserPageService.DeletePage(pageId,OnDeletePageComplete);
var tabButton=document.getElementById('ctl00_cphHomeMaster_'+"Tab"+pageId);
var tabList=document.getElementById('ctl00_cphHomeMaster_'+"tabList");
tabList.removeChild(tabButton);
}

function OnDeletePageComplete(arg)
{
__doPostBack('ctl00_cphHomeMaster_UpdatePanelTabAndLayout','');
}

function ChangePageLayout(pageID,newLayout)
{
UserPageService.ChangePageLayout(pageID,newLayout,OnChangePageLayoutComplete);
}

function OnChangePageLayoutComplete(arg)
{
__doPostBack('ctl00_cphHomeMaster_UpdatePanelLayout','');
}

function NewPage(newLayout)
{
UserPageService.NewPage(newLayout,OnNewPageComplete);
}

function OnNewPageComplete(arg)
{
__doPostBack('ctl00_cphHomeMaster_UpdatePanelTabAndLayout','');
}

function RenamePage(pageId,newLabel)
{
var newPageName=document.getElementById('ctl00_cphHomeMaster_'+newLabel).value;
UserPageService.RenamePage(pageId,newPageName);
location .reload(true);

}

function ChangePage(pageId)
{
UserPageService.ChangeCurrentPage(pageId,OnChangePageComplete);
}

function OnChangePageComplete(arg)
{
__doPostBack('ctl00_cphHomeMaster_UpdatePanelTabAndLayout','');
}

function $hide(id)
{
document.getElementById(id).style.display="none";
}

function pageUnload()
{}

var Utility=
{
nodisplay:function(e)
{
if(typeof e=="object")e.style.display="none";else if($get(e)!=null)$get(e).style.display="none";
},
display:function(e,inline)
{
if(typeof e=="object")e.style.display=(inline?"inline":"block");else if($get(e)!=null)$get(e).style.display=(inline?"inline":"block");
},
getContentHeight:function()
{
if(document.body&&document.body.offsetHeight)
{
return document.body.offsetHeight;
}
},
blockUI:function()
{
Utility.display('blockUI');
var blockUI=$get('blockUI');
if(blockUI!=null)
blockUI.style.height=Math.max(Utility.getContentHeight(),1000)+"px";
},
unblockUI:function()
{
Utility.nodisplay('blockUI');
}};
var LayoutPicker=
{
yesCallback:null,
noCallback:null,
type1Callback:null,
type2Callback:null,
type3Callback:null,
type4Callback:null,
_initialized:false,
clientID:null,
init:function()
{
if(LayoutPicker._initialized)return;
var hiddenHtmlTextArea=$get('LayoutPickerPopupPlaceholder');
var html=hiddenHtmlTextArea.value;
var div=document.createElement('div');
div.innerHTML=html;
document.body.appendChild(div);
LayoutPicker._initialized=true;
},
show:function(Type1Callback,Type2Callback,Type3Callback,Type4Callback,noCallback,clientID)
{
LayoutPicker.init();
Utility.blockUI();
var popup=$get('LayoutPickerPopup');
Utility.display(popup);
LayoutPicker.type1Callback=Type1Callback;
LayoutPicker.type2Callback=Type2Callback;
LayoutPicker.type3Callback=Type3Callback;
LayoutPicker.type4Callback=Type4Callback;
LayoutPicker.clientID=clientID;
LayoutPicker.noCallback=noCallback;
$addHandler($get("SelectLayoutPopup_Cancel"),'click',LayoutPicker._noHandler);
$addHandler($get("SelectLayoutPopup_Type1"),'click',LayoutPicker._type1Handler);
$addHandler($get("SelectLayoutPopup_Type2"),'click',LayoutPicker._type2Handler);
$addHandler($get("SelectLayoutPopup_Type3"),'click',LayoutPicker._type3Handler);
$addHandler($get("SelectLayoutPopup_Type4"),'click',LayoutPicker._type4Handler);
},
hide:function()
{
LayoutPicker.init();
var popup=$get('LayoutPickerPopup');
Utility.nodisplay(popup);
$clearHandlers($get('SelectLayoutPopup_Type1'));
$clearHandlers($get('SelectLayoutPopup_Type2'));
$clearHandlers($get('SelectLayoutPopup_Type3'));
$clearHandlers($get('SelectLayoutPopup_Type4'));
Utility.unblockUI();
},
_type1Handler:function()
{
LayoutPicker.hide();
LayoutPicker.type1Callback();
},
_type2Handler:function()
{
LayoutPicker.hide();
LayoutPicker.type2Callback();
},
_type3Handler:function()
{
LayoutPicker.hide();
LayoutPicker.type3Callback();
},
_type4Handler:function()
{
LayoutPicker.hide();
LayoutPicker.type4Callback();
},
_noHandler:function()
{
LayoutPicker.hide();
LayoutPicker.noCallback();
}};

var DeleteWarning=
{
yesCallback:null,
noCallback:null,
_initialized:false,
init:function()
{
if(DeleteWarning._initialized)return;
//var hiddenHtmlTextArea=$get('DeleteConfirmPopupPlaceholder'); //0.1  
//var html=hiddenHtmlTextArea.value;//0.1  
var div=document.createElement('div');
//div.innerHTML=html;//0.1  
document.body.appendChild(div);
DeleteWarning._initialized=true;},
show:function(yesCallback,noCallback)
{
DeleteWarning.init();
Utility.blockUI();
var popup=$get('DeleteConfirmPopup');
Utility.display(popup);
DeleteWarning.yesCallback=yesCallback;
DeleteWarning.noCallback=noCallback;
$addHandler($get("DeleteConfirmPopup_Yes"),'click',DeleteWarning._yesHandler);
$addHandler($get("DeleteConfirmPopup_No"),'click',DeleteWarning._noHandler);
},
hide:function()
{
DeleteWarning.init();
var popup=$get('DeleteConfirmPopup');
Utility.nodisplay(popup);
$clearHandlers($get('DeleteConfirmPopup_Yes'));
Utility.unblockUI();
},
_yesHandler:function()
{
DeleteWarning.hide();
DeleteWarning.yesCallback();
},
_noHandler:function()
{
DeleteWarning.hide();
DeleteWarning.noCallback();
}};
var DeletePageWarning=
{
yesCallback:null,
noCallback:null,
_initialized:false,
init:function()
{
if(DeletePageWarning._initialized)return;
var hiddenHtmlTextArea=$get('DeletePageConfirmPopupPlaceholder');
var html=hiddenHtmlTextArea.value;
var div=document.createElement('div');
div.innerHTML=html;
document.body.appendChild(div);
DeletePageWarning._initialized=true;
},
show:function(yesCallback,noCallback)
{
DeletePageWarning.init();
Utility.blockUI();
var popup=$get('DeletePageConfirmPopup');
Utility.display(popup);
DeletePageWarning.yesCallback=yesCallback;
DeletePageWarning.noCallback=noCallback;
$addHandler($get("DeletePageConfirmPopup_Yes"),'click',DeletePageWarning._yesHandler);
$addHandler($get("DeletePageConfirmPopup_No"),'click',DeletePageWarning._noHandler);
},
hide:function()
{
DeletePageWarning.init();
var popup=$get('DeletePageConfirmPopup');
Utility.nodisplay(popup);
$clearHandlers($get('DeletePageConfirmPopup_Yes'));
Utility.unblockUI();
},
_yesHandler:function()
{
DeletePageWarning.hide();
DeletePageWarning.yesCallback();
},
_noHandler:function()
{
DeletePageWarning.hide();
DeletePageWarning.noCallback();
}};