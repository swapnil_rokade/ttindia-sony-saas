﻿var UpdateInterval = 20;
var PixelPerInterval = 2;
var intTotPix;
var scorllerInterval;
var MenuId;

function start_scroll_right()
{
	scorllerInterval = setInterval(scroll_left, UpdateInterval);
}

function scroll_right()
{
	document.getElementById('scroller').scrollLeft -= PixelPerInterval;
}

function start_scroll_left()
{
	scorllerInterval = setInterval(scroll_right, UpdateInterval);
}

function scroll_left()
{
	document.getElementById('scroller').scrollLeft += PixelPerInterval;
}

function stop_scrolling()
{
	clearInterval(scorllerInterval);
}

///////////////////////////////////////////////////////////////////////////////

function startmain_scroll_right()
{
	scorllerInterval = setInterval(scrollmain_left, UpdateInterval);
}

function scrollmain_right()
{
	document.getElementById('maintab').scrollLeft -= PixelPerInterval;
}

function startmain_scroll_left()
{
	scorllerInterval = setInterval(scrollmain_right, UpdateInterval);
}

function scrollmain_left()
{
	document.getElementById('maintab').scrollLeft += PixelPerInterval;
}

function stopmain_scrolling()
{
	clearInterval(scorllerInterval);
}