var changeTooltipPosition = function(event) {
	  var tooltipX = event.pageX - 8;
	  var tooltipY = event.pageY + 8;
	  $('div.Gridtooltip').css({top: tooltipY, left: tooltipX});
	};
 
	var showTooltip = function(event) {
	var isIE = /*@cc_on!@*/false || testCSS('msTransform'); 
	var parwidth=0
 	if($(this).parents('td').width() ==null) parwidth = parseInt  ($(this).parent().width());
 	else parwidth = parseInt ( $(this).parents('td').width());
 	
 	  var currentwidth=parseInt ( $(this).width());
        if(isIE )currentwidth +=20;
        
	 if (currentwidth >= parwidth ) {
	 
	  
	  $('div.Gridtooltip').remove();
	  $('<div class="Gridtooltip">'+$(this).html()  +'</div>')
            .appendTo('body');
	  changeTooltipPosition(event);
	  }
	};
 
	var hideTooltip = function() {
	   $('div.Gridtooltip').remove();
	};
	function testCSS(prop) {
    return prop in document.documentElement.style;
}
Sys.Application.add_load(function() { 
    
	
 
    $('.row span, .row a, .altrow span, .altrow a').each(function () {
        var isIE = /*@cc_on!@*/false || testCSS('msTransform'); 
        var currentwidth=parseInt ( $(this).width());
        var parWidth= parseInt ( $(this).parents('td').width());
        if(isIE )currentwidth +=20;
        if (currentwidth >=parWidth ) {
                  
		  $(this).bind({
		       
		   mousemove : changeTooltipPosition,
	   mouseenter : showTooltip,
	   mouseleave: hideTooltip
	});
}
});
	 $('.overviewColumn span, .overviewColumn  a').each(function () {
	 
	   var isIE = /*@cc_on!@*/false || testCSS('msTransform'); 
        var currentwidth=parseInt ( $(this).width());
        var parWidth=  parseInt ( $(this).parent().width());
       
        if(isIE )currentwidth +=20;
        if (currentwidth  >= parWidth ) {

		  $(this).bind({ 
		   mousemove : changeTooltipPosition,
	   mouseenter : showTooltip,
	   mouseleave: hideTooltip
	});
	}
	});
       

    
});

function callToolTip()
{

 $('.ShowToolTip').each(function () {
        
        if (parseInt ( $(this).width()) >= parseInt ( $(this).parents('td').width())) {
		  $(this).bind({
		   mousemove : changeTooltipPosition,
	   mouseenter : showTooltip,
	   mouseleave: hideTooltip
	});
	}
	});
}	