var mastertabvar=new Object();
mastertabvar.baseopacity=0;
mastertabvar.browserdetect="";

function showsubmenu(masterid, id,selectedSubTab,flag)
{

    if (typeof highlighting!="undefined")
        clearInterval(highlighting);
    
    submenuobject=document.getElementById(id);
    mastertabvar.browserdetect=submenuobject.filters? "ie" : typeof submenuobject.style.MozOpacity=="string"? "mozilla" : "";

    hidesubmenus(mastertabvar[masterid]);
    submenuobject.style.display="block";
    //instantset(mastertabvar.baseopacity);
    //highlighting=setInterval("gradualfade(submenuobject)",0);
    
    if(flag)
    {
        var menuitems=document.getElementById(masterid).getElementsByTagName("li")
        
        for (var i=0; i<menuitems.length; i++)
        {
	        menuitems[i].className="";
        }
    }
    
    var element = document.getElementById(selectedSubTab);
       
    if(element != null && element != 'undefined')
    {
        element.className="subMenuSelected";
    } 
}

function hidesubmenus(submenuarray)
{
try{
    for (var i=0; i<submenuarray.length; i++)
    {
        document.getElementById(submenuarray[i]).style.display="none";
        document.getElementById(submenuarray[i]).parentNode.className="";
    }
    }
    catch (e)
    {
    }
    
}

function instantset(degree)
{
    if (mastertabvar.browserdetect=="mozilla")
    {
        submenuobject.style.MozOpacity=degree/100;
    }
    else if (mastertabvar.browserdetect=="ie")
    {
        submenuobject.filters.alpha.opacity=degree;
    }
}


function gradualfade(cur2)
{
    if (mastertabvar.browserdetect=="mozilla" && cur2.style.MozOpacity<1)
    {
        cur2.style.MozOpacity=Math.min(parseFloat(cur2.style.MozOpacity)+0.1, 0.99);
    }
    else if (mastertabvar.browserdetect=="ie" && cur2.filters.alpha.opacity<100)
    {
        cur2.filters.alpha.opacity+=10;
    }
    else if (typeof highlighting!="undefined") //fading animation over
    {
        clearInterval(highlighting);
    }
}

function initalizetab(tabid, selectedTab, selectedSubTab, navigate)
{
    //debugger;   
	mastertabvar[tabid]=new Array();
	var menuitems=document.getElementById(tabid).getElementsByTagName("li");
	
	for (var i=0; i<menuitems.length; i++)
	{	
	    var selectedRel = menuitems[i].getAttribute("rel");
	    
		if (selectedRel && selectedTab)
		{	
		    if (selectedRel == selectedTab)
		    {
		        showsubmenu(tabid, selectedTab, selectedSubTab, true);
		        menuitems[i].className="selected";
		    }
				
			menuitems[i].setAttribute("rev", tabid); //associate this submenu with main tab
			mastertabvar[tabid][mastertabvar[tabid].length] = menuitems[i].getAttribute("rel"); //store ids of submenus of tab menu
	        
//			if (menuitems[i].className=="selected")
//			{
//				showsubmenu(tabid, menuitems[i].getAttribute("rel"), false);
//			}

			menuitems[i].getElementsByTagName("a")[0].onclick = function()
			{
			    showsubmenu(this.parentNode.getAttribute("rev"), this.parentNode.getAttribute("rel"), '', true  )
			    this.parentNode.className="selected";
			}
		}		
	}
}

// Defect id 10143 starts
function initalizeOverviewTab(tabid, selectedTab, navigate)
{   
	mastertabvar[tabid] = new Array();
	var menuitems = document.getElementById(tabid).getElementsByTagName("li");
	
	for (var i=0; i<menuitems.length; i++)
	{	
	    var selectedRel = menuitems[i].getAttribute("rel");
	    
		if (selectedRel && selectedTab)
		{	
		    if (selectedRel == selectedTab)
		    {		        
		        menuitems[i].className="selected";
		    }
				
			menuitems[i].setAttribute("rev", tabid); //associate this submenu with main tab			
		}		
	}
	//debugger;	
	if(navigator.cookieEnabled)
	{
	    var strCook = get_cookie("OverPos");
        if(strCook > 0)
        {
            document.getElementById(tabid).scrollLeft += strCook;
            delete_cookie("OverPos");
        }
    }
}

/////////////////////////////////////////////////////////////////////////////////////

function startOverView_scroll_right(MenuClientId)
{	
    MenuId = MenuClientId;
	scorllerInterval = setInterval(scrollOverView_left, UpdateInterval);
}

function scrollOverView_left()
{
    //debugger;
	document.getElementById(MenuId).scrollLeft += PixelPerInterval;
	intTotPix = document.getElementById(MenuId).scrollLeft;
    if(navigator.cookieEnabled)
        set_cookie("OverPos",intTotPix,30);
}

function startOverView_scroll_left(MenuClientId)
{
    MenuId=MenuClientId;
	scorllerInterval = setInterval(scrollOverView_right, UpdateInterval);
}

function scrollOverView_right()
{
    //debugger;
	document.getElementById(MenuId).scrollLeft -= PixelPerInterval;
	intTotPix = document.getElementById(MenuId).scrollLeft;
    if(navigator.cookieEnabled)
        set_cookie("OverPos",intTotPix,30);	
}

function stopOverView_scrolling()
{
	clearInterval(scorllerInterval);
}

//////////////////////////////////////////////////////////////////////////////

function set_cookie ( cookie_name, cookie_value, lifespan_in_days)
{
    document.cookie = cookie_name + "=" + encodeURIComponent( cookie_value ) + "; max-age=" + 60 * 60 * 24 * lifespan_in_days;
}

function get_cookie ( cookie_name )
{        
    var cookie_string = document.cookie ;
    if (cookie_string.length != 0) 
    {
        var cookie_value = cookie_string.match ('(^|;)[\s]*' + cookie_name + '=([^;]*)' );
        if(cookie_value != null)
            return decodeURIComponent ( cookie_value[2] ) ;
        else
            return 0;
    }
    return 0;
}

function delete_cookie ( cookie_name)
{
    document.cookie = cookie_name + "=; max-age=0;";
}

// Defect id 10143 ends