﻿

<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ResumeUpload.aspx.cs" Inherits="TPS360.WebUI.ResumeUpload" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

  
<html xmlns="http://www.w3.org/1999/xhtml">
<head  id="Head2" runat="server">

<script runat="server">
    void ValidateUploadFile(Object source, ServerValidateEventArgs args)
    {
        args.IsValid = (fuCopyPaste.HasFile);
    }
    
    
</script>


    <title>Untitled Page</title>
</head>
<body runat ="server"  >
    <form runat ="server" >
    <asp:ScriptManager ID="script" runat ="server" ></asp:ScriptManager>
    <asp:UpdatePanel ID="upUpload" runat ="server" >
    <ContentTemplate >
    
    <asp:Label ID="lblMessage" runat ="server" ></asp:Label>
       <asp:FileUpload ID="fuCopyPaste" runat ="server" CssClass ="CommonButton"  />
                <asp:Button ID="btnUpload" runat ="server" CssClass ="CommonButton"  runat ="server" Text ="Upload" ValidationGroup ="Profile" OnClick ="btnUpload_Click" />
                </ContentTemplate>
                <Triggers >
                <asp:PostBackTrigger ControlID= "btnUpload" />
                </Triggers>
    </asp:UpdatePanel>
    
     <div class="TableRow">
            <div class="TableFormValidatorContent">
                <asp:RegularExpressionValidator ID="revfuDocument" runat="server" ErrorMessage="Invalid File Format"
                    ControlToValidate="fuCopyPaste" ValidationExpression="^.+\.((doc)|(DOC)|(txt)|(TXT)|(pdf)|(PDF)|(rtf)|(RTF)|(docx)|(DOCX))$"
                    Display="Dynamic" ValidationGroup="Profile"></asp:RegularExpressionValidator>
                <asp:CustomValidator ID="rqfCheck" runat="server" ValidationGroup="Profile" OnServerValidate="ValidateUploadFile"
                    ErrorMessage="Please Select Resume." Display="Dynamic"></asp:CustomValidator>
            </div>
        </div>
        <div class="TableFormValidatorContent" style="clear: both; color: #888888; ">
                Supported File Formats: .doc, .docx, .pdf, .rtf, .txt</div>

    </form>
</body>
</html>