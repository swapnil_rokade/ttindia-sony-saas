﻿<%@ Page Language="C#" MasterPageFile="~/CandidatePortal/CandidatePortalLeft.master" AutoEventWireup="true" CodeFile="SearchJobOpenings.aspx.cs" Inherits="TPS360.Web.UI.SearchJobOpenings" Title="Untitled Page" EnableEventValidation ="false"  %>
<%@ Register Src ="~/Controls/PagerControl.ascx" TagName ="Pager" TagPrefix ="ucl" %>

<asp:Content ID="Content3" ContentPlaceHolderID="cphCandidateMaster" Runat="Server">
<script >
function ValidateSkill(sender,args)
{
    var skills=document .getElementById ('<%= txtSkills.ClientID %>').value;
    var skillarr=skills.split(',');
    if(skillarr .length>3)
        args.IsValid=false;
    else args.IsValid=true ;
}
</script>
 <asp:HiddenField ID="hdnSearchJob" runat="server" />      
<asp:UpdatePanel ID ="Search" runat ="server" >
<ContentTemplate >

<asp:Panel ID="pnlSearchBoxBody" runat="server" CssClass="well well-small nomargin">
<script type="text/javascript" language="javascript" src="../js/AjaxVariables.js"></script>
<script type="text/javascript" language="javascript" src="../js/AjaxScript.js"></script>
    <asp:Panel ID="pnlSearchRegion" runat="server">
       <asp:Panel ID="pnlSearchBoxHeader" runat="server">
            <div class="SearchBoxContainer">
                <div class="SearchTitleContainer">
                  
                    <div class="TitleContainer">
                        Search Job Openings
                    </div>
                </div>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlSearchBoxContent" runat="server" Style="overflow: hidden;">
              <div class="TableRow spacer">
                <div class="FormLeftColumn" style="width: 50%;">
                    <div class="TableRow">
                        <div class="TableFormLeble" style="width: 40%;">
                            <asp:Label EnableViewState="false" ID="lblJobTitle" runat="server" Text="Job Title"></asp:Label>:
                        </div>
                        <div class="TableFormContent">
                            <asp:TextBox ID="txtJobTitle" runat="server" CssClass="SearchTextbox" Width="150px"></asp:TextBox>
                        </div>
                    </div>
                  <div class="TableRow">
                        <div class="TableFormLeble" style="width: 40%;">
                            <asp:Label EnableViewState="false" ID="lblSkills" runat="server" Text="Skills"></asp:Label>:
                        </div>
                        <div class="TableFormContent">
                            <asp:TextBox ID="txtSkills" runat="server" CssClass="SearchTextbox" placeholder="Separate keywords with commas" Width="150px"></asp:TextBox> 
                        </div>
                    </div>
                    
                  <div class ="TableRow">
                    <div class ="TableFormValidatorContent" style =" margin-left : 40%">
                           <asp:CustomValidator ID="cvSkills" runat ="server" ControlToValidate ="txtSkills" ClientValidationFunction ="ValidateSkill" ErrorMessage ="Enter three or fewer keywords." Display ="Dynamic" ValidationGroup ="SearchValidator" ></asp:CustomValidator>
                    </div>
                  </div>
                </div>
                <div class="FormRightColumn" style="width: 49%">
                    <div class="TableRow" style="white-space: nowrap;">
                        <div class="TableFormLeble" style="width: 40%;">
                            <asp:Label EnableViewState="false" ID="lblByLocation" runat="server" Text="Location City"></asp:Label>:
                        </div>
                        <div class="TableFormContent">
                            <asp:TextBox EnableViewState="false" ID="txtCity" CssClass="SearchTextbox" runat="server"></asp:TextBox>
                        </div>
                    </div>
                    <div class="TableRow" style="white-space: nowrap;">
                        <div class="TableFormLeble" style="width: 40%;">
                            <asp:Label EnableViewState="false" ID="lblState" runat="server" Text="State"></asp:Label>:
                        </div>
                        <div class="TableFormContent">
                            <asp:DropDownList ID="ddlState" CssClass="CommonDropDownList" AutoPostBack="false"
                                EnableViewState="true" runat="server">
                            </asp:DropDownList>
                            <asp:HiddenField ID="hdnSelectedStateId" runat="server" Value="0" />
                        </div>
                    </div>
                    <div class="TableRow" style="white-space: nowrap;">
                        <div class="TableFormLeble" style="width: 40%;">
                            <asp:Label EnableViewState="false" ID="lblCountry" runat="server" Text="Country"></asp:Label>:
                        </div>
                        <div class="TableFormContent">
                            <asp:DropDownList EnableViewState="true" ID="ddlCountry" CssClass="CommonDropDownList"
                                runat="server" AutoPostBack="false">
                            </asp:DropDownList>
                        </div>
                    </div>
                 
                </div>
                <div class="TableRow" style="text-align: center; padding-top: 5px; padding-bottom: 5px">
                    <asp:LinkButton ID="btnSearch" runat="server" Text="Search" AlternateText="Search" CssClass="btn btn-primary" OnClick ="btnSearch_Click"
                        EnableViewState="false" ValidationGroup ="SearchValidator" ><i class="icon-search icon-white"></i> Search</asp:LinkButton>
                    <asp:Button ID="btnClear" runat="server" Text="Clear" AlternateText="Clear" CssClass="btn" OnClick ="btnClear_Click"
                        EnableViewState="false"  />
                </div>
            </div>
        </asp:Panel>
        </asp:Panel> 
        </asp:Panel> 
        
        <asp:ObjectDataSource ID="odsRequisitionList" runat="server" SelectMethod="GetPagedRequisitionListForCandidatePortal"
    OnSelecting="odsRequisitionList_Selecting" TypeName="TPS360.Web.UI.JobPostingDataSource"
    SelectCountMethod="GetListCountForCandidatePortal" EnablePaging="True" SortParameterName="sortExpression">
    <SelectParameters>
        <asp:Parameter Name="memberId" />
        <asp:ControlParameter ControlID="txtJobTitle" Name="JobTitle" PropertyName="Text"
            Type="String" />
            <asp:Parameter Name ="ReqCode" DefaultValue ="" />
        <asp:ControlParameter ControlID="TxtCity" Name="City" PropertyName="Text" Type="String" />
        <asp:ControlParameter ControlID="hdnSelectedStateId" Name="StateID" PropertyName=" Value"
            Type="String" />
        <asp:ControlParameter ControlID="ddlCountry" Name="CountryID" PropertyName="SelectedValue"
            Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>


        </ContentTemplate>
</asp:UpdatePanel>

<asp:UpdatePanel ID="upcandidateList" runat ="server" >
<ContentTemplate >
<asp:HiddenField ID="hdnSortColumn" runat="server"  />
<asp:HiddenField ID="hdnSortOrder" runat="server" />
<div class="GridContainer" style =" padding-top : 5px;">
    <div style="overflow: auto; overflow-y: hidden" id="bigDiv" onscroll='SetScrollPosition()'>
        <asp:ListView ID="lsvJobPosting" runat="server" EnableViewState="true" DataKeyNames="Id"
            OnItemDataBound="lsvJobPosting_ItemDataBound" OnItemCommand="lsvJobPosting_ItemCommand"
            OnPreRender="lsvJobPosting_PreRender" DataSourceID="odsRequisitionList">
            <LayoutTemplate>
                <table id="tlbTemplate" runat="server" class="Grid" cellspacing="0" border="0">
                    <tr id="trHeadLevel" runat="server">
                        <th style="white-space: nowrap; width : 100px !important;  ">
                            <asp:LinkButton ID="btnPostedDate" runat="server" CommandName="Sort" CommandArgument="[J].[PostedDate]"
                                Width="60%" Text="Date Posted" TabIndex="2" />
                        </th>
                        <th style="white-space: nowrap; min-width: 60px">
                            <asp:LinkButton ID="btnJobTitle" runat="server" CommandName="Sort" CommandArgument="[J].[JobTitle]"
                                Width="50%" Text="Job Title" TabIndex="5" />
                        </th>
                        <th style="white-space: nowrap; min-width: 60px">
                            <asp:LinkButton ID="btnCity" runat="server" CommandName="Sort" CommandArgument="[J].[City]"
                                Width="50%" Text="Location" TabIndex="6" />
                        </th>
                        <th style="text-align: center; white-space: nowrap; width: 80px !important; " id="thAction" runat="server">
                           Apply
                        </th>
                    </tr>
                    
                    <tr id="itemPlaceholder" runat="server">
                    </tr>
                    <tr class="Pager">
                        <td colspan="4" runat="server" id="tdPager">
                            <ucl:Pager ID="pagerControl" runat="server" EnableViewState="true"  />
                        </td>
                    </tr>
                </table>
            </LayoutTemplate>
            <EmptyDataTemplate>
                <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                    <tr>
                        <td>
                            No data was returned.
                        </td>
                    </tr>
                </table>
            </EmptyDataTemplate>
            <ItemTemplate>
                <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                    <td>
                        <asp:Label ID="lblPostedDate" runat="server" Width="80px" />
                    </td>
                  
                    <td>
                        <asp:HyperLink ID="lnkJobTitle" runat="server" Target="_blank" TabIndex="8" Width="120px"></asp:HyperLink>
                    </td>
                    <td>
                        <asp:Label ID="lblCity" runat="server" Width="80px" />
                    </td>
                    <td style="text-align: center; white-space: nowrap;" runat="server" id="tdAction"
                        enableviewstate="true">
                           <asp:ImageButton ID="btnApply" SkinID="sknApplyToJobButton" runat="server" CommandName="Apply" 
                            TabIndex="16"></asp:ImageButton>
                            <asp:Label ID="lblAppy" runat ="server" Text ="Applied" ></asp:Label>
                            
                    </td>
                </tr>
            </ItemTemplate>
        </asp:ListView>
       <asp:Label ID="lblMessage" runat ="server" EnableViewState ="false"  ></asp:Label>
    </div>


</div>


                        
                        
</ContentTemplate>
</asp:UpdatePanel>

</asp:Content>

