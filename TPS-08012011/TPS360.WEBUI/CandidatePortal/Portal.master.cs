﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName:       Portal.master.cs
    Description:    This is the Master file for Portal
    Created By:      
    Created On:      
    Modification Log:
    --------------------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ---------------------------------------------------------------------------------------------------------------------------------------------
      0.1               2/Nov/2015          Prasanth            Introduced InterviewerFeedback condition
 *    0.2               1/Feb/2015          Prasanth            Introduced Version and Copyright
      0.3               2/Nov/2015          Prasanth            Introduced careerpage condition
 *    0.4               9/Feb/2016          Pravin khot         Introduced by code Break (added by pravin khot on 9/Feb/2016 using temporary other menu hide in candidate login portal)

---------------------------------------------------------------------------------------------------------------------------------------------------        
*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Web.UI.Helper;
using System.Web.Security;
using System.Web.UI.HtmlControls;
using TPS360.Providers;
using System.Text;
using System.Web.Configuration;
namespace TPS360.Web.UI
{
    public partial class Portal : BaseMasterPage
    {
        string _currentParentNode = string.Empty;
        #region Variables

        ArrayList _permittedMenuIdList;
        static string strWelcomePageName;

        #endregion

        #region Properties

        #endregion

        #region Methods

        

        private IDbConnection CreateConnection()
        {
            return new System.Data.SqlClient.SqlConnection(MiscUtil.ConnectionString);
        }

        private bool IsInPermitedMenuList(int menuId)
        {
            if (CurrentMember != null)
            {
                if (_permittedMenuIdList == null)
                {
                    _permittedMenuIdList = Facade.GetAllMemberPrivilegeIdsByMemberId(CurrentMember.Id);
                }
            }

            if ((_permittedMenuIdList != null) && (_permittedMenuIdList.Count > 0))
            {
                if (_permittedMenuIdList.Contains(menuId))
                {
                    return true;
                }
            }

            return false;
        }

        private void AttachClientScript()
        {
            SiteMapNodeCollection permittedNodeList = new SiteMapNodeCollection();

            SiteMapNode root = SiteMap.Providers["SqlSiteMap"].FindSiteMapNodeFromKey(StringHelper.Convert((int)SiteMapType.ApplicationTopMenu));

            if (root != null)
            {
                SiteMapNodeCollection nodeList = root.ChildNodes;

                if (nodeList != null && nodeList.Count > 0)
                {
                    foreach (SiteMapNode node in nodeList)
                    {
                        if (IsInPermitedMenuList(Convert.ToInt32(node["Id"])))
                        {
                            permittedNodeList.Add(node);
                        }
                    }
                }

                string script = "\n" +
                                "<script language=\"javascript\" type=\"text/javascript\">  " + "\n" +
                                "/* <![CDATA[ */                                            " + "\n" +
                                "                                                           " + "\n" +
                                "	GBL_NUMBER_OF_ITEMS = " + permittedNodeList.Count + "            " + "\n" +
                                "														    " + "\n" +
                                "/* ]]> */												    " + "\n" +
                                "</script>";

                if (!Page.ClientScript.IsClientScriptBlockRegistered("calc"))
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "calc", script);
                }
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            AttachClientScript();
            base.OnPreRender(e);
        }
        private void BuildCompanyLogo(int memberId)
        {
            if (memberId <= 0)
            {
                return;
            }

            CompanyContact contact = Facade.GetCompanyContactByMemberId(CurrentMember.Id);

            if (contact != null && contact.CompanyId > 0)
            {
                Company company = Facade.GetCompanyById(contact.CompanyId);

                if (!string.IsNullOrEmpty(company.CompanyLogo))
                {
                    string filePath = Path.Combine(UrlConstants.GetPhysicalCompanyUploadDirectory(), company.CompanyLogo);

                    if (File.Exists(filePath))
                    {
                        SecureUrl secureUrl = UrlHelper.BuildSecureUrl(UrlConstants.DRAW_MEDIA_PAGE, string.Empty, UrlConstants.PARAM_IMG_FILE, filePath, UIConstants.SHOW_THUMB, bool.TrueString);
                    }
                }
            }
        }

        private void LoggedinUserInfo()
        {
            if (CurrentMember != null)
            {
                if (CurrentMember.Id > 0)
                {
                    if (IsUserPartner || IsUserVendor || IsUserClient)
                    {
                        BuildCompanyLogo(CurrentMember.Id);
                    }
                }
            }
        }

        #endregion

        #region Events
       
        protected void Page_Load(object sender, EventArgs e)
        {
            //Code introduced by Prasanth on 1/Feb/2016 Start

            if (WebConfigurationManager.AppSettings["CopyRightsText"].ToString() != string.Empty)
            {
                lblCopyRights.Text = "Copyright © " + DateTime.Now.Year.ToString() + " " + WebConfigurationManager.AppSettings["CopyRightsText"].ToString() + "All rights reserved.";
            }
            if (WebConfigurationManager.AppSettings["VersionText"].ToString() != string.Empty)
            {
                lblVersionText.Text = WebConfigurationManager.AppSettings["VersionText"].ToString();
            }

            //******************END********************

            _currentParentNode = Helper.Url.SecureUrl[UrlConstants.PARAM_SITEMAP_PARENT_ID].ToString();
            if (Request.Url.ToString().ToLower().Contains("candidateregistration.aspx"))
            {
                A1.HRef = "~/CandidatePortal/CandidateProfile.aspx";
            }
            else if (Request.Url.ToString().ToLower().Contains("employeereferral.aspx"))
            {
                A1.HRef = "~/referral/Employeereferral.aspx";
                //Adding Employee Referral Banner Upload Feature
				LogoContainer1.Visible = true;

            }

            //Code modified by Prasanth on 2/Nov/2015  Start
            else if (Request.Url.ToString().ToLower().Contains("interviewerfeedback.aspx"))
            {
                A1.HRef = "~/Interview/InterviewerFeedback.aspx";
            }
            //**************End******************
            //Code modified by Pravin on 17/Jan/2016  Start
            else if (Request.Url.ToString().ToLower().Contains("careerpage.aspx"))
            {
                A1.HRef = "~/CandidatePortal/CareerPage.aspx";
            }
            //**************End******************

            //code commented by pravin khot on 11/Feb/2016 on integration time

            ////Line Modified by Prasanth on 2/Nov/2015 Start
            ////Adding Employee Referral Banner Upload Feature
            //if (Request.Url.ToString().ToLower().Contains("candidateregistration.aspx") || Request.Url.ToString().ToLower().Contains("employeereferral.aspx"))
            //{
            //    divHeaderBarRight.Visible = false;
            //    return;
            //}
            //**********END**************************

            //Line Modified by Prasanth on 2/Nov/2015 Start                     //Line Modified by Pravin on 17/Jan/2016 added careerpage.aspx in line
            if (Request.Url.ToString().ToLower().Contains("candidateregistration.aspx") || Request.Url.ToString().ToLower().Contains("employeereferral.aspx") || Request.Url.ToString().ToLower().Contains("interviewerfeedback.aspx") || Request.Url.ToString().ToLower().Contains("careerpage.aspx"))
            {
                divHeaderBarRight.Visible = false;
                return;
            }
            else
            {
                BuildMenu();
            }
           
            if (CurrentMember != null)
            {
                divHeaderBarRight.Visible = true;
                lblUserName.Text = CurrentMember.FirstName + " " + CurrentMember.LastName;
            }
            
           
        }

        protected void lgs_LogOut(object sender, EventArgs e)
        {
            string role = (Roles.GetRolesForUser(base.CurrentUserName)).GetValue(0).ToString();
            MiscUtil.AddActivity(role, base.CurrentMember.Id, base.CurrentMember.Id, ActivityType.Logout, Facade);
            string dmnName = GettingCommonValues.GetDomainName();
            Facade.DeleteUserAccessApp(base.CurrentUser.UserName, "TPS", dmnName);
            MembershipUser membershipUser = Membership.GetUser(base.CurrentUser.UserName, false);
            if (membershipUser != null)
            {
                Facade.UpdateAllMemberDailyReportByLoginTimeAndMemberId(membershipUser.LastLoginDate, base.CurrentMember.Id);
            }
            Response.Cache.SetExpires(DateTime.Now.AddSeconds(-1));
            Session.Abandon();
            Response.Expires = -1;
            Response.Clear(); Response.Redirect("Login.aspx");
        }
        private void BuildMenu()
        {
            if (CurrentMember == null || CurrentUser == null)
            {
                return;
            }

            SiteMapNodeCollection permittedNodeList = new SiteMapNodeCollection();

            SqlSiteMapProvider provider = SiteMap.Providers["SqlSiteMap"] as SqlSiteMapProvider;
            SiteMapNode root = provider.FindSiteMapNodeFromKey(StringHelper.Convert((int)SiteMapType.CandidatePortalMenu ));
            
            if (root != null)
            {
                SiteMapNodeCollection nodeList = root.ChildNodes;
                StringBuilder strMenu = new StringBuilder();

                if (nodeList != null && nodeList.Count > 0)
                {
                    foreach (SiteMapNode node in nodeList)
                    {
                        strMenu.Append(("<li><a href=\"../" + BuildUrl(node.Url) + "\" title=\"" + node.Description + "\">" + node.Title + "</a></li>"));
                        break;//code added by pravin khot on 9/Feb/2016 using temporary other menu hide in candidate login portal
                    }
                    liId.InnerHtml = strMenu.ToString();
                }

               
            }
        }
      
        protected string BuildUrl(string url)
        {
            string pageUrl = url;
            pageUrl = (new SecureUrl(pageUrl)).ToString();

            return pageUrl;
        }

        #endregion
    }
}