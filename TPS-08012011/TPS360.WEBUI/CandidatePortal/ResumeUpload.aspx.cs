﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using TPS360.Common;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Web.UI.Helper;
using WebChart;
using System.Configuration;
using AjaxControlToolkit;
using System.Data.SqlClient;
using System.Linq;
using System.Data;
using System.Web.Configuration;
using TPS360.Web.UI;
namespace TPS360.WebUI
{
    public partial class ResumeUpload : CandidateBasePage  
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        private void UploadDocument()
        {

            string strMessage = "";
         //   lblMessage.Text = "";
            bool boolError = false;
            string UploadedFilename = Convert.ToString(fuCopyPaste.FileName);
            string[] FileName_Split = UploadedFilename.Split('.');
            string ResumeName = FileName_Split[0] + " - Resume." + FileName_Split[FileName_Split.Length - 1];
            UploadedFilename = ResumeName;
            int _memberid = 0;
            string canid = Request.QueryString["CanId"];
            if (canid == null || canid == "0" || canid == "")
                _memberid = CurrentMember.Id;
            else _memberid = Convert.ToInt32(canid);
            string strFilePath = MiscUtil.GetMemberDocumentPath(this.Page,_memberid , UploadedFilename, "Word Resume", false);//1.6
            //if (CheckFileSize())
            {
               

                fuCopyPaste.SaveAs(strFilePath);
                if (System.IO.File.Exists(strFilePath))
                {
                    MemberDocument memberDocument = Facade.GetMemberDocumentByMemberIdTypeAndFileName(_memberid , "Word Resume", UploadedFilename);//Facade.GetMemberDocumentByMemberIdAndFileName(_memberId, UploadedFilename);//1.6
                    if (memberDocument == null)
                    {
                        MemberDocument newDoc = new MemberDocument();
                        newDoc.FileName = UploadedFilename;//1.6
                        newDoc.FileTypeLookupId = 55;
                        newDoc.Description = "";
                       
                            newDoc.MemberId = _memberid ;
                        newDoc.Title = "Word Resume";
                        if (newDoc.Title.ToString() != string.Empty)
                        {
                            Facade.AddMemberDocument(newDoc);
                            string file = GetDocumentLink(newDoc.FileName, "Word Resume");
                            ScriptManager.RegisterStartupScript(lblMessage, typeof(MiscUtil), "file", "<script>parent.UpdateUploadFile('"+ file+" ','Successfully uploaded the file');</script>", false);
                            
                           // ScriptManager.RegisterClientScriptBlock(lblMessage, typeof(MiscUtil), "File", "<script>parent.UpdateUploadFile('" + file + "','" + "Successfully uploaded the file" + "');</script>", false);

                            //divUploadResume.Style.Add("display", "none");
                            strMessage = "Successfully uploaded the file";
                        }
                    }
                    else
                    {
                        strMessage = "Already this document available";
                        ScriptManager.RegisterStartupScript(lblMessage, typeof(MiscUtil), "file", "<script>parent.UpdateUploadFile('','Already this document available');</script>", false);
                    }
                }
               
              //  MiscUtil.ShowMessage(lblMessage, strMessage + lblMessage.Text, boolError);
            }

        }

        private string GetDocumentLink(string strFileName, string strDocumenType)
        {
            int _memberid = 0;
            string canid = Request.QueryString["CanId"];
            if (canid == null || canid == "0" || canid == "")
                _memberid = CurrentMember.Id;
            else _memberid = Convert.ToInt32(canid);
            string strFilePath = MiscUtil.GetMemberDocumentPath(this.Page, _memberid, strFileName, strDocumenType, true);

            if (strFilePath != strFileName)
            {
                return "<a href=\"" + strFilePath + "\" target=\"_blank\">" + strFileName + "</a>"; // 0.1
            }
            else
            {
                return strFileName + "(File not found)";
            }
        }
        protected void btnUpload_Click(object sender, EventArgs args)
        {
            if (fuCopyPaste.HasFile)
            {
                UploadDocument();

            }
        }

    }
}
