
//Global XMLHTTP Request object
var XmlHttp;
//Creating and setting the instance of appropriate XMLHTTP Request object to a �XmlHttp� variable  

function CreateXmlHttp1()
{
	//Creating object of XMLHTTP in IE
	try
	{
		XmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
	}
	catch(e)
	{
		try
		{
			XmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
		} 
		catch(oc)
		{
			XmlHttp = null;
		}
	}
	//Creating object of XMLHTTP in Mozilla and Safari 
	if(!XmlHttp && typeof XMLHttpRequest != "undefined") 
	{
		XmlHttp = new XMLHttpRequest();
	}
}

function RequisitionOnChange(ddlRequisition,hdnSelectedRequisition)
{

    var SelectedRequisition = document.getElementById (hdnSelectedRequisition );
  var ddl=document .getElementById (ddlRequisition );
    var RequisitionSelected= ddl.options[ddl .selectedIndex].value;
    SelectedRequisition .value=RequisitionSelected ;
}

    var ddlReq='';
function RequisitionStatusOnChange(ddlStatusList,ddlRequisitionList) 
{

ddlReq =ddlRequisitionList ;

	var StatusList = document.getElementById (ddlStatusList );
    var selectedStatus= StatusList.options[StatusList .selectedIndex].value;


    var requestUrl = "../AJAXServers/JobPostingByStatus.aspx?selectedStatus=" + encodeURIComponent(selectedStatus) ;
	CreateXmlHttp1();
	if(XmlHttp)
	{
		XmlHttp.onreadystatechange = HandleResponseRequisitionByStatus;
		XmlHttp.open("GET", requestUrl,  true);
		XmlHttp.send(null);		
	}
}


function HandleResponseRequisitionByStatus()
{	
	if(XmlHttp.readyState == 4)
	{

		if(XmlHttp.status == 200)
		{			
			
			ClearAndSetRequisitionByStatusListItems(XmlHttp.responseXML.documentElement);
					
		}
		else
		{
			alert("There was a problem retrieving data from the server." );
		}
	}
}

function ClearAndSetRequisitionByStatusListItems(ReqNode)
{


    var ReqList = document.getElementById(ddlReq );
    var FistText=ReqList.options[0].text;
  if( ReqList.options.length>0)
  {
	for (var count = ReqList.options.length-1; count >-1; count--)
		ReqList.options[count] = null;
   }

	var req = ReqNode.getElementsByTagName('Requisition');
	var textValue; 
	var optionItem;
	for (var count = 0; count < req.length; count++)
	{
	    try{	
	   var Id= req[count] .getElementsByTagName('Id');
	
	   var Name= req[count] .getElementsByTagName('Name');
	   	
   		textValue = GetInnerText(Id[0]);
        textName= GetInnerText(Name[0]);
   		optionItem = new Option( textName , textValue);
		ReqList.options[ReqList.length] = optionItem;

		}
		catch (e)
		{
		
		}
	
	}
	     ReqList.options[0].text="Any" ;
	     ReqList .disabled =false;
}
