
//Global XMLHTTP Request object
var XmlHttp;
//Creating and setting the instance of appropriate XMLHTTP Request object to a �XmlHttp� variable  

function CreateXmlHttp()
{
	//Creating object of XMLHTTP in IE
	try
	{
		XmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
	}
	catch(e)
	{
		try
		{
			XmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
		} 
		catch(oc)
		{
			XmlHttp = null;
		}
	}
	//Creating object of XMLHTTP in Mozilla and Safari 
	if(!XmlHttp && typeof XMLHttpRequest != "undefined") 
	{
		XmlHttp = new XMLHttpRequest();
	}
}

    var ddlemp='';
    
    
function EmployeeOnChange(ddlEmployee,hdnSelectedEmployee)
{
    $('#' +hdnSelectedEmployee ).val($('#' + ddlEmployee ).val());
}
function TeamOnChange(ddlTeam,ddlEmployee,hdnSelectedEmployee) 
{

 $('#' +hdnSelectedEmployee ).val("0");
ddlemp =ddlEmployee ;

	var team = document.getElementById (ddlTeam );
    var selectedteam= team.options[team .selectedIndex].value;


    var requestUrl = "../AJAXServers/EmployeeTeamAjaxServer.aspx?SelectedTeamId=" + encodeURIComponent(selectedteam)  ;
	CreateXmlHttp();
	if(XmlHttp)
	{
		XmlHttp.onreadystatechange = HandleResponseEmployeeTeam;
		XmlHttp.open("GET", requestUrl,  true);
		XmlHttp.send(null);		
	}
}


function HandleResponseEmployeeTeam()
{	
	if(XmlHttp.readyState == 4)
	{

		if(XmlHttp.status == 200)
		{			
			
			ClearAndSetEmployeeTeamItems(XmlHttp.responseText);
					
		}
		else
		{
			alert("There was a problem retrieving data from the server." );
		}
	}
}

function ClearAndSetEmployeeTeamItems(EmployeeTeam)
{

     var ReqList = document.getElementById(ddlemp );
      if( ReqList.options.length>0)
      {
	    for (var count = ReqList.options.length-1; count >-1; count--)
	    {
		    ReqList.options[count] = null;
	    }
      }
   
   
    xmlDoc = $.parseXML( EmployeeTeam );
    var $xml = $( xmlDoc );
    $xml.find( "Team" ).each(function (){
        optionItem = new Option( $(this).find('Name').text() , $(this).find('Id').text());
		ReqList.options[ReqList.length] = optionItem;
    });
    
    if( ReqList.options.length==1)
	{
	     ReqList .options[0].text="No Users";
	     ReqList .disabled=true ;
	}
	else
	{
	     ReqList.options[0].text="All" ;
	     ReqList .disabled =false;
	}
	

}
