﻿using System;

namespace TPS360.Web.UI
{
    public partial class PopupMaster : BaseMasterPage
    {
        #region Variables


        #endregion

        #region Properties

        #endregion

        #region Methods
                

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Expires = -1;
            if (!IsPostBack)
            {
                
            }
        }

        #endregion
    }
}