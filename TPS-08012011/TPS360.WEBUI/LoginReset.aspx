﻿<%@ Page Language="C#" MasterPageFile="~/Public.master" AutoEventWireup="true" CodeFile="LoginReset.aspx.cs"
    Inherits="TPS360.Web.UI.LoginReset" Title="Login Reset" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphHomeMasterTitle" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphHomeMaster" runat="Server">
    <br />
    <br />
    <br />
    <div class="MidBodyBoxRow">
        <div class="MainBox" style="width: 400px; height: 10px">
            <div class="MasterBoxContainer2" style="height: 205px">
                <div class="section-header">
                    Reset User</div>
                <div class="ChildBoxContainer2">
                    <div class="BoxContainer">
                        <div runat="server" id="divReset">
                            <div class="well" style="height: 40px; margin-bottom: 0px; font-weight: bold;">
                                <center>
                                    This user is already logged in. Click 'Reset User' to reset the session, or log
                                    in as another user.</center>
                            </div>
                            <div style="width: 98%;">
                                <br />
                                <div class="TableRow">
                                    <center>
                                        <asp:Button ID="btnReset" runat="server" Text="Reset User" OnClick="btnReset_Click"
                                            ValidationGroup="Profile" CssClass="btn btn-primary" />
                                        <asp:Button ID="btnUser" runat="server" Text="Login as another User" OnClick="btnUser_Click"
                                            ValidationGroup="Profile" CssClass="btn" />
                                    </center>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
