﻿<%--
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: InterviewerFeedback.aspx
    Description: This is the InterviewerFeedback page.
    Created By: Prasanth Kumar G
    Created On: 20/Jul/2015
    Modification Log:
    --------------------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ---------------------------------------------------------------------------------------------------------------------------------------------
     
--%>

<%@ Page Language="C#" MasterPageFile="~/CandidatePortal/Portal.master" AutoEventWireup="true"
    CodeFile="InterviewerFeedback.aspx.cs" EnableEventValidation="false" Inherits="TPS360.Web.UI.InterviewerFeedback"
    Title="Interviewer Feed Back" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Controls/InterviewerFeedback.ascx" TagName="IF" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/InterviewerAssessmentForm.ascx" TagName="IA" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/ConfirmationWindow.ascx" TagName="Confirm" TagPrefix="ucl" %>
<asp:Content ID="Content2" ContentPlaceHolderID="cphHomeMasterTitle" runat="Server">
<script src="../Scripts/fixWebkit.js" type="text/javascript"></script>


<asp:Label ID="titleContainer"  runat ="server" Text ="Interviewer Feed Back" ></asp:Label>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphHomeMaster" runat="Server">
       <asp:UpdatePanel ID="pnlInterviewerFeedback" runat="server">
            <ContentTemplate>
    <div style="padding: 18px;">
     <div id="basicinfoarea">
                                <div class="basicinfotab TabPanelHeader" style="float: left; font-size: 15px;">
                                    Interview Feedback</div>
                               <uc1:IF ID="uclIF" runat="server" />
                               
                            </div>

                 <asp:Label ID="lblMessage" runat="server" EnableViewState="false" Style="z-index: 9999"></asp:Label>
                 <ucl:Confirm ID="uclConfirm" runat="server"></ucl:Confirm>
              <div class="section-body">
                        <div class="section-body" data-target="#reqNavScroll" data-spy="scroll">
                           
                            <div id="descriptionarea">
                                <br />
                                <br />
                                <br />
                                <div class="basicinfotab TabPanelHeader" style="float: left; font-size: 15px">
                                    Assessment</div>
                               <uc1:IA ID="uclIA" runat ="server" />
                            </div>
                           </div>
               </div>
            
    
            
            
        <div class="section-body">
            <div class="section-body" data-target="#reqNavScroll" data-spy="scroll" >
                <asp:Button ID="btnSubmit" CssClass="btn btn-primary" runat="server" Text="Submit"  OnClick="btnSubmit_Click"  ValidationGroup ="NoteValidation"/>
            </div>
        </div>
          </div>
 </ContentTemplate>
       <Triggers>
        <%--<asp:PostBackTrigger ControlID="btnSubmit" />--%>
    </Triggers>
    </asp:UpdatePanel>           
            
 

  
</asp:Content>
