﻿
/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: InterviewerfeedbackDetail.ascx.cs
    Description: This is the user control page used for member interviewer feedback details show
    Created By: pravin khot
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
 */
using System;
using System.Web.UI.WebControls;
using TPS360.Common.Helper;
using System.Drawing;
namespace TPS360.Web.UI
{
    public partial class InterviewerfeedbackDetail : RequisitionBasePage 
    {
        public string modelTitle
        {
            get;
            set;
        }
        protected void Page_Load(object sender, EventArgs args)
        {
            Label lbModalTitle = (Label)this.Page.Master.FindControl("lbModalTitle");

            lbModalTitle.Text = ucntrlInterviewerfeedbackDetail.InterviewTitle + " - " + "Feedback";
        }  
    }
}
