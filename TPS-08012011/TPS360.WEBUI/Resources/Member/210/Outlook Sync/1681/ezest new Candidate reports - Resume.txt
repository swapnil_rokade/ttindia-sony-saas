set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[Candidate_GetPagedReport]
	@StartRowIndex	int,
	@RowPerPage		int,
	@WhereClause	nvarchar(max),
	@SortColumn		nvarchar(256),
	@SortOrder		nvarchar(4),
	@SQL ntext=''
--WITH ENCRYPTION
AS
BEGIN 

SET @StartRowIndex = isnull(@StartRowIndex, -1)
SET @RowPerPage = isnull(@RowPerPage, -1)
SET @WhereClause = isnull(@WhereClause, '')
SET @SortColumn = isnull(@SortColumn, '')
SET @SortOrder = isnull(@SortOrder, '')

--DECLARE @SQL ntext
DECLARE @PagingClause nvarchar(512)

IF (@WhereClause != '')
BEGIN
	SET @WhereClause = 'WHERE ' + char(13) + @WhereClause
	SET @WhereClause = @WhereClause+ char(13) +'AND [C].[IsRemoved]=0'
END
ELSE
BEGIN
	SET @WhereClause = 'WHERE [C].[IsRemoved]=0'
END

IF (@SortColumn != '')
BEGIN
    if(@SortColumn='[ME].[TotalExperienceYears]')
		Set @SortColumn='cast((case when [C].[TotalExperienceYears]='''' then ''-1'' else  isnull([C].[TotalExperienceYears],''-1'') end) as  decimal(15,3)) '
	SET @SortColumn = 'ORDER BY ' + @SortColumn

	IF (@SortOrder != '')
	BEGIN
		SET @SortColumn = @SortColumn + ' ' + @SortOrder
	END
END
ELSE
BEGIN
	SET @SortColumn = @SortColumn + ' ORDER BY [C].[FirstName] ASC'
END

IF ((@RowPerPage > -1) AND (@StartRowIndex > -1))
BEGIN		
	SET @PagingClause = 'WHERE Row between '+ CONVERT(nvarchar(10), @StartRowIndex+1) +'And ('+ CONVERT(nvarchar(10), @StartRowIndex) +' + '+ CONVERT(nvarchar(10), @RowPerPage) +')'
END

DECLARE @SQL1 nvarchar(max)
DECLARE @SQL2 nvarchar(max)
DECLARE @SQL3 nvarchar(max)
DECLARE @SQL11 nvarchar(max)
SET @SQL1 = 'WITH CandidateEntries AS (
						SELECT ROW_NUMBER() OVER ('+ @SortColumn +')AS Row,											
						[C].[Id],
						[C].[FirstName],
						[C].[MiddleName],
						[C].[LastName],
						[C].[PrimaryEmail],
						[C].[PrimaryPhone],
						[C].[PrimaryPhoneExtension],
						[C].[CellPhone],
						[C].[Status],
						[C].[HomePhone],
						[C].[PermanentCity],
						[S].[Name] AS [StateName],
						[S].[StateCode],
						'''' AS [OfficeCity],
						[ME].[CurrentPosition],
						[ME].[Remarks],
						[MOS].[Objective],
						[MOS].[Summary],
						[dbo].GetMemberSkill([C].[Id]) [SkillSet],
						[C].[UserId],
						[C].[CreatorId],
						[C].[UpdatorId],
						[C].[CreateDate],
						[C].[UpdateDate],
						''Report'' AS RecordType,
						[C].[NickName],
						[C].[DateOfBirth],
						[C].[PermanentAddressLine1],
						[C].[PermanentAddressLine2],
						[C].[CurrentCity],
						[C].[PermanentStateId],
						[SP].[Name] AS [PermanentStateName],
						[SP].[StateCode] AS [PermanentStateCode],
						[C].[PermanentZip],
						[C].[PermanentCountryId],
						[CP].[Name] AS [PermanentCountryName],
						[CP].[CountryCode] AS [PermanentCountryCode],
						[C].[PermanentPhone],
						[C].[PermanentPhoneExt],
						[C].[PermanentMobile],
						[C].[AlternateEmail],
						[C].[ResumeSource],
						[C].[CurrentAddressLine1],
						[C].[CurrentAddressLine2],
						[C].[CurrentStateId],
						[SCU].[Name] AS [CurrentStateName],
						[SCU].[StateCode] AS [CurrentStateCode],
						[C].[CurrentZip],
						[C].[CurrentCountryId],
						[CCU].[Name] AS [CurrentCountryName],
						[CCU].[CountryCode] AS [CurrentCountryCode],
						[C].[OfficePhone],
						[C].[OfficePhoneExtension],
						[C].[CityOfBirth],
						[C].[CountryIdOfBirth],
						[CCB].[Name] AS [BirthCountryName],
						[CCB].[CountryCode] AS [BirthCountryCode],
						[C].[CountryIdOfCitizenship],
						[CCC].[Name] AS [CitizenshipCountryName],
						[CCC].[CountryCode] AS [CitizenshipCountryCode],
						[C].[GenderLookupId],
						[GG].[Name] AS [Gender],
						[C].[EthnicGroupLookupId],
						[GEG].[Name] AS [EthnicGroup],
						[C].[BloodGroupLookupId],
						[GBG].[Name] AS [BloodGroup],
						[C].[MaritalStatusLookupId],
						[GMS].[Name] AS [MaritalStatus],
						[ME].[Relocation],
						[ME].[LastEmployer],
						[ME].[TotalExperienceYears],
						[ME].[Availability],
						[GA].[Name] AS [AvailabilityText],
						[ME].[AvailableDate],
						[ME].[CurrentYearlyRate],
						[ME].[CurrentYearlyCurrencyLookupId],
						[GCY].[Name] AS [CurrentYearlyCurrency],
						[ME].[CurrentMonthlyRate],
						[ME].[CurrentMonthlyCurrencyLookupId],
						[GCM].[Name] AS [CurrentMonthlyCurrency],
						[ME].[CurrentHourlyRate],
						[ME].[CurrentHourlyCurrencyLookupId],
						[GCH].[Name] AS [CurrentHourlyCurrency],
						[ME].[ExpectedYearlyRate],
						[ME].[ExpectedYearlyCurrencyLookupId],
						[GCEY].[Name] AS [ExpectedYearlyCurrency],
						[ME].[ExpectedMonthlyRate],
						[ME].[ExpectedMonthlyCurrencyLookupId],
						[GCEM].[Name] AS [ExpectedMonthlyCurrency],
						[ME].[ExpectedHourlyRate],
						[ME].[ExpectedHourlyCurrencyLookupId],
						[GCEH].[Name] AS [ExpectedHourlyCurrency],
						[ME].[WillingToTravel],
						[ME].[SalaryTypeLookupId],
						[GST].[Name] AS [SalaryType],
						[ME].[JobTypeLookupId],
						[GJT].[Name] AS [JobType],
						[ME].[SecurityClearance],
						[ME].[WorkAuthorizationLookupId],
						[GWA].[Name] AS [WorkAuthorization],
						[MC].[FirstName]+'' ''+[MC].[LastName] AS [Creator],
						[MU].[FirstName]+'' ''+[MU].[LastName] AS [Updator],
						[C].[WorkScheduleLookUpId]  ,
						[GWS].[Name] AS [WorkSchedule],
						'
				SET @SQL11='[ME].[Website],
						[ME].[LinkedinProfile],
						[ME].[PassportStatus],
						[GID].[Name] AS [IDCard],
						[ME].[IdCardDetail],
						dbo.MemberEducationGetHighestDegreeNameByMemberId(C.ID) AS [HighestEducation],
						[GS].[Id] as sourceID,
						[GS].[Name] AS [Source],
						[ME].[SourceDescription]
				'
				SET @SQL2 = 	' FROM 
						[Candidate] as [C]
						LEFT JOIN [dbo].[State] [S] ON [C].[CurrentStateId]=[S].[Id]
--                        LEFT JOIN [dbo].[MemberEducation] [MEd] ON [C].[Id]=[MEd].[MemberId]
						LEFT JOIN [dbo].[MemberExtendedInformation] [ME] ON [C].[Id]=[ME].[MemberId]
						LEFT JOIN [dbo].[MemberObjectiveAndSummary] [MOS] ON [C].[Id]=[MOS].[MemberId]
						LEFT JOIN [dbo].[State] [SP] ON [C].[PermanentStateId]=[SP].[Id]
						LEFT JOIN [dbo].[Country] [CP] ON [C].[PermanentCountryId]=[CP].[Id]
						LEFT JOIN [dbo].[State] [SCU] ON [C].[CurrentStateId]=[SCU].[Id]
						LEFT JOIN [dbo].[Country] [CCU] ON [C].[CurrentCountryId]=[CCU].[Id]
						LEFT JOIN [dbo].[Country] [CCB] ON [C].[CountryIdOfBirth]=[CCB].[Id]
						LEFT JOIN [dbo].[Country] [CCC] ON [C].[CountryIdOfCitizenship]=[CCC].[Id]
						LEFT JOIN [dbo].[GenericLookup] [GG] ON [C].[GenderLookupId]=[GG].[Id]
						LEFT JOIN [dbo].[GenericLookup] [GEG] ON [C].[EthnicGroupLookupId]=[GEG].[Id]
						LEFT JOIN [dbo].[GenericLookup] [GBG] ON [C].[BloodGroupLookupId]=[GBG].[Id]
						LEFT JOIN [dbo].[GenericLookup] [GMS] ON [C].[MaritalStatusLookupId]=[GMS].[Id]
						LEFT JOIN [dbo].[GenericLookup] [GA] ON [ME].[Availability]=[GA].[Id]
						LEFT JOIN [dbo].[GenericLookup] [GCY] ON [ME].[CurrentYearlyCurrencyLookupId]=[GCY].[Id]
						LEFT JOIN [dbo].[GenericLookup] [GCM] ON [ME].[CurrentMonthlyCurrencyLookupId]=[GCM].[Id]
						LEFT JOIN [dbo].[GenericLookup] [GCH] ON [ME].[CurrentHourlyCurrencyLookupId]=[GCH].[Id]
						LEFT JOIN [dbo].[GenericLookup] [GCEY] ON [ME].[ExpectedYearlyCurrencyLookupId]=[GCEY].[Id]
						LEFT JOIN [dbo].[GenericLookup] [GCEM] ON [ME].[ExpectedMonthlyCurrencyLookupId]=[GCEM].[Id]
						LEFT JOIN [dbo].[GenericLookup] [GCEH] ON [ME].[ExpectedHourlyCurrencyLookupId]=[GCEH].[Id]
						LEFT JOIN [dbo].[GenericLookup] [GST] ON [ME].[SalaryTypeLookupId]=[GST].[Id]
						LEFT JOIN [dbo].[GenericLookup] [GJT] ON [ME].[JobTypeLookupId]=[GJT].[Id]
						LEFT JOIN [dbo].[GenericLookup] [GWA] ON [ME].[WorkAuthorizationLookupId]=[GWA].[Id]
						LEFT JOIN [dbo].[GenericLookup] [GWS] ON [ME].[WorkScheduleLookupId]=[GWS].[Id]
--						LEFT JOIN [dbo].[GenericLookup] [GHE] ON [MEd].[LevelOfEducationLookupId]=[GHE].[Id]
						LEFT JOIN [dbo].[Member] [MC] ON [C].[CreatorId]=[MC].[Id]
						LEFT JOIN [dbo].[Member] [MU] ON [C].[UpdatorId]=[MU].[Id]
						LEFT JOIN [dbo].[GenericLookup] [GID] ON [ME].[IdCardLookUpId]=[GID].[Id]
						LEFT JOIN [dbo].[GenericLookup] [GS] ON [ME].[SourceLookupId]=[GS].[Id]
				'+ @WhereClause +')'
		SET @SQL3 = 	'SELECT 
						[Id],
						[FirstName],
						[MiddleName],
						[LastName],
						[PrimaryEmail],
						[PrimaryPhone],
						[PrimaryPhoneExtension],
						[CellPhone],
						[Status],
						[HomePhone],
						[PermanentCity],
						[StateName],
						[StateCode],
						[OfficeCity],
						[CurrentPosition],
						[Remarks],
						[Objective],
						[Summary],
						[SkillSet],
						[UserId],
						[CreatorId],
						[UpdatorId],
						[CreateDate],
						[UpdateDate],
						[RecordType],
						[NickName],
						[DateOfBirth],
						[PermanentAddressLine1],
						[PermanentAddressLine2],
						[CurrentCity],
						[PermanentStateId],
						[PermanentStateName],
						[PermanentStateCode],
						[PermanentZip],
						[PermanentCountryId],
						[PermanentCountryName],
						[PermanentCountryCode],
						[PermanentPhone],
						[PermanentPhoneExt],
						[PermanentMobile],
						[AlternateEmail],
						[ResumeSource],
						[CurrentAddressLine1],
						[CurrentAddressLine2],
						[CurrentStateId],
						[CurrentStateName],
						[CurrentStateCode],
						[CurrentZip],
						[CurrentCountryId],
						[CurrentCountryName],
						[CurrentCountryCode],
						[OfficePhone],
						[OfficePhoneExtension],
						[CityOfBirth],
						[CountryIdOfBirth],
						[BirthCountryName],
						[BirthCountryCode],
						[CountryIdOfCitizenship],
						[CitizenshipCountryName],
						[CitizenshipCountryCode],
						[GenderLookupId],
						[Gender],
						[EthnicGroupLookupId],
						[EthnicGroup],
						[BloodGroupLookupId],
						[BloodGroup],
						[MaritalStatusLookupId],
						[MaritalStatus],
						[Relocation],
						[LastEmployer],
						[TotalExperienceYears],
						[Availability],
						[AvailabilityText],
						[AvailableDate],
						[CurrentYearlyRate],
						[CurrentYearlyCurrencyLookupId],
						[CurrentYearlyCurrency],
						[CurrentMonthlyRate],
						[CurrentMonthlyCurrencyLookupId],
						[CurrentMonthlyCurrency],
						[CurrentHourlyRate],
						[CurrentHourlyCurrencyLookupId],
						[CurrentHourlyCurrency],
						[ExpectedYearlyRate],
						[ExpectedYearlyCurrencyLookupId],
						[ExpectedYearlyCurrency],
						[ExpectedMonthlyRate],
						[ExpectedMonthlyCurrencyLookupId],
						[ExpectedMonthlyCurrency],
						[ExpectedHourlyRate],
						[ExpectedHourlyCurrencyLookupId],
						[ExpectedHourlyCurrency],
						[WillingToTravel],
						[SalaryTypeLookupId],
						[SalaryType],
						[JobTypeLookupId],
						[JobType],
						[SecurityClearance],
						[WorkAuthorizationLookupId],
						[WorkAuthorization],
						[Creator],
						[Updator],
						[WorkScheduleLookupId],
						[WorkSchedule],
						[Website],
						[LinkedinProfile],
						[PassportStatus],
						[IDCard],
						[IdCardDetail],
						[HighestEducation],
						[Source],
						dbo.GetCandidateSourceDescriptionTextBySourceAndSourceDesc(sourceID,[SourceDescription])
				FROM 
						CandidateEntries
					' + ISNULL(@PagingClause, '') + ''

SET @SQL=@SQL1+@SQL11+@SQL2+@SQL3
print @SQL1+@SQL11+@SQL2+@SQL3
--print @SQL
EXEC sp_executesql @SQL

SET @SQL = 'SELECT COUNT(DISTINCT [C].[Id])
			FROM 
			[Candidate] as [C]
			LEFT JOIN [dbo].[State] [S] ON [C].[CurrentStateId]=[S].[Id]
            LEFT JOIN [dbo].[MemberEducation] [MEd] ON [C].[Id]=[MEd].[MemberId]
			LEFT JOIN [dbo].[MemberExtendedInformation] [ME] ON [C].[Id]=[ME].[MemberId]
			LEFT JOIN [dbo].[MemberObjectiveAndSummary] [MOS] ON [C].[Id]=[MOS].[MemberId]
			LEFT JOIN [dbo].[State] [SP] ON [C].[PermanentStateId]=[SP].[Id]
			LEFT JOIN [dbo].[Country] [CP] ON [C].[PermanentCountryId]=[CP].[Id]
			LEFT JOIN [dbo].[State] [SCU] ON [C].[CurrentStateId]=[SCU].[Id]
			LEFT JOIN [dbo].[Country] [CCU] ON [C].[CurrentCountryId]=[CCU].[Id]
			LEFT JOIN [dbo].[Country] [CCB] ON [C].[CountryIdOfBirth]=[CCB].[Id]
			LEFT JOIN [dbo].[Country] [CCC] ON [C].[CountryIdOfCitizenship]=[CCC].[Id]
			LEFT JOIN [dbo].[GenericLookup] [GG] ON [C].[GenderLookupId]=[GG].[Id]
			LEFT JOIN [dbo].[GenericLookup] [GEG] ON [C].[EthnicGroupLookupId]=[GEG].[Id]
			LEFT JOIN [dbo].[GenericLookup] [GBG] ON [C].[BloodGroupLookupId]=[GBG].[Id]
			LEFT JOIN [dbo].[GenericLookup] [GMS] ON [C].[MaritalStatusLookupId]=[GMS].[Id]
			LEFT JOIN [dbo].[GenericLookup] [GA] ON [ME].[Availability]=[GA].[Id]
			LEFT JOIN [dbo].[GenericLookup] [GCY] ON [ME].[CurrentYearlyCurrencyLookupId]=[GCY].[Id]
			LEFT JOIN [dbo].[GenericLookup] [GCM] ON [ME].[CurrentMonthlyCurrencyLookupId]=[GCM].[Id]
			LEFT JOIN [dbo].[GenericLookup] [GCH] ON [ME].[CurrentHourlyCurrencyLookupId]=[GCH].[Id]
			LEFT JOIN [dbo].[GenericLookup] [GCEY] ON [ME].[ExpectedYearlyCurrencyLookupId]=[GCEY].[Id]
			LEFT JOIN [dbo].[GenericLookup] [GCEM] ON [ME].[ExpectedMonthlyCurrencyLookupId]=[GCEM].[Id]
			LEFT JOIN [dbo].[GenericLookup] [GCEH] ON [ME].[ExpectedHourlyCurrencyLookupId]=[GCEH].[Id]
			LEFT JOIN [dbo].[GenericLookup] [GST] ON [ME].[SalaryTypeLookupId]=[GST].[Id]
			LEFT JOIN [dbo].[GenericLookup] [GJT] ON [ME].[JobTypeLookupId]=[GJT].[Id]
			LEFT JOIN [dbo].[GenericLookup] [GWA] ON [ME].[WorkAuthorizationLookupId]=[GWA].[Id]
			LEFT JOIN [dbo].[Member] [MC] ON [C].[CreatorId]=[MC].[Id]
			LEFT JOIN [dbo].[Member] [MU] ON [C].[UpdatorId]=[MU].[Id]
			LEFT JOIN [dbo].[GenericLookup] [GID] ON [ME].[IdCardLookUpId]=[GID].[Id]
            LEFT JOIN [dbo].[GenericLookup] [GHE] ON [MEd].[LevelOfEducationLookupId]=[GHE].[Id]
--			LEFT JOIN [dbo].[GenericLookup] [GHE] ON [ME].[HighestEducationLookupId]=[GHE].[Id]
			LEFT JOIN [dbo].[GenericLookup] [GS] ON [ME].[SourceLookupId]=[GS].[Id]
				' + @WhereClause

EXEC sp_executesql @SQL

END











