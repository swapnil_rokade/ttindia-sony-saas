GO
print 'Script 1 Start'
GO 

/****** By PRAVIN KHOT on 16/Nov/2016  - insert values Requisition into RequisitionApprovalEmailNotification  
***/   
 INSERT INTO RequisitionApprovalEmailNotification VALUES('Resource Demand For Approved| [REQ CODE]:[JOB TITLE]','Dear [DIVISION HEAD],  Demand [REQ CODE] has been approved by [APPROVER] ([GID]) and his comment is [COMMENT]  Regards,  [TOWER HEAD]   *** This is an auto-generated mail. Please do not reply***','Department Head Approved')
 INSERT INTO RequisitionApprovalEmailNotification VALUES('Resource Demand For Pushed back| [REQ CODE]:[JOB TITLE]','Dear [CREATOR NAME],  Demand [REQ CODE] has been pushed back by [APPROVER] ([GID]) and his comment is [COMMENT]  Regards,  [TOWER HEAD]  *** This is an auto-generated mail. Please do not reply***','Department Head Rejected')
  
GO
print 'Script 1 End'
GO