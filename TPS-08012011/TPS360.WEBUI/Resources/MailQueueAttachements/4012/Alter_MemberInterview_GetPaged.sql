ALTER PROCEDURE [dbo].[MemberInterview_GetPaged]
	@StartRowIndex	int,
	@RowPerPage		int,
	@WhereClause	nvarchar(4000),
	@SortColumn		nvarchar(128),
	@SortOrder		nvarchar(4)
--WITH ENCRYPTION
AS
BEGIN 

SET @StartRowIndex = isnull(@StartRowIndex, -1)
SET @RowPerPage = isnull(@RowPerPage, -1)
SET @WhereClause = isnull(@WhereClause, '')
SET @SortColumn = isnull(@SortColumn, '')
SET @SortOrder = isnull(@SortOrder, '')

DECLARE @SQL nvarchar(4000)

IF (@WhereClause != '')
BEGIN
	SET @WhereClause = 'WHERE ' + char(13) + @WhereClause
	SET @WhereClause = @WhereClause+ char(13) +'AND [I].[IsRemoved]=0'
END
ELSE
BEGIN
	SET @WhereClause = 'WHERE [I].[IsRemoved]=0'
END

IF (@SortColumn != '')
BEGIN
	SET @SortColumn = 'ORDER BY ' + @SortColumn

	IF (@SortOrder != '')
	BEGIN
		SET @SortColumn = @SortColumn + ' ' + @SortOrder
	END
END
ELSE
BEGIN
	SET @SortColumn = @SortColumn + ' ORDER BY [I].[CreateDate] DESC'
END

SET @SQL = 'WITH MemberInterviewEntries AS (
						SELECT ROW_NUMBER() OVER ('+ @SortColumn +')AS Row,											
							[I].[Id]
							FROM [dbo].[Interview] [I] LEFT JOIN
							[JobPosting] [JP] ON [I].[JobPostingId]=[JP].[Id] LEFT JOIN
							[Company] [C] ON [I].[ClientId]=[C].[Id] LEFT JOIN
							[GenericLookup] [GL] ON [I].[TypeLookupId]=[GL].[Id] left join
							[InterviewersNameView] [INV] on [I].[Id]=[INV].[Id]
							
				'+ @WhereClause +'
				)
				SELECT 
					[I].[Id],
							[I].[Location],
							[I].[Remark],
							[I].[MemberId],
                            [I].[TypeLookupId],
							[I].[IsRemoved],
							[I].[CreatorId],
							[I].[UpdatorId],
							[I].[CreateDate],
							[I].[UpdateDate],
							[I].[Title],
							[JP].[Id],
							[JP].[JobTitle],
							[JP].[JobPostingCode],
							[C].[Id],
							--[C].[CompanyName],
							[I].[Division],
							[GL].[Name],
							Dbo.GetInterviwerNameById(Dbo.GetInterviewerIdByInterviewId([I].[Id]))AS [InterviewersName],							
							[I].[StartDateTime],
							[I].[Duration],
							[I].[ActivityId],
							[I].[AllDayEvent],
							[I].[EnableReminder],
							[I].[ReminderInterval],
							[I].[OtherInterviewers]
							
				FROM 
						MemberInterviewEntries [MIE] join
							[dbo].[Interview] [I] ON [MIE].[Id]=[I].[Id] LEFT JOIN
							[JobPosting] [JP] ON [I].[JobPostingId]=[JP].[Id] LEFT JOIN
						
							[Company] [C] ON [I].[ClientId]=[C].[Id] LEFT JOIN
							[GenericLookup] [GL] ON [I].[TypeLookupId]=[GL].[Id] left join
							[InterviewersNameView] [INV] on [I].[Id]=[INV].[Id]
				WHERE 
						Row between '+ CONVERT(nvarchar(10), (@StartRowIndex+1)) +'And ('+ CONVERT(nvarchar(10), @StartRowIndex) +' + '+ CONVERT(nvarchar(10), @RowPerPage) +')'
print @Sql
EXEC sp_executesql @SQL

SET @SQL = 'SELECT COUNT([I].[Id])
							FROM [dbo].[Interview] [I]
							
				'+ @WhereClause 

EXEC sp_executesql @SQL

END


