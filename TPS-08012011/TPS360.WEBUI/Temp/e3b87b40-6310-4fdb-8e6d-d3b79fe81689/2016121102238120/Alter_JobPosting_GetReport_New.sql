GO
print 'Script 1 Start'
GO 
/*
NAME : ALTER StoredProcedure JobPosting_GetReport_New 1,100,'[J].[IsTemplate] = 0','PostedDate','desc'
**********************************************************************************************************************                              
*/
-- =============================================
-- Author:        Kanchan Yeware
-- Updated date:   06/29/2016
-- Description:   Update stored Procedure for Inserting New values into the jobposting Table(New Requirement)
-- =============================================
--exec JobPosting_GetReport_New 1,100,'[J].[IsTemplate] = 0','PostedDate','desc'
ALTER PROCEDURE [dbo].[JobPosting_GetReport_New]    
 @StartRowIndex int,    
 @RowPerPage  int,    
 @WhereClause nvarchar(MAX),    
 @SortColumn  nvarchar(128),    
 @SortOrder  nvarchar(4)    
    
AS    
BEGIN     
    
SET @StartRowIndex = isnull(@StartRowIndex, -1)    
SET @RowPerPage = isnull(@RowPerPage, -1)    
SET @WhereClause = isnull(@WhereClause, '')    
SET @SortColumn = isnull(@SortColumn, '')    
SET @SortOrder = isnull(@SortOrder, '')    
declare @sC varchar(200)    
  
DECLARE @SQL nvarchar(MAX)    
DECLARE @SQL2 nvarchar(MAX)
DECLARE @PagingClause nvarchar(512)    
  
    if(CHARINDEX('level', @SortColumn)>0)  
  begin  
    set @SortColumn= replace(@SortColumn,'level','')  
     if(@sortOrder='asc' or @SortOrder='') set @SortOrder='desc'  
     else set @SortOrder='asc'  
  end  
if(charindex('[JPTAT].',@SortColumn)>0)  
begin  
 if(@sortOrder='asc' or @SortOrder='') set @SortOrder='desc'  
     else set @SortOrder='asc'  
end  
set @sC=@SortColumn    
IF (@WhereClause != '')    
BEGIN    
 SET @WhereClause = 'WHERE ' + char(13) + @WhereClause    
END  

if(@SortColumn='NoOfUnfilledOpenings')
begin
	set @sc=' cast((NoOfOpenings-cast(isnull(JoiningCount,0) as int)) as int) '
end 
if(@SortColumn='[J].[InternalNote]')
begin
	set @sc='cast([J].[InternalNote] as varchar(max))'
end
    
IF (@SortColumn != '')    
BEGIN    
 SET @SortColumn = 'ORDER BY ' + @SortColumn    
    
 IF (@SortOrder != '')    
 BEGIN    
  SET @SortColumn = @SortColumn + ' ' + @SortOrder    
 END    
END    
ELSE    
BEGIN    
 SET @SortColumn = @SortColumn + ' ORDER BY [J].[JobTitle] ASC'    
END    
    
IF ((@RowPerPage > -1) AND (@StartRowIndex > -1))    
BEGIN      
 SET @PagingClause = 'WHERE Row between '+ CONVERT(nvarchar(10), @StartRowIndex) +'And ('+ CONVERT(nvarchar(10), @StartRowIndex) +' + '+ CONVERT(nvarchar(10), @RowPerPage) +')'    
END    
declare @hiringLevel varchar(max)    
set @hiringLevel=''    
declare @n int    
declare @i int    
set @i=0    
select @n =max(sortorder) from hiringmatrixlevels    
while(@i<=@n)    
begin    
    
 if(@hiringLevel<>'' and substring(@hiringLevel,len(@hiringLevel),1)<>',') set @hiringLevel=@hiringLevel+','    
 select  @hiringLevel=@hiringLevel +'['+ cast(ID as varchar(10)) + ']' from hiringmatrixlevels where sortorder=@i    
   set @i= @i + 1    
end   

if(isnumeric(substring(@sc,2,len(@sC)-2))=1)    
begin    
set @SQL='WITH JobPostingEntries AS ( select  ROW_NUMBER() OVER ('+@sortColumn+')AS Row ,jobpostingid,'+  @hiringLevel + '    from (       
SELECT H.ID,MJD.ID as IDs,j.id as jobpostingid    
 FROM [JobPosting] AS [J]'
end    
else if(@sc='[J].[Name]')    
begin    
 set @SQL='WITH JobPostingEntries AS (select  ROW_NUMBER() OVER (order by Name  ' + @sortOrder + ')AS Row ,jobpostingid,'+  @hiringLevel + '   from  (           
SELECT GL.Name as Name   , H.ID,MJD.ID as IDs,j.id as jobpostingid    
 FROM [JobPosting] AS [J] Join GenericLookup [GL] on [GL].[ID] = [J].[JobStatus]'    
end    
else if(@sc='[J].[MinExpRequired]')    
begin    
 set @SQL='WITH JobPostingEntries AS (select  ROW_NUMBER() OVER (order by MinExp  ' + @sortOrder + ')AS Row ,jobpostingid,'+  @hiringLevel + '   from  (           
SELECT case when [J].[MinExpRequired]='''' then -1 else  cast( [J].[MinExpRequired] as decimal(15,3))  end AS MinExp    , H.ID,MJD.ID as IDs,j.id as jobpostingid    
 FROM [JobPosting] AS [J] '    
end    
else if(@sc='[J].[MaxExpRequired]')    
begin    
set @SQL='WITH JobPostingEntries AS (select  ROW_NUMBER() OVER (order by MaxExp  ' + @sortOrder + ')AS Row ,jobpostingid,'+  @hiringLevel + '   from  (           
SELECT case when [J].[MaxExpRequired]='''' then -1 else  cast( [J].[MaxExpRequired] as decimal(15,3))  end AS MaxExp   , H.ID,MJD.ID as IDs,j.id as jobpostingid    
 FROM [JobPosting] AS [J] '    
end    
else if(@sc='[J].[ClientHourlyRate]')    
begin    
set @SQL='WITH JobPostingEntries AS (select  ROW_NUMBER() OVER (order by rate  ' + @sortOrder + ', Cycle  
 '+@sortOrder+'   
)AS Row ,jobpostingid,'+  @hiringLevel + '   from  (           
SELECT case when [J].[ClientHourlyRate]='''' then -1 else  cast( [J].[ClientHourlyRate] as decimal(15,3))  end AS rate,  
  
case When [J].[ClientRatePayCycle] =0 then '' ''  
when [J].[ClientRatePayCycle] =1 then ''Hourly''  
when [J].[ClientRatePayCycle] =2 then ''Daily''  
when [J].[ClientRatePayCycle] =3 then ''Monthly''  
when [J].[ClientRatePayCycle] =4 then ''Yearly''  
 end as Cycle    
   , H.ID,MJD.ID as IDs,j.id as jobpostingid    
 FROM [JobPosting] AS [J] '    
end 
else if(@sc='[J].[PayRate]')    
begin    
set @SQL='WITH JobPostingEntries AS (select  ROW_NUMBER() OVER (order by rate  ' + @sortOrder + ',Cycle '+@sortOrder+' )  
AS Row ,jobpostingid,'+  @hiringLevel + '   from  (           
SELECT case  when [J].[PayRate]='''' or  [J].[PayRate] =''open'' then -1 else   
cast ([J].[PayRate] as decimal(18,3)) end as rate,  
case When [J].[PayCycle] =0 then '' ''  
when [J].[PayCycle] =1 then ''Hourly''  
when [J].[PayCycle] =2 then ''Daily''  
when [J].[PayCycle] =3 then ''Monthly''  
when [J].[PayCycle] =4 then ''Yearly''  
 end as Cycle
 , H.ID,MJD.ID as IDs,j.id as jobpostingid    
 FROM [JobPosting] AS [J] left join Company [C] on [J].[ClientId]=[C].[Id]'    
end    
else if(@sc='[J].[ClientId]')    
begin    
set @SQL='WITH JobPostingEntries AS (select  ROW_NUMBER() OVER (order by CompanyName  ' + @sortOrder + ')AS Row ,jobpostingid,'+  @hiringLevel + '   from  (           
SELECT C.CompanyName  , H.ID,MJD.ID as IDs,j.id as jobpostingid    
 FROM [JobPosting] AS [J] left join Company [C] on [J].[ClientId]=[C].[Id]'    
end   
    
else if(@sc='[J].[CreatorId]')    
begin    
set @SQL='WITH JobPostingEntries AS (select  ROW_NUMBER() OVER (order by name  ' + @sortOrder + ')AS Row ,jobpostingid,'+  @hiringLevel + '   from  (         SELECT M.FirstName as name  , H.ID,MJD.ID as IDs,j.id as jobpostingid    
 FROM [JobPosting] AS [J] left join Member [M] on [J].[CreatorId]=[M].[Id]'    
end    
    
else if(@sc='[J].[UpdatorId]')    
begin    
set @SQL='WITH JobPostingEntries AS (select  ROW_NUMBER() OVER (order by name  ' + @sortOrder + ')AS Row ,jobpostingid,'+  @hiringLevel + '   from  (           
SELECT M.FirstName as name  , H.ID,MJD.ID as IDs,j.id as jobpostingid    
 FROM [JobPosting] AS [J] left join Member [M] on [J].[UpdatorId]=[M].[Id]'    
end    
    
else if(@sc='[J].[dateDifferce]')    
begin    
 set @SQL='WITH JobPostingEntries AS (select  ROW_NUMBER() OVER (order by dateDifferce  ' + @sortOrder + ')AS Row ,jobpostingid,'+  @hiringLevel + '   from  (           
SELECT  DATEDIFF (ww ,[J].[ActivationDate] , [J].[FinalHiredDate] ) as dateDifferce   , H.ID,MJD.ID as IDs,j.id as jobpostingid    
 FROM [JobPosting] AS [J]'    
end    
    
else if(@sc='[J].[JobCategoryLookupId]')    
begin    
 set @SQL='WITH JobPostingEntries AS (select  ROW_NUMBER() OVER (order by JobCategoryLookupId  ' + @sortOrder + ')AS Row ,jobpostingid,'+  @hiringLevel + '   from  (           
SELECT  [CAT].[Name] as JobCategoryLookupId    , H.ID,MJD.ID as IDs,j.id as jobpostingid    
 FROM [JobPosting] AS [J] left join [Category] [CAT] on [J].[JobCategoryLookupId]=[CAT].[ID]'    
end    
else if(@sc='[J].[WorkScheduleLookupId]')    
begin    
 set @SQL='WITH JobPostingEntries AS (select  ROW_NUMBER() OVER (order by WorkScheduleLookupId  ' + @sortOrder + ')AS Row ,jobpostingid,'+  @hiringLevel + '   from  (           
SELECT  isnull([GL].[Name],'''') as WorkScheduleLookupId    , H.ID,MJD.ID as IDs,j.id as jobpostingid    
 FROM [JobPosting] AS [J] left join [GenericLookup] [GL] on  [J].[WorkScheduleLookupID]=[GL].[ID]'    
end 
  
else    
begin    
set @SQL='WITH JobPostingEntries AS (select  ROW_NUMBER() OVER (order by sortColumn  ' + @sortOrder + ')AS Row ,jobpostingid,'+  @hiringLevel + '   from  (           
SELECT '+@sC + ' as sortColumn , H.ID,MJD.ID as IDs,j.id as jobpostingid    
 FROM [JobPosting] AS [J]' 
end               
  set @SQL=@SQL+'     
                   
               left  JOIN [State] [S] ON [J].[StateId]=[S].[Id]     
                        left  JOIN [GenericLookup] [G] ON [G].[ID] = [J].[JobStatus]    
                       left join MemberJobCart [MJ] on mj.jobpostingid=j.id    
      left join MemberJobCartDetail MJD  on MJD.MemberJobCartId = MJ.Id and   MJD.IsRemoved = 0  and MJ.IsRemoved=0  
   left join [JobPostingTurnArroundTime] [JPTAT] on [J].[Id]=[JPTAT].[Id]  
      Left Join [vwJobPostingInterviewCount] [JIC] on [J].[Id]=[JIC].[JobPostingID]  
   Left Join [vwJobPostingJoiningDetailsCount] [JJDC] on [J].[Id]=[JJDC].[JobPostingID]  
      Left Join [vwJobPostingOfferDetailsCount] [JODC]  on [J].[Id]=[JODC].[JobPostingID]  
      Left Join [vwJobPostingSubmissionCount] [JSDC]   on [J].[Id]=[JSDC].[JobPostingID]  
   
      left join HiringMatrixLevels H on H.ID=MJD.SelectionStepLookupId     
    '+ @WhereClause +'  )p    
PIVOT    
(    
COUNT (IDs)    
FOR ID in( ' + @hiringLevel+ ')    
) AS pvt     
    )    
    SELECT     
      [J].[Id],    
      [JobTitle],    
      [JobPostingCode],    
      [ClientJobId],     
      [NoOfOpenings],    
      [PayRate],    
      [PayRateCurrencyLookupId],    
      [PayCycle],    
      [TravelRequired],    
      [TravelRequiredPercent],    
      [OtherBenefits],    
      [JobStatus],    
      [JobDurationLookupId],    
      [JobDurationMonth],    
      [JobAddress1],    
      [JobAddress2],    
     [J]. [City],    
     [J]. [ZipCode],    
     [J]. [CountryId],    
     [J]. [StateId],    
      [StartDate],    
      [FinalHiredDate],    
      [JobDescription],    
      [AuthorizationTypeLookupId],    
      [PostedDate],    
      [ActivationDate],    
      [RequiredDegreeLookupId],    
      [MinExpRequired],    
      [MaxExpRequired],
      [IsJobActive],
      [JobType],    
      [IsApprovalRequired],    
      [InternalNote],    
      [ClientId], 
      [ClientHourlyRate],    
      [ClientHourlyRateCurrencyLookupId],    
      [ClientRatePayCycle],
      [TaxTermLookupIds],    
      [JobCategoryLookupId],  
      [ExpectedRevenue],    
      [ExpectedRevenueCurrencyLookupId],
      [WorkflowApproved], 
      [TeleCommunication],    
      [IsTemplate],    
      [IsRemoved],    
     [J]. [CreatorId],    
     [J]. [UpdatorId],    
     [J]. [CreateDate],    
     [J]. [UpdateDate],    
      [IsExpensesPaid],    
      [RawDescription],    
      [JobDepartmentLookupId],    
      [SkillLookupId],    
      [OccupationalSeriesLookupId],    
      [PayGradeLookupId],    
      [WorkScheduleLookupId],    
      [SecurityClearance]      ,   
      [ClientContactID],   
   [ReportingTo],  
   [NoOfReportees],  
   [MinimumQualifyingParameters],  
   [MaxPayRate],  
      [AllowRecruitersToChangeStatus],  [ShowInCandidatePortal],[ShowInEmployeeReferralPortal],[DisplayRequisitionInVendorPortal],
    [OpenDate],
	[SalesRegionLookUp],
 [SalesGroupLookUp],
 [CustomerName],
 [POAvailability],
[JobLocationLookUpID],
DBO.GetGenericLookupNameById([JobLocationLookUpID]),
DBO.GetGenericLookupNameById([SalesRegionLookUp]),
DBO.GetGenericLookupNameById([SalesGroupLookUp]),
CC.FirstName + '' '' + CC.LastName,
[CG].Name AS JobCategory,
[EmployementTypeLookUpID],
[ClientBrief],'  
   set @SQL2='	isnull([TurnArroundTime],0) as TurnArroundTime,  
     cast(isnull( SubmissionCount,0) as int) as SubmissionCount,  
     cast(isnull(OfferCount,0) as int) as OfferCount,
		cast(isnull(JoiningCount,0) as int) as JoiningCount,
   --cast(isnull(OfferCount,0) as int) as OfferCount,   cast(isnull(OfferCount,0) as int) as OfferCount1,
   --cast(isnull(JoiningCount,0) as int) as JoiningCount,  
   cast(isnull(InterviewCount,0) as int) as InterviewCount, 
cast((NoOfOpenings-cast(isnull(JoiningCount,0) as int)) as int) as NoOfUnfilled,   
      [RequisitionSource],    
      [EmailSubject],    
--[JPICN].CandidateName as InterviewCandidateName,
--	[JPSCN].CandidateName as SubmittedCandidateName,
--	[JPOCN].CandidateName as OfferedCandidateName,
--	[JPJCN].CandidateName as JoinedCandidateName,
     dbo.GetInterviewCandidateName(J.id),
	  dbo.GetSubmittedCandidateName(J.id),
	  dbo.GetOfferedCandidateName(J.id),
	  dbo.GetJoinedCandidateName(J.id),
	[J].InternalNote as JobPostNotes,
	[G].Name,	
	  -- Added by Kanchan Yeware For Additional Fields in JobPosting Table --
	  [GDemandT].Name AS [DemandType],
	  --[J].[BillabilityLookUpID],	  	 
	  [GBillabilityT].[Name] AS [Billability],
	  --[J].[SupervisoryOrganizationCode],
	   --[OrgU].[ReportingOrgStTxt] AS [SupervisoryOrgShortText],
	  --[OrgU].[ReportingOrgLngTxt] AS [SupervisoryOrgDesc],	
	  [OrgUnitAA].DEPT_NAME AS [SupervisoryOrgShortText],
	  [OrgUnitAA].ORG_UNIT_NAME AS [SupervisoryOrgDesc],		
	  [CCen].[COST_CENTER_NAME] AS [CostCenter],
	  [GRegion].[Name] AS [Region],
	  [GContrManag].[Name] AS [IndContrMangement],
	  [JP].[JobProfileCode],
	  [J].[ProjectName],
	  [J].[CustomerInfo],	 
	   [OrgUnitA].[DIV_NAME] AS [Division],
	  [GDC].[Name] AS [GDCGSC],	  
	  dbo.[GetDivisionManagerName]([J].[ReportingMgrGID]) as [ManagerName],  
	   [OrgUnitA].[DIV_HEAD] AS [OrgUnitHead],
	  [GPriority].[Name] AS [Priority],
	 [J].[SecondarySkill],
	  [J].[MinRelExpRequired],
	  [J].[MaxRelExpRequired],	
	  [J].[ReasonForHire],	   	
	  [J].[Location],
	  
	     [J].[SupervisoryOrganizationCode] AS SupervisoryOrganizationCode,
	  [JP].[JobFamily] AS JobFamilyName,	
	  [J].[ReportingMgrGID] AS CandidateManagerGID, 
	  [J].[CreateDate] AS CreatedOn,   
      [J].[UpdateDate] AS SubmittedOn,   
     
      
	  ' + @hiringLevel 
	  
 Declare @SQL1 varchar(max)
   set @SQL1 =
 'from  JobPosting  J
    join  JobPostingEntries   JE on  J.ID= JE.JobPostingID   
    left join [JobPostingTurnArroundTime] [JPTAT] on [J].[Id]=[JPTAT].[Id]  
    Left Join [vwJobPostingInterviewCount] [JIC] on [J].[Id]=[JIC].[JobPostingID]  
    Left Join [vwJobPostingJoiningDetailsCount] [JJDC] on [J].[Id]=[JJDC].[JobPostingID]  
    Left Join [vwJobPostingOfferDetailsCount] [JODC]  on [J].[Id]=[JODC].[JobPostingID]  
    Left Join [vwJobPostingSubmissionCount] [JSDC]   on [J].[Id]=[JSDC].[JobPostingID]  
   -- Left Join [JobPostingInterviewCandidateNames][JPICN] on [J].[Id]=[JPICN].[ID]  
	--Left Join [JobPostingSubmittedCandidateNames][JPSCN] on [J].[Id]=[JPSCN].[ID]  
	--Left Join [JobpostingOfferedCandidateNames][JPOCN] on [J].[Id]=[JPOCN].[ID]  
	--Left Join [JobpostingJoinedCandidateNames][JPJCN] on [J].[Id]=[JPJCN].[ID]  
	Left Join CompanyContact CC on [J].[ClientContactID]=[CC].[Id]
	left join Category [CG] ON [J].JobCategoryLookupId=[CG].Id
	Left Join GenericLookup [G] on [J].[RequisitionType]=[G].[Id]

	-- Added by Kanchan Yeware Joing with New Table Fields --
	Left Join GenericLookup [GDemandT] on [J].[DemandType]=[GDemandT].[Id]
	Left Join GenericLookup [GBillabilityT] on [J].[Billability]=[GBillabilityT].[Id]
	Left Join OrganizationUnit [OrgU] on [J].[SupervisoryOrganizationCode] = [OrgU].[OrgUnitCode]
	Left Join CostCenter [CCen] on [J].[CostCenter] = [CCen].[COST_CENTER_ID]
	Left Join GenericLookup [GRegion] on [J].[Region]=[GRegion].[Id]
	Left Join GenericLookup [GContrManag] on [J].[IndContrMangement] = [GContrManag].[Id]
    Left Join OrganizationUnitApprovers [OrgUnitA] on [J].[Division] = [OrgUnitA].[ORG_UNIT] 
	Left Join OrganizationUnitApprovers [OrgUnitAA] on [J].[SupervisoryOrganizationCode] = [OrgUnitAA].[ORG_UNIT]  
	Left Join JobProfile [JP] on [J].[JobFamily] = [JP].[ID]
	Left Join GenericLookup [GDC] on [J].[GDCGSC]=[GDC].[Id]	
	Left Join GenericLookup [GPriority] on [J].[Priority]=[GPriority].[Id]
	Left Join OrganizationUnit [OrgUnit] on [J].[Division] = [OrgUnit].[OrgUnitCode]

'+case when (@StartRowIndex=-1 And @RowPerPage=-1) then '' else 'WHERE  Row between '+ CONVERT(nvarchar(10), @StartRowIndex+1)+ ' And ('+ CONVERT(nvarchar(10), @StartRowIndex) +' + '+ CONVERT(nvarchar(10), @RowPerPage) +') 'end    
--print @SQL 
--print '------------------'
--print @SQL1  
--  --sp_executesql
--EXEC  (@SQL + @SQL1  )
    
 EXEC ( @SQL  + @SQL2 + @SQL1  )
print @SQL 
print @SQL2 
print @SQL1

SET @SQL = 'SELECT COUNT([J].[Id])       
   FROM [JobPosting] AS [J]    
   LEFT JOIN [State] [S] ON [J].[StateId]=[S].[Id]    
   ' + @WhereClause    
    
EXEC sp_executesql @SQL 
END  


GO
print 'Script 2 Start'
GO 