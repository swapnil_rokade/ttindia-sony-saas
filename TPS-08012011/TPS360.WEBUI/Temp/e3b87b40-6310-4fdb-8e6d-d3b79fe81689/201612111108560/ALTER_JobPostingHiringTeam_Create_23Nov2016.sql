GO
print 'Script 1 Start'
GO 

/*
**************************************************************
ALTER STORED PROCEDURE JobPostingHiringTeam_Create
**************************************************************                              
*/ 

-- =============================================
-- Author:        pravin khot 
-- Updated date:   23/11/2016
-- Description:   added if condition for insert record time
-- =============================================
-- exec JobPostingHiringTeam_Create '','internal',289,3058,3250
ALTER PROCEDURE [dbo].[JobPostingHiringTeam_Create]
	@ReturnCode	int	= NULL OUTPUT,
	@EmployeeType varchar(64), 
	@JobPostingId int,
	@MemberId int,
	@CreatorId int

--WITH ENCRYPTION
AS
BEGIN
    SET NOCOUNT ON
    
    if((SELECT  COUNT(*) FROM [JobPostingHiringTeam] WHERE [MemberId]=@MemberId AND [JobPostingId]=@JobPostingId)=0)
    begin
    
          INSERT INTO [dbo].[JobPostingHiringTeam] (
	[EmployeeType], 
	[JobPostingId],
	[MemberId],
	[CreatorId],
	[UpdatorId],
	[CreateDate],
	[UpdateDate]
    ) VALUES (
	@EmployeeType,
	@JobPostingId,
	@MemberId,
	@CreatorId,
	@CreatorId,
	GETDATE(),
	GETDATE()
    )

         EXECUTE [JobPostingHiringTeam_GetById] @@IDENTITY
	    SET @ReturnCode = 1001
	
	end
    SET @ReturnCode = 1001
    
END

GO
print 'Script 1 End'
GO
