--exec JobPosting_GetApprovingManager 286
ALTER PROCEDURE [dbo].[JobPosting_GetApprovingManager]
	-- Add the parameters for the stored procedure here	
	@JobPostingId int
AS
BEGIN	
	SET NOCOUNT ON;
    Declare @MemberId int
	Declare @CreatorId int
	declare @ApprovalStatus int
	declare @demand int
    declare @DIV_HEAD nvarchar(250)
	

	
	--set @ApprovalStatus = (select ApprovalType from JobPosting where Id= @JobPostingId)
	select @ApprovalStatus = ApprovalType, @CreatorId = CreatorId, @demand = DemandType from JobPosting where Id= @JobPostingId
	if(@ApprovalStatus = dbo.GetIdByApprovalStatus('Division Head Rejected') or @ApprovalStatus = dbo.GetIdByApprovalStatus('Division Head Approved'))
	begin	
		if (@ApprovalStatus = dbo.GetIdByApprovalStatus('Division Head Approved'))
		begin	
		
		     SELECT @DIV_HEAD=DIV_HEAD FROM OrganizationUnitApprovers WHERE [ORG_UNIT] IN (      
             select [OrgUnit] from EmployeeMemberMapping where [MemberId]=@CreatorId AND [OrgUnit] IN
              (select ORG_UNIT from OrganizationUnitApprovers WHERE [ORG_UNIT] IN
             (select [OrgUnit] from EmployeeMemberMapping where [MemberId]=@CreatorId)))		
		end
		else 
		begin
			 SELECT @DIV_HEAD=TOWER_HEAD FROM OrganizationUnitApprovers WHERE [ORG_UNIT] IN (      
             select [OrgUnit] from EmployeeMemberMapping where [MemberId]=@CreatorId AND [OrgUnit] IN
              (select ORG_UNIT from OrganizationUnitApprovers WHERE [ORG_UNIT] IN
             (select [OrgUnit] from EmployeeMemberMapping where [MemberId]=@CreatorId)))	
		end
	end
	
	ELSE if(@ApprovalStatus = dbo.GetIdByApprovalStatus('Tower Head Approved') or @ApprovalStatus = dbo.GetIdByApprovalStatus('Tower Head Rejected'))
	begin	
		if (@ApprovalStatus = dbo.GetIdByApprovalStatus('Tower Head Approved'))
		begin			
		     SELECT @DIV_HEAD=TOWER_HEAD FROM OrganizationUnitApprovers WHERE [ORG_UNIT] IN (      
             select [OrgUnit] from EmployeeMemberMapping where [MemberId]=@CreatorId AND [OrgUnit] IN
              (select ORG_UNIT from OrganizationUnitApprovers WHERE [ORG_UNIT] IN
             (select [OrgUnit] from EmployeeMemberMapping where [MemberId]=@CreatorId)))		
		end
		else 
		begin
			 SELECT @DIV_HEAD=DEPT_HEAD FROM OrganizationUnitApprovers WHERE [ORG_UNIT] IN (      
             select [OrgUnit] from EmployeeMemberMapping where [MemberId]=@CreatorId AND [OrgUnit] IN
              (select ORG_UNIT from OrganizationUnitApprovers WHERE [ORG_UNIT] IN
             (select [OrgUnit] from EmployeeMemberMapping where [MemberId]=@CreatorId)))	
		end
	end
	ELSE if(@ApprovalStatus = dbo.GetIdByApprovalStatus('Department Head Approved') or @ApprovalStatus = dbo.GetIdByApprovalStatus('Department Head Rejected'))
	begin	
		if (@ApprovalStatus = dbo.GetIdByApprovalStatus('Department Head Approved'))
		begin			
		     SELECT @DIV_HEAD=DEPT_HEAD FROM OrganizationUnitApprovers WHERE [ORG_UNIT] IN (      
             select [OrgUnit] from EmployeeMemberMapping where [MemberId]=@CreatorId AND [OrgUnit] IN
              (select ORG_UNIT from OrganizationUnitApprovers WHERE [ORG_UNIT] IN
             (select [OrgUnit] from EmployeeMemberMapping where [MemberId]=@CreatorId)))		
		end
		else 
		begin
			SELECT @DIV_HEAD= (FirstName + ' ' + MiddleName + ' ' + LastName) from Member where Id = @CreatorId	
		end
	end
	ELSE
	    BEGIN
	         SELECT @DIV_HEAD= (FirstName + ' ' + MiddleName + ' ' + LastName) from Member where Id = @CreatorId	
	    END	
	--END
		
	SELECT @DIV_HEAD
	
	
	
	--set @ApprovalStatus = (select ApprovalType from JobPosting where Id= @JobPostingId)
	--select @ApprovalStatus = ApprovalType, @CreatorId = CreatorId, @demand = DemandType from JobPosting where Id= @JobPostingId
	--if(@ApprovalStatus = dbo.GetIdByApprovalStatus('Division Head Rejected') or @ApprovalStatus = dbo.GetIdByApprovalStatus('Division Head Approved'))
	--begin	
	--	if (@ApprovalStatus = dbo.GetIdByApprovalStatus('Division Head Approved'))
	--	begin	
	--		if (ltrim(rtrim(dbo.GetGenericLookupNameById(@demand))) = 'Contract')
	--		begin
	--			set @MemberId = (select [MemberId] from EmployeeMemberMapping where GID = (select [OrgUnitHeadGId] from EmployeeMemberMapping where [MemberId] = @CreatorId))                    
	--			if(@MemberId is null)
	--			begin
	--				set @MemberId = @CreatorId
	--			end 	 
	--		end			
	--		else
	--		begin
	--			set @MemberId = (select [MemberId] from EmployeeMemberMapping where GID = (select [DivisionHeadGid] from EmployeeMemberMapping where [MemberId] = @CreatorId))                    	 
	--			if(@MemberId is null)
	--			begin
	--				set @MemberId = @CreatorId
	--			end 
	--		end
	--	end
	--	else 
	--	begin
	--		set @MemberId = (select [MemberId] from EmployeeMemberMapping where GID = (select [DivisionHeadGid] from EmployeeMemberMapping where [MemberId] = @CreatorId))                    	 
	--		if(@MemberId is null)
	--		begin
	--			set @MemberId = @CreatorId
	--		end 
	--	end
	--end
	--if(@ApprovalStatus = dbo.GetIdByApprovalStatus('Tower Head Rejected') or @ApprovalStatus = dbo.GetIdByApprovalStatus('Tower Head Approved'))
	--begin	 
	-- set @MemberId = (select [MemberId] from EmployeeMemberMapping where GID = (select [OrgUnitHeadGId] from EmployeeMemberMapping where [MemberId] = @CreatorId))                    
 --    if(@MemberId is null)
 --    begin
 --    set @MemberId = @CreatorId
 --    end 	 
	--end
	--SELECT FirstName + ' ' + MiddleName + ' ' + LastName as ApprovingManager from Member where Id = @MemberId
END


