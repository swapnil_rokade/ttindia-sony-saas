﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: IInterviewResponseDataAccess.cs
    Description: This is the DataAccess  library page.
    Created By: Prasanth Kumar G
    Created On: 15/Jul/2015
    Modification Log:
    --------------------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ---------------------------------------------------------------------------------------------------------------------------------------------
    
*/
using System.Collections.Generic;
using TPS360.Common.Shared;
using TPS360.Common.BusinessEntities;



namespace TPS360.DataAccess
{
    #region IInterviewResponseDataAccess

    public interface IInterviewResponseDataAccess
    {
        void Add(InterviewResponse InterviewResponse);

        void Update(InterviewResponse InterviewResponse);

        InterviewResponse GetById(int id);

        IList<InterviewResponse> GetAll();

        PagedResponse<InterviewResponse> GetPaged(PagedRequest request);

        IList<InterviewResponse> GetByInterviewId(int InterviewId);

        IList<InterviewResponse> GetByInterviewId_Email(int InterviewId, string Email);

        //IList<GenericLookup> InterviewAssessment_GetBy_InterviewId_InterviewerEmail(int InterviewId, string Email);


        void DeleteById(int id);
    }

    #endregion
}