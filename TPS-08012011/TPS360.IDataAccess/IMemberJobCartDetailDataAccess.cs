﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IMemberJobCartDetailDataAccess

    public interface IMemberJobCartDetailDataAccess
    {
        MemberJobCartDetail Add(MemberJobCartDetail memberJobCartDetail);

        MemberJobCartDetail Update(MemberJobCartDetail memberJobCartDetail);

        MemberJobCartDetail GetById(int id);

        IList<MemberJobCartDetail> GetAll();

        bool DeleteById(int id);

        bool DeleteAllByJobCartId(int memberJobCartId);

        bool RemoveAllByJobCartId(int memberJobCartId);

        IList<MemberJobCartDetail> GetAllByMemberJobCartId(int memberJobCartId);

        MemberJobCartDetail GetByMemberJobCartIdAndSelectionStepId(int memberJobCartId, int selectionStepId);

        MemberJobCartDetail GetCurrentByMemberJobCartId(int memberJobCartId);
    }

    #endregion
}