﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IRecurrenceDataAccess

    public interface IRecurrenceDataAccess
    {
        Recurrence Add(Recurrence recurrence);

        Recurrence Update(Recurrence recurrence);

        Recurrence GetByRecurrenceID(int recurrenceID);

        IList<Recurrence> GetAll();

        bool DeleteByRecurrenceID(int recurrenceID);
    }

    #endregion
}