﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IJobPostingHiringMatrixDataAccess

    public interface IJobPostingHiringMatrixDataAccess
    {
        JobPostingHiringMatrix Add(JobPostingHiringMatrix jobPostingHiringMatrix);

        JobPostingHiringMatrix Update(JobPostingHiringMatrix jobPostingHiringMatrix);

        JobPostingHiringMatrix GetById(int id);

        IList<JobPostingHiringMatrix> GetAll();

        bool DeleteById(int id);

        JobPostingHiringMatrix GetByJobPostingId(int jobPostingId);

        bool DeleteByJobPostingId(int jobPostingId);
    }

    #endregion
}