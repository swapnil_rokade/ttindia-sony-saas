﻿using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region ICandidateDashboardDataAccess

    public interface ICandidateDashboardDataAccess
    {
        CandidateDashboard GetByMemberId(int memberId);
    }

    #endregion
}