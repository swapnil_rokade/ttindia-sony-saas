﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: IEmployeeReferralDataAccess.cs
    Description: 
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1             14-November-2012         Charles       
    -------------------------------------------------------------------------------------------------------------------------------------------       
*/


using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using System;
using System.Data;
using TPS360.Common.BusinessEntity;
using System.Collections.Generic;
using System.Collections;
namespace TPS360.DataAccess
{
    #region IEmployeeReferralDataAccess

    public interface IEmployeeReferralDataAccess
    {
        int Add(EmployeeReferral obj);     

        PagedResponse<EmployeeReferral> GetPaged(PagedRequest request);

        PagedResponse<ListWithCount> GetPagedReferralSummary(PagedRequest request, string type);

        ArrayList GetAllReferrerList();

        IList<ListWithCount> GetAllListGroupByDate(int JobPostingId, int ReferrerID, int StartDate, int EndDate);

        EmployeeReferral GetByMemberId(int MemberID);

        //*********[Kanchan Yeware] - [Change for IJP Portal] – [20-Sept-2016] – Start *****************
        void AddInternal(int memid, int jobid);
        //*********[Kanchan Yeware] - [Change for IJP Portal] – [20-Sept-2016] – End *****************
        //*******************Suraj Adsule******Start********For********20160920*********//
        ArrayList GetEmployeeReferrerListByJobpostingId(string JobpostingId);
        //*********************Suraj Adsule******End********For ********20160920********//
        //*******************Suraj Adsule******Start********For********20160920*********//\
        ArrayList GetEmployeeReferrerListByTeamLeaderId(int TeamLeaderId);
        //*********************Suraj Adsule******End********For ********20160920********//
        //*******************Suraj Adsule******Start********For********20160920*********//\
        PagedResponse<EmployeeReferral> GetPagedInternal(PagedRequest request);

        string GetJobPostingIdByMemberIdForERPortal(int MemberId);
        //*********************Suraj Adsule******End********For ********20160920********//
    }

    #endregion
}