﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IMemberNoteDataAccess

    public interface IMemberNoteDataAccess
    {
        MemberNote Add(MemberNote memberNote);

        MemberNote GetById(int id);

        IList<MemberNote> GetAll();

        bool DeleteById(int id);

        IList<MemberNote> GetAllByMemberId(int id);
    }

    #endregion
}