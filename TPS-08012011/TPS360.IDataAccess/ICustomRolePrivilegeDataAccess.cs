﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region ICustomRolePrivilegeDataAccess

    public interface ICustomRolePrivilegeDataAccess
    {
        CustomRolePrivilege Add(CustomRolePrivilege customRolePrivilege);

        CustomRolePrivilege Update(CustomRolePrivilege customRolePrivilege);

        CustomRolePrivilege GetById(int id);

        IList<CustomRolePrivilege> GetAllByRoleId(int roleId);

        IList<CustomRolePrivilege> GetAll();

        bool DeleteById(int id);

        bool DeleteByRoleId(int roleId);

        void DeleteAll_CustomRolePrivilege();
    }

    #endregion
}