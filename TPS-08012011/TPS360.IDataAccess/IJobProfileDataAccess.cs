﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: IGenericLookupDataAccess.cs
    Description:  
    Created By:  
    Created On:  
    Modification Log:
    --------------------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
 *  1.0                 5/Dec/2015          Prasanth Kumar G    Introduced Function for Get interviewe Response
    ---------------------------------------------------------------------------------------------------------------------------------------------
    
*/

//-------------------------Rishi Code Start-------------------------------------------//
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    #region IJobProfileDataAccess

    public interface IJobProfileDataAccess
    {
        IList<TPS360.Common.BusinessEntities.JobProfile> GetAll(string division, string IndivContribMgnt);
        TPS360.Common.BusinessEntities.JobProfile GetByJobFamilyName(string jobFamilyName);
    }

    #endregion
}

//-------------------------Rishi Code End-------------------------------------------//