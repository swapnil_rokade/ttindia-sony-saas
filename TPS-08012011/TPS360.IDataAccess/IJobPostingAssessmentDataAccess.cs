using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IJobPostingAssessmentDataAccess

    public interface IJobPostingAssessmentDataAccess
    {
        JobPostingAssessment Add(JobPostingAssessment jobPostingAssessment);

        JobPostingAssessment Update(JobPostingAssessment jobPostingAssessment);

        JobPostingAssessment GetById(int id);

        JobPostingAssessment GetByJobPostingIdAndTestMasterId(int jobPostingId, int testMasterId);

        IList<JobPostingAssessment> GetAll();

        IList<JobPostingAssessment> GetAllByJobPostingId(int jobPostingId);

        bool DeleteById(int id);

        bool DeleteByJobPostingId(int jobPostingId);
    }

    #endregion
}