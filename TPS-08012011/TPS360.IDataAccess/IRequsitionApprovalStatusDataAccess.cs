﻿using System.Collections.Generic;
using TPS360.Common.BusinessEntities;
using System;

namespace TPS360.DataAccess
{
    #region IRequsitionApprovalStatusDataAccess
    public interface IRequsitionApprovalStatusDataAccess
    {
        string GetRequistionStatusById(int id);
        Int32 GetStatusIdByRequistionStatus(string statusName);
    }
    #endregion
}
