﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    #region ICandidateHireStatusDataAccess
    public interface ICandidateHireStatusDataAccess
    {
        //-- Declare Function for Getting all candidate list assinged to a particular Hiring Manger
        IList<CandidatesHireStatus> GetCandidateListbyManagerId(int ManagerId);

        //-- Declare GetAllHiringManager Function for Getting all HiringMatrixLevel Values for Hiring Manager only
        IList<HiringMatrixLevels> GetAllHiringManager(int MemberID);

        void GetUpdateCandidateStatus(int ID, int JobStatus);

        void GetCandidHiringStatusRoleSave(int Roleid, int StatusID, int creatorId, bool IsRemovedFlag);

        void GetSendCandidtaeStatusEmail(int ID, int CandidateId, int RequsitionStatus, int ManagerID, int JobPostingId, int AdminMemberid);

        IList<CandidatesHireStatus> GetAllHiringStatusRoleId(int RoleId);

        IList<CustomRole> GetCustomUserRoleAll();
    }
    #endregion
}
