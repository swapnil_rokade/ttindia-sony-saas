﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName:ICustomRoleDataAccess.cs
    Description: 
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1             3/March/2016          pravin khot           added GetAllUser        
-------------------------------------------------------------------------------------------------------------------------------------------       

>*/
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    #region ICustomRoleDataAccess

    public interface ICustomRoleDataAccess
    {
        CustomRole Add(CustomRole customRole);

        CustomRole Update(CustomRole customRole);

        CustomRole GetById(int id);

        CustomRole GetByName(string Name);

        IList<CustomRole> GetAll();

        IList<CustomRole> GetAllUser(); //code added by pravin khot on 3/March/2016

        PagedResponse<CustomRole> GetPaged(PagedRequest request);

        bool DeleteById(int id);
        string GetCustomRoleNameByMemberId(int MemberId);
    }

    #endregion
}