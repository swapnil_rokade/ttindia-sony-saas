﻿using System.Collections.Generic;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using System;

namespace TPS360.DataAccess
{
    #region ISearchAgentEmailTemplate

    public interface ISearchAgentEmailTemplateDataAccess
    {

        SearchAgentEmailTemplate Add(SearchAgentEmailTemplate template);

        SearchAgentEmailTemplate Update(SearchAgentEmailTemplate template);

        SearchAgentEmailTemplate GetById(int id);

        SearchAgentEmailTemplate GetByJobPostingId(int id);

        bool DeleteSearchAgentEmailTemplateByJobPostingId(int JobPostingId);

    }

    #endregion
}