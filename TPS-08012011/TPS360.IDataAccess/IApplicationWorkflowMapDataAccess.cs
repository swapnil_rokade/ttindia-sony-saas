﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IApplicationWorkflowMapDataAccess

    public interface IApplicationWorkflowMapDataAccess
    {
        ApplicationWorkflowMap Add(ApplicationWorkflowMap applicationWorkflowMap);

        ApplicationWorkflowMap Update(ApplicationWorkflowMap applicationWorkflowMap);

        ApplicationWorkflowMap GetById(int id);

        ApplicationWorkflowMap GetByApplicationWorkflowIdAndMemberId(int applicationWorkflowId, int memberId);

        IList<ApplicationWorkflowMap> GetAllByApplicationWorkflowId(int applicationWorkflowId);

        IList<ApplicationWorkflowMap> GetAll();

        bool DeleteById(int id);

        bool DeleteByApplicationWorkflowId(int applicationWorkFlowId);
    }

    #endregion
}