﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    public interface IMemberPendingJoinersDataAccess
    {
        PagedResponse<MemberPendingJoiners> GetPaged(PagedRequest request);
    }
}
