﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region ICompanyNoteDataAccess

    public interface ICompanyNoteDataAccess
    {
        CompanyNote Add(CompanyNote companyNote);

        CompanyNote GetById(int id);

        IList<CompanyNote> GetAll();

        IList<CompanyNote> GetAllByCompanyId(int companyId);

        bool DeleteById(int id);

        bool DeleteByCompanyId(int companyId);
    }

    #endregion
}