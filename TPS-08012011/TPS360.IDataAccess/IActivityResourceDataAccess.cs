﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IActivityResourceDataAccess

    public interface IActivityResourceDataAccess
    {
        ActivityResource Add(ActivityResource activityResource);

        //ActivityResource Update(ActivityResource activityResource);

        ActivityResource GetByActivityID(int activityID);

        IList<ActivityResource> GetAll();

        bool DeleteByActivityID(int activityID);

        
    }

    #endregion
}