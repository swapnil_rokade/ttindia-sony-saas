﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
namespace TPS360.DataAccess
{
    #region ICompanyDocumentDataAccess

    public interface ICompanyDocumentDataAccess
    {
        CompanyDocument Add(CompanyDocument companyDocument);

        CompanyDocument Update(CompanyDocument companyDocument);

        CompanyDocument GetById(int id);

        IList<CompanyDocument> GetAllByCompanyId(int companyId);

        IList<CompanyDocument> GetAll();

        bool DeleteById(int id);

        bool DeleteByCompanyId(int companyId);

        CompanyDocument GetAllByCompanyIDTypeAndFileName(int companyId, string type, string FileName);


        PagedResponse<CompanyDocument> GetPagedByCompanyId(int companyId, PagedRequest request);
    }

    #endregion
}