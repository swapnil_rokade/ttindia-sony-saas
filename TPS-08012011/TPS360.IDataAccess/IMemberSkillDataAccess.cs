﻿
/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: IMemberSkillDataAccess.cs
    Description: his is used for member skill data access
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1            Feb-4-2009           Gopala Swamy           Defect ID:9090; Added Overloaded method for "GetAllByMemberId()"
 --------------------------------------------------------------------------------------------------------------------------------------------        
*/



using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IMemberSkillDataAccess

    public interface IMemberSkillDataAccess
    {
        IList<MemberSkill> GetAllByMemberId(int memberId);

        IList<MemberSkill> GetAllByMemberId(int memberId, string sortExpression);// 0.1
    }

    #endregion
}