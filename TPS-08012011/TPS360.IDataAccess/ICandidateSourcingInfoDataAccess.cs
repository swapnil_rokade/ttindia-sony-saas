﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: ICandidateSourcingInfoDataAccess.cs
    Description: This page defenes interface for Candidate Data Access.
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.        Date               Author           Modification
    ------------------------------------------------------------------------------------------------------------------------------    
   
-------------------------------------------------------------------------------------------------------------------------------------------        
*/

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using System;
using System.Collections.Generic;

namespace TPS360.DataAccess
{
    #region ICandidateSourcingInfoDataAccess

    public interface ICandidateSourcingInfoDataAccess
    {   
        PagedResponse<CandidateSourcingInfo> GetPagedForSourcingInfoReport(PagedRequest request);
    }

    #endregion
}