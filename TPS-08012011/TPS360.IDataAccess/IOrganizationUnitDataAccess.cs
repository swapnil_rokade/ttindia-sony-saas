﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: IGenericLookupDataAccess.cs
    Description:  
    Created By:  
    Created On:  
    Modification Log:
    --------------------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
 *  1.0                 5/Dec/2015          Prasanth Kumar G    Introduced Function for Get interviewe Response
    ---------------------------------------------------------------------------------------------------------------------------------------------
    
*/

//-------------------------Rishi Code Start-------------------------------------------//
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;


namespace TPS360.DataAccess
{
    #region IOrganizationUnitDataAccess

    public interface IOrganizationUnitDataAccess
    {
        IList<TPS360.Common.BusinessEntities.OrganizationUnit> GetAll(int active);
        //IList<TPS360.Common.BusinessEntities.OrganizationUnit> GetAllDictint();
        IList<TPS360.Common.BusinessEntities.OrganizationUnit> GetDivisionsCode(string parameter, int querySequence);
        TPS360.Common.BusinessEntities.OrganizationUnit GetById(string orgId);
        OrganizationUnit GetOrgApprovalMatrixByOrgCode(string OrgCode);//added by pravin khot on 14/Feb/2017
        OrganizationUnit GetEmployeeMemberMappingByGId(string orgId);
        IList<TPS360.Common.BusinessEntities.OrganizationUnit> GetDivisionsCodeByMemberId(string parameter, int querySequence, int CurrentMemberId);
    }


    #endregion
}

//-------------------------Rishi Code End-------------------------------------------//