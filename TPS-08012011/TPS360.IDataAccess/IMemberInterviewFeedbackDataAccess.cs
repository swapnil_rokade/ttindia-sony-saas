﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName:    IMemberInterviewFeedbackDataAccess.cs
    Description: 
    Created By:  Prasanth Kumar G  
    Created On:  24/Dec/2015
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author             Modification
    ------------------------------------------------------------------------------------------------------------------------------
    
                                                               
*/
using System;
using System.Collections.Generic;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
namespace TPS360.DataAccess
{
    #region IMemberInterviewFeedbackDataAccess

    public interface IMemberInterviewFeedbackDataAccess
    {
        MemberDocument Add(MemberDocument memberDocument);

        MemberDocument Update(MemberDocument memberDocument);

        //MemberDocument GetById(int id);

        MemberDocument GetAllByMemberIDInterviewIDInterviewerEmailDocumentTypeAndFileName(int memberId, int InterviewID, string InterviewerEmail,string type, string FileName);



        //IList<MemberDocument> GetAllByMemberId(int memberId);

        //IList<MemberDocument> GetAllByTypeAndMemberId(string type, int memberId);

        //MemberDocument GetAllByMemberIDTypeAndFileName(int memberId,string type, string FileName );

        //IList<MemberDocument> GetAll();

        //bool DeleteById(int id);
        
        //bool DeleteByMemberId(int MemberId);
        
        //IList<MemberDocument> GetAllByTypeAndMembersId(string membersId, string documentType);

        //MemberDocument GetByMemberIdAndFileName(int memberId, string fileName);

        //PagedResponse<MemberDocument> GetPagedByMemberId(int MemberID, PagedRequest request);

        //IList<MemberDocument> GetLatestResumeByMemberID(int memberId);

        //MemberDocument GetRecentResumeDocument(int memberId);
    }

    #endregion
}