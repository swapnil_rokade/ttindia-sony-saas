﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: IRejectCandidateDataAccess.cs
    Description: This page defenes interface for Candidate Data Access.
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.        Date               Author           Modification
    ------------------------------------------------------------------------------------------------------------------------------    
 *  0.1            14/March/2016      pravin khot     Introduced by GetPagedForReportReject,GetPagedForReportRejectBySelectedColumn

-------------------------------------------------------------------------------------------------------------------------------------------        
*/
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using System;
using System.Collections.Generic;

namespace TPS360.DataAccess
{
    #region IRejectCandidateDataAccess

    public interface IRejectCandidateDataAccess
    {
        void Add(RejectCandidate Candidate,string MemberId);

        void Update(RejectCandidate Candidate);

        RejectCandidate GetById(int id);

        IList<RejectCandidate> GetByMemberId(int memberId);

        bool DeleteByMemberId(int id);
      
        bool DeleteByMemberIdAndJobPostingID(int Memberid,int  JobPostingID);
     
        RejectCandidate GetByMemberIdAndJobPostingId(int Memberid, int JobPostingID);

        PagedResponse<RejectCandidate> GetPaged(PagedRequest request);

        int GetRejectCandidateCount(int JobPostingId);

        //*********************code added by pravin khot on 14/March/2016****************************
        PagedResponse<RejectCandidate> GetPagedForReportReject(PagedRequest request, DateTime addedFrom, DateTime addedTo, int addedBy, int addedBySource, 
        int country, int state, string city, DateTime interviewFrom, DateTime interviewTo, int interviewLevel, int interviewStatus, int industry, int functionalCategory, int workPermit, int WorkSchedule,
        int gender, int maritalStatus, int educationId, int assessmentId, bool isBroadcastedResume, bool isReferredApplicants, bool hasJobAgent, int assignedManager, string JobPostingId);

        PagedResponse<DynamicDictionary> GetPagedForReportRejectBySelectedColumn(PagedRequest request, DateTime addedFrom, DateTime addedTo, int addedBy, int addedBySource,
        int country, int state, string city, DateTime interviewFrom, DateTime interviewTo, int interviewLevel, int interviewStatus, int industry, int functionalCategory, int workPermit, int WorkSchedule,
        int gender, int maritalStatus, int educationId, int assessmentId, bool isBroadcastedResume, bool isReferredApplicants, bool hasJobAgent, int assignedManager, string JobPostingId, IList<string> CheckedList);

        //******************************End*****************************************

    }

    #endregion
}