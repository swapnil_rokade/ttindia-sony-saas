﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using System.Collections;

namespace TPS360.DataAccess
{
    #region IEmployeeTeamBuilderDataAccess

    public interface IEmployeeTeamBuilderDataAccess
    {
        EmployeeTeamBuilder Add(EmployeeTeamBuilder employeeTeamBuilders);

        EmployeeTeamBuilder Update(EmployeeTeamBuilder employeeTeamBuilders);

        EmployeeTeamBuilder GetByTeamId(int id);

        IList<EmployeeTeamBuilder> GetAllTeam();
        PagedResponse<EmployeeTeamBuilder> GetPaged(PagedRequest request);

        bool DeleteById(int id);
        //*******************Suraj Adsule******Start********For********20160920*********//
        ArrayList GetAllTeamByTeamLeaderId(int TeamleaderId);
        //*********************Suraj Adsule******End********For ********20160920********//
    }

    #endregion
}