﻿using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using System;
using System.Collections.Generic;

namespace TPS360.DataAccess
{
    public interface ICandidateJoiningReportDataAccess
    {
        PagedResponse<CandidateJoiningReport> GetCandidateJoiningReport1(PagedRequest request, DateTime addedFrom, DateTime addedTo, string City);

        PagedResponse<CandidateJoiningReport> GetCandidateJoiningReport2(PagedRequest request, DateTime addedFrom, DateTime addedTo, string City); 
    }
}
