﻿
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    #region IMemberInterviewDataAccess

    public interface IMemberInterviewDataAccess
    {
        MemberInterview GetById(int id);

        IList<MemberInterview> GetAllByMemberId(int memberId);

        IList<MemberInterview> GetAllByMemberId(int memberId, string sortExpression); // Defect id : 10254

        PagedResponse<MemberInterview> GetPaged(PagedRequest request);
        PagedResponse<MemberInterview> GetPagedForDashboard(PagedRequest request);
        PagedResponse<MemberInterview> GetInterviewReport(PagedRequest request);
        bool SuggestedInterviewerId(int interviewId, int InterviewerId); // added by pravin khot 
    }

    #endregion
}