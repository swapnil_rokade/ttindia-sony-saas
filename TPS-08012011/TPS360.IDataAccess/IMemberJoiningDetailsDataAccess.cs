﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    public interface IMemberJoiningDetailsDataAccess
    {
        void Add(MemberJoiningDetail memberJoiningDetails, string MemberId);

        MemberJoiningDetail GetById(int id);

        MemberJoiningDetail GetByMemberIdAndJobPosstingID(int memberId, int JobPostingId);

        bool DeleteJoiningDetialsById(int HiringDetailsID);
    }
}
