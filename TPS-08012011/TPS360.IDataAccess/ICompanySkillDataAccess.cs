﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region ICompanySkillDataAccess

    public interface ICompanySkillDataAccess
    {
        CompanySkill Add(CompanySkill companySkill);        

        CompanySkill GetById(int id);

        IList<CompanySkill> GetAll();

        IList<CompanySkill> GetAllByCompanyId(int companyId);

        bool DeleteById(int id);

        bool DeleteByCompanyId(int companyId);
    }

    #endregion
}