﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: ICompanyAssignedManagerDataAccess.cs
    Description: This is .cs page used to assign managers to company.
    Created By: 
    Created On:
    Modification Log:
    --------------------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ---------------------------------------------------------------------------------------------------------------------------------------------
    0.1              Jan-22-2008           Jagadish            Defect ID: 8822; Implemented sorting.
---------------------------------------------------------------------------------------------------------------------------------------------------        
*/
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region ICompanyAssignedManagerDataAccess

    public interface ICompanyAssignedManagerDataAccess
    {
        CompanyAssignedManager Add(CompanyAssignedManager companyAssignedManager);

        CompanyAssignedManager Update(CompanyAssignedManager companyAssignedManager);

        CompanyAssignedManager GetById(int id);

        IList<CompanyAssignedManager> GetAll();

        IList<CompanyAssignedManager> GetAllByMemberId(int memberId);

        IList<CompanyAssignedManager> GetAllByCompanyId(int companyId);

        CompanyAssignedManager GetCompanyAssignedManagerByCompanyIdAndMemberId(int companyId, int memberId);

        IList<CompanyAssignedManager> GetAllByCompanyId(int companyId, string sortExpression);

        bool DeleteById(int id);

        bool DeleteByCompanyIdAndMemberId(int companyId, int memberId);

        string GetPrimaryManagerNameByCompanyId(int companyId);
    }

    #endregion
}