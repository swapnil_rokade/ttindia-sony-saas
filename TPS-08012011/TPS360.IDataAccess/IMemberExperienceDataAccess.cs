﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    #region IMemberExperienceDataAccess

    public interface IMemberExperienceDataAccess
    {
        MemberExperience Add(MemberExperience memberExperience);

        MemberExperience Update(MemberExperience memberExperience);

        MemberExperience GetById(int id);

        IList<MemberExperience> GetAll();

        IList<MemberExperience> GetAllByMemberId(int memberId);

        PagedResponse<MemberExperience> GetPaged(PagedRequest request);

        PagedResponse<MemberExperience> GetPagedByMemberId(PagedRequest request);

        bool DeleteById(int id);

        bool DeleteByMemberId(int id);
    }

    #endregion
}