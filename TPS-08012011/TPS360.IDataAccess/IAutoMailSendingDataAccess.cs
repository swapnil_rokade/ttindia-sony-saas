﻿using System.Collections.Generic;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using System.Collections;
using System.Data;
using System;

namespace TPS360.DataAccess
{
    #region IAutoMailSendingDataAccess
    public interface IAutoMailSendingDataAccess
    {
       IList <AutoMailSending> GetSearchDetails();
    }
    #endregion
}
