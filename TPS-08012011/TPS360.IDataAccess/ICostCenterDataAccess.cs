﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: IGenericLookupDataAccess.cs
    Description:  
    Created By:  
    Created On:  
    Modification Log:
    --------------------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
 *  1.0                 5/Dec/2015          Prasanth Kumar G    Introduced Function for Get interviewe Response
    ---------------------------------------------------------------------------------------------------------------------------------------------
    
*/

//-------------------------Rishi Code Start-------------------------------------------//
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    #region ICostCenterDataAccess

    public interface ICostCenterDataAccess
    {
        IList<TPS360.Common.BusinessEntities.CostCenter> GetAll();
    }

    #endregion
}

//-------------------------Rishi Code End-------------------------------------------//