﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: IGenericLookupDataAccess.cs
    Description:  
    Created By:  
    Created On:  
    Modification Log:
    --------------------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
 *  1.0                 5/Dec/2015          Prasanth Kumar G    Introduced Function for Get interviewe Response
    ---------------------------------------------------------------------------------------------------------------------------------------------
    
*/

//-------------------------Rishi Code Start-------------------------------------------//
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;


namespace TPS360.DataAccess
{
    #region IOfferGenerationDataAccess

    public interface IOfferGenerationDataAccess
    {
        IList<TPS360.Common.BusinessEntities.OfferGeneration> GetAllById(int jobPostingId, int queryResult);
        IList<TPS360.Common.BusinessEntities.OfferGeneration> GetOfferedSalaryById(int memberHiringId);
        TPS360.Common.BusinessEntities.OfferGeneration Add(int JobPostingId, int memberHiringId, string gradeValue, string demandType, string headValue, double formulaValue);
        //**************************Suraj Adsule 20160824 For Permanent PDF Generation**************************//
        OfferGeneration GetCandidateDetail_UsingMemberHiringId(int memberHiringId, int type);
        //**************************Suraj Adsule 20160824 For Permanent PDF Generation**************************//
    }

    #endregion
}

//-------------------------Rishi Code End-------------------------------------------//