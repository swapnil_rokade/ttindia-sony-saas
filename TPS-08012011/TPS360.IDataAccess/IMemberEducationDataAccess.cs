﻿using TPS360.Common.Shared;
using TPS360.Common.BusinessEntities;
using System.Collections.Generic;
namespace TPS360.DataAccess
{
    #region IMemberEducationDataAccess

    public interface IMemberEducationDataAccess
    {
        MemberEducation Add(MemberEducation memberEducation);

        MemberEducation Update(MemberEducation memberEducation);

        MemberEducation GetById(int id);

        MemberEducation GetByMemberId(int memberId);

        MemberEducation GetHighestEducationByMemberId(int memberId);
//evan
        string GetLevelofEducationByLookupId(int lookupId);

        IList<MemberEducation> GetAll();

        IList<MemberEducation> GetAllMemberEducationByMemberId(int memberId);

        PagedResponse<MemberEducation> GetPaged(PagedRequest request);

        PagedResponse<MemberEducation> GetPagedByMemberId(PagedRequest request);

        bool DeleteById(int id);
        bool DeleteByMemberId(int id);
    }

    #endregion
}