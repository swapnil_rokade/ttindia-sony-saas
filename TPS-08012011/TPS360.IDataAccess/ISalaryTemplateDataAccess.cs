﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: IGenericLookupDataAccess.cs
    Description:  
    Created By:  
    Created On:  
    Modification Log:
    --------------------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
 *  1.0                 5/Dec/2015          Prasanth Kumar G    Introduced Function for Get interviewe Response
    ---------------------------------------------------------------------------------------------------------------------------------------------
    
*/

//-------------------------Rishi Code Start-------------------------------------------//
using System.Collections.Generic;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    #region ISalaryTemplateDataAccess

    public interface ISalaryTemplateDataAccess
    {
        IList<TPS360.Common.BusinessEntities.SalaryTemplate> GetAll();
        TPS360.Common.BusinessEntities.SalaryTemplate Add(string head, string calculation, double formula, string grade);
        IList<TPS360.Common.BusinessEntities.SalaryTemplate> Display(string grade);
    }

    #endregion
}
