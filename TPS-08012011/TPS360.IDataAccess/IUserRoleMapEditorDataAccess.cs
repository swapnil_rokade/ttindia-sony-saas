﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: IUserRoleMapEditorDataAccess.cs
    Description:  
    Created By: pravin khot 
    Created On: 23/Feb/2016
    Modification Log:
    --------------------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ---------------------------------------------------------------------------------------------------------------------------------------------
 * ----------------------------------------------------------------------------------------------------------------------------------------------
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using System.Collections;
namespace TPS360.DataAccess
{
    #region IUserRoleMapEditorDataAccess
    public interface IUserRoleMapEditorDataAccess
    {
        UserRoleMapEditor Add(UserRoleMapEditor UserRoleMapping);

        UserRoleMapEditor Update(UserRoleMapEditor UserRoleMapping);

        bool DeleteById(int id);

        PagedResponse<UserRoleMapEditor> GetPaged(PagedRequest request);

        UserRoleMapEditor GetAllByUserRoleMappingId(int id);

        ArrayList GetUserByUserRoleid(int buid, int jobpostingId);

        ArrayList GetUserNameByUserRoleid(int buid, int jobpostingId);

        int UserRoleMapping_Id(int RoleId);
    }
    #endregion
}
