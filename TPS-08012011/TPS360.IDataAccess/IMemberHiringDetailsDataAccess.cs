﻿using System;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using System.Collections;
namespace TPS360.DataAccess
{
    #region IMemberHiringDetailsDataAccess

    public interface IMemberHiringDetailsDataAccess
    {
        void  Add(MemberHiringDetails memberHiringDetails,string MemberId);

        MemberHiringDetails Update(MemberHiringDetails memberHiringDetails);

        MemberHiringDetails GetById(int id);

        MemberHiringDetails GetByMemberIdAndJobPosstingID(int memberId,int JobPostingId);

        bool DeleteHiringDetialsById(int HiringDetailsID);

        PagedResponse<MemberHiringDetails> GetPaged(PagedRequest PageRequest);

        
        
        PagedResponse<MemberOfferJoinDetails > GetPagedOfferJoined(PagedRequest request);

        PagedResponse<MemberHiringDetails> GetPagedEmbeddedSalarydetails(PagedRequest request);

        PagedResponse<MemberHiringDetails>  GetPagedMechSalarydetails(PagedRequest request);

        PagedResponse<MemberOfferJoinDetails> GetPagedOfferJoinedCommon(PagedRequest request);

        MemberHiringDetails GetSalaryComponentsByJobpostingIdAndMemberId(int jobpostingId, int MemberId);

        MemberHiringDetails GetOfferDetailsByJobpostingIdAndMemberId(int jobpostingId, int MemberId);
        MemberHiringDetails GetJobDescriptionByMHDId(int Id);
        IList<MemberHiringDetails> GetAllManagementBand();

        IList<MemberHiringDetails> GetAllLocation();

        IList<MemberHiringDetails> GetAllManagementBandByManagementBandId(int ManagementBandId);

        IList<MemberHiringDetails> GetAllBand();

        //**********Code starts by Vishal Tripathy*****************
        PagedResponse<MemberHiringDetails> GetPagedMemberHriingDetails(PagedRequest request, int memberid, string approvalType, string SortColumn, string status);


        PagedResponse<MemberHiringDetails> GetPagedMemberHRApprovalDetails(PagedRequest request, int memberid, string approvalType, string SortColumn);

        PagedResponse<MemberHiringDetails> GetPagedDivisionHeadApprovalList(PagedRequest request, int memberid, string approvalType, string SortColumn);

        ArrayList GetPagedMemberHiringDetailsShowValue(int memberId,int memberLoginId);


        List<MemberHiringDetails> GetMemberHiringDetailsShowRecords(int memberId);

        int UpdateOfferApprovalStatus(int MemberHiringId, int MemberId, string OfferStatus, string Commenttext);
        int UpdateSalaryTemplate(double value, int Id);

        int SendApprovalEmail(int MemberHiringId, int MemberId, string OfferStatus, string Commenttext, int status, int AdminMemberid);

        int SendRecruiterManagerEmail(int MemberHiringId, int MemberId, string OfferStatus, string Commenttext, int status, int AdminMemberid);

        int SendApprovalHREmail(int MemberHiringId, int MemberId, string OfferStatus, string Commenttext, int status, int AdminMemberid);
        int SendApprovalDivisionEmail(int MemberHiringId, int MemberId, string OfferStatus, string Commenttext, int status,int AdminMemberid);
        string GetDemandTypeValue(int memberId, int jobpostingId);
        string GetCandidateIdOfHiringMatrixByStatusAndJobPostingId(int jobpostingId);
        //**********Code ends by Vishal Tripathy*****************
    }
    



    #endregion
}