﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    #region IMemberActivityTypeDataAccess

    public interface IMemberActivityTypeDataAccess
    {
        MemberActivityType Add(MemberActivityType memberActivityType);

        MemberActivityType Update(MemberActivityType memberActivityType);

        MemberActivityType GetById(int id);

        MemberActivityType GetByActivityType(int ActivityType);

        PagedResponse<MemberActivityType> GetPaged(PagedRequest request);

        IList<MemberActivityType> GetAll();

        bool DeleteById(int id);
    }

    #endregion
}