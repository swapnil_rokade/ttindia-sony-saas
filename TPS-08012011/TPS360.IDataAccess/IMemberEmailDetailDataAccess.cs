﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IMemberEmailDetailDataAccess

    public interface IMemberEmailDetailDataAccess
    {
        MemberEmailDetail Add(MemberEmailDetail memberEmailDetail);

        MemberEmailDetail Update(MemberEmailDetail memberEmailDetail);

        MemberEmailDetail GetById(int id);

        IList<MemberEmailDetail> GetAllByMemberEmailId(int memberEmailId);

        IList<MemberEmailDetail> GetAll();

        bool DeleteById(int id);

        bool DeleteByMemberEmailId(int memberEmailId);
    }

    #endregion
}