﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: IInterviewQuestionBankDataAccess.cs
    Description: This is the DataAccess  library page.
    Created By: Prasanth Kumar G
    Created On: 15/Jul/2015
    Modification Log:
    --------------------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ---------------------------------------------------------------------------------------------------------------------------------------------
    
*/
using System.Collections.Generic;
using TPS360.Common.Shared;
using TPS360.Common.BusinessEntities;



namespace TPS360.DataAccess
{
    #region IInterviewQuestionBankDataAccess

    public interface IInterviewQuestionBankDataAccess
    {
        void Add(InterviewQuestionBank InterviewQuestionBank);

        void Update(InterviewQuestionBank InterviewQuestionBank);

        InterviewQuestionBank GetById(int id);

        IList<InterviewQuestionBank> GetAll();

        PagedResponse<InterviewQuestionBank> GetPaged(PagedRequest request);

        IList<InterviewQuestionBank> GetByInterviewId(int InterviewId);
        void DeleteById(int id);
    }

    #endregion
}