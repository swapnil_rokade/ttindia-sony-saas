﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    #region IJobPostingNoteDataAccess

    public interface IJobPostingNoteDataAccess
    {
        JobPostingNote Add(JobPostingNote jobPostingNote);

        JobPostingNote GetById(int id);

        IList<JobPostingNote> GetAll();

        IList<JobPostingNote> GetAllByJobPostingId(int jobPostingId);

        bool DeleteById(int id);

        bool DeleteByJobPostingId(int jobPostingId);

    }

    #endregion

    #region IReqNotesDataAccess

    public interface IReqNotesDataAccess
    {
        PagedResponse<RequisitionNotesEntry> GetAllReqNotesById(PagedRequest pagereq);
    }

    #endregion
}