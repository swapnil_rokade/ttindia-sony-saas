﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using System;

namespace TPS360.DataAccess
{
    #region IExternalViewStagesDataAccess
    public interface IExternalViewStagesDataAccess
    {
        ExternalViewStages Add(ExternalViewStages externalStages);
        ExternalViewStages Update(ExternalViewStages externalStages);
        bool DeleteById(int id);
        IList<ExternalViewStages> GetAllExternalStages();
    }
    #endregion
}
