﻿using System;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
namespace TPS360.DataAccess
{
    #region IMemberOfferRejectionDataAccess

    public interface IMemberOfferRejectionDataAccess
    {
        void  Add(MemberOfferRejection memberOfferRejection,string MemberId);

        MemberOfferRejection GetById(int id);

        MemberOfferRejection GetByMemberIdAndJobPosstingID(int memberId,int JobPostingId);

        //bool DeleteMemberOfferRejectionById(int HiringDetailsID);
    }

    #endregion
}