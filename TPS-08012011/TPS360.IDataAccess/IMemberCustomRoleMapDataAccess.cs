﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IMemberCustomRoleMapDataAccess

    public interface IMemberCustomRoleMapDataAccess
    {
        MemberCustomRoleMap Add(MemberCustomRoleMap memberCustomRoleMap);

        MemberCustomRoleMap Update(MemberCustomRoleMap memberCustomRoleMap);

        MemberCustomRoleMap GetById(int id);

        MemberCustomRoleMap GetByMemberId(int memberId);

        IList<MemberCustomRoleMap> GetAll();

        bool DeleteById(int id);
    }

    #endregion
}