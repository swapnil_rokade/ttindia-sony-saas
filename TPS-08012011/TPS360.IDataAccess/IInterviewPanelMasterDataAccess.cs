﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: IInterviewPanelMasterDataAccess.cs
    Description: This is the DataAccess  library page.
    Created By: Pravin khot
    Created On: 27/nov/2015
    Modification Log:
    --------------------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ---------------------------------------------------------------------------------------------------------------------------------------------
    
*/
using System.Collections.Generic;
using TPS360.Common.Shared;
using TPS360.Common.BusinessEntities;



namespace TPS360.DataAccess
{
    #region IInterviewPanelMasterDataAccess

    public interface IInterviewPanelMasterDataAccess
    {
        InterviewPanelMaster Add(InterviewPanelMaster InterviewPanelMaster);

        InterviewPanelMaster Update(InterviewPanelMaster InterviewPanelMaster);

        InterviewPanelMaster GetById(int id);

        IList<InterviewPanelMaster> GetAll();
        IList<InterviewPanelMaster> GetAll(string SortExpression);
        PagedResponse<InterviewPanelMaster> GetPaged(PagedRequest request);
        string GetSkills(int InterviewPanelMaster_ById);

        bool DeleteById(int InterviewPanelMaster_ById);

        int InterviewPanelMaster_Id(string InterviewPanel_Name);
        int InterviewPanelMaster_Id(string InterviewPanel_Name, int InterviewPanel_EditId);
    }

    #endregion
}