﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: IInterviewerFeedbackDataAccess.cs
    Description: This is the DataAccess  library page.
    Created By: Prasanth Kumar G
    Created On: 20/Jul/2015
    Modification Log:
    --------------------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ---------------------------------------------------------------------------------------------------------------------------------------------
    
*/
using System.Collections.Generic;
using TPS360.Common.Shared;
using TPS360.Common.BusinessEntities;



namespace TPS360.DataAccess
{
    #region IInterviewerFeedbackDataAccess

    public interface IInterviewerFeedbackDataAccess
    {
        void Add(InterviewFeedback interviewFeedback);

        void Update(InterviewFeedback interviewFeedback);

        InterviewFeedback GetById(int id);

        IList<InterviewFeedback> GetAll();

        PagedResponse<InterviewFeedback> GetPaged(PagedRequest request);
      
        IList<InterviewFeedback> GetByInterviewId(int InterviewId);

        void DeleteById(int id);
    }

    #endregion
}