﻿using System;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    #region ISavedQueryDataAccess

    public interface ISavedQueryDataAccess
    {
        SavedQuery Add(SavedQuery savedQuery);

        SavedQuery Update(SavedQuery savedQuery);

        SavedQuery GetById(int id);

        IList<SavedQuery> GetAll();

        PagedResponse<SavedQuery> GetPaged(PagedRequest request);

        bool DeleteById(int id);
    }

    #endregion
}