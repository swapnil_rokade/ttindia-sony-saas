﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName:ICountryDataAccess.cs
    Description: This page is used for Countries  functionality
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1            July-7-2009           Gopala Swamy J         Misc:Added 1) GetStateIdByStateCode() 2) GetCountryIdByCountryCode() to assist 
                                                                Resume parser team
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------        
*/

using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using System;

namespace TPS360.DataAccess
{
    #region IHiringMatrixLevelsDataAccess

    public interface IHiringMatrixLevelsDataAccess
    {
        HiringMatrixLevels Add(HiringMatrixLevels hiringMatrixLevels);

        HiringMatrixLevels Update(HiringMatrixLevels hiringMatrixLevels);

        HiringMatrixLevels GetById(int id);

        IList<HiringMatrixLevels> GetAll();

        //*********Rupesh Kadam – Candidate Stage Tracking – 18/7/2016 – Start *****************
        //IList<ExternalViewStages> GetAllExternalStages();
        //*********Rupesh Kadam – Candidate Stage Tracking – 18/7/2016 – End *****************

        bool DeleteById(int id);

        void DeleteAllHiringMatrix();

        HiringMatrixLevels GetPreviousLevelByID(int id);

        HiringMatrixLevels GetNextLevelByID(int id);

        HiringMatrixLevels GetInitialLevel();

        HiringMatrixLevels GetFinalLevel();

        int GetHiringMatrixLevelsCount();

        IList<HiringMatrixLevels> GetAllWithCount(int JobPostingId);

        HiringMatrixLevels GetLevelByName(string  LevelName);
    }

    #endregion
}