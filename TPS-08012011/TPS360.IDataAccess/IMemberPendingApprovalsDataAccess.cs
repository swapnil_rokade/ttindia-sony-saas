﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: IMemberPendingApprovalsDataAccess.cs
    Description: This is IMemberPendingApprovalsDataAccess
    Created By: Kanchan Yeware
    Created On: 20-Sept-2016
    Modification Log:   
------------------------------------------------------------------------------------------------------------------------------------------- 
*/
#region Namespaces
using System.Collections.Generic;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
#endregion

#region IMemberPendingApprovalsDataAccess
namespace TPS360.DataAccess
{
    public interface IMemberPendingApprovalsDataAccess
    {
        int GetByMemberIDandJobpostingId(int memid, int jobid);

        void AddInternalReferral(int MemberId, int JobpostingId);

        MemberPendingApprovals GetById(int id, int MemberID);

        void ApproveSubmissionForIJP(int MemberId);
    }
}
#endregion