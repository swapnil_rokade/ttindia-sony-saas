﻿using System;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IMemberAttendenceDataAccess

    public interface IMemberAttendenceDataAccess
    {
        MemberAttendence Add(MemberAttendence memberAttendence);

        MemberAttendence Update(MemberAttendence memberAttendence);

        MemberAttendence GetById(int id);

        MemberAttendence GetByAttendanceDateAndMemberId(DateTime attendanceDate, int memberId);

        IList<MemberAttendence> GetAll();

        IList<MemberAttendence> GetAllByMemberId(int memberId);

        bool DeleteById(int id);

        IList<MemberAttendence> GetByYearAndMemberId(Int32 year, Int32 memberId);
    }

    #endregion
}