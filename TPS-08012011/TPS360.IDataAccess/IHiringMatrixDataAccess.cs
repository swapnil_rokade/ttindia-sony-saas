﻿using System;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    #region IHiringMatrixDataAccess

    public interface IHiringMatrixDataAccess
    {
        PagedResponse<HiringMatrix> GetPaged(PagedRequest request);

        PagedResponse<HiringMatrix> MinGetPaged(PagedRequest request);

        HiringMatrix GetByJobPostingIDandCandidateId(int JobPostingId, int CandidateId);
        HiringMatrix getMatrixLevelByJobPostingIDAndCandidateId(int JobPostingId, int CandidateId);

    }

    #endregion
}