﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName:ICountryDataAccess.cs
    Description: This page is used for Countries  functionality
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1            July-7-2009           Gopala Swamy J         Misc:Added 1) GetStateIdByStateCode() 2) GetCountryIdByCountryCode() to assist 
                                                                Resume parser team
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------        
*/

using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using System;

namespace TPS360.DataAccess
{
    #region IPasswordResetDataAccess

    public interface IPasswordResetDataAccess
    {
        PasswordReset  Add(PasswordReset  passwordreset);
        PasswordReset  GetById(int id);
        PasswordReset Update(PasswordReset passwordreset);

    }

    #endregion
}