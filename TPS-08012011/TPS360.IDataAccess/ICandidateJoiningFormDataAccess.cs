﻿//*******************Suraj Adsule******Start********For Candidate Joining Form********20160805*********//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region ICandidateJoiningFormDataAccess
    public interface ICandidateJoiningFormDataAccess
    {
        void AddCandidateJoiningForm(CandidateJoiningDetails candidateJoiningDetail);
        CandidateJoiningDetails GetCandidateJoiningDetail(int candidateId, int demandID);
        bool DeleteCandidateJoiningDetail(int candidateId);
        bool UpdateCandidateJoiningForm(CandidateJoiningDetails candidateJoiningDetail);
    }
    #endregion
}
//*******************Suraj Adsule******End********For Candidate Joining Form********20160805*********//