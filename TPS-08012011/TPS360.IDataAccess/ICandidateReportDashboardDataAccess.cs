﻿using TPS360.Common.BusinessEntities;
using System;

namespace TPS360.DataAccess
{
    #region ICandidateReportDashboardDataAccess

    public interface ICandidateReportDashboardDataAccess
    {
        CandidateReportDashboard GetByParameter(DateTime addedFrom, DateTime addedTo, int addedBy, int addedBySource, DateTime updatedFrom, DateTime updatedTo, int updatedBy,
            int country, int state, string city, DateTime interviewFrom, DateTime interviewTo, int interviewLevel, int interviewStatus, int industry, int functionalCategory, int workPermit,
            int gender, int maritalStatus, int educationId, int assessmentId, bool isBroadcastedResume, bool isReferredApplicants, bool hasJobAgent,int assignedManager);
    }

    #endregion
}