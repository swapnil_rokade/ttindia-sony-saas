﻿using System.Collections.Generic;
using TPS360.Common.BusinessEntities;
using System;

namespace TPS360.DataAccess
{
    #region IExternalInternalStageMappingDataAccess
    public interface IExternalInternalStageMappingDataAccess
    {
        ExternalInternalStageMapping Add(ExternalInternalStageMapping externalStages);
        bool DeleteMappingByHiringStatus(string hiringLevel);
        IList<ExternalInternalStageMapping> GetAllExternalInternalStageMapping();
    }
    #endregion
}
