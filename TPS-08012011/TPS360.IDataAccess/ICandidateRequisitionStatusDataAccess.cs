﻿using TPS360.Common.BusinessEntities;
using System.Collections.Generic;
using TPS360.Common .Shared ;
namespace TPS360.DataAccess
{
    #region ICandidateRequisitionStatusDataAccess
    public interface ICandidateRequisitionStatusDataAccess
    {
        void Add(int JobPostingId,string MemberId,int CreatorId);

        void Update(int JobPostingId, string MemberId, int CreatorId, int StatusId);

        CandidateRequisitionStatus GetByRequisitionIdandMemberId(int RequisitionId, int MemberId);

        PagedResponse<CandidateRequisitionStatus> GetPaged(PagedRequest request, int ManagerId);
    }
    #endregion
}
