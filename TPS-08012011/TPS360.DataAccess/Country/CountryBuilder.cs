﻿using System.Data;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    internal sealed class CountryBuilder : IEntityBuilder<Country>
    {
        IList<Country> IEntityBuilder<Country>.BuildEntities(IDataReader reader)
        {
            List<Country> countrys = new List<Country>();

            while (reader.Read())
            {
                countrys.Add(((IEntityBuilder<Country>)this).BuildEntity(reader));
            }

            return (countrys.Count > 0) ? countrys : null;
        }

        Country IEntityBuilder<Country>.BuildEntity(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_NAME = 1;
            const int FLD_COUNTRYCODE = 2;

            Country country = new Country();

            country.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            country.Name = reader.IsDBNull(FLD_NAME) ? string.Empty : reader.GetString(FLD_NAME);
            country.CountryCode = reader.IsDBNull(FLD_COUNTRYCODE) ? string.Empty : reader.GetString(FLD_COUNTRYCODE);

            return country;
        }
    }
}