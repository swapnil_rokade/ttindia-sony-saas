﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;
using System.Collections;
using Microsoft.Practices.EnterpriseLibrary.Data;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Helper;

namespace TPS360.DataAccess
{
    internal sealed class MemberPendingApprovalsDataAccess : BaseDataAccess, IMemberPendingApprovalsDataAccess
    {
        public MemberPendingApprovalsDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<MemberPendingApprovals> CreateEntityBuilder<MemberPendingApprovals>()
        {
            return (new MemberPendingApprovalsBuilder()) as IEntityBuilder<MemberPendingApprovals>;
        }

        int IMemberPendingApprovalsDataAccess.GetByMemberIDandJobpostingId(int MemberId, int JobpostingId)
        {
            const string SP = "dbo.InternalJobposting_GetByMemberIDandJobpostingId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, MemberId);
                Database.AddInParameter(cmd, "@JobpostingId", DbType.Int32, JobpostingId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    int JobPostingId = 0;

                    while (reader.Read())
                    {
                        JobPostingId = reader.GetInt32(0);
                    }

                    return JobPostingId;
                }
            }
        }

        void IMemberPendingApprovalsDataAccess.AddInternalReferral(int MemberId, int JobpostingId)
        {
            const string SP = "dbo.InternalJobpostingReferral_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, MemberId);
                Database.AddInParameter(cmd, "@JobpostingId", DbType.Int32, JobpostingId);

                Database.ExecuteNonQuery(cmd);
            }
        }

        void IMemberPendingApprovalsDataAccess.ApproveSubmissionForIJP(int Id)
        {

            if (Id < 0)
            {
                throw new ArgumentException("MemberId");
            }

            const string SP = "dbo.ApprovalSubmission_AcceptMemberForIJP";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, Id);
                Database.ExecuteNonQuery(cmd);
            }
        }

        MemberPendingApprovals IMemberPendingApprovalsDataAccess.GetById(int Id, int MemberID)
        {
            const string SP = "dbo.InternalJobposting_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, Id);
                Database.AddInParameter(cmd, "@MemberID", DbType.Int32, MemberID);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    MemberPendingApprovals response = new MemberPendingApprovals();

                    while (reader.Read())
                    {
                        response.MemberId = reader.GetInt32(0);
                        response.JobPostingId = reader.GetInt32(1);
                    }

                    return response;
                }
            }
        }
    }
}
