﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: MemberPendingApprovalsBuilder.cs
    Description: This is Builder Class used for MemberPendingApprovals
    Created By: Kanchan Yeware
    Created On: 20-Sept-2016
    Modification Log:   
------------------------------------------------------------------------------------------------------------------------------------------- 
*/
#region Namespaces
using System;
using System.Data;
using System.Collections.Generic;
using TPS360.Common.BusinessEntities;
#endregion

#region MemberPendingApprovalsBuilder
namespace TPS360.DataAccess
{
    internal sealed class MemberPendingApprovalsBuilder : IEntityBuilder<MemberPendingApprovals>
    {
        #region Methods
        IList<MemberPendingApprovals> IEntityBuilder<MemberPendingApprovals>.BuildEntities(IDataReader reader)
        {
            List<MemberPendingApprovals> MemberPendingApprovals = new List<MemberPendingApprovals>();

            while (reader.Read())
            {
                MemberPendingApprovals.Add(((IEntityBuilder<MemberPendingApprovals>)this).BuildEntity(reader));
            }

            return (MemberPendingApprovals.Count > 0) ? MemberPendingApprovals : null;
        }

        MemberPendingApprovals IEntityBuilder<MemberPendingApprovals>.BuildEntity(IDataReader reader)
        {
            MemberPendingApprovals mp = new MemberPendingApprovals();

            if (reader.FieldCount == 13)
            {
                const int FLD_TRANSITIONID = 0;
                const int FLD_CURRENTAPPROVALSTAGEID = 1;
                const int FLD_ISSTATEAPPROVED = 2;
                const int FLD_DATESUBMITTED = 3;
                const int FLD_SUBMITTEDBY = 4;
                const int FLD_REQUISITION = 5;
                const int FLD_OLDSTAGENAME = 6;
                const int FLD_NEWSTAGENAME = 7;

                const int FLD_JOBPOSTINGCREATORID = 8;
                const int FLD_JOBPOSTINGID = 9;
                const int FLD_NEXTSTAGEDETAILID = 10;

                const int FLD_REQCODE = 11;
                const int FLD_CUSTOMERNAME = 12;

                mp.Id = reader.IsDBNull(FLD_TRANSITIONID) ? 0 : reader.GetInt32(FLD_TRANSITIONID);
                mp.CurrentApprovalStageId = reader.IsDBNull(FLD_CURRENTAPPROVALSTAGEID) ? 0 : reader.GetInt32(FLD_CURRENTAPPROVALSTAGEID);
                mp.IsStateApprovedByUser = reader.IsDBNull(FLD_ISSTATEAPPROVED) ? false : reader.GetBoolean(FLD_ISSTATEAPPROVED);
                mp.DateSubmitted = reader.IsDBNull(FLD_DATESUBMITTED) ? DateTime.MinValue : reader.GetDateTime(FLD_DATESUBMITTED);
                mp.SubmittedBy = reader.IsDBNull(FLD_SUBMITTEDBY) ? string.Empty : reader.GetString(FLD_SUBMITTEDBY);
                mp.Requisition = reader.IsDBNull(FLD_REQUISITION) ? string.Empty : reader.GetString(FLD_REQUISITION);
                mp.OldStageName = reader.IsDBNull(FLD_OLDSTAGENAME) ? string.Empty : reader.GetString(FLD_OLDSTAGENAME);
                mp.NewStageName = reader.IsDBNull(FLD_NEWSTAGENAME) ? string.Empty : reader.GetString(FLD_NEWSTAGENAME);
                mp.JobpostingCreatorid = reader.IsDBNull(FLD_JOBPOSTINGCREATORID) ? 0 : reader.GetInt32(FLD_JOBPOSTINGCREATORID);
                mp.JobPostingId = reader.IsDBNull(FLD_JOBPOSTINGID) ? 0 : reader.GetInt32(FLD_JOBPOSTINGID);
                mp.NextStageDetailId = reader.IsDBNull(FLD_NEXTSTAGEDETAILID) ? 0 : reader.GetInt32(FLD_NEXTSTAGEDETAILID);
                mp.JobPostingCode = reader.IsDBNull(FLD_REQCODE) ? string.Empty : reader.GetString(FLD_REQCODE);
                mp.CustomerName = reader.IsDBNull(FLD_CUSTOMERNAME) ? string.Empty : reader.GetString(FLD_CUSTOMERNAME);

            }
            else
            {
                const int FLD_ID = 0;
                const int FLD_MEMBERID = 1;
                const int FLD_MEMBERNAME = 2;
                const int FLD_JOBPOSTINGID = 3;
                const int FLD_REQUISITION = 4;

                mp.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
                mp.MemberId = reader.IsDBNull(FLD_MEMBERID) ? 0 : reader.GetInt32(FLD_MEMBERID);
                mp.MemberName = reader.IsDBNull(FLD_MEMBERNAME) ? string.Empty : reader.GetString(FLD_MEMBERNAME);
                mp.JobPostingId = reader.IsDBNull(FLD_JOBPOSTINGID) ? 0 : reader.GetInt32(FLD_JOBPOSTINGID);
                mp.Requisition = reader.IsDBNull(FLD_REQUISITION) ? string.Empty : reader.GetString(FLD_REQUISITION);
            }
            return mp;
        }
        #endregion
    }
}
#endregion
