﻿using System;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Text;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    internal sealed class HiringMatrixDataAccess : BaseDataAccess, IHiringMatrixDataAccess
    {
        #region Constructors

        public HiringMatrixDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<HiringMatrix> CreateEntityBuilder<HiringMatrix>()
        {
            return (new HiringMatrixBuilder()) as IEntityBuilder<HiringMatrix>;
        }

        #endregion

        #region  Methods
        PagedResponse<HiringMatrix> IHiringMatrixDataAccess.MinGetPaged(PagedRequest request)
        {
            const string SP = "dbo.HiringMatrix_minGetPaged";
            string whereClause = string.Empty;
            string jobpostingid = "";
            if (request.Conditions.Count > 0)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();

                string column = null;
                string value = null;

                foreach (DictionaryEntry entry in request.Conditions)
                {
                    column = StringHelper.Convert(entry.Key);
                    value = StringHelper.Convert(entry.Value);

                    if (!StringHelper.IsBlank(column))
                    {
                        value = value.Replace("'", "''");

                        if (StringHelper.IsEqual(column, "JobPostingId"))
                        {
                            if (sb.ToString() != String.Empty)
                            {
                                sb.Append(" AND ");
                            }
                            sb.Append("[MJC].[JobPostingId]");
                            sb.Append(" = ");
                            sb.Append(value);
                            jobpostingid = value;
                        }
                        else if (StringHelper.IsEqual(column, "SelectionStepLookupId"))
                        {
                            if (sb.ToString() != String.Empty)
                            {
                                sb.Append(" AND ");
                            }
                            sb.Append("[MJCD].[SelectionStepLookupId]");
                            sb.Append(" = ");
                            sb.Append(value);
                        }
                    }
                }

                whereClause = sb.ToString();
            }

            if (StringHelper.IsBlank(request.SortColumn))
            {
                request.SortColumn = "MatchingPercentage";
            }
            if (StringHelper.IsBlank(request.SortOrder))
            {
                request.SortOrder = "Desc";
            }
            request.SortColumn = request.SortColumn;

            object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder),
                                                    StringHelper .Convert (jobpostingid ) 
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<HiringMatrix> response = new PagedResponse<HiringMatrix>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = CreateEntityBuilder<HiringMatrix>().BuildEntities(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }




        PagedResponse<HiringMatrix> IHiringMatrixDataAccess.GetPaged(PagedRequest request)
        {
            const string SP = "dbo.HiringMatrix_GetPaged";
            string whereClause = string.Empty;
            string jobpostingid="";
            if (request.Conditions.Count > 0)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();

                string column = null;
                string value = null;

                foreach (DictionaryEntry entry in request.Conditions)
                {
                    column = StringHelper.Convert(entry.Key);
                    value = StringHelper.Convert(entry.Value);

                    if (!StringHelper.IsBlank(column))
                    {
                        value = value.Replace("'", "''");

                        if (StringHelper.IsEqual(column, "JobPostingId"))
                        {
                            if (sb.ToString() != String.Empty)
                            {
                                sb.Append(" AND ");
                            }
                            sb.Append("[MJC].[JobPostingId]");
                            sb.Append(" = ");
                            sb.Append(value);   
                            jobpostingid=value ;
                        }
                        else if (StringHelper.IsEqual(column, "SelectionStepLookupId"))
                        {
                            if (sb.ToString() != String.Empty)
                            {
                                sb.Append(" AND ");
                            }
                            sb.Append("[MJCD].[SelectionStepLookupId]");
                            sb.Append(" = ");
                            sb.Append(value);
                        }                        
                    }
                }

                whereClause = sb.ToString();
            }

            if (StringHelper.IsBlank(request.SortColumn))
            {
                request.SortColumn = "MatchingPercentage";
            }
            if (StringHelper.IsBlank(request.SortOrder ))
            {
                request.SortOrder  = "Desc";
            }
            request.SortColumn = request.SortColumn;

            object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder),
                                                    StringHelper .Convert (jobpostingid ) 
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<HiringMatrix> response = new PagedResponse<HiringMatrix>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = CreateEntityBuilder<HiringMatrix>().BuildEntities(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }

        HiringMatrix IHiringMatrixDataAccess.GetByJobPostingIDandCandidateId(int JobPostingId, int CandidateId)
        {

            const string SP = "dbo.HiringMatrix_GetByCandidateIdAndJobPostingID";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@JobPostingId", DbType.Int32, JobPostingId );
                Database.AddInParameter(cmd, "@CandidateId", DbType.Int32, CandidateId );

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<HiringMatrix >()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }
        ////////////////////////////Sumit Sonawane 1/Mar/2017///////////////////////////////////////////////////////////////////
        HiringMatrix IHiringMatrixDataAccess.getMatrixLevelByJobPostingIDAndCandidateId(int JobPostingId, int cid)
        {

            const string SP = "dbo.getMatrixLevelByJobPostingIDAndCandidateId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@JobPostingId", DbType.Int32, JobPostingId);
                Database.AddInParameter(cmd, "@cid", DbType.Int32, cid);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {

                    HiringMatrix rp = new HiringMatrix();
                    while (reader.Read())
                    {
                        rp.Id = reader.GetInt32(0);
                    }
                    return rp;
                }
            }
        }


      
        #endregion
    }
}