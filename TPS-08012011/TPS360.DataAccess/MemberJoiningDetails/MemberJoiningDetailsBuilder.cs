﻿using System;
using System.Data;
using System.Collections.Generic;
using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    internal sealed class MemberJoiningDetailsBuilder : IEntityBuilder<MemberJoiningDetail>
    {
        IList<MemberJoiningDetail> IEntityBuilder<MemberJoiningDetail>.BuildEntities(IDataReader reader)
        {
            List<MemberJoiningDetail> MemberJoiningDetail = new List<MemberJoiningDetail>();

            while (reader.Read())
            {
                MemberJoiningDetail.Add(((IEntityBuilder<MemberJoiningDetail>)this).BuildEntity(reader));
            }

            return (MemberJoiningDetail.Count > 0) ? MemberJoiningDetail : null;
        }

        MemberJoiningDetail IEntityBuilder<MemberJoiningDetail>.BuildEntity(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_MEMBERID = 1;
            const int FLD_JOBPOSTINGID = 2;
            const int FLD_OFFEREDSALARY = 3;
            const int FLD_BILLABLESALARY = 4;
            const int FLD_BILLINGRATE = 5;
            const int FLD_REVENUE = 6;
            const int FLD_JOININGDATE = 7;
            const int FLD_ISREMOVED = 8;
            const int FLD_CREATORID = 9;
            const int FLD_UPDATORID = 10;
            const int FLD_CREATEDATE = 11;
            const int FLD_UPDATEDATE = 12;
            const int FLD_OFFEREDSALARYPAYCYCLE = 13;
            const int FLD_OFFEREDSALARYCURRENCY = 14;
            const int FLD_BILLABLESALARYCURRENCY = 15;
            const int FLD_REVENUECURRENCY = 16;
            const int FLD_PSID = 17;
            const int FLD_LOCATIOM = 18;
            const int FLD_OTHERLOCATION = 19;
            MemberJoiningDetail MemberJoiningDetail = new MemberJoiningDetail();

            MemberJoiningDetail.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            MemberJoiningDetail.MemberId = reader.IsDBNull(FLD_MEMBERID) ? 0 : reader.GetInt32(FLD_MEMBERID);
            MemberJoiningDetail.JobPostingId = reader.IsDBNull(FLD_JOBPOSTINGID) ? 0 : reader.GetInt32(FLD_JOBPOSTINGID);
            MemberJoiningDetail.OfferedSalary = reader.IsDBNull(FLD_OFFEREDSALARY) ? string.Empty : reader.GetString(FLD_OFFEREDSALARY);
            MemberJoiningDetail.BillableSalary = reader.IsDBNull(FLD_BILLABLESALARY) ? string.Empty : reader.GetString(FLD_BILLABLESALARY);
            MemberJoiningDetail.BillingRate = reader.IsDBNull(FLD_BILLINGRATE) ? string.Empty : reader.GetString(FLD_BILLINGRATE);
            MemberJoiningDetail.Revenue = reader.IsDBNull(FLD_REVENUE) ? string.Empty : reader.GetString(FLD_REVENUE);
            MemberJoiningDetail.JoiningDate = reader.IsDBNull(FLD_JOININGDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_JOININGDATE);
            MemberJoiningDetail.IsRemoved = reader.IsDBNull(FLD_ISREMOVED) ? false : reader.GetBoolean(FLD_ISREMOVED);
            MemberJoiningDetail.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
            MemberJoiningDetail.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);
            MemberJoiningDetail.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
            MemberJoiningDetail.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);
            MemberJoiningDetail.OfferedSalaryPayCycle = reader.IsDBNull(FLD_OFFEREDSALARYPAYCYCLE) ? 0 : reader.GetInt32(FLD_OFFEREDSALARYPAYCYCLE);
            MemberJoiningDetail.OfferedSalaryCurrency = reader.IsDBNull(FLD_OFFEREDSALARYCURRENCY) ? 0 : reader.GetInt32(FLD_OFFEREDSALARYCURRENCY);
            MemberJoiningDetail.BillableSalaryCurrency = reader.IsDBNull(FLD_BILLABLESALARYCURRENCY) ? 0 : reader.GetInt32(FLD_BILLABLESALARYCURRENCY);
            MemberJoiningDetail.RevenueCurrency = reader.IsDBNull(FLD_REVENUECURRENCY) ? 0 : reader.GetInt32(FLD_REVENUECURRENCY);
            MemberJoiningDetail.PSID = reader.IsDBNull(FLD_PSID) ? string.Empty : reader.GetString(FLD_PSID);
            MemberJoiningDetail.Location = reader.IsDBNull(FLD_LOCATIOM) ? 0 : reader.GetInt32(FLD_LOCATIOM);
            MemberJoiningDetail.OtherLocation = reader.IsDBNull(FLD_OTHERLOCATION) ? string.Empty : reader.GetString (FLD_OTHERLOCATION);
            return MemberJoiningDetail;
        }
    }
}
