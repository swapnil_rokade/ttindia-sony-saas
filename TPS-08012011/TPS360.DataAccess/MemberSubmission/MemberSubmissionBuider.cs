﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{

    internal sealed class MemberSubmissionBuider : IEntityBuilder<MemberSubmission>
    {
        IList<MemberSubmission> IEntityBuilder<MemberSubmission>.BuildEntities(IDataReader reader)
        {
            List<MemberSubmission> memberSubmission = new List<MemberSubmission>();

            while (reader.Read())
            {
                memberSubmission.Add(((IEntityBuilder<MemberSubmission>)this).BuildEntity(reader));
            }

            return (memberSubmission.Count > 0) ? memberSubmission : null;
        }

        MemberSubmission IEntityBuilder<MemberSubmission>.BuildEntity(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_MEMBERID = 1;
            const int FLD_JOBPOSTINGID = 2;
            const int FLD_ISREMOVED = 3;
            const int FLD_CREATORID = 4;
            const int FLD_UPDATORID = 5;
            const int FLD_CREATEDATE = 6;
            const int FLD_UPDATEDATE = 7;
            const int FLD_RECEIVEREMAIL = 8;
            const int FLD_SUBMITTEDDATE = 9;
            const int FLD_COMPANYID = 10;

            MemberSubmission memberSubmission = new MemberSubmission();

            memberSubmission.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            memberSubmission.MemberId = reader.IsDBNull(FLD_MEMBERID) ? 0 : reader.GetInt32(FLD_MEMBERID);
            memberSubmission.JobPostingId = reader.IsDBNull(FLD_MEMBERID) ? 0 : reader.GetInt32(FLD_JOBPOSTINGID);
            memberSubmission.IsRemoved = reader.IsDBNull(FLD_ISREMOVED) ? false : reader.GetBoolean(FLD_ISREMOVED);
            memberSubmission.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
            memberSubmission.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);
            memberSubmission.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
            memberSubmission.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);
            memberSubmission.ReceiverEmail = reader.IsDBNull(FLD_RECEIVEREMAIL) ? string.Empty : reader.GetString(FLD_RECEIVEREMAIL);//12129
            memberSubmission.SubmittedDate = reader.IsDBNull(FLD_SUBMITTEDDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_SUBMITTEDDATE);
            memberSubmission.CompanyId = reader.IsDBNull(FLD_COMPANYID) ? 0 : reader.GetInt32(FLD_COMPANYID);
            return memberSubmission;
        }
       

    }
}
