﻿using System;
using System.Data;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    internal sealed class CandidateRequisitionStatusBuilder : IEntityBuilder<CandidateRequisitionStatus>
    {
        IList<CandidateRequisitionStatus> IEntityBuilder<CandidateRequisitionStatus>.BuildEntities(IDataReader reader)
        {
            List<CandidateRequisitionStatus> Status = new List<CandidateRequisitionStatus>();

            while (reader.Read())
            {
                Status.Add(((IEntityBuilder<CandidateRequisitionStatus>)this).BuildEntity(reader));
            }

            return (Status.Count > 0) ? Status : null;
        }
        CandidateRequisitionStatus IEntityBuilder<CandidateRequisitionStatus>.BuildEntity(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_JOBPOSTINGID = 1;
            const int FLD_MEMBERID = 2;
            const int FLD_STATUSID = 3;
            const int FLD_CREATORID = 4;
            const int FLD_UPDATORID = 5;
            const int FLD_CREATEDATE = 6;
            const int FLD_UPDATEDATE = 7;
            const int FLD_ISREMOVED = 8;


            const int FLD_CandidateName = 9;
            const int FLD_JobTitle = 10;
            const int FLD_LevelName = 11;
            const int FLD_AddedToRequisitionDate = 12;
            const int FLD_RejectedDate = 13;
            const int FLD_UsersName = 14;
            CandidateRequisitionStatus CandidateStatus = new CandidateRequisitionStatus();

            CandidateStatus.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            CandidateStatus.JobPostingId = reader.IsDBNull(FLD_JOBPOSTINGID) ? 0 : reader.GetInt32(FLD_JOBPOSTINGID);
            CandidateStatus.MemberId = reader.IsDBNull(FLD_MEMBERID) ? 0 : reader.GetInt32(FLD_MEMBERID);
            CandidateStatus.StatusId = reader.IsDBNull(FLD_STATUSID) ? 0 : reader.GetInt32(FLD_STATUSID);
            CandidateStatus.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
            CandidateStatus.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);
            CandidateStatus.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
            CandidateStatus.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);
            CandidateStatus.IsRemoved = reader.IsDBNull(FLD_ISREMOVED) ? false : reader.GetBoolean(FLD_ISREMOVED);


            if (reader.FieldCount > FLD_CandidateName)
            {
                CandidateStatus.CandidateName = reader.IsDBNull(FLD_CandidateName) ? string.Empty : reader.GetString(FLD_CandidateName);
                CandidateStatus.JobTitle = reader.IsDBNull(FLD_JobTitle) ? string.Empty : reader.GetString(FLD_JobTitle);
                CandidateStatus.LevelName = reader.IsDBNull(FLD_LevelName) ? string.Empty : reader.GetString(FLD_LevelName);
                CandidateStatus.AddedToRequisitionDate = reader.IsDBNull(FLD_AddedToRequisitionDate) ? DateTime.MinValue : reader.GetDateTime (FLD_AddedToRequisitionDate);
                CandidateStatus.RejectedDate = reader.IsDBNull(FLD_RejectedDate) ? DateTime.MinValue : reader.GetDateTime(FLD_RejectedDate);
                CandidateStatus.UsersName = reader.IsDBNull(FLD_UsersName) ? string.Empty : reader.GetString(FLD_UsersName);
            }
            return CandidateStatus;
        }
        
    }
}
