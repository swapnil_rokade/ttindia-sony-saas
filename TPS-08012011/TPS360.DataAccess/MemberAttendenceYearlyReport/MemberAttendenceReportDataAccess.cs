﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    internal sealed class MemberAttendenceReportDataAccess : BaseDataAccess, IMemberAttendenceYearlyReport
    {
        #region Constructors

        public MemberAttendenceReportDataAccess(Context context)
            : base(context)
        {
        }


        protected override IEntityBuilder<MemberAttendenceYearlyReport> CreateEntityBuilder<MemberAttendenceYearlyReport>()
        {
            return (new MemberAttendenceReportBuilder()) as IEntityBuilder<MemberAttendenceYearlyReport>;
        }
        #endregion

        #region  Methods

        IList<MemberAttendenceYearlyReport> IMemberAttendenceYearlyReport.GetMemberAttendeceYearlyReport(Int32 year, Int32 memberId)
        {
            if (year < 1 || year > 9999)
            {
                throw new ArgumentNullException("year");
            }

            if (memberId < 1)
            {
                throw new ArgumentNullException("memberId");
            }

            const string SP = "dbo.MemberAttendence_YearlyReport";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Year", DbType.Int32, year);
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<MemberAttendenceYearlyReport>().BuildEntities(reader);
                }
            }
        }

        #endregion
    }
}