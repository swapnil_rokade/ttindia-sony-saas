﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: CandidateSourcingInfoDataAccess.cs
    Description: This page is used for candidate data access.
    Created By: 
    Created On:
    Modification Log:
    ----------------------------------------------------------------------------------------------------------------------------------------
    Ver.No.        Date               Author           Modification
    ----------------------------------------------------------------------------------------------------------------------------------------    
   
    ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 */

using System;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Text;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using TPS360.Common.BusinessEntity;
// using System.Web.Configuration;
namespace TPS360.DataAccess
{
    internal sealed class CandidateSourcingInfoDataAccess : BaseDataAccess, ICandidateSourcingInfoDataAccess
    {
        #region Constructors

        public CandidateSourcingInfoDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<CandidateSourcingInfo> CreateEntityBuilder<CandidateSourcingInfo>()
        {
            return (new CandidateSourcingInfoBuilder()) as IEntityBuilder<CandidateSourcingInfo>;
        }

        #endregion

        #region  Methods

       PagedResponse<CandidateSourcingInfo> ICandidateSourcingInfoDataAccess.GetPagedForSourcingInfoReport(PagedRequest request)
        {
            const string SP = "dbo.CandidateSourcing_GetPagedReport";
            string whereClause = string.Empty;

            if (request.Conditions.Count > 0)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();

                string column = null;
                string value = null;

                foreach (DictionaryEntry entry in request.Conditions)
                {
                    column = StringHelper.Convert(entry.Key);
                    value = StringHelper.Convert(entry.Value);
                    
                    if (StringHelper.IsEqual(column, "SourcedDateFrom"))
                    {
                        if (value != DateTime.MinValue.ToString())
                        {
                            if (sb.Length > 0)
                            {
                                sb.Append(" and ");
                            }
                            sb.Append(" [MJC].CreateDate");
                            sb.Append(" >= ");
                            value = "Convert(DateTime,'" + Convert.ToDateTime(value).ToString("dd/MM/yyyy") + "',103) ";
                            sb.Append(value);
                        }
                    }
                    if (StringHelper.IsEqual(column, "SourcedDateTo"))
                    {
                        if (value != DateTime.MinValue.ToString())
                        {
                            if (sb.Length > 0)
                            {
                                sb.Append(" and ");
                            }
                            sb.Append(" [MJC].CreateDate");
                            sb.Append(" < ");
                            value = "Convert(DateTime,'" + Convert.ToDateTime(value).AddDays(1).ToString("dd/MM/yyyy") + "',103) ";
                            sb.Append(value);

                        }
                    }
                    if (StringHelper.IsEqual(column, "SubmissionFrom"))
                    {
                        if (value != DateTime.MinValue.ToString())
                        {
                            if (sb.Length > 0)
                            {
                                sb.Append(" and ");
                            }
                            sb.Append(" [MS].SubmittedDate");
                            sb.Append(" >= ");
                            value = "Convert(DateTime,'" + Convert.ToDateTime(value).ToString("dd/MM/yyyy") + "',103) ";
                            sb.Append(value);
                        }
                    }
                    if (StringHelper.IsEqual(column, "SubmissionTo"))
                    {
                        if (value != DateTime.MinValue.ToString())
                        {
                            if (sb.Length > 0)
                            {
                                sb.Append(" and ");
                            }
                            sb.Append(" [MS].SubmittedDate");
                            sb.Append(" < ");
                            value = "Convert(DateTime,'" + Convert.ToDateTime(value).AddDays(1).ToString("dd/MM/yyyy") + "',103) ";
                            sb.Append(value);

                        }
                    }
                    if (StringHelper.IsEqual(column, "OfferedFrom"))
                    {
                        if (value != DateTime.MinValue.ToString())
                        {
                            if (sb.Length > 0)
                            {
                                sb.Append(" and ");
                            }
                            sb.Append(" [MH].OfferedDate");
                            sb.Append(" >= ");
                            value = "Convert(DateTime,'" + Convert.ToDateTime(value).ToString("dd/MM/yyyy") + "',103) ";
                            sb.Append(value);
                        }
                    }
                    if (StringHelper.IsEqual(column, "OfferedTo"))
                    {
                        if (value != DateTime.MinValue.ToString())
                        {
                            if (sb.Length > 0)
                            {
                                sb.Append(" and ");
                            }
                            sb.Append(" [MH].OfferedDate");
                            sb.Append(" < ");
                            value = "Convert(DateTime,'" + Convert.ToDateTime(value).AddDays(1).ToString("dd/MM/yyyy") + "',103) ";
                            sb.Append(value);

                        }
                    }
                    if (StringHelper.IsEqual(column, "JoinedFrom"))
                    {
                        if (value != DateTime.MinValue.ToString())
                        {
                            if (sb.Length > 0)
                            {
                                sb.Append(" and ");
                            }
                            sb.Append(" [MJ].JoiningDate");
                            sb.Append(" >= ");
                            value = "Convert(DateTime,'" + Convert.ToDateTime(value).ToString("dd/MM/yyyy") + "',103) ";
                            sb.Append(value);
                        }
                    }
                    if (StringHelper.IsEqual(column, "JoinedTo"))
                    {
                        if (value != DateTime.MinValue.ToString())
                        {
                            if (sb.Length > 0)
                            {
                                sb.Append(" and ");
                            }
                            sb.Append(" [MJ].JoiningDate");
                            sb.Append(" < ");
                            value = "Convert(DateTime,'" + Convert.ToDateTime(value).AddDays(1).ToString("dd/MM/yyyy") + "',103) ";
                            sb.Append(value);

                        }
                    }
                    if (StringHelper.IsEqual(column, "ByRequisition"))
                    {
                        if (value != string.Empty && value !="0")
                        {
                            if (sb.Length > 0)
                            {
                                sb.Append(" AND ");
                            }
                            sb.Append("[J].[Id]");
                            sb.Append(" = ");

                            sb.Append(value);
                        }
                    }
                    if (StringHelper.IsEqual(column, "ByAccount"))
                    {
                        if (value != string.Empty && value != "0")
                        {
                            if (sb.Length > 0)
                            {
                                sb.Append(" AND ");
                            }
                            sb.Append("[J].ClientId ");
                            sb.Append(" in (");
                            sb.Append(value);
                            sb.Append(")");
                            //sb.Append(" = ");
                            //sb.Append(value);
                        }
                    }
                    if (StringHelper.IsEqual(column, "ByTopLevelStatus"))
                    {
                        if (value != string.Empty && value != "0")
                        {
                            if (sb.Length > 0)
                            {
                                sb.Append(" AND ");
                            }
                            sb.Append("[MJD].SelectionStepLookupId  ");
                            sb.Append(" = ");
                            sb.Append(value);
                        }
                    }
                    if (StringHelper.IsEqual(column, "BySubLevelStatus"))
                    {
                        if (value != string.Empty && value != "0")
                        {
                            if (sb.Length > 0)
                            {
                                sb.Append(" AND ");
                            }
                            sb.Append("[MJD].SelectionStepLookupId  ");
                            sb.Append(" = ");
                            sb.Append(value);
                        }
                    }
                    if (StringHelper.IsEqual(column, "ByRecruiter"))
                    {
                        if (value != string.Empty && value != "0")
                        {
                            if (sb.Length > 0)
                            {
                                sb.Append(" AND ");
                            }
                            sb.Append("[MJD].AddedBy ");
                            sb.Append(" = ");
                            sb.Append(value);
                        }
                    }
                    if (StringHelper.IsEqual(column, "ByLead"))
                    {
                        if (value != string.Empty && value != "0")
                        {
                            if (sb.Length > 0)
                            {
                                sb.Append(" AND ");
                            }
                            sb.Append("[EMP].Id  ");
                            sb.Append(" = ");
                            sb.Append(value);
                        }
                    }
                    if (StringHelper.IsEqual(column, "ByCandidateName"))
                    {
                        if (value != string.Empty )
                        {
                            if (sb.Length > 0)
                            {
                                sb.Append(" AND ");
                            }
                            if (value.Contains(" "))
                            {
                                string[] names = value.Split(null);
                                sb.Append("[C].[CandidateName] like '");
                                sb.Append(names[0]);
                                sb.Append("%");
                                sb.Append(names[1]);
                                sb.Append("%'");
                            }
                            else
                            {
                                sb.Append("[C].[CandidateName] like '");
                                sb.Append(value);
                                sb.Append("%'");
                            }
                        }
                    }
                    if (StringHelper.IsEqual(column, "ByLocation"))
                    {
                        if (value != string.Empty)
                        {
                            if (sb.Length > 0)
                            {
                                sb.Append(" AND ");
                            }
                            sb.Append("[C].[PermanentCity] like '");
                            sb.Append(value);
                            sb.Append("%'");
                        }
                    }

                    if (StringHelper.IsEqual(column, "CompanyStatus"))
                    {
                        if (value != string.Empty)
                        {
                            if (sb.Length > 0)
                            {
                                sb.Append(" AND ");
                            }
                            sb.Append("[CY].[CompanyStatus]");
                            sb.Append(" = ");
                            sb.Append(value);
                        }
                    }
                }

                whereClause = sb.ToString();
            }

            if (StringHelper.IsBlank(request.SortColumn))
            {
                request.SortColumn = "[C].[FirstName]";
            }

            request.SortColumn = request.SortColumn;

            object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder)

												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<CandidateSourcingInfo> response = new PagedResponse<CandidateSourcingInfo>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = CreateEntityBuilder<CandidateSourcingInfo>().BuildEntities(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }

       
       
        #endregion
    }
}