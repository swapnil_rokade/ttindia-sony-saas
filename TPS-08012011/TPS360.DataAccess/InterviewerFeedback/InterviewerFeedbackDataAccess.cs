﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName            :   InterviewerFeedbackDataAccess.cs
    Description         :   This page is used Call the Stored Proceedures for Interviewer Feed back.
    Created By          :   Prasanth
    Created On          :   20/Jul/2015
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
   0.1              28/Dec/2015             Prasanth Kumar G    Introduced OverallFeedback
 ---------------------------------------------------------------------------------------------------------------------------------------------------------------------        
*/
using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;
using Microsoft.Practices.EnterpriseLibrary.Data;
using TPS360.Common.Shared;
using TPS360.Common.Helper;
using System.Collections;
using System.Text;
using TPS360.Common.BusinessEntities;
namespace TPS360.DataAccess
{
    internal sealed class InterviewerFeedbackDataAccess : BaseDataAccess,IInterviewerFeedbackDataAccess
    {
         #region Constructors

        public InterviewerFeedbackDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<InterviewFeedback> CreateEntityBuilder<InterviewFeedback>()
        {
            return (new InterviewerFeedbackBuilder()) as IEntityBuilder<InterviewFeedback>;
        }

        #endregion

        #region  Methods
        void  IInterviewerFeedbackDataAccess.Add(InterviewFeedback interviewFeedback) 
        {
            const string SP = "dbo.InterviewerFeedback_Create";
            using (DbCommand cmd = Database.GetStoredProcCommand(SP)) 
            {
                //Database.AddInParameter(cmd, "@Title", DbType.String, interviewFeedback.Title);
                Database.AddInParameter(cmd, "@InterviewFeedback", DbType.String, interviewFeedback.Feedback);
                Database.AddInParameter(cmd, "@InterviewRound", DbType.Int32, interviewFeedback.InterviewRound);

                Database.AddInParameter(cmd, "@FirstName", DbType.String, interviewFeedback.FirstName);
                Database.AddInParameter(cmd, "@LastName", DbType.String, interviewFeedback.LastName);
                Database.AddInParameter(cmd, "@EmailId", DbType.String, interviewFeedback.Email);
                Database.AddInParameter(cmd, "@EmployeeId", DbType.String, interviewFeedback.EmployeeID);
                Database.AddInParameter(cmd, "@DocumentName", DbType.String, interviewFeedback.DocumentName);

                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, interviewFeedback.CreatorId);
                Database.AddInParameter(cmd, "@UpdatorId", DbType.Int32, interviewFeedback.UpdatorId);
                Database.AddInParameter(cmd, "@InterviewId", DbType.Int32, interviewFeedback.InterviewId);
                Database.AddInParameter(cmd, "@OverallFeedback", DbType.String, interviewFeedback.OverallFeedback); //Line introduced by Prasanth on 28/Dec/2015
                Database.ExecuteReader(cmd);
             
            }
        
        }

        void IInterviewerFeedbackDataAccess.Update(InterviewFeedback interviewFeedback)
        {
            const string SP = "dbo.InterviewerFeedback_Update";
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, interviewFeedback.Id);
                Database.AddInParameter(cmd, "@InterviewFeedback", DbType.String, interviewFeedback.Feedback);
                Database.AddInParameter(cmd, "@InterviewRound", DbType.Int32, interviewFeedback.InterviewRound);

                Database.AddInParameter(cmd, "@FirstName", DbType.String, interviewFeedback.FirstName);
                Database.AddInParameter(cmd, "@LastName", DbType.String, interviewFeedback.LastName);
                Database.AddInParameter(cmd, "@EmailId", DbType.String, interviewFeedback.Email);
                Database.AddInParameter(cmd, "@EmployeeId", DbType.String, interviewFeedback.EmployeeID);
                Database.AddInParameter(cmd, "@DocumentName", DbType.String, interviewFeedback.DocumentName);

                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, interviewFeedback.CreatorId);
                Database.AddInParameter(cmd, "@UpdatorId", DbType.Int32, interviewFeedback.UpdatorId);
                Database.AddInParameter(cmd, "@InterviewId", DbType.Int32, interviewFeedback.InterviewId);
                
                Database.ExecuteReader(cmd);

            }

        }

        IList<InterviewFeedback> IInterviewerFeedbackDataAccess.GetAll()
        {
            const string SP = "dbo.InterviewerFeedback_GetAll";
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<InterviewFeedback>().BuildEntities(reader);
                }
            }

        }

        void IInterviewerFeedbackDataAccess.DeleteById(int Id)
        {
            if (Id < 1)
            {
                throw new ArgumentNullException("Id");
            }

            const string SP = "dbo.InterviewerFeedback_DeleteById";
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, Id);
                Database.ExecuteNonQuery(cmd);  
            }

        }

        InterviewFeedback IInterviewerFeedbackDataAccess.GetById(int Id)
        {
            if (Id < 1)
            {
                throw new ArgumentNullException("Id");
            }

            const string SP = "dbo.InterviewerFeedback_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, Id);
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<InterviewFeedback>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }

        }

        IList<InterviewFeedback> IInterviewerFeedbackDataAccess.GetByInterviewId(int InterviewId) 
        {
            if (InterviewId < 0) 
            {
                throw new ArgumentNullException("InterviewId");
            }
            const string SP = "dbo.InterviewerFeedback_GetByInterviewId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@InterviewId", DbType.Int32, InterviewId);
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                   
                        return CreateEntityBuilder<InterviewFeedback>().BuildEntities(reader);
               
                }
            }
        }

        PagedResponse<InterviewFeedback> IInterviewerFeedbackDataAccess.GetPaged(PagedRequest request)
        {
            const string SP = "dbo.InterviewerFeedback_GetPaged";
            
            string whereClause = string.Empty;

             if (request.Conditions.Count > 0)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();

                string column = null;
                string value = null;

                foreach (DictionaryEntry entry in request.Conditions)
                {
                    column = StringHelper.Convert(entry.Key);
                    value = StringHelper.Convert(entry.Value);

                    if (!StringHelper.IsBlank(column))
                    {
                        value = value.Replace("'", "''");

                        if (StringHelper.IsEqual(column, "InterviewId")) 
                        {
                            if (value.Trim() != "0")
                            {
                                if (sb.Length > 0)
                                {
                                    sb.Append(" and ");
                                }

                                sb.Append(" [I].interviewid ");
                                sb.Append(" = ");
                                sb.Append("'");
                                sb.Append(value);
                                sb.Append("'");
                               

                            }
                        }
                        if (StringHelper.IsEqual(column, "EmailId"))  
                        {
                            if (value.Trim() != "")
                            {
                                if (sb.Length > 0)
                                {
                                    sb.Append(" and ");
                                }
                                sb.Append(" [I].EmailId");
                                sb.Append(" = ");
                                sb.Append("'");
                                sb.Append(value);
                                sb.Append("'");
                            }
                        }

                        
                    }

                }
                whereClause = sb.ToString();

            }





            object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
                                                    //10,
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder)
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<InterviewFeedback> response = new PagedResponse<InterviewFeedback>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = CreateEntityBuilder<InterviewFeedback>().BuildEntities(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }
        #endregion
    }
}
