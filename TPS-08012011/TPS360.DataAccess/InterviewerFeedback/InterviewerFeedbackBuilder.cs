﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName            :   InterviewerFeedbackBuilder.cs
    Description         :   This page is used to fill up Interviewer Feed back Properties.
    Created By          :   Prasanth
    Created On          :   20/Jul/2015
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
  
 ---------------------------------------------------------------------------------------------------------------------------------------------------------------------        
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TPS360.Common.BusinessEntities;
using System.Data;

namespace TPS360.DataAccess
{
   

    internal sealed class InterviewerFeedbackBuilder : IEntityBuilder<InterviewFeedback>
    {
        IList<InterviewFeedback> IEntityBuilder<InterviewFeedback>.BuildEntities(IDataReader reader)
        {
            List<InterviewFeedback> interFeedback = new List<InterviewFeedback>();

            while (reader.Read())
            {
                interFeedback.Add(((IEntityBuilder<InterviewFeedback>)this).BuildEntity(reader));
            }

            return (interFeedback.Count > 0) ? interFeedback : null;
        }

        InterviewFeedback IEntityBuilder<InterviewFeedback>.BuildEntity(IDataReader reader)
        {
            const int FLD_ID = 0;
            
            const int FLD_SUBMITTEDBY = 1;
            const int FLD_TITLE = 2;
            const int FLD_INTERVIEWFEEDBACK = 3;

            const int FLD_INTERVIEWROUND = 4;

            
            const int FLD_FIRSTNAME = 5;
            const int FLD_LASTNAME = 6;
            const int FLD_EMAIL = 7;
            const int FLD_EMPLOYEEID = 8;
            const int FLD_DOCUMENTNAME = 9;
            const int FLD_CREATEDATE = 10;
            const int FLD_UPDATEDATE = 11;
            const int FLD_CREATORID = 12;
            const int FLD_UPDATORID = 13;
            const int FLD_INTERVIEWID = 14;
            



            InterviewFeedback interviewFeedback = new InterviewFeedback();

            interviewFeedback.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            interviewFeedback.Title = reader.IsDBNull(FLD_TITLE) ? string.Empty : reader.GetString(FLD_TITLE);
            interviewFeedback.Feedback = reader.IsDBNull(FLD_INTERVIEWFEEDBACK) ? string.Empty : reader.GetString(FLD_INTERVIEWFEEDBACK);
            interviewFeedback.SubmittedBy = reader.IsDBNull(FLD_SUBMITTEDBY) ? string.Empty : reader.GetString(FLD_SUBMITTEDBY);
            interviewFeedback.InterviewRound = reader.IsDBNull(FLD_INTERVIEWROUND) ? 0 : reader.GetInt32(FLD_INTERVIEWROUND);
            interviewFeedback.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
            interviewFeedback.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);
            interviewFeedback.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
            interviewFeedback.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);
            interviewFeedback.InterviewId = reader.IsDBNull(FLD_INTERVIEWID) ? 0 : reader.GetInt32(FLD_INTERVIEWID);

            interviewFeedback.FirstName = reader.IsDBNull(FLD_FIRSTNAME) ? string.Empty : reader.GetString(FLD_FIRSTNAME);
            interviewFeedback.LastName = reader.IsDBNull(FLD_LASTNAME) ? string.Empty : reader.GetString(FLD_LASTNAME);
            interviewFeedback.Email = reader.IsDBNull(FLD_EMAIL) ? string.Empty : reader.GetString(FLD_EMAIL);
            interviewFeedback.EmployeeID = reader.IsDBNull(FLD_EMPLOYEEID) ? string.Empty : reader.GetString(FLD_EMPLOYEEID);
            interviewFeedback.DocumentName = reader.IsDBNull(FLD_DOCUMENTNAME) ? string.Empty : reader.GetString(FLD_DOCUMENTNAME);


            return interviewFeedback;
        }

        #region IEntityBuilder<InterviewFeedback> InterviewFeedback


        public InterviewFeedback BuildEntity(IDataReader dataReader)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
