﻿using System.Data;
using System.Collections.Generic;
using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    internal sealed class RequsitionApprovalStatusBuilder : IEntityBuilder<RequsitionApprovalStatus>
    {
        IList<RequsitionApprovalStatus> IEntityBuilder<RequsitionApprovalStatus>.BuildEntities(IDataReader reader)
        {
            List<RequsitionApprovalStatus> status = new List<RequsitionApprovalStatus>();

            while (reader.Read())
            {
                status.Add(((IEntityBuilder<RequsitionApprovalStatus>)this).BuildEntity(reader));
            }

            return (status.Count > 0) ? status : null;
        }
        RequsitionApprovalStatus IEntityBuilder<RequsitionApprovalStatus>.BuildEntity(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_Status = 1;

            RequsitionApprovalStatus status = new RequsitionApprovalStatus();

            status.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            status.Status = reader.IsDBNull(FLD_Status) ? string.Empty : reader.GetString(FLD_Status);

            return status;
        }
    }
}
