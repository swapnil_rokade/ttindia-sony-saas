﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;
using Microsoft.Practices.EnterpriseLibrary.Data;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    internal sealed class RequsitionApprovalStatusDataAccess : BaseDataAccess, IRequsitionApprovalStatusDataAccess
    {
          #region Constructors

        public RequsitionApprovalStatusDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<RequsitionApprovalStatus> CreateEntityBuilder<RequsitionApprovalStatus>()
        {
            return (new RequsitionApprovalStatusBuilder()) as IEntityBuilder<RequsitionApprovalStatus>;
        }

        #endregion

        #region  Methods
        string IRequsitionApprovalStatusDataAccess.GetRequistionStatusById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.RequsitionStatus_GetRequistionStatusById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<RequsitionApprovalStatus>()).BuildEntity(reader).Status;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }
        Int32 IRequsitionApprovalStatusDataAccess.GetStatusIdByRequistionStatus(string statusName)
        {
            if (statusName == null)
            {
                statusName = ""; ;
            }

            const string SP = "dbo.RequsitionStatus_GetStatusIdByStatusName";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@statusName", DbType.String, statusName);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    Int32 statusId = 0;

                    while (reader.Read())
                    {
                        statusId = reader.GetInt32(0);
                    }

                    return statusId;
                }
            }
        }
        #endregion
    }
}
