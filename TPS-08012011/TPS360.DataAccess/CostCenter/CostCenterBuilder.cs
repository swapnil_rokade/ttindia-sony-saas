﻿using System;
using System.Data;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    internal sealed class CostCenterBuilder : IEntityBuilder<CostCenter>
    {
        IList<CostCenter> IEntityBuilder<CostCenter>.BuildEntities(IDataReader reader)
        {
            List<CostCenter> costCenter = new List<CostCenter>();

            while (reader.Read())
            {
                costCenter.Add(((IEntityBuilder<CostCenter>)this).BuildEntity(reader));
            }

            return (costCenter.Count > 0) ? costCenter : null;
        }

        CostCenter IEntityBuilder<CostCenter>.BuildEntity(IDataReader reader)
        {
            //const int FLD_COST_CENTER_ID = 0;
            //const int FLD_COST_CENTER_NAME = 1;

            //CostCenter costCenter = new CostCenter();

            //costCenter.COST_CENTER_ID = reader.IsDBNull(FLD_COST_CENTER_ID) ? 0 : reader.GetInt64(FLD_COST_CENTER_ID);
            //costCenter.COST_CENTER_NAME = reader.IsDBNull(FLD_COST_CENTER_NAME) ? string.Empty : reader.GetString(FLD_COST_CENTER_NAME);

            //return costCenter;
            const int FLD_COST_CENTER_NAME = 0;
            const int FLD_COST_CENTER_ID = 1;

            CostCenter costCenter = new CostCenter();

            costCenter.COST_CENTER_NAME = reader.IsDBNull(FLD_COST_CENTER_NAME) ? string.Empty : reader.GetString(FLD_COST_CENTER_NAME);
            //costCenter.COST_CENTER_ID = reader.IsDBNull(FLD_COST_CENTER_ID) ? 0 : Convert.ToInt64(reader.GetValue(FLD_COST_CENTER_ID));
            costCenter.COST_CENTER_ID = reader.IsDBNull(FLD_COST_CENTER_ID) ? string.Empty : reader.GetString(FLD_COST_CENTER_ID);
            return costCenter;
        }
    }
}
