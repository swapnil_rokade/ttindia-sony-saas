﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: GenericLookupDataAccess.cs
    Description:  
    Created By:  
    Created On:  
    Modification Log:
    --------------------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
 *  1.0                 5/Dec/2015          Prasanth Kumar G    Introduced Function InterviewAssessment_GetBy_InterviewId_InterviewerEmail
    ---------------------------------------------------------------------------------------------------------------------------------------------
    
*/

//-----------------------------------------Rishi Code Start-------------------------------------//
using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Helper;

namespace TPS360.DataAccess
{
    internal sealed class CostCenterDataAccess : BaseDataAccess, ICostCenterDataAccess
    {
        #region Constructors

        public CostCenterDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<CostCenter> CreateEntityBuilder<CostCenter>()
        {
            return (new CostCenterBuilder()) as IEntityBuilder<CostCenter>;
        }

        #endregion

        #region  Methods

        IList<TPS360.Common.BusinessEntities.CostCenter> ICostCenterDataAccess.GetAll()
        {
            const string SP = "dbo.CostCenter_GetAll";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<TPS360.Common.BusinessEntities.CostCenter>().BuildEntities(reader);
                }
            }
        }

        #endregion
    }
}
//-----------------------------------------Rishi Code End-------------------------------------//
