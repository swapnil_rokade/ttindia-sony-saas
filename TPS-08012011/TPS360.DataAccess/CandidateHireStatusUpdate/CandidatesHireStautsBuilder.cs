﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    internal sealed class CandidatesHireStautsBuilder : IEntityBuilder<CandidatesHireStatus>, IEntityBuilder<HiringMatrixLevels>, IEntityBuilder<CustomRole>
    {
        IList<CandidatesHireStatus> IEntityBuilder<CandidatesHireStatus>.BuildEntities(IDataReader reader)
        {
            List<CandidatesHireStatus> CandidateStatus = new List<CandidatesHireStatus>();

            while (reader.Read())
            {
                CandidateStatus.Add(((IEntityBuilder<CandidatesHireStatus>)this).BuildEntity(reader));
            }

            return (CandidateStatus.Count > 0) ? CandidateStatus : null;
        }
        CandidatesHireStatus IEntityBuilder<CandidatesHireStatus>.BuildEntity(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_MemberID = 1;
            const int FLD_CandidateName = 2;
            const int FLD_RequisitionJobTitle = 3;
            const int FLD_JobStatus = 4;
            const int FLD_MangerId = 5;
            const int FLD_RequisitionCode = 6;
            const int FLD_JobPostingID = 7;

            CandidatesHireStatus CandidateList = new CandidatesHireStatus();
            if (reader.FieldCount == 3)
            {
                const int FLD_Name = 1;
                const int FLD_ISREMOVED = 2;
                CandidateList.ID = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
                CandidateList.Name = reader.IsDBNull(FLD_Name) ? string.Empty : reader.GetString(FLD_Name);
                CandidateList.IsRemoved = reader.IsDBNull(FLD_ISREMOVED) ? false : reader.GetBoolean(FLD_ISREMOVED);
            }
            else if (reader.FieldCount > 3)
            {
                CandidateList.ID = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
                CandidateList.MemberID = reader.IsDBNull(FLD_MemberID) ? 0 : reader.GetInt32(FLD_MemberID);
                CandidateList.CandidateName = reader.IsDBNull(FLD_CandidateName) ? string.Empty : reader.GetString(FLD_CandidateName);
                CandidateList.RequisitionJobTitle = reader.IsDBNull(FLD_RequisitionJobTitle) ? string.Empty : reader.GetString(FLD_RequisitionJobTitle);
                CandidateList.JobStatus = reader.IsDBNull(FLD_JobStatus) ? 0 : reader.GetInt32(FLD_JobStatus);
                CandidateList.ManagerId = reader.IsDBNull(FLD_MangerId) ? 0 : reader.GetInt32(FLD_MangerId);
                CandidateList.RequisitionCode = reader.IsDBNull(FLD_RequisitionCode) ? string.Empty : reader.GetString(FLD_RequisitionCode);
                CandidateList.JobPostingID = reader.IsDBNull(FLD_JobPostingID) ? 0 : reader.GetInt32(FLD_JobPostingID);
            }

            return CandidateList;
        }
        IList<HiringMatrixLevels> IEntityBuilder<HiringMatrixLevels>.BuildEntities(IDataReader reader)
        {
            List<HiringMatrixLevels> hiringMatrixLevels = new List<HiringMatrixLevels>();

            while (reader.Read())
            {
                hiringMatrixLevels.Add(((IEntityBuilder<HiringMatrixLevels>)this).BuildEntity(reader));
            }

            return (hiringMatrixLevels.Count > 0) ? hiringMatrixLevels : null;
        }

        HiringMatrixLevels IEntityBuilder<HiringMatrixLevels>.BuildEntity(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_NAME = 1;
            const int FLD_SORTORDER = 2;
            const int FLD_CREATORID = 3;
            const int FLD_UPDATORID = 4;
            const int FLD_CREATEDATE = 5;
            const int FLD_UPDATEDATE = 6;
            const int FLD_COUNT = 7;


            HiringMatrixLevels hiringMatrixLevels = new HiringMatrixLevels();

            hiringMatrixLevels.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            hiringMatrixLevels.Name = reader.IsDBNull(FLD_NAME) ? string.Empty : reader.GetString(FLD_NAME);
            hiringMatrixLevels.SortingOrder = reader.IsDBNull(FLD_SORTORDER) ? 0 : reader.GetInt32(FLD_SORTORDER);
            hiringMatrixLevels.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
            hiringMatrixLevels.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);
            hiringMatrixLevels.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
            hiringMatrixLevels.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);
            if (reader.FieldCount > FLD_COUNT)
                hiringMatrixLevels.Count = reader.IsDBNull(FLD_COUNT) ? 0 : reader.GetInt32(FLD_COUNT);
            return hiringMatrixLevels;
        }

        IList<CustomRole> IEntityBuilder<CustomRole>.BuildEntities(IDataReader reader)
        {
            List<CustomRole> customRoles = new List<CustomRole>();

            while (reader.Read())
            {
                customRoles.Add(((IEntityBuilder<CustomRole>)this).BuildEntity(reader));
            }

            return (customRoles.Count > 0) ? customRoles : null;
        }

        CustomRole IEntityBuilder<CustomRole>.BuildEntity(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_NAME = 1;
            const int FLD_DESCRIPTION = 2;
            const int FLD_ISSYSTEMDEFAULT = 3;
            const int FLD_ISREMOVED = 4;
            const int FLD_CREATORID = 5;
            const int FLD_UPDATORID = 6;
            const int FLD_CREATEDATE = 7;
            const int FLD_UPDATEDATE = 8;

            CustomRole customRole = new CustomRole();

            customRole.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            customRole.Name = reader.IsDBNull(FLD_NAME) ? string.Empty : reader.GetString(FLD_NAME);
            if (reader.FieldCount > 2)
            {
                customRole.Description = reader.IsDBNull(FLD_DESCRIPTION) ? string.Empty : reader.GetString(FLD_DESCRIPTION);
                customRole.IsSystemDefault = reader.IsDBNull(FLD_ISSYSTEMDEFAULT) ? false : reader.GetBoolean(FLD_ISSYSTEMDEFAULT);
                customRole.IsRemoved = reader.IsDBNull(FLD_ISREMOVED) ? false : reader.GetBoolean(FLD_ISREMOVED);
                customRole.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
                customRole.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);
                customRole.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
                customRole.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);
            }
            return customRole;
        }
    }
}
