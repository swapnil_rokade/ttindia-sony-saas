﻿using System;
using System.Data;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    internal sealed class MemberAttendenceBuilder : IEntityBuilder<MemberAttendence>
    {
        IList<MemberAttendence> IEntityBuilder<MemberAttendence>.BuildEntities(IDataReader reader)
        {
            List<MemberAttendence> memberAttendences = new List<MemberAttendence>();

            while (reader.Read())
            {
                memberAttendences.Add(((IEntityBuilder<MemberAttendence>)this).BuildEntity(reader));
            }

            return (memberAttendences.Count > 0) ? memberAttendences : null;
        }

        MemberAttendence IEntityBuilder<MemberAttendence>.BuildEntity(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_ATTENDENCETYPE = 1;
            const int FLD_ATTENDENCEDATE = 2;
            const int FLD_COMMENT = 3;
            const int FLD_MEMBERID = 4;
            const int FLD_ISREMOVED = 5;
            const int FLD_CREATORID = 6;
            const int FLD_UPDATORID = 7;
            const int FLD_CREATEDATE = 8;
            const int FLD_UPDATEDATE = 9;

            MemberAttendence memberAttendence = new MemberAttendence();

            memberAttendence.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            memberAttendence.AttendenceType = reader.IsDBNull(FLD_ATTENDENCETYPE) ? 0 : reader.GetInt32(FLD_ATTENDENCETYPE);
            memberAttendence.AttendenceDate = reader.IsDBNull(FLD_ATTENDENCEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_ATTENDENCEDATE);
            memberAttendence.Comment = reader.IsDBNull(FLD_COMMENT) ? string.Empty : reader.GetString(FLD_COMMENT);
            memberAttendence.MemberId = reader.IsDBNull(FLD_MEMBERID) ? 0 : reader.GetInt32(FLD_MEMBERID);
            memberAttendence.IsRemoved = reader.IsDBNull(FLD_ISREMOVED) ? false : reader.GetBoolean(FLD_ISREMOVED);
            memberAttendence.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
            memberAttendence.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);
            memberAttendence.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
            memberAttendence.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);

            return memberAttendence;
        }
    }

   
}