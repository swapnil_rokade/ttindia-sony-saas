﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    internal sealed class MemberAttendenceDataAccess : BaseDataAccess, IMemberAttendenceDataAccess
    {
        #region Constructors

        public MemberAttendenceDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<MemberAttendence> CreateEntityBuilder<MemberAttendence>()
        {
            return (new MemberAttendenceBuilder()) as IEntityBuilder<MemberAttendence>;
        }       
        #endregion

        #region  Methods

        MemberAttendence IMemberAttendenceDataAccess.Add(MemberAttendence memberAttendence)
        {
            const string SP = "dbo.MemberAttendence_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@AttendenceType", DbType.Int32, memberAttendence.AttendenceType);
                Database.AddInParameter(cmd, "@AttendenceDate", DbType.DateTime, memberAttendence.AttendenceDate);
                Database.AddInParameter(cmd, "@Comment", DbType.AnsiString, memberAttendence.Comment);
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberAttendence.MemberId);
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, memberAttendence.IsRemoved);
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, memberAttendence.CreatorId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        memberAttendence = CreateEntityBuilder<MemberAttendence>().BuildEntity(reader);
                    }
                    else
                    {
                        memberAttendence = null;
                    }
                }

                if (memberAttendence == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("MemberAttendence already exists. Please specify another memberAttendence.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while creating this memberAttendence.");
                            }
                    }
                }

                return memberAttendence;
            }
        }

        MemberAttendence IMemberAttendenceDataAccess.Update(MemberAttendence memberAttendence)
        {
            const string SP = "dbo.MemberAttendence_Update";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, memberAttendence.Id);
                Database.AddInParameter(cmd, "@AttendenceType", DbType.Int32, memberAttendence.AttendenceType);
                Database.AddInParameter(cmd, "@AttendenceDate", DbType.DateTime, memberAttendence.AttendenceDate);
                Database.AddInParameter(cmd, "@Comment", DbType.AnsiString, memberAttendence.Comment);
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, memberAttendence.IsRemoved);
                Database.AddInParameter(cmd, "@UpdatorId", DbType.Int32, memberAttendence.UpdatorId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        memberAttendence = CreateEntityBuilder<MemberAttendence>().BuildEntity(reader);
                    }
                    else
                    {
                        memberAttendence = null;
                    }
                }

                if (memberAttendence == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("MemberAttendence already exists. Please specify another memberAttendence.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while updating this memberAttendence.");
                            }
                    }
                }

                return memberAttendence;
            }
        }

        MemberAttendence IMemberAttendenceDataAccess.GetById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.MemberAttendence_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<MemberAttendence>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        MemberAttendence IMemberAttendenceDataAccess.GetByAttendanceDateAndMemberId(DateTime attendanceDate,int memberId)
        {
            if (attendanceDate==null || attendanceDate==DateTime.MinValue)
            {
                throw new ArgumentNullException("attendanceDate");
            }

            if (memberId < 1)
            {
                throw new ArgumentNullException("memberId");
            }

            const string SP = "dbo.MemberAttendence_GetByAttendenceDateAndMemberId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@AttendenceDate", DbType.DateTime, attendanceDate);
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<MemberAttendence>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        IList<MemberAttendence> IMemberAttendenceDataAccess.GetAll()
        {
            const string SP = "dbo.MemberAttendence_GetAll";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<MemberAttendence>().BuildEntities(reader);
                }
            }
        }

        IList<MemberAttendence> IMemberAttendenceDataAccess.GetAllByMemberId(int memberId)
        {
            const string SP = "dbo.MemberAttendence_GetAllByMemberId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<MemberAttendence>().BuildEntities(reader);
                }
            }
        }

        bool IMemberAttendenceDataAccess.DeleteById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.MemberAttendence_DeleteById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a memberAttendence which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this memberAttendence.");
                        }
                }
            }
        }

        IList<MemberAttendence> IMemberAttendenceDataAccess.GetByYearAndMemberId(Int32 year, Int32 memberId)
        {
            if (year < 1 || year > 9999)
            {
                throw new ArgumentNullException("year");
            }

            if (memberId < 1)
            {
                throw new ArgumentNullException("memberId");
            }

            const string SP = "dbo.MemberAttendence_GetByYearAndMemberId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Year", DbType.Int32, year);
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return CreateEntityBuilder<MemberAttendence>().BuildEntities(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        #endregion
    }
}