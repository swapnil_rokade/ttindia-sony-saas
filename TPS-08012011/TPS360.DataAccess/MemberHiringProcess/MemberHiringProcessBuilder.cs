﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    internal sealed class MemberHiringProcessBuilder : IEntityBuilder<MemberHiringProcess>
    {
        IList<MemberHiringProcess> IEntityBuilder<MemberHiringProcess>.BuildEntities(IDataReader reader)
        {
            List<MemberHiringProcess> memberHiringProcesss = new List<MemberHiringProcess>();

            while (reader.Read())
            {
                memberHiringProcesss.Add(((IEntityBuilder<MemberHiringProcess>)this).BuildEntity(reader));
            }

            return (memberHiringProcesss.Count > 0) ? memberHiringProcesss : null;
        }

        MemberHiringProcess IEntityBuilder<MemberHiringProcess>.BuildEntity(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_MEMBERID = 1;
            const int FLD_JOBPOSTINGID = 2;
            const int FLD_INTERVIEWLEVELID = 3;
            const int FLD_INTERVIEWERID = 4;
            const int FLD_INTERVIEWERNOTE = 5;
            const int FLD_INTERVIEWERDATE = 6;
            const int FLD_INTERVIEWTOTAL = 7;
            const int FLD_INTERVIEWSUBMITTEDSTAGES = 8;
            const int FLD_APPLICANTMATCHINGPERCENT = 9;
            const int FLD_INTERVIEWREMARKS = 10;
            
            MemberHiringProcess memberHiringProcess = new MemberHiringProcess();

            memberHiringProcess.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            memberHiringProcess.MemberId = reader.IsDBNull(FLD_MEMBERID) ? 0 : reader.GetInt32(FLD_MEMBERID);
            memberHiringProcess.JobPostingId = reader.IsDBNull(FLD_JOBPOSTINGID) ? 0 : reader.GetInt32(FLD_JOBPOSTINGID);
            memberHiringProcess.JobPostingId = reader.IsDBNull(FLD_INTERVIEWLEVELID ) ? 0 : reader.GetInt32(FLD_INTERVIEWLEVELID );
            memberHiringProcess.InterviewerId = reader.IsDBNull(FLD_INTERVIEWERID) ? 0 : reader.GetInt32(FLD_INTERVIEWERID);
            memberHiringProcess.InterviewerNote = reader.IsDBNull(FLD_INTERVIEWERNOTE) ? string.Empty : reader.GetString(FLD_INTERVIEWERNOTE);
            memberHiringProcess.InterviewerDate = reader.IsDBNull(FLD_INTERVIEWERDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_INTERVIEWERDATE);
            memberHiringProcess.InterviewTotal = reader.IsDBNull(FLD_INTERVIEWTOTAL) ? 0 : reader.GetDecimal(FLD_INTERVIEWTOTAL);
            memberHiringProcess.InterviewSubmittedStages = reader.IsDBNull(FLD_INTERVIEWSUBMITTEDSTAGES) ? 0 : reader.GetInt32(FLD_INTERVIEWSUBMITTEDSTAGES);
            memberHiringProcess.ApplicantMatchingPercent = reader.IsDBNull(FLD_APPLICANTMATCHINGPERCENT) ? 0 : reader.GetInt32(FLD_APPLICANTMATCHINGPERCENT);
            memberHiringProcess.InterviewRemarks = reader.IsDBNull(FLD_INTERVIEWREMARKS) ? string.Empty : reader.GetString(FLD_INTERVIEWREMARKS);

            return memberHiringProcess;
        }
    }
}