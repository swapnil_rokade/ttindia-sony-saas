﻿//--------------------------------Rishi Code Start-------------------------------------------------//
using System;
using System.Data;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    internal sealed class SalaryTemplateBuilder : IEntityBuilder<SalaryTemplate>
    {
        IList<SalaryTemplate> IEntityBuilder<SalaryTemplate>.BuildEntities(IDataReader reader)
        {
            List<SalaryTemplate> salaryTemplate = new List<SalaryTemplate>();

            while (reader.Read())
            {
                salaryTemplate.Add(((IEntityBuilder<SalaryTemplate>)this).BuildEntity(reader));
            }

            return (salaryTemplate.Count > 0) ? salaryTemplate : null;
        }

        SalaryTemplate IEntityBuilder<SalaryTemplate>.BuildEntity(IDataReader reader)
        {
            const int FLD_GRADE = 0;
            const int FLD_HEAD = 1;
            const int FLD_CALCULATION = 2;
            const int FLD_FORMULA = 3;

            SalaryTemplate salaryTemplate = new SalaryTemplate();

            if (reader.FieldCount == 1)
            {
                salaryTemplate.NewGrade = reader.IsDBNull(FLD_GRADE) ? string.Empty : reader.GetString(FLD_GRADE);
            }
            else if (reader.FieldCount > 1)
            {
                salaryTemplate.NewGrade = reader.IsDBNull(FLD_GRADE) ? string.Empty : reader.GetString(FLD_GRADE);
                salaryTemplate.Heads = reader.IsDBNull(FLD_HEAD) ? string.Empty : reader.GetString(FLD_HEAD);
                salaryTemplate.Calculation = reader.IsDBNull(FLD_CALCULATION) ? string.Empty : reader.GetString(FLD_CALCULATION);
                salaryTemplate.Value = reader.IsDBNull(FLD_FORMULA) ? 0 : Convert.ToDouble(reader.GetValue(FLD_FORMULA));
            }

            return salaryTemplate;
        }
    }
}
//--------------------------------Rishi Code End-------------------------------------------------//