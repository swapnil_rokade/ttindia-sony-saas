﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: GenericLookupDataAccess.cs
    Description:  
    Created By:  
    Created On:  
    Modification Log:
    --------------------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
 *  1.0                 5/Dec/2015          Prasanth Kumar G    Introduced Function InterviewAssessment_GetBy_InterviewId_InterviewerEmail
    ---------------------------------------------------------------------------------------------------------------------------------------------
    
*/

//-----------------------------------------Rishi Code Start-------------------------------------//
using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Helper;

namespace TPS360.DataAccess
{
    internal sealed class SalaryTemplateDataAccess : BaseDataAccess, ISalaryTemplateDataAccess
    {
        #region Constructors

        public SalaryTemplateDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<SalaryTemplate> CreateEntityBuilder<SalaryTemplate>()
        {
            return (new SalaryTemplateBuilder()) as IEntityBuilder<SalaryTemplate>;
        }

        #endregion

        #region  Methods

        IList<TPS360.Common.BusinessEntities.SalaryTemplate> ISalaryTemplateDataAccess.GetAll()
        {
            const string SP = "dbo.GetDictintSalaryGrade";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<TPS360.Common.BusinessEntities.SalaryTemplate>().BuildEntities(reader);
                }
            }
        }

        TPS360.Common.BusinessEntities.SalaryTemplate ISalaryTemplateDataAccess.Add(string head, string calculation, double formula, string grade)
        {

            const string SP = "dbo.SalaryTemplate_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Heads", DbType.AnsiString, StringHelper.Convert(head));
                Database.AddInParameter(cmd, "@Calculation", DbType.AnsiString, StringHelper.Convert(calculation));
                Database.AddInParameter(cmd, "@Formula", DbType.Double, Convert.ToDouble(formula));
                Database.AddInParameter(cmd, "@Grade", DbType.AnsiString, StringHelper.Convert(grade));


                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<TPS360.Common.BusinessEntities.SalaryTemplate>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }


        IList<TPS360.Common.BusinessEntities.SalaryTemplate> ISalaryTemplateDataAccess.Display(string grade)
        {
            const string SP = "dbo.SalaryTemplate_Display";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Grade", DbType.AnsiString, StringHelper.Convert(grade));

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<TPS360.Common.BusinessEntities.SalaryTemplate>().BuildEntities(reader);
                }
            }
        }

        #endregion
    }
}
//-----------------------------------------Rishi Code End-------------------------------------//