﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    internal sealed class MemberCustomRoleMapDataAccess : BaseDataAccess, IMemberCustomRoleMapDataAccess
    {
        #region Constructors

        public MemberCustomRoleMapDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<MemberCustomRoleMap> CreateEntityBuilder<MemberCustomRoleMap>()
        {
            return (new MemberCustomRoleMapBuilder()) as IEntityBuilder<MemberCustomRoleMap>;
        }

        #endregion

        #region  Methods

        MemberCustomRoleMap IMemberCustomRoleMapDataAccess.Add(MemberCustomRoleMap memberCustomRoleMap)
        {
            const string SP = "dbo.MemberCustomRoleMap_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, memberCustomRoleMap.IsRemoved);
                Database.AddInParameter(cmd, "@CustomRoleId", DbType.Int32, memberCustomRoleMap.CustomRoleId);
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberCustomRoleMap.MemberId);
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, memberCustomRoleMap.CreatorId);               

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        memberCustomRoleMap = CreateEntityBuilder<MemberCustomRoleMap>().BuildEntity(reader);
                    }
                    else
                    {
                        memberCustomRoleMap = null;
                    }
                }

                if (memberCustomRoleMap == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Member custom role map already exists. Please specify another member custom role map.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while creating this member custom role map.");
                            }
                    }
                }

                return memberCustomRoleMap;
            }
        }

        MemberCustomRoleMap IMemberCustomRoleMapDataAccess.Update(MemberCustomRoleMap memberCustomRoleMap)
        {
            const string SP = "dbo.MemberCustomRoleMap_Update";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, memberCustomRoleMap.Id);
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, memberCustomRoleMap.IsRemoved);
                Database.AddInParameter(cmd, "@CustomRoleId", DbType.Int32, memberCustomRoleMap.CustomRoleId);
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberCustomRoleMap.MemberId);                
                Database.AddInParameter(cmd, "@UpdatorId", DbType.Int32, memberCustomRoleMap.UpdatorId);
               
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        memberCustomRoleMap = CreateEntityBuilder<MemberCustomRoleMap>().BuildEntity(reader);
                    }
                    else
                    {
                        memberCustomRoleMap = null;
                    }
                }

                if (memberCustomRoleMap == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Member custom role map already exists. Please specify another member custom role map.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while updating this member custom role map.");
                            }
                    }
                }

                return memberCustomRoleMap;
            }
        }

        MemberCustomRoleMap IMemberCustomRoleMapDataAccess.GetById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.MemberCustomRoleMap_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<MemberCustomRoleMap>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        MemberCustomRoleMap IMemberCustomRoleMapDataAccess.GetByMemberId(int memberId)
        {
            if (memberId < 1)
            {
                throw new ArgumentNullException("memberId");
            }

            const string SP = "dbo.MemberCustomRoleMap_GetByMemberId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<MemberCustomRoleMap>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        IList<MemberCustomRoleMap> IMemberCustomRoleMapDataAccess.GetAll()
        {
            const string SP = "dbo.MemberCustomRoleMap_GetAll";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<MemberCustomRoleMap>().BuildEntities(reader);
                }
            }
        }

        bool IMemberCustomRoleMapDataAccess.DeleteById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.MemberCustomRoleMap_DeleteById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a member custom role map which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this member custom role map.");
                        }
                }
            }
        }

        #endregion
    }
}