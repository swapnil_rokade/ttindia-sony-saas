﻿using System.Data;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    internal sealed class InterviewInterviewerMapBuilder : IEntityBuilder<InterviewInterviewerMap>
    {
        IList<InterviewInterviewerMap> IEntityBuilder<InterviewInterviewerMap>.BuildEntities(IDataReader reader)
        {
            List<InterviewInterviewerMap> InterviewerMaps = new List<InterviewInterviewerMap>();

            while (reader.Read())
            {
                InterviewerMaps.Add(((IEntityBuilder<InterviewInterviewerMap>)this).BuildEntity(reader));
            }

            return (InterviewerMaps.Count > 0) ? InterviewerMaps : null;
        }

        InterviewInterviewerMap IEntityBuilder<InterviewInterviewerMap>.BuildEntity(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_INTERVIEWID = 1;
            const int FLD_INTERVIEWERID = 2;
            const int FLD_CREATORID = 3;
            const int FLD_UPDATORID = 4;
            const int FLD_ISCLIENT = 5;

            InterviewInterviewerMap InterviewerMap = new InterviewInterviewerMap();

            InterviewerMap.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            InterviewerMap.InterviewId = reader.IsDBNull(FLD_INTERVIEWID) ? 0 : reader.GetInt32(FLD_INTERVIEWID);
            InterviewerMap.InterviewerId = reader.IsDBNull(FLD_INTERVIEWERID) ? 0  : reader.GetInt32  (FLD_INTERVIEWERID);
            InterviewerMap.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
            InterviewerMap.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);
            InterviewerMap.IsClient  = reader.IsDBNull(FLD_ISCLIENT) ? false  : reader.GetBoolean (FLD_ISCLIENT);
            return InterviewerMap;
        }
    }
}