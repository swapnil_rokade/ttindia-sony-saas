﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: CommonNoteDataAccess.cs
    Description: This is the class library page.
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author             Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1              Jan-09-2008           Jagadish            Defect ID: 9073; Added an overload for method 'GetAllByMemberId'.
 *  0.2              Feb-2-2009            Gopala Swamy        Defect ID: 9061 ;Added new parameter "string sortExpression" to method called "GetAllByProjectId"
-------------------------------------------------------------------------------------------------------------------------------------------        
*/
using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    internal sealed class CommonNoteDataAccess : BaseDataAccess, ICommonNoteDataAccess
    {
        #region Constructors

        public CommonNoteDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<CommonNote> CreateEntityBuilder<CommonNote>()
        {
            return (new CommonNoteBuilder()) as IEntityBuilder<CommonNote>;
        }

        #endregion

        #region  Methods

        CommonNote ICommonNoteDataAccess.Add(CommonNote commonNote)
        {
            const string SP = "dbo.CommonNote_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@NoteDetail", DbType.AnsiString, commonNote.NoteDetail);
                Database.AddInParameter(cmd, "@IsInternal", DbType.Boolean, commonNote.IsInternal);
                Database.AddInParameter(cmd, "@Status", DbType.Int32, commonNote.Status);
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, commonNote.IsRemoved);
                Database.AddInParameter(cmd, "@NoteCategoryLookupId", DbType.Int32, commonNote.NoteCategoryLookupId);
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, commonNote.CreatorId);                

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        commonNote = CreateEntityBuilder<CommonNote>().BuildEntity(reader);
                    }
                    else
                    {
                        commonNote = null;
                    }
                }

                if (commonNote == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("CommonNote already exists. Please specify another commonNote.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while creating this commonNote.");
                            }
                    }
                }

                return commonNote;
            }
        }

        CommonNote ICommonNoteDataAccess.Update(CommonNote commonNote)
        {
            const string SP = "dbo.CommonNote_Update";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, commonNote.Id);
                Database.AddInParameter(cmd, "@NoteDetail", DbType.AnsiString, commonNote.NoteDetail);
                Database.AddInParameter(cmd, "@IsInternal", DbType.Boolean, commonNote.IsInternal);
                Database.AddInParameter(cmd, "@Status", DbType.Int32, commonNote.Status);
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, commonNote.IsRemoved);
                Database.AddInParameter(cmd, "@NoteCategoryLookupId", DbType.Int32, commonNote.NoteCategoryLookupId);
                Database.AddInParameter(cmd, "@UpdatorId", DbType.Int32, commonNote.UpdatorId);                

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        commonNote = CreateEntityBuilder<CommonNote>().BuildEntity(reader);
                    }
                    else
                    {
                        commonNote = null;
                    }
                }

                if (commonNote == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("CommonNote already exists. Please specify another commonNote.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while updating this commonNote.");
                            }
                    }
                }

                return commonNote;
            }
        }

        CommonNote ICommonNoteDataAccess.GetById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.CommonNote_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<CommonNote>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        IList<CommonNote> ICommonNoteDataAccess.GetAllByMemberId(int memberId)
        {
            const string SP = "dbo.CommonNote_GetAllCommonNoteByMemberId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<CommonNote>().BuildEntities(reader);
                }
            }
        }

        // 0.1 starts here
        IList<CommonNote> ICommonNoteDataAccess.GetAllByMemberId(int memberId, string SortExpression)
        {
            const string SP = "dbo.CommonNote_GetAllCommonNoteByMemberId";

            string SortColumn = "[C].[CreateDate]";
            string SortOrder = "desc";
            string[] part = (string.IsNullOrEmpty(SortExpression)) ? null : SortExpression.Split(' ');

            if (part != null && part.Length > 0)
            {
                SortColumn = part[0];

                if (part.Length > 1 && part[1] != null)
                {
                    SortOrder = part[1];
                }
                else
                {
                    SortOrder = "asc";
                }
            }            

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberId);
                Database.AddInParameter(cmd, "@SortColumn", DbType.String, SortColumn);
                Database.AddInParameter(cmd, "@SortOrder", DbType.String, SortOrder);
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<CommonNote>().BuildEntities(reader);
                }
            }
        }
        // 0.1 ends here
        IList<CommonNote> ICommonNoteDataAccess.GetAllByCompanyId(int companyId, string SortExpression)
        {
            const string SP = "dbo.CommonNote_GetAllCommonNoteByCompanyId";

            string SortColumn = "[C].[CreateDate]";
            string SortOrder = "desc";
            string[] part = (string.IsNullOrEmpty(SortExpression)) ? null : SortExpression.Split(' ');

            if (part != null && part.Length > 0)
            {
                SortColumn = part[0];

                if (part.Length > 1 && part[1] != null)
                {
                    SortOrder = part[1];
                }
                else
                {
                    SortOrder = "asc";
                }
            }

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@CompanyId", DbType.Int32, companyId );
                Database.AddInParameter(cmd, "@SortColumn", DbType.String, SortColumn);
                Database.AddInParameter(cmd, "@SortOrder", DbType.String, SortOrder);
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<CommonNote>().BuildEntities(reader);
                }
            }
        }
        IList<CommonNote> ICommonNoteDataAccess.GetAllByCampaignId(int campaignId)
        {
            const string SP = "dbo.CommonNote_GetAllCommonNoteByCampaignId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@CampaignId", DbType.Int32, campaignId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<CommonNote>().BuildEntities(reader);
                }
            }
        }

        //0.2
        IList<CommonNote> ICommonNoteDataAccess.GetAllByProjectId(int projectId,string sortExpression)
        {
            const string SP = "dbo.CommonNote_GetAllCommonNoteByProjectId"; 
            string SortOrder = "Order By [C].[CreateDate] ASC"; //0.2

            if (!string.IsNullOrEmpty(sortExpression)) //0.2
            {
                SortOrder = "Order By " + sortExpression;//0.2

            }
            
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@ProjectId", DbType.Int32, projectId);
                Database.AddInParameter(cmd, "@SortOrder", DbType.String, SortOrder);//0.2
                

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<CommonNote>().BuildEntities(reader);
                }
            }
        }

        IList<CommonNote> ICommonNoteDataAccess.GetAll()
        {
            const string SP = "dbo.CommonNote_GetAll";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<CommonNote>().BuildEntities(reader);
                }
            }
        }                

        bool ICommonNoteDataAccess.DeleteById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.CommonNote_DeleteById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a commonNote which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this commonNote.");
                        }
                }
            }
        }

        CommonNote ICommonNoteDataAccess.GetByCompanyIdAndNoteCategoryLookUpId(int companyId, int noteCategoryLookUpId)
        {
            if (companyId < 1)
            {
                throw new ArgumentNullException("companyId");
            }

            if (noteCategoryLookUpId < 1)
            {
                throw new ArgumentNullException("noteCategoryLookUpId");
            }

            const string SP = "dbo.CommonNote_GetByCompanyIdAndLookUpId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@companyId", DbType.Int32, companyId);
                Database.AddInParameter(cmd, "@noteCategoryLookUpId", DbType.Int32, noteCategoryLookUpId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<CommonNote>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        #endregion
    }
}