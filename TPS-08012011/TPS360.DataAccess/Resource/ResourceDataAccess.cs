﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    internal sealed class ResourceDataAccess : BaseDataAccess, IResourceDataAccess
    {
        #region Constructors

        public ResourceDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<Resource> CreateEntityBuilder<Resource>()
        {
            return (new ResourceBuilder()) as IEntityBuilder<Resource>;
        }

        #endregion

        #region  Methods

        Resource IResourceDataAccess.Add(Resource resource)
        {
            const string SP = "dbo.Resource_Add";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                //AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@ResourceName", DbType.String, resource.ResourceName);
                Database.AddInParameter(cmd, "@ResourceDesc", DbType.String, resource.ResourceDescription);
                Database.AddInParameter(cmd, "@ResourceEmail", DbType.String, resource.EmailAddress);
                Database.AddInParameter(cmd, "@EnableEmailReminders", DbType.Int32, resource.EnableEmailReminders);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        resource.ResourceID = reader.IsDBNull(0) ? 0 : reader.GetInt32(0);
                    }
                    else
                    {
                        resource = null;
                    }
                }

                if (resource == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Resource already exists. Please specify another resource.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while creating this resource.");
                            }
                    }
                }

                return resource;
            }
        }

        Resource IResourceDataAccess.Update(Resource resource)
        {
            const string SP = "dbo.Resource_Update";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                //AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@ResourceID", DbType.Int32, resource.ResourceID);
                Database.AddInParameter(cmd, "@ResourceName", DbType.String, resource.ResourceName);
                Database.AddInParameter(cmd, "@ResourceDesc", DbType.String, resource.ResourceDescription);
                Database.AddInParameter(cmd, "@ResourceEmail", DbType.String, resource.EmailAddress);
                Database.AddInParameter(cmd, "@EnableEmailReminders", DbType.Int32, resource.EnableEmailReminders);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    //if (reader.Read())
                    //{
                    //    resource = CreateEntityBuilder<Resource>().BuildEntity(reader);
                    //}
                    //else
                    //{
                    //    resource = null;
                    //}
                }

                if (resource == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Resource already exists. Please specify another resource.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while updating this resource.");
                            }
                    }
                }

                return resource;
            }
        }

        Resource IResourceDataAccess.GetByResourceID(int resourceID)
        {
            if (resourceID < 1)
            {
                throw new ArgumentNullException("resourceID");
            }

            const string SP = "dbo.Resource_GetByResourceID";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@ResourceID", DbType.Int32, resourceID);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<Resource>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        IList<Resource> IResourceDataAccess.GetAll()
        {
            const string SP = "dbo.Resource_GetAll";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<Resource>().BuildEntities(reader);
                }
            }
        }

        bool IResourceDataAccess.DeleteByResourceID(int resourceID)
        {
            if (resourceID < 1)
            {
                throw new ArgumentNullException("resourceID");
            }

            const string SP = "dbo.Resource_DeleteByResourceID";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@ResourceID", DbType.Int32, resourceID);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a resource which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this resource.");
                        }
                }
            }
        }

        Resource IResourceDataAccess.GetByResourceName(string resourceName)
        {
            if (resourceName ==string.Empty)
            {
                throw new ArgumentNullException("resourceName");
            }

            const string SP = "dbo.Resource_GetByResourceName";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@ResourceName", DbType.String, resourceName);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<Resource>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        #endregion
    }
}