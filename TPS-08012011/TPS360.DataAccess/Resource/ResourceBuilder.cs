﻿using System.Data;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    internal sealed class ResourceBuilder : IEntityBuilder<Resource>
    {
        IList<Resource> IEntityBuilder<Resource>.BuildEntities(IDataReader reader)
        {
            List<Resource> resources = new List<Resource>();

            while (reader.Read())
            {
                resources.Add(((IEntityBuilder<Resource>)this).BuildEntity(reader));
            }

            return (resources.Count > 0) ? resources : null;
        }

        Resource IEntityBuilder<Resource>.BuildEntity(IDataReader reader)
		{
				const int FLD_RESOURCEID = 0;
				const int FLD_RESOURCENAME = 1;
				const int FLD_RESOURCEDESCRIPTION = 2;
				const int FLD_EMAILADDRESS = 3;
				//const int FLD__TS = 4;

            Resource resource = new Resource();

			resource.ResourceID = reader.IsDBNull(FLD_RESOURCEID) ? 0 : reader.GetInt32(FLD_RESOURCEID);
			resource.ResourceName = reader.IsDBNull(FLD_RESOURCENAME) ? string.Empty : reader.GetString(FLD_RESOURCENAME);
			resource.ResourceDescription = reader.IsDBNull(FLD_RESOURCEDESCRIPTION) ? string.Empty : reader.GetString(FLD_RESOURCEDESCRIPTION);
			resource.EmailAddress = reader.IsDBNull(FLD_EMAILADDRESS) ? string.Empty : reader.GetString(FLD_EMAILADDRESS);
			//resource._ts = reader.IsDBNull(FLD__TS) ? 0 : reader.GetByte[](FLD__TS);
		
            return resource;
		}
    }
}