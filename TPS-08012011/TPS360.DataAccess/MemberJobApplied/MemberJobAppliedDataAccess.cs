﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;
using System.Collections;
using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Helper;

namespace TPS360.DataAccess
{
    internal sealed class MemberJobAppliedDataAccess : BaseDataAccess, IMemberJobAppliedDataAccess
    {
        #region Constructors

        public MemberJobAppliedDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<MemberJobApplied> CreateEntityBuilder<MemberJobApplied>()
        {
            return (new MemberJobAppliedBuilder()) as IEntityBuilder<MemberJobApplied>;
        }

        #endregion

        #region  Methods

      
        PagedResponse<MemberJobApplied> IMemberJobAppliedDataAccess.getPaged (PagedRequest request)
        {
            string whereClause = "";
            if (request.Conditions.Count > 0)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();

                string column = null;
                string value = null;

                foreach (DictionaryEntry entry in request.Conditions)
                {
                    column = StringHelper.Convert(entry.Key);
                    value = StringHelper.Convert(entry.Value);

                    if (!StringHelper.IsBlank(column))
                    {
                        value = value.Replace("'", "''");


                        if (StringHelper.IsEqual(column, "MemberId"))
                        {
                            if (value.Trim() != "")
                            {
                                //********Code commented by pravin khot on 12/Feb/2016*******
                                //if (sb.Length > 0)
                                //{
                                //    sb.Append(" and ");
                                //}
                                //sb.Append("[JA].[MemberId]");
                                //sb.Append("=");
                                //***************END****************************
                                sb.Append(value);
                            }
                        }
                    }
                }

                whereClause += sb.ToString();
            }




            object[] paramValues = new object[] {request .PageIndex,
													request .RowPerPage   ,
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request .SortColumn  ),
                                                    StringHelper.Convert(request.SortOrder),
												};
            //const string SP = "dbo.MemberJobApplied_GetPaged";
            const string SP = "dbo.MemberJobAppliedCareerPortal_GetPaged";//Sp name change by pravin khot on 10/Feb/2016 (using fetch data from new table MemberCareerPortalSubmissionList)

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<MemberJobApplied> response = new PagedResponse<MemberJobApplied>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = CreateEntityBuilder<MemberJobApplied>().BuildEntities(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }
        MemberJobApplied IMemberJobAppliedDataAccess.Add(MemberJobApplied memberJobApplied)
        {
            const string SP = "dbo.MemberJobApplied_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberJobApplied.MemberId);
                Database.AddInParameter(cmd, "@JobPostingId", DbType.Int32, memberJobApplied.JobPostingId);
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, memberJobApplied.CreatorId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        memberJobApplied = CreateEntityBuilder<MemberJobApplied>().BuildEntity(reader);
                    }
                    else
                    {
                        memberJobApplied = null;
                    }
                }
                return memberJobApplied;
            }
        }


        MemberJobApplied IMemberJobAppliedDataAccess.GetByMemberIdAndJobPostingId(int memberId, int JobPostingID)
        {
            if (memberId < 1)
            {
                throw new ArgumentNullException("memberId");
            }

            if (JobPostingID < 1)
            {
                throw new ArgumentNullException("JobPostingID");
            }

            const string SP = "dbo.MemberJobApplied_GetByMemberIdAndJobPostingId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberId);
                Database.AddInParameter(cmd, "@JobPostingId", DbType.Int32, JobPostingID );

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<MemberJobApplied>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }

        }

        #endregion
    }
}