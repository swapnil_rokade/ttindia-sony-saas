﻿using System;
using System.Data;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    internal sealed class ActionLogBuilder : IEntityBuilder<ActionLog>
    {
        IList<ActionLog> IEntityBuilder<ActionLog>.BuildEntities(IDataReader reader)
        {
            List<ActionLog> actionLogs = new List<ActionLog>();

            while (reader.Read())
            {
                actionLogs.Add(((IEntityBuilder<ActionLog>)this).BuildEntity(reader));
            }

            return (actionLogs.Count > 0) ? actionLogs : null;
        }

        ActionLog IEntityBuilder<ActionLog>.BuildEntity(IDataReader reader)
		{
				const int FLD_ID = 0;
				const int FLD_TABLENAME = 1;
				//const int FLD_PRIMARYOBJECT = 2;
				//const int FLD_MODIFIEDOBJECT = 3;
				const int FLD_ACTION = 4;
				const int FLD_PERFORMERID = 5;
				const int FLD_ACTIONDATE = 6;

            ActionLog actionLog = new ActionLog();

			actionLog.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
			actionLog.TableName = reader.IsDBNull(FLD_TABLENAME) ? string.Empty : reader.GetString(FLD_TABLENAME);
			//actionLog.PrimaryObject = reader.IsDBNull(FLD_PRIMARYOBJECT) ? 0 : reader.GetByte[](FLD_PRIMARYOBJECT);
			//actionLog.ModifiedObject = reader.IsDBNull(FLD_MODIFIEDOBJECT) ? 0 : reader.GetByte[](FLD_MODIFIEDOBJECT);
			actionLog.Action = reader.IsDBNull(FLD_ACTION) ? string.Empty : reader.GetString(FLD_ACTION);
			actionLog.PerformerId = reader.IsDBNull(FLD_PERFORMERID) ? 0 : reader.GetInt32(FLD_PERFORMERID);
			actionLog.ActionDate = reader.IsDBNull(FLD_ACTIONDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_ACTIONDATE);
		
            return actionLog;
		}
    }
}