﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Helper;

namespace TPS360.DataAccess
{
    internal sealed class ActionLogDataAccess : BaseDataAccess, IActionLogDataAccess
    {
        #region Constructors

        public ActionLogDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<ActionLog> CreateEntityBuilder<ActionLog>()
        {
            return (new ActionLogBuilder()) as IEntityBuilder<ActionLog>;
        }

        #endregion

        #region  Methods

        ActionLog IActionLogDataAccess.Add(ActionLog actionLog)
        {
            const string SP = "dbo.ActionLog_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@TableName", DbType.AnsiString, StringHelper.Convert(actionLog.TableName));
                Database.AddInParameter(cmd, "@PrimaryObject", DbType.Binary, actionLog.PrimaryObject);
                Database.AddInParameter(cmd, "@ModifiedObject", DbType.Binary, actionLog.ModifiedObject);
                Database.AddInParameter(cmd, "@Action", DbType.AnsiString, StringHelper.Convert(actionLog.Action));
                Database.AddInParameter(cmd, "@PerformerId", DbType.Int32, actionLog.PerformerId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        actionLog = CreateEntityBuilder<ActionLog>().BuildEntity(reader);
                    }
                    else
                    {
                        actionLog = null;
                    }
                }

                if (actionLog == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Action log already exists. Please specify another action log.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while creating this action log.");
                            }
                    }
                }

                return actionLog;
            }
        }

        ActionLog IActionLogDataAccess.Update(ActionLog actionLog)
        {
            const string SP = "dbo.ActionLog_Update";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, actionLog.Id);
                Database.AddInParameter(cmd, "@TableName", DbType.AnsiString, StringHelper.Convert(actionLog.TableName));
                Database.AddInParameter(cmd, "@PrimaryObject", DbType.Binary, actionLog.PrimaryObject);
                Database.AddInParameter(cmd, "@ModifiedObject", DbType.Binary, actionLog.ModifiedObject);
                Database.AddInParameter(cmd, "@Action", DbType.AnsiString, StringHelper.Convert(actionLog.Action));
                Database.AddInParameter(cmd, "@PerformerId", DbType.Int32, actionLog.PerformerId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        actionLog = CreateEntityBuilder<ActionLog>().BuildEntity(reader);
                    }
                    else
                    {
                        actionLog = null;
                    }
                }

                if (actionLog == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Action log already exists. Please specify another action log.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while updating this action log.");
                            }
                    }
                }

                return actionLog;
            }
        }

        ActionLog IActionLogDataAccess.GetById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.ActionLog_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<ActionLog>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        IList<ActionLog> IActionLogDataAccess.GetAll()
        {
            const string SP = "dbo.ActionLog_GetAll";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<ActionLog>().BuildEntities(reader);
                }
            }
        }

        bool IActionLogDataAccess.DeleteById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.ActionLog_DeleteById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a action log which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this action log.");
                        }
                }
            }
        }

        #endregion
    }
}