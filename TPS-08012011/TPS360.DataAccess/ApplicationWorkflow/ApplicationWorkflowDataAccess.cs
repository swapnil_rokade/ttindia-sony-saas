﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Helper;

namespace TPS360.DataAccess
{
    internal sealed class ApplicationWorkflowDataAccess : BaseDataAccess, IApplicationWorkflowDataAccess
    {
        #region Constructors

        public ApplicationWorkflowDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<ApplicationWorkflow> CreateEntityBuilder<ApplicationWorkflow>()
        {
            return (new ApplicationWorkflowBuilder()) as IEntityBuilder<ApplicationWorkflow>;
        }

        #endregion

        #region  Methods

        ApplicationWorkflow IApplicationWorkflowDataAccess.Add(ApplicationWorkflow applicationWorkflow)
        {
            const string SP = "dbo.ApplicationWorkflow_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Title", DbType.AnsiString, StringHelper.Convert(applicationWorkflow.Title));
                Database.AddInParameter(cmd, "@Description", DbType.AnsiString, StringHelper.Convert(applicationWorkflow.Description));
                Database.AddInParameter(cmd, "@RequireApproval", DbType.Boolean, applicationWorkflow.RequireApproval);
                Database.AddInParameter(cmd, "@TimeFrame", DbType.AnsiString, applicationWorkflow.TimeFrame);
                Database.AddInParameter(cmd, "@IsEmailAlert", DbType.Boolean, applicationWorkflow.IsEmailAlert);
                Database.AddInParameter(cmd, "@IsSmsAlert", DbType.Boolean, applicationWorkflow.IsSmsAlert);
                Database.AddInParameter(cmd, "@IsDashboardAlert", DbType.Boolean, applicationWorkflow.IsDashboardAlert);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        applicationWorkflow = CreateEntityBuilder<ApplicationWorkflow>().BuildEntity(reader);
                    }
                    else
                    {
                        applicationWorkflow = null;
                    }
                }

                if (applicationWorkflow == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Application workflow already exists. Please specify another application workflow.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while creating this application workflow.");
                            }
                    }
                }

                return applicationWorkflow;
            }
        }

        ApplicationWorkflow IApplicationWorkflowDataAccess.Update(ApplicationWorkflow applicationWorkflow)
        {
            const string SP = "dbo.ApplicationWorkflow_Update";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, applicationWorkflow.Id);
                Database.AddInParameter(cmd, "@Title", DbType.AnsiString, StringHelper.Convert(applicationWorkflow.Title));
                Database.AddInParameter(cmd, "@Description", DbType.AnsiString, StringHelper.Convert(applicationWorkflow.Description));
                Database.AddInParameter(cmd, "@RequireApproval", DbType.Boolean, applicationWorkflow.RequireApproval);
                Database.AddInParameter(cmd, "@TimeFrame", DbType.AnsiString, applicationWorkflow.TimeFrame);
                Database.AddInParameter(cmd, "@IsEmailAlert", DbType.Boolean, applicationWorkflow.IsEmailAlert);
                Database.AddInParameter(cmd, "@IsSmsAlert", DbType.Boolean, applicationWorkflow.IsSmsAlert);
                Database.AddInParameter(cmd, "@IsDashboardAlert", DbType.Boolean, applicationWorkflow.IsDashboardAlert);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        applicationWorkflow = CreateEntityBuilder<ApplicationWorkflow>().BuildEntity(reader);
                    }
                    else
                    {
                        applicationWorkflow = null;
                    }
                }

                if (applicationWorkflow == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Application workflow already exists. Please specify another application workflow.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while updating this application workflow.");
                            }
                    }
                }

                return applicationWorkflow;
            }
        }

        ApplicationWorkflow IApplicationWorkflowDataAccess.GetById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.ApplicationWorkflow_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<ApplicationWorkflow>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        ApplicationWorkflow IApplicationWorkflowDataAccess.GetByTitle(string title)
        {
            if (title == "")
            {
                throw new ArgumentNullException("title");
            }

            const string SP = "dbo.ApplicationWorkflow_GetByTitle";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Title", DbType.AnsiString, title);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<ApplicationWorkflow>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        ApplicationWorkflow IApplicationWorkflowDataAccess.GetByMemberId(int memberId)
        {
            if (memberId < 1)
            {
                throw new ArgumentNullException("memberId");
            }

            const string SP = "dbo.ApplicationWorkflow_GetApplicationWorkflowByMemberId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<ApplicationWorkflow>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        IList<ApplicationWorkflow> IApplicationWorkflowDataAccess.GetAll()
        {
            const string SP = "dbo.ApplicationWorkflow_GetAll";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<ApplicationWorkflow>().BuildEntities(reader);
                }
            }
        }

        bool IApplicationWorkflowDataAccess.DeleteById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.ApplicationWorkflow_DeleteById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a application workflow which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this application workflow.");
                        }
                }
            }
        }

        #endregion
    }
}