﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Helper;

namespace TPS360.DataAccess
{
    internal sealed class MemberObjectiveAndSummaryDataAccess : BaseDataAccess, IMemberObjectiveAndSummaryDataAccess
    {
        #region Constructors

        public MemberObjectiveAndSummaryDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<MemberObjectiveAndSummary> CreateEntityBuilder<MemberObjectiveAndSummary>()
        {
            return (new MemberObjectiveAndSummaryBuilder()) as IEntityBuilder<MemberObjectiveAndSummary>;
        }

        #endregion

        #region  Methods

        MemberObjectiveAndSummary IMemberObjectiveAndSummaryDataAccess.Add(MemberObjectiveAndSummary memberObjectiveAndSummary)
        {
            const string SP = "dbo.MemberObjectiveAndSummary_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Objective", DbType.AnsiString, StringHelper.Convert(memberObjectiveAndSummary.Objective));
                Database.AddInParameter(cmd, "@Summary", DbType.AnsiString, StringHelper.Convert(memberObjectiveAndSummary.Summary));
                Database.AddInParameter(cmd, "@CopyPasteResume", DbType.AnsiString, StringHelper.Convert(memberObjectiveAndSummary.CopyPasteResume));
                Database.AddInParameter(cmd, "@RawCopyPasteResume", DbType.AnsiString, StringHelper.Convert(memberObjectiveAndSummary.RawCopyPasteResume));                
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberObjectiveAndSummary.MemberId);
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, memberObjectiveAndSummary.IsRemoved);
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, memberObjectiveAndSummary.CreatorId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        memberObjectiveAndSummary = CreateEntityBuilder<MemberObjectiveAndSummary>().BuildEntity(reader);
                    }
                    else
                    {
                        memberObjectiveAndSummary = null;
                    }
                }

                if (memberObjectiveAndSummary == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("MemberObjectiveAndSummary already exists. Please specify another memberObjectiveAndSummary.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while creating this memberObjectiveAndSummary.");
                            }
                    }
                }

                return memberObjectiveAndSummary;
            }
        }

        MemberObjectiveAndSummary IMemberObjectiveAndSummaryDataAccess.Update(MemberObjectiveAndSummary memberObjectiveAndSummary)
        {
            const string SP = "dbo.MemberObjectiveAndSummary_Update";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, memberObjectiveAndSummary.Id);
                Database.AddInParameter(cmd, "@Objective", DbType.AnsiString, StringHelper.Convert(memberObjectiveAndSummary.Objective));
                Database.AddInParameter(cmd, "@Summary", DbType.AnsiString, StringHelper.Convert(memberObjectiveAndSummary.Summary));
                Database.AddInParameter(cmd, "@CopyPasteResume", DbType.AnsiString, StringHelper.Convert(memberObjectiveAndSummary.CopyPasteResume));
                Database.AddInParameter(cmd, "@RawCopyPasteResume", DbType.AnsiString, StringHelper.Convert(memberObjectiveAndSummary.RawCopyPasteResume)); 
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, memberObjectiveAndSummary.IsRemoved);
                Database.AddInParameter(cmd, "@UpdatorId", DbType.Int32, memberObjectiveAndSummary.UpdatorId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        memberObjectiveAndSummary = CreateEntityBuilder<MemberObjectiveAndSummary>().BuildEntity(reader);
                    }
                    else
                    {
                        memberObjectiveAndSummary = null;
                    }
                }

                if (memberObjectiveAndSummary == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("MemberObjectiveAndSummary already exists. Please specify another memberObjectiveAndSummary.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while updating this memberObjectiveAndSummary.");
                            }
                    }
                }

                return memberObjectiveAndSummary;
            }
        }

        MemberObjectiveAndSummary IMemberObjectiveAndSummaryDataAccess.GetById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.MemberObjectiveAndSummary_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<MemberObjectiveAndSummary>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        MemberObjectiveAndSummary IMemberObjectiveAndSummaryDataAccess.GetByMemberId(int memberId)
        {
            if (memberId < 1)
            {
                throw new ArgumentNullException("memberId");
            }

            const string SP = "dbo.MemberObjectiveAndSummary_GetMemberObjectiveAndSummaryByMemberId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<MemberObjectiveAndSummary>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        IList<MemberObjectiveAndSummary> IMemberObjectiveAndSummaryDataAccess.GetAll()
        {
            const string SP = "dbo.MemberObjectiveAndSummary_GetAll";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<MemberObjectiveAndSummary>().BuildEntities(reader);
                }
            }
        }

        bool IMemberObjectiveAndSummaryDataAccess.DeleteById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.MemberObjectiveAndSummary_DeleteById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a memberObjectiveAndSummary which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this memberObjectiveAndSummary.");
                        }
                }
            }
        }

        #endregion
    }
}