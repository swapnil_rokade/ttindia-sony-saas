﻿using System;
using System.Data;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    internal sealed class MemberOfferRejectionBuilder : IEntityBuilder<MemberOfferRejection>
    {
        IList<MemberOfferRejection> IEntityBuilder<MemberOfferRejection>.BuildEntities(IDataReader reader)
        {
            List<MemberOfferRejection> MemberOfferRejections = new List<MemberOfferRejection>();

            while (reader.Read())
            {
                MemberOfferRejections.Add(((IEntityBuilder<MemberOfferRejection>)this).BuildEntity(reader));
            }

            return (MemberOfferRejections.Count > 0) ? MemberOfferRejections : null;
        }

        MemberOfferRejection IEntityBuilder<MemberOfferRejection>.BuildEntity(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_REJECTEDDATE = 1;
            const int FLD_MEMBERID = 2;
            const int FLD_JOBPOSTINGID = 3;
            const int FLD_CREATORID = 4;
            const int FLD_UPDATORID = 5;
            const int FLD_CREATEDATE = 6;
            const int FLD_UPDATEDATE = 7;
            const int FLD_REASONFORREJECTIONLOOKUPID = 8;

            MemberOfferRejection MemberOfferRejection = new MemberOfferRejection();

            MemberOfferRejection.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            MemberOfferRejection.RejectedDate = reader.IsDBNull(FLD_REJECTEDDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_REJECTEDDATE);
            MemberOfferRejection.MemberId = reader.IsDBNull(FLD_MEMBERID) ? 0 : reader.GetInt32(FLD_MEMBERID);
            MemberOfferRejection.JobPostingId  = reader.IsDBNull(FLD_JOBPOSTINGID ) ? 0 : reader.GetInt32(FLD_JOBPOSTINGID );
            MemberOfferRejection.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
            MemberOfferRejection.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);
            MemberOfferRejection.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
            MemberOfferRejection.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);
            MemberOfferRejection.ReasonForRejectionLookUpID = reader.IsDBNull(FLD_REASONFORREJECTIONLOOKUPID) ? 0 : reader.GetInt32(FLD_REASONFORREJECTIONLOOKUPID);
            return MemberOfferRejection;
        }
    }
}