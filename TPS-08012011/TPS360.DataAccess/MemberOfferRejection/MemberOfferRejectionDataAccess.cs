﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;
using System.Collections;
using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Helper;

namespace TPS360.DataAccess
{
    internal sealed class MemberOfferRejectionDataAccess : BaseDataAccess, IMemberOfferRejectionDataAccess
    {
        #region Constructors

        public MemberOfferRejectionDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<MemberOfferRejection> CreateEntityBuilder<MemberOfferRejection>()
        {
            return (new MemberOfferRejectionBuilder()) as IEntityBuilder<MemberOfferRejection>;
        }

        #endregion

        #region  Methods

        void  IMemberOfferRejectionDataAccess.Add(MemberOfferRejection MemberOfferRejection,string MemberId)
        {
            const string SP = "dbo.OfferRejection_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@RejectedDate", DbType.DateTime, NullConverter.Convert(MemberOfferRejection.RejectedDate ));
                Database.AddInParameter(cmd, "@MemberId", DbType.AnsiString  , MemberId );
                Database.AddInParameter(cmd, "@JobPostingId", DbType.Int32 , MemberOfferRejection.JobPostingId );
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, MemberOfferRejection.CreatorId);
                Database.AddInParameter(cmd, "@ReasonForRejectionLookUpID", DbType.Int32, MemberOfferRejection.ReasonForRejectionLookUpID);

                Database.ExecuteNonQuery(cmd);
            }
        }

        MemberOfferRejection IMemberOfferRejectionDataAccess.GetById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.OfferRejection_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<MemberOfferRejection>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        MemberOfferRejection IMemberOfferRejectionDataAccess.GetByMemberIdAndJobPosstingID (int memberId, int jobpostingid)
        {
            if (memberId < 1)
            {
                throw new ArgumentNullException("memberId");
            }
            if (jobpostingid  < 1)
            {
                throw new ArgumentNullException("jobpostingid");
            }
            const string SP = "dbo.OfferRejection_GetByMemberIDAndJobPostingId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberId);
                Database.AddInParameter(cmd, "@JobPostingId", DbType.Int32, jobpostingid );

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<MemberOfferRejection>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }           
        }

        //bool IMemberOfferRejectionDataAccess.DeleteHiringDetialsById(int HiringDetailsID)
        //{
        //    if (HiringDetailsID < 1)
        //    {
        //        throw new ArgumentNullException("id");
        //    }

        //    const string SP = "dbo.MemberOfferRejection_DeleteById";

        //    using (DbCommand cmd = Database.GetStoredProcCommand(SP))
        //    {
        //        AddOutputParameter(cmd);
        //        Database.AddInParameter(cmd, "@Id", DbType.Int32, HiringDetailsID);

        //        Database.ExecuteNonQuery(cmd);

        //        int returnCode = GetReturnCodeFromParameter(cmd);

        //        switch (returnCode)
        //        {
        //            case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
        //                {
        //                    return true;
        //                }
        //            case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
        //                {
        //                    throw new ArgumentException("Cannot delete a member Hiring Detail which has association.");
        //                }
        //            default:
        //                {
        //                    throw new SystemException("An unexpected error has occurred while deleting this member hiring detail.");
        //                }
        //        }
        //    }
        //}

        #endregion
    }
}