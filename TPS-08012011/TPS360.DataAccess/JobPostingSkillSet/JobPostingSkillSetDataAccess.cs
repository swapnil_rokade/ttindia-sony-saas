﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: JobPostingSkillSetDataAccess.cs
    Description: This is the class library page.
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1              Nov-10-2008           Jagadish            Defect ID: 9184; (Kaizen) Added a method called 'DeleteByJobPostingID'
                                                               to delete jobposting skills having same JobPostingId.
-------------------------------------------------------------------------------------------------------------------------------------------        
*/
using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    internal sealed class JobPostingSkillSetDataAccess : BaseDataAccess, IJobPostingSkillSetDataAccess
    {
        #region Constructors

        public JobPostingSkillSetDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<JobPostingSkillSet> CreateEntityBuilder<JobPostingSkillSet>()
        {
            return (new JobPostingSkillSetBuilder()) as IEntityBuilder<JobPostingSkillSet>;
        }

        #endregion

        #region  Methods

        JobPostingSkillSet IJobPostingSkillSetDataAccess.Add(JobPostingSkillSet jobPostingSkillSet)
        {
            const string SP = "dbo.JobPostingSkillSet_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@JobPostingId", DbType.Int32, jobPostingSkillSet.JobPostingId);
                Database.AddInParameter(cmd, "@SkillId", DbType.Int32, jobPostingSkillSet.SkillId);
                Database.AddInParameter(cmd, "@ProficiencyLookupId", DbType.Int32, jobPostingSkillSet.ProficiencyLookupId);
                Database.AddInParameter(cmd, "@NoOfYearsExp", DbType.Int32, jobPostingSkillSet.NoOfYearsExp);
                Database.AddInParameter(cmd, "@AssessmentRequired", DbType.Boolean, jobPostingSkillSet.AssessmentRequired);
                
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        jobPostingSkillSet = CreateEntityBuilder<JobPostingSkillSet>().BuildEntity(reader);
                    }
                    else
                    {
                        jobPostingSkillSet = null;
                    }
                }

                if (jobPostingSkillSet == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Job posting skill set already exists. Please specify another job posting skill set.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while creating this job posting skill set.");
                            }
                    }
                }

                return jobPostingSkillSet;
            }
        }

        JobPostingSkillSet IJobPostingSkillSetDataAccess.Update(JobPostingSkillSet jobPostingSkillSet)
        {
            const string SP = "dbo.JobPostingSkillSet_Update";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, jobPostingSkillSet.Id);
                Database.AddInParameter(cmd, "@ProficiencyLookupId", DbType.Int32, jobPostingSkillSet.ProficiencyLookupId);
                Database.AddInParameter(cmd, "@NoOfYearsExp", DbType.Int32, jobPostingSkillSet.NoOfYearsExp);
                Database.AddInParameter(cmd, "@AssessmentRequired", DbType.Boolean, jobPostingSkillSet.AssessmentRequired);
                
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        jobPostingSkillSet = CreateEntityBuilder<JobPostingSkillSet>().BuildEntity(reader);
                    }
                    else
                    {
                        jobPostingSkillSet = null;
                    }
                }

                if (jobPostingSkillSet == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Job posting skill set already exists. Please specify another job posting skill set.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while updating this job posting skill set.");
                            }
                    }
                }

                return jobPostingSkillSet;
            }
        }

        JobPostingSkillSet IJobPostingSkillSetDataAccess.GetById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.JobPostingSkillSet_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<JobPostingSkillSet>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        JobPostingSkillSet IJobPostingSkillSetDataAccess.GetBySkillId(int skillId)
        {
            if (skillId < 1)
            {
                throw new ArgumentNullException("skillId");
            }

            const string SP = "dbo.JobPostingSkillSet_GetJobPostingSkillSetBySkillId";
            

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@SkillId", DbType.Int32, skillId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<JobPostingSkillSet>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        JobPostingSkillSet IJobPostingSkillSetDataAccess.GetByJobPostingIdAndSkillId(int jobPostingId, int skillId)
        {
            if (jobPostingId < 1)
            {
                throw new ArgumentNullException("jobPostingId");
            }

            if (skillId < 1)
            {
                throw new ArgumentNullException("skillId");
            }

            const string SP = "dbo.JobPostingSkillSet_GetJobPostingSkillSetByJobPostingIdAndSkillId";


            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@JobPostingId", DbType.Int32, jobPostingId);
                Database.AddInParameter(cmd, "@SkillId", DbType.Int32, skillId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<JobPostingSkillSet>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }
        
        IList<JobPostingSkillSet> IJobPostingSkillSetDataAccess.GetAll()
        {
            const string SP = "dbo.JobPostingSkillSet_GetAll";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<JobPostingSkillSet>().BuildEntities(reader);
                }
            }
        }

        bool IJobPostingSkillSetDataAccess.DeleteById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.JobPostingSkillSet_DeleteById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a job posting skill set which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this job posting skill set.");
                        }
                }
            }
        }
        // 0.1 starts here
        bool IJobPostingSkillSetDataAccess.DeleteByJobPostingID(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.JobPostingSkillSet_DeleteJobPostingSkillSetByJobPostingId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@JobPostingId", DbType.Int32, id);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a job posting skill set which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this job posting skill set.");
                        }
                }
            }
        }
        // 0.1 ends here

        IList<JobPostingSkillSet> IJobPostingSkillSetDataAccess.GetAllByJobPostingId(int jobPostingId)
        {
            const string SP = "dbo.JobPostingSkillSet_GetJobPostingSkillSetByJobPostingId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@JobPostingId", DbType.Int32, jobPostingId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<JobPostingSkillSet>().BuildEntities(reader);
                }
            }
        }

        #endregion
    }
}