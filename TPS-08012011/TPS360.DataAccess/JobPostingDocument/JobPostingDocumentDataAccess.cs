﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    internal sealed class JobPostingDocumentDataAccess : BaseDataAccess, IJobPostingDocumentDataAccess
    {
        #region Constructors

        public JobPostingDocumentDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<JobPostingDocument> CreateEntityBuilder<JobPostingDocument>()
        {
            return (new JobPostingDocumentBuilder()) as IEntityBuilder<JobPostingDocument>;
        }

        #endregion

        #region  Methods

        JobPostingDocument IJobPostingDocumentDataAccess.Add(JobPostingDocument jobPostingDocument)
        {
            const string SP = "dbo.JobPostingDocument_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Title", DbType.AnsiString, jobPostingDocument.Title);
                Database.AddInParameter(cmd, "@FileName", DbType.AnsiString, jobPostingDocument.FileName);
                Database.AddInParameter(cmd, "@DocumentType", DbType.Int32, jobPostingDocument.DocumentType);
                Database.AddInParameter(cmd, "@JobPostingId", DbType.Int32, jobPostingDocument.JobPostingId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        jobPostingDocument = CreateEntityBuilder<JobPostingDocument>().BuildEntity(reader);
                    }
                    else
                    {
                        jobPostingDocument = null;
                    }
                }

                if (jobPostingDocument == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("JobPostingDocument already exists. Please specify another jobPostingDocument.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while creating this jobPostingDocument.");
                            }
                    }
                }

                return jobPostingDocument;
            }
        }

        JobPostingDocument IJobPostingDocumentDataAccess.Update(JobPostingDocument jobPostingDocument)
        {
            const string SP = "dbo.JobPostingDocument_Update";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, jobPostingDocument.Id);
                Database.AddInParameter(cmd, "@Title", DbType.AnsiString, jobPostingDocument.Title);
                Database.AddInParameter(cmd, "@FileName", DbType.AnsiString, jobPostingDocument.FileName);
                Database.AddInParameter(cmd, "@DocumentType", DbType.Int32, jobPostingDocument.DocumentType);
                Database.AddInParameter(cmd, "@JobPostingId", DbType.Int32, jobPostingDocument.JobPostingId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        jobPostingDocument = CreateEntityBuilder<JobPostingDocument>().BuildEntity(reader);
                    }
                    else
                    {
                        jobPostingDocument = null;
                    }
                }

                if (jobPostingDocument == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("JobPostingDocument already exists. Please specify another jobPostingDocument.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while updating this jobPostingDocument.");
                            }
                    }
                }

                return jobPostingDocument;
            }
        }

        JobPostingDocument IJobPostingDocumentDataAccess.GetById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.JobPostingDocument_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<JobPostingDocument>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        IList<JobPostingDocument> IJobPostingDocumentDataAccess.GetAllByJobPostingId(int jobPostingId)
        {
            if (jobPostingId < 1)
            {
                throw new ArgumentNullException("jobPostingId");
            }
            
            const string SP = "dbo.JobPostingDocument_GetAllJobPostingDocumentByJobPostingId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@JobPostingId", DbType.Int32, jobPostingId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<JobPostingDocument>().BuildEntities(reader);
                }
            }
        }
        
        IList<JobPostingDocument> IJobPostingDocumentDataAccess.GetAll()
        {
            const string SP = "dbo.JobPostingDocument_GetAll";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<JobPostingDocument>().BuildEntities(reader);
                }
            }
        }
        
        bool IJobPostingDocumentDataAccess.DeleteById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.JobPostingDocument_DeleteById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a jobPostingDocument which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this jobPostingDocument.");
                        }
                }
            }
        }

        bool IJobPostingDocumentDataAccess.DeleteByJobPostingId(int jobPostingId)
        {
            if (jobPostingId < 1)
            {
                throw new ArgumentNullException("jobPostingId");
            }

            const string SP = "dbo.JobPostingDocument_DeleteJobPostingDocumentByJobPostingId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@JobPostingId", DbType.Int32, jobPostingId);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a jobPostingDocument which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this jobPostingDocument.");
                        }
                }
            }
        }

        #endregion
    }
}