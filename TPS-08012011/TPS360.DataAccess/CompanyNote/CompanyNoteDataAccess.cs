﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    internal sealed class CompanyNoteDataAccess : BaseDataAccess, ICompanyNoteDataAccess
    {
        #region Constructors

        public CompanyNoteDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<CompanyNote> CreateEntityBuilder<CompanyNote>()
        {
            return (new CompanyNoteBuilder()) as IEntityBuilder<CompanyNote>;
        }

        #endregion

        #region  Methods

        CompanyNote ICompanyNoteDataAccess.Add(CompanyNote companyNote)
        {
            const string SP = "dbo.CompanyNote_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@CompanyId", DbType.Int32, companyNote.CompanyId);
                Database.AddInParameter(cmd, "@CommonNoteId", DbType.Int32, companyNote.CommonNoteId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        companyNote = CreateEntityBuilder<CompanyNote>().BuildEntity(reader);
                    }
                    else
                    {
                        companyNote = null;
                    }
                }

                if (companyNote == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Company note already exists. Please specify another company note.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while creating this company note.");
                            }
                    }
                }

                return companyNote;
            }
        }

        CompanyNote ICompanyNoteDataAccess.GetById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.CompanyNote_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<CompanyNote>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        IList<CompanyNote> ICompanyNoteDataAccess.GetAll()
        {
            const string SP = "dbo.CompanyNote_GetAll";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<CompanyNote>().BuildEntities(reader);
                }
            }
        }

        IList<CompanyNote> ICompanyNoteDataAccess.GetAllByCompanyId(int companyId)
        {
            const string SP = "dbo.CompanyNote_GetAllByCompanyId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@CompanyId", DbType.Int32, companyId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<CompanyNote>().BuildEntities(reader);
                }
            }
        }

        bool ICompanyNoteDataAccess.DeleteById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.CompanyNote_DeleteById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a company note which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this company note.");
                        }
                }
            }
        }

        bool ICompanyNoteDataAccess.DeleteByCompanyId(int companyId)
        {
            if (companyId < 1)
            {
                throw new ArgumentNullException("companyId");
            }

            const string SP = "dbo.CompanyNote_DeleteByCompanyId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@CompanyId", DbType.Int32, companyId);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a company note which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this company note.");
                        }
                }
            }
        }

        #endregion
    }
}