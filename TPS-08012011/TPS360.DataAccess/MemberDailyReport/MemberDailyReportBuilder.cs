﻿using System;
using System.Data;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    internal sealed class MemberDailyReportBuilder : IEntityBuilder<MemberDailyReport>
    {
        IList<MemberDailyReport> IEntityBuilder<MemberDailyReport>.BuildEntities(IDataReader reader)
        {
            List<MemberDailyReport> memberDailyReports = new List<MemberDailyReport>();

            while (reader.Read())
            {
                memberDailyReports.Add(((IEntityBuilder<MemberDailyReport>)this).BuildEntity(reader));
            }

            return (memberDailyReports.Count > 0) ? memberDailyReports : null;
        }

        MemberDailyReport IEntityBuilder<MemberDailyReport>.BuildEntity(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_LOGINTIME = 1;
            const int FLD_LOGOUTTIME = 2;
            const int FLD_WORKDESCRIPTION = 3;
            const int FLD_MANAGEMENTNOTE = 4;
            const int FLD_MEMBERID = 5;
            const int FLD_ISREMOVED = 6;
            const int FLD_CREATORID = 7;
            const int FLD_UPDATORID = 8;
            const int FLD_CREATEDATE = 9;
            const int FLD_UPDATEDATE = 10;

            MemberDailyReport memberDailyReport = new MemberDailyReport();

            memberDailyReport.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            memberDailyReport.LoginTime = reader.IsDBNull(FLD_LOGINTIME) ? DateTime.MinValue : reader.GetDateTime(FLD_LOGINTIME);
            memberDailyReport.LogoutTime = reader.IsDBNull(FLD_LOGOUTTIME) ? DateTime.MinValue : reader.GetDateTime(FLD_LOGOUTTIME);
            memberDailyReport.WorkDescription = reader.IsDBNull(FLD_WORKDESCRIPTION) ? string.Empty : reader.GetString(FLD_WORKDESCRIPTION);
            memberDailyReport.ManagementNote = reader.IsDBNull(FLD_MANAGEMENTNOTE) ? string.Empty : reader.GetString(FLD_MANAGEMENTNOTE);
            memberDailyReport.MemberId = reader.IsDBNull(FLD_MEMBERID) ? 0 : reader.GetInt32(FLD_MEMBERID);
            memberDailyReport.IsRemoved = reader.IsDBNull(FLD_ISREMOVED) ? false : reader.GetBoolean(FLD_ISREMOVED);
            memberDailyReport.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
            memberDailyReport.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);
            memberDailyReport.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
            memberDailyReport.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);

            return memberDailyReport;
        }
    }
}