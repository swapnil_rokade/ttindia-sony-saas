﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Helper;
using System.Collections;

namespace TPS360.DataAccess
{
    internal sealed class MemberDailyReportDataAccess : BaseDataAccess, IMemberDailyReportDataAccess
    {
        #region Constructors

        public MemberDailyReportDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<MemberDailyReport> CreateEntityBuilder<MemberDailyReport>()
        {
            return (new MemberDailyReportBuilder()) as IEntityBuilder<MemberDailyReport>;
        }

        #endregion

        #region  Methods

        MemberDailyReport IMemberDailyReportDataAccess.Add(MemberDailyReport memberDailyReport)
        {
            const string SP = "dbo.MemberDailyReport_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@LoginTime", DbType.DateTime, NullConverter.Convert(memberDailyReport.LoginTime));
                Database.AddInParameter(cmd, "@LogoutTime", DbType.DateTime, NullConverter.Convert(memberDailyReport.LogoutTime));
                Database.AddInParameter(cmd, "@WorkDescription", DbType.AnsiString, memberDailyReport.WorkDescription);
                Database.AddInParameter(cmd, "@ManagementNote", DbType.AnsiString, memberDailyReport.ManagementNote);
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberDailyReport.MemberId);
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, memberDailyReport.IsRemoved);
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, memberDailyReport.CreatorId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        memberDailyReport = CreateEntityBuilder<MemberDailyReport>().BuildEntity(reader);
                    }
                    else
                    {
                        memberDailyReport = null;
                    }
                }

                if (memberDailyReport == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("MemberDailyReport already exists. Please specify another memberDailyReport.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while creating this memberDailyReport.");
                            }
                    }
                }

                return memberDailyReport;
            }
        }

        MemberDailyReport IMemberDailyReportDataAccess.Update(MemberDailyReport memberDailyReport)
        {
            const string SP = "dbo.MemberDailyReport_Update";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, memberDailyReport.Id);
                Database.AddInParameter(cmd, "@LoginTime", DbType.DateTime, NullConverter.Convert(memberDailyReport.LoginTime));
                Database.AddInParameter(cmd, "@LogoutTime", DbType.DateTime, NullConverter.Convert(memberDailyReport.LogoutTime));
                Database.AddInParameter(cmd, "@WorkDescription", DbType.AnsiString, memberDailyReport.WorkDescription);
                Database.AddInParameter(cmd, "@ManagementNote", DbType.AnsiString, memberDailyReport.ManagementNote);
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, memberDailyReport.IsRemoved);
                Database.AddInParameter(cmd, "@UpdatorId", DbType.Int32, memberDailyReport.UpdatorId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        memberDailyReport = CreateEntityBuilder<MemberDailyReport>().BuildEntity(reader);
                    }
                    else
                    {
                        memberDailyReport = null;
                    }
                }

                if (memberDailyReport == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("MemberDailyReport already exists. Please specify another memberDailyReport.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while updating this memberDailyReport.");
                            }
                    }
                }

                return memberDailyReport;
            }
        }

        bool IMemberDailyReportDataAccess.UpdateAllByLoginTimeAndMemberId(DateTime loginTime, int memberId)
        {
            const string SP = "dbo.MemberDailyReport_UpdateAllByLoginTimeAndMemberId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberId);
                Database.AddInParameter(cmd, "@LoginTime", DbType.DateTime, loginTime);
                
                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_UPDATE:
                        {
                            return true;
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this memberDailyReport.");
                        }
                }
            }
        }

        MemberDailyReport IMemberDailyReportDataAccess.GetById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.MemberDailyReport_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<MemberDailyReport>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        MemberDailyReport IMemberDailyReportDataAccess.GetByLoginTimeAndMemberId(DateTime loginTime,int memberId)
        {
            if (loginTime == null || loginTime==DateTime.MinValue)
            {
                throw new ArgumentNullException("loginTime");
            }

            if (memberId < 1)
            {
                throw new ArgumentNullException("memberId");
            }

            const string SP = "dbo.MemberDailyReport_GetByLoginTimeAndMemberId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@LoginTime", DbType.DateTime, loginTime);
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<MemberDailyReport>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        IList<MemberDailyReport> IMemberDailyReportDataAccess.GetAll()
        {
            const string SP = "dbo.MemberDailyReport_GetAll";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<MemberDailyReport>().BuildEntities(reader);
                }
            }
        }
        
        PagedResponse<MemberDailyReport> IMemberDailyReportDataAccess.GetPaged(PagedRequest request)
        {
            const string SP = "dbo.MemberDailyReport_GetPaged";
            string whereClause = string.Empty;
            
            if (request.Conditions.Count > 0)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();

                string column = null;
                string value = null;

                foreach (DictionaryEntry entry in request.Conditions)
                {
                    column = StringHelper.Convert(entry.Key);
                    value = StringHelper.Convert(entry.Value);

                    if (!StringHelper.IsBlank(column))
                    {
                        value = value.Replace("'", "''");

                        if (StringHelper.IsEqual(column, "memberId"))
                        {
                            sb.Append("[MDR].[MemberId]");
                            sb.Append(" = ");
                            sb.Append(value);
                        }
                    }
                }

                whereClause = sb.ToString();
            }
            
            if (StringHelper.IsBlank(request.SortColumn))
            {
                request.SortColumn = "LoginTime";
                request.SortOrder = "DESC";
            }

            //request.SortColumn = "[MDR].[" + request.SortColumn + "]";

            //0.1 starts here
            //9409
            request.SortColumn = "(" + request.SortColumn + ")";

            object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder)
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<MemberDailyReport> response = new PagedResponse<MemberDailyReport>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = CreateEntityBuilder<MemberDailyReport>().BuildEntities(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }
        
        bool IMemberDailyReportDataAccess.DeleteById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.MemberDailyReport_DeleteById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a memberDailyReport which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this memberDailyReport.");
                        }
                }
            }
        }

        #endregion
    }
}