﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
namespace TPS360.DataAccess
{
    internal sealed class JobPostingSearchAgentDataAccess : BaseDataAccess, IJobPostingSearchAgentDataAccess 
    {
        #region Constructors

        public JobPostingSearchAgentDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<JobPostingSearchAgent> CreateEntityBuilder<JobPostingSearchAgent>()
        {
            return (new JobPostingSearchAgentBuilder()) as IEntityBuilder<JobPostingSearchAgent>;
        }

        #endregion

        #region Methods

        JobPostingSearchAgent IJobPostingSearchAgentDataAccess.Add(JobPostingSearchAgent jobPosting)
        {
            const string SP = "dbo.JobPostingSearchAgent_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@JobPostingId", DbType.Int32, jobPosting.JobPostingId );
                Database.AddInParameter(cmd, "@Keywords", DbType.AnsiString, StringHelper.Convert(jobPosting.KeyWord ));
                Database.AddInParameter(cmd, "@JobTitle", DbType.AnsiString, StringHelper.Convert(jobPosting.JobTitle));
                Database.AddInParameter(cmd, "@City", DbType.AnsiString, StringHelper.Convert(jobPosting.City));
                Database.AddInParameter(cmd, "@StateId", DbType.Int32, jobPosting.StateId);
                Database.AddInParameter(cmd, "@CountryId", DbType.Int32, jobPosting.CountryId);
                Database.AddInParameter(cmd, "@MinExperience", DbType.AnsiString, StringHelper.Convert(jobPosting.MinExperience ));
                Database.AddInParameter(cmd, "@MaxExperience", DbType.AnsiString, StringHelper.Convert(jobPosting.MaxExperience ));
                Database.AddInParameter(cmd, "@SalaryFrom", DbType.AnsiString, StringHelper.Convert(jobPosting.SalaryFrom ));
                Database.AddInParameter(cmd, "@SalaryTo", DbType.AnsiString, StringHelper.Convert(jobPosting.SalaryTo ));
                Database.AddInParameter(cmd, "@CurrencyLookupId", DbType.Int32, jobPosting.CurrencyLookUpId );
                Database.AddInParameter(cmd, "@SalaryType", DbType.AnsiString, StringHelper.Convert(jobPosting.SalaryType ));
                Database.AddInParameter(cmd, "@HotlistId", DbType.Int32, jobPosting.HotListId );
                Database.AddInParameter(cmd, "@EducationLookUpId", DbType.AnsiString, StringHelper.Convert(jobPosting.EducationLookupId ));
                Database.AddInParameter(cmd, "@EmploymentTypeLookUpId", DbType.Int32 , jobPosting.EmployementTypeLookUpId );
                Database.AddInParameter(cmd, "@ResumeLastUpdated", DbType.AnsiString, StringHelper.Convert(jobPosting.ResumeLastUpdated ));
                Database.AddInParameter(cmd, "@CandidateTypeLookUpId", DbType.Int32 , jobPosting.CandidateTypeLookUpId );
                Database.AddInParameter(cmd, "@WorkScheduleLookUpId", DbType.Int32 , jobPosting.WorkScheduleLookupId );
                Database.AddInParameter(cmd, "@WorkStatusLookUpId", DbType.AnsiString, StringHelper.Convert(jobPosting.WorkStatusLookUpId ));
                Database.AddInParameter(cmd, "@HiringStatusLookUpId", DbType.AnsiString, StringHelper.Convert(jobPosting.HiringStatusLookUpId ));
                Database.AddInParameter(cmd, "@AvailableAfter", DbType.DateTime, NullConverter.Convert(jobPosting.AvailableAfter ));
                Database.AddInParameter(cmd, "@SecurityClearance", DbType.Boolean, jobPosting.SecurityClearance);
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, jobPosting.CreatorId);
                Database.AddInParameter(cmd, "@UpdatorId", DbType.Int32, jobPosting.UpdatorId );
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, jobPosting.IsRemoved);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        jobPosting = CreateEntityBuilder<JobPostingSearchAgent>().BuildEntity(reader);
                    }
                    else
                    {
                        jobPosting = null;
                    }
                }

                if (jobPosting == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Job posting agent already exists. Please specify another job posting agent.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while creating this job posting agent.");
                            }
                    }
                }

                return jobPosting;
            }
        }

        JobPostingSearchAgent IJobPostingSearchAgentDataAccess.Update(JobPostingSearchAgent jobPosting)
        {
            const string SP = "dbo.JobPostingSearchAgent_Update";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, jobPosting.Id);
                Database.AddInParameter(cmd, "@JobPostingId", DbType.Int32, jobPosting.JobPostingId);
                Database.AddInParameter(cmd, "@Keywords", DbType.AnsiString, StringHelper.Convert(jobPosting.KeyWord));
                Database.AddInParameter(cmd, "@JobTitle", DbType.AnsiString, StringHelper.Convert(jobPosting.JobTitle));
                Database.AddInParameter(cmd, "@City", DbType.AnsiString, StringHelper.Convert(jobPosting.City));
                Database.AddInParameter(cmd, "@StateId", DbType.Int32, jobPosting.StateId);
                Database.AddInParameter(cmd, "@CountryId", DbType.Int32, jobPosting.CountryId);
                Database.AddInParameter(cmd, "@MinExperience", DbType.AnsiString, StringHelper.Convert(jobPosting.MinExperience));
                Database.AddInParameter(cmd, "@MaxExperience", DbType.AnsiString, StringHelper.Convert(jobPosting.MaxExperience));
                Database.AddInParameter(cmd, "@SalaryFrom", DbType.AnsiString, StringHelper.Convert(jobPosting.SalaryFrom));
                Database.AddInParameter(cmd, "@SalaryTo", DbType.AnsiString, StringHelper.Convert(jobPosting.SalaryTo));
                Database.AddInParameter(cmd, "@CurrencyLookupId", DbType.Int32, jobPosting.CurrencyLookUpId);
                Database.AddInParameter(cmd, "@SalaryType", DbType.AnsiString, StringHelper.Convert(jobPosting.SalaryType));
                Database.AddInParameter(cmd, "@HotlistId", DbType.Int32, jobPosting.HotListId);
                Database.AddInParameter(cmd, "@EducationLookUpId", DbType.AnsiString, StringHelper.Convert(jobPosting.EducationLookupId));
                Database.AddInParameter(cmd, "@EmploymentTypeLookUpId", DbType.Int32, jobPosting.EmployementTypeLookUpId);
                Database.AddInParameter(cmd, "@ResumeLastUpdated", DbType.AnsiString, StringHelper.Convert(jobPosting.ResumeLastUpdated));
                Database.AddInParameter(cmd, "@CandidateTypeLookUpId", DbType.Int32, jobPosting.CandidateTypeLookUpId);
                Database.AddInParameter(cmd, "@WorkScheduleLookUpId", DbType.Int32, jobPosting.WorkScheduleLookupId);
                Database.AddInParameter(cmd, "@WorkStatusLookUpId", DbType.AnsiString, StringHelper.Convert(jobPosting.WorkStatusLookUpId));
                Database.AddInParameter(cmd, "@HiringStatusLookUpId", DbType.AnsiString, StringHelper.Convert(jobPosting.HiringStatusLookUpId));
                Database.AddInParameter(cmd, "@AvailableAfter", DbType.DateTime, NullConverter.Convert(jobPosting.AvailableAfter));
                Database.AddInParameter(cmd, "@SecurityClearance", DbType.Boolean, jobPosting.SecurityClearance);
                Database.AddInParameter(cmd, "@UpdatorId", DbType.Int32, jobPosting.UpdatorId);
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, jobPosting.IsRemoved);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        jobPosting = CreateEntityBuilder<JobPostingSearchAgent>().BuildEntity(reader);
                    }
                    else
                    {
                        jobPosting = null;
                    }
                }

                if (jobPosting == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Job posting agent already exists. Please specify another job posting agent.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while updating this job posting agent.");
                            }
                    }
                }

                return jobPosting;
            }
        }

        JobPostingSearchAgent IJobPostingSearchAgentDataAccess.GetById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.JobPostingSearchAgent_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<JobPostingSearchAgent>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        JobPostingSearchAgent IJobPostingSearchAgentDataAccess.GetByJobPostingId(int JobPostingId)
        {
            if (JobPostingId < 1)
            {
                throw new ArgumentNullException("JobPostingId");
            }

            const string SP = "dbo.JobPostingSearchAgent_GetByJobPostingId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@JobPostingId", DbType.Int32, JobPostingId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<JobPostingSearchAgent>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        void IJobPostingSearchAgentDataAccess.UpdateByJobPostingIdAndIsRemoved(int JobPostingId,bool IsRemoved)
        {
            const string SP = "dbo.JobPostingSearchAgent_UpdateByJobPostingIdAndIsRemoved";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@JobPostingId", DbType.Int32, JobPostingId );
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, IsRemoved );
                Database.ExecuteNonQuery(cmd);
            }
        }

        bool IJobPostingSearchAgentDataAccess.DeleteByJobPostingId(int JobPostingId)
        {
            if (JobPostingId < 1)
            {
                throw new ArgumentNullException("JobPostingId");
            }

            const string SP = "dbo.JobPostingSearchAgent_DeleteByJobPostingId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@JobPostingId", DbType.Int32, JobPostingId);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a job posting agent which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this job posting agent.");
                        }
                }
            }
        }

        #endregion
    }
}
