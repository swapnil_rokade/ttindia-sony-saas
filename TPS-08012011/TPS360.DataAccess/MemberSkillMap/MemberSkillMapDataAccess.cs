﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Collections;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Helper;

namespace TPS360.DataAccess
{
    internal sealed class MemberSkillMapDataAccess : BaseDataAccess, IMemberSkillMapDataAccess
    {
        #region Constructors

        public MemberSkillMapDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<MemberSkillMap> CreateEntityBuilder<MemberSkillMap>()
        {
            return (new MemberSkillMapBuilder()) as IEntityBuilder<MemberSkillMap>;
        }

        #endregion

        #region  Methods

        MemberSkillMap IMemberSkillMapDataAccess.Add(MemberSkillMap memberSkillMap)
        {
            const string SP = "dbo.MemberSkillMap_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@ProficiencyLookupId", DbType.Int32, memberSkillMap.ProficiencyLookupId);
                Database.AddInParameter(cmd, "@YearsOfExperience", DbType.Int32, memberSkillMap.YearsOfExperience);
                Database.AddInParameter(cmd, "@LastUsed", DbType.DateTime, NullConverter.Convert(memberSkillMap.LastUsed));
                Database.AddInParameter(cmd, "@Comment", DbType.AnsiString, StringHelper.Convert(memberSkillMap.Comment));
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, memberSkillMap.IsRemoved);
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberSkillMap.MemberId);
                Database.AddInParameter(cmd, "@SkillId", DbType.Int32, memberSkillMap.SkillId);
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, memberSkillMap.CreatorId);                

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        memberSkillMap = CreateEntityBuilder<MemberSkillMap>().BuildEntity(reader);
                    }
                    else
                    {
                        memberSkillMap = null;
                    }
                }

                if (memberSkillMap == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("MemberSkillMap already exists. Please specify another memberSkillMap.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while creating this memberSkillMap.");
                            }
                    }
                }

                return memberSkillMap;
            }
        }

        MemberSkillMap IMemberSkillMapDataAccess.Update(MemberSkillMap memberSkillMap)
        {
            const string SP = "dbo.MemberSkillMap_Update";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, memberSkillMap.Id);
                Database.AddInParameter(cmd, "@SkillId", DbType.Int32, memberSkillMap.SkillId );
                Database.AddInParameter(cmd, "@ProficiencyLookupId", DbType.Int32, memberSkillMap.ProficiencyLookupId);
                Database.AddInParameter(cmd, "@YearsOfExperience", DbType.Int32, memberSkillMap.YearsOfExperience);
                Database.AddInParameter(cmd, "@LastUsed", DbType.DateTime, NullConverter.Convert(memberSkillMap.LastUsed));
                Database.AddInParameter(cmd, "@Comment", DbType.AnsiString, StringHelper.Convert(memberSkillMap.Comment));
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, memberSkillMap.IsRemoved);
                Database.AddInParameter(cmd, "@UpdatorId", DbType.Int32, memberSkillMap.UpdatorId);               

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        memberSkillMap = CreateEntityBuilder<MemberSkillMap>().BuildEntity(reader);
                    }
                    else
                    {
                        memberSkillMap = null;
                    }
                }

                if (memberSkillMap == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("MemberSkillMap already exists. Please specify another memberSkillMap.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while updating this memberSkillMap.");
                            }
                    }
                }

                return memberSkillMap;
            }
        }

        MemberSkillMap IMemberSkillMapDataAccess.GetById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.MemberSkillMap_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<MemberSkillMap>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        MemberSkillMap IMemberSkillMapDataAccess.GetByMemberIdAndSkillId(int memberId, int skillId)
        {
            if (memberId < 1)
            {
                throw new ArgumentNullException("memberId");
            }

            if (skillId < 1)
            {
                throw new ArgumentNullException("skillId");
            }

            const string SP = "dbo.MemberSkillMap_GetMemberSkillMapByMemberIdAndSkillId";


            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberId);
                Database.AddInParameter(cmd, "@SkillId", DbType.Int32, skillId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<MemberSkillMap>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        IList<MemberSkillMap> IMemberSkillMapDataAccess.GetAll()
        {
            const string SP = "dbo.MemberSkillMap_GetAll";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<MemberSkillMap>().BuildEntities(reader);
                }
            }
        }

        IList<MemberSkillMap> IMemberSkillMapDataAccess.GetAllByMemberId(int memberId)
        {
            if (memberId < 1)
            {
                throw new ArgumentNullException("memberId");
            }

            const string SP = "dbo.MemberSkillMap_GetAllMemberSkillMapByMemberId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberId);
                
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<MemberSkillMap>().BuildEntities(reader);
                }
            }
        }

        bool IMemberSkillMapDataAccess.DeleteById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.MemberSkillMap_DeleteById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a memberSkillMap which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this memberSkillMap.");
                        }
                }
            }
        }


        bool IMemberSkillMapDataAccess.DeleteByIds(string  id)
        {
          
            const string SP = "dbo.MemberSkillMap_DeleteByIds";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.AnsiString , id);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a memberSkillMap which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this memberSkillMap.");
                        }
                }
            }
        }

        bool IMemberSkillMapDataAccess.DeleteByMemberId(int MemberId)
        {
            if (MemberId < 1)
            {
                throw new ArgumentNullException("MemberId");
            }

            const string SP = "dbo.MemberSkillMap_DeleteMemberSkillMapByMemberId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, MemberId);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a memberSkillMap which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this memberSkillMap.");
                        }
                }
            }
        }



        PagedResponse<MemberSkillMap> IMemberSkillMapDataAccess.GetPaged(PagedRequest request)
        {
            const string SP = "dbo.MemberSkillMap_GetPaged";
            string whereClause = string.Empty;

            if (StringHelper.IsBlank(request.SortColumn))
            {
                request.SortColumn = "SkillId";
            }

            if (StringHelper.IsBlank(request.SortOrder))
            {
                request.SortOrder = "asc";
            }

            request.SortColumn = "[MS].[" + request.SortColumn + "]";

            object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder)
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<MemberSkillMap> response = new PagedResponse<MemberSkillMap>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = CreateEntityBuilder<MemberSkillMap>().BuildEntities(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }

        PagedResponse<MemberSkillMap> IMemberSkillMapDataAccess.GetPagedByMemberId(PagedRequest request)
        {
            const string SP = "dbo.MemberSkillMap_GetPaged";
            string whereClause = string.Empty;

            if (request.Conditions.Count > 0)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();

                string column = null;
                string value = null;

                foreach (DictionaryEntry entry in request.Conditions)
                {
                    column = StringHelper.Convert(entry.Key);
                    value = StringHelper.Convert(entry.Value);

                    if (!StringHelper.IsBlank(column))
                    {
                        value = value.Replace("'", "''");

                        if (StringHelper.IsEqual(column, "MemberId"))
                        {
                            sb.Append("[MS].[MemberId]");
                            sb.Append(" = ");
                            sb.Append(value);
                        }
                    }
                }

                whereClause = sb.ToString();
            }

            if (StringHelper.IsBlank(request.SortColumn))
            {
                request.SortColumn = "SkillId";
            }

            if (StringHelper.IsBlank(request.SortOrder))
            {
                request.SortOrder = "asc";
            }

            request.SortColumn = "[MS].[" + request.SortColumn + "]";

            object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder)
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<MemberSkillMap> response = new PagedResponse<MemberSkillMap>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = CreateEntityBuilder<MemberSkillMap>().BuildEntities(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }
        string IMemberSkillMapDataAccess.GetAllMemberSkillNamesByMemberId(int memberId)
        {
            if (memberId < 1)
            {
                throw new ArgumentNullException("MemberId");
            }
            const string SP = "dbo.MemberSkillMap_GetAllMemberSkillNamesByMemberId";
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberId);
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    while (reader.Read())
                    {
                        return reader.IsDBNull(0) ? "" : reader.GetString(0);
                    }
                    return "";
                }
            }
        }

        #endregion
    }
}