﻿using System.Data;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    internal sealed class ApplicationWorkflowMapBuilder : IEntityBuilder<ApplicationWorkflowMap>
    {
        IList<ApplicationWorkflowMap> IEntityBuilder<ApplicationWorkflowMap>.BuildEntities(IDataReader reader)
        {
            List<ApplicationWorkflowMap> applicationWorkflowMaps = new List<ApplicationWorkflowMap>();

            while (reader.Read())
            {
                applicationWorkflowMaps.Add(((IEntityBuilder<ApplicationWorkflowMap>)this).BuildEntity(reader));
            }

            return (applicationWorkflowMaps.Count > 0) ? applicationWorkflowMaps : null;
        }

        ApplicationWorkflowMap IEntityBuilder<ApplicationWorkflowMap>.BuildEntity(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_APPLICATIONWORKFLOWID = 1;
            const int FLD_MEMBERID = 2;

            ApplicationWorkflowMap applicationWorkflowMap = new ApplicationWorkflowMap();

            applicationWorkflowMap.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            applicationWorkflowMap.ApplicationWorkflowId = reader.IsDBNull(FLD_APPLICATIONWORKFLOWID) ? 0 : reader.GetInt32(FLD_APPLICATIONWORKFLOWID);
            applicationWorkflowMap.MemberId = reader.IsDBNull(FLD_MEMBERID) ? 0 : reader.GetInt32(FLD_MEMBERID);

            return applicationWorkflowMap;
        }
    }
}