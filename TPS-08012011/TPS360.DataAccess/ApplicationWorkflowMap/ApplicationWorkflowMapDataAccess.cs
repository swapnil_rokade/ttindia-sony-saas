﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    internal sealed class ApplicationWorkflowMapDataAccess : BaseDataAccess, IApplicationWorkflowMapDataAccess
    {
        #region Constructors

        public ApplicationWorkflowMapDataAccess(Context context) : base(context)
        {
        }

        protected override IEntityBuilder<ApplicationWorkflowMap> CreateEntityBuilder<ApplicationWorkflowMap>()
        {
            return (new ApplicationWorkflowMapBuilder()) as IEntityBuilder<ApplicationWorkflowMap>;
        }

        #endregion

        #region  Methods

        ApplicationWorkflowMap IApplicationWorkflowMapDataAccess.Add(ApplicationWorkflowMap applicationWorkflowMap)
        {
            const string SP = "dbo.ApplicationWorkflowMap_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@ApplicationWorkflowId", DbType.Int32, applicationWorkflowMap.ApplicationWorkflowId);
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, applicationWorkflowMap.MemberId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        applicationWorkflowMap = CreateEntityBuilder<ApplicationWorkflowMap>().BuildEntity(reader);
                    }
                    else
                    {
                        applicationWorkflowMap = null;
                    }
                }

                if (applicationWorkflowMap == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Application workflow map already exists. Please specify another application workflow map.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while creating this application workflow map.");
                            }
                    }
                }

                return applicationWorkflowMap;
            }
        }

        ApplicationWorkflowMap IApplicationWorkflowMapDataAccess.Update(ApplicationWorkflowMap applicationWorkflowMap)
        {
            const string SP = "dbo.ApplicationWorkflowMap_Update";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, applicationWorkflowMap.Id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        applicationWorkflowMap = CreateEntityBuilder<ApplicationWorkflowMap>().BuildEntity(reader);
                    }
                    else
                    {
                        applicationWorkflowMap = null;
                    }
                }

                if (applicationWorkflowMap == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Application workflow map already exists. Please specify another application workflow map.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while updating this application workflow map.");
                            }
                    }
                }

                return applicationWorkflowMap;
            }
        }

        ApplicationWorkflowMap IApplicationWorkflowMapDataAccess.GetById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.ApplicationWorkflowMap_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<ApplicationWorkflowMap>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        ApplicationWorkflowMap IApplicationWorkflowMapDataAccess.GetByApplicationWorkflowIdAndMemberId(int applicationWorkflowId, int memberId)
        { 
            const string SP = "dbo.ApplicationWorkflowMap_GetApplicationWorkflowMapByApplicationWorkflowIdAndMemberId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@ApplicationWorkflowId", DbType.Int32, applicationWorkflowId);
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<ApplicationWorkflowMap>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        IList<ApplicationWorkflowMap> IApplicationWorkflowMapDataAccess.GetAllByApplicationWorkflowId(int applicationWorkflowId)
        {
            const string SP = "dbo.ApplicationWorkflowMap_GetAllApplicationWorkflowMapByApplicationWorkflowId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@ApplicationWorkflowId", DbType.Int32, applicationWorkflowId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<ApplicationWorkflowMap>().BuildEntities(reader);
                }
            }
        }

        IList<ApplicationWorkflowMap> IApplicationWorkflowMapDataAccess.GetAll()
        {
            const string SP = "dbo.ApplicationWorkflowMap_GetAll";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<ApplicationWorkflowMap>().BuildEntities(reader);
                }
            }
        }

        bool IApplicationWorkflowMapDataAccess.DeleteById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.ApplicationWorkflowMap_DeleteById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a application workflow map which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this application workflow map.");
                        }
                }
            }
        }

        bool IApplicationWorkflowMapDataAccess.DeleteByApplicationWorkflowId(int applicationWorkFlowId)
        {
            if (applicationWorkFlowId < 1)
            {
                throw new ArgumentNullException("applicationWorkFlowId");
            }

            const string SP = "dbo.ApplicationWorkflowMap_DeleteByApplicationWorkflowId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@ApplicationWorkflowId", DbType.Int32, applicationWorkFlowId);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a application workflow map which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this application workflow map.");
                        }
                }
            }
        }

        #endregion
    }
}