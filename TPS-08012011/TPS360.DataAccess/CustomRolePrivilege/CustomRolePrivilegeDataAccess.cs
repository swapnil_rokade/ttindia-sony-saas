﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    internal sealed class CustomRolePrivilegeDataAccess : BaseDataAccess, ICustomRolePrivilegeDataAccess
    {
        #region Constructors

        public CustomRolePrivilegeDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<CustomRolePrivilege> CreateEntityBuilder<CustomRolePrivilege>()
        {
            return (new CustomRolePrivilegeBuilder()) as IEntityBuilder<CustomRolePrivilege>;
        }

        #endregion

        #region  Methods

        CustomRolePrivilege ICustomRolePrivilegeDataAccess.Add(CustomRolePrivilege customRolePrivilege)
        {
            const string SP = "dbo.CustomRolePrivilege_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Privilege", DbType.Int32, customRolePrivilege.Privilege);
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, customRolePrivilege.IsRemoved);
                Database.AddInParameter(cmd, "@CustomRoleId", DbType.Int32, customRolePrivilege.CustomRoleId);
                Database.AddInParameter(cmd, "@CustomSiteMapId", DbType.Int32, customRolePrivilege.CustomSiteMapId);
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, customRolePrivilege.CreatorId);
               
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        customRolePrivilege = CreateEntityBuilder<CustomRolePrivilege>().BuildEntity(reader);
                    }
                    else
                    {
                        customRolePrivilege = null;
                    }
                }

                if (customRolePrivilege == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Custom role privilege already exists. Please specify another custom role privilege.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while creating this custom role privilege.");
                            }
                    }
                }

                return customRolePrivilege;
            }
        }

        CustomRolePrivilege ICustomRolePrivilegeDataAccess.Update(CustomRolePrivilege customRolePrivilege)
        {
            const string SP = "dbo.CustomRolePrivilege_Update";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, customRolePrivilege.Id);
                Database.AddInParameter(cmd, "@Privilege", DbType.Int32, customRolePrivilege.Privilege);
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, customRolePrivilege.IsRemoved);
                Database.AddInParameter(cmd, "@UpdatorId", DbType.Int32, customRolePrivilege.UpdatorId);
                
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        customRolePrivilege = CreateEntityBuilder<CustomRolePrivilege>().BuildEntity(reader);
                    }
                    else
                    {
                        customRolePrivilege = null;
                    }
                }

                if (customRolePrivilege == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Custom role privilege already exists. Please specify another custom role privilege.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while updating this custom role privilege.");
                            }
                    }
                }

                return customRolePrivilege;
            }
        }

        CustomRolePrivilege ICustomRolePrivilegeDataAccess.GetById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.CustomRolePrivilege_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<CustomRolePrivilege>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        IList<CustomRolePrivilege> ICustomRolePrivilegeDataAccess.GetAllByRoleId(int roleId)
        {
            const string SP = "dbo.CustomRolePrivilege_GetAllByCustomRoleId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@CustomRoleId", DbType.Int32, roleId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<CustomRolePrivilege>().BuildEntities(reader);
                }
            }
        }

        IList<CustomRolePrivilege> ICustomRolePrivilegeDataAccess.GetAll()
        {
            const string SP = "dbo.CustomRolePrivilege_GetAll";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<CustomRolePrivilege>().BuildEntities(reader);
                }
            }
        }

        bool ICustomRolePrivilegeDataAccess.DeleteById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.CustomRolePrivilege_DeleteById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a custom role privilege which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this custom role privilege.");
                        }
                }
            }
        }

        bool ICustomRolePrivilegeDataAccess.DeleteByRoleId(int roleId)
        {
            if (roleId < 1)
            {
                throw new ArgumentNullException("roleId");
            }

            const string SP = "dbo.CustomRolePrivilege_DeleteCustomRolePrivilegeByCustomRoleId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@CustomRoleId", DbType.Int32, roleId);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a custom role privilege which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this custom role privilege.");
                        }
                }
            }
        }

        void ICustomRolePrivilegeDataAccess.DeleteAll_CustomRolePrivilege()
        {
            const string SP = "dbo.CustomRolePrivilege_DeleteAll";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.ExecuteNonQuery(cmd);
            }
        }

        #endregion
    }
}