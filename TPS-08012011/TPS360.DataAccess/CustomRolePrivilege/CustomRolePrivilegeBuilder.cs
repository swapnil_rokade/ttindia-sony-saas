﻿using System;
using System.Data;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    internal sealed class CustomRolePrivilegeBuilder : IEntityBuilder<CustomRolePrivilege>
    {
        IList<CustomRolePrivilege> IEntityBuilder<CustomRolePrivilege>.BuildEntities(IDataReader reader)
        {
            List<CustomRolePrivilege> customRolePrivileges = new List<CustomRolePrivilege>();

            while (reader.Read())
            {
                customRolePrivileges.Add(((IEntityBuilder<CustomRolePrivilege>)this).BuildEntity(reader));
            }

            return (customRolePrivileges.Count > 0) ? customRolePrivileges : null;
        }

        CustomRolePrivilege IEntityBuilder<CustomRolePrivilege>.BuildEntity(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_PRIVILEGE = 1;
            const int FLD_ISREMOVED = 2;
            const int FLD_CUSTOMROLEID = 3;
            const int FLD_CUSTOMSITEMAPID = 4;
            const int FLD_CREATORID = 5;
            const int FLD_UPDATORID = 6;
            const int FLD_CREATEDATE = 7;
            const int FLD_UPDATEDATE = 8;

            CustomRolePrivilege customRolePrivilege = new CustomRolePrivilege();

            customRolePrivilege.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            customRolePrivilege.Privilege = reader.IsDBNull(FLD_PRIVILEGE) ? 0 : reader.GetInt32(FLD_PRIVILEGE);
            customRolePrivilege.IsRemoved = reader.IsDBNull(FLD_ISREMOVED) ? false : reader.GetBoolean(FLD_ISREMOVED);
            customRolePrivilege.CustomRoleId = reader.IsDBNull(FLD_CUSTOMROLEID) ? 0 : reader.GetInt32(FLD_CUSTOMROLEID);
            customRolePrivilege.CustomSiteMapId = reader.IsDBNull(FLD_CUSTOMSITEMAPID) ? 0 : reader.GetInt32(FLD_CUSTOMSITEMAPID);
            customRolePrivilege.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
            customRolePrivilege.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);
            customRolePrivilege.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
            customRolePrivilege.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);

            return customRolePrivilege;
        }
    }
}