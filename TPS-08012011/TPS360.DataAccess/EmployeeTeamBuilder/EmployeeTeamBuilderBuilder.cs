﻿using System;
using System.Data;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    internal sealed class EmployeeTeamBuilderBuilder : IEntityBuilder<EmployeeTeamBuilder>
    {
        IList<EmployeeTeamBuilder> IEntityBuilder<EmployeeTeamBuilder>.BuildEntities(IDataReader reader)
        {
            List<EmployeeTeamBuilder> employeeTeamBuilders = new List<EmployeeTeamBuilder>();

            while (reader.Read())
            {
                employeeTeamBuilders.Add(((IEntityBuilder<EmployeeTeamBuilder>)this).BuildEntity(reader));
            }

            return (employeeTeamBuilders.Count > 0) ? employeeTeamBuilders : null;
        }

        EmployeeTeamBuilder IEntityBuilder<EmployeeTeamBuilder>.BuildEntity(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_TITLE = 1;
            const int FLD_TEAMLEADERID = 2;
            const int FLD_TEAMLEADERNAME = 3;  
            const int FLD_EMPLOYEEID = 4;
            const int FLD_MEMBERSTEAMLIST = 5;  
            const int FLD_CREATORID = 6;
            const int FLD_UPDATORID = 7;
            const int FLD_CREATEDATE = 8;
            const int FLD_UPDATEDATE = 9;

            EmployeeTeamBuilder employeeTeamBuilder = new EmployeeTeamBuilder();

            employeeTeamBuilder.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            employeeTeamBuilder.Title = reader.IsDBNull(FLD_TITLE) ? string.Empty : reader.GetString(FLD_TITLE);
            employeeTeamBuilder.TeamLeader = reader.IsDBNull(FLD_TEAMLEADERID) ? 0 : reader.GetInt32(FLD_TEAMLEADERID);
            employeeTeamBuilder.TeamLeaderName= reader.IsDBNull(FLD_TEAMLEADERNAME) ? string.Empty : reader.GetString(FLD_TEAMLEADERNAME);
            employeeTeamBuilder.EmployeeId = reader.IsDBNull(FLD_EMPLOYEEID) ? string.Empty : reader.GetString(FLD_EMPLOYEEID);
            employeeTeamBuilder.TeamMemberList = reader.IsDBNull(FLD_MEMBERSTEAMLIST) ? string.Empty : reader.GetString(FLD_MEMBERSTEAMLIST);
            employeeTeamBuilder.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
            employeeTeamBuilder.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);
            employeeTeamBuilder.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
            employeeTeamBuilder.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);

            return employeeTeamBuilder;
        }
    }
}