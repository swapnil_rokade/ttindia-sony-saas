﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;
using System.Collections;
using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Helper;

namespace TPS360.DataAccess
{
    internal sealed class MemberCertificationMapDataAccess : BaseDataAccess, IMemberCertificationMapDataAccess
    {
        #region Constructors

        public MemberCertificationMapDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<MemberCertificationMap> CreateEntityBuilder<MemberCertificationMap>()
        {
            return (new MemberCertificationMapBuilder()) as IEntityBuilder<MemberCertificationMap>;
        }

        #endregion

        #region  Methods

        MemberCertificationMap IMemberCertificationMapDataAccess.Add(MemberCertificationMap memberCertificationMap)
        {
            const string SP = "dbo.MemberCertificationMap_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@CertificationName", DbType.AnsiString , memberCertificationMap.CerttificationName );
                Database.AddInParameter(cmd, "@ValidFrom", DbType.DateTime   , NullConverter.Convert( memberCertificationMap.ValidFrom));
                Database.AddInParameter(cmd, "@ValidTo", DbType.DateTime , NullConverter.Convert(memberCertificationMap.ValidTo));
                Database.AddInParameter(cmd, "@IssuingAthority", DbType.AnsiString, StringHelper.Convert(memberCertificationMap.IssuingAthority ));
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, memberCertificationMap.IsRemoved);
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberCertificationMap.MemberId);
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, memberCertificationMap.CreatorId);                

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        memberCertificationMap = CreateEntityBuilder<MemberCertificationMap>().BuildEntity(reader);
                    }
                    else
                    {
                        memberCertificationMap = null;
                    }
                }

                if (memberCertificationMap == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("MemberCertificationMap already exists. Please specify another memberCertificationMap.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while creating this memberCertificationMap.");
                            }
                    }
                }

                return memberCertificationMap;
            }
        }

        MemberCertificationMap IMemberCertificationMapDataAccess.Update(MemberCertificationMap memberCertificationMap)
        {
            const string SP = "dbo.MemberCertificationMap_Update";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, memberCertificationMap.Id);
                Database.AddInParameter(cmd, "@CertificationName", DbType.AnsiString , memberCertificationMap.CerttificationName );
                Database.AddInParameter(cmd, "@ValidFrom", DbType.DateTime, NullConverter.Convert(memberCertificationMap.ValidFrom));
                Database.AddInParameter(cmd, "@ValidTo", DbType.DateTime, NullConverter.Convert(memberCertificationMap.ValidTo));
                Database.AddInParameter(cmd, "@IssuingAthority", DbType.AnsiString, StringHelper.Convert(memberCertificationMap.IssuingAthority ));
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, memberCertificationMap.IsRemoved);
                Database.AddInParameter(cmd, "@UpdatorId", DbType.Int32, memberCertificationMap.UpdatorId);               

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        memberCertificationMap = CreateEntityBuilder<MemberCertificationMap>().BuildEntity(reader);
                    }
                    else
                    {
                        memberCertificationMap = null;
                    }
                }

                if (memberCertificationMap == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("MemberCertificationMap already exists. Please specify another memberCertificationMap.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while updating this memberCertificationMap.");
                            }
                    }
                }

                return memberCertificationMap;
            }
        }

        MemberCertificationMap IMemberCertificationMapDataAccess.GetById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.MemberCertificationMap_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<MemberCertificationMap>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }


        MemberCertificationMap IMemberCertificationMapDataAccess.GetByCertificationName (string  CertificationName)
        {
            if (CertificationName  == string .Empty )
            {
                throw new ArgumentNullException("CertificationName");
            }

            const string SP = "dbo.MemberCertificationMap_GetByCertificationName";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@CertificationName", DbType.AnsiString , CertificationName );

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<MemberCertificationMap>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }



  
        IList<MemberCertificationMap> IMemberCertificationMapDataAccess.GetAll()
        {
            const string SP = "dbo.MemberCertificationMap_GetAll";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<MemberCertificationMap>().BuildEntities(reader);
                }
            }
        }

        IList<MemberCertificationMap> IMemberCertificationMapDataAccess.GetAllByMemberId(int memberId)
        {
            if (memberId < 1)
            {
                throw new ArgumentNullException("memberId");
            }

            const string SP = "dbo.MemberCertificationMap_GetAllMemberCertificationMapByMemberId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberId);
                
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<MemberCertificationMap>().BuildEntities(reader);
                }
            }
        }

        bool IMemberCertificationMapDataAccess.DeleteById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.MemberCertificationMap_DeleteById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a memberCertificationMap which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this memberCertificationMap.");
                        }
                }
            }
        }


        bool IMemberCertificationMapDataAccess.DeleteByMemberId(int MemberId)
        {
            if (MemberId < 1)
            {
                throw new ArgumentNullException("MemberId");
            }

            const string SP = "dbo.MemberCertificationMap_DeleteMemberCertificationMapByMemberId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, MemberId);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a memberCertificationMap which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this memberCertificationMap.");
                        }
                }
            }
        }




        PagedResponse<MemberCertificationMap> IMemberCertificationMapDataAccess.GetPaged(PagedRequest request)
        {
            const string SP = "dbo.MemberCertificationMap_GetPaged";
            string whereClause = string.Empty;

            if (StringHelper.IsBlank(request.SortColumn))
            {
                request.SortColumn = "CertificationName";
            }

            if (StringHelper.IsBlank(request.SortOrder))
            {
                request.SortOrder = "DESC";
            }

            request.SortColumn = "[MC].[" + request.SortColumn + "]";

            object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder)
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<MemberCertificationMap> response = new PagedResponse<MemberCertificationMap>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = CreateEntityBuilder<MemberCertificationMap>().BuildEntities(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }

        PagedResponse<MemberCertificationMap> IMemberCertificationMapDataAccess.GetPagedByMemberId(PagedRequest request)
        {
            const string SP = "dbo.MemberCertificationMap_GetPaged";
            string whereClause = string.Empty;

            if (request.Conditions.Count > 0)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();

                string column = null;
                string value = null;

                foreach (DictionaryEntry entry in request.Conditions)
                {
                    column = StringHelper.Convert(entry.Key);
                    value = StringHelper.Convert(entry.Value);

                    if (!StringHelper.IsBlank(column))
                    {
                        value = value.Replace("'", "''");

                        if (StringHelper.IsEqual(column, "MemberId"))
                        {
                            sb.Append("[MC].[MemberId]");
                            sb.Append(" = ");
                            sb.Append(value);
                        }
                    }
                }

                whereClause = sb.ToString();
            }

            if (StringHelper.IsBlank(request.SortColumn))
            {
                request.SortColumn = "CertificationName";
            }

            if (StringHelper.IsBlank(request.SortOrder))
            {
                request.SortOrder = "ASC";
            }

            request.SortColumn = "[MC].[" + request.SortColumn + "]";

            object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder)
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<MemberCertificationMap> response = new PagedResponse<MemberCertificationMap>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = CreateEntityBuilder<MemberCertificationMap>().BuildEntities(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }



        #endregion
    }
}