﻿using System;
using System.Data;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    internal sealed class MemberGroupMapBuilder : IEntityBuilder<MemberGroupMap>
    {
        IList<MemberGroupMap> IEntityBuilder<MemberGroupMap>.BuildEntities(IDataReader reader)
        {
            List<MemberGroupMap> memberGroupMaps = new List<MemberGroupMap>();

            while (reader.Read())
            {
                memberGroupMaps.Add(((IEntityBuilder<MemberGroupMap>)this).BuildEntity(reader));
            }

            return (memberGroupMaps.Count > 0) ? memberGroupMaps : null;
        }

        MemberGroupMap IEntityBuilder<MemberGroupMap>.BuildEntity(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_MEMBERID = 1;
            const int FLD_MEMBERGROUPID = 2;
            const int FLD_ISREMOVED = 3;
            const int FLD_CREATORID = 4;
            const int FLD_UPDATORID = 5;
            const int FLD_CREATEDATE = 6;
            const int FLD_UPDATEDATE = 7;

            const int FLD_GROUPNAME = 8;
            MemberGroupMap memberGroupMap = new MemberGroupMap();

            memberGroupMap.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            memberGroupMap.MemberId = reader.IsDBNull(FLD_MEMBERID) ? 0 : reader.GetInt32(FLD_MEMBERID);
            memberGroupMap.MemberGroupId = reader.IsDBNull(FLD_MEMBERGROUPID) ? 0 : reader.GetInt32(FLD_MEMBERGROUPID);
            memberGroupMap.IsRemoved = reader.IsDBNull(FLD_ISREMOVED) ? false : reader.GetBoolean(FLD_ISREMOVED);
            memberGroupMap.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
            memberGroupMap.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);
            memberGroupMap.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
            memberGroupMap.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);
            if (reader.FieldCount > FLD_GROUPNAME)
                memberGroupMap.GroupName = reader.IsDBNull(FLD_GROUPNAME) ? string.Empty : reader.GetString(FLD_GROUPNAME);
            return memberGroupMap;
        }
    }
}