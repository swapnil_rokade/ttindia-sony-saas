﻿using System;
using System.Data;
using System.Collections.Generic;
using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    internal sealed class CandidateJoinReportBuilder : IEntityBuilder<CandidateJoiningReport>
    {       
        #region IEntityBuilder<CandidateJoiningReport> Members

        IList<CandidateJoiningReport> IEntityBuilder<CandidateJoiningReport>.BuildEntities(IDataReader reader)
        {           
            List<CandidateJoiningReport> Candidates = new List<CandidateJoiningReport>();

            while (reader.Read())
            {

                Candidates.Add(((IEntityBuilder<CandidateJoiningReport>)this).BuildEntity(reader));
            }

            return (Candidates.Count > 0) ? Candidates : null;
        }

        public IList<DynamicDictionary> BuildEntities(IDataReader reader, IList<string> CheckedList)
        {
            List<DynamicDictionary> re = new List<DynamicDictionary>();
            while (reader.Read())
            {

                re.Add(BuildEntityForReport(reader, CheckedList));

            }

            return (re.Count > 0) ? re : null;
        }

        DynamicDictionary BuildEntityForReport(IDataReader reader, IList<string> CheckedList)
        {
            DynamicDictionary newDyn = new DynamicDictionary();
            foreach (string s in CheckedList)
            {
                try
                {
                    string m = "";                   
                    m = reader[s].ToString();
                    if (s == "ResumeSource")
                    {
                        m = TPS360.Common.EnumHelper.GetDescription((TPS360.Common.Shared.ResumeSource)Convert.ToInt32(m));
                    }

                    newDyn.properties.Add(s, m);
                }
                catch
                {
                }
            }
            return newDyn;
        }

        CandidateJoiningReport IEntityBuilder<CandidateJoiningReport>.BuildEntity(IDataReader reader)
        {
            const int FLD_ID = 0;

            CandidateJoiningReport CandidateJoin = new CandidateJoiningReport();

            if (reader.FieldCount == 30)
            {
                const int FLD_JobPostingCode = 1;
                const int FLD_FIRSTNAME = 2;
                const int FLD_MIDDLENAME = 3;
                const int FLD_LASTNAME = 4;
                const int FLD_SupervisoryOrgCode = 5;
                const int FLD_SupervisoryOrgShortText = 6;
                const int FLD_SupervisoryOrgDesc = 7;
                const int FLD_ReasonForHire = 8;
                const int FLD_JobTitle = 9;
                const int FLD_NoOfOpenings = 10;

                const int FLD_FinalHiredDate = 11;
                const int FLD_OpenDate = 12;
                const int FLD_ActivationDate = 13;
                const int FLD_JobRestrictions = 14;
                const int FLD_JobFamily = 15;
                const int FLD_JobProfile = 16;
                const int FLD_City = 17;
                const int FLD_TimeType = 18;
                const int FLD_WorkerType = 19;
                const int FLD_WorkerSubType = 20;
                const int FLD_Company = 21;
                const int FLD_CostCenter = 22;
                const int FLD_Region = 23;
                const int FLD_InternationalEmployeeStatus = 24;
                const int FLD_PayGroup = 25;
                const int FLD_PayGroupGrade = 26;
                const int FLD_MaximumFTE = 27;
                const int FLD_Notes = 28;

                CandidateJoin.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
                CandidateJoin.JobPostingCode = reader.IsDBNull(FLD_JobPostingCode) ? string.Empty : reader.GetString(FLD_JobPostingCode);
                CandidateJoin.FirstName = reader.IsDBNull(FLD_FIRSTNAME) ? string.Empty : reader.GetString(FLD_FIRSTNAME);
                CandidateJoin.MiddleName = reader.IsDBNull(FLD_MIDDLENAME) ? string.Empty : reader.GetString(FLD_MIDDLENAME);
                CandidateJoin.LastName = reader.IsDBNull(FLD_LASTNAME) ? string.Empty : reader.GetString(FLD_LASTNAME);
                CandidateJoin.SupervisoryOrgCode = reader.IsDBNull(FLD_SupervisoryOrgCode) ? 0 : reader.GetInt32(FLD_SupervisoryOrgCode);
                CandidateJoin.SupervisoryOrgShortText = reader.IsDBNull(FLD_SupervisoryOrgShortText) ? string.Empty : reader.GetString(FLD_SupervisoryOrgShortText);
                CandidateJoin.SupervisoryOrgDesc = reader.IsDBNull(FLD_SupervisoryOrgDesc) ? string.Empty : reader.GetString(FLD_SupervisoryOrgDesc);
                CandidateJoin.ReasonForHire = reader.IsDBNull(FLD_ReasonForHire) ? string.Empty : reader.GetString(FLD_ReasonForHire);
                CandidateJoin.JobTitle = reader.IsDBNull(FLD_JobTitle) ? string.Empty : reader.GetString(FLD_JobTitle);
                CandidateJoin.NoOfOpenings = reader.IsDBNull(FLD_NoOfOpenings) ? 0 : reader.GetInt32(FLD_NoOfOpenings);
                CandidateJoin.FinalHiredDate = reader.IsDBNull(FLD_FinalHiredDate) ? DateTime.MinValue : reader.GetDateTime(FLD_FinalHiredDate);
                CandidateJoin.OpenDate = reader.IsDBNull(FLD_OpenDate) ? DateTime.MinValue : reader.GetDateTime(FLD_OpenDate);
                CandidateJoin.ActivationDate = reader.IsDBNull(FLD_ActivationDate) ? DateTime.MinValue : reader.GetDateTime(FLD_ActivationDate);
                CandidateJoin.JobRestrictions = reader.IsDBNull(FLD_JobRestrictions) ? string.Empty : reader.GetString(FLD_JobRestrictions);
                CandidateJoin.JobFamily = reader.IsDBNull(FLD_JobFamily) ? string.Empty : reader.GetString(FLD_JobFamily);
                CandidateJoin.JobProfile = reader.IsDBNull(FLD_JobProfile) ? string.Empty : reader.GetString(FLD_JobProfile);
                CandidateJoin.City = reader.IsDBNull(FLD_City) ? string.Empty : reader.GetString(FLD_City);
                CandidateJoin.TimeType = reader.IsDBNull(FLD_TimeType) ? string.Empty : reader.GetString(FLD_TimeType);
                CandidateJoin.WorkerType = reader.IsDBNull(FLD_WorkerType) ? string.Empty : reader.GetString(FLD_WorkerType);
                CandidateJoin.WorkerSubType = reader.IsDBNull(FLD_WorkerSubType) ? string.Empty : reader.GetString(FLD_WorkerSubType);
                CandidateJoin.Company = reader.IsDBNull(FLD_Company) ? string.Empty : reader.GetString(FLD_Company);
                CandidateJoin.CostCenter = reader.IsDBNull(FLD_CostCenter) ? string.Empty : reader.GetString(FLD_CostCenter);
                CandidateJoin.Region = reader.IsDBNull(FLD_Region) ? string.Empty : reader.GetString(FLD_Region);
                CandidateJoin.InternationalEmployeeStatus = reader.IsDBNull(FLD_InternationalEmployeeStatus) ? string.Empty : reader.GetString(FLD_InternationalEmployeeStatus);
                CandidateJoin.PayGroup = reader.IsDBNull(FLD_PayGroup) ? string.Empty : reader.GetString(FLD_PayGroup);
                CandidateJoin.PayGroupGrade = reader.IsDBNull(FLD_PayGroupGrade) ? string.Empty : reader.GetString(FLD_PayGroupGrade);
                CandidateJoin.MaximumFTE = reader.IsDBNull(FLD_MaximumFTE) ? 0 : reader.GetInt32(FLD_MaximumFTE);
                CandidateJoin.Notes = reader.IsDBNull(FLD_Notes) ? string.Empty : reader.GetString(FLD_Notes);
            }

            if (reader.FieldCount == 49)
            {
                const int FLD_DOJ = 1;
                const int FLD_DOC = 2;
                const int FLD_GID = 3;
                const int FLD_Position = 4;
                const int FLD_FirstName = 5;
                const int FLD_MiddleName = 6;
                const int FLD_LastName = 7;
                const int FLD_PrimaryEmail = 8;
                const int FLD_DateOfBirth = 9;
                const int FLD_MaritalStatus = 10;
                const int FLD_Gender = 11;
                const int FLD_NationalIDType = 12;
                const int FLD_NationalID = 13;
                const int FLD_PhoneNumber = 14;
                const int FLD_PhoneDevice = 15;
                const int FLD_PhoneType = 16;
                const int FLD_Address = 17;
                const int FLD_VendorName = 18;
                const int FLD_CITY = 19;
                const int FLD_ReasonForHIRE = 20;
                const int FLD_EmployeeType = 21;
                const int FLD_SubtypeofCW = 22;
                const int FLD_IndividualContributor = 23;
                const int FLD_JobFamilY = 24;
                const int FLD_JobProfiles = 25;
                const int FLD_JobProfileName = 26;
                const int FLD_SuperVisoryOrgCode = 27;
                const int FLD_SuperVisoryOrgShortText = 28;
                const int FLD_SuperVisoryOrgDesc = 29;

                const int FLD_CostCenteR = 30;
                const int FLD_Designation = 31;
                const int FLD_GlobalGrade = 32;
                const int FLD_Currency = 33;
                const int FLD_Freequency = 34;
                const int FLD_CompensationPlan = 35;
                const int FLD_OfferHead1 = 36;
                const int FLD_OfferHead2 = 37;
                const int FLD_OfferHead3 = 38;
                const int FLD_OfferHead4 = 39;
                const int FLD_OfferHead5 = 40;
                const int FLD_OfferHead6 = 41;
                const int FLD_OfferHead7 = 42;
                const int FLD_ProjectName = 43;
                const int FLD_Division = 44;
                const int FLD_ReportMangGID = 45;
                const int FLD_ManagerName = 46;
                const int FLD_OrgUnitHead = 47;

                CandidateJoin.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
                CandidateJoin.DOJ = reader.IsDBNull(FLD_DOJ) ? DateTime.MinValue : reader.GetDateTime(FLD_DOJ);
                CandidateJoin.DOC = reader.IsDBNull(FLD_DOC) ? string.Empty : reader.GetString(FLD_DOC);
                CandidateJoin.GID = reader.IsDBNull(FLD_GID) ? string.Empty : reader.GetString(FLD_GID);
                CandidateJoin.Position = reader.IsDBNull(FLD_Position) ? string.Empty : reader.GetString(FLD_Position);
                CandidateJoin.FirstName = reader.IsDBNull(FLD_FirstName) ? string.Empty : reader.GetString(FLD_FirstName);
                CandidateJoin.MiddleName = reader.IsDBNull(FLD_MiddleName) ? string.Empty : reader.GetString(FLD_MiddleName);
                CandidateJoin.LastName = reader.IsDBNull(FLD_LastName) ? string.Empty : reader.GetString(FLD_LastName);
                CandidateJoin.PrimaryEmail = reader.IsDBNull(FLD_PrimaryEmail) ? string.Empty : reader.GetString(FLD_PrimaryEmail);
                CandidateJoin.DateOfBirth = reader.IsDBNull(FLD_DateOfBirth) ? DateTime.MinValue : reader.GetDateTime(FLD_DateOfBirth);
                CandidateJoin.MaritalStatus = reader.IsDBNull(FLD_MaritalStatus) ? string.Empty : reader.GetString(FLD_MaritalStatus);
                CandidateJoin.Gender = reader.IsDBNull(FLD_Gender) ? string.Empty : reader.GetString(FLD_Gender);
                CandidateJoin.NationalIDType = reader.IsDBNull(FLD_NationalIDType) ? string.Empty : reader.GetString(FLD_NationalIDType);
                CandidateJoin.NationalID = reader.IsDBNull(FLD_NationalID) ? string.Empty : reader.GetString(FLD_NationalID);
                CandidateJoin.PhoneNumber = reader.IsDBNull(FLD_PhoneNumber) ? string.Empty : reader.GetString(FLD_PhoneNumber);
                CandidateJoin.PhoneDevice = reader.IsDBNull(FLD_PhoneDevice) ? string.Empty : reader.GetString(FLD_PhoneDevice);
                CandidateJoin.PhoneType = reader.IsDBNull(FLD_PhoneType) ? string.Empty : reader.GetString(FLD_PhoneType);
                CandidateJoin.Address = reader.IsDBNull(FLD_Address) ? string.Empty : reader.GetString(FLD_Address);

                CandidateJoin.VendorName = reader.IsDBNull(FLD_VendorName) ? string.Empty : reader.GetString(FLD_VendorName);
                CandidateJoin.City = reader.IsDBNull(FLD_CITY) ? string.Empty : reader.GetString(FLD_CITY);
                CandidateJoin.ReasonForHire = reader.IsDBNull(FLD_ReasonForHIRE) ? string.Empty : reader.GetString(FLD_ReasonForHIRE);
                CandidateJoin.EmployeeType = reader.IsDBNull(FLD_EmployeeType) ? string.Empty : reader.GetString(FLD_EmployeeType);
                CandidateJoin.SubtypeofCW = reader.IsDBNull(FLD_SubtypeofCW) ? string.Empty : reader.GetString(FLD_SubtypeofCW);
                CandidateJoin.IndividualContributor = reader.IsDBNull(FLD_IndividualContributor) ? string.Empty : reader.GetString(FLD_IndividualContributor);
                CandidateJoin.JobFamily = reader.IsDBNull(FLD_JobFamilY) ? string.Empty : reader.GetString(FLD_JobFamilY);
                CandidateJoin.JobProfile = reader.IsDBNull(FLD_JobProfiles) ? string.Empty : reader.GetString(FLD_JobProfiles);
                CandidateJoin.jobProfileName = reader.IsDBNull(FLD_JobProfileName) ? string.Empty : reader.GetString(FLD_JobProfileName);
                CandidateJoin.SupervisoryOrgCode = reader.IsDBNull(FLD_SuperVisoryOrgCode) ? 0 : reader.GetInt32(FLD_SuperVisoryOrgCode);
                CandidateJoin.SupervisoryOrgShortText = reader.IsDBNull(FLD_SuperVisoryOrgShortText) ? string.Empty : reader.GetString(FLD_SuperVisoryOrgShortText);
                CandidateJoin.SupervisoryOrgDesc = reader.IsDBNull(FLD_SuperVisoryOrgDesc) ? string.Empty : reader.GetString(FLD_SuperVisoryOrgDesc);

                CandidateJoin.CostCenter = reader.IsDBNull(FLD_CostCenteR) ? string.Empty : reader.GetString(FLD_CostCenteR);
                CandidateJoin.Designation = reader.IsDBNull(FLD_Designation) ? string.Empty : reader.GetString(FLD_Designation);
                CandidateJoin.GlobalGrade = reader.IsDBNull(FLD_GlobalGrade) ? string.Empty : reader.GetString(FLD_GlobalGrade);
                CandidateJoin.Currency = reader.IsDBNull(FLD_Currency) ? string.Empty : reader.GetString(FLD_Currency);
                CandidateJoin.Frequency = reader.IsDBNull(FLD_Freequency) ? string.Empty : reader.GetString(FLD_Freequency);
                CandidateJoin.CompensationPlan = reader.IsDBNull(FLD_CompensationPlan) ? string.Empty : reader.GetString(FLD_CompensationPlan);
                CandidateJoin.OfferHead1 = reader.IsDBNull(FLD_OfferHead1) ? 0 : reader.GetDecimal(FLD_OfferHead1);
                CandidateJoin.OfferHead2 = reader.IsDBNull(FLD_OfferHead2) ? 0 : reader.GetDecimal(FLD_OfferHead2);
                CandidateJoin.OfferHead3 = reader.IsDBNull(FLD_OfferHead3) ? 0 : reader.GetDecimal(FLD_OfferHead3);
                CandidateJoin.OfferHead4 = reader.IsDBNull(FLD_OfferHead4) ? 0 : reader.GetDecimal(FLD_OfferHead4);
                CandidateJoin.OfferHead5 = reader.IsDBNull(FLD_OfferHead5) ? 0 : reader.GetDecimal(FLD_OfferHead5);
                CandidateJoin.OfferHead6 = reader.IsDBNull(FLD_OfferHead6) ? 0 : reader.GetDecimal(FLD_OfferHead6);
                CandidateJoin.OfferHead7 = reader.IsDBNull(FLD_OfferHead7) ? 0 : reader.GetDecimal(FLD_OfferHead7);
                CandidateJoin.ProjectName = reader.IsDBNull(FLD_ProjectName) ? string.Empty : reader.GetString(FLD_ProjectName);
                CandidateJoin.Division = reader.IsDBNull(FLD_Division) ? string.Empty : reader.GetString(FLD_Division);
                CandidateJoin.ReportMangGID = reader.IsDBNull(FLD_ReportMangGID) ? string.Empty : reader.GetString(FLD_ReportMangGID);
                CandidateJoin.ManagerName = reader.IsDBNull(FLD_ManagerName) ? string.Empty : reader.GetString(FLD_ManagerName);
                CandidateJoin.OrgUnitHead = reader.IsDBNull(FLD_OrgUnitHead) ? string.Empty : reader.GetString(FLD_OrgUnitHead);
            }

            return CandidateJoin;
        }

        #endregion
    }
}
