﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Data;
using TPS360.Common.Shared;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using System.Collections;

namespace TPS360.DataAccess
{
    internal sealed class CandidateJoinReportDataAccess : BaseDataAccess, ICandidateJoiningReportDataAccess
    {
        public CandidateJoinReportDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<CandidateJoiningReport> CreateEntityBuilder<CandidateJoiningReport>()
        {
            return (new CandidateJoinReportBuilder()) as IEntityBuilder<CandidateJoiningReport>;
        }

        private StringBuilder getWhereClauseForSearch(PagedRequest request, DateTime addedFrom, DateTime addedTo, string SuperVisoryCode)
        {
            StringBuilder whereClause = new StringBuilder();
            if (addedFrom != DateTime.MinValue)
            {
                if (!StringHelper.IsBlank(whereClause))
                {
                    whereClause.Append(" AND ");
                }           
                whereClause.Append("[C].[CreateDate]");
                whereClause.Append(" >= ");                
                whereClause.Append("Convert(DateTime,'" + Convert.ToDateTime(addedFrom).ToString("dd/MM/yyyy") + "',103) ");
            }
            if (addedTo != null && addedTo != DateTime.MinValue)
            {
                if (!StringHelper.IsBlank(whereClause))
                {
                    whereClause.Append(" AND ");
                }              
                whereClause.Append("[C].[CreateDate]");
                whereClause.Append(" < ");              
                whereClause.Append("Convert(DateTime,'" + Convert.ToDateTime(addedTo).AddDays(1).ToString("dd/MM/yyyy") + "',103) ");
            }

            if (!string.IsNullOrEmpty(SuperVisoryCode))
            {
                if (!StringHelper.IsBlank(whereClause))
                {
                    whereClause.Append(" AND ");
                }
                whereClause.Append("[Org].[OrgUnitCode]");
                whereClause.Append(" LIKE ");
                whereClause.Append("'%" + SuperVisoryCode + "%'");
            }
            
            return whereClause;
        }

        PagedResponse<CandidateJoiningReport> ICandidateJoiningReportDataAccess.GetCandidateJoiningReport1(PagedRequest request, DateTime addedFrom, DateTime addedTo, string SuperVisoryCode)
        {
            const string SP = "dbo.CandidateJoining_GetPagedReport1";
            StringBuilder whereClause = new StringBuilder();

            whereClause = getWhereClauseForSearch(request, addedFrom, addedTo, SuperVisoryCode);
            PagedResponse<CandidateJoiningReport> response = new PagedResponse<CandidateJoiningReport>();

           
            if (StringHelper.IsBlank(request.SortColumn))
            {
                request.SortColumn = "[C].[FirstName]";
            }
            if (StringHelper.IsBlank(request.SortOrder))
            {
                request.SortOrder = "ASC";

            }
            request.SortColumn = request.SortColumn;
            object[] paramValues = new object[] {	request.PageIndex,
													            request.RowPerPage,
													            StringHelper.Convert(whereClause),
													            StringHelper.Convert(request.SortColumn),
                                                                StringHelper.Convert(request.SortOrder),
                                                                String.Empty
            };

			using (DbCommand cmd = Database.GetStoredProcCommand(SP,paramValues))
            {             
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = CreateEntityBuilder<CandidateJoiningReport>().BuildEntities(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }
                return response;
            }
        }

        private StringBuilder getWhereClauseForSearchReport(PagedRequest request, DateTime addedFrom, DateTime addedTo, string SuperVisoryCode)
        {
            StringBuilder whereClause = new StringBuilder();
            if (addedFrom != DateTime.MinValue)
            {
                if (!StringHelper.IsBlank(whereClause))
                {
                    whereClause.Append(" AND ");
                }
                whereClause.Append("[C].[CreateDate]");
                whereClause.Append(" >= ");
                whereClause.Append("Convert(DateTime,'" + Convert.ToDateTime(addedFrom).ToString("dd/MM/yyyy") + "',103) ");
            }
            if (addedTo != null && addedTo != DateTime.MinValue)
            {
                if (!StringHelper.IsBlank(whereClause))
                {
                    whereClause.Append(" AND ");
                }
                whereClause.Append("[C].[CreateDate]");
                whereClause.Append(" < ");
                whereClause.Append("Convert(DateTime,'" + Convert.ToDateTime(addedTo).AddDays(1).ToString("dd/MM/yyyy") + "',103) ");
            }

            if (!string.IsNullOrEmpty(SuperVisoryCode))
            {
                if (!StringHelper.IsBlank(whereClause))
                {
                    whereClause.Append(" AND ");
                }
                whereClause.Append("[Org].[OrgUnitCode]");
                whereClause.Append(" LIKE ");
                whereClause.Append("'%" + SuperVisoryCode + "%'");
            }

            return whereClause;
        }

        PagedResponse<CandidateJoiningReport> ICandidateJoiningReportDataAccess.GetCandidateJoiningReport2(PagedRequest request, DateTime addedFrom, DateTime addedTo, string SuperVisoryCode)
        {
            const string SP = "dbo.CandidateJoining_GetPagedReport2";
            StringBuilder whereClause = new StringBuilder();

            whereClause = getWhereClauseForSearchReport(request, addedFrom, addedTo, SuperVisoryCode);
            PagedResponse<CandidateJoiningReport> response = new PagedResponse<CandidateJoiningReport>();


            if (StringHelper.IsBlank(request.SortColumn))
            {
                request.SortColumn = "[C].[FirstName]";
            }
            if (StringHelper.IsBlank(request.SortOrder))
            {
                request.SortOrder = "ASC";

            }
            request.SortColumn = request.SortColumn;
            object[] paramValues = new object[] {	request.PageIndex,
													            request.RowPerPage,
													            StringHelper.Convert(whereClause),
													            StringHelper.Convert(request.SortColumn),
                                                                StringHelper.Convert(request.SortOrder),
                                                                String.Empty
            };

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = CreateEntityBuilder<CandidateJoiningReport>().BuildEntities(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }
                return response;
            }
        }
    }
}
