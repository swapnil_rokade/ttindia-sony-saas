﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using Microsoft.Practices.EnterpriseLibrary.Data;
using TPS360.Common.BusinessEntities;
using TPS360.Common;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    internal sealed class DuplicateMemberBuilder : IEntityBuilder<DuplicateMember>
    {
        IList<DuplicateMember> IEntityBuilder<DuplicateMember>.BuildEntities(IDataReader reader)
        {
            List<DuplicateMember> objDuplicateMember = new List<DuplicateMember>();

            while (reader.Read())
            {
                objDuplicateMember.Add(((IEntityBuilder<DuplicateMember>)this).BuildEntity(reader));
            }

            return (objDuplicateMember.Count > 0) ? objDuplicateMember : null;
        }

        DuplicateMember IEntityBuilder<DuplicateMember>.BuildEntity(IDataReader reader)
        {
            const int FLD_MEMBERCODE = 0;
            const int FLD_FIRSTNAME = 1;
            const int FLD_MIDDLENAME = 2;
            const int FLD_LASTNAME = 3;
            const int FLD_DATEOFBIRTH = 4;
            const int FLD_PRIMARYEMAIL = 5;
            const int FLD_SSN = 6;
            const int FLD_PERMANENTADDRESSLINE1 = 7;
            const int FLD_PERMANENTADDRESSLINE2 = 8;
            const int FLD_PERMANENTCITY = 9;
            const int FLD_PERMANENTZIP = 10;
            const int FLD_STATENAME = 11;
            const int FLD_COUNTRYNAME = 12;

            DuplicateMember objDuplicateMember = new DuplicateMember();

            //objDuplicateMember.SSN = string.IsNullOrEmpty(Convert.ToString(reader[6])) ? string.Empty : Convert.ToString(reader[6]);
            //objDuplicateMember.StateName = string.IsNullOrEmpty(Convert.ToString(reader[11])) ? string.Empty : Convert.ToString(reader[11]);
            //objDuplicateMember.CountryName = string.IsNullOrEmpty(Convert.ToString(reader[12])) ? string.Empty : Convert.ToString(reader[12]);

            objDuplicateMember.Id = reader.IsDBNull(FLD_MEMBERCODE) ? 0 : reader.GetInt32(FLD_MEMBERCODE);

            objDuplicateMember.FirstName = reader.IsDBNull(FLD_FIRSTNAME) ? string.Empty : reader.GetString(FLD_FIRSTNAME);

            objDuplicateMember.MiddleName = reader.IsDBNull(FLD_MIDDLENAME) ? string.Empty : reader.GetString(FLD_MIDDLENAME);

            objDuplicateMember.LastName = reader.IsDBNull(FLD_LASTNAME) ? string.Empty : reader.GetString(FLD_LASTNAME);

            objDuplicateMember.DateOfBirth = reader.IsDBNull(FLD_DATEOFBIRTH) ? DateTime.MinValue : reader.GetDateTime(FLD_DATEOFBIRTH);

            objDuplicateMember.PrimaryEmail = reader.IsDBNull(FLD_PRIMARYEMAIL) ? string.Empty : reader.GetString(FLD_PRIMARYEMAIL);

            objDuplicateMember.SSN = reader.IsDBNull(FLD_SSN) ? string.Empty : reader.GetString(FLD_SSN);

            objDuplicateMember.PermanentAddressLine1 = reader.IsDBNull(FLD_PERMANENTADDRESSLINE1) ? string.Empty : reader.GetString(FLD_PERMANENTADDRESSLINE1);

            objDuplicateMember.PermanentAddressLine2 = reader.IsDBNull(FLD_PERMANENTADDRESSLINE2) ? string.Empty : reader.GetString(FLD_PERMANENTADDRESSLINE2);

            objDuplicateMember.PermanentCity = reader.IsDBNull(FLD_PERMANENTCITY) ? string.Empty : reader.GetString(FLD_PERMANENTCITY);

            objDuplicateMember.PermanentZip = reader.IsDBNull(FLD_PERMANENTZIP) ? string.Empty : reader.GetString(FLD_PERMANENTZIP);

            objDuplicateMember.StateName = reader.IsDBNull(FLD_STATENAME) ? string.Empty : reader.GetString(FLD_STATENAME);

            objDuplicateMember.CountryName = reader.IsDBNull(FLD_COUNTRYNAME) ? string.Empty : reader.GetString(FLD_COUNTRYNAME);

            return objDuplicateMember;
        }
    }
}