﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    internal sealed class MemberGroupManagerDataAccess : BaseDataAccess, IMemberGroupManagerDataAccess
    {
        #region Constructors

        public MemberGroupManagerDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<MemberGroupManager> CreateEntityBuilder<MemberGroupManager>()
        {
            return (new MemberGroupManagerBuilder()) as IEntityBuilder<MemberGroupManager>;
        }

        #endregion

        #region  Methods

        MemberGroupManager IMemberGroupManagerDataAccess.Add(MemberGroupManager memberGroupManager)
        {
            const string SP = "dbo.MemberGroupManager_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@MemberGroupId", DbType.Int32, memberGroupManager.MemberGroupId);
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberGroupManager.MemberId);
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, memberGroupManager.IsRemoved);
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, memberGroupManager.CreatorId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        memberGroupManager = CreateEntityBuilder<MemberGroupManager>().BuildEntity(reader);
                    }
                    else
                    {
                        memberGroupManager = null;
                    }
                }

                if (memberGroupManager == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Member group manager already exists. Please specify another member group manager.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while creating this member group manager.");
                            }
                    }
                }

                return memberGroupManager;
            }
        }
              
        MemberGroupManager IMemberGroupManagerDataAccess.GetById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.MemberGroupManager_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<MemberGroupManager>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        IList<MemberGroupManager> IMemberGroupManagerDataAccess.GetAllByMemberId(int memberId)
        {
            if (memberId < 1)
            {
                throw new ArgumentNullException("memberId");
            }
            
            const string SP = "dbo.MemberGroupManager_GetAllMemberGroupManagerByMemberId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberId);
                
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<MemberGroupManager>().BuildEntities(reader);
                }
            }
        }

        IList<MemberGroupManager> IMemberGroupManagerDataAccess.GetAll()
        {
            const string SP = "dbo.MemberGroupManager_GetAll";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<MemberGroupManager>().BuildEntities(reader);
                }
            }
        }

        int IMemberGroupManagerDataAccess.GetTotalMemberByMemberGroupId(int memberGroupId)
        {
            if (memberGroupId < 1)
            {
                throw new ArgumentNullException("memberGroupId");
            }

            const string SP = "GetTotalMemberByMemberGroupId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberGroupId", DbType.Int32, memberGroupId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return reader.GetInt32(0);
                    }
                }
            }

            return 0;
        }

        bool IMemberGroupManagerDataAccess.DeleteById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.MemberGroupManager_DeleteById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a member group manager which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this member group manager.");
                        }
                }
            }
        }

        bool IMemberGroupManagerDataAccess.DeleteByMemberGroupIdAndMemberId(int memberGroupId, int memberId)
        {
            if (memberGroupId < 1)
            {
                throw new ArgumentNullException("memberGroupId");
            }

            if (memberId < 1)
            {
                throw new ArgumentNullException("memberId");
            }

            const string SP = "dbo.MemberGroupManager_DeleteByMemberGroupIdAndMemberId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@MemberGroupId", DbType.Int32, memberGroupId);
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberId);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a member group manager which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this member group manager.");
                        }
                }
            }
        }

        #endregion
    }
}