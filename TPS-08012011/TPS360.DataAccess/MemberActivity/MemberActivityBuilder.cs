﻿using System;
using System.Data;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    internal sealed class MemberActivityBuilder : IEntityBuilder<MemberActivity>
    {
        IList<MemberActivity> IEntityBuilder<MemberActivity>.BuildEntities(IDataReader reader)
        {
            List<MemberActivity> memberActivitys = new List<MemberActivity>();

            while (reader.Read())
            {
                memberActivitys.Add(((IEntityBuilder<MemberActivity>)this).BuildEntity(reader));
            }

            return (memberActivitys.Count > 0) ? memberActivitys : null;
        }

        MemberActivity IEntityBuilder<MemberActivity>.BuildEntity(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_ACTIVITYON = 1;
            const int FLD_ACTIVITYONOBJECTID = 2;
            const int FLD_ISREMOVED = 3;
            const int FLD_MEMBERACTIVITYTYPEID = 4;
            const int FLD_CREATORID = 5;
            const int FLD_UPDATORID = 6;
            const int FLD_CREATEDATE = 7;
            const int FLD_UPDATEDATE = 8;

            MemberActivity memberActivity = new MemberActivity();

            memberActivity.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            memberActivity.ActivityOn = reader.IsDBNull(FLD_ACTIVITYON) ? 0 : reader.GetInt32(FLD_ACTIVITYON);
            memberActivity.ActivityOnObjectId = reader.IsDBNull(FLD_ACTIVITYONOBJECTID) ? 0 : reader.GetInt32(FLD_ACTIVITYONOBJECTID);
            memberActivity.IsRemoved = reader.IsDBNull(FLD_ISREMOVED) ? false : reader.GetBoolean(FLD_ISREMOVED);
            memberActivity.MemberActivityTypeId = reader.IsDBNull(FLD_MEMBERACTIVITYTYPEID) ? 0 : reader.GetInt32(FLD_MEMBERACTIVITYTYPEID);
            memberActivity.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
            memberActivity.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);
            memberActivity.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
            memberActivity.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);

            return memberActivity;
        }
    }
}