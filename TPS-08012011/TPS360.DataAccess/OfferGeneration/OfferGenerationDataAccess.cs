﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: GenericLookupDataAccess.cs
    Description:  
    Created By:  
    Created On:  
    Modification Log:
    --------------------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
 *  1.0                 5/Dec/2015          Prasanth Kumar G    Introduced Function InterviewAssessment_GetBy_InterviewId_InterviewerEmail
    ---------------------------------------------------------------------------------------------------------------------------------------------
    
*/
//-----------------------------------------Rishi Code Start-------------------------------------//
using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;
using Microsoft.Practices.EnterpriseLibrary.Data;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Helper;

namespace TPS360.DataAccess
{
    internal sealed class OfferGenerationDataAccess : BaseDataAccess, IOfferGenerationDataAccess
    {
        #region Constructors

        public OfferGenerationDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<OfferGeneration> CreateEntityBuilder<OfferGeneration>()
        {
            return (new OfferGenerationBuilder()) as IEntityBuilder<OfferGeneration>;
        }

        #endregion

        #region  Methods

        IList<TPS360.Common.BusinessEntities.OfferGeneration> IOfferGenerationDataAccess.GetAllById(int jobPostingId , int queryResult)
        {
            const string SP = "dbo.GradeDemandType_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@JobPostingId", DbType.Int32, Convert.ToInt32(jobPostingId));
                Database.AddInParameter(cmd, "@QueryResult", DbType.Int32, Convert.ToInt32(queryResult));
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<TPS360.Common.BusinessEntities.OfferGeneration>().BuildEntities(reader);
                }
            }
        }

        IList<TPS360.Common.BusinessEntities.OfferGeneration> IOfferGenerationDataAccess.GetOfferedSalaryById(int memberHiringId)
        {
            const string SP = "dbo.OfferedSalaryTemplate_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberHiringId", DbType.Int32, Convert.ToInt32(memberHiringId));
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<TPS360.Common.BusinessEntities.OfferGeneration>().BuildEntities(reader);
                }
            }
        }

        TPS360.Common.BusinessEntities.OfferGeneration IOfferGenerationDataAccess.Add(int JobPostingId, int memberHiringId, string gradeValue, string demandType, string headValue, double formulaValue)
        {

            const string SP = "dbo.OfferedSalaryTemplate_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@JobPostingId", DbType.Int32, Convert.ToInt32(JobPostingId));
                Database.AddInParameter(cmd, "@MemberHiringId", DbType.Int32, Convert.ToInt32(memberHiringId));
                Database.AddInParameter(cmd, "@Grade", DbType.AnsiString, StringHelper.Convert(gradeValue));
                Database.AddInParameter(cmd, "@DemandType", DbType.AnsiString, StringHelper.Convert(demandType));
                Database.AddInParameter(cmd, "@Heads", DbType.AnsiString, StringHelper.Convert(headValue));
                Database.AddInParameter(cmd, "@Formula", DbType.Double, Convert.ToDouble(formulaValue));

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<TPS360.Common.BusinessEntities.OfferGeneration>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        //**************************Suraj Adsule 20160824 For Permanent PDF Generation**************************//
        OfferGeneration IOfferGenerationDataAccess.GetCandidateDetail_UsingMemberHiringId(int memberHiringId, int type)
        {
            const string SP = "dbo.GetCandidateDetail_UsingMemberHiringId";

            OfferGeneration offGeneration = new OfferGeneration();
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberHiringId", DbType.Int32, Convert.ToInt32(memberHiringId));
                Database.AddInParameter(cmd, "@type", DbType.Int32, Convert.ToInt32(type));
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<TPS360.Common.BusinessEntities.OfferGeneration>()).BuildEntity(reader);
                        //offGeneration = CreateEntityBuilder<OfferGeneration>().BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                        //offGeneration = null;
                    }
                }
            }

            return offGeneration;
        }
        //**************************Suraj Adsule 20160824 For Permanent PDF Generation**************************//

        #endregion
    }
}
//-----------------------------------------Rishi Code End-------------------------------------//