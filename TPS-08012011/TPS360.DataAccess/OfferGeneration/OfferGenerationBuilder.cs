﻿//--------------------------------Rishi Code Start-------------------------------------------------//
using System;
using System.Data;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    internal sealed class OfferGenerationBuilder : IEntityBuilder<OfferGeneration>
    {
        IList<OfferGeneration> IEntityBuilder<OfferGeneration>.BuildEntities(IDataReader reader)
        {
            List<OfferGeneration> offerGeneration = new List<OfferGeneration>();

            while (reader.Read())
            {
                offerGeneration.Add(((IEntityBuilder<OfferGeneration>)this).BuildEntity(reader));
            }

            return (offerGeneration.Count > 0) ? offerGeneration : null;
        }

        OfferGeneration IEntityBuilder<OfferGeneration>.BuildEntity(IDataReader reader)
        {
            const int FLD_DEMANDTYPE = 0;
            const int FLD_JOBPOSTINGID = 1;
            const int FLD_MEMBERHIRINGID = 2;
            const int FLD_GRADE = 3;
            const int FLD_HEADS = 4;
            const int FLD_VALUE = 5;

            OfferGeneration offerGeneration = new OfferGeneration();

            if (reader.FieldCount == 1)
            {
                offerGeneration.DemandType = reader.IsDBNull(FLD_DEMANDTYPE) ? string.Empty : reader.GetString(FLD_DEMANDTYPE);
            }
            else if (reader.FieldCount == 6)
            {
                offerGeneration.DemandType = reader.IsDBNull(FLD_DEMANDTYPE) ? string.Empty : reader.GetString(FLD_DEMANDTYPE);
                offerGeneration.Heads = reader.IsDBNull(FLD_HEADS) ? string.Empty : reader.GetString(FLD_HEADS);
                offerGeneration.Grade = reader.IsDBNull(FLD_GRADE) ? string.Empty : reader.GetString(FLD_GRADE);
                offerGeneration.JobPostingId = reader.IsDBNull(FLD_JOBPOSTINGID) ? 0 : Convert.ToInt32(reader.GetValue(FLD_JOBPOSTINGID));
                offerGeneration.MemberHiringId = reader.IsDBNull(FLD_MEMBERHIRINGID) ? 0 : Convert.ToInt32(reader.GetValue(FLD_MEMBERHIRINGID));
                offerGeneration.Value = reader.IsDBNull(FLD_VALUE) ? 0 : Convert.ToDouble(reader.GetValue(FLD_VALUE));
            }
            //**************************Suraj Adsule 20160824 For Permanent PDF Generation**************************//
            else if (reader.FieldCount == 21)
            {
                const int FLD_FirstName = 0;
                const int FLD_MiddleName = 1;
                const int FLD_LastName = 2;
                const int FLD_PermanentAddressLine1 = 3;
                const int FLD_PermanentAddressLine2 = 4;
                const int FLD_PermanentCity = 5;
                const int FLD_PermanentState = 6;
                const int FLD_PermanentZip = 7;
                const int FLD_CurrentPosition = 8;
                const int FLD_Grade = 9;
                const int FLD_Basic = 10;
                const int FLD_Conveyance = 11;
                const int FLD_CompensationPlan = 12;
                const int FLD_HouseRent = 13;
                const int FLD_ProvidentFund = 14;
                const int FLD_SpecialAllowance = 15;
                const int FLD_VariablePay = 16;
                const int FLD_DateOfJoining = 17;
                const int FLD_FilePath = 18;
                const int FLD_DateOfOffered = 19;
                const int FLD_OfferedSalary = 20;
            
                offerGeneration.FirstName = reader.IsDBNull(FLD_FirstName) ? string.Empty : reader.GetString(FLD_FirstName);
                offerGeneration.MiddleName = reader.IsDBNull(FLD_MiddleName) ? string.Empty : reader.GetString(FLD_MiddleName);
                offerGeneration.LastName = reader.IsDBNull(FLD_LastName) ? string.Empty : reader.GetString(FLD_LastName);
                offerGeneration.PermanentAddressLine1 = reader.IsDBNull(FLD_PermanentAddressLine1) ? string.Empty : reader.GetString(FLD_PermanentAddressLine1);
                offerGeneration.PermanentAddressLine2 = reader.IsDBNull(FLD_PermanentAddressLine2) ? string.Empty : reader.GetString(FLD_PermanentAddressLine2);
                offerGeneration.PermanentCity = reader.IsDBNull(FLD_PermanentCity) ? string.Empty : reader.GetString(FLD_PermanentCity);
                offerGeneration.PermanentState = reader.IsDBNull(FLD_PermanentState) ? string.Empty : reader.GetString(FLD_PermanentState);
                offerGeneration.PermanentZip = reader.IsDBNull(FLD_PermanentZip) ? string.Empty : reader.GetString(FLD_PermanentZip);
                offerGeneration.CurrentPosition = reader.IsDBNull(FLD_CurrentPosition) ? string.Empty : reader.GetString(FLD_CurrentPosition);
                offerGeneration.Grade = reader.IsDBNull(FLD_Grade) ? string.Empty : reader.GetString(FLD_Grade);
                offerGeneration.Basic = reader.IsDBNull(FLD_Basic) ? 0 : reader.GetDecimal(FLD_Basic);
                offerGeneration.Conveyance = reader.IsDBNull(FLD_Conveyance) ? 0 : reader.GetDecimal(FLD_Conveyance);
                offerGeneration.CompensationPlan = reader.IsDBNull(FLD_CompensationPlan) ? 0 : reader.GetDecimal(FLD_CompensationPlan);
                offerGeneration.HouseRent = reader.IsDBNull(FLD_HouseRent) ? 0 : reader.GetDecimal(FLD_HouseRent);
                offerGeneration.ProvidentFund = reader.IsDBNull(FLD_ProvidentFund) ? 0 : reader.GetDecimal(FLD_ProvidentFund);
                offerGeneration.SpecialAllowance = reader.IsDBNull(FLD_SpecialAllowance) ? 0 : reader.GetDecimal(FLD_SpecialAllowance);
                offerGeneration.VariablePay = reader.IsDBNull(FLD_VariablePay) ? 0 : reader.GetDecimal(FLD_VariablePay);
                offerGeneration.DateOfJoining = reader.IsDBNull(FLD_DateOfJoining) ? DateTime.MinValue : reader.GetDateTime(FLD_DateOfJoining);
                offerGeneration.FilePath = reader.IsDBNull(FLD_FilePath) ? string.Empty : reader.GetString(FLD_FilePath);
                offerGeneration.DateOfOffered = reader.IsDBNull(FLD_DateOfOffered) ? DateTime.MinValue : reader.GetDateTime(FLD_DateOfOffered);
                offerGeneration.OfferedSalary = reader.IsDBNull(FLD_OfferedSalary) ? 0 : reader.GetDecimal(FLD_OfferedSalary);
            }
            //**************************Suraj Adsule 20160824 For Permanent PDF Generation********************
            else if (reader.FieldCount > 1)
            {
                const int FLD_HEADS_OS = 5;
                const int FLD_VALUE_OS = 6;

                offerGeneration.Heads = reader.IsDBNull(FLD_HEADS_OS) ? string.Empty : reader.GetString(FLD_HEADS_OS);
                offerGeneration.Value = reader.IsDBNull(FLD_VALUE_OS) ? 0 : Convert.ToDouble(reader.GetValue(FLD_VALUE_OS));

            }

            return offerGeneration;
        }
    }
}
//--------------------------------Rishi Code End-------------------------------------------------//