﻿using System.Data;
using System.Collections.Generic;


using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    internal sealed class MemberEmailAttachmentBuilder : IEntityBuilder<MemberEmailAttachment>
    {
        IList<MemberEmailAttachment> IEntityBuilder<MemberEmailAttachment>.BuildEntities(IDataReader reader)
        {
            List<MemberEmailAttachment> memberEmailAttachments = new List<MemberEmailAttachment>();

            while (reader.Read())
            {
                memberEmailAttachments.Add(((IEntityBuilder<MemberEmailAttachment>)this).BuildEntity(reader));
            }

            return (memberEmailAttachments.Count > 0) ? memberEmailAttachments : null;
        }

        MemberEmailAttachment IEntityBuilder<MemberEmailAttachment>.BuildEntity(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_ATTACHMENTFILE = 1;
            const int FLD_ATTACHMENTSIZE = 2;
            const int FLD_MEMBEREMAILID = 3;

            MemberEmailAttachment memberEmailAttachment = new MemberEmailAttachment();

            memberEmailAttachment.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            memberEmailAttachment.AttachmentFile = reader.IsDBNull(FLD_ATTACHMENTFILE) ? string.Empty : reader.GetString(FLD_ATTACHMENTFILE);
            memberEmailAttachment.AttachmentSize = reader.IsDBNull(FLD_ATTACHMENTSIZE) ? 0 : reader.GetInt32(FLD_ATTACHMENTSIZE);
            memberEmailAttachment.MemberEmailId = reader.IsDBNull(FLD_MEMBEREMAILID) ? 0 : reader.GetInt32(FLD_MEMBEREMAILID);

            return memberEmailAttachment;
        }
    }
}