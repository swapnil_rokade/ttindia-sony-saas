﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    internal sealed class MemberEmailAttachmentDataAccess : BaseDataAccess, IMemberEmailAttachmentDataAccess
    {
        #region Constructors

        public MemberEmailAttachmentDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<MemberEmailAttachment> CreateEntityBuilder<MemberEmailAttachment>()
        {
            return (new MemberEmailAttachmentBuilder()) as IEntityBuilder<MemberEmailAttachment>;
        }

        #endregion

        #region  Methods

        MemberEmailAttachment IMemberEmailAttachmentDataAccess.Add(MemberEmailAttachment memberEmailAttachment)
        {
            const string SP = "dbo.MemberEmailAttachment_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@AttachmentFile", DbType.AnsiString, memberEmailAttachment.AttachmentFile);
                Database.AddInParameter(cmd, "@AttachmentSize", DbType.Int32, memberEmailAttachment.AttachmentSize);
                Database.AddInParameter(cmd, "@MemberEmailId", DbType.Int32, memberEmailAttachment.MemberEmailId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        memberEmailAttachment = CreateEntityBuilder<MemberEmailAttachment>().BuildEntity(reader);
                    }
                    else
                    {
                        memberEmailAttachment = null;
                    }
                }

                if (memberEmailAttachment == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Member email attachment already exists. Please specify another member email attachment.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while creating this member email attachment.");
                            }
                    }
                }

                return memberEmailAttachment;
            }
        }

        MemberEmailAttachment IMemberEmailAttachmentDataAccess.Update(MemberEmailAttachment memberEmailAttachment)
        {
            const string SP = "dbo.MemberEmailAttachment_Update";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, memberEmailAttachment.Id);
                Database.AddInParameter(cmd, "@AttachmentFile", DbType.AnsiString, memberEmailAttachment.AttachmentFile);
                Database.AddInParameter(cmd, "@AttachmentSize", DbType.Int32, memberEmailAttachment.AttachmentSize);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        memberEmailAttachment = CreateEntityBuilder<MemberEmailAttachment>().BuildEntity(reader);
                    }
                    else
                    {
                        memberEmailAttachment = null;
                    }
                }

                if (memberEmailAttachment == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Member email attachment already exists. Please specify another member email attachment.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while updating this member email attachment.");
                            }
                    }
                }

                return memberEmailAttachment;
            }
        }

        MemberEmailAttachment IMemberEmailAttachmentDataAccess.GetById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.MemberEmailAttachment_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<MemberEmailAttachment>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        IList<MemberEmailAttachment> IMemberEmailAttachmentDataAccess.GetAllByMemberEmailId(int memberEmailId)
        {
            if (memberEmailId < 1)
            {
                throw new ArgumentNullException("memberEmailId");
            }

            const string SP = "dbo.MemberEmailAttachment_GetAllMemberEmailAttachmentByMemberEmailId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberEmailId", DbType.Int32, memberEmailId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<MemberEmailAttachment>().BuildEntities(reader);
                }
            }
        }

        IList<MemberEmailAttachment> IMemberEmailAttachmentDataAccess.GetAll()
        {
            const string SP = "dbo.MemberEmailAttachment_GetAll";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<MemberEmailAttachment>().BuildEntities(reader);
                }
            }
        }

        bool IMemberEmailAttachmentDataAccess.DeleteById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.MemberEmailAttachment_DeleteById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a member email attachment which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this member email attachment.");
                        }
                }
            }
        }

        bool IMemberEmailAttachmentDataAccess.DeleteByMemberEmailId(int memberEmailId)
        {
            if (memberEmailId < 1)
            {
                throw new ArgumentNullException("memberEmailId");
            }

            const string SP = "dbo.MemberEmailAttachment_DeleteByMemberEmailId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@MemberEmailId", DbType.Int32, memberEmailId);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a member email attachment which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this member email attachment.");
                        }
                }
            }
        }

        #endregion
    }
}