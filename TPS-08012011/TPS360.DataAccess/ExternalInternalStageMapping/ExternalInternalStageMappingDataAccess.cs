﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;
using Microsoft.Practices.EnterpriseLibrary.Data;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Helper;

namespace TPS360.DataAccess
{
    internal sealed class ExternalInternalStageMappingDataAccess : BaseDataAccess, IExternalInternalStageMappingDataAccess
    {
         #region Constructors

        public ExternalInternalStageMappingDataAccess(Context context)
            : base(context)
        {
        }
        protected override IEntityBuilder<ExternalInternalStageMapping> CreateEntityBuilder<ExternalInternalStageMapping>()
        {
            return (new ExternalInternalStageMappingBuilder()) as IEntityBuilder<ExternalInternalStageMapping>;
        }
        #endregion
        #region  Methods
        ExternalInternalStageMapping IExternalInternalStageMappingDataAccess.Add(ExternalInternalStageMapping externalViewStages)
        {
            const string SP = "dbo.ExternalInternalStageMapping_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@ExternalStageId", DbType.Int32, externalViewStages.ExternalViewStageID);
                //Database.AddInParameter(cmd, "@InternalStageId", DbType.Int32, externalViewStages.HiringMatrixLevelsID);
                Database.AddInParameter(cmd, "@InternalStage", DbType.AnsiString, StringHelper.Convert(externalViewStages.HiringMatrixLevel));
                Database.AddInParameter(cmd, "@CreatorID", DbType.AnsiString, Convert.ToInt32(externalViewStages.CreatorId));
                //Database.AddInParameter(cmd, "@UpdatorID", DbType.AnsiString, Convert.ToInt32(externalViewStages.UpdatorId));

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        externalViewStages = CreateEntityBuilder<ExternalInternalStageMapping>().BuildEntity(reader);
                    }
                    else
                    {
                        externalViewStages = null;
                    }
                }

                if (externalViewStages == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Mapping is already exists. Please specify another mapping.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while creating this Mapping.");
                            }
                    }
                }

                return externalViewStages;
            }
        }
        bool IExternalInternalStageMappingDataAccess.DeleteMappingByHiringStatus(string hiringLevel)
        {
            const string SP = "dbo.ExternalInternalStageMapping_DeleteByHiringLevel";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@InternalStage", DbType.AnsiString, hiringLevel);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this internal external mapping.");
                        }
                }
            }
        }
        IList<ExternalInternalStageMapping> IExternalInternalStageMappingDataAccess.GetAllExternalInternalStageMapping()
        {
            const string SP = "dbo.ExternalInternalStageMapping_GetAll";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<ExternalInternalStageMapping>().BuildEntities(reader);
                }
            }
        }
        #endregion
    }
}
