﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    internal sealed class SavedQueryBuilder : IEntityBuilder<SavedQuery>
    {
        IList<SavedQuery> IEntityBuilder<SavedQuery>.BuildEntities(IDataReader reader)
        {
            List<SavedQuery> savedQuerys = new List<SavedQuery>();

            while (reader.Read())
            {
                savedQuerys.Add(((IEntityBuilder<SavedQuery>)this).BuildEntity(reader));
            }

            return (savedQuerys.Count > 0) ? savedQuerys : null;
        }

        SavedQuery IEntityBuilder<SavedQuery>.BuildEntity(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_QUERYCODE = 1;
            const int FLD_NAME = 2;
            const int FLD_QUERYTYPE = 3;
            const int FLD_QUERY = 4;
            const int FLD_ISREMOVED = 5;
            const int FLD_CREATORID = 6;
            const int FLD_UPDATORID = 7;
            const int FLD_CREATEDATE = 8;
            const int FLD_UPDATEDATE = 9;

            SavedQuery savedQuery = new SavedQuery();

            savedQuery.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            savedQuery.QueryCode = reader.IsDBNull(FLD_QUERYCODE) ? string.Empty : reader.GetString(FLD_QUERYCODE);
            savedQuery.Name = reader.IsDBNull(FLD_NAME) ? string.Empty : reader.GetString(FLD_NAME);
            savedQuery.QueryType = reader.IsDBNull(FLD_QUERYTYPE) ? 0 : reader.GetInt32(FLD_QUERYTYPE);
            savedQuery.Query = reader.IsDBNull(FLD_QUERY) ? string.Empty : reader.GetString(FLD_QUERY);
            savedQuery.IsRemoved = reader.IsDBNull(FLD_ISREMOVED) ? false : reader.GetBoolean(FLD_ISREMOVED);
            savedQuery.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
            savedQuery.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);
            savedQuery.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
            savedQuery.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);

            return savedQuery;
        }
    }
}