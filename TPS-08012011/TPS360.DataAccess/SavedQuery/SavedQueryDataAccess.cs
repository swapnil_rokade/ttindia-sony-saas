﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common;
using TPS360.Common.Shared;
using TPS360.Common.Helper;

namespace TPS360.DataAccess
{
    internal sealed class SavedQueryDataAccess : BaseDataAccess, ISavedQueryDataAccess
    {
        #region Constructors

        public SavedQueryDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<SavedQuery> CreateEntityBuilder<SavedQuery>()
        {
            return (new SavedQueryBuilder()) as IEntityBuilder<SavedQuery>;
        }

        #endregion

        #region  Methods

        SavedQuery ISavedQueryDataAccess.Add(SavedQuery savedQuery)
        {
            const string SP = "dbo.SavedQuery_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@QueryCode", DbType.AnsiString, savedQuery.QueryCode);
                Database.AddInParameter(cmd, "@Name", DbType.AnsiString, savedQuery.Name);
                Database.AddInParameter(cmd, "@QueryType", DbType.Int32, savedQuery.QueryType);
                Database.AddInParameter(cmd, "@Query", DbType.AnsiString, savedQuery.Query);
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, savedQuery.IsRemoved);
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, savedQuery.CreatorId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        savedQuery = CreateEntityBuilder<SavedQuery>().BuildEntity(reader);
                    }
                    else
                    {
                        savedQuery = null;
                    }
                }

                if (savedQuery == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("SavedQuery already exists. Please specify another savedQuery.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while creating this savedQuery.");
                            }
                    }
                }

                return savedQuery;
            }
        }

        SavedQuery ISavedQueryDataAccess.Update(SavedQuery savedQuery)
        {
            const string SP = "dbo.SavedQuery_Update";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, savedQuery.Id);
                Database.AddInParameter(cmd, "@QueryCode", DbType.AnsiString, savedQuery.QueryCode);
                Database.AddInParameter(cmd, "@Name", DbType.AnsiString, savedQuery.Name);
                Database.AddInParameter(cmd, "@QueryType", DbType.Int32, savedQuery.QueryType);
                Database.AddInParameter(cmd, "@Query", DbType.AnsiString, savedQuery.Query);
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, savedQuery.IsRemoved);
                Database.AddInParameter(cmd, "@UpdatorId", DbType.Int32, savedQuery.UpdatorId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        savedQuery = CreateEntityBuilder<SavedQuery>().BuildEntity(reader);
                    }
                    else
                    {
                        savedQuery = null;
                    }
                }

                if (savedQuery == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("SavedQuery already exists. Please specify another savedQuery.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while updating this savedQuery.");
                            }
                    }
                }

                return savedQuery;
            }
        }

        SavedQuery ISavedQueryDataAccess.GetById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.SavedQuery_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<SavedQuery>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        IList<SavedQuery> ISavedQueryDataAccess.GetAll()
        {
            const string SP = "dbo.SavedQuery_GetAll";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<SavedQuery>().BuildEntities(reader);
                }
            }
        }

        bool ISavedQueryDataAccess.DeleteById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.SavedQuery_DeleteById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a savedQuery which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this savedQuery.");
                        }
                }
            }
        }

        PagedResponse<SavedQuery> ISavedQueryDataAccess.GetPaged(PagedRequest request)
        {
            const string SP = "dbo.SavedQuery_GetPaged";
            string whereClause = string.Empty;

            if (request.Conditions.Count > 0)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();

                string column = null;
                string value = null;

                foreach (DictionaryEntry entry in request.Conditions)
                {
                    column = StringHelper.Convert(entry.Key);
                    value = StringHelper.Convert(entry.Value);

                    if (!StringHelper.IsBlank(column))
                    {
                        value = value.Replace("'", "''");

                        if (StringHelper.IsEqual(column, "creatorId"))
                        {
                            sb.Append("[S].[CreatorId]");
                            sb.Append(" = ");
                            sb.Append(value);
                        }
                        else
                        {
                            sb.Append("[S].[");
                            sb.Append(column);
                            sb.Append("]");
                            sb.Append(" LIKE '");
                            if (!value.StartsWith("%"))
                            {
                                sb.Append("%");
                            }
                            sb.Append(value);
                            if (!value.EndsWith("%"))
                            {
                                sb.Append("%");
                            }
                            sb.Append("'");
                        }
                    }

                    sb.Append(" AND ");
                }

                //Remove the Last AND Clasue
                sb.Remove(sb.Length - 5, 5);
                whereClause = sb.ToString();
            }

            if (StringHelper.IsBlank(request.SortColumn))
            {
                request.SortColumn = "QueryCode";
            }

            request.SortColumn = "[S].[" + request.SortColumn + "]";

            object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder)
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<SavedQuery> response = new PagedResponse<SavedQuery>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = CreateEntityBuilder<SavedQuery>().BuildEntities(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }

        #endregion
    }
}