﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: RejectCandidateBuilder.cs
    Description: This page is used to build reject candidate entities.
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.        Date               Author           Modification
    ------------------------------------------------------------------------------------------------------------------------------    
 *   0.1           14/March/2016     pravin khot      added if (reader.FieldCount > 11)  ,IList<DynamicDictionary> ,DynamicDictionary BuildEntityForReport

                                                                        
-------------------------------------------------------------------------------------------------------------------------------------------        
*/
using System;
using System.Data;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    internal sealed class RejectCandidateBuilder : IEntityBuilder<RejectCandidate>
    {
        IList<RejectCandidate> IEntityBuilder<RejectCandidate>.BuildEntities(IDataReader reader)
        {
            List<RejectCandidate> RejectCandidate = new List<RejectCandidate>();

            while (reader.Read())
            {
                RejectCandidate.Add(((IEntityBuilder<RejectCandidate>)this).BuildEntity(reader));
            }

            return (RejectCandidate.Count > 0) ? RejectCandidate : null;
        }
        //*********Code added by pravin khot on 14/March/2016***********************
        public IList<DynamicDictionary> BuildEntities(IDataReader reader, IList<string> CheckedList)
        {
            List<DynamicDictionary> re = new List<DynamicDictionary>();
            while (reader.Read())
            {

                re.Add(BuildEntityForReport(reader, CheckedList));

            }

            return (re.Count > 0) ? re : null;
        }
        DynamicDictionary BuildEntityForReport(IDataReader reader, IList<string> CheckedList)
        {
            DynamicDictionary newDyn = new DynamicDictionary();
            foreach (string s in CheckedList)
            {
                try
                {
                    string m = "";
                    //if (s == "Name")
                    //    m = reader["FirstName"].ToString () + " " + reader["LastName"].ToString ();
                    //else 
                    m = reader[s].ToString();
                    if (s == "ResumeSource")
                    {
                        m = TPS360.Common.EnumHelper.GetDescription((TPS360.Common.Shared.ResumeSource)Convert.ToInt32(m));
                    }

                    newDyn.properties.Add(s, m);
                }
                catch
                {
                }
            }
            return newDyn;
        }
        //****************************END**************************************************

        RejectCandidate IEntityBuilder<RejectCandidate>.BuildEntity(IDataReader reader)
        {
            RejectCandidate RejectCandi = new RejectCandidate();

            if (reader.FieldCount == 11)
            {
                const int FLD_ID = 0;
                const int FLD_MEMBERID = 1;
                const int FLD_REJECTDETAILS = 2;
                const int FLD_CREATORID = 3;
                const int FLD_UPDATORID = 4;
                const int FLD_CREATEDATE = 5;
                const int FLD_UPDATEDATE = 6;
                const int FLD_HIRINGMATRIXLEVEL = 7;
                const int FLD_JOBPOSTINGID = 8;
                const int FLD_JOBTITLE = 9;
                const int FLD_REJECTOR = 10;

                RejectCandi.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
                RejectCandi.MemberId = reader.IsDBNull(FLD_MEMBERID) ? 0 : reader.GetInt32(FLD_MEMBERID);
                RejectCandi.JobPostingID = reader.IsDBNull(FLD_JOBPOSTINGID) ? 0 : reader.GetInt32(FLD_JOBPOSTINGID);
                RejectCandi.RejectDetails = reader.IsDBNull(FLD_REJECTDETAILS) ? string.Empty : reader.GetString(FLD_REJECTDETAILS);
                RejectCandi.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
                RejectCandi.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);
                RejectCandi.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
                RejectCandi.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);
                RejectCandi.HiringMatrixLevel = reader.IsDBNull(FLD_HIRINGMATRIXLEVEL) ? 0 : reader.GetInt32(FLD_HIRINGMATRIXLEVEL);
                RejectCandi.JobTitle = reader.IsDBNull(FLD_JOBTITLE) ? string.Empty : reader.GetString(FLD_JOBTITLE);
                RejectCandi.RejectorName = reader.IsDBNull(FLD_REJECTOR) ? string.Empty : reader.GetString(FLD_REJECTOR);
               

            }

            if (reader.FieldCount == 9)
            {
                const int FLD_ID = 0;
                const int FLD_MEMBERID = 1;
                const int FLD_JOBPOSTINGID = 2;
                const int FLD_REJECTDETAILS = 3;
                const int FLD_CREATORID = 4;
                const int FLD_UPDATORID = 5;
                const int FLD_CREATEDATE = 6;
                const int FLD_UPDATEDATE = 7;
                const int FLD_HIRINGMATRIXLEVEL = 8;

                RejectCandi.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
                RejectCandi.MemberId = reader.IsDBNull(FLD_MEMBERID) ? 0 : reader.GetInt32(FLD_MEMBERID);
                RejectCandi.JobPostingID = reader.IsDBNull(FLD_JOBPOSTINGID) ? 0 : reader.GetInt32(FLD_JOBPOSTINGID);
                RejectCandi.RejectDetails = reader.IsDBNull(FLD_REJECTDETAILS) ? string.Empty : reader.GetString(FLD_REJECTDETAILS);
                RejectCandi.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
                RejectCandi.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);
                RejectCandi.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
                RejectCandi.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);
                RejectCandi.HiringMatrixLevel = reader.IsDBNull(FLD_HIRINGMATRIXLEVEL) ? 0 : reader.GetInt32(FLD_HIRINGMATRIXLEVEL);
                
            }
          
            //************Code added by pravin khot on 14/March/2016*******************
            if (reader.FieldCount > 11) 
            {
                const int FLD_ID = 0;
                const int FLD_FIRSTNAME = 1;
                const int FLD_MIDDLENAME = 2;
                const int FLD_LASTNAME = 3;
                const int FLD_PRIMARYEMAIL = 4;
                const int FLD_PRIMARYPHONE = 5;
                const int FLD_PRIMARYPHONEEXTENSION = 6;
                const int FLD_CELLPHONE = 7;
                const int FLD_STATUS = 8;
                const int FLD_HOMEPHONE = 9;
                const int FLD_CURRENTCITY = 10;
                const int FLD_STATENAME = 11;
                const int FLD_STATECODE = 12;
                const int FLD_OFFICECITY = 13;
                const int FLD_CURRENTPOSITION = 14;
                const int FLD_REMARKS = 15;
                const int FLD_OBJECTIVE = 16;
                const int FLD_SUMMARY = 17;
                const int FLD_SKILLS = 18;
                const int FLD_USERID = 19;
                const int FLD_CREATORID = 20;
                const int FLD_UPDATORID = 21;
                const int FLD_CREATEDATE = 22;
                const int FLD_UPDATEDATE = 23;

                const int FLD_RECORDTYPE = 24;
                const int FLD_NICKNAME = 25;
                const int FLD_DATEOFBIRTH = 26;
                const int FLD_PERMANENTADDRESSLINE1 = 27;
                const int FLD_PERMANENTADDRESSLINE2 = 28;
                const int FLD_PERMANENTCITY = 29;
                const int FLD_PERMANENTSTATEID = 30;
                const int FLD_PERMANENTSTATENAME = 31;
                const int FLD_PERMANENTSTATECODE = 32;
                const int FLD_PERMANENTZIP = 33;
                const int FLD_PERMANENTCOUNTRYID = 34;
                const int FLD_PERMANENTCOUNTRYNAME = 35;
                const int FLD_PERMANENTCOUNTRYCODE = 36;
                const int FLD_PERMANENTPHONE = 37;
                const int FLD_PERMANENTPHONEEXT = 38;
                const int FLD_PERMANENTMOBILE = 39;
                const int FLD_ALTERNATEEMAIL = 40;
                const int FLD_RESUMESOURCE = 41;
                const int FLD_CURRENTADDRESSLINE1 = 42;
                const int FLD_CURRENTADDRESSLINE2 = 43;
                const int FLD_CURRENTSTATEID = 44;
                const int FLD_CURRENTSTATENAME = 45;
                const int FLD_CURRENTSTATECODE = 46;
                const int FLD_CURRENTZIP = 47;
                const int FLD_CURRENTCOUNTRYID = 48;
                const int FLD_CURRENTCOUNTRYNAME = 49;
                const int FLD_CURRENTCOUNTRYCODE = 50;
                const int FLD_OFFICEPHONE = 51;
                const int FLD_OFFICEPHONEEXTENSION = 52;
                const int FLD_CITYOFBIRTH = 53;
                const int FLD_COUNTRYIDOFBIRTH = 54;
                const int FLD_BIRTHCOUNTRYNAME = 55;
                const int FLD_BIRTHCOUNTRYCODE = 56;
                const int FLD_COUNTRYIDOFCITIZENSHIP = 57;
                const int FLD_CITIZENSHIPCOUNTRYNAME = 58;
                const int FLD_CITIZENSHIPCOUNTRYCODE = 59;
                const int FLD_GENDERLOOKUPID = 60;
                const int FLD_GENDER = 61;
                const int FLD_ETHNICGROUPLOOKUPID = 62;
                const int FLD_ETHNICGROUP = 63;
                const int FLD_BLOODGROUPLOOKUPID = 64;
                const int FLD_BLOODGROUP = 65;
                const int FLD_MARITALSTATUSLOOKUPID = 66;
                const int FLD_MARITALSTATUS = 67;
                const int FLD_RELOCATION = 68;
                const int FLD_LASTEMPLOYER = 69;
                const int FLD_TOTALEXPERIENCEYEARS = 70;
                const int FLD_AVAILABILITY = 71;
                const int FLD_AVAILABILITYTEXT = 72;
                const int FLD_AVAILABLEDATE = 73;
                const int FLD_CURRENTYEARLYRATE = 74;
                const int FLD_CURRENTYEARLYCURRENCYLOOKUPID = 75;
                const int FLD_CURRENTYEARLYCURRENCY = 76;
                const int FLD_CURRENTMONTHLYRATE = 77;
                const int FLD_CURRENTMONTHLYCURRENCYLOOKUPID = 78;
                const int FLD_CURRENTMONTHLYCURRENCY = 79;
                const int FLD_CURRENTHOURLYRATE = 80;
                const int FLD_CURRENTHOURLYCURRENCYLOOKUPID = 81;
                const int FLD_CURRENTHOURLYCURRENCY = 82;
                const int FLD_EXPECTEDYEARLYRATE = 83;
                const int FLD_EXPECTEDYEARLYCURRENCYLOOKUPID = 84;
                const int FLD_EXPECTEDYEARLYCURRENCY = 85;
                const int FLD_EXPECTEDMONTHLYRATE = 86;
                const int FLD_EXPECTEDMONTHLYCURRENCYLOOKUPID = 87;
                const int FLD_EXPECTEDMONTHLYCURRENCY = 88;
                const int FLD_EXPECTEDHOURLYRATE = 89;
                const int FLD_EXPECTEDHOURLYCURRENCYLOOKUPID = 90;
                const int FLD_EXPECTEDHOURLYCURRENCY = 91;
                const int FLD_WILLINGTOTRAVEL = 92;
                const int FLD_SALARYTYPELOOKUPID = 93;
                const int FLD_SALARYTYPE = 94;
                const int FLD_JOBTYPELOOKUPID = 95;
                const int FLD_JOBTYPE = 96;
                const int FLD_SECURITYCLEARANCE = 97;
                const int FLD_WORKAUTHORIZATIONLOOKUPID = 98;
                const int FLD_WORKAUTHORIZATION = 99;
                const int FLD_CREATOR = 100;
                const int FLD_UPDATOR = 101;
                const int FLD_WORKSCEDULELOOKIPID = 102;
                const int FLD_WORKSCEDULE = 103;
                const int FLD_WESITE = 104;
                const int FLD_LINKEDINPROFILE = 105;
                const int FLD_PASSPORTSTATUS = 106;
                const int FLD_IDCARD = 107;
                const int FLD_IDCARDDETAIL = 108;
                const int FLD_HIGHEREDUCATION = 109;
                const int FLD_SOURCE = 110;
                const int FLD_SOURCEDESCRIPTION = 111;            
                const int FLD_NOTICEPERIOD = 112;
                const int FLD_DEPARTMENT = 113;
                const int FLD_JOBCODE = 114;
                const int FLD_FEEDBACKSTATUS = 115;
                const int FLD_HIRINGMANAGER = 116;              
                const int FLD_RejectDetails = 117;
                const int FLD_RejectedDate = 118;
                const int FLD_JobTitle = 119;             

                RejectCandi.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
                RejectCandi.FirstName = reader.IsDBNull(FLD_FIRSTNAME) ? string.Empty : reader.GetString(FLD_FIRSTNAME);
                RejectCandi.MiddleName = reader.IsDBNull(FLD_MIDDLENAME) ? string.Empty : reader.GetString(FLD_MIDDLENAME);
                RejectCandi.LastName = reader.IsDBNull(FLD_LASTNAME) ? string.Empty : reader.GetString(FLD_LASTNAME);
                RejectCandi.PrimaryEmail = reader.IsDBNull(FLD_PRIMARYEMAIL) ? string.Empty : reader.GetString(FLD_PRIMARYEMAIL);
                RejectCandi.PrimaryPhone = reader.IsDBNull(FLD_PRIMARYPHONE) ? string.Empty : reader.GetString(FLD_PRIMARYPHONE);
                RejectCandi.PrimaryPhoneExtension = reader.IsDBNull(FLD_PRIMARYPHONEEXTENSION) ? string.Empty : reader.GetString(FLD_PRIMARYPHONEEXTENSION);
                RejectCandi.CellPhone = reader.IsDBNull(FLD_CELLPHONE) ? string.Empty : reader.GetString(FLD_CELLPHONE);
                RejectCandi.Status = reader.IsDBNull(FLD_STATUS) ? 0 : reader.GetInt32(FLD_STATUS);
                RejectCandi.HomePhone = reader.IsDBNull(FLD_HOMEPHONE) ? string.Empty : reader.GetString(FLD_HOMEPHONE);
                RejectCandi.CurrentCity = reader.IsDBNull(FLD_CURRENTCITY) ? string.Empty : reader.GetString(FLD_CURRENTCITY);
                RejectCandi.StateName = reader.IsDBNull(FLD_STATENAME) ? string.Empty : reader.GetString(FLD_STATENAME);
                RejectCandi.StateCode = reader.IsDBNull(FLD_STATECODE) ? string.Empty : reader.GetString(FLD_STATECODE);
                RejectCandi.OfficeCity = reader.IsDBNull(FLD_OFFICECITY) ? string.Empty : reader.GetString(FLD_OFFICECITY);
                RejectCandi.CurrentPosition = reader.IsDBNull(FLD_CURRENTPOSITION) ? string.Empty : reader.GetString(FLD_CURRENTPOSITION);
                RejectCandi.Remarks = reader.IsDBNull(FLD_REMARKS) ? string.Empty : reader.GetString(FLD_REMARKS);
                RejectCandi.Objective = reader.IsDBNull(FLD_OBJECTIVE) ? string.Empty : reader.GetString(FLD_OBJECTIVE);
                RejectCandi.Summary = reader.IsDBNull(FLD_SUMMARY) ? string.Empty : reader.GetString(FLD_SUMMARY);
                RejectCandi.Skills = reader.IsDBNull(FLD_SKILLS) ? string.Empty : reader.GetString(FLD_SKILLS);
                RejectCandi.UserId = reader.IsDBNull(FLD_USERID) ? Guid.Empty : reader.GetGuid(FLD_USERID);
                RejectCandi.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
                RejectCandi.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);
                RejectCandi.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
                RejectCandi.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);
                string strRecordType = reader.IsDBNull(FLD_RECORDTYPE) ? string.Empty : reader.GetString(FLD_RECORDTYPE);
                string IDCard = "";
                string IDCardDetail = "";
                if (strRecordType.ToUpper() == "LIST")
                {
                    RejectCandi.LastEmployer = reader.IsDBNull(25) ? string.Empty : reader.GetString(25);
                }
                else if (strRecordType.ToUpper() == "REPORT")
                {
                    RejectCandi.NickName = reader.IsDBNull(FLD_NICKNAME) ? string.Empty : reader.GetString(FLD_NICKNAME);
                    RejectCandi.DateOfBirth = reader.IsDBNull(FLD_DATEOFBIRTH) ? DateTime.MinValue : reader.GetDateTime(FLD_DATEOFBIRTH);
                    RejectCandi.PermanentAddressLine1 = reader.IsDBNull(FLD_PERMANENTADDRESSLINE1) ? string.Empty : reader.GetString(FLD_PERMANENTADDRESSLINE1);
                    RejectCandi.PermanentAddressLine2 = reader.IsDBNull(FLD_PERMANENTADDRESSLINE2) ? string.Empty : reader.GetString(FLD_PERMANENTADDRESSLINE2);
                    RejectCandi.PermanentCity = reader.IsDBNull(FLD_PERMANENTCITY) ? string.Empty : reader.GetString(FLD_PERMANENTCITY);
                    RejectCandi.PermanentStateId = reader.IsDBNull(FLD_PERMANENTSTATEID) ? 0 : reader.GetInt32(FLD_PERMANENTSTATEID);
                    RejectCandi.PermanentStateName = reader.IsDBNull(FLD_PERMANENTSTATENAME) ? string.Empty : reader.GetString(FLD_PERMANENTSTATENAME);
                    RejectCandi.PermanentStateCode = reader.IsDBNull(FLD_PERMANENTSTATECODE) ? string.Empty : reader.GetString(FLD_PERMANENTSTATECODE);
                    RejectCandi.PermanentZip = reader.IsDBNull(FLD_PERMANENTZIP) ? string.Empty : reader.GetString(FLD_PERMANENTZIP);
                    RejectCandi.PermanentCountryId = reader.IsDBNull(FLD_PERMANENTCOUNTRYID) ? 0 : reader.GetInt32(FLD_PERMANENTCOUNTRYID);
                    RejectCandi.PermanentCountryName = reader.IsDBNull(FLD_PERMANENTCOUNTRYNAME) ? string.Empty : reader.GetString(FLD_PERMANENTCOUNTRYNAME);
                    RejectCandi.PermanentCountryCode = reader.IsDBNull(FLD_PERMANENTCOUNTRYCODE) ? string.Empty : reader.GetString(FLD_PERMANENTCOUNTRYCODE);
                    RejectCandi.PermanentPhone = reader.IsDBNull(FLD_PERMANENTPHONE) ? string.Empty : reader.GetString(FLD_PERMANENTPHONE);
                    RejectCandi.PermanentPhoneExt = reader.IsDBNull(FLD_PERMANENTPHONEEXT) ? string.Empty : reader.GetString(FLD_PERMANENTPHONEEXT);
                    RejectCandi.PermanentMobile = reader.IsDBNull(FLD_PERMANENTMOBILE) ? string.Empty : reader.GetString(FLD_PERMANENTMOBILE);
                    RejectCandi.AlternateEmail = reader.IsDBNull(FLD_ALTERNATEEMAIL) ? string.Empty : reader.GetString(FLD_ALTERNATEEMAIL);
                    RejectCandi.ResumeSource = reader.IsDBNull(FLD_RESUMESOURCE) ? 0 : reader.GetInt32(FLD_RESUMESOURCE);
                    RejectCandi.CurrentAddressLine1 = reader.IsDBNull(FLD_CURRENTADDRESSLINE1) ? string.Empty : reader.GetString(FLD_CURRENTADDRESSLINE1);
                    RejectCandi.CurrentAddressLine2 = reader.IsDBNull(FLD_CURRENTADDRESSLINE2) ? string.Empty : reader.GetString(FLD_CURRENTADDRESSLINE2);
                    RejectCandi.CurrentStateId = reader.IsDBNull(FLD_CURRENTSTATEID) ? 0 : reader.GetInt32(FLD_CURRENTSTATEID);
                    RejectCandi.CurrentStateName = reader.IsDBNull(FLD_CURRENTSTATENAME) ? string.Empty : reader.GetString(FLD_CURRENTSTATENAME);
                    RejectCandi.CurrentStateCode = reader.IsDBNull(FLD_CURRENTSTATECODE) ? string.Empty : reader.GetString(FLD_CURRENTSTATECODE);
                    RejectCandi.CurrentZip = reader.IsDBNull(FLD_CURRENTZIP) ? string.Empty : reader.GetString(FLD_CURRENTZIP);
                    RejectCandi.CurrentCountryId = reader.IsDBNull(FLD_CURRENTCOUNTRYID) ? 0 : reader.GetInt32(FLD_CURRENTCOUNTRYID);
                    RejectCandi.CurrentCountryName = reader.IsDBNull(FLD_CURRENTCOUNTRYNAME) ? string.Empty : reader.GetString(FLD_CURRENTCOUNTRYNAME);
                    RejectCandi.CurrentCountryCode = reader.IsDBNull(FLD_CURRENTCOUNTRYCODE) ? string.Empty : reader.GetString(FLD_CURRENTCOUNTRYCODE);
                    RejectCandi.OfficePhone = reader.IsDBNull(FLD_OFFICEPHONE) ? string.Empty : reader.GetString(FLD_OFFICEPHONE);
                    RejectCandi.OfficePhoneExtension = reader.IsDBNull(FLD_OFFICEPHONEEXTENSION) ? string.Empty : reader.GetString(FLD_OFFICEPHONEEXTENSION);
                    RejectCandi.CityOfBirth = reader.IsDBNull(FLD_CITYOFBIRTH) ? string.Empty : reader.GetString(FLD_CITYOFBIRTH);
                    RejectCandi.CountryIdOfBirth = reader.IsDBNull(FLD_COUNTRYIDOFBIRTH) ? string.Empty : reader.GetString(FLD_COUNTRYIDOFBIRTH);
                    RejectCandi.BirthCountryName = reader.IsDBNull(FLD_BIRTHCOUNTRYNAME) ? string.Empty : reader.GetString(FLD_BIRTHCOUNTRYNAME);
                    RejectCandi.BirthCountryCode = reader.IsDBNull(FLD_BIRTHCOUNTRYCODE) ? string.Empty : reader.GetString(FLD_BIRTHCOUNTRYCODE);
                    RejectCandi.CountryIdOfCitizenship = reader.IsDBNull(FLD_COUNTRYIDOFCITIZENSHIP) ? string.Empty : reader.GetString(FLD_COUNTRYIDOFCITIZENSHIP);
                    RejectCandi.CitizenshipCountryName = reader.IsDBNull(FLD_CITIZENSHIPCOUNTRYNAME) ? string.Empty : reader.GetString(FLD_CITIZENSHIPCOUNTRYNAME);
                    RejectCandi.CitizenshipCountryCode = reader.IsDBNull(FLD_CITIZENSHIPCOUNTRYCODE) ? string.Empty : reader.GetString(FLD_CITIZENSHIPCOUNTRYCODE);
                    RejectCandi.GenderLookupId = reader.IsDBNull(FLD_GENDERLOOKUPID) ? 0 : reader.GetInt32(FLD_GENDERLOOKUPID);
                    RejectCandi.Gender = reader.IsDBNull(FLD_GENDER) ? string.Empty : reader.GetString(FLD_GENDER);
                    RejectCandi.EthnicGroupLookupId = reader.IsDBNull(FLD_ETHNICGROUPLOOKUPID) ? 0 : reader.GetInt32(FLD_ETHNICGROUPLOOKUPID);
                    RejectCandi.EthnicGroup = reader.IsDBNull(FLD_ETHNICGROUP) ? string.Empty : reader.GetString(FLD_ETHNICGROUP);
                    RejectCandi.BloodGroupLookupId = reader.IsDBNull(FLD_BLOODGROUPLOOKUPID) ? 0 : reader.GetInt32(FLD_BLOODGROUPLOOKUPID);
                    RejectCandi.BloodGroup = reader.IsDBNull(FLD_BLOODGROUP) ? string.Empty : reader.GetString(FLD_BLOODGROUP);
                    RejectCandi.MaritalStatusLookupId = reader.IsDBNull(FLD_MARITALSTATUSLOOKUPID) ? 0 : reader.GetInt32(FLD_MARITALSTATUSLOOKUPID);
                    RejectCandi.MaritalStatus = reader.IsDBNull(FLD_MARITALSTATUS) ? string.Empty : reader.GetString(FLD_MARITALSTATUS);
                    RejectCandi.Relocation = reader.IsDBNull(FLD_RELOCATION) ? false : reader.GetBoolean(FLD_RELOCATION);
                    RejectCandi.LastEmployer = reader.IsDBNull(FLD_LASTEMPLOYER) ? string.Empty : reader.GetString(FLD_LASTEMPLOYER);
                    RejectCandi.TotalExperienceYears = reader.IsDBNull(FLD_TOTALEXPERIENCEYEARS) ? string.Empty : reader.GetString(FLD_TOTALEXPERIENCEYEARS);
                    RejectCandi.Availability = reader.IsDBNull(FLD_AVAILABILITY) ? 0 : reader.GetInt32(FLD_AVAILABILITY);
                    RejectCandi.AvailabilityText = reader.IsDBNull(FLD_AVAILABILITYTEXT) ? string.Empty : reader.GetString(FLD_AVAILABILITYTEXT);
                    RejectCandi.AvailableDate = reader.IsDBNull(FLD_AVAILABLEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_AVAILABLEDATE);
                    RejectCandi.CurrentYearlyRate = reader.IsDBNull(FLD_CURRENTYEARLYRATE) ? 0 : reader.GetDecimal(FLD_CURRENTYEARLYRATE);
                    RejectCandi.CurrentYearlyCurrencyLookupId = reader.IsDBNull(FLD_CURRENTYEARLYCURRENCYLOOKUPID) ? 0 : reader.GetInt32(FLD_CURRENTYEARLYCURRENCYLOOKUPID);
                    RejectCandi.CurrentYearlyCurrency = reader.IsDBNull(FLD_CURRENTYEARLYCURRENCY) ? string.Empty : reader.GetString(FLD_CURRENTYEARLYCURRENCY);
                    RejectCandi.CurrentMonthlyRate = reader.IsDBNull(FLD_CURRENTMONTHLYRATE) ? 0 : reader.GetDecimal(FLD_CURRENTMONTHLYRATE);
                    RejectCandi.CurrentMonthlyCurrencyLookupId = reader.IsDBNull(FLD_CURRENTMONTHLYCURRENCYLOOKUPID) ? 0 : reader.GetInt32(FLD_CURRENTMONTHLYCURRENCYLOOKUPID);
                    RejectCandi.CurrentMonthlyCurrency = reader.IsDBNull(FLD_CURRENTMONTHLYCURRENCY) ? string.Empty : reader.GetString(FLD_CURRENTMONTHLYCURRENCY);
                    RejectCandi.CurrentHourlyRate = reader.IsDBNull(FLD_CURRENTHOURLYRATE) ? 0 : reader.GetDecimal(FLD_CURRENTHOURLYRATE);
                    RejectCandi.CurrentHourlyCurrencyLookupId = reader.IsDBNull(FLD_CURRENTHOURLYCURRENCYLOOKUPID) ? 0 : reader.GetInt32(FLD_CURRENTHOURLYCURRENCYLOOKUPID);
                    RejectCandi.CurrentHourlyCurrency = reader.IsDBNull(FLD_CURRENTHOURLYCURRENCY) ? string.Empty : reader.GetString(FLD_CURRENTHOURLYCURRENCY);
                    RejectCandi.ExpectedYearlyRate = reader.IsDBNull(FLD_EXPECTEDYEARLYRATE) ? 0 : reader.GetDecimal(FLD_EXPECTEDYEARLYRATE);
                    RejectCandi.ExpectedYearlyCurrencyLookupId = reader.IsDBNull(FLD_EXPECTEDYEARLYCURRENCYLOOKUPID) ? 0 : reader.GetInt32(FLD_EXPECTEDYEARLYCURRENCYLOOKUPID);
                    RejectCandi.ExpectedYearlyCurrency = reader.IsDBNull(FLD_EXPECTEDYEARLYCURRENCY) ? string.Empty : reader.GetString(FLD_EXPECTEDYEARLYCURRENCY);
                    RejectCandi.ExpectedMonthlyRate = reader.IsDBNull(FLD_EXPECTEDMONTHLYRATE) ? 0 : reader.GetDecimal(FLD_EXPECTEDMONTHLYRATE);
                    RejectCandi.ExpectedMonthlyCurrencyLookupId = reader.IsDBNull(FLD_EXPECTEDMONTHLYCURRENCYLOOKUPID) ? 0 : reader.GetInt32(FLD_EXPECTEDMONTHLYCURRENCYLOOKUPID);
                    RejectCandi.ExpectedMonthlyCurrency = reader.IsDBNull(FLD_EXPECTEDMONTHLYCURRENCY) ? string.Empty : reader.GetString(FLD_EXPECTEDMONTHLYCURRENCY);
                    RejectCandi.ExpectedHourlyRate = reader.IsDBNull(FLD_EXPECTEDHOURLYRATE) ? 0 : reader.GetDecimal(FLD_EXPECTEDHOURLYRATE);
                    RejectCandi.ExpectedHourlyCurrencyLookupId = reader.IsDBNull(FLD_EXPECTEDHOURLYCURRENCYLOOKUPID) ? 0 : reader.GetInt32(FLD_EXPECTEDHOURLYCURRENCYLOOKUPID);
                    RejectCandi.ExpectedHourlyCurrency = reader.IsDBNull(FLD_EXPECTEDHOURLYCURRENCY) ? string.Empty : reader.GetString(FLD_EXPECTEDHOURLYCURRENCY);
                    RejectCandi.WillingToTravel = reader.IsDBNull(FLD_WILLINGTOTRAVEL) ? false : reader.GetBoolean(FLD_WILLINGTOTRAVEL);
                    RejectCandi.SalaryTypeLookupId = reader.IsDBNull(FLD_SALARYTYPELOOKUPID) ? 0 : reader.GetInt32(FLD_SALARYTYPELOOKUPID);
                    RejectCandi.SalaryType = reader.IsDBNull(FLD_SALARYTYPE) ? string.Empty : reader.GetString(FLD_SALARYTYPE);
                    RejectCandi.JobTypeLookupId = reader.IsDBNull(FLD_JOBTYPELOOKUPID) ? 0 : reader.GetInt32(FLD_JOBTYPELOOKUPID);
                    RejectCandi.JobType = reader.IsDBNull(FLD_JOBTYPE) ? string.Empty : reader.GetString(FLD_JOBTYPE);
                    RejectCandi.SecurityClearance = reader.IsDBNull(FLD_SECURITYCLEARANCE) ? false : reader.GetBoolean(FLD_SECURITYCLEARANCE);
                    RejectCandi.WorkAuthorizationLookupId = reader.IsDBNull(FLD_WORKAUTHORIZATIONLOOKUPID) ? 0 : reader.GetInt32(FLD_WORKAUTHORIZATIONLOOKUPID);
                    RejectCandi.WorkAuthorization = reader.IsDBNull(FLD_WORKAUTHORIZATION) ? string.Empty : reader.GetString(FLD_WORKAUTHORIZATION);
                    RejectCandi.CreatorName = reader.IsDBNull(FLD_CREATOR) ? string.Empty : reader.GetString(FLD_CREATOR);
                    RejectCandi.LastUpdatorName = reader.IsDBNull(FLD_UPDATOR) ? string.Empty : reader.GetString(FLD_UPDATOR);
                    RejectCandi.WorkScheduleLookupId = reader.IsDBNull(FLD_WORKSCEDULELOOKIPID) ? 0 : reader.GetInt32(FLD_WORKSCEDULELOOKIPID);
                    RejectCandi.WorkSchedule = reader.IsDBNull(FLD_WORKSCEDULE) ? string.Empty : reader.GetString(FLD_WORKSCEDULE);
                    RejectCandi.Website = reader.IsDBNull(FLD_WESITE) ? string.Empty : reader.GetString(FLD_WESITE);
                    RejectCandi.LinkedInProfile = reader.IsDBNull(FLD_LINKEDINPROFILE) ? string.Empty : reader.GetString(FLD_LINKEDINPROFILE);
                    RejectCandi.PassportStatus = reader.IsDBNull(FLD_PASSPORTSTATUS) ? false : reader.GetBoolean(FLD_PASSPORTSTATUS);
                    IDCard = reader.IsDBNull(FLD_IDCARD) ? string.Empty : reader.GetString(FLD_IDCARD);
                    IDCardDetail = reader.IsDBNull(FLD_IDCARDDETAIL) ? string.Empty : reader.GetString(FLD_IDCARDDETAIL);

                    if (IDCard != "")
                    {
                        RejectCandi.IDCard = IDCard;
                        if (IDCardDetail != "")
                            RejectCandi.IDCard += "-" + IDCardDetail;
                    }
                    else
                    {
                        RejectCandi.IDCard = "";
                    }
                    RejectCandi.HighestDegree = reader.IsDBNull(FLD_HIGHEREDUCATION) ? string.Empty : reader.GetString(FLD_HIGHEREDUCATION);
                    RejectCandi.Source = reader.IsDBNull(FLD_SOURCE) ? string.Empty : reader.GetString(FLD_SOURCE);
                    RejectCandi.SourceDescription = reader.IsDBNull(FLD_SOURCEDESCRIPTION) ? string.Empty : reader.GetString(FLD_SOURCEDESCRIPTION);

                    RejectCandi.NoticePeriod = reader.IsDBNull(FLD_NOTICEPERIOD) ? string.Empty : reader.GetString(FLD_NOTICEPERIOD);
                    RejectCandi.Department = reader.IsDBNull(FLD_DEPARTMENT) ? string.Empty : reader.GetString(FLD_DEPARTMENT);
                    RejectCandi.JobCode = reader.IsDBNull(FLD_JOBCODE) ? string.Empty : reader.GetString(FLD_JOBCODE);
                    RejectCandi.FeedbackStatus = reader.IsDBNull(FLD_FEEDBACKSTATUS) ? string.Empty : reader.GetString(FLD_FEEDBACKSTATUS);
                    RejectCandi.HiringManager = reader.IsDBNull(FLD_HIRINGMANAGER) ? string.Empty : reader.GetString(FLD_HIRINGMANAGER);
                    if (reader.FieldCount > FLD_RejectDetails)
                    {
                        RejectCandi.RejectDetails = reader.IsDBNull(FLD_RejectDetails) ? string.Empty : reader.GetString(FLD_RejectDetails);
                        RejectCandi.RejectedDate = reader.IsDBNull(FLD_RejectedDate) ? DateTime.MinValue : reader.GetDateTime(FLD_RejectedDate);
                        RejectCandi.JobTitle = reader.IsDBNull(FLD_JobTitle) ? string.Empty : reader.GetString(FLD_JobTitle);
                    }
                    
                }               
            }
            //**********************************END*******************************************************
            return RejectCandi;
        }       

        public IList<RejectCandidate> BuildEntitiesForRejectCandidateList(IDataReader reader)
        {
            List<RejectCandidate> RejectCandidate = new List<RejectCandidate>();

            while (reader.Read())
            {
                RejectCandidate.Add(BuildEntityForRejectCandidateList(reader));
            }

            return (RejectCandidate.Count > 0) ? RejectCandidate : null;
        }

        public RejectCandidate BuildEntityForRejectCandidateList(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_FIRSTNAME = 1;
            const int FLD_MIDDLENAME = 2;
            const int FLD_LASTNAME = 3;
            const int FLD_CELLPHONE = 4;
            const int FLD_PRIMARYEMAIL = 5;
            const int FLD_CREATEDATE = 6;
            const int FLD_REJECTORNAME = 7;
            const int FLD_REJECTDETAILS = 8;
            const int FLD_HIRINGMATRIXLEVEL = 9;

            RejectCandidate RejectCandi = new RejectCandidate();

            RejectCandi.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            RejectCandi.FirstName  = reader.IsDBNull(FLD_FIRSTNAME ) ? string .Empty  : reader.GetString (FLD_FIRSTNAME );
            RejectCandi.MiddleName  = reader.IsDBNull(FLD_MIDDLENAME ) ? string .Empty  : reader.GetString (FLD_MIDDLENAME );
            RejectCandi.LastName = reader.IsDBNull(FLD_LASTNAME ) ? string .Empty  : reader.GetString (FLD_LASTNAME );
            RejectCandi.CellPhone = reader.IsDBNull(FLD_CELLPHONE ) ? string .Empty  : reader.GetString (FLD_CELLPHONE );
            RejectCandi.PrimaryEmail  = reader.IsDBNull(FLD_PRIMARYEMAIL ) ? string .Empty  : reader.GetString (FLD_PRIMARYEMAIL );
            RejectCandi.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
            RejectCandi.RejectorName = reader.IsDBNull(FLD_REJECTORNAME ) ? string .Empty  : reader.GetString (FLD_REJECTORNAME );
            RejectCandi.RejectDetails = reader.IsDBNull(FLD_REJECTDETAILS) ? string.Empty : reader.GetString(FLD_REJECTDETAILS);
            RejectCandi.HiringMatrixLevel = reader.IsDBNull(FLD_HIRINGMATRIXLEVEL) ? 0 : reader.GetInt32(FLD_HIRINGMATRIXLEVEL);

            return RejectCandi;
        }
    }
}