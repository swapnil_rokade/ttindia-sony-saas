﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: SqlConstants.cs
    Description: 
    Created By: 
    Created On:
    Modification Log:
    --------------------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author                  MOdification
    ---------------------------------------------------------------------------------------------------------------------------------------------
    0.1              April-6-2009           Nagarathna V.B          Defect ID: 9545; created new constant "DB_STATUS_CODE_NO_DUPLICATE_DATA" 
   ---------------------------------------------------------------------------------------------------------------------------------------------
*/
namespace TPS360.DataAccess
{
    internal static class SqlConstants
    {
        public const string PRM_RETURN_CODE = "@ReturnCode";
        public const string PRM_ENTITY_ID = "@ID";
        public const string PRM_TOTAL_ROW = "@TotalRow";
        public const string PRM_TOTAL_PAGE = "@TotalPage";
        public const string PRM_COUNT = "@Count";

        public const int DB_STATUS_CODE_SUCCESS_ADD = 1001;
        public const int DB_STATUS_CODE_SUCCESS_UPDATE = 1002;
        public const int DB_STATUS_CODE_SUCCESS_DELETE = 1003;
        public const int DB_STATUS_CODE_SUCCESS_LOCK = 1004;

        public const int DB_STATUS_CODE_ERROR_DUPLICATE_DATA = 2003;
        public const int DB_STATUS_CODE_NO_DUPLICATE_DATA = 2005;//0.1
        public const int DB_STATUS_CODE_ERROR_CHILD_EXISTS = 2004;
        //public const int DB_STATUS_CODE_ERROR_CHILD_EXISTS = 2004;
    }
}