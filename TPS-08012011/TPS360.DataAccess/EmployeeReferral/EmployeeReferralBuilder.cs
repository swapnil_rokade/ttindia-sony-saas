﻿using System;
using System.Data;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    internal sealed class EmployeeReferralBuilder : IEntityBuilder<EmployeeReferral>
    {



        IList<EmployeeReferral> IEntityBuilder<EmployeeReferral>.BuildEntities(IDataReader reader)
        {
            List<EmployeeReferral> employeesref = new List<EmployeeReferral>();

            while (reader.Read())
            {
                employeesref.Add(((IEntityBuilder<EmployeeReferral>)this).BuildEntity(reader));
            }

            return (employeesref.Count > 0) ? employeesref : null;
        }




        EmployeeReferral IEntityBuilder<EmployeeReferral>.BuildEntity(IDataReader reader)
        {
            EmployeeReferral employeeRef = new EmployeeReferral();
            if (reader.FieldCount == 18)
            {
                const int FLD_ID = 0;
                const int FLD_MemberId = 1;
                const int FLD_Gid = 2;
                const int FLD_Grade = 3;
                const int FLD_DoJ = 4;
                const int FLD_Unit = 5;
                const int FLD_Designation = 6;
                const int FLD_ManagerID = 7;
                const int FLD_ManagerName = 8;
                const int FLD_OrgUnit = 9;
                const int FLD_OrgUnitLogText = 10;
                const int FLD_OrgUnitHeadGID = 11;
                const int FLD_DoL = 12;
                const int FLD_Reason = 13;
                const int FLD_DivisionName = 14;
                const int FLD_DivisionHeadGID = 15;
                const int FLD_DivisionHeadEmailID = 16;
                const int FLD_EmployeeType = 17;

                employeeRef.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
                employeeRef.MemberId = reader.IsDBNull(FLD_MemberId) ? 0 : reader.GetInt32(FLD_MemberId);
                employeeRef.Gid = reader.IsDBNull(FLD_Gid) ? string.Empty : reader.GetString(FLD_Gid);
                employeeRef.Grade = reader.IsDBNull(FLD_Grade) ? string.Empty : reader.GetString(FLD_Grade);
                employeeRef.DOJ = reader.IsDBNull(FLD_DoJ) ? DateTime.MinValue : reader.GetDateTime(FLD_DoJ);
                employeeRef.Unit = reader.IsDBNull(FLD_Unit) ? string.Empty : reader.GetString(FLD_Unit);
                employeeRef.Designation = reader.IsDBNull(FLD_Designation) ? string.Empty : reader.GetString(FLD_Designation);
                employeeRef.ManagerId = reader.IsDBNull(FLD_ManagerID) ? string.Empty : reader.GetString(FLD_ManagerID);
                employeeRef.ManagerName = reader.IsDBNull(FLD_ManagerName) ? string.Empty : reader.GetString(FLD_ManagerName);
                employeeRef.OrgUnit = reader.IsDBNull(FLD_OrgUnit) ? string.Empty : reader.GetString(FLD_OrgUnit);
                employeeRef.OrgUnitLogText = reader.IsDBNull(FLD_OrgUnitLogText) ? string.Empty : reader.GetString(FLD_OrgUnitLogText);
                employeeRef.OrgUnitHeadGid = reader.IsDBNull(FLD_OrgUnitHeadGID) ? string.Empty : reader.GetString(FLD_OrgUnitHeadGID);
                employeeRef.DOL = reader.IsDBNull(FLD_DoL) ? DateTime.MinValue : reader.GetDateTime(FLD_DoL);
                employeeRef.Reason = reader.IsDBNull(FLD_Reason) ? string.Empty : reader.GetString(FLD_Reason);
                employeeRef.DivisionName = reader.IsDBNull(FLD_DivisionName) ? string.Empty : reader.GetString(FLD_DivisionName);
                employeeRef.DivisionHeadGid = reader.IsDBNull(FLD_DivisionHeadGID) ? string.Empty : reader.GetString(FLD_DivisionHeadGID);
                employeeRef.DivisionHeadEmailId = reader.IsDBNull(FLD_DivisionHeadEmailID) ? string.Empty : reader.GetString(FLD_DivisionHeadEmailID);
                employeeRef.EmployeeType = reader.IsDBNull(FLD_EmployeeType) ? string.Empty : reader.GetString(FLD_EmployeeType);
            }
            else if (reader.FieldCount == 16)
            {
                const int FLD_ID = 0;
                const int FLD_PRIMARYEMAIL = 1;
                const int FLD_FIRSTNAME = 2;
                const int FLD_LASTNAME = 3;
                const int FLD_EMPLOYYEEID = 4;
                const int FLD_MEMBERID = 5;
                const int FLD_JOBPOSTINGID = 6;
                const int FLD_CREATORID = 7;
                const int FLD_UPDATORID = 8;
                const int FLD_CREATEDATE = 9;
                const int FLD_UPDATEDATE = 10;
                const int FLD_CANDIDATENAME = 11;
                const int FLD_JOBTITLE = 12;

                const int FLD_JOBPOSTINGCODE = 13;
                const int FLD_JOBSTATUS = 14;
                const int FLD_JOBSKILLLOOKUPID = 15;

                employeeRef.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
                employeeRef.RefererEmail = reader.IsDBNull(FLD_PRIMARYEMAIL) ? string.Empty : reader.GetString(FLD_PRIMARYEMAIL);
                employeeRef.RefererFirstName = reader.IsDBNull(FLD_FIRSTNAME) ? string.Empty : reader.GetString(FLD_FIRSTNAME);
                employeeRef.RefererLastName = reader.IsDBNull(FLD_LASTNAME) ? string.Empty : reader.GetString(FLD_LASTNAME);
                employeeRef.EmployeeId = reader.IsDBNull(FLD_EMPLOYYEEID) ? string.Empty : reader.GetString(FLD_EMPLOYYEEID);
                employeeRef.MemberId = reader.IsDBNull(FLD_MEMBERID) ? 0 : reader.GetInt32(FLD_MEMBERID);
                employeeRef.JobpostingId = reader.IsDBNull(FLD_JOBPOSTINGID) ? string.Empty : reader.GetString(FLD_JOBPOSTINGID);
                employeeRef.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
                employeeRef.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);
                employeeRef.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
                employeeRef.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);
                employeeRef.CandidateName = reader.IsDBNull(FLD_CANDIDATENAME) ? string.Empty : reader.GetString(FLD_CANDIDATENAME);
                employeeRef.JobTitle = reader.IsDBNull(FLD_JOBTITLE) ? string.Empty : reader.GetString(FLD_JOBTITLE);
                employeeRef.RequisitionID = reader.IsDBNull(FLD_JOBPOSTINGCODE) ? string.Empty : reader.GetString(FLD_JOBPOSTINGCODE);
                employeeRef.RequisitionStatus = reader.IsDBNull(FLD_JOBSTATUS) ? 0 : reader.GetInt32(FLD_JOBSTATUS);
                employeeRef.ReqPrimarySkills = reader.IsDBNull(FLD_JOBSKILLLOOKUPID) ? string.Empty : reader.GetString(FLD_JOBSKILLLOOKUPID);
            }
            else if (reader.FieldCount == 13)
            {
                const int FLD_ID = 0;
                const int FLD_PRIMARYEMAIL = 1;
                const int FLD_FIRSTNAME = 2;
                const int FLD_LASTNAME = 3;
                const int FLD_EMPLOYYEEID = 4;
                const int FLD_MEMBERID = 5;
                const int FLD_JOBPOSTINGID = 6;
                const int FLD_CREATORID = 7;
                const int FLD_UPDATORID = 8;
                const int FLD_CREATEDATE = 9;
                const int FLD_UPDATEDATE = 10;
                const int FLD_CANDIDATENAME = 11;
                const int FLD_JOBTITLE = 12;

                employeeRef.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
                employeeRef.RefererEmail = reader.IsDBNull(FLD_PRIMARYEMAIL) ? string.Empty : reader.GetString(FLD_PRIMARYEMAIL);
                employeeRef.RefererFirstName = reader.IsDBNull(FLD_FIRSTNAME) ? string.Empty : reader.GetString(FLD_FIRSTNAME);
                employeeRef.RefererLastName = reader.IsDBNull(FLD_LASTNAME) ? string.Empty : reader.GetString(FLD_LASTNAME);
                employeeRef.EmployeeId = reader.IsDBNull(FLD_EMPLOYYEEID) ? string.Empty : reader.GetString(FLD_EMPLOYYEEID);
                employeeRef.MemberId = reader.IsDBNull(FLD_MEMBERID) ? 0 : reader.GetInt32(FLD_MEMBERID);
                employeeRef.JobpostingId = reader.IsDBNull(FLD_JOBPOSTINGID) ? string.Empty : reader.GetString(FLD_JOBPOSTINGID);
                employeeRef.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
                employeeRef.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);
                employeeRef.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
                employeeRef.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);
                employeeRef.CandidateName = reader.IsDBNull(FLD_CANDIDATENAME) ? string.Empty : reader.GetString(FLD_CANDIDATENAME);
                employeeRef.JobTitle = reader.IsDBNull(FLD_JOBTITLE) ? string.Empty : reader.GetString(FLD_JOBTITLE);
            }
            else if (reader.FieldCount == 12) //----- Added by Kanchan yeware
            {
                const int FLD_PSNumber = 0;
                const int FLD_MemberId = 1;
                const int FLD_JobpostingId = 2;
                const int FLD_CREATORID = 3;
                const int FLD_UPDATORID = 4;
                const int FLD_CREATEDATE = 5;
                const int FLD_UPDATEDATE = 6;
                const int FLD_CANDIDATENAME = 7;             
                const int FLD_JobTitle = 8;
                const int FLD_CandiateStatus = 9;
                const int FLD_BU = 10;
                const int FLD_Location = 11;

                employeeRef.Gid = reader.IsDBNull(FLD_PSNumber) ? string.Empty : reader.GetString(FLD_PSNumber);
                employeeRef.MemberId = reader.IsDBNull(FLD_MemberId) ? 0 : reader.GetInt32(FLD_MemberId);
                employeeRef.JobpostingId = reader.IsDBNull(FLD_JobpostingId) ? string.Empty : reader.GetString(FLD_JobpostingId);
                employeeRef.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
                employeeRef.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);
                employeeRef.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
                employeeRef.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);
                employeeRef.CandidateName = reader.IsDBNull(FLD_CANDIDATENAME) ? string.Empty : reader.GetString(FLD_CANDIDATENAME);
                //employeeRef.FirstName = reader.IsDBNull(FLD_firstName) ? string.Empty : reader.GetString(FLD_firstName);
                employeeRef.JobTitle = reader.IsDBNull(FLD_JobTitle) ? string.Empty : reader.GetString(FLD_JobTitle);
                employeeRef.CandiateStatus = reader.IsDBNull(FLD_CandiateStatus) ? string.Empty : reader.GetString(FLD_CandiateStatus);
                employeeRef.BUName = reader.IsDBNull(FLD_BU) ? string.Empty : reader.GetString(FLD_BU);
                employeeRef.Location = reader.IsDBNull(FLD_Location) ? string.Empty : reader.GetString(FLD_Location);
            }
            return employeeRef;
        }
    }
}
