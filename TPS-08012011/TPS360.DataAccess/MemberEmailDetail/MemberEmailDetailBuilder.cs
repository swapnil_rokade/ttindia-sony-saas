﻿using System.Data;
using System.Collections.Generic;


using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    internal sealed class MemberEmailDetailBuilder : IEntityBuilder<MemberEmailDetail>
    {
        IList<MemberEmailDetail> IEntityBuilder<MemberEmailDetail>.BuildEntities(IDataReader reader)
        {
            List<MemberEmailDetail> memberEmailDetails = new List<MemberEmailDetail>();

            while (reader.Read())
            {
                memberEmailDetails.Add(((IEntityBuilder<MemberEmailDetail>)this).BuildEntity(reader));
            }

            return (memberEmailDetails.Count > 0) ? memberEmailDetails : null;
        }

        MemberEmailDetail IEntityBuilder<MemberEmailDetail>.BuildEntity(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_MEMBERID = 1;
            const int FLD_EMAILADDRESS = 2;
            const int FLD_ADDRESSTYPEID = 3;
            const int FLD_SENDINGTYPEID = 4;
            const int FLD_MEMBEREMAILID = 5;

            MemberEmailDetail memberEmailDetail = new MemberEmailDetail();

            memberEmailDetail.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            memberEmailDetail.MemberId = reader.IsDBNull(FLD_MEMBERID) ? 0 : reader.GetInt32(FLD_MEMBERID);
            memberEmailDetail.EmailAddress = reader.IsDBNull(FLD_EMAILADDRESS) ? string.Empty : reader.GetString(FLD_EMAILADDRESS);
            memberEmailDetail.AddressTypeId = reader.IsDBNull(FLD_ADDRESSTYPEID) ? 0 : reader.GetInt32(FLD_ADDRESSTYPEID);
            memberEmailDetail.SendingTypeId = reader.IsDBNull(FLD_SENDINGTYPEID) ? 0 : reader.GetInt32(FLD_SENDINGTYPEID);
            memberEmailDetail.MemberEmailId = reader.IsDBNull(FLD_MEMBEREMAILID) ? 0 : reader.GetInt32(FLD_MEMBEREMAILID);

            return memberEmailDetail;
        }
    }
}