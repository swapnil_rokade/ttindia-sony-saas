﻿using System;
using System.Data;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    internal sealed class MemberDetailBuilder : IEntityBuilder<MemberDetail>
    {
        IList<MemberDetail> IEntityBuilder<MemberDetail>.BuildEntities(IDataReader reader)
        {
            List<MemberDetail> memberDetails = new List<MemberDetail>();

            while (reader.Read())
            {
                memberDetails.Add(((IEntityBuilder<MemberDetail>)this).BuildEntity(reader));
            }

            return (memberDetails.Count > 0) ? memberDetails : null;
        }

        MemberDetail IEntityBuilder<MemberDetail>.BuildEntity(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_ISCURRENTSAMEASPRIMARYADDRESS = 1;
            const int FLD_CURRENTADDRESSLINE1 = 2;
            const int FLD_CURRENTADDRESSLINE2 = 3;
            const int FLD_CURRENTCITY = 4;
            const int FLD_CURRENTSTATEID = 5;
            const int FLD_CURRENTZIP = 6;
            const int FLD_CURRENTCOUNTRYID = 7;
            const int FLD_HOMEPHONE = 8;
            const int FLD_OFFICEPHONE = 9;
            const int FLD_OFFICEPHONEEXTENSION = 10;
            const int FLD_FAX = 11;
            const int FLD_CITYOFBIRTH = 12;
            const int FLD_PROVINCEOFBIRTH = 13;
            const int FLD_STATEIDOFBIRTH = 14;
            const int FLD_COUNTRYIDOFBIRTH = 15;
            const int FLD_COUNTRYIDOFCITIZENSHIP = 16;
            const int FLD_SSNPAN = 17;
            const int FLD_BIRTHMARK = 18;
            const int FLD_GENDERLOOKUPID = 19;
            const int FLD_ETHNICGROUPLOOKUPID = 20;
            const int FLD_PHOTO = 21;
            const int FLD_BLOODGROUPLOOKUPID = 22;
            const int FLD_HEIGHT = 23;
            const int FLD_WEIGHT = 24;
            const int FLD_EMERGENCYCONTACTPERSON = 25;
            const int FLD_EMERGENCYCONTACTNUMBER = 26;
            const int FLD_EMERGENCYCONTACTRELATION = 27;
            const int FLD_FATHERNAME = 28;
            const int FLD_MOTHERNAME = 29;
            const int FLD_MARITALSTATUSLOOKUPID = 30;
            const int FLD_SPOUSENAME = 31;
            const int FLD_ANNIVERSARYDATE = 32;
            const int FLD_NUMBEROFCHILDREN = 33;
            const int FLD_DISABILITYINFORMATION = 34;
            const int FLD_ISREMOVED = 35;
            const int FLD_MEMBERID = 36;
            const int FLD_CREATORID = 37;
            const int FLD_UPDATORID = 38;
            const int FLD_CREATEDATE = 39;
            const int FLD_UPDATEDATE = 40;

            const int FLD_CANDIDATEINDUSTRYLOOKUPID = 41;
            const int FLD_JOBFUNCTIONLOOKUPID = 42;

            MemberDetail memberDetail = new MemberDetail();

            memberDetail.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            memberDetail.IsCurrentSameAsPrimaryAddress = reader.IsDBNull(FLD_ISCURRENTSAMEASPRIMARYADDRESS) ? false : reader.GetBoolean(FLD_ISCURRENTSAMEASPRIMARYADDRESS);
            memberDetail.CurrentAddressLine1 = reader.IsDBNull(FLD_CURRENTADDRESSLINE1) ? string.Empty : reader.GetString(FLD_CURRENTADDRESSLINE1);
            memberDetail.CurrentAddressLine2 = reader.IsDBNull(FLD_CURRENTADDRESSLINE2) ? string.Empty : reader.GetString(FLD_CURRENTADDRESSLINE2);
            memberDetail.CurrentCity = reader.IsDBNull(FLD_CURRENTCITY) ? string.Empty : reader.GetString(FLD_CURRENTCITY);
            memberDetail.CurrentStateId = reader.IsDBNull(FLD_CURRENTSTATEID) ? 0 : reader.GetInt32(FLD_CURRENTSTATEID);
            memberDetail.CurrentZip = reader.IsDBNull(FLD_CURRENTZIP) ? string.Empty : reader.GetString(FLD_CURRENTZIP);
            memberDetail.CurrentCountryId = reader.IsDBNull(FLD_CURRENTCOUNTRYID) ? 0 : reader.GetInt32(FLD_CURRENTCOUNTRYID);
            memberDetail.HomePhone = reader.IsDBNull(FLD_HOMEPHONE) ? string.Empty : reader.GetString(FLD_HOMEPHONE);
            memberDetail.OfficePhone = reader.IsDBNull(FLD_OFFICEPHONE) ? string.Empty : reader.GetString(FLD_OFFICEPHONE);
            memberDetail.OfficePhoneExtension = reader.IsDBNull(FLD_OFFICEPHONEEXTENSION) ? string.Empty : reader.GetString(FLD_OFFICEPHONEEXTENSION);
            memberDetail.Fax = reader.IsDBNull(FLD_FAX) ? string.Empty : reader.GetString(FLD_FAX);
            memberDetail.CityOfBirth = reader.IsDBNull(FLD_CITYOFBIRTH) ? string.Empty : reader.GetString(FLD_CITYOFBIRTH);
            memberDetail.ProvinceOfBirth = reader.IsDBNull(FLD_PROVINCEOFBIRTH) ? string.Empty : reader.GetString(FLD_PROVINCEOFBIRTH);
            memberDetail.StateIdOfBirth = reader.IsDBNull(FLD_STATEIDOFBIRTH) ? string.Empty : reader.GetString(FLD_STATEIDOFBIRTH);
            memberDetail.CountryIdOfBirth = reader.IsDBNull(FLD_COUNTRYIDOFBIRTH) ? string.Empty : reader.GetString(FLD_COUNTRYIDOFBIRTH);
            memberDetail.CountryIdOfCitizenship = reader.IsDBNull(FLD_COUNTRYIDOFCITIZENSHIP) ? string.Empty : reader.GetString(FLD_COUNTRYIDOFCITIZENSHIP);
            memberDetail.SSNPAN = reader.IsDBNull(FLD_SSNPAN) ? string.Empty : reader.GetString(FLD_SSNPAN);
            memberDetail.BirthMark = reader.IsDBNull(FLD_BIRTHMARK) ? string.Empty : reader.GetString(FLD_BIRTHMARK);
            memberDetail.GenderLookupId = reader.IsDBNull(FLD_GENDERLOOKUPID) ? 0 : reader.GetInt32(FLD_GENDERLOOKUPID);
            memberDetail.EthnicGroupLookupId = reader.IsDBNull(FLD_ETHNICGROUPLOOKUPID) ? 0 : reader.GetInt32(FLD_ETHNICGROUPLOOKUPID);
            memberDetail.Photo = reader.IsDBNull(FLD_PHOTO) ? string.Empty : reader.GetString(FLD_PHOTO);
            memberDetail.BloodGroupLookupId = reader.IsDBNull(FLD_BLOODGROUPLOOKUPID) ? 0 : reader.GetInt32(FLD_BLOODGROUPLOOKUPID);
            memberDetail.Height = reader.IsDBNull(FLD_HEIGHT) ? string.Empty : reader.GetString(FLD_HEIGHT);
            memberDetail.Weight = reader.IsDBNull(FLD_WEIGHT) ? string.Empty : reader.GetString(FLD_WEIGHT);
            memberDetail.EmergencyContactPerson = reader.IsDBNull(FLD_EMERGENCYCONTACTPERSON) ? string.Empty : reader.GetString(FLD_EMERGENCYCONTACTPERSON);
            memberDetail.EmergencyContactNumber = reader.IsDBNull(FLD_EMERGENCYCONTACTNUMBER) ? string.Empty : reader.GetString(FLD_EMERGENCYCONTACTNUMBER);
            memberDetail.EmergencyContactRelation = reader.IsDBNull(FLD_EMERGENCYCONTACTRELATION) ? string.Empty : reader.GetString(FLD_EMERGENCYCONTACTRELATION);
            memberDetail.FatherName = reader.IsDBNull(FLD_FATHERNAME) ? string.Empty : reader.GetString(FLD_FATHERNAME);
            memberDetail.MotherName = reader.IsDBNull(FLD_MOTHERNAME) ? string.Empty : reader.GetString(FLD_MOTHERNAME);
            memberDetail.MaritalStatusLookupId = reader.IsDBNull(FLD_MARITALSTATUSLOOKUPID) ? 0 : reader.GetInt32(FLD_MARITALSTATUSLOOKUPID);
            memberDetail.SpouseName = reader.IsDBNull(FLD_SPOUSENAME) ? string.Empty : reader.GetString(FLD_SPOUSENAME);
            memberDetail.AnniversaryDate = reader.IsDBNull(FLD_ANNIVERSARYDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_ANNIVERSARYDATE);
            memberDetail.NumberOfChildren = reader.IsDBNull(FLD_NUMBEROFCHILDREN) ? 0 : reader.GetInt32(FLD_NUMBEROFCHILDREN);
            memberDetail.DisabilityInformation = reader.IsDBNull(FLD_DISABILITYINFORMATION) ? string.Empty : reader.GetString(FLD_DISABILITYINFORMATION);
            memberDetail.IsRemoved = reader.IsDBNull(FLD_ISREMOVED) ? false : reader.GetBoolean(FLD_ISREMOVED);
            memberDetail.MemberId = reader.IsDBNull(FLD_MEMBERID) ? 0 : reader.GetInt32(FLD_MEMBERID);
            memberDetail.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
            memberDetail.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);
            memberDetail.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
            memberDetail.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);

            memberDetail.CandidateIndustryLookupID = reader.IsDBNull(FLD_CANDIDATEINDUSTRYLOOKUPID) ? 0 : reader.GetInt32(FLD_CANDIDATEINDUSTRYLOOKUPID);
            memberDetail.JobFunctionLookupID = reader.IsDBNull(FLD_JOBFUNCTIONLOOKUPID) ? 0 : reader.GetInt32(FLD_JOBFUNCTIONLOOKUPID);

            return memberDetail;
        }
    }
}