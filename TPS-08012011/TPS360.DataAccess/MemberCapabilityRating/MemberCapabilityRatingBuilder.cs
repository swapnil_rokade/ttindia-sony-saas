﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    internal sealed class MemberCapabilityRatingBuilder : IEntityBuilder<MemberCapabilityRating>
    {
        IList<MemberCapabilityRating> IEntityBuilder<MemberCapabilityRating>.BuildEntities(IDataReader reader)
        {
            List<MemberCapabilityRating> memberCapabilityRatings = new List<MemberCapabilityRating>();

            while (reader.Read())
            {
                memberCapabilityRatings.Add(((IEntityBuilder<MemberCapabilityRating>)this).BuildEntity(reader));
            }

            return (memberCapabilityRatings.Count > 0) ? memberCapabilityRatings : null;
        }

        MemberCapabilityRating IEntityBuilder<MemberCapabilityRating>.BuildEntity(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_MEMBERID = 1;
            const int FLD_POSITIONID = 2;
            const int FLD_CAPABILITYTYPE = 3;
            const int FLD_POSITIONCAPABILITYMAPID = 4;
            const int FLD_RATING = 5;
            const int FLD_ISREMOVED = 6;
            const int FLD_CREATORID = 7;
            const int FLD_UPDATORID = 8;
            const int FLD_CREATEDATE = 9;
            const int FLD_UPDATEDATE = 10;

            MemberCapabilityRating memberCapabilityRating = new MemberCapabilityRating();

            memberCapabilityRating.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            memberCapabilityRating.MemberId = reader.IsDBNull(FLD_MEMBERID) ? 0 : reader.GetInt32(FLD_MEMBERID);
            memberCapabilityRating.PositionId = reader.IsDBNull(FLD_POSITIONID) ? 0 : reader.GetInt32(FLD_POSITIONID);
            memberCapabilityRating.CapabilityType = reader.IsDBNull(FLD_CAPABILITYTYPE) ? 0 : reader.GetInt32(FLD_CAPABILITYTYPE);
            memberCapabilityRating.PositionCapabilityMapId = reader.IsDBNull(FLD_POSITIONCAPABILITYMAPID) ? 0 : reader.GetInt32(FLD_POSITIONCAPABILITYMAPID);
            memberCapabilityRating.Rating = reader.IsDBNull(FLD_RATING) ? 0 : reader.GetInt32(FLD_RATING);
            memberCapabilityRating.IsRemoved = reader.IsDBNull(FLD_ISREMOVED) ? false : reader.GetBoolean(FLD_ISREMOVED);
            memberCapabilityRating.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
            memberCapabilityRating.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);
            memberCapabilityRating.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
            memberCapabilityRating.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);

            return memberCapabilityRating;
        }
    }
}