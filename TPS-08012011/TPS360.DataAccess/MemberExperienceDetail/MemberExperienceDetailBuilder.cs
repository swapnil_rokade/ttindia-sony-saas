﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName:MemberExperienceDetailBuilder.cs
    Description: This page is used to build entity for MemberExperience Details.
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.         Date                Author               Modification
    ------------------------------------------------------------------------------------------------------------------------------
     0.1            July-7-2009         Gopala Swamy J       Defect #10847; Added new constants FLD_STATEID
    ------------------------------------------------------------------------------------------------------------------------------
*/

using System;
using System.Data;
using System.Collections.Generic;


using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    internal sealed class MemberExperienceDetailBuilder : IEntityBuilder<MemberExperienceDetail>
    {
        IList<MemberExperienceDetail> IEntityBuilder<MemberExperienceDetail>.BuildEntities(IDataReader reader)
        {
            List<MemberExperienceDetail> memberExperienceDetails = new List<MemberExperienceDetail>();

            while (reader.Read())
            {
                memberExperienceDetails.Add(((IEntityBuilder<MemberExperienceDetail>)this).BuildEntity(reader));
            }

            return (memberExperienceDetails.Count > 0) ? memberExperienceDetails : null;
        }


       


        MemberExperienceDetail IEntityBuilder<MemberExperienceDetail>.BuildEntity(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_COMPANYNAME = 1;
            const int FLD_POSITIONNAME = 2;
            const int FLD_CITY = 3;
            const int FLD_DATEFROM = 4;
            const int FLD_DATETO = 5;
            const int FLD_ISTILLDATE = 6;
            const int FLD_RESPONSIBILITIES = 7;
            const int FLD_ISREMOVED = 8;
            const int FLD_COUNTRYLOOKUPID = 9;
            const int FLD_COUNTRY = 10;
            const int FLD_INDUSTRYCATEGORYLOOKUPID = 11;
            const int FLD_INDUSTRYCATEGORY = 12;
            const int FLD_FUNCTIONALCATEGORYLOOKUPID = 13;
            const int FLD_FUNCTIONALCATEGORY = 14;
            const int FLD_MEMBERID = 15;
            const int FLD_CREATORID = 16;
            const int FLD_UPDATORID = 17;
            const int FLD_CREATEDATE = 18;
            const int FLD_UPDATEDATE = 19;
            const int FLD_STATEID=20; //0.1

            MemberExperienceDetail memberExperienceDetail = new MemberExperienceDetail();

            memberExperienceDetail.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            memberExperienceDetail.CompanyName = reader.IsDBNull(FLD_COMPANYNAME) ? string.Empty : reader.GetString(FLD_COMPANYNAME);
            memberExperienceDetail.PositionName = reader.IsDBNull(FLD_POSITIONNAME) ? string.Empty : reader.GetString(FLD_POSITIONNAME);
            memberExperienceDetail.City = reader.IsDBNull(FLD_CITY) ? string.Empty : reader.GetString(FLD_CITY);
            memberExperienceDetail.DateFrom = reader.IsDBNull(FLD_DATEFROM) ? DateTime.MinValue : reader.GetDateTime(FLD_DATEFROM);
            memberExperienceDetail.DateTo = reader.IsDBNull(FLD_DATETO) ? DateTime.MinValue : reader.GetDateTime(FLD_DATETO);
            memberExperienceDetail.IsTillDate = reader.IsDBNull(FLD_ISTILLDATE) ? false : reader.GetBoolean(FLD_ISTILLDATE);
            memberExperienceDetail.Responsibilities = reader.IsDBNull(FLD_RESPONSIBILITIES) ? string.Empty : reader.GetString(FLD_RESPONSIBILITIES);
            memberExperienceDetail.IsRemoved = reader.IsDBNull(FLD_ISREMOVED) ? false : reader.GetBoolean(FLD_ISREMOVED);
            memberExperienceDetail.CountryLookupId = reader.IsDBNull(FLD_COUNTRYLOOKUPID) ? 0 : reader.GetInt32(FLD_COUNTRYLOOKUPID);
            memberExperienceDetail.Country = reader.IsDBNull(FLD_COUNTRY) ? string.Empty : reader.GetString(FLD_COUNTRY);
            memberExperienceDetail.IndustryCategoryLookupId = reader.IsDBNull(FLD_INDUSTRYCATEGORYLOOKUPID) ? 0 : reader.GetInt32(FLD_INDUSTRYCATEGORYLOOKUPID);
            memberExperienceDetail.IndustryCategory = reader.IsDBNull(FLD_INDUSTRYCATEGORY) ? string.Empty : reader.GetString(FLD_INDUSTRYCATEGORY);
            memberExperienceDetail.FunctionalCategoryLookupId = reader.IsDBNull(FLD_FUNCTIONALCATEGORYLOOKUPID) ? 0 : reader.GetInt32(FLD_FUNCTIONALCATEGORYLOOKUPID);
            memberExperienceDetail.FunctionalCategory = reader.IsDBNull(FLD_FUNCTIONALCATEGORY) ? string.Empty : reader.GetString(FLD_FUNCTIONALCATEGORY);
            memberExperienceDetail.MemberId = reader.IsDBNull(FLD_MEMBERID) ? 0 : reader.GetInt32(FLD_MEMBERID);
            memberExperienceDetail.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
            memberExperienceDetail.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);
            memberExperienceDetail.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
            memberExperienceDetail.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);

            //0.1 starts here
            if (reader.FieldCount > FLD_STATEID)
            {
                memberExperienceDetail.StateId = reader.IsDBNull(FLD_STATEID) ? 0 : reader.GetInt32(FLD_STATEID);

            }
            //0.1 end here

            return memberExperienceDetail;
        }     
          
        
    }
}