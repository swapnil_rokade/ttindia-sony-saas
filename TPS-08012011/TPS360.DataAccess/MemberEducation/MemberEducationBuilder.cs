﻿using System;
using System.Data;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    internal sealed class MemberEducationBuilder : IEntityBuilder<MemberEducation>
    {
        IList<MemberEducation> IEntityBuilder<MemberEducation>.BuildEntities(IDataReader reader)
        {
            List<MemberEducation> memberEducations = new List<MemberEducation>();

            while (reader.Read())
            {
                memberEducations.Add(((IEntityBuilder<MemberEducation>)this).BuildEntity(reader));
            }

            return (memberEducations.Count > 0) ? memberEducations : null;
        }

        MemberEducation IEntityBuilder<MemberEducation>.BuildEntity(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_LEVELOFEDUCATIONLOOKUPID = 1;
            const int FLD_DEGREETITLE = 2;
            const int FLD_FIELDOFSTUDYLOOKUPID = 3;
            const int FLD_MAJORSUBJECTS = 4;
            const int FLD_GPA = 5;
            const int FLD_GPAOUTOF = 6;
            const int FLD_ATTENDEDFROM = 7;
            const int FLD_ATTENDEDTO = 8;
            const int FLD_DEGREEOBTAINEDYEAR = 9;
            const int FLD_INSTITUTENAME = 10;
            const int FLD_INSTITUTECITY = 11;
            const int FLD_INSTITUTECOUNTRYID = 12;
            const int FLD_BRIEFDESCRIPTION = 13;
            const int FLD_ISHIGHESTEDUCATION = 14;
            const int FLD_ISREMOVED = 15;
            const int FLD_MEMBERID = 16;
            const int FLD_CREATORID = 17;
            const int FLD_UPDATORID = 18;
            const int FLD_CREATEDATE = 19;
            const int FLD_UPDATEDATE = 20;
            const int FLD_STATEID = 21; //11065
            MemberEducation memberEducation = new MemberEducation();

            memberEducation.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            memberEducation.LevelOfEducationLookupId = reader.IsDBNull(FLD_LEVELOFEDUCATIONLOOKUPID) ? 0 : reader.GetInt32(FLD_LEVELOFEDUCATIONLOOKUPID);
            memberEducation.DegreeTitle = reader.IsDBNull(FLD_DEGREETITLE) ? string.Empty : reader.GetString(FLD_DEGREETITLE);
            memberEducation.FieldOfStudyLookupId = reader.IsDBNull(FLD_FIELDOFSTUDYLOOKUPID) ? 0 : reader.GetInt32(FLD_FIELDOFSTUDYLOOKUPID);
            memberEducation.MajorSubjects = reader.IsDBNull(FLD_MAJORSUBJECTS) ? string.Empty : reader.GetString(FLD_MAJORSUBJECTS);
            memberEducation.GPA = reader.IsDBNull(FLD_GPA) ? string.Empty : reader.GetString(FLD_GPA);
            memberEducation.GpaOutOf = reader.IsDBNull(FLD_GPAOUTOF) ? string.Empty : reader.GetString(FLD_GPAOUTOF);
            memberEducation.AttendedFrom = reader.IsDBNull(FLD_ATTENDEDFROM) ? DateTime.MinValue : reader.GetDateTime(FLD_ATTENDEDFROM);
            memberEducation.AttendedTo = reader.IsDBNull(FLD_ATTENDEDTO) ? DateTime.MinValue : reader.GetDateTime(FLD_ATTENDEDTO);
            memberEducation.DegreeObtainedYear = reader.IsDBNull(FLD_DEGREEOBTAINEDYEAR) ? string.Empty : reader.GetString(FLD_DEGREEOBTAINEDYEAR);
            memberEducation.InstituteName = reader.IsDBNull(FLD_INSTITUTENAME) ? string.Empty : reader.GetString(FLD_INSTITUTENAME);
            memberEducation.InstituteCity = reader.IsDBNull(FLD_INSTITUTECITY) ? string.Empty : reader.GetString(FLD_INSTITUTECITY);
            memberEducation.InstituteCountryId = reader.IsDBNull(FLD_INSTITUTECOUNTRYID) ? 0 : reader.GetInt32(FLD_INSTITUTECOUNTRYID);
            memberEducation.BriefDescription = reader.IsDBNull(FLD_BRIEFDESCRIPTION) ? string.Empty : reader.GetString(FLD_BRIEFDESCRIPTION);
            memberEducation.IsHighestEducation = reader.IsDBNull(FLD_ISHIGHESTEDUCATION) ? false : reader.GetBoolean(FLD_ISHIGHESTEDUCATION);
            memberEducation.IsRemoved = reader.IsDBNull(FLD_ISREMOVED) ? false : reader.GetBoolean(FLD_ISREMOVED);
            memberEducation.MemberId = reader.IsDBNull(FLD_MEMBERID) ? 0 : reader.GetInt32(FLD_MEMBERID);
            memberEducation.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
            memberEducation.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);
            memberEducation.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
            memberEducation.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);
            //11065 starts here 
            if (reader.FieldCount > FLD_STATEID)
            {
                memberEducation.StateId = reader.IsDBNull(FLD_STATEID) ? 0 : reader.GetInt32(FLD_STATEID);
            }
            //11065 end here

            return memberEducation;
        }
    }
}