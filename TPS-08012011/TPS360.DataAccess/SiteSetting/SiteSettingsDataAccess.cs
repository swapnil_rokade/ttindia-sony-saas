﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    internal sealed class SiteSettingDataAccess : BaseDataAccess, ISiteSettingDataAccess
    {
        #region Constructors

        public SiteSettingDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<SiteSetting> CreateEntityBuilder<SiteSetting>()
        {
            return (new SiteSettingBuilder()) as IEntityBuilder<SiteSetting>;
        }

        #endregion

        #region  Methods

        SiteSetting ISiteSettingDataAccess.Add(SiteSetting siteSetting)
        {
            const string SP = "dbo.SiteSetting_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@SettingType", DbType.Int32, siteSetting.SettingType);
                Database.AddInParameter(cmd, "@SettingConfiguration", DbType.Binary, siteSetting.SettingConfiguration);
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, siteSetting.IsRemoved);
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, siteSetting.CreatorId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        siteSetting = CreateEntityBuilder<SiteSetting>().BuildEntity(reader);
                    }
                    else
                    {
                        siteSetting = null;
                    }
                }

                if (siteSetting == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("SiteSetting already exists. Please specify another siteSetting.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while creating this siteSetting.");
                            }
                    }
                }

                return siteSetting;
            }
        }

        SiteSetting ISiteSettingDataAccess.Update(SiteSetting siteSetting)
        {
            const string SP = "dbo.SiteSetting_Update";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, siteSetting.Id);
                Database.AddInParameter(cmd, "@SettingType", DbType.Int32, siteSetting.SettingType);
                Database.AddInParameter(cmd, "@SettingConfiguration", DbType.Binary, siteSetting.SettingConfiguration);
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, siteSetting.IsRemoved);
                Database.AddInParameter(cmd, "@UpdatorId", DbType.Int32, siteSetting.UpdatorId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        siteSetting = CreateEntityBuilder<SiteSetting>().BuildEntity(reader);
                    }
                    else
                    {
                        siteSetting = null;
                    }
                }

                if (siteSetting == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("SiteSetting already exists. Please specify another siteSetting.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while updating this siteSetting.");
                            }
                    }
                }

                return siteSetting;
            }
        }

        SiteSetting ISiteSettingDataAccess.GetById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.SiteSetting_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<SiteSetting>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        IList<SiteSetting> ISiteSettingDataAccess.GetAll()
        {
            const string SP = "dbo.SiteSetting_GetAll";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<SiteSetting>().BuildEntities(reader);
                }
            }
        }

        bool ISiteSettingDataAccess.DeleteById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.SiteSetting_DeleteById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a siteSetting which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this siteSetting.");
                        }
                }
            }
        }

        SiteSetting ISiteSettingDataAccess.GetBySettingType(int settingType)
        {
            if (settingType < 1)
            {
                throw new ArgumentNullException("settingType");
            }

            const string SP = "dbo.SiteSetting_GetBySettingType";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@SettingType", DbType.Int32, settingType);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<SiteSetting>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        #endregion
    }
}