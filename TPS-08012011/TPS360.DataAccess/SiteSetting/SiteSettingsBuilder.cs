﻿using System;
using System.Data;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    internal sealed class SiteSettingBuilder : IEntityBuilder<SiteSetting>
    {
        IList<SiteSetting> IEntityBuilder<SiteSetting>.BuildEntities(IDataReader reader)
        {
            List<SiteSetting> siteSettings = new List<SiteSetting>();

            while (reader.Read())
            {
                siteSettings.Add(((IEntityBuilder<SiteSetting>)this).BuildEntity(reader));
            }

            return (siteSettings.Count > 0) ? siteSettings : null;
        }

        SiteSetting IEntityBuilder<SiteSetting>.BuildEntity(IDataReader reader)
		{
				const int FLD_ID = 0;
				const int FLD_SETTINGTYPE = 1;
				const int FLD_SETTINGCONFIGURATION = 2;
				const int FLD_ISREMOVED = 3;
				const int FLD_CREATORID = 4;
				const int FLD_UPDATORID = 5;
				const int FLD_CREATEDATE = 6;
				const int FLD_UPDATEDATE = 7;

            SiteSetting siteSetting = new SiteSetting();

			siteSetting.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
			siteSetting.SettingType = reader.IsDBNull(FLD_SETTINGTYPE) ? 0 : reader.GetInt32(FLD_SETTINGTYPE);
			siteSetting.SettingConfiguration = (byte[])(reader.GetValue(FLD_SETTINGCONFIGURATION));
			siteSetting.IsRemoved = reader.IsDBNull(FLD_ISREMOVED) ? false : reader.GetBoolean(FLD_ISREMOVED);
			siteSetting.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
			siteSetting.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);
			siteSetting.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
			siteSetting.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);
		
            return siteSetting;
		}
    }
}