﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: GenericLookupDataAccess.cs
    Description:  
    Created By:  
    Created On:  
    Modification Log:
    --------------------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
 *  1.0                 5/Dec/2015          Prasanth Kumar G    Introduced Function InterviewAssessment_GetBy_InterviewId_InterviewerEmail
    ---------------------------------------------------------------------------------------------------------------------------------------------
    
*/

//-----------------------------------------Rishi Code Start-------------------------------------//
using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Helper;

namespace TPS360.DataAccess
{
    internal sealed class OrganizationUnitDataAccess : BaseDataAccess, IOrganizationUnitDataAccess
    {
        #region Constructors

        public OrganizationUnitDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<OrganizationUnit> CreateEntityBuilder<OrganizationUnit>()
        {
            return (new OrganizationUnitBuilder()) as IEntityBuilder<OrganizationUnit>;
        }

        #endregion

        #region  Methods

        IList<TPS360.Common.BusinessEntities.OrganizationUnit> IOrganizationUnitDataAccess.GetAll(int active)
        {
            const string SP = "dbo.OrganizationUnit_GetAll";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@active", DbType.Int32, active);
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<TPS360.Common.BusinessEntities.OrganizationUnit>().BuildEntities(reader);
                }
            }
        }
        IList<TPS360.Common.BusinessEntities.OrganizationUnit> IOrganizationUnitDataAccess.GetDivisionsCode(string parameter, int querySequence)
        {
            const string SP = "dbo.OrganizationUnit_GetDivisionsCode";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@parameter", DbType.String, parameter);
                Database.AddInParameter(cmd, "@querySequence", DbType.Int32, querySequence);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {

                    return CreateEntityBuilder<TPS360.Common.BusinessEntities.OrganizationUnit>().BuildEntities(reader);

                }
            }
        }

        IList<TPS360.Common.BusinessEntities.OrganizationUnit> IOrganizationUnitDataAccess.GetDivisionsCodeByMemberId(string parameter, int querySequence, int CurrentMemberId)
        {
            const string SP = "dbo.OrganizationUnit_GetDivisionsCodeByCurrentMemberId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@parameter", DbType.String, parameter);
                Database.AddInParameter(cmd, "@querySequence", DbType.Int32, querySequence);
                Database.AddInParameter(cmd, "@CurrentMemberId", DbType.Int32, CurrentMemberId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {

                    return CreateEntityBuilder<TPS360.Common.BusinessEntities.OrganizationUnit>().BuildEntities(reader);

                }
            }
        }

        TPS360.Common.BusinessEntities.OrganizationUnit IOrganizationUnitDataAccess.GetById(string orgId)
        {
            const string SP = "dbo.OrganizationUnit_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@OrgUnitCode", DbType.String, Convert.ToString(orgId));

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<TPS360.Common.BusinessEntities.OrganizationUnit>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }
        TPS360.Common.BusinessEntities.OrganizationUnit IOrganizationUnitDataAccess.GetOrgApprovalMatrixByOrgCode(string OrgCode)
        {
            const string SP = "dbo.OrgApprovalMatrix_GetByOrgCode";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@OrgCode", DbType.String, Convert.ToString(OrgCode));

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<TPS360.Common.BusinessEntities.OrganizationUnit>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }
        OrganizationUnit IOrganizationUnitDataAccess.GetEmployeeMemberMappingByGId(string GId)
        {
            const string SP = "dbo.EmployeeMemberMapping_GetByGId";
            OrganizationUnit OrgUnit = new OrganizationUnit(); ;
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@GId", DbType.String, Convert.ToString(GId));

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        OrgUnit.Id = reader.IsDBNull(0) ? 0 : reader.GetInt32(0);
                        OrgUnit.MemberId = reader.IsDBNull(1) ? 0 : reader.GetInt32(1);
                        OrgUnit.HeadGID = reader.IsDBNull(2) ? string.Empty : reader.GetString(2);
                        OrgUnit.OrgUnitCode = reader.IsDBNull(3) ? string.Empty : reader.GetString(3);
                        return OrgUnit;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        #endregion
    }
}
//-----------------------------------------Rishi Code End-------------------------------------//