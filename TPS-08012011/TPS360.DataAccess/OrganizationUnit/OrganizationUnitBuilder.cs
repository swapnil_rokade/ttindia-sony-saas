﻿//--------------------------------Rishi Code Start-------------------------------------------------//
using System;
using System.Data;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    internal sealed class OrganizationUnitBuilder : IEntityBuilder<OrganizationUnit>
    {
        IList<OrganizationUnit> IEntityBuilder<OrganizationUnit>.BuildEntities(IDataReader reader)
        {
            List<OrganizationUnit> organizationUnit = new List<OrganizationUnit>();

            while (reader.Read())
            {
                organizationUnit.Add(((IEntityBuilder<OrganizationUnit>)this).BuildEntity(reader));
            }

            return (organizationUnit.Count > 0) ? organizationUnit : null;
        }

        OrganizationUnit IEntityBuilder<OrganizationUnit>.BuildEntity(IDataReader reader)
        {
            const int FLD_DIVISION = 0; // Reporting Org Lng Txt
            const int FLD_ORGNAME = 1;
            const int FLD_ORGUNITCODE = 2;
            const int FLD_ORGSHORTTEXT = 3;
            const int FLD_ORGHEADNAME = 4;

            OrganizationUnit organizationUnit = new OrganizationUnit();

            if (reader.FieldCount == 3)
            {
                const int FLD_HeadGID = 0;
                const int FLD_Id = 1;
                const int FLD_MemberName = 2;
                organizationUnit.HeadGID = reader.IsDBNull(FLD_HeadGID) ? string.Empty : reader.GetString(FLD_HeadGID);
                organizationUnit.Id = reader.IsDBNull(FLD_Id) ? 0 : Convert.ToInt32(reader.GetValue(FLD_Id));
                organizationUnit.HeadName = reader.IsDBNull(FLD_MemberName) ? string.Empty : reader.GetString(FLD_MemberName);
                return organizationUnit;
            }
            else if (reader.FieldCount > 2)
            {
                organizationUnit.ReportingOrgLngTxt = reader.IsDBNull(FLD_DIVISION) ? string.Empty : reader.GetString(FLD_DIVISION);
                organizationUnit.OrgName = reader.IsDBNull(FLD_ORGNAME) ? string.Empty : reader.GetString(FLD_ORGNAME);
                organizationUnit.OrgUnitCode = reader.IsDBNull(FLD_ORGUNITCODE) ? string.Empty : Convert.ToString(reader.GetString(FLD_ORGUNITCODE));
                organizationUnit.ReportingOrgStTxt = reader.IsDBNull(FLD_ORGSHORTTEXT) ? string.Empty : reader.GetString(FLD_ORGSHORTTEXT);
                organizationUnit.HeadName = reader.IsDBNull(FLD_ORGHEADNAME) ? string.Empty : reader.GetString(FLD_ORGHEADNAME);
            }
            else if (reader.FieldCount == 2)
            {
                const int FLD_ORGUNITCODE_DIVISION = 0;
                const int FLD_ORGID_DIVISION = 1;

                organizationUnit.OrgUnitCode = reader.IsDBNull(FLD_ORGUNITCODE_DIVISION) ? string.Empty : Convert.ToString(reader.GetString(FLD_ORGUNITCODE_DIVISION));
                organizationUnit.OrgId = reader.IsDBNull(FLD_ORGID_DIVISION) ? string.Empty : reader.GetString(FLD_ORGID_DIVISION);

            }
            else if (reader.FieldCount == 1)
            {
                const int FLD_HEADGID = 0;
                organizationUnit.HeadGID = reader.IsDBNull(FLD_HEADGID) ? string.Empty : reader.GetString(FLD_HEADGID);
            }
            else if (reader.FieldCount == 12)
            {
                const int FLD_ORG_CODE = 0;
                const int FLD_SHORT_TEXT = 1;
                const int FLD_LONG_TEXT = 2;
                const int FLD_Department_Head_Approval = 3;
                const int FLD_Department_Head_Approval_GID = 4;
                const int FLD_Department_Head_Approval_Email_ID = 5;
                const int FLD_Tower_Head_Approval = 6;
                const int FLD_Tower_Head_GID = 7;
                const int FLD_Tower_Head_Email_ID = 8;
                const int FLD_Division_Head_Approval = 9;
                const int FLD_Division_Head_GID = 10;
                const int FLD_Division_Head_Email_ID = 11;

                organizationUnit.ORG_CODE = reader.IsDBNull(FLD_ORG_CODE) ? string.Empty : reader.GetString(FLD_ORG_CODE);
                organizationUnit.SHORT_TEXT = reader.IsDBNull(FLD_SHORT_TEXT) ? string.Empty : reader.GetString(FLD_SHORT_TEXT);
                organizationUnit.LONG_TEXT = reader.IsDBNull(FLD_LONG_TEXT) ? string.Empty : reader.GetString(FLD_LONG_TEXT);
                organizationUnit.Department_Head_Approval = reader.IsDBNull(FLD_Department_Head_Approval) ? string.Empty : reader.GetString(FLD_Department_Head_Approval);
                organizationUnit.Department_Head_Approval_GID = reader.IsDBNull(FLD_Department_Head_Approval_GID) ? string.Empty : reader.GetString(FLD_Department_Head_Approval_GID);
                organizationUnit.Department_Head_Approval_Email_ID = reader.IsDBNull(FLD_Department_Head_Approval_Email_ID) ? string.Empty : reader.GetString(FLD_Department_Head_Approval_Email_ID);
                organizationUnit.Tower_Head_Approval = reader.IsDBNull(FLD_Tower_Head_Approval) ? string.Empty : reader.GetString(FLD_Tower_Head_Approval);
                organizationUnit.Tower_Head_GID = reader.IsDBNull(FLD_Tower_Head_GID) ? string.Empty : reader.GetString(FLD_Tower_Head_GID);
                organizationUnit.Tower_Head_Email_ID = reader.IsDBNull(FLD_Tower_Head_Email_ID) ? string.Empty : reader.GetString(FLD_Tower_Head_Email_ID);
                organizationUnit.Division_Head_Approval = reader.IsDBNull(FLD_Division_Head_Approval) ? string.Empty : reader.GetString(FLD_Division_Head_Approval);
                organizationUnit.Division_Head_GID = reader.IsDBNull(FLD_Division_Head_GID) ? string.Empty : reader.GetString(FLD_Division_Head_GID);
                organizationUnit.Division_Head_Email_ID = reader.IsDBNull(FLD_Division_Head_Email_ID) ? string.Empty : reader.GetString(FLD_Division_Head_Email_ID);

                return organizationUnit;
            }

            return organizationUnit;
        }

    }
}
//--------------------------------Rishi Code End-------------------------------------------------//