﻿//--------------------------------Rishi Code Start-------------------------------------------------//
using System;
using System.Data;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    internal sealed class EmployeeMemberMappingBuilder : IEntityBuilder<EmployeeMemberMapping>
    {
        IList<EmployeeMemberMapping> IEntityBuilder<EmployeeMemberMapping>.BuildEntities(IDataReader reader)
        {
            List<EmployeeMemberMapping> empMemberMapping = new List<EmployeeMemberMapping>();

            while (reader.Read())
            {
                empMemberMapping.Add(((IEntityBuilder<EmployeeMemberMapping>)this).BuildEntity(reader));
            }

            return (empMemberMapping.Count > 0) ? empMemberMapping : null;
        }

        EmployeeMemberMapping IEntityBuilder<EmployeeMemberMapping>.BuildEntity(IDataReader reader)
        {
            const int FLD_REPORTINGMGRGID = 0;
            const int FLD_MANAGERNAME = 1;

            EmployeeMemberMapping empMemberMapping = new EmployeeMemberMapping();
            empMemberMapping.Gid = reader.IsDBNull(FLD_REPORTINGMGRGID) ? string.Empty : reader.GetString(FLD_REPORTINGMGRGID);
            empMemberMapping.ManagerName = reader.IsDBNull(FLD_MANAGERNAME) ? string.Empty : reader.GetString(FLD_MANAGERNAME);

            return empMemberMapping;
        }
    }
}
//--------------------------------Rishi Code End-------------------------------------------------//
