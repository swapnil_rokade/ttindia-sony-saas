﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: GenericLookupDataAccess.cs
    Description:  
    Created By:  
    Created On:  
    Modification Log:
    --------------------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
 *  1.0                 5/Dec/2015          Prasanth Kumar G    Introduced Function InterviewAssessment_GetBy_InterviewId_InterviewerEmail
    ---------------------------------------------------------------------------------------------------------------------------------------------
    
*/

//-----------------------------------------Rishi Code Start-------------------------------------//
using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Helper;

namespace TPS360.DataAccess
{
    internal sealed class EmployeeMemberMappingDataAccess : BaseDataAccess, IEmployeeMemberMappingDataAccess
    {
        #region Constructors

        public EmployeeMemberMappingDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<EmployeeMemberMapping> CreateEntityBuilder<EmployeeMemberMapping>()
        {
            return (new EmployeeMemberMappingBuilder()) as IEntityBuilder<EmployeeMemberMapping>;
        }

        #endregion

        #region  Methods

        IList<TPS360.Common.BusinessEntities.EmployeeMemberMapping> IEmployeeMemberMappingDataAccess.GetAll()
        {
            const string SP = "dbo.EmployeeMemberMapping_GetAll";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<TPS360.Common.BusinessEntities.EmployeeMemberMapping>().BuildEntities(reader);
                }
            }
        }

        TPS360.Common.BusinessEntities.EmployeeMemberMapping IEmployeeMemberMappingDataAccess.GetById(string reportingMgrGID)
        {
            const string SP = "dbo.EmployeeMemberMapping_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@ReportingMgrGID", DbType.String, reportingMgrGID);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<TPS360.Common.BusinessEntities.EmployeeMemberMapping>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        #endregion
    }
}
//--------------------------------Rishi Code End-------------------------------------------------//