﻿using System;
using System.Data;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Helper;

namespace TPS360.DataAccess
{
    internal sealed class CompanyStatusChangeRequestBuilder : IEntityBuilder<CompanyStatusChangeRequest>
    {
        IList<CompanyStatusChangeRequest> IEntityBuilder<CompanyStatusChangeRequest>.BuildEntities(IDataReader reader)
        {
            List<CompanyStatusChangeRequest> companyStatusChangeRequests = new List<CompanyStatusChangeRequest>();

            while (reader.Read())
            {
                companyStatusChangeRequests.Add(((IEntityBuilder<CompanyStatusChangeRequest>)this).BuildEntity(reader));
            }

            return (companyStatusChangeRequests.Count > 0) ? companyStatusChangeRequests : null;
        }

        CompanyStatusChangeRequest IEntityBuilder<CompanyStatusChangeRequest>.BuildEntity(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_NEWSTATUS = 1;
            const int FLD_CURRENTSTATUS = 2;
            const int FLD_CHECKLIST = 3;
            const int FLD_NOTES = 4;
            const int FLD_COMPANYID = 5;
            const int FLD_REQUESTORID = 6;
            const int FLD_REQUESTDATE = 7;
            const int FLD_ISAPPROVED = 8;
            const int FLD_APPROVERID = 9;
            const int FLD_APPROVEDDATE = 10;
            const int FLD_APPROVEDCOMMENTS = 11;

            CompanyStatusChangeRequest companyStatusChangeRequest = new CompanyStatusChangeRequest();

            companyStatusChangeRequest.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            companyStatusChangeRequest.LastStatus = reader.IsDBNull(FLD_CURRENTSTATUS) ? 0 : reader.GetInt32(FLD_CURRENTSTATUS);
            companyStatusChangeRequest.RequestedStatus = reader.IsDBNull(FLD_NEWSTATUS) ? 0 : reader.GetInt32(FLD_NEWSTATUS);

            if (!reader.IsDBNull(FLD_CHECKLIST))
            {
                string[] checkList = StringHelper.Convert(reader.GetString(FLD_CHECKLIST)).Split(new char[] { '|' });

                if ((checkList != null) && (checkList.Length > 0))
                {
                    companyStatusChangeRequest.CheckList.AddRange(checkList);
                }
            }

            companyStatusChangeRequest.Notes = reader.IsDBNull(FLD_NOTES) ? string.Empty : reader.GetString(FLD_NOTES);
            companyStatusChangeRequest.CompanyId = reader.IsDBNull(FLD_COMPANYID) ? 0 : reader.GetInt32(FLD_COMPANYID);
            companyStatusChangeRequest.RequestorId = reader.IsDBNull(FLD_REQUESTORID) ? 0 : reader.GetInt32(FLD_REQUESTORID);
            companyStatusChangeRequest.RequestDate = reader.IsDBNull(FLD_REQUESTDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_REQUESTDATE);
            companyStatusChangeRequest.ApproveStatus = (ApproveStatus) (reader.IsDBNull(FLD_ISAPPROVED) ? 0 : reader.GetInt32(FLD_ISAPPROVED));
            companyStatusChangeRequest.ApproveStatusChangerId = reader.IsDBNull(FLD_APPROVERID) ? 0 : reader.GetInt32(FLD_APPROVERID);
            companyStatusChangeRequest.ApproveStatusChangeDate = reader.IsDBNull(FLD_APPROVEDDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_APPROVEDDATE);
            companyStatusChangeRequest.ApproveStatusComments = reader.IsDBNull(FLD_APPROVEDCOMMENTS) ? string.Empty : reader.GetString(FLD_APPROVEDCOMMENTS);

            return companyStatusChangeRequest;
        }
    }
}