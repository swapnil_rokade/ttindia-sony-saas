﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: GenericLookupDataAccess.cs
    Description:  
    Created By:  
    Created On:  
    Modification Log:
    --------------------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
 *  1.0                 5/Dec/2015          Prasanth Kumar G    Introduced Function InterviewAssessment_GetBy_InterviewId_InterviewerEmail
    ---------------------------------------------------------------------------------------------------------------------------------------------
    
*/

//-----------------------------------------Rishi Code Start-------------------------------------//
using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Helper;

namespace TPS360.DataAccess
{
    internal sealed class JobProfileDataAccess : BaseDataAccess, IJobProfileDataAccess
    {
        #region Constructors

        public JobProfileDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<JobProfile> CreateEntityBuilder<JobProfile>()
        {
            return (new JobProfileBuilder()) as IEntityBuilder<JobProfile>;
        }

        #endregion

        #region  Methods

        IList<TPS360.Common.BusinessEntities.JobProfile> IJobProfileDataAccess.GetAll(string division, string IndivContribMgnt)
        {
            const string SP = "dbo.JobProfile_GetAll";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@division", DbType.String, division);
                Database.AddInParameter(cmd, "@IndivContribMgnt", DbType.String, IndivContribMgnt);
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<TPS360.Common.BusinessEntities.JobProfile>().BuildEntities(reader);
                }
            }
        }

        TPS360.Common.BusinessEntities.JobProfile IJobProfileDataAccess.GetByJobFamilyName(string jobFamilyName)
        {
            const string SP = "dbo.JobProfile_GetByJobFamilyName";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@JobFamilyName", DbType.String, jobFamilyName);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<TPS360.Common.BusinessEntities.JobProfile>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        #endregion
    }
}
//-----------------------------------------Rishi Code End-------------------------------------//