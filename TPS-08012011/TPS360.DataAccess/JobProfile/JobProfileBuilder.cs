﻿//--------------------------------Rishi Code Start-------------------------------------------------//
using System;
using System.Data;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
     internal sealed class JobProfileBuilder : IEntityBuilder<JobProfile>
    {
        IList<JobProfile> IEntityBuilder<JobProfile>.BuildEntities(IDataReader reader)
        {
            List<JobProfile> jobProfile = new List<JobProfile>();

            while (reader.Read())
            {
                jobProfile.Add(((IEntityBuilder<JobProfile>)this).BuildEntity(reader));
            }

            return (jobProfile.Count > 0) ? jobProfile : null;
        }

        JobProfile IEntityBuilder<JobProfile>.BuildEntity(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_JOBFAMILY = 1;
            const int FLD_JOBPROFILECODE = 2;

            JobProfile jobProfile = new JobProfile();

            jobProfile.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            jobProfile.JobFamily = reader.IsDBNull(FLD_JOBFAMILY) ? string.Empty : reader.GetString(FLD_JOBFAMILY);
            jobProfile.JobProfileCode = reader.IsDBNull(FLD_JOBPROFILECODE) ? string.Empty : reader.GetString(FLD_JOBPROFILECODE);

            return jobProfile;
        }
    }
}
//--------------------------------Rishi Code End-------------------------------------------------//
