﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using System.Collections;

namespace TPS360.DataAccess
{
    internal sealed class WebParserDomainDataAccess : BaseDataAccess, IWebParserDomainDataAccess
    {
        #region Constructors

        public WebParserDomainDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<WebParserDomain> CreateEntityBuilder<WebParserDomain>()
        {
            return (new WebParserDomainBuilder()) as IEntityBuilder<WebParserDomain>;
        }

        #endregion

        #region  Methods

        WebParserDomain IWebParserDomainDataAccess.Add(WebParserDomain webParserDomain)
        {
    
            const string SP = "dbo.WebParserDomain_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@DomainName", DbType.String, webParserDomain.DomainName);
                Database.AddInParameter(cmd, "@DomainUrl", DbType.String, webParserDomain.DomainUrl);
                Database.AddInParameter(cmd, "@FullName", DbType.String, webParserDomain.FullName);
                Database.AddInParameter(cmd, "@CurrentPosition", DbType.String, webParserDomain.CurrentPosition);
                Database.AddInParameter(cmd, "@Address1", DbType.String, webParserDomain.Address1);
                Database.AddInParameter(cmd, "@Email", DbType.String, webParserDomain.Email);
                Database.AddInParameter(cmd, "@OfficePhone", DbType.String, webParserDomain.OfficePhone);
                Database.AddInParameter(cmd, "@HomePhone", DbType.String, webParserDomain.HomePhone);
                Database.AddInParameter(cmd, "@CellPhone", DbType.String, webParserDomain.CellPhone);
                Database.AddInParameter(cmd, "@TotalExperienceYears", DbType.String, webParserDomain.TotalExperienceYears);
                Database.AddInParameter(cmd, "@CurrentCompany", DbType.String, webParserDomain.CurrentCompany);
                Database.AddInParameter(cmd, "@Objective", DbType.String, webParserDomain.Objective);
                Database.AddInParameter(cmd, "@Summary", DbType.String, webParserDomain.Summary);
                Database.AddInParameter(cmd, "@Skill", DbType.String, webParserDomain.Skill);
                Database.AddInParameter(cmd, "@Education", DbType.String, webParserDomain.Education);
                Database.AddInParameter(cmd, "@WorkAuthorization", DbType.String, webParserDomain.WorkAuthorization);
                Database.AddInParameter(cmd, "@JobType", DbType.String, webParserDomain.JobType);
                Database.AddInParameter(cmd, "@Relocation", DbType.String, webParserDomain.Relocation);
                Database.AddInParameter(cmd, "@WillingToTravel", DbType.String, webParserDomain.WillingToTravel);
                Database.AddInParameter(cmd, "@YearlySalaryRate", DbType.String, webParserDomain.YearlySalaryRate);
                Database.AddInParameter(cmd, "@HourlySalarRate", DbType.String, webParserDomain.HourlySalarRate);
                Database.AddInParameter(cmd, "@SecurityClearance", DbType.String, webParserDomain.SecurityClearance);
                Database.AddInParameter(cmd, "@CopyPasteResume", DbType.String, webParserDomain.CopyPasteResume);
                Database.AddInParameter(cmd, "@WholeResume", DbType.String, webParserDomain.WholeResume);
                Database.AddInParameter(cmd, "@WordResume", DbType.String, webParserDomain.WordResume);
                Database.AddInParameter(cmd, "@DocumentUrl", DbType.String, webParserDomain.DocumentUrl);
                Database.AddInParameter(cmd, "@ReplaceKeyword", DbType.String, webParserDomain.ReplaceKeyword);
                Database.AddInParameter(cmd, "@Address2", DbType.String, webParserDomain.Address2);
                Database.AddInParameter(cmd, "@City", DbType.String, webParserDomain.City);
                Database.AddInParameter(cmd, "@State", DbType.String, webParserDomain.State);
                Database.AddInParameter(cmd, "@Country", DbType.String, webParserDomain.Country);
                Database.AddInParameter(cmd, "@ZipCode", DbType.String, webParserDomain.ZipCode);
                Database.AddInParameter(cmd, "@DateOfBirth", DbType.String, webParserDomain.DateOfBirth);
                Database.AddInParameter(cmd, "@Gender", DbType.String, webParserDomain.Gender);
                Database.AddInParameter(cmd, "@MaritalStatus", DbType.String, webParserDomain.MaritalStatus);
                Database.AddInParameter(cmd, "@IsOmitEmptyLine", DbType.Boolean, webParserDomain.IsOmitEmptyLine);
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, webParserDomain.IsRemoved);
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, webParserDomain.CreatorId);
                Database.AddInParameter(cmd, "@CreateDate", DbType.DateTime, webParserDomain.CreateDate);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        webParserDomain = CreateEntityBuilder<WebParserDomain>().BuildEntity(reader);
                    }
                    else
                    {
                        webParserDomain = null;
                    }
                }

                if (webParserDomain == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("WebParserDomain already exists. Please specify another webParserDomain.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while creating this webParserDomain.");
                            }
                    }
                }

                return webParserDomain;
            }
        }

        WebParserDomain IWebParserDomainDataAccess.Update(WebParserDomain webParserDomain)
        {
            const string SP = "dbo.WebParserDomain_Update";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, webParserDomain.Id);
                Database.AddInParameter(cmd, "@DomainName", DbType.String, webParserDomain.DomainName);
                Database.AddInParameter(cmd, "@DomainUrl", DbType.String, webParserDomain.DomainUrl);
                Database.AddInParameter(cmd, "@FullName", DbType.String, webParserDomain.FullName);
                Database.AddInParameter(cmd, "@CurrentPosition", DbType.String, webParserDomain.CurrentPosition);
                Database.AddInParameter(cmd, "@Address1", DbType.String, webParserDomain.Address1);
                Database.AddInParameter(cmd, "@Email", DbType.String, webParserDomain.Email);
                Database.AddInParameter(cmd, "@OfficePhone", DbType.String, webParserDomain.OfficePhone);
                Database.AddInParameter(cmd, "@HomePhone", DbType.String, webParserDomain.HomePhone);
                Database.AddInParameter(cmd, "@CellPhone", DbType.String, webParserDomain.CellPhone);
                Database.AddInParameter(cmd, "@TotalExperienceYears", DbType.String, webParserDomain.TotalExperienceYears);
                Database.AddInParameter(cmd, "@CurrentCompany", DbType.String, webParserDomain.CurrentCompany);
                Database.AddInParameter(cmd, "@Objective", DbType.String, webParserDomain.Objective);
                Database.AddInParameter(cmd, "@Summary", DbType.String, webParserDomain.Summary);
                Database.AddInParameter(cmd, "@Skill", DbType.String, webParserDomain.Skill);
                Database.AddInParameter(cmd, "@Education", DbType.String, webParserDomain.Education);
                Database.AddInParameter(cmd, "@WorkAuthorization", DbType.String, webParserDomain.WorkAuthorization);
                Database.AddInParameter(cmd, "@JobType", DbType.String, webParserDomain.JobType);
                Database.AddInParameter(cmd, "@Relocation", DbType.String, webParserDomain.Relocation);
                Database.AddInParameter(cmd, "@WillingToTravel", DbType.String, webParserDomain.WillingToTravel);
                Database.AddInParameter(cmd, "@YearlySalaryRate", DbType.String, webParserDomain.YearlySalaryRate);
                Database.AddInParameter(cmd, "@HourlySalarRate", DbType.String, webParserDomain.HourlySalarRate);
                Database.AddInParameter(cmd, "@SecurityClearance", DbType.String, webParserDomain.SecurityClearance);
                Database.AddInParameter(cmd, "@CopyPasteResume", DbType.String, webParserDomain.CopyPasteResume);
                Database.AddInParameter(cmd, "@WholeResume", DbType.String, webParserDomain.WholeResume);
                Database.AddInParameter(cmd, "@WordResume", DbType.String, webParserDomain.WordResume);
                Database.AddInParameter(cmd, "@DocumentUrl", DbType.String, webParserDomain.DocumentUrl);
                Database.AddInParameter(cmd, "@ReplaceKeyword", DbType.String, webParserDomain.ReplaceKeyword);
                Database.AddInParameter(cmd, "@Address2", DbType.String, webParserDomain.Address2);
                Database.AddInParameter(cmd, "@City", DbType.String, webParserDomain.City);
                Database.AddInParameter(cmd, "@State", DbType.String, webParserDomain.State);
                Database.AddInParameter(cmd, "@Country", DbType.String, webParserDomain.Country);
                Database.AddInParameter(cmd, "@ZipCode", DbType.String, webParserDomain.ZipCode);
                Database.AddInParameter(cmd, "@DateOfBirth", DbType.String, webParserDomain.DateOfBirth);
                Database.AddInParameter(cmd, "@Gender", DbType.String, webParserDomain.Gender);
                Database.AddInParameter(cmd, "@MaritalStatus", DbType.String, webParserDomain.MaritalStatus);
                Database.AddInParameter(cmd, "@IsOmitEmptyLine", DbType.Boolean, webParserDomain.IsOmitEmptyLine);
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, webParserDomain.IsRemoved);
                Database.AddInParameter(cmd, "@UpdatorId", DbType.Int32, webParserDomain.UpdatorId);
                Database.AddInParameter(cmd, "@UpdateDate", DbType.DateTime, webParserDomain.UpdateDate);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        webParserDomain = CreateEntityBuilder<WebParserDomain>().BuildEntity(reader);
                    }
                    else
                    {
                        webParserDomain = null;
                    }
                }

                if (webParserDomain == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("WebParserDomain already exists. Please specify another webParserDomain.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while updating this webParserDomain.");
                            }
                    }
                }

                return webParserDomain;
            }
        }

        WebParserDomain IWebParserDomainDataAccess.GetById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.WebParserDomain_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<WebParserDomain>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        IList<WebParserDomain> IWebParserDomainDataAccess.GetAll()
        {
            const string SP = "dbo.WebParserDomain_GetAll";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<WebParserDomain>().BuildEntities(reader);
                }
            }
        }

        bool IWebParserDomainDataAccess.DeleteById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.WebParserDomain_DeleteById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a webParserDomain which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this webParserDomain.");
                        }
                }
            }
        }

        DataTable IWebParserDomainDataAccess.GetAllName()
        {
            const string SP = "dbo.WebParserDomain_GetAllName";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    DataTable webParserDomainList = new DataTable("WebParserDomain");
                    webParserDomainList.Columns.Add("Id", typeof(Int32));
                    webParserDomainList.Columns.Add("Name", typeof(String));
                    while (reader.Read())
                    {
                        DataRow newRow = webParserDomainList.NewRow();
                        newRow["Id"] = reader.GetInt32(0);
                        newRow["Name"] = reader.GetString(1);
                        webParserDomainList.Rows.Add(newRow);
                    }

                    return webParserDomainList;
                }
            }
        }

        #endregion
    }
}