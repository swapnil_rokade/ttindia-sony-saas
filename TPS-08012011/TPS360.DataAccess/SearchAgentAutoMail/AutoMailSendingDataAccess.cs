﻿using System.Data;
using System.Data.Common;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using System.Collections.Generic;
namespace TPS360.DataAccess
{
    internal sealed class AutoMailSendingDataAccess : BaseDataAccess, IAutoMailSendingDataAccess   
    {
            #region Constructors

        public AutoMailSendingDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<AutoMailSending> CreateEntityBuilder<AutoMailSending>()
        {
            return (new AutoMailSendingBuilder()) as IEntityBuilder<AutoMailSending>;
        }

        #endregion

            #region Methods
        IList<AutoMailSending> IAutoMailSendingDataAccess.GetSearchDetails ()
        {
            const string SP = "dbo.SearchAgent_GetAllCurrentSearchAgent";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<AutoMailSending>().BuildEntities(reader);
                }
            }
        }
        #endregion
    }
}
