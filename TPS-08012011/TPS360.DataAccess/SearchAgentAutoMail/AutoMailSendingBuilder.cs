﻿using System;
using System.Data;
using System.Collections.Generic;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    internal sealed class AutoMailSendingBuilder:IEntityBuilder<AutoMailSending >
    {
        IList<AutoMailSending> IEntityBuilder<AutoMailSending>.BuildEntities(IDataReader reader)
        {
            List<AutoMailSending> SearchAgent = new List<AutoMailSending>();

            while (reader.Read())
            {
                SearchAgent.Add(((IEntityBuilder<AutoMailSending>)this).BuildEntity(reader));
            }

            return (SearchAgent.Count > 0) ? SearchAgent : null;
        }
        AutoMailSending IEntityBuilder<AutoMailSending>.BuildEntity(IDataReader reader)
        {
            AutoMailSending SearchAgent = new AutoMailSending();

            const int FLD_ID = 0;
            const int FLD_JOBPOSTINGID = 1;
            const int FLD_KEYWORDS = 2;
            const int FLD_JOBTITLE = 3;
            const int FLD_CITY = 4;
            const int FLD_STATEID = 5;
            const int FLD_COUNTRYID = 6;
            const int FLD_MINEXPERIENCE = 7;
            const int FLD_MAXEXPERIENCE = 8;
            const int FLD_SALARYFROM = 9;
            const int FLD_SALARYTO = 10;
            const int FLD_CURRENCYLOOKUPID = 11;
            const int FLD_SALARYTYPE = 12;
            const int FLD_HOTLISTID = 13;
            const int FLD_EDUCATIONLOOKUPID = 14;
            const int FLD_EMPLOYEMENTTYPELOOKUPID = 15;
            const int FLD_RESUMELASTUPDATED = 16;
            const int FLD_CANDIDATETYPELOOKUPID = 17;
            const int FLD_WORKSCHEDULELOOKUPID = 18;
            const int FLD_WORKSTATUSLOOKUPID = 19;
            const int FLD_HIRINGSTATUSLOOKUPID = 20;
            const int FLD_AVAILABLEAFTER = 21;
            const int FLD_SECURITUCLEARANCE = 22;
            const int FLD_SCHEDULEID = 23;
            const int FLD_STARTDATE = 24;
            const int FLD_REPEAT = 25;
            const int FLD_ENDDATE = 26;
            const int FLD_LASTSENTDATE = 27;
            const int FLD_NEXTSENDDATE = 28;
            const int FLD_EMAILTEMPLATEID = 29;
            const int FLD_SENDERID = 30;
            const int FLD_SUBJECT = 31;
            const int FLD_EMAILBODY = 32;

            SearchAgent.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            SearchAgent.JobPostingId = reader.IsDBNull(FLD_JOBPOSTINGID) ? 0 : reader.GetInt32(FLD_JOBPOSTINGID);
            SearchAgent.KeyWord = reader.IsDBNull(FLD_KEYWORDS) ? string.Empty : reader.GetString(FLD_KEYWORDS);
            SearchAgent.JobTitle = reader.IsDBNull(FLD_JOBTITLE) ? string.Empty : reader.GetString(FLD_JOBTITLE);
            SearchAgent.City = reader.IsDBNull(FLD_CITY) ? string.Empty : reader.GetString(FLD_CITY);
            SearchAgent.StateId = reader.IsDBNull(FLD_STATEID) ? 0 : reader.GetInt32(FLD_STATEID);
            SearchAgent.CountryId = reader.IsDBNull(FLD_COUNTRYID) ? 0 : reader.GetInt32(FLD_COUNTRYID);
            SearchAgent.MinExperience = reader.IsDBNull(FLD_MINEXPERIENCE) ? string.Empty : reader.GetString(FLD_MINEXPERIENCE);
            SearchAgent.MaxExperience = reader.IsDBNull(FLD_MAXEXPERIENCE) ? string.Empty : reader.GetString(FLD_MAXEXPERIENCE);
            SearchAgent.SalaryFrom = reader.IsDBNull(FLD_SALARYFROM) ? string.Empty : reader.GetString(FLD_SALARYFROM);
            SearchAgent.SalaryTo = reader.IsDBNull(FLD_SALARYTO) ? string.Empty : reader.GetString(FLD_SALARYTO);
            SearchAgent.CurrencyLookUpId = reader.IsDBNull(FLD_CURRENCYLOOKUPID) ? 0 : reader.GetInt32(FLD_CURRENCYLOOKUPID);
            SearchAgent.SalaryType = reader.IsDBNull(FLD_SALARYTYPE) ? string.Empty : reader.GetString(FLD_SALARYTYPE);
            SearchAgent.HotListId = reader.IsDBNull(FLD_HOTLISTID) ? 0 : reader.GetInt32(FLD_HOTLISTID);
            SearchAgent.EducationLookupId = reader.IsDBNull(FLD_EDUCATIONLOOKUPID) ? string.Empty : reader.GetString(FLD_EDUCATIONLOOKUPID);
            SearchAgent.EmployementTypeLookUpId = reader.IsDBNull(FLD_EMPLOYEMENTTYPELOOKUPID) ? 0 : reader.GetInt32(FLD_EMPLOYEMENTTYPELOOKUPID);
            SearchAgent.ResumeLastUpdated = reader.IsDBNull(FLD_RESUMELASTUPDATED) ? string.Empty : reader.GetString(FLD_RESUMELASTUPDATED);
            SearchAgent.CandidateTypeLookUpId = reader.IsDBNull(FLD_CANDIDATETYPELOOKUPID) ? 0 : reader.GetInt32(FLD_CANDIDATETYPELOOKUPID);
            SearchAgent.WorkScheduleLookupId = reader.IsDBNull(FLD_WORKSCHEDULELOOKUPID) ? 0 : reader.GetInt32(FLD_WORKSCHEDULELOOKUPID);
            SearchAgent.WorkStatusLookUpId = reader.IsDBNull(FLD_WORKSTATUSLOOKUPID) ? string.Empty : reader.GetString(FLD_WORKSTATUSLOOKUPID);
            SearchAgent.HiringStatusLookUpId = reader.IsDBNull(FLD_HIRINGSTATUSLOOKUPID) ? string.Empty : reader.GetString(FLD_HIRINGSTATUSLOOKUPID);
            SearchAgent.AvailableAfter = reader.IsDBNull(FLD_AVAILABLEAFTER) ? DateTime.MinValue : reader.GetDateTime(FLD_AVAILABLEAFTER);
            SearchAgent.SecurityClearance = reader.IsDBNull(FLD_SECURITUCLEARANCE) ? false : reader.GetBoolean(FLD_SECURITUCLEARANCE);
            SearchAgent.ScheduleId = reader.IsDBNull(FLD_SCHEDULEID) ? 0 : reader.GetInt32(FLD_SCHEDULEID);
            SearchAgent.StartDate = reader.IsDBNull(FLD_STARTDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_STARTDATE);
            SearchAgent.Repeat = reader.IsDBNull(FLD_REPEAT) ? string.Empty : reader.GetString(FLD_REPEAT);
            SearchAgent.EndDate = reader.IsDBNull(FLD_ENDDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_ENDDATE);
            SearchAgent.LastSentDate = reader.IsDBNull(FLD_LASTSENTDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_LASTSENTDATE);
            SearchAgent.NextSendDate = reader.IsDBNull(FLD_NEXTSENDDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_NEXTSENDDATE);
            SearchAgent.EmailFormatId = reader.IsDBNull(FLD_EMAILTEMPLATEID) ? 0 : reader.GetInt32(FLD_EMAILTEMPLATEID);
            SearchAgent.SenderId = reader.IsDBNull(FLD_SENDERID) ? 0 : reader.GetInt32(FLD_SENDERID);
            SearchAgent.Subject = reader.IsDBNull(FLD_SUBJECT) ? string.Empty : reader.GetString(FLD_SUBJECT);
            SearchAgent.EmailBody = reader.IsDBNull(FLD_EMAILBODY) ? string.Empty : reader.GetString(FLD_EMAILBODY);

            return SearchAgent;
        }
    }
}
