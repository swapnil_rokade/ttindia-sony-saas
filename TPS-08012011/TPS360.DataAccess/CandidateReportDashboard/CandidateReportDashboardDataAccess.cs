﻿using System;
using System.Data;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using System.Text;
using TPS360.Common.Helper;

namespace TPS360.DataAccess
{
    internal sealed class CandidateReportDashboardDataAccess : BaseDataAccess, ICandidateReportDashboardDataAccess
    {
        #region Constructors

        public CandidateReportDashboardDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<CandidateReportDashboard> CreateEntityBuilder<CandidateReportDashboard>()
        {
            return (new CandidateReportDashboardBuilder()) as IEntityBuilder<CandidateReportDashboard>;
        }

        #endregion

        #region  Methods

        CandidateReportDashboard ICandidateReportDashboardDataAccess.GetByParameter(DateTime addedFrom, DateTime addedTo, int addedBy, int addedBySource, DateTime updatedFrom, DateTime updatedTo, int updatedBy,
            int country, int state, string city, DateTime interviewFrom, DateTime interviewTo, int interviewLevel, int interviewStatus, int industry, int functionalCategory, int workPermit,
            int gender, int maritalStatus, int educationId, int assessmentId, bool isBroadcastedResume, bool isReferredApplicants, bool hasJobAgent,int assignedManager)
        {
            const string SP = "dbo.CandidateReportDashboard_GetByParameter";
            //New Applicant
            StringBuilder whereClauseNewApplicant = new StringBuilder();
            if (addedFrom != DateTime.MinValue)
            {
                if (!StringHelper.IsBlank(whereClauseNewApplicant))
                {
                    whereClauseNewApplicant.Append(" AND ");
                }
                else
                {
                    whereClauseNewApplicant.Append(" WHERE ");
                }
                whereClauseNewApplicant.Append("[C].[CreateDate]");
                whereClauseNewApplicant.Append(" >= ");
                whereClauseNewApplicant.Append("'" + addedFrom.ToShortDateString() + "'");
            }
            if (addedTo != null && addedTo != DateTime.MinValue)
            {
                if (!StringHelper.IsBlank(whereClauseNewApplicant))
                {
                    whereClauseNewApplicant.Append(" AND ");
                }
                else
                {
                    whereClauseNewApplicant.Append(" WHERE ");
                }
                whereClauseNewApplicant.Append("[C].[CreateDate]");
                whereClauseNewApplicant.Append(" <= ");
                whereClauseNewApplicant.Append("'" + addedTo.ToShortDateString() + "'");
            }
            if (addedBy > 0)
            {
                if (!StringHelper.IsBlank(whereClauseNewApplicant))
                {
                    whereClauseNewApplicant.Append(" AND ");
                }
                else
                {
                    whereClauseNewApplicant.Append(" WHERE ");
                }
                whereClauseNewApplicant.Append("[C].[CreatorId]");
                whereClauseNewApplicant.Append(" = ");
                whereClauseNewApplicant.Append(addedBy.ToString());
            }
            if (addedBySource > 0)
            {
                if (!StringHelper.IsBlank(whereClauseNewApplicant))
                {
                    whereClauseNewApplicant.Append(" AND ");
                }
                else
                {
                    whereClauseNewApplicant.Append(" WHERE ");
                }
                whereClauseNewApplicant.Append("[C].[ResumeSource]");
                whereClauseNewApplicant.Append(" = ");
                whereClauseNewApplicant.Append(addedBySource.ToString());
            }
            //Updated Applicant
            StringBuilder whereClauseUpdateApplicant = new StringBuilder();
            if (updatedFrom != null && updatedFrom != DateTime.MinValue)
            {
                if (!StringHelper.IsBlank(whereClauseUpdateApplicant))
                {
                    whereClauseUpdateApplicant.Append(" AND ");
                }
                else
                {
                    whereClauseUpdateApplicant.Append(" WHERE ");
                }
                whereClauseUpdateApplicant.Append("[C].[UpdateDate]");
                whereClauseUpdateApplicant.Append(" >= ");
                whereClauseUpdateApplicant.Append("'" + updatedFrom.ToShortDateString() + "'");
            }
            if (updatedTo != null && updatedTo != DateTime.MinValue)
            {
                if (!StringHelper.IsBlank(whereClauseUpdateApplicant))
                {
                    whereClauseUpdateApplicant.Append(" AND ");
                }
                else
                {
                    whereClauseUpdateApplicant.Append(" WHERE ");
                }
                whereClauseUpdateApplicant.Append("[C].[UpdateDate]");
                whereClauseUpdateApplicant.Append(" <= ");
                whereClauseUpdateApplicant.Append("'" + updatedTo.ToShortDateString() + "'");
            }
            if (updatedBy > 0)
            {
                if (!StringHelper.IsBlank(whereClauseUpdateApplicant))
                {
                    whereClauseUpdateApplicant.Append(" AND ");
                }
                else
                {
                    whereClauseUpdateApplicant.Append(" WHERE ");
                }
                whereClauseUpdateApplicant.Append("[C].[UpdatorId]");
                whereClauseUpdateApplicant.Append(" = ");
                whereClauseUpdateApplicant.Append(updatedBy.ToString());
            }

            //By Interview
            StringBuilder whereClauseByInterview = new StringBuilder();
            StringBuilder interviewWhereBuilder = new StringBuilder();
            if (interviewFrom != null && interviewFrom != DateTime.MinValue)
            {
                if (!StringHelper.IsBlank(interviewWhereBuilder))
                {
                    interviewWhereBuilder.Append(" AND ");
                }
                interviewWhereBuilder.Append("[A].[StartDateTimeUtc]");
                interviewWhereBuilder.Append(" >= ");
                interviewWhereBuilder.Append("'" + interviewFrom.AddHours(-6).ToShortDateString() + "'");
            }
            if (interviewTo != null && interviewTo != DateTime.MinValue)
            {
                if (!StringHelper.IsBlank(interviewWhereBuilder))
                {
                    interviewWhereBuilder.Append(" AND ");
                }
                interviewWhereBuilder.Append("[A].[StartDateTimeUtc]");
                interviewWhereBuilder.Append(" <= ");
                interviewWhereBuilder.Append("'" + interviewTo.AddHours(-6).ToShortDateString() + "'");
            }
            if (interviewLevel > 0)
            {
                if (!StringHelper.IsBlank(interviewWhereBuilder))
                {
                    interviewWhereBuilder.Append(" AND ");
                }
                interviewWhereBuilder.Append("[I].[InterviewLevel]");
                interviewWhereBuilder.Append(" = ");
                interviewWhereBuilder.Append(interviewLevel.ToString());
            }
            if (interviewStatus > 0)
            {
                if (!StringHelper.IsBlank(interviewWhereBuilder))
                {
                    interviewWhereBuilder.Append(" AND ");
                }
                interviewWhereBuilder.Append("[I].[Status]");
                interviewWhereBuilder.Append(" = ");
                interviewWhereBuilder.Append(interviewStatus.ToString());
            }

            if (!StringHelper.IsBlank(interviewWhereBuilder))
            {
                StringBuilder interviewBuilder = new StringBuilder(@" ( SELECT DISTINCT [M].[Id] FROM
[Interview] [I] LEFT JOIN [MemberJobCart] [MJC] ON [I].[MemberJobCartId]=[MJC].[Id]
LEFT JOIN [Member] [M] ON [MJC].[MemberId]=[M].[Id]
LEFT JOIN [Activity] [A] ON [A].[ActivityId]=[A].[ActivityID]");
                interviewBuilder.Append(" WHERE ");
                interviewBuilder.Append(interviewWhereBuilder);
                interviewBuilder.Append(" ) ");

                whereClauseByInterview.Append(" WHERE ");
                whereClauseByInterview.Append(" [C].[Id] ");
                whereClauseByInterview.Append(" IN ");
                whereClauseByInterview.Append(interviewBuilder.ToString());
            }
            
            //By Industry Category
            StringBuilder whereClauseByIndustryCategory = new StringBuilder();
            if (industry > 0)
            {
                StringBuilder experienceBuilder = new StringBuilder(@" ( SELECT DISTINCT [ME].[MemberId] FROM
[MemberExperience] [ME]");
                experienceBuilder.Append(" WHERE ");
                experienceBuilder.Append(" [ME].[IndustryCategoryLookupId] ");
                experienceBuilder.Append(" = ");
                experienceBuilder.Append(industry.ToString());
                experienceBuilder.Append(" ) ");

                whereClauseByIndustryCategory.Append(" WHERE ");
                whereClauseByIndustryCategory.Append(" [C].[Id] ");
                whereClauseByIndustryCategory.Append(" IN ");
                whereClauseByIndustryCategory.Append(experienceBuilder.ToString());
            }

            //By Functional Category
            StringBuilder whereClauseByFunctionalCategory = new StringBuilder();
            if (functionalCategory > 0)
            {
                
                StringBuilder experienceBuilder = new StringBuilder(@" ( SELECT DISTINCT [ME].[MemberId] FROM
[MemberExperience] [ME]");
                experienceBuilder.Append(" WHERE ");
                experienceBuilder.Append(" [ME].[FunctionalCategoryLookupId] ");
                experienceBuilder.Append(" = ");
                experienceBuilder.Append(functionalCategory.ToString());
                experienceBuilder.Append(" ) ");

                whereClauseByFunctionalCategory.Append(" WHERE ");
                whereClauseByFunctionalCategory.Append(" [C].[Id] ");
                whereClauseByFunctionalCategory.Append(" IN ");
                whereClauseByFunctionalCategory.Append(experienceBuilder.ToString());
            }

            //By Work Permit
            StringBuilder whereClauseByWorkPermit = new StringBuilder();
            if (workPermit > 0)
            {
                whereClauseByWorkPermit.Append(" WHERE ");
                whereClauseByWorkPermit.Append("[ME].[WorkAuthorizationLookupId]");
                whereClauseByWorkPermit.Append(" = ");
                whereClauseByWorkPermit.Append(workPermit.ToString());
            }

            //By Gender
            StringBuilder whereClauseByGender = new StringBuilder();
            if (gender > 0)
            {
                whereClauseByGender.Append(" WHERE ");
                whereClauseByGender.Append(" [C].[GenderLookupId] ");
                whereClauseByGender.Append(" = ");
                whereClauseByGender.Append(gender.ToString());
            }

            //By Marital Status
            StringBuilder whereClauseByMaritalStatus = new StringBuilder();
            if (maritalStatus > 0)
            {
                whereClauseByMaritalStatus.Append(" WHERE ");
                whereClauseByMaritalStatus.Append(" [C].[MaritalStatusLookupId] ");
                whereClauseByMaritalStatus.Append(" = ");
                whereClauseByMaritalStatus.Append(maritalStatus.ToString());
            }

            //By Education
            StringBuilder whereClauseByEducation = new StringBuilder();
            if (educationId > 0)
            {
                StringBuilder educationBuilder = new StringBuilder(@" ( SELECT DISTINCT [ME].[MemberId] FROM
[MemberEducation] [ME]");
                educationBuilder.Append(" WHERE ");
                educationBuilder.Append(" [ME].[LevelOfEducationLookupId] ");
                educationBuilder.Append(" = ");
                educationBuilder.Append(educationId.ToString());
                educationBuilder.Append(" ) ");

                whereClauseByEducation.Append(" WHERE ");
                whereClauseByEducation.Append(" [C].[Id] ");
                whereClauseByEducation.Append(" IN ");
                whereClauseByEducation.Append(educationBuilder.ToString());
            }

            //By Education
            StringBuilder whereClauseByAssessment = new StringBuilder();
            if (assessmentId > 0)
            {
                StringBuilder assessmentBuilder = new StringBuilder(@" ( SELECT DISTINCT [MT].[MemberId] FROM
[MemberTestScore] [MT]");
                assessmentBuilder.Append(" WHERE ");
                assessmentBuilder.Append(" [MT].[TestMasterId] ");
                assessmentBuilder.Append(" = ");
                assessmentBuilder.Append(assessmentId.ToString());
                assessmentBuilder.Append(" ) ");

                whereClauseByAssessment.Append(" WHERE ");
                whereClauseByAssessment.Append(" [C].[Id] ");
                whereClauseByAssessment.Append(" IN ");
                whereClauseByAssessment.Append(assessmentBuilder.ToString());
            }

            //By Broadcasted Resume
            StringBuilder whereClauseByBroadCast = new StringBuilder();
            if (isBroadcastedResume)
            {
                StringBuilder broadcastResumeBuilder = new StringBuilder(@" ( SELECT DISTINCT [ActivityOnObjectId]
FROM [MemberActivity] [MA] LEFT JOIN
[MemberActivityType] [MAT] ON [MA].[MemberActivityTypeId]=[MAT].[Id] ");
                broadcastResumeBuilder.Append(" WHERE ");
                broadcastResumeBuilder.Append(" [MA].[ActivityOn] ");
                broadcastResumeBuilder.Append(" = ");
                broadcastResumeBuilder.Append((int)MemberType.Candidate);
                broadcastResumeBuilder.Append(" AND ");
                broadcastResumeBuilder.Append(" [MAT].[ActivityType] ");
                broadcastResumeBuilder.Append(" = ");
                broadcastResumeBuilder.Append((int)ActivityType.ResumeBroadCast);
                broadcastResumeBuilder.Append(" ) ");

                whereClauseByBroadCast.Append(" WHERE ");
                whereClauseByBroadCast.Append(" [C].[Id] ");
                whereClauseByBroadCast.Append(" IN ");
                whereClauseByBroadCast.Append(broadcastResumeBuilder.ToString());
            }

            //By Referred Applicants
            StringBuilder whereClauseByReferred = new StringBuilder();
            if (isReferredApplicants)
            {
                StringBuilder referredApplicantsBuilder = new StringBuilder(@" ( SELECT [Id] FROM [Candidate] ) ");

                whereClauseByReferred.Append(" WHERE ");
                whereClauseByReferred.Append(" [C].[CreatorId] ");
                whereClauseByReferred.Append(" IN ");
                whereClauseByReferred.Append(referredApplicantsBuilder.ToString());
            }
            
            //By Job Agent
            StringBuilder whereClauseByJobAgent = new StringBuilder();
            if (hasJobAgent)
            {

            }

            //By Location
            StringBuilder whereClauseByLocation = new StringBuilder();
            if (country > 0)
            {
                if (!StringHelper.IsBlank(whereClauseByLocation))
                {
                    whereClauseByLocation.Append(" AND ");
                }
                else
                {
                    whereClauseByLocation.Append(" WHERE ");
                }
                whereClauseByLocation.Append("[C].[CurrentCountryId]");
                whereClauseByLocation.Append(" = ");
                whereClauseByLocation.Append(country);
            }
            if (state > 0)
            {
                if (!StringHelper.IsBlank(whereClauseByLocation))
                {
                    whereClauseByLocation.Append(" AND ");
                }
                else
                {
                    whereClauseByLocation.Append(" WHERE ");
                }
                whereClauseByLocation.Append("[C].[CurrentStateId]");
                whereClauseByLocation.Append(" = ");
                whereClauseByLocation.Append(state);
            }
            if (!string.IsNullOrEmpty(city))
            {
                if (!StringHelper.IsBlank(whereClauseByLocation))
                {
                    whereClauseByLocation.Append(" AND ");
                }
                else
                {
                    whereClauseByLocation.Append(" WHERE ");
                }
                whereClauseByLocation.Append("[C].[CurrentCity]");
                whereClauseByLocation.Append(" LIKE ");
                whereClauseByLocation.Append("'%" + city + "%'");
            }

            //Assigned Manager
            StringBuilder whereClauseByAssignedManager = new StringBuilder();
            if (assignedManager > 0)
            {
                StringBuilder assignedManagerBuilder = new StringBuilder(@" ( SELECT DISTINCT [MemberId]
                                                FROM [MemberManager] [MM]");
                assignedManagerBuilder.Append(" WHERE ");
                assignedManagerBuilder.Append(" [MM].[ManagerId] ");
                assignedManagerBuilder.Append(" = ");
                assignedManagerBuilder.Append(assignedManager);
                assignedManagerBuilder.Append(" ) ");

                whereClauseByAssignedManager.Append(" WHERE ");
                whereClauseByAssignedManager.Append(" [C].[Id] ");
                whereClauseByAssignedManager.Append(" IN ");
                whereClauseByAssignedManager.Append(assignedManagerBuilder.ToString());
            }

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@WhereClauseNewApplicant", DbType.String, whereClauseNewApplicant.ToString());
                Database.AddInParameter(cmd, "@WhereClauseUpdateApplicant", DbType.String, whereClauseUpdateApplicant.ToString());
                Database.AddInParameter(cmd, "@WhereClauseByInterview", DbType.String, whereClauseByInterview.ToString());
                Database.AddInParameter(cmd, "@WhereClauseByIndustryCategory", DbType.String, whereClauseByIndustryCategory.ToString());
                Database.AddInParameter(cmd, "@WhereClauseByFunctionalCategory", DbType.String, whereClauseByFunctionalCategory.ToString());
                Database.AddInParameter(cmd, "@WhereClauseByWorkPermit", DbType.String, whereClauseByWorkPermit.ToString());
                Database.AddInParameter(cmd, "@WhereClauseByGender", DbType.String, whereClauseByGender.ToString());
                Database.AddInParameter(cmd, "@WhereClauseByMaritalStatus", DbType.String, whereClauseByMaritalStatus.ToString());
                Database.AddInParameter(cmd, "@WhereClauseByEducation", DbType.String, whereClauseByEducation.ToString());
                Database.AddInParameter(cmd, "@WhereClauseByAssessment", DbType.String, whereClauseByAssessment.ToString());
                Database.AddInParameter(cmd, "@WhereClauseByBroadCast", DbType.String, whereClauseByBroadCast.ToString());
                Database.AddInParameter(cmd, "@WhereClauseByReferred", DbType.String, whereClauseByReferred.ToString());
                Database.AddInParameter(cmd, "@WhereClauseByJobAgent", DbType.String, whereClauseByJobAgent.ToString());
                Database.AddInParameter(cmd, "@WhereClauseByLocation", DbType.String, whereClauseByLocation.ToString());
                Database.AddInParameter(cmd, "@WhereClauseByAssignedManager", DbType.String, whereClauseByAssignedManager.ToString());
                Database.AddInParameter(cmd, "@WholeSql", DbType.String, String.Empty);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    CandidateReportDashboard candidateDashboard = CreateEntityBuilder<CandidateReportDashboard>().BuildEntity(reader);
                    return candidateDashboard;
                }
            }
        }

        #endregion
    }
}