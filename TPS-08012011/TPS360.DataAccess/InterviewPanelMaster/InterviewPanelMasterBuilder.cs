﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName            :   InterviewPanelMasterBuilder.cs
    Description         :   This page is used Call the DB Table Column for InterviewPanelMaster.
    Created By          :   Pravin khot
    Created On          :   27/Nov/2015
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
  
 ---------------------------------------------------------------------------------------------------------------------------------------------------------------------        
*/


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TPS360.Common.BusinessEntities;
using System.Data;

namespace TPS360.DataAccess
{
    internal sealed class InterviewPanelMasterBuilder : IEntityBuilder<InterviewPanelMaster>
    {
        IList<InterviewPanelMaster> IEntityBuilder<InterviewPanelMaster>.BuildEntities(IDataReader reader)
        {
            List<InterviewPanelMaster> interFeedback = new List<InterviewPanelMaster>();

            while (reader.Read())
            {
                interFeedback.Add(((IEntityBuilder<InterviewPanelMaster>)this).BuildEntity(reader));
            }

            return (interFeedback.Count > 0) ? interFeedback : null;
        }

        InterviewPanelMaster IEntityBuilder<InterviewPanelMaster>.BuildEntity(IDataReader reader) 
        {
           
            const int FLD_INTERVIEWPANEL_ID = 0;
            const int FLD_INTERVIEWPANEL_NAME = 1; 
           
            const int FLD_SKILL = 2;

            const int FLD_CREATORID = 3;
            //const int FLD_UPDATORID = 4;
            //const int FLD_CREATEDATE = 5;
            //const int FLD_UPDATEDATE = 6;

            

            InterviewPanelMaster InterviewPanelMaster = new InterviewPanelMaster();


            InterviewPanelMaster.InterviewPanel_Id = reader.IsDBNull(FLD_INTERVIEWPANEL_ID) ? 0 : reader.GetInt32(FLD_INTERVIEWPANEL_ID);
            InterviewPanelMaster.InterviewPanel_Name = reader.IsDBNull(FLD_INTERVIEWPANEL_NAME) ? string.Empty : reader.GetString(FLD_INTERVIEWPANEL_NAME);
            InterviewPanelMaster.InterviewSkill = reader.IsDBNull(FLD_SKILL) ? string.Empty : reader.GetString(FLD_SKILL);

            //InterviewPanelMaster.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
            //InterviewPanelMaster.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);
            InterviewPanelMaster.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
           // InterviewPanelMaster.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);


            return InterviewPanelMaster;
        }

        #region IEntityBuilder<InterviewPanelMaster> InterviewPanelMaster


        public InterviewPanelMaster BuildEntity(IDataReader dataReader)
        {
            throw new NotImplementedException();
        }

        #endregion
    }

}
