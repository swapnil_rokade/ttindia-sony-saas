﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TPS360.Common.BusinessEntities;
using System.Data;

namespace TPS360.DataAccess
{
    internal sealed class QuestionBankBuilder : IEntityBuilder<QuestionBank>
    {
        IList<QuestionBank> IEntityBuilder<QuestionBank>.BuildEntities(IDataReader reader)
        {
            List<QuestionBank> interFeedback = new List<QuestionBank>();

            while (reader.Read())
            {
                interFeedback.Add(((IEntityBuilder<QuestionBank>)this).BuildEntity(reader));
            }

            return (interFeedback.Count > 0) ? interFeedback : null;
        }

        QuestionBank IEntityBuilder<QuestionBank>.BuildEntity(IDataReader reader) 
        {
            //const int FLD_QUESTIONBANK_ID = 0;
            //const int FLD_QUESTIONBANKTYPE_ID = 1;
            //const int FLD_QUESTION = 2;
            //const int FLD_ANSWERTYPE_ID = 3;
             
               
            //const int FLD_CREATEDATE = 4;
            //const int FLD_UPDATEDATE = 5;
            //const int FLD_CREATORID = 6;
            //const int FLD_UPDATORID = 7;
            //const int FLD_INTERVIEWID = 8;

            const int FLD_QUESTIONBANK_ID = 0;
            const int FLD_QUESTIONBANKTYPE_ID = 1;
            const int FLD_QUESTION = 2;
            const int FLD_ANSWERTYPE_ID = 3;

            const int FLD_CREATORID = 4;
            const int FLD_UPDATORID = 5;
            const int FLD_CREATEDATE = 6;
            const int FLD_UPDATEDATE = 7;

          



            QuestionBank QuestionBank = new QuestionBank();


            QuestionBank.QuestionBank_Id = reader.IsDBNull(FLD_QUESTIONBANK_ID) ? 0 : reader.GetInt32(FLD_QUESTIONBANK_ID);
            QuestionBank.QuestionBankType_Id = reader.IsDBNull(FLD_QUESTIONBANKTYPE_ID) ? 0 : reader.GetInt32(FLD_QUESTIONBANKTYPE_ID);
            QuestionBank.Question = reader.IsDBNull(FLD_QUESTION) ? string.Empty : reader.GetString(FLD_QUESTION);
            QuestionBank.AnswerType_Id = reader.IsDBNull(FLD_ANSWERTYPE_ID) ? 0 : reader.GetInt32(FLD_ANSWERTYPE_ID);
            //QuestionBank.AnswerType = reader.IsDBNull(FLD_QUESTION) ? string.Empty : reader.GetString(FLD_QUESTION);

            QuestionBank.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
            QuestionBank.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);
            QuestionBank.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
            QuestionBank.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);
          


            return QuestionBank;
        }


      





        #region IEntityBuilder<QuestionBank> QuestionBank


        public QuestionBank BuildEntity(IDataReader dataReader)
        {
            throw new NotImplementedException();
        }

        #endregion
    }

}
