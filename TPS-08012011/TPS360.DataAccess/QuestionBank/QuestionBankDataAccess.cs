﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName            :   QuestionBankDataAccess.cs
    Description         :   This page is used Call the Stored Proceedures for Interviewer Feed back.
    Created By          :   Prasanth
    Created On          :   15/Oct/2015
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
  
 ---------------------------------------------------------------------------------------------------------------------------------------------------------------------        
*/
using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;
using Microsoft.Practices.EnterpriseLibrary.Data;
using TPS360.Common.Shared;
using TPS360.Common.Helper;
using System.Collections;
using System.Text;
using TPS360.Common.BusinessEntities;
namespace TPS360.DataAccess
{
    internal sealed class QuestionBankDataAccess : BaseDataAccess,IQuestionBankDataAccess
    {
         #region Constructors

        public QuestionBankDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<QuestionBank> CreateEntityBuilder<QuestionBank>()
        {
            return (new QuestionBankBuilder()) as IEntityBuilder<QuestionBank>;
        }

        #endregion

        #region  Methods
        QuestionBank IQuestionBankDataAccess.Add(QuestionBank QuestionBank) 
        {
            const string SP = "dbo.QuestionBank_Create";
            using (DbCommand cmd = Database.GetStoredProcCommand(SP)) 
            {

                Database.AddInParameter(cmd, "@QuestionBankType_Id", DbType.Int32, QuestionBank.QuestionBankType_Id);
                Database.AddInParameter(cmd, "@Question", DbType.String, QuestionBank.Question);
                Database.AddInParameter(cmd, "@AnswerType_ID", DbType.Int32, QuestionBank.AnswerType_Id);
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, QuestionBank.CreatorId);
                Database.AddInParameter(cmd, "@UpdatorId", DbType.Int32, QuestionBank.UpdatorId);
                //Database.AddInParameter(cmd, "@InterviewId", DbType.Int32, QuestionBank.InterviewId);
                //Database.ExecuteReader(cmd);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        QuestionBank = CreateEntityBuilder<QuestionBank>().BuildEntity(reader);
                    }
                    else
                    {
                        QuestionBank = null;
                    }
                }

                if (QuestionBank == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Assessment Form already exists. Please specify another Assessment Form.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while creating this Asssessment Form.");
                            }
                    }
                }

                return QuestionBank;

             
            }
        
        }

        QuestionBank IQuestionBankDataAccess.Update(QuestionBank QuestionBank)
        {
            const string SP = "dbo.QuestionBank_Update";
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {

                Database.AddInParameter(cmd, "@QuestionBank_Id", DbType.Int32, QuestionBank.QuestionBank_Id);
                Database.AddInParameter(cmd, "@QuestionBankType_Id", DbType.Int32, QuestionBank.QuestionBankType_Id);
                Database.AddInParameter(cmd, "@Question", DbType.String, QuestionBank.Question);
                Database.AddInParameter(cmd, "@AnswerType_ID", DbType.Int32, QuestionBank.AnswerType_Id);
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, QuestionBank.CreatorId);
                Database.AddInParameter(cmd, "@UpdatorId", DbType.Int32, QuestionBank.UpdatorId);
                //Database.AddInParameter(cmd, "@InterviewId", DbType.Int32, QuestionBank.InterviewId);
                Database.ExecuteReader(cmd);

                //Database.ExecuteReader(cmd);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        QuestionBank = CreateEntityBuilder<QuestionBank>().BuildEntity(reader);
                    }
                    else
                    {
                        QuestionBank = null;
                    }
                }

                if (QuestionBank == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Assessment Form already exists. Please specify another Assessment Form.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while updating this commonNote.");
                            }
                    }
                }

                return QuestionBank;
            }
        }


      
        IList<QuestionBank> IQuestionBankDataAccess.GetAll()
        {
            const string SP = "dbo.QuestionBank_GetAll";
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<QuestionBank>().BuildEntities(reader);
                }
            }

        }

        IList<QuestionBank> IQuestionBankDataAccess.GetAll( string SortExpression)
        {
            const string SP = "dbo.QuestionBank_GetAllWithSort";
            
            

            string SortColumn = "[Q].[CreateDate]";
            string SortOrder = "desc";
            string[] part = (string.IsNullOrEmpty(SortExpression)) ? null : SortExpression.Split(' ');

            if (part != null && part.Length > 0)
            {
                SortColumn = part[0];

                if (part.Length > 1 && part[1] != null)
                {
                    SortOrder = part[1];
                }
                else
                {
                    SortOrder = "asc";
                }
            }

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
               
                Database.AddInParameter(cmd, "@SortColumn", DbType.String, SortColumn);
                Database.AddInParameter(cmd, "@SortOrder", DbType.String, SortOrder);
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<QuestionBank>().BuildEntities(reader);
                }
            }
        }
 
        bool IQuestionBankDataAccess.DeleteById(int Id)
        {
            if (Id < 1)
            {
                throw new ArgumentNullException("Id");
            }

            const string SP = "dbo.QuestionBank_DeleteById";
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, Id);
                Database.ExecuteNonQuery(cmd);
                return true;
                //int returnCode = GetReturnCodeFromParameter(cmd);
                //int returnCode = Database.ExecuteNonQuery(cmd);



                //switch (returnCode)
                //{
                //    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                //        {
                //            return true;
                //        }
                //    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                //        {
                //            throw new ArgumentException("Cannot delete a Assessment Form which has association.");
                //        }
                //    default:
                //        {
                //            throw new SystemException("An unexpected error has occurred while deleting this Assessment Form.");
                //        }
                //}
            }

        }
        
        QuestionBank IQuestionBankDataAccess.GetById(int Id)
        {
            if (Id < 1)
            {
                throw new ArgumentNullException("Id");
            }

            const string SP = "dbo.QuestionBank_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, Id);
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<QuestionBank>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }

        }
       
       
        //Check this Functionality Later 0123456789
        PagedResponse<QuestionBank> IQuestionBankDataAccess.GetPaged(PagedRequest request)
        {
            const string SP = "dbo.InterviewerFeedback_GetPaged";
            
            string whereClause = string.Empty;

            object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder)
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<QuestionBank> response = new PagedResponse<QuestionBank>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = CreateEntityBuilder<QuestionBank>().BuildEntities(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }
        #endregion
    }
}
