﻿
/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: MemberSkillDataAccess.cs
    Description: his is used for member skill data access
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1            Feb-4-2009           Gopala Swamy           Defect ID:9090; Added Overloaded method for "GetAllByMemberId()"
 --------------------------------------------------------------------------------------------------------------------------------------------        
*/


using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    internal sealed class MemberSkillDataAccess : BaseDataAccess, IMemberSkillDataAccess
    {
        #region Constructors

        public MemberSkillDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<MemberSkill> CreateEntityBuilder<MemberSkill>()
        {
            return (new MemberSkillBuilder()) as IEntityBuilder<MemberSkill>;
        }

        #endregion

        #region  Methods

        IList<MemberSkill> IMemberSkillDataAccess.GetAllByMemberId(int memberId)
        {
            if (memberId < 1)
            {
                throw new ArgumentNullException("memberId");
            }

            const string SP = "dbo.MemberSkill_GetAllByMemberId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<MemberSkill>().BuildEntities(reader);
                }
            }
        }
        IList<MemberSkill> IMemberSkillDataAccess.GetAllByMemberId(int memberId, string sortExpression)//0.1 starts here
        {
            if (memberId < 1)
            {
                throw new ArgumentNullException("memberId");
            }

            const string SP = "dbo.MemberSkill_GetAllByMemberId";
            string SortOrder = "Order By [S].[Name] ASC"; 
            if (!string.IsNullOrEmpty(sortExpression)) 
            {
                SortOrder = "Order By " + sortExpression;

            }                      

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberId);
                Database.AddInParameter(cmd, "@SortOrder", DbType.String, SortOrder);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<MemberSkill>().BuildEntities(reader);
                }
            }
        }//0.1 end here 

        #endregion
    }
}