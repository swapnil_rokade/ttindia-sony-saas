﻿using System;
using System.Data;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    internal sealed class MemberSkillBuilder : IEntityBuilder<MemberSkill>
    {
        IList<MemberSkill> IEntityBuilder<MemberSkill>.BuildEntities(IDataReader reader)
        {
            List<MemberSkill> memberSkillMaps = new List<MemberSkill>();

            while (reader.Read())
            {
                memberSkillMaps.Add(((IEntityBuilder<MemberSkill>)this).BuildEntity(reader));
            }

            return (memberSkillMaps.Count > 0) ? memberSkillMaps : null;
        }

        MemberSkill IEntityBuilder<MemberSkill>.BuildEntity(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_PROFICIENCYLOOKUPID = 1;
            const int FLD_PROFICIENCY = 2;
            const int FLD_YEARSOFEXPERIENCE = 3;
            const int FLD_LASTUSED = 4;
            const int FLD_COMMENT = 5;
            const int FLD_MEMBERID = 6;
            const int FLD_SKILLID = 7;
            const int FLD_NAME = 8;
            const int FLD_DESCRIPTION = 9;
            const int FLD_CATEGORYID = 10;
            const int FLD_CATEGORYNAME = 11;
            const int FLD_ISREMOVED = 12;
            const int FLD_CREATORID = 13;
            const int FLD_UPDATORID = 14;
            const int FLD_CREATEDATE = 15;
            const int FLD_UPDATEDATE = 16;

            MemberSkill memberSkill = new MemberSkill();

            memberSkill.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            memberSkill.ProficiencyLookupId = reader.IsDBNull(FLD_PROFICIENCYLOOKUPID) ? 0 : reader.GetInt32(FLD_PROFICIENCYLOOKUPID);
            memberSkill.Proficiency = reader.IsDBNull(FLD_PROFICIENCY) ? string.Empty : reader.GetString(FLD_PROFICIENCY);
            memberSkill.YearsOfExperience = reader.IsDBNull(FLD_YEARSOFEXPERIENCE) ? 0 : reader.GetInt32(FLD_YEARSOFEXPERIENCE);
            memberSkill.LastUsed = reader.IsDBNull(FLD_LASTUSED) ? DateTime.MinValue : reader.GetDateTime(FLD_LASTUSED);
            memberSkill.Comment = reader.IsDBNull(FLD_COMMENT) ? string.Empty : reader.GetString(FLD_COMMENT);
            memberSkill.MemberId = reader.IsDBNull(FLD_MEMBERID) ? 0 : reader.GetInt32(FLD_MEMBERID);
            memberSkill.SkillId = reader.IsDBNull(FLD_SKILLID) ? 0 : reader.GetInt32(FLD_SKILLID);
            memberSkill.Name = reader.IsDBNull(FLD_NAME) ? string.Empty : reader.GetString(FLD_NAME);
            memberSkill.Description = reader.IsDBNull(FLD_DESCRIPTION) ? string.Empty : reader.GetString(FLD_DESCRIPTION);
            memberSkill.CategoryId = reader.IsDBNull(FLD_CATEGORYID) ? 0 : reader.GetInt32(FLD_CATEGORYID);
            memberSkill.Category = reader.IsDBNull(FLD_CATEGORYNAME) ? string.Empty : reader.GetString(FLD_CATEGORYNAME);
            memberSkill.IsRemoved = reader.IsDBNull(FLD_ISREMOVED) ? false : reader.GetBoolean(FLD_ISREMOVED);
            memberSkill.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
            memberSkill.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);
            memberSkill.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
            memberSkill.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);

            return memberSkill;
        }
    }
}
