﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;
using Microsoft.Practices.EnterpriseLibrary.Data;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Helper;

namespace TPS360.DataAccess
{
    internal sealed class ExternalViewStagesDataAccess : BaseDataAccess, IExternalViewStagesDataAccess
    {
        #region Constructors

        public ExternalViewStagesDataAccess(Context context)
            : base(context)
        {
        }
        protected override IEntityBuilder<ExternalViewStages> CreateEntityBuilder<ExternalViewStages>()
        {
            return (new ExternalViewStagesBuilder()) as IEntityBuilder<ExternalViewStages>;
        }
        #endregion
        #region  Methods

        ExternalViewStages IExternalViewStagesDataAccess.Add(ExternalViewStages externalViewStages)
        {
            const string SP = "dbo.ExternalViewStages_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Name", DbType.AnsiString, StringHelper.Convert(externalViewStages.Name));
                Database.AddInParameter(cmd, "@CreatorID", DbType.AnsiString, Convert.ToInt32(externalViewStages.CreatorId));
                Database.AddInParameter(cmd, "@UpdatorID", DbType.AnsiString, Convert.ToInt32(externalViewStages.UpdatorId));

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        externalViewStages = CreateEntityBuilder<ExternalViewStages>().BuildEntity(reader);
                    }
                    else
                    {
                        externalViewStages = null;
                    }
                }

                if (externalViewStages == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("External stage already exists. Please specify another external stage.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while creating this external stage.");
                            }
                    }
                }

                return externalViewStages;
            }
        }

        ExternalViewStages IExternalViewStagesDataAccess.Update(ExternalViewStages externalViewStages)
        {
            const string SP = "dbo.ExternalViewStages_Update";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, externalViewStages.Id);
                Database.AddInParameter(cmd, "@Name", DbType.AnsiString, StringHelper.Convert(externalViewStages.Name));
                Database.AddInParameter(cmd, "@UpdatorID", DbType.AnsiString, Convert.ToInt32(externalViewStages.UpdatorId));

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        externalViewStages = CreateEntityBuilder<ExternalViewStages>().BuildEntity(reader);
                    }
                    else
                    {
                        externalViewStages = null;
                    }
                }

                if (externalViewStages == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("External Stage already exists. Please specify another HiringMatrixLevels.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while updating this External stage.");
                            }
                    }
                }

                return externalViewStages;
            }
        }
        IList<ExternalViewStages> IExternalViewStagesDataAccess.GetAllExternalStages()
        {
            const string SP = "dbo.ExternalViewStages_GetAll";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<ExternalViewStages>().BuildEntities(reader);
                }
            }
        }

        bool IExternalViewStagesDataAccess.DeleteById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.ExternalViewStages_DeleteById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }                   
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this external stage.");
                        }
                }
            }
        }
        #endregion
    }
}
