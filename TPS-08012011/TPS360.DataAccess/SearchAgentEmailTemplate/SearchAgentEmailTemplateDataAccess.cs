﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: MemberEmailDataAccess.cs
    Description: This is the .cs file used for data accesse in email pages.
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1            Dec-05-2008           Jagadish            Defect id: 9358; changed 'Or' to 'And' in the Query
    0.2            Dec-26-2008           Jagadish            Defect id: 9290; Changes made in method Getpaged().
-------------------------------------------------------------------------------------------------------------------------------------------        
*/
using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Helper;
using System.Collections;
using System.Text;

namespace TPS360.DataAccess
{
    internal sealed class SearchAgentEmailTemplateDataAccess : BaseDataAccess, ISearchAgentEmailTemplateDataAccess
    {
        #region Constructors

        public SearchAgentEmailTemplateDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<SearchAgentEmailTemplate> CreateEntityBuilder<SearchAgentEmailTemplate>()
        {
            return (new SearchAgentEmailTemplateBuilder()) as IEntityBuilder<SearchAgentEmailTemplate>;
        }

        #endregion

        #region  Methods


        SearchAgentEmailTemplate ISearchAgentEmailTemplateDataAccess.Add(SearchAgentEmailTemplate template)
        {
            const string SP = "dbo.SearchAgentEmailTemplate_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@SenderId", DbType.Int32, template.SenderId);
                Database.AddInParameter(cmd, "@JobPostingID", DbType.Int32, template.JobPostingID);
                Database.AddInParameter(cmd, "@Subject", DbType.AnsiString, template.Subject);
                Database.AddInParameter(cmd, "@EmailBody", DbType.AnsiString, template.EmailBody);
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, template.IsRemoved);
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, template.CreatorId);
              
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        template = CreateEntityBuilder<SearchAgentEmailTemplate>().BuildEntity(reader);
                    }
                    else
                    {
                        template = null;
                    }
                }

                if (template == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Member email already exists. Please specify another SearchAgentEmailTemplate.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while creating this SearchAgentEmailTemplate.");
                            }
                    }  
                }

                return template;
            }
        }

        SearchAgentEmailTemplate ISearchAgentEmailTemplateDataAccess.Update(SearchAgentEmailTemplate template)
        {
            const string SP = "dbo.SearchAgentEmailTemplate_Update";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, template.Id);
                Database.AddInParameter(cmd, "@SenderId", DbType.Int32, template.SenderId);
                Database.AddInParameter(cmd, "@JobPostingID", DbType.Int32, template.JobPostingID);
                Database.AddInParameter(cmd, "@Subject", DbType.AnsiString, template.Subject);
                Database.AddInParameter(cmd, "@EmailBody", DbType.AnsiString, template.EmailBody);
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, template.IsRemoved);
                Database.AddInParameter(cmd, "@UpdatorId", DbType.Int32, template.UpdatorId);
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        template = CreateEntityBuilder<SearchAgentEmailTemplate>().BuildEntity(reader);
                    }
                    else
                    {
                        template = null;
                    }
                }

                if (template == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Member email already exists. Please specify another SearchAgentEmailTemplate.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while updating this SearchAgentEmailTemplate.");
                            }
                    }
                }

                return template;
            }
        }

        SearchAgentEmailTemplate ISearchAgentEmailTemplateDataAccess.GetById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.SearchAgentEmailTemplate_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<SearchAgentEmailTemplate>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        SearchAgentEmailTemplate ISearchAgentEmailTemplateDataAccess.GetByJobPostingId(int JobPostingId)
        {
            if (JobPostingId < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.SearchAgentEmailTemplate_GetByJobPostingId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@JobPostingId", DbType.Int32, JobPostingId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<SearchAgentEmailTemplate>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        bool ISearchAgentEmailTemplateDataAccess.DeleteSearchAgentEmailTemplateByJobPostingId(int JobPostingId)
        {
            if (JobPostingId < 1)
            {
                throw new ArgumentNullException("JobPostingId");
            }

            const string SP = "dbo.SearchAgentEmailTemplate_DeleteByJobPostingId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@JobPostingId", DbType.Int32, JobPostingId);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a search agent email template which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this search agent email template.");
                        }
                }
            }
        }
        #endregion
    }
}