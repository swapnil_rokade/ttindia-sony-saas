﻿//*******************Suraj Adsule******Start********For Candidate Joining Form********20160808*********//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TPS360.Common.BusinessEntities;


namespace TPS360.DataAccess
{
    internal sealed class CandidateJoiningFormBuilder: IEntityBuilder<CandidateJoiningDetails>
    {
        IList<CandidateJoiningDetails> IEntityBuilder<CandidateJoiningDetails>.BuildEntities(System.Data.IDataReader reader)
        {
            List<CandidateJoiningDetails> Status = new List<CandidateJoiningDetails>();

            while (reader.Read())
            {
                Status.Add(((IEntityBuilder<CandidateJoiningDetails>)this).BuildEntity(reader));
            }

            return (Status.Count > 0) ? Status : null;
        }
        CandidateJoiningDetails IEntityBuilder<CandidateJoiningDetails>.BuildEntity(System.Data.IDataReader reader)
        {
            const int fld_jobPostingID = 0;
            const int fld_jobRestrictions = 1;
            const int fld_DateOfJoining = 2;
            const int fld_EmployeeType = 3;
            const int fld_WorkerSubType = 4;
            const int fld_PayGroup = 5;
            const int fld_PayGroupGrade = 6;
            const int fld_Currency = 7;
            const int fld_Frequency = 8;
            const int fld_CompensationPlan = 9;
            const int fld_MaximumFTE = 10;
            const int fld_Notes = 11;
            const int fld_ID = 12;
            const int fld_CandidateID = 13;
            const int fld_CREATORID = 14;
            const int fld_UPDATORID = 15;
            const int fld_CREATEDATE = 16;
            const int fld_UPDATEDATE = 17;

            CandidateJoiningDetails CandidateStatus = new CandidateJoiningDetails();

            CandidateStatus.Id = reader.IsDBNull(fld_ID) ? 0 : reader.GetInt32(fld_ID);
            CandidateStatus.DemandID = reader.IsDBNull(fld_jobPostingID) ? 0 : reader.GetInt32(fld_jobPostingID);
            CandidateStatus.JobRestrictions = reader.IsDBNull(fld_jobRestrictions) ? 0 : reader.GetInt32(fld_jobRestrictions);
            CandidateStatus.MaximumFTE = reader.IsDBNull(fld_MaximumFTE) ? 0 : reader.GetInt32(fld_MaximumFTE);
            CandidateStatus.Notes = reader.IsDBNull(fld_Notes) ? "" : reader.GetString(fld_Notes);
            CandidateStatus.PayGroup = reader.IsDBNull(fld_PayGroup) ? 0 : reader.GetInt32(fld_PayGroup);
            CandidateStatus.PayGroupGrade = reader.IsDBNull(fld_PayGroupGrade) ? 0 : reader.GetInt32(fld_PayGroupGrade);
            CandidateStatus.WorkerSubType = reader.IsDBNull(fld_WorkerSubType) ? 0 : reader.GetInt32(fld_WorkerSubType);
            CandidateStatus.Frequency = reader.IsDBNull(fld_Frequency) ? 0 : reader.GetInt32(fld_Frequency);
            CandidateStatus.EmployeeType = reader.IsDBNull(fld_EmployeeType) ? 0 : reader.GetInt32(fld_EmployeeType);
            CandidateStatus.CandidateID = reader.IsDBNull(fld_CandidateID) ? string.Empty : reader.GetString(fld_CandidateID);
            CandidateStatus.CompensationPlan = reader.IsDBNull(fld_CompensationPlan) ? 0 : reader.GetInt32(fld_CompensationPlan);
            CandidateStatus.Currency = reader.IsDBNull(fld_Currency) ? 0 : reader.GetInt32(fld_Currency);
            CandidateStatus.DateOfJoining = reader.IsDBNull(fld_DateOfJoining) ? DateTime.MinValue : reader.GetDateTime(fld_DateOfJoining);
            CandidateStatus.CreateDate = reader.IsDBNull(fld_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(fld_CREATEDATE);
            CandidateStatus.CreatorId = reader.IsDBNull(fld_CREATORID) ? 0 : reader.GetInt32(fld_CREATORID);
            CandidateStatus.UpdateDate = reader.IsDBNull(fld_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(fld_UPDATEDATE);
            CandidateStatus.UpdatorId = reader.IsDBNull(fld_UPDATORID) ? 0 : reader.GetInt32(fld_UPDATORID);

            //if (reader.FieldCount > FLD_CandidateName)
            //{
            //    CandidateStatus.CandidateName = reader.IsDBNull(FLD_CandidateName) ? string.Empty : reader.GetString(FLD_CandidateName);
            //    CandidateStatus.JobTitle = reader.IsDBNull(FLD_JobTitle) ? string.Empty : reader.GetString(FLD_JobTitle);
            //    CandidateStatus.LevelName = reader.IsDBNull(FLD_LevelName) ? string.Empty : reader.GetString(FLD_LevelName);
            //    CandidateStatus.AddedToRequisitionDate = reader.IsDBNull(FLD_AddedToRequisitionDate) ? DateTime.MinValue : reader.GetDateTime(FLD_AddedToRequisitionDate);
            //    CandidateStatus.RejectedDate = reader.IsDBNull(FLD_RejectedDate) ? DateTime.MinValue : reader.GetDateTime(FLD_RejectedDate);
            //    CandidateStatus.UsersName = reader.IsDBNull(FLD_UsersName) ? string.Empty : reader.GetString(FLD_UsersName);
            //}
            return CandidateStatus;
        }
    }
}
//*******************Suraj Adsule******End********For Candidate Joining Form********20160808*********//