﻿//*******************Suraj Adsule******Start********For Candidate Joining Form********20160805*********//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Data;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    internal sealed class CandidateJoiningFormDataAccess:BaseDataAccess,ICandidateJoiningFormDataAccess
    {
        #region Constructors

        public CandidateJoiningFormDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<CandidateJoiningFormDataAccess> CreateEntityBuilder<CandidateJoiningFormDataAccess>()
        {
            return (new CandidateJoiningFormBuilder()) as IEntityBuilder<CandidateJoiningFormDataAccess>;
        }

        #endregion
        #region Methods
        void ICandidateJoiningFormDataAccess.AddCandidateJoiningForm(CandidateJoiningDetails candidateJoiningDetail)
        {
            const string SP = "dbo.CandidateJoining_Create";
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@CompensationPlan", DbType.Int32, candidateJoiningDetail.CompensationPlan);
                Database.AddInParameter(cmd, "@Currency", DbType.Int32, candidateJoiningDetail.Currency);
                Database.AddInParameter(cmd, "@DateOfJoining", DbType.DateTime, candidateJoiningDetail.DateOfJoining);
                Database.AddInParameter(cmd, "@JobPostingID", DbType.Int32, candidateJoiningDetail.DemandID);
                Database.AddInParameter(cmd, "@EmployeeType", DbType.Int32, candidateJoiningDetail.EmployeeType);
                Database.AddInParameter(cmd, "@Frequency", DbType.Int32, candidateJoiningDetail.Frequency);
                Database.AddInParameter(cmd, "@JobRestrictions", DbType.Int32, candidateJoiningDetail.JobRestrictions);
                Database.AddInParameter(cmd, "@MaximumFTE", DbType.Int32, candidateJoiningDetail.MaximumFTE);
                Database.AddInParameter(cmd, "@PayGroup", DbType.Int32, candidateJoiningDetail.PayGroup);
                Database.AddInParameter(cmd, "@PayGroupGrade", DbType.Int32, candidateJoiningDetail.PayGroupGrade);
                Database.AddInParameter(cmd, "@Notes", DbType.AnsiString, candidateJoiningDetail.Notes);
                Database.AddInParameter(cmd, "@CandidateId", DbType.String, candidateJoiningDetail.CandidateID);
                Database.AddInParameter(cmd, "@WorkerSubType", DbType.AnsiString, candidateJoiningDetail.WorkerSubType);
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, candidateJoiningDetail.CreatorId);
                Database.AddInParameter(cmd, "@CreateDate", DbType.DateTime, candidateJoiningDetail.CreateDate);
                Database.AddInParameter(cmd, "@UpdatorId", DbType.Int32, candidateJoiningDetail.UpdatorId);
                Database.AddInParameter(cmd, "@UpdateDate", DbType.DateTime, candidateJoiningDetail.UpdateDate);
                Database.ExecuteNonQuery(cmd);
            }
        }

        CandidateJoiningDetails ICandidateJoiningFormDataAccess.GetCandidateJoiningDetail(int candidateId, int demandID)
        {
            const string SP = "dbo.CandidateJoining_Read";
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@CandidateID", DbType.Int32, candidateId);
              //  Database.AddInParameter(cmd, "@JobRestrictions", DbType.Int32, demandID);
                Database.AddInParameter(cmd, "@DemandID", DbType.Int32, demandID);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<CandidateJoiningDetails>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        bool ICandidateJoiningFormDataAccess.DeleteCandidateJoiningDetail(int candidateId)
        {
            const string SP = "dbo.CandidateJoining_Delete";
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@CandidateID", DbType.Int32, candidateId);
                Database.ExecuteNonQuery(cmd);
            }
            return true;
        }

        bool ICandidateJoiningFormDataAccess.UpdateCandidateJoiningForm(CandidateJoiningDetails candidateJoiningDetail)
        {
                    const string SP = "dbo.CandidateJoining_Update";
                    using (DbCommand cmd = Database.GetStoredProcCommand(SP))
                    {
                        Database.AddInParameter(cmd, "@CompensationPlan", DbType.Int32, candidateJoiningDetail.CompensationPlan);
                        Database.AddInParameter(cmd, "@Currency", DbType.Int32, candidateJoiningDetail.Currency);
                        Database.AddInParameter(cmd, "@DateOfJoining", DbType.DateTime, candidateJoiningDetail.DateOfJoining);
                        Database.AddInParameter(cmd, "@JobPostingID", DbType.Int32, candidateJoiningDetail.DemandID);
                        Database.AddInParameter(cmd, "@EmployeeType", DbType.Int32, candidateJoiningDetail.EmployeeType);
                        Database.AddInParameter(cmd, "@Frequency", DbType.Int32, candidateJoiningDetail.Frequency);
                        Database.AddInParameter(cmd, "@JobRestrictions", DbType.Int32, candidateJoiningDetail.JobRestrictions);
                        Database.AddInParameter(cmd, "@MaximumFTE", DbType.Int32, candidateJoiningDetail.MaximumFTE);
                        Database.AddInParameter(cmd, "@PayGroup", DbType.Int32, candidateJoiningDetail.PayGroup);
                        Database.AddInParameter(cmd, "@PayGroupGrade", DbType.Int32, candidateJoiningDetail.PayGroupGrade);
                        Database.AddInParameter(cmd, "@Notes", DbType.AnsiString, candidateJoiningDetail.Notes);
                        Database.AddInParameter(cmd, "@CandidateId", DbType.AnsiString, candidateJoiningDetail.CandidateID);
                        Database.AddInParameter(cmd, "@WorkerSubType", DbType.AnsiString, candidateJoiningDetail.WorkerSubType);
                        Database.ExecuteNonQuery(cmd);
                    }
                    return true;
        }

        #endregion
    }
}
//*******************Suraj Adsule******End********For Candidate Joining Form********20160805*********//