﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    internal sealed class MemberAlertBuilder : IEntityBuilder<MemberAlert>
    {
        IList<MemberAlert> IEntityBuilder<MemberAlert>.BuildEntities(IDataReader reader)
        {
            List<MemberAlert> memberAlerts = new List<MemberAlert>();

            while (reader.Read())
            {
                memberAlerts.Add(((IEntityBuilder<MemberAlert>)this).BuildEntity(reader));
            }

            return (memberAlerts.Count > 0) ? memberAlerts : null;
        }

        MemberAlert IEntityBuilder<MemberAlert>.BuildEntity(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_ALERTID = 1;
            const int FLD_MEMBERID = 2;
            const int FLD_ALERTDATE = 3;
            const int FLD_ALERTSTATUS = 4;
            const int FLD_ALERTVIEWDATE = 5;
            const int FLD_ISREMOVED = 6;
            const int FLD_CREATORID = 7;
            const int FLD_UPDATORID = 8;
            const int FLD_CREATEDATE = 9;
            const int FLD_UPDATEDATE = 10;

            MemberAlert memberAlert = new MemberAlert();

            memberAlert.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            memberAlert.AlertId = reader.IsDBNull(FLD_ALERTID) ? 0 : reader.GetInt32(FLD_ALERTID);
            memberAlert.MemberId = reader.IsDBNull(FLD_MEMBERID) ? 0 : reader.GetInt32(FLD_MEMBERID);
            memberAlert.AlertDate = reader.IsDBNull(FLD_ALERTDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_ALERTDATE);
            memberAlert.AlertStatus = (AlertStatus) (reader.IsDBNull(FLD_ALERTSTATUS) ? 0 : reader.GetInt32(FLD_ALERTSTATUS));
            memberAlert.AlertViewDate = reader.IsDBNull(FLD_ALERTVIEWDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_ALERTVIEWDATE);
            memberAlert.IsRemoved = reader.IsDBNull(FLD_ISREMOVED) ? false : reader.GetBoolean(FLD_ISREMOVED);
            memberAlert.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
            memberAlert.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);
            memberAlert.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
            memberAlert.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);

            using (Context ctx = new Context())
            {
                IAlertDataAccess da = new AlertDataAccess(ctx);
                memberAlert.Alert = da.GetById(memberAlert.AlertId);
            }

            return memberAlert;
        }
    }
}