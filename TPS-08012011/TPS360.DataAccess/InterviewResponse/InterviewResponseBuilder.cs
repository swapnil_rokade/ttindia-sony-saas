﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName            :   InterviewResponseBuilder.cs
    Description         :   This page is used to Create Properties for InterviewResponse.
    Created By          :   Prasanth Kumar G
    Created On          :   16/Dec/2015
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1                 27/Dec/2015         Prasanth Kumar G    Introduced RowNo
 ---------------------------------------------------------------------------------------------------------------------------------------------------------------------        
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TPS360.Common.BusinessEntities;
using System.Data;

namespace TPS360.DataAccess
{
    internal sealed class InterviewResponseBuilder : IEntityBuilder<InterviewResponse>
    {
        IList<InterviewResponse> IEntityBuilder<InterviewResponse>.BuildEntities(IDataReader reader)
        {
            List<InterviewResponse> InterviewResponse = new List<InterviewResponse>();

            while (reader.Read())
            {
                InterviewResponse.Add(((IEntityBuilder<InterviewResponse>)this).BuildEntity(reader));
            }

            return (InterviewResponse.Count > 0) ? InterviewResponse : null;
        }

        InterviewResponse IEntityBuilder<InterviewResponse>.BuildEntity(IDataReader reader) 
        {

            const int FLD_ROWNO = 0; 
            const int FLD_INTERVIEWRESPONSE_ID = 1; 
            const int FLD_INTERVIEWID = 2;
            const int FLD_QUESTIONBANKTYPE_NAME = 3;
            const int FLD_QUESTION = 4;
            const int FLD_QUESTIONBANK_ID = 5;
            const int FLD_ANSWERTYPE_NAME = 6;
            const int FLD_ANSWERTYPE_ID = 7;
            const int FLD_RESPONSE = 8;
            const int FLD_RATIONALE = 9;
            const int FLD_CREATEDATE = 10;
            const int FLD_UPDATEDATE = 11;
            const int FLD_CREATORID = 12;
            const int FLD_UPDATORID = 13;
        


            InterviewResponse InterviewResponse = new InterviewResponse();


            InterviewResponse.RowNo = reader.IsDBNull(FLD_ROWNO) ? 0 : reader.GetInt64(FLD_ROWNO);
            InterviewResponse.InterviewResponse_Id = reader.IsDBNull(FLD_INTERVIEWRESPONSE_ID) ? 0 : reader.GetInt32(FLD_INTERVIEWRESPONSE_ID);
            InterviewResponse.InterviewId = reader.IsDBNull(FLD_INTERVIEWID) ? 0 : reader.GetInt32(FLD_INTERVIEWID);
            InterviewResponse.QuestionBankType = reader.IsDBNull(FLD_QUESTIONBANKTYPE_NAME) ? string.Empty : reader.GetString(FLD_QUESTIONBANKTYPE_NAME);
            InterviewResponse.Question = reader.IsDBNull(FLD_QUESTION) ? string.Empty : reader.GetString(FLD_QUESTION);
            InterviewResponse.QuestionBank_id = reader.IsDBNull(FLD_QUESTIONBANK_ID) ? 0 : reader.GetInt32(FLD_QUESTIONBANK_ID); 
            InterviewResponse.Response = reader.IsDBNull(FLD_ANSWERTYPE_NAME) ? string.Empty : reader.GetString(FLD_ANSWERTYPE_NAME);
            InterviewResponse.AnswerType_ID = reader.IsDBNull(FLD_ANSWERTYPE_ID) ? 0 : reader.GetInt32(FLD_ANSWERTYPE_ID);
            InterviewResponse.AnswerType = reader.IsDBNull(FLD_ANSWERTYPE_NAME) ? string.Empty : reader.GetString(FLD_ANSWERTYPE_NAME);
            InterviewResponse.Response = reader.IsDBNull(FLD_RESPONSE) ? string.Empty : reader.GetString(FLD_RESPONSE);
            InterviewResponse.Rationale = reader.IsDBNull(FLD_RATIONALE) ? string.Empty : reader.GetString(FLD_RATIONALE);

            InterviewResponse.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
            InterviewResponse.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);
            InterviewResponse.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
            InterviewResponse.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);
            


            return InterviewResponse;
        }


      





        #region IEntityBuilder<InterviewResponse> InterviewResponse


        public InterviewResponse BuildEntity(IDataReader dataReader)
        {
            throw new NotImplementedException();
        }

        #endregion
    }

}
