﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName:    MemberInterviewFeedbackDocumentBuilder.cs
    Description: 
    Created By:  Prasanth Kumar G  
    Created On:  24/Dec/2015
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author             Modification
    ------------------------------------------------------------------------------------------------------------------------------
    
                                                               
*/
using System;
using System.Data;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    internal sealed class MemberInterviewFeedbackDocumentBuilder : IEntityBuilder<MemberDocument>
    {
        IList<MemberDocument> IEntityBuilder<MemberDocument>.BuildEntities(IDataReader reader)
        {
            List<MemberDocument> memberDocuments = new List<MemberDocument>();

            while (reader.Read())
            {
                memberDocuments.Add(((IEntityBuilder<MemberDocument>)this).BuildEntity(reader));
            }

            return (memberDocuments.Count > 0) ? memberDocuments : null;
        }

        MemberDocument IEntityBuilder<MemberDocument>.BuildEntity(IDataReader reader)
        {
            const int FLD_ID = 0;
            
            const int FLD_FILENAME = 1;
            const int FLD_DESCRIPTION = 2;
            const int FLD_FILETYPE_LOOKUP_ID = 3;
            const int FLD_ISREMOVED = 4;
            const int FLD_MEMBERID = 5;
            const int FLD_INTERVIEWID = 6;
            const int FLD_INTERVIEWEREMAIL = 7;

            const int FLD_CREATEDATE = 8;
            const int FLD_UPDATEDATE = 9;

            MemberDocument memberDocument = new MemberDocument();

            memberDocument.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            
            memberDocument.FileName = reader.IsDBNull(FLD_FILENAME) ? string.Empty : reader.GetString(FLD_FILENAME);
            memberDocument.Description = reader.IsDBNull(FLD_DESCRIPTION) ? string.Empty : reader.GetString(FLD_DESCRIPTION);
            memberDocument.FileTypeLookupId = reader.IsDBNull(FLD_FILETYPE_LOOKUP_ID) ? 0 : reader.GetInt32(FLD_FILETYPE_LOOKUP_ID);
            memberDocument.IsRemoved = reader.IsDBNull(FLD_ISREMOVED) ? false : reader.GetBoolean(FLD_ISREMOVED);
            memberDocument.MemberId = reader.IsDBNull(FLD_MEMBERID) ? 0 : reader.GetInt32(FLD_MEMBERID);
            memberDocument.InterviewId = reader.IsDBNull(FLD_INTERVIEWID) ? 0 : reader.GetInt32(FLD_INTERVIEWID);
            memberDocument.InterviewerEmail = reader.IsDBNull(FLD_INTERVIEWEREMAIL) ? string.Empty : reader.GetString(FLD_INTERVIEWEREMAIL);
            memberDocument.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
            memberDocument.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);

            return memberDocument;
        }
    }
}