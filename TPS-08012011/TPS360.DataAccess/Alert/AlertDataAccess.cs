﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common;
using TPS360.Common.Shared;
using TPS360.Common.Helper;

namespace TPS360.DataAccess
{
    internal sealed class AlertDataAccess : BaseDataAccess, IAlertDataAccess
    {
        #region Constructors

        public AlertDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<Alert> CreateEntityBuilder<Alert>()
        {
            return (new AlertBuilder()) as IEntityBuilder<Alert>;
        }

        #endregion

        #region  Methods

        Alert IAlertDataAccess.Add(Alert alert)
        {
            const string SP = "dbo.Alert_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Title", DbType.AnsiString, alert.Title);
                Database.AddInParameter(cmd, "@Description", DbType.AnsiString, alert.Description);
                Database.AddInParameter(cmd, "@CategoryLookupId", DbType.Int32, alert.CategoryLookupId);
                Database.AddInParameter(cmd, "@WorkflowTitleId", DbType.Int32, alert.WorkflowTitleId);
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Int32, alert.IsRemoved);
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, alert.CreatorId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        alert = CreateEntityBuilder<Alert>().BuildEntity(reader);
                    }
                    else
                    {
                        alert = null;
                    }
                }

                if (alert == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Alert already exists. Please specify another alert.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while creating this alert.");
                            }
                    }
                }

                return alert;
            }
        }

        Alert IAlertDataAccess.Update(Alert alert)
        {
            const string SP = "dbo.Alert_Update";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, alert.Id);
                Database.AddInParameter(cmd, "@Title", DbType.AnsiString, alert.Title);
                Database.AddInParameter(cmd, "@Description", DbType.AnsiString, alert.Description);
                Database.AddInParameter(cmd, "@CategoryLookupId", DbType.Int32, alert.CategoryLookupId);
                Database.AddInParameter(cmd, "@WorkflowTitleId", DbType.Int32, alert.WorkflowTitleId);
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Int32, alert.IsRemoved);
                Database.AddInParameter(cmd, "@UpdatorId", DbType.Int32, alert.UpdatorId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        alert = CreateEntityBuilder<Alert>().BuildEntity(reader);
                    }
                    else
                    {
                        alert = null;
                    }
                }

                if (alert == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Alert already exists. Please specify another alert.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while updating this alert.");
                            }
                    }
                }

                return alert;
            }
        }

        Alert IAlertDataAccess.GetById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.Alert_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<Alert>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        Alert IAlertDataAccess.GetByWorkflowTitle(WorkFlowTitle workFlowTitle)
        {
            const string SP = "dbo.Alert_GetByWorkflowTitleId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@WorkflowTitleId", DbType.Int32, workFlowTitle);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<Alert>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        IList<Alert> IAlertDataAccess.GetAll()
        {
            const string SP = "dbo.Alert_GetAll";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<Alert>().BuildEntities(reader);
                }
            }
        }

        PagedResponse<Alert> IAlertDataAccess.GetPaged(PagedRequest request)
        {
            const string SP = "dbo.Alert_GetPaged";
            string whereClause = string.Empty;

            if (StringHelper.IsBlank(request.SortColumn))
            {
                request.SortColumn = "Title";
            }

            request.SortColumn = "[A].[" + request.SortColumn + "]";

            object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder)
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<Alert> response = new PagedResponse<Alert>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = CreateEntityBuilder<Alert>().BuildEntities(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }

        bool IAlertDataAccess.DeleteById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.Alert_DeleteById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a alert which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this alert.");
                        }
                }
            }
        }

        #endregion
    }
}