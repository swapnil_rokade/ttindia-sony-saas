﻿using System;
using System.Data;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    internal sealed class RecurrenceBuilder : IEntityBuilder<Recurrence>
    {
        IList<Recurrence> IEntityBuilder<Recurrence>.BuildEntities(IDataReader reader)
        {
            List<Recurrence> recurrences = new List<Recurrence>();

            while (reader.Read())
            {
                recurrences.Add(((IEntityBuilder<Recurrence>)this).BuildEntity(reader));
            }

            return (recurrences.Count > 0) ? recurrences : null;
        }

        Recurrence IEntityBuilder<Recurrence>.BuildEntity(IDataReader reader)
		{
				const int FLD_RECURRENCEID = 0;
				const int FLD_ENDDATEUTC = 1;
				const int FLD_DAYOFWEEKMASKUTC = 2;
				const int FLD_UTCOFFSET = 3;
				const int FLD_DAYOFMONTH = 4;
				const int FLD_MONTHOFYEAR = 5;
				const int FLD_PERIODMULTIPLE = 6;
				const int FLD_PERIOD = 7;
				const int FLD_EDITTYPE = 8;
				const int FLD_LASTREMINDERDATETIMEUTC = 9;
				//const int FLD__TS = 10;

            Recurrence recurrence = new Recurrence();

			recurrence.RecurrenceID = reader.IsDBNull(FLD_RECURRENCEID) ? 0 : reader.GetInt32(FLD_RECURRENCEID);
			recurrence.EndDateUtc = reader.IsDBNull(FLD_ENDDATEUTC) ? DateTime.MinValue : reader.GetDateTime(FLD_ENDDATEUTC);
			recurrence.DayOfWeekMaskUtc = reader.IsDBNull(FLD_DAYOFWEEKMASKUTC) ? 0 : reader.GetInt32(FLD_DAYOFWEEKMASKUTC);
			recurrence.UtcOffset = reader.IsDBNull(FLD_UTCOFFSET) ? 0 : reader.GetInt32(FLD_UTCOFFSET);
			recurrence.DayOfMonth = reader.IsDBNull(FLD_DAYOFMONTH) ? 0 : reader.GetInt32(FLD_DAYOFMONTH);
			recurrence.MonthOfYear = reader.IsDBNull(FLD_MONTHOFYEAR) ? 0 : reader.GetInt32(FLD_MONTHOFYEAR);
			recurrence.PeriodMultiple = reader.IsDBNull(FLD_PERIODMULTIPLE) ? 0 : reader.GetInt32(FLD_PERIODMULTIPLE);
			recurrence.Period = reader.IsDBNull(FLD_PERIOD) ? string.Empty : reader.GetString(FLD_PERIOD);
			recurrence.EditType = reader.IsDBNull(FLD_EDITTYPE) ? 0 : reader.GetInt32(FLD_EDITTYPE);
			recurrence.LastReminderDateTimeUtc = reader.IsDBNull(FLD_LASTREMINDERDATETIMEUTC) ? DateTime.MinValue : reader.GetDateTime(FLD_LASTREMINDERDATETIMEUTC);
			//recurrence._ts = reader.IsDBNull(FLD__TS) ? 0 : reader.GetByte[](FLD__TS);
		
            return recurrence;
		}
    }
}