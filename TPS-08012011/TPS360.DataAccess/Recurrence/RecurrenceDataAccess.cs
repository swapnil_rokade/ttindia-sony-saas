﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    internal sealed class RecurrenceDataAccess : BaseDataAccess, IRecurrenceDataAccess
    {
        #region Constructors

        public RecurrenceDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<Recurrence> CreateEntityBuilder<Recurrence>()
        {
            return (new RecurrenceBuilder()) as IEntityBuilder<Recurrence>;
        }

        #endregion

        #region  Methods

        Recurrence IRecurrenceDataAccess.Add(Recurrence recurrence)
        {
            const string SP = "dbo.Recurrence_Add";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);

                Database.AddInParameter(cmd, "@EndDateUtc", DbType.DateTime, recurrence.EndDateUtc);
                Database.AddInParameter(cmd, "@DayOfWeekMaskUtc", DbType.Int32, recurrence.DayOfWeekMaskUtc);
                Database.AddInParameter(cmd, "@UtcOffset", DbType.Int32, recurrence.UtcOffset);
                Database.AddInParameter(cmd, "@DayOfMonth", DbType.Int32, recurrence.DayOfMonth);
                Database.AddInParameter(cmd, "@MonthOfYear", DbType.Int32, recurrence.MonthOfYear);
                Database.AddInParameter(cmd, "@PeriodMultiple", DbType.Int32, recurrence.PeriodMultiple);
                Database.AddInParameter(cmd, "@Period", DbType.StringFixedLength, recurrence.Period);
                Database.AddInParameter(cmd, "@EditType", DbType.Int32, recurrence.EditType);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        recurrence = CreateEntityBuilder<Recurrence>().BuildEntity(reader);
                    }
                    else
                    {
                        recurrence = null;
                    }
                }

                if (recurrence == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Recurrence already exists. Please specify another recurrence.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while creating this recurrence.");
                            }
                    }
                }

                return recurrence;
            }
        }

        Recurrence IRecurrenceDataAccess.Update(Recurrence recurrence)
        {
            const string SP = "dbo.Recurrence_Upd";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);

                Database.AddInParameter(cmd, "@RecurrenceID", DbType.Int32, recurrence.RecurrenceID);
                Database.AddInParameter(cmd, "@EndDateUtc", DbType.DateTime, recurrence.EndDateUtc);
                Database.AddInParameter(cmd, "@DayOfWeekMaskUtc", DbType.Int32, recurrence.DayOfWeekMaskUtc);
                Database.AddInParameter(cmd, "@UtcOffset", DbType.Int32, recurrence.UtcOffset);
                Database.AddInParameter(cmd, "@DayOfMonth", DbType.Int32, recurrence.DayOfMonth);
                Database.AddInParameter(cmd, "@MonthOfYear", DbType.Int32, recurrence.MonthOfYear);
                Database.AddInParameter(cmd, "@PeriodMultiple", DbType.Int32, recurrence.PeriodMultiple);
                Database.AddInParameter(cmd, "@Period", DbType.StringFixedLength, recurrence.Period);
                Database.AddInParameter(cmd, "@EditType", DbType.Int32, recurrence.EditType);
                Database.AddInParameter(cmd, "@LastReminderDateTimeUtc", DbType.DateTime, recurrence.LastReminderDateTimeUtc);
              
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        recurrence = CreateEntityBuilder<Recurrence>().BuildEntity(reader);
                    }
                    else
                    {
                        recurrence = null;
                    }
                }

                if (recurrence == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Recurrence already exists. Please specify another recurrence.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while updating this recurrence.");
                            }
                    }
                }

                return recurrence;
            }
        }

        Recurrence IRecurrenceDataAccess.GetByRecurrenceID(int recurrenceID)
        {
            if (recurrenceID < 1)
            {
                throw new ArgumentNullException("recurrenceID");
            }

            const string SP = "dbo.Recurrence_GetByRecurrenceID";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@RecurrenceID", DbType.Int32, recurrenceID);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<Recurrence>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        IList<Recurrence> IRecurrenceDataAccess.GetAll()
        {
            const string SP = "dbo.Recurrence_GetAll";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<Recurrence>().BuildEntities(reader);
                }
            }
        }

        bool IRecurrenceDataAccess.DeleteByRecurrenceID(int recurrenceID)
        {
            if (recurrenceID < 1)
            {
                throw new ArgumentNullException("recurrenceID");
            }

            const string SP = "dbo.Recurrence_DeleteByRecurrenceID";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@RecurrenceID", DbType.Int32, recurrenceID);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a recurrence which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this recurrence.");
                        }
                }
            }
        }

        #endregion
    }
}