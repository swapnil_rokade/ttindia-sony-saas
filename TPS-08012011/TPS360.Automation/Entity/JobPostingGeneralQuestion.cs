﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Automation
{
    internal sealed class JobPostingGeneralQuestion : BaseEntity
    {
        #region Properties

        public string Question
        {
            get;
            set;
        }
        public int ScreeningScore
        {
            get;
            set;
        }
        public int QuestionOrder
        {
            get;
            set;
        }
        public string QuestionType
        {
            get;
            set;
        }
        public int JobPostingId
        {
            get;
            set;
        }

        #endregion

        #region Constructor

        public JobPostingGeneralQuestion()
            : base()
        {
        }

        #endregion
    }
}