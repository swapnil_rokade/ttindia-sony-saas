using System;
using System.Runtime.Serialization;

namespace TPS360.Automation
{
    internal sealed class JobPostingAssessment : BaseEntity
   {
        #region Properties
        
        public int TestMasterId
        {
            get;
            set;
        }
        
        public int JobPostingId
        {
            get;
            set;
        }

        #endregion

        #region Constructor

        public JobPostingAssessment()
            : base()
        {
        }

        #endregion
    }
}