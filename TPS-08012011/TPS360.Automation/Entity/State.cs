﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Automation
{
    internal sealed class State : BaseEntity
    {
        #region Property
        public string Name
        {
            get;
            set;
        }
        public string StateCode
        {
            get;
            set;
        }
        public int CountryId
        {
            get;
            set;
        }
        #endregion

        #region Constructor
        public State()
            : base()
        {
        }
        #endregion
    }
}
