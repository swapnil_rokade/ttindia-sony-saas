using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;



public static class Reports
{
    public static string[] GetCountDataForDailyReport(string strEmployeeID, SqlConnection objSqlCon, DateTime dtReport)
    {
        string[] strCount = new string[8];

        string strSqlQuery = @" SELECT convert(varchar,count(distinct intUserId)) FROM UserHistory WHERE intOwnerId = '" + strEmployeeID + @"' and dttmActivityDate >= '" + dtReport + @"' and (varchrActivity like '%Updated Candidate Profile%' or varchrActivity like '%New Candidate added%')
                            UNION ALL
                            SELECT convert(varchar,count(distinct intUserId)) FROM UserHistory WHERE intOwnerId = '" + strEmployeeID + @"' and dttmActivityDate >= '" + dtReport + @"' and varchrActivity like '%Previewed Candidate%'
                            UNION ALL
                            SELECT convert(varchar,count(*)) FROM UserHistory WHERE intOwnerId = '" + strEmployeeID + @"' and dttmActivityDate >= '" + dtReport + @"' and varchrActivity like '%Created Requisition as Draft%'
                            UNION ALL
                            select count(*) from JobPosting where convert(varchar,dttmDatePosted,101)='" + String.Format("{0:MM/dd/yyyy}", dtReport) + @"' and intStatus>=" + RequisitionStatus.SubmittedMin.ToString() + @" and intStatus<=" + RequisitionStatus.SubmittedMax.ToString() + @"
                            UNION ALL
                            SELECT convert(varchar,count(distinct intUserId)) FROM UserHistory WHERE intOwnerId = '" + strEmployeeID + @"' and dttmActivityDate >= '" + dtReport + @"' and varchrActivity like '%Created New Client%'
                            UNION ALL
                            SELECT convert(varchar,count(distinct intUserId)) FROM UserHistory WHERE intOwnerId = '" + strEmployeeID + @"' and dttmActivityDate >= '" + dtReport + @"' and varchrActivity like '%Created New Contact%'
                            UNION ALL
                            SELECT convert(varchar,count(*)) FROM Invoices WHERE intInvoiceCreatedBy = '" + strEmployeeID + @"' and dttmInvoiceDate >= '" + dtReport + @"'
                            UNION ALL
                            SELECT convert(varchar,sum(monTotalAmount)) FROM Invoices WHERE intInvoiceCreatedBy = '" + strEmployeeID + @"' and dttmInvoiceDate >= '" + dtReport + @"'";

        SqlCommand objSqlCommand = new SqlCommand(strSqlQuery, objSqlCon);
        objSqlCommand.CommandTimeout = 300;
        SqlDataReader dreaderCandidateCount = null;

        dreaderCandidateCount = objSqlCommand.ExecuteReader();
        long index = 0;
        while (dreaderCandidateCount.Read())
        {
            strCount[index] = dreaderCandidateCount[0].ToString();
            index++;
        }
        //objSqlCon.Dispose();
        dreaderCandidateCount.Close();
        dreaderCandidateCount.Dispose();
        return strCount;
    }

    public static string GetCount(string strQuery, SqlConnection objOpenedSqlCon)
    {
        SqlCommand objSqlCmd = null;
        string strCount = "";
        try
        {
            objSqlCmd = new SqlCommand(strQuery, objOpenedSqlCon);
            SqlDataReader objDR = objSqlCmd.ExecuteReader();

            if (objDR.Read())
            {
                strCount = objDR[0].ToString();
            }
            else
            {
                strCount = "0";
            }
            objDR.Close();
        }
        catch (Exception ex)
        {
            strCount = "";
        }

        return strCount;
    }

    public static string GetSingleValue(string strQuery, SqlConnection objOpenedSqlCon)
    {
        SqlCommand objSqlCmd = null;
        string strCount = "";
        try
        {
            objSqlCmd = new SqlCommand(strQuery, objOpenedSqlCon);
            SqlDataReader objDR = objSqlCmd.ExecuteReader();

            if (objDR.Read())
            {
                strCount = objDR[0].ToString();
            }
            else
            {
                strCount = String.Empty;
            }
            objDR.Close();
        }
        catch (Exception ex)
        {
            strCount = "";
        }

        return strCount;
    }
}

public static class RequisitionStatus
{
    public static Int16 OpenMin
    {
        get { return 1; }
    }

    public static Int16 OpenMax
    {
        get { return 5; }
    }

    public static Int16 SubmittedMin
    {
        get { return 6; }
    }

    public static Int16 SubmittedMax
    {
        get { return 7; }
    }

    public static Int16 ClosedMin
    {
        get { return 8; }
    }

    public static Int16 ClosedMax
    {
        get { return 11; }
    }

    public static string GetStatus(string strStatusID)
    {
        switch (strStatusID)
        {
            case "1":
                return "Open";               
            case "2":
                return "Open - Difficult skillset";               
            case "3":
                return "Open - F2F Not Feasible";               
            case "4":
                return "Open - Low Rate";               
            case "5":
                return "Open - Resume Searching";              
            case "6":
                return "Submitted -  Waiting for feedback";               
            case "7":
                return "Submitted - Interview in progress";                
            case "8":
                return "Closed - Without submission";               
            case "9":
                return "Closed - Successful";               
            case "10":
                return "Closed - Interviewed not successful";               
            case "11":
                return "Closed - Submitted not successful";               
            default:
                return String.Empty;                
        }
    }
}
