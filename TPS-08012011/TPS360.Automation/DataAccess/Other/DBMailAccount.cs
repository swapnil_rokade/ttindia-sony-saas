using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.Sql;
using System.Data.SqlTypes;
using Microsoft.SqlServer.Server;
using System.Data.SqlClient;
using System.Collections;
using System.Web;

namespace TPS360.Automation
{
    internal class DBMailAccount
    {
        public bool Create(string emailId, string smtp, string connectionString)
        {
            try
            {
                const string SP = "Automation_CreateEmailAccount";

                using (SqlConnection objSqlCon = new SqlConnection(connectionString))
                {
                    objSqlCon.Open();
                    using (SqlCommand cmd = new SqlCommand(SP, objSqlCon))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@v_email", SqlDbType.VarChar);
                        cmd.Parameters["@v_email"].Value = emailId;
                        cmd.Parameters.Add("@v_smtp", SqlDbType.VarChar);
                        cmd.Parameters["@v_smtp"].Value = smtp;
                        cmd.ExecuteNonQuery();
                        return true;
                    }
                }
            }
            catch
            {
                return false;
            }
        }

        public bool SetDefault(int accountId, string connectionString)
        {
            try
            {
                const string SP = "Automation_SetDefaultEmailAccount";

                using (SqlConnection objSqlCon = new SqlConnection(connectionString))
                {
                    objSqlCon.Open();
                    using (SqlCommand cmd = new SqlCommand(SP, objSqlCon))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@accountId", SqlDbType.Int);
                        cmd.Parameters["@accountId"].Value = accountId;
                        cmd.ExecuteNonQuery();
                        return true;
                    }
                }
            }
            catch
            {
                return false;
            }
        }

        public int GetDefaultAccountId(string connectionString)
        {
            try
            {
                const string SP = "Automation_GetDefaultEmailAccountId";

                using (SqlConnection objSqlCon = new SqlConnection(connectionString))
                {
                    objSqlCon.Open();
                    using (SqlCommand cmd = new SqlCommand(SP, objSqlCon))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        SqlParameter newParameter = cmd.CreateParameter();
                        newParameter.Direction = ParameterDirection.Output;
                        newParameter.SqlDbType = SqlDbType.Int;
                        newParameter.ParameterName = "@accountId";
                        cmd.Parameters.Add(newParameter);
                        cmd.ExecuteNonQuery();
                        return Convert.ToInt32(cmd.Parameters[0].Value);
                    }
                }
            }
            catch
            {
                return 0;
            }
        }

        public bool Delete(int Id, string connectionString)
        {
            try
            {
                const string SP = "Automation_DeleteEmailAccount";

                using (SqlConnection objSqlCon = new SqlConnection(connectionString))
                {
                    objSqlCon.Open();
                    using (SqlCommand cmd = new SqlCommand(SP, objSqlCon))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@Id", SqlDbType.Int);
                        cmd.Parameters["@Id"].Value = Id;
                        cmd.ExecuteNonQuery();
                        return true;
                    }
                }
            }
            catch
            {
                return false;
            }
        }

        public DataTable GetAll(string connectionString)
        {
            try
            {
                const string SqlQuery = "SELECT * FROM msdb.dbo.sysmail_account";

                using (SqlConnection objSqlCon = new SqlConnection(connectionString))
                {
                    objSqlCon.Open();
                    using (SqlDataAdapter da = new SqlDataAdapter(SqlQuery, objSqlCon))
                    {
                        using (DataTable dt = new DataTable())
                        {
                            da.Fill(dt);
                            return dt;
                        }
                    }
                }
            }
            catch
            {
                return null;
            }
        }
    }
}

