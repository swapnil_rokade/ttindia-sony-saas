using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.Sql;
using System.Data.SqlTypes;
using Microsoft.SqlServer.Server;
using System.Data.SqlClient;
using System.Collections;
using System.Web;

namespace TPS360.Automation
{
    internal class DBJobAgent
    {
        /// <summary>
        /// Active or Inactive Configuration
        /// </summary>
        /// <param name="enable"></param>
        /// <returns></returns>
        public bool ChangeConfiguration(bool enable, string DbConnectionString)
        {
            try
            {
                const string SP = "Automation_Configuration";

                using (SqlConnection objSqlCon = new SqlConnection(DbConnectionString))
                {
                    objSqlCon.Open();
                    using (SqlCommand cmd = new SqlCommand(SP, objSqlCon))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@Enable", SqlDbType.Bit);
                        cmd.Parameters["@Enable"].Value = enable;
                        cmd.ExecuteNonQuery();
                        return true;
                    }
                }
            }
            catch
            {
                return false;
            }
        }

        public bool Create(string JobType, string SqlCommandText, DateTime StartDateTime, DateTime EndDateTime, bool Active, string DbConnectionString)
        {
            try
            {
                if (!String.IsNullOrEmpty(JobType) && !String.IsNullOrEmpty(SqlCommandText) && StartDateTime!=DateTime.MinValue)
                {
                    const string SP = "Automation_CreateJobSchedule";
                   
                    using (SqlConnection objSqlCon = new SqlConnection(DbConnectionString))
                    {
                        objSqlCon.Open();
                        using (SqlCommand cmd = new SqlCommand(SP, objSqlCon))
                        {  
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add("@JobType", SqlDbType.VarChar);
                            cmd.Parameters["@JobType"].Value = JobType;
                            cmd.Parameters.Add("@SqlCommandText", SqlDbType.VarChar);
                            cmd.Parameters["@SqlCommandText"].Value = SqlCommandText;
                            cmd.Parameters.Add("@StartDateTime", SqlDbType.DateTime);
                            cmd.Parameters["@StartDateTime"].Value = StartDateTime;
                            cmd.Parameters.Add("@EndDateTime", SqlDbType.DateTime);
                            cmd.Parameters["@EndDateTime"].Value = EndDateTime;
                            cmd.Parameters.Add("@Active", SqlDbType.Bit);
                            cmd.Parameters["@Active"].Value = Active;
                            
                            cmd.ExecuteNonQuery();
                            return true;
                        }
                    }
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }

        public DataTable GetAll(string DbConnectionString)
        {
            try
            {
                const string SP = "Automation_GetAllJobSchedule";

                using (SqlConnection objSqlCon = new SqlConnection(DbConnectionString))
                {
                    objSqlCon.Open();
                    using (SqlCommand cmd = new SqlCommand(SP, objSqlCon))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            using (DataTable dt = new DataTable())
                            {
                                da.Fill(dt);
                                return dt;
                            }
                        }
                    }
                }
            }
            catch
            {
                return null;
            }
        }

        public bool Delete(string JobId, string DbConnectionString)
        {
            try
            {
                const string SP = "Automation_DeleteJobSchedule";

                using (SqlConnection objSqlCon = new SqlConnection(DbConnectionString))
                {
                    objSqlCon.Open();
                    using (SqlCommand cmd = new SqlCommand(SP, objSqlCon))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@JobId", SqlDbType.VarChar);
                        cmd.Parameters["@JobId"].Value = JobId;
                        cmd.ExecuteNonQuery();
                        return true;
                    }
                }
            }
            catch
            {
                return false;
            }
        }

        public bool Update(string JobId, string JobType, string SqlCommandText, DateTime StartDateTime, DateTime EndDateTime, bool Active, string DbConnectionString)
        {
            try
            {
                if (!String.IsNullOrEmpty(JobId))
                {
                    const string SP = "Automation_UpdateJobSchedule";

                    using (SqlConnection objSqlCon = new SqlConnection(DbConnectionString))
                    {
                        objSqlCon.Open();
                        using (SqlCommand cmd = new SqlCommand(SP, objSqlCon))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add("@JobId", SqlDbType.VarChar);
                            cmd.Parameters["@JobId"].Value = JobId;
                            cmd.Parameters.Add("@JobType", SqlDbType.VarChar);
                            cmd.Parameters["@JobType"].Value = JobType;
                            cmd.Parameters.Add("@SqlCommandText", SqlDbType.VarChar);
                            cmd.Parameters["@SqlCommandText"].Value = SqlCommandText;
                            cmd.Parameters.Add("@StartDateTime", SqlDbType.DateTime);
                            cmd.Parameters["@StartDateTime"].Value = StartDateTime;
                            cmd.Parameters.Add("@EndDateTime", SqlDbType.DateTime);
                            cmd.Parameters["@EndDateTime"].Value = EndDateTime;
                            cmd.Parameters.Add("@Active", SqlDbType.Bit);
                            cmd.Parameters["@Active"].Value = Active;
                            cmd.ExecuteNonQuery();
                            return true;
                        }
                    }
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }
        
    }
}

