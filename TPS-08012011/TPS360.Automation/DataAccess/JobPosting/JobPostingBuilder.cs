﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace TPS360.Automation
{
    internal sealed class JobPostingBuilder : IEntityBuilder<JobPosting>
    {
        IList<JobPosting> IEntityBuilder<JobPosting>.BuildEntities(SqlDataReader reader)
        {
            List<JobPosting> jobPostings = new List<JobPosting>();

            while (reader.Read())
            {
                jobPostings.Add(((IEntityBuilder<JobPosting>)this).BuildEntity(reader));
            }

            return (jobPostings.Count > 0) ? jobPostings : null;
        }

        JobPosting IEntityBuilder<JobPosting>.BuildEntity(SqlDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_JOBTITLE = 1;
            const int FLD_JOBPOSTINGCODE = 2;
            const int FLD_CLIENTJOBID = 3;
            const int FLD_NOOFOPENINGS = 4;
            const int FLD_PAYRATE = 5;
            const int FLD_PAYRATECURRENCYLOOKUPID = 6;
            const int FLD_PAYCYCLE = 7;
            const int FLD_TRAVELREQUIRED = 8;
            const int FLD_TRAVELREQUIREDPERCENT = 9;
            const int FLD_OTHERBENEFITS = 10;
            const int FLD_JOBSTATUS = 11;
            const int FLD_JOBDURATIONLOOKUPID = 12;
            const int FLD_JOBDURATIONMONTH = 13;
            const int FLD_JOBADDRESS1 = 14;
            const int FLD_JOBADDRESS2 = 15;
            const int FLD_CITY = 16;
            const int FLD_ZIPCODE = 17;
            const int FLD_COUNTRYID = 18;
            const int FLD_STATEID = 19;
            const int FLD_STARTDATE = 20;
            const int FLD_FINALHIREDDATE = 21;
            const int FLD_JOBDESCRIPTION = 22;
            const int FLD_AUTHORIZATIONTYPELOOKUPID = 23;
            const int FLD_POSTEDDATE = 24;
            const int FLD_ACTIVATIONDATE = 25;
            const int FLD_REQUIREDDEGREELOOKUPID = 26;
            const int FLD_MINEXPREQUIRED = 27;
            const int FLD_MAXEXPREQUIRED = 28;
            const int FLD_VENDORGROUPID = 29;
            const int FLD_ISJOBACTIVE = 30;
            const int FLD_PUBLISHEDFORINTERNAL = 31;
            const int FLD_PUBLISHEDFORPUBLIC = 32;
            const int FLD_PUBLISHEDFORPARTNER = 33;
            const int FLD_MINAGEREQUIRED = 34;
            const int FLD_MAXAGEREQUIRED = 35;
            const int FLD_JOBTYPE = 36;
            const int FLD_ISAPPROVALREQUIRED = 37;
            const int FLD_INTERNALNOTE = 38;
            const int FLD_CLIENTID = 39;
            const int FLD_CLIENTPROJECTID = 40;
            const int FLD_CLIENTPROJECT = 41;
            const int FLD_CLIENTENDCLIENTID = 42;
            const int FLD_CLIENTHOURLYRATE = 43;
            const int FLD_CLIENTHOURLYRATECURRENCYLOOKUPID = 44;
            const int FLD_CLIENTRATEPAYCYCLE = 45;
            const int FLD_CLIENTDISPLAYNAME = 46;
            const int FLD_CLIENTID2 = 47;
            const int FLD_CLIENTID3 = 48;
            const int FLD_CLIENTJOBDESCRIPTION = 49;
            const int FLD_TAXTERMLOOKUPIDS = 50;
            const int FLD_JOBCATEGORYLOOKUPID = 51;
            const int FLD_JOBCATEGORYSUBID = 52;
            const int FLD_JOBINDUSTRYLOOKUPID = 53;
            const int FLD_EXPECTEDREVENUE = 54;
            const int FLD_EXPECTEDREVENUECURRENCYLOOKUPID = 55;
            const int FLD_SOURCINGCHANNEL = 56;
            const int FLD_SOURCINGEXPENSES = 57;
            const int FLD_SOURCINGEXPENSESCURRENCYLOOKUPID = 58;
            const int FLD_WORKFLOWAPPROVED = 59;
            const int FLD_PUBLISHEDFORVENDOR = 60;
            const int FLD_TELECOMMUNICATION = 61;
            const int FLD_ISTEMPLATE = 62;
            const int FLD_ISREMOVED = 63;
            const int FLD_CREATORID = 64;
            const int FLD_UPDATORID = 65;
            const int FLD_CREATEDATE = 66;
            const int FLD_UPDATEDATE = 67;

            JobPosting jobPosting = new JobPosting();

            jobPosting.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            jobPosting.JobTitle = reader.IsDBNull(FLD_JOBTITLE) ? string.Empty : reader.GetString(FLD_JOBTITLE);
            jobPosting.JobPostingCode = reader.IsDBNull(FLD_JOBPOSTINGCODE) ? string.Empty : reader.GetString(FLD_JOBPOSTINGCODE);
            jobPosting.ClientJobId = reader.IsDBNull(FLD_CLIENTJOBID) ? string.Empty : reader.GetString(FLD_CLIENTJOBID);
            jobPosting.NoOfOpenings = reader.IsDBNull(FLD_NOOFOPENINGS) ? 0 : reader.GetInt32(FLD_NOOFOPENINGS);
            jobPosting.PayRate = reader.IsDBNull(FLD_PAYRATE) ? string.Empty : reader.GetString(FLD_PAYRATE);
            jobPosting.PayRateCurrencyLookupId = reader.IsDBNull(FLD_PAYRATECURRENCYLOOKUPID) ? 0 : reader.GetInt32(FLD_PAYRATECURRENCYLOOKUPID);
            jobPosting.PayCycle = reader.IsDBNull(FLD_PAYCYCLE) ? string.Empty : reader.GetString(FLD_PAYCYCLE);
            jobPosting.TravelRequired = reader.IsDBNull(FLD_TRAVELREQUIRED) ? false : reader.GetBoolean(FLD_TRAVELREQUIRED);
            jobPosting.TravelRequiredPercent = reader.IsDBNull(FLD_TRAVELREQUIREDPERCENT) ? string.Empty : reader.GetString(FLD_TRAVELREQUIREDPERCENT);
            jobPosting.OtherBenefits = reader.IsDBNull(FLD_OTHERBENEFITS) ? string.Empty : reader.GetString(FLD_OTHERBENEFITS);
            jobPosting.JobStatus = reader.IsDBNull(FLD_JOBSTATUS) ? 0 : reader.GetInt32(FLD_JOBSTATUS);
            jobPosting.JobDurationLookupId = reader.IsDBNull(FLD_JOBDURATIONLOOKUPID) ? 0 : reader.GetInt32(FLD_JOBDURATIONLOOKUPID);
            jobPosting.JobDurationMonth = reader.IsDBNull(FLD_JOBDURATIONMONTH) ? string.Empty : reader.GetString(FLD_JOBDURATIONMONTH);
            jobPosting.JobAddress1 = reader.IsDBNull(FLD_JOBADDRESS1) ? string.Empty : reader.GetString(FLD_JOBADDRESS1);
            jobPosting.JobAddress2 = reader.IsDBNull(FLD_JOBADDRESS2) ? string.Empty : reader.GetString(FLD_JOBADDRESS2);
            jobPosting.City = reader.IsDBNull(FLD_CITY) ? string.Empty : reader.GetString(FLD_CITY);
            jobPosting.ZipCode = reader.IsDBNull(FLD_ZIPCODE) ? string.Empty : reader.GetString(FLD_ZIPCODE);
            jobPosting.CountryId = reader.IsDBNull(FLD_COUNTRYID) ? 0 : reader.GetInt32(FLD_COUNTRYID);
            jobPosting.StateId = reader.IsDBNull(FLD_STATEID) ? 0 : reader.GetInt32(FLD_STATEID);
            jobPosting.StartDate = reader.IsDBNull(FLD_STARTDATE) ? string.Empty : reader.GetString(FLD_STARTDATE);
            jobPosting.FinalHiredDate = reader.IsDBNull(FLD_FINALHIREDDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_FINALHIREDDATE);
            jobPosting.JobDescription = reader.IsDBNull(FLD_JOBDESCRIPTION) ? string.Empty : reader.GetString(FLD_JOBDESCRIPTION);
            jobPosting.AuthorizationTypeLookupId = reader.IsDBNull(FLD_AUTHORIZATIONTYPELOOKUPID) ? string.Empty : reader.GetString(FLD_AUTHORIZATIONTYPELOOKUPID);
            jobPosting.PostedDate = reader.IsDBNull(FLD_POSTEDDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_POSTEDDATE);
            jobPosting.ActivationDate = reader.IsDBNull(FLD_ACTIVATIONDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_ACTIVATIONDATE);
            jobPosting.RequiredDegreeLookupId = reader.IsDBNull(FLD_REQUIREDDEGREELOOKUPID) ? 0 : reader.GetInt32(FLD_REQUIREDDEGREELOOKUPID);
            jobPosting.MinExpRequired = reader.IsDBNull(FLD_MINEXPREQUIRED) ? string.Empty : reader.GetString(FLD_MINEXPREQUIRED);
            jobPosting.MaxExpRequired = reader.IsDBNull(FLD_MAXEXPREQUIRED) ? string.Empty : reader.GetString(FLD_MAXEXPREQUIRED);
            jobPosting.VendorGroupId = reader.IsDBNull(FLD_VENDORGROUPID) ? 0 : reader.GetInt32(FLD_VENDORGROUPID);
            jobPosting.IsJobActive = reader.IsDBNull(FLD_ISJOBACTIVE) ? false : reader.GetBoolean(FLD_ISJOBACTIVE);
            jobPosting.PublishedForInternal = reader.IsDBNull(FLD_PUBLISHEDFORINTERNAL) ? false : reader.GetBoolean(FLD_PUBLISHEDFORINTERNAL);
            jobPosting.PublishedForPublic = reader.IsDBNull(FLD_PUBLISHEDFORPUBLIC) ? false : reader.GetBoolean(FLD_PUBLISHEDFORPUBLIC);
            jobPosting.PublishedForPartner = reader.IsDBNull(FLD_PUBLISHEDFORPARTNER) ? false : reader.GetBoolean(FLD_PUBLISHEDFORPARTNER);
            jobPosting.MinAgeRequired = reader.IsDBNull(FLD_MINAGEREQUIRED) ? 0 : reader.GetInt32(FLD_MINAGEREQUIRED);
            jobPosting.MaxAgeRequired = reader.IsDBNull(FLD_MAXAGEREQUIRED) ? 0 : reader.GetInt32(FLD_MAXAGEREQUIRED);
            jobPosting.JobType = reader.IsDBNull(FLD_JOBTYPE) ? 0 : reader.GetInt32(FLD_JOBTYPE);
            jobPosting.IsApprovalRequired = reader.IsDBNull(FLD_ISAPPROVALREQUIRED) ? false : reader.GetBoolean(FLD_ISAPPROVALREQUIRED);
            jobPosting.InternalNote = reader.IsDBNull(FLD_INTERNALNOTE) ? string.Empty : reader.GetString(FLD_INTERNALNOTE);
            jobPosting.ClientId = reader.IsDBNull(FLD_CLIENTID) ? 0 : reader.GetInt32(FLD_CLIENTID);
            jobPosting.ClientProjectId = reader.IsDBNull(FLD_CLIENTPROJECTID) ? 0 : reader.GetInt32(FLD_CLIENTPROJECTID);
            jobPosting.ClientProject = reader.IsDBNull(FLD_CLIENTPROJECT) ? string.Empty : reader.GetString(FLD_CLIENTPROJECT);
            jobPosting.ClientEndClientId = reader.IsDBNull(FLD_CLIENTENDCLIENTID) ? 0 : reader.GetInt32(FLD_CLIENTENDCLIENTID);
            jobPosting.ClientHourlyRate = reader.IsDBNull(FLD_CLIENTHOURLYRATE) ? string.Empty : reader.GetString(FLD_CLIENTHOURLYRATE);
            jobPosting.ClientHourlyRateCurrencyLookupId = reader.IsDBNull(FLD_CLIENTHOURLYRATECURRENCYLOOKUPID) ? 0 : reader.GetInt32(FLD_CLIENTHOURLYRATECURRENCYLOOKUPID);
            jobPosting.ClientRatePayCycle = reader.IsDBNull(FLD_CLIENTRATEPAYCYCLE) ? string.Empty : reader.GetString(FLD_CLIENTRATEPAYCYCLE);
            jobPosting.ClientDisplayName = reader.IsDBNull(FLD_CLIENTDISPLAYNAME) ? string.Empty : reader.GetString(FLD_CLIENTDISPLAYNAME);
            jobPosting.ClientId2 = reader.IsDBNull(FLD_CLIENTID2) ? 0 : reader.GetInt32(FLD_CLIENTID2);
            jobPosting.ClientId3 = reader.IsDBNull(FLD_CLIENTID3) ? 0 : reader.GetInt32(FLD_CLIENTID3);
            jobPosting.ClientJobDescription = reader.IsDBNull(FLD_CLIENTJOBDESCRIPTION) ? string.Empty : reader.GetString(FLD_CLIENTJOBDESCRIPTION);
            jobPosting.TaxTermLookupIds = reader.IsDBNull(FLD_TAXTERMLOOKUPIDS) ? string.Empty : reader.GetString(FLD_TAXTERMLOOKUPIDS);
            jobPosting.JobCategoryLookupId = reader.IsDBNull(FLD_JOBCATEGORYLOOKUPID) ? 0 : reader.GetInt32(FLD_JOBCATEGORYLOOKUPID);
            jobPosting.JobCategorySubId = reader.IsDBNull(FLD_JOBCATEGORYSUBID) ? 0 : reader.GetInt32(FLD_JOBCATEGORYSUBID);
            jobPosting.JobIndustryLookupId = reader.IsDBNull(FLD_JOBINDUSTRYLOOKUPID) ? 0 : reader.GetInt32(FLD_JOBINDUSTRYLOOKUPID);
            jobPosting.ExpectedRevenue = reader.IsDBNull(FLD_EXPECTEDREVENUE) ? 0 : reader.GetDecimal(FLD_EXPECTEDREVENUE);
            jobPosting.ExpectedRevenueCurrencyLookupId = reader.IsDBNull(FLD_EXPECTEDREVENUECURRENCYLOOKUPID) ? 0 : reader.GetInt32(FLD_EXPECTEDREVENUECURRENCYLOOKUPID);
            jobPosting.SourcingChannel = reader.IsDBNull(FLD_SOURCINGCHANNEL) ? string.Empty : reader.GetString(FLD_SOURCINGCHANNEL);
            jobPosting.SourcingExpenses = reader.IsDBNull(FLD_SOURCINGEXPENSES) ? 0 : reader.GetDecimal(FLD_SOURCINGEXPENSES);
            jobPosting.SourcingExpensesCurrencyLookupId = reader.IsDBNull(FLD_SOURCINGEXPENSESCURRENCYLOOKUPID) ? 0 : reader.GetInt32(FLD_SOURCINGEXPENSESCURRENCYLOOKUPID);
            jobPosting.WorkflowApproved = reader.IsDBNull(FLD_WORKFLOWAPPROVED) ? false : reader.GetBoolean(FLD_WORKFLOWAPPROVED);
            jobPosting.PublishedForVendor = reader.IsDBNull(FLD_PUBLISHEDFORVENDOR) ? false : reader.GetBoolean(FLD_PUBLISHEDFORVENDOR);
            jobPosting.TeleCommunication = reader.IsDBNull(FLD_TELECOMMUNICATION) ? false : reader.GetBoolean(FLD_TELECOMMUNICATION);
            jobPosting.IsTemplate = reader.IsDBNull(FLD_ISTEMPLATE) ? false : reader.GetBoolean(FLD_ISTEMPLATE);
            jobPosting.IsRemoved = reader.IsDBNull(FLD_ISREMOVED) ? false : reader.GetBoolean(FLD_ISREMOVED);
            jobPosting.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
            jobPosting.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);
            jobPosting.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
            jobPosting.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);

            return jobPosting;
        }
    }
}