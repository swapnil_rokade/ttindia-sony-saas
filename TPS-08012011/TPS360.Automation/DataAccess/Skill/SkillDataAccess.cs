﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;

using System.Data.SqlClient;


namespace TPS360.Automation
{
    internal sealed class SkillDataAccess : BaseDataAccess
    {
        #region Constructor
        public SkillDataAccess()
        {
        }
        protected override IEntityBuilder<Skill> CreateEntityBuilder<Skill>()
        {
            return (new SkillBuilder()) as IEntityBuilder<Skill>;
        }
        #endregion
        #region Method
        public Skill GetById(int id)
        {
            const string SP = "Skill_GetById";

            using (SqlConnection objSqlCon = new SqlConnection("context connection=true;"))
            {
                objSqlCon.Open();
                using (SqlCommand cmd = new SqlCommand(SP, objSqlCon))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@Id", SqlDbType.Int);
                    cmd.Parameters["@Id"].Value = id;

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            return CreateEntityBuilder<Skill>().BuildEntity(reader);
                        }
                        else
                        {
                            return null;
                        }
                    }
                }
            }
        }       
        #endregion
    }
}
