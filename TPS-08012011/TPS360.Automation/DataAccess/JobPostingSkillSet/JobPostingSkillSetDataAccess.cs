﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;

using System.Data.SqlClient;


namespace TPS360.Automation
{
    internal sealed class JobPostingSkillSetDataAccess : BaseDataAccess
    {
        #region Constructors

        public JobPostingSkillSetDataAccess()            
        {
        }

        protected override IEntityBuilder<JobPostingSkillSet> CreateEntityBuilder<JobPostingSkillSet>()
        {
            return (new JobPostingSkillSetBuilder()) as IEntityBuilder<JobPostingSkillSet>;
        }


        #endregion

        #region Method     
        public IList<JobPostingSkillSet> GetAllByJobPostingId(int jobPostingId)
        {
            if (jobPostingId < 1)
            {
                throw new ArgumentNullException("jobPostingId");
            }

            const string SP = "JobPostingSkillSet_GetJobPostingSkillSetByJobPostingId";

            using (SqlConnection objSqlCon = new SqlConnection("context connection=true;"))
            {
                objSqlCon.Open();
                using (SqlCommand cmd = new SqlCommand(SP, objSqlCon))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@jobPostingId", SqlDbType.Int);
                    cmd.Parameters["@jobPostingId"].Value = jobPostingId;

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        return CreateEntityBuilder<JobPostingSkillSet>().BuildEntities(reader);
                    }
                }
            }
        }
        #endregion
    }
}
