﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;

using System.Data.SqlClient;


namespace TPS360.Automation
{
    internal sealed class TestMasterDataAccess : BaseDataAccess
    {
        #region Constructors

        public TestMasterDataAccess()
            : base()
        {
        }

        protected override IEntityBuilder<TestMaster> CreateEntityBuilder<TestMaster>()
        {
            return (new TestMasterBuilder()) as IEntityBuilder<TestMaster>;
        }

        #endregion

        #region Method
        public TestMaster GetById(int id)
        {
            const string SP = "TestMaster_GetById";

            using (SqlConnection objSqlCon = new SqlConnection("context connection=true;"))
            {
                objSqlCon.Open();
                using (SqlCommand cmd = new SqlCommand(SP, objSqlCon))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@Id", SqlDbType.Int);
                    cmd.Parameters["@Id"].Value = id;

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            return CreateEntityBuilder<TestMaster>().BuildEntity(reader);
                        }
                        else
                        {
                            return null;
                        }
                    }
                }
            }
        }
        
        #endregion
    }
}
