﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Data.SqlClient;


namespace TPS360.Automation
{
    internal sealed class TestMasterBuilder : IEntityBuilder<TestMaster>
    {
        IList<TestMaster> IEntityBuilder<TestMaster>.BuildEntities(SqlDataReader reader)
        {
            List<TestMaster> testMasters = new List<TestMaster>();

            while (reader.Read())
            {
                testMasters.Add(((IEntityBuilder<TestMaster>)this).BuildEntity(reader));
            }

            return (testMasters.Count > 0) ? testMasters : null;
        }

        TestMaster IEntityBuilder<TestMaster>.BuildEntity(SqlDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_NAME = 1;
            const int FLD_DESCRIPTION = 2;
            const int FLD_TYPE = 3;
            const int FLD_NUMBEROFDISPLAYQUESTIONS = 4;
            const int FLD_DURATION = 5;
            const int FLD_TOTALSCORE = 6;
            const int FLD_PASSINGSCORE = 7;
            const int FLD_NEGATIVEMARKING = 8;
            const int FLD_NEGATIVEMARKS = 9;
            const int FLD_ISPUBLISH = 10;
            const int FLD_ISFROMEXISTINGTEST = 11;
            const int FLD_ISREMOVED = 12;
            const int FLD_PUBLISHERID = 13;
            const int FLD_CREATORID = 14;
            const int FLD_UPDATORID = 15;
            const int FLD_PUBLISHDATE = 16;
            const int FLD_CREATEDATE = 17;
            const int FLD_UPDATEDATE = 18;

            TestMaster testMaster = new TestMaster();

            testMaster.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            testMaster.Name = reader.IsDBNull(FLD_NAME) ? string.Empty : reader.GetString(FLD_NAME);
            testMaster.Description = reader.IsDBNull(FLD_DESCRIPTION) ? string.Empty : reader.GetString(FLD_DESCRIPTION);
            testMaster.Type = reader.IsDBNull(FLD_TYPE) ? 0 : reader.GetInt32(FLD_TYPE);
            testMaster.NumberOfDisplayQuestions = reader.IsDBNull(FLD_NUMBEROFDISPLAYQUESTIONS) ? 0 : reader.GetInt32(FLD_NUMBEROFDISPLAYQUESTIONS);
            testMaster.Duration = reader.IsDBNull(FLD_DURATION) ? string.Empty : reader.GetString(FLD_DURATION);
            testMaster.TotalScore = reader.IsDBNull(FLD_TOTALSCORE) ? 0 : reader.GetInt32(FLD_TOTALSCORE);
            testMaster.PassingScore = reader.IsDBNull(FLD_PASSINGSCORE) ? 0 : reader.GetInt32(FLD_PASSINGSCORE);
            testMaster.NegativeMarking = reader.IsDBNull(FLD_NEGATIVEMARKING) ? false : reader.GetBoolean(FLD_NEGATIVEMARKING);
            testMaster.NegativeMarks = reader.IsDBNull(FLD_NEGATIVEMARKS) ? 0 : reader.GetInt32(FLD_NEGATIVEMARKS);
            testMaster.PublishStatus = reader.IsDBNull(FLD_ISPUBLISH) ? 0 : reader.GetInt32(FLD_ISPUBLISH);
            testMaster.IsFromExistingTest = reader.IsDBNull(FLD_ISFROMEXISTINGTEST) ? false : reader.GetBoolean(FLD_ISFROMEXISTINGTEST);
            testMaster.IsRemoved = reader.IsDBNull(FLD_ISREMOVED) ? false : reader.GetBoolean(FLD_ISREMOVED);
            testMaster.PublisherId = reader.IsDBNull(FLD_PUBLISHERID) ? 0 : reader.GetInt32(FLD_PUBLISHERID);
            testMaster.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
            testMaster.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);
            testMaster.PublishDate = reader.IsDBNull(FLD_PUBLISHDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_PUBLISHDATE);
            testMaster.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
            testMaster.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);

            return testMaster;
        }
    }
}
