﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;

using System.Data.SqlClient;


namespace TPS360.Automation
{
    internal sealed class StateDataAccess : BaseDataAccess
    {
        #region Constructor
        public  StateDataAccess()
        {
        }
        protected override IEntityBuilder<State> CreateEntityBuilder<State>()
        {
            return (new StateBuilder()) as IEntityBuilder<State>; 
        }
        #endregion
        #region Method
        public State GetById(int id)
        {
            const string SP = "State_GetById";

            using (SqlConnection objSqlCon = new SqlConnection("context connection=true;"))
            {
                objSqlCon.Open();
                using (SqlCommand cmd = new SqlCommand(SP, objSqlCon))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@Id", SqlDbType.Int);
                    cmd.Parameters["@Id"].Value = id;

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            return CreateEntityBuilder<State>().BuildEntity(reader);
                        }
                        else
                        {
                            return null;
                        }
                    }
                }
            }
        }
        #endregion
    }
}
