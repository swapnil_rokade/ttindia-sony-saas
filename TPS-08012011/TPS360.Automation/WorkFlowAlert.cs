﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Microsoft.SqlServer.Server;
using System.Text;

namespace TPS360.Automation
{
    static class WorkFlowAlert
    {
        public static string GetAlertForBirthdayNotification(string candidateName,string candidateEmail)
        {
            StringBuilder strHTMLText = new StringBuilder();
            strHTMLText.Append("<table width=780px border=0 align=center cellpadding=0 cellspacing=0>");
            strHTMLText.Append("    <tr>");
            strHTMLText.Append("        <td colspan=4>");
            strHTMLText.Append("            <table width=100% border=0 cellspacing=0 cellpadding=5>");
            strHTMLText.Append("                <tr>");
            strHTMLText.Append("                    <td class=JSFormHeading>Application Work Flow Alert</td>");
            strHTMLText.Append("                </tr>");
            strHTMLText.Append("                <tr>");
            strHTMLText.Append("                    <td height=5 bgcolor=#FFCC00><SPACER height=1 width=1 type=block></td>");
            strHTMLText.Append("		        </tr>");
            strHTMLText.Append("            </table>");
            strHTMLText.Append("        </td>");
            strHTMLText.Append("    </tr>");
            strHTMLText.Append("    <tr>");
            strHTMLText.Append("        <td>");
            strHTMLText.Append("            <table width=100% border=0 cellspacing=1 cellpadding=0>");
            strHTMLText.Append("                <tr>");
            strHTMLText.Append("                    <td valign=top align=left width=100%>");
            strHTMLText.Append("                        <table width=100% border=0 cellspacing=1 cellpadding=5>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Alert Type</td>");
            strHTMLText.Append("                                <td>Candidate Birthday Notification</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Alert Time</td>");
            strHTMLText.Append("                                <td>" + DateTime.Now.ToLongDateString() + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Name</td>");
            strHTMLText.Append("                                <td>" + candidateName + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>User Id</td>");
            strHTMLText.Append("                                <td>" + candidateEmail + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                        </table>");
            strHTMLText.Append("                    </td>");
            strHTMLText.Append("                </tr>");
            strHTMLText.Append("            </table>");
            strHTMLText.Append("        </td>");
            strHTMLText.Append("    </tr>");

            strHTMLText.Append("</table>");

            return strHTMLText.ToString();
        }
    }
}
